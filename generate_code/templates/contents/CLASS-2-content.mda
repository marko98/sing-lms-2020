<%  def packageNames = c.owner.getFullyQualifiedName(".") %><% if(c.hasStereotype("generate")) { %><%  if(c.hasStereotype("entity")) { %>package $packageNames;

@javax.persistence.Entity
${jpa.classifierSignature(c)} {
    ${jpa.primaryKey(c)}

<%  def atts = c.attributes.findAll({it.name && !jpa.isIdentifier(it)}) %>
<%  atts.each() { %>
    ${jpa.attribute(it)}
<%  } %>

    public ${c.name}() {
		super();
    }

<%  atts.each() { %>
    ${jpa.getter(it)}
    ${jpa.setter(it)}
<%  } %>
}<%  } else if(c.hasStereotype("class")) { %>package $packageNames;

<% 
    def isGenericClass = false;
    def isAbstractClass = false;
    def className = c.name;
    c.stereotypes.each() {
        if(it.name.contains("generic")){
            isGenericClass = true;
            className += "<";
            if(it.name.contains("2")){
                className += "T, E";
            } else if(it.name.contains("3")){
                className += "T, E, G";
            } else {
                className += "T"
            }
            className += ">";
        } else if (it.name == "abstract") {
            isAbstractClass = true;
        }
    }

    if(isAbstractClass) {
        className = 'abstract class ' + className;
    } else {
        className = "class " + className;
    }

    def visibility = 'public ' 
    if(c.visibility.toString() == "PRIVATE") { 
        visibility = 'private '
    }

    className = visibility + className;

    if (c.generalizations.size > 0) {
        className += " extends " + c.generalizations.get(0).name;
    }

    def interfaces = c.realizations;
    def interfacesSize = interfaces.size;
    if (interfacesSize > 0) {
        className += " implements ";
    }
    def index = 0;
    interfaces.each() {
        index += 1;
        className += it.name;
        if (index < interfacesSize) {
            className += ", ";
        }
    }
%>

$className {
<% 
    def attributes =  c.attributes;
    attributes.each() {
            def isAutowired = false;
            def stereotypes = it.stereotypes;
            stereotypes.each() {
                if(it.name == "autowired"){
                    isAutowired = true;
                }
            } 
        %>

        <% if (isAutowired) { %>
            @Autowired${jpa.attribute(it)}
        <% } else { %>
            ${jpa.attribute(it)}
        <% } %>
    <% }
%>

    public ${c.name}() {
		super();
    }
<%  
    def operations = c.operations.findAll({it.name});
    operations.each() { 
        if (it.name == c.name) {
            return;
        }

        def text = '';

        def returnStereotype = false;
        def returnStereotypeName = '';
        def stereotypes = it.stereotypes;
        stereotypes.each() {
            if(it.name.contains("public") || it.name.contains("private")){
                returnStereotype = true;
                returnStereotypeName = it.name;
            }
        }

        if (returnStereotype) {
            text += returnStereotypeName + ' ';
        } else {
            if(it.visibility.toString() == "PRIVATE") { 
                text = 'private '
            } else { 
                text = 'public ' 
            }
            text += it.returnType.name + ' ';
        }

        text += it.name + '(';
        
        
        def params = it.parameters;
        def size = params.size;
        def l = 0;
        params.each() { 
            l += 1;
            text += it.dataType.name + ' ' + it.name;
            if (l < size) {
                text += ', ';
            }
        }

        text += ') {}';
    %>
    $text
<% } %>

<%  attributes.each() { %>
    ${jpa.getter(it)}
    ${jpa.setter(it)}
<%  } %>
}<% } else if(c.hasStereotype("interface") && !c.hasStereotype("repository") && !c.hasStereotype("springframework")) { %>package $packageNames;

<% 
    def isGenericInterface = false;
    def isAbstractInterface = false;
    def interfaceName = c.name;
    c.stereotypes.each() {
        if(it.name.contains("generic")){
            isGenericInterface = true;
            interfaceName += "<";
            if(it.name.contains("2")){
                interfaceName += "T, E";
            } else if(it.name.contains("3")){
                interfaceName += "T, E, G";
            } else {
                interfaceName += "T"
            }
            interfaceName += ">";
        } else if (it.name == "abstract") {
            isAbstractInterface = true;
        }
    }

    if(isAbstractInterface) {
        interfaceName = 'abstract interface ' + interfaceName;
    } else {
        interfaceName = "interface " + interfaceName;
    }

    def visibility = 'public ' 
    if(c.visibility.toString() == "PRIVATE") { 
        visibility = 'private '
    }

    interfaceName = visibility + interfaceName;

    if (c.generalizations.size > 0) {
        interfaceName += " extends " + c.generalizations.get(0).name;
    }
%>

$interfaceName {
<%  def operations = c.operations.findAll({it.name});
    operations.each() { 
        def text; 
        
        if(it.visibility.toString() == "PRIVATE") { 
            text = 'private '
        } else { 
            text = 'public ' 
        }

        text += it.returnType.name + ' ' + it.name + '(';
        def params = it.parameters;
        def size = params.size;
        def l = 0;
        params.each() { 
            l += 1;
            text += it.dataType.name + ' ' + it.name;
            if (l < size) {
                text += ', ';
            }
        }

        text += ');';
    %>
    $text
<% } %>
}<%  } else if (c.hasStereotype("interface") && c.hasStereotype("repository") && !c.hasStereotype("springframework")) { %>package ${c.owner.getFullyQualifiedName(".")};
<% 
    def isGenericInterface = false;
    def isAbstractInterface = false;
    def interfaceName = c.name;
    c.stereotypes.each() {
        if(it.name.contains("generic")){
            isGenericInterface = true;
            interfaceName += "<";
            if(it.name.contains("2")){
                interfaceName += "T, E";
            } else if(it.name.contains("3")){
                interfaceName += "T, E, G";
            } else {
                interfaceName += "T"
            }
            interfaceName += ">";
        } else if (it.name == "abstract") {
            isAbstractInterface = true;
        }
    }

    if(isAbstractInterface) {
        interfaceName = 'abstract interface ' + interfaceName;
    } else {
        interfaceName = "interface " + interfaceName;
    }

    def visibility = 'public ' 
    if(c.visibility.toString() == "PRIVATE") { 
        visibility = 'private '
    }

    interfaceName = visibility + interfaceName;

    if (c.generalizations.size > 0) {
        interfaceName += " extends " + c.generalizations.get(0).name;
    }
%>
<% if (c.hasStereotype("singleton") || c.hasStereotype("prototype")) { %>import org.springframework.context.annotation.Scope;
<% } %><% c.generalizations.each { %><% if (it.name.contains("PagingAndSortingRepository")) { %>import org.springframework.data.repository.PagingAndSortingRepository;
<% } %><% } %>import org.springframework.stereotype.Repository;

@Repository<% if (c.hasStereotype("singleton")) { %>
@Scope("singleton")<% } %><% if (c.hasStereotype("prototype")) { %>
@Scope("prototype")<% } %>
$interfaceName {
<%  def operations = c.operations.findAll({it.name});
    operations.each() { 
        def text; 
        
        if(it.visibility.toString() == "PRIVATE") { 
            text = 'private '
        } else { 
            text = 'public ' 
        }

        text += it.returnType.name + ' ' + it.name + '(';
        def params = it.parameters;
        def size = params.size;
        def l = 0;
        params.each() { 
            l += 1;
            text += it.dataType.name + ' ' + it.name;
            if (l < size) {
                text += ', ';
            }
        }

        text += ');';
    %>
    $text
<% } %>
}<% } %><% } %>