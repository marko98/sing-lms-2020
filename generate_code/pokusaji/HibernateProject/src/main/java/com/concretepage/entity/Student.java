package com.concretepage.entity;

public interface Student {
    String getName();
    int getAge();
}
