package com.concretepage;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import com.concretepage.entity.Boy;
import com.concretepage.entity.College;
import com.concretepage.entity.Girl;
import com.concretepage.entity.Student;

public class HibernateManyToAnyDemo {
	public static void main(String[] args) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		Boy boy = new Boy();
		boy.setId(1);
		boy.setName("Mukesh");
		boy.setAge(30);
		session.persist(boy);
		
		Girl girl = new Girl();
		girl.setId(1);
		girl.setName("Neerja");
		girl.setAge(25);
		session.persist(girl);
		
        College college = new College();
        college.setId(1);
        college.setName("ABCD");
        List<Student> students = new ArrayList<>();
        students.add(boy);
        students.add(girl);
        college.setStudents(students);
		session.persist(college);
		
		session.getTransaction().commit();
		
		College col = (College) session.get(College.class, 1);
		for(Student student: col.getStudents()) {
			System.out.println(student.getName()+":"+ student.getAge());
		}
		session.close();
		HibernateUtil.closeSessionFactory();
	}
}
