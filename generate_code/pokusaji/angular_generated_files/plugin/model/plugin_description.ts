
export class PluginDescription {

    private name: string;
        
    private categories: List<String>;
        
    private description: string;
        
    private scheme: string;
        
    private host: string;
        
    private endpoints: HashMap<String, HashMap<HttpMethod, HashMap<String, String>>> = new HashMap<String, HashMap<HttpMethod, HashMap<String, String>>>();
        


    public getName = (): string => {
        return this.name;
    }

    public setName = (name: string): void => {
        this.name = name;
    }
    
    public getCategories = (): List<String> => {
        return this.categories;
    }

    public setCategories = (categories: List<String>): void => {
        this.categories = categories;
    }
    
    public getDescription = (): string => {
        return this.description;
    }

    public setDescription = (description: string): void => {
        this.description = description;
    }
    
    public getScheme = (): string => {
        return this.scheme;
    }

    public setScheme = (scheme: string): void => {
        this.scheme = scheme;
    }
    
    public getHost = (): string => {
        return this.host;
    }

    public setHost = (host: string): void => {
        this.host = host;
    }
    
    public getEndpoints = (): HashMap<String, HashMap<HttpMethod, HashMap<String, String>>> => {
        return this.endpoints;
    }

    public setEndpoints = (endpoints: HashMap<String, HashMap<HttpMethod, HashMap<String, String>>>): void => {
        this.endpoints = endpoints;
    }
    

    public constructor () {
    }

    public constructor () {
	super();
	this.name = name;
	this.categories = categories;
	this.description = description;
	this.scheme = scheme;
	this.host = host;
	this.endpoints = endpoints;
}

    public constructor () {
	super();
	this.name = name;
	this.categories = new ArrayList<String>();
	this.description = description;
	this.scheme = scheme;
	this.host = host;
	this.endpoints = endpoints;
}

    public getUrl (): String {
	return this.scheme + "://" + this.host;
    }

    public getEndpointUrl (category: String, method: HttpMethod, endpointName: String): String {
	HashMap<String, String> existingEndpoitns = this.endpoints.get(category).get(method);
	if (existingEndpoitns != null) {
	    return existingEndpoitns.get(endpointName);
	}
	return null;
    }


}
