
export class Plugin {

    private description: PluginDescription;
        


    public getDescription = (): PluginDescription => {
        return this.description;
    }

    public setDescription = (description: PluginDescription): void => {
        this.description = description;
    }
    

    public constructor () {
    }

    public constructor () {
	this.description = description;
}

    public findAll (category: String, method: HttpMethod, targetEndpoint: String, pageable: Pageable, httpHeaders: HttpHeaders): ResponseEntity<?> {
	RestTemplate rt = this.getRestTemplateWithCustomErrorHandling();
	
	HttpEntity<Object> request = new HttpEntity<Object>(httpHeaders);
	
//	return rt.getForEntity(this.getUriString(category, method, targetEndpoint, "", pageable), Object.class);
	return rt.exchange(this.getUriString(category, method, targetEndpoint, "", pageable), HttpMethod.GET, request, Object.class);
    }

    public findOne (category: String, method: HttpMethod, targetEndpoint: String, id: String, httpHeaders: HttpHeaders): ResponseEntity<?> {
	RestTemplate rt = this.getRestTemplateWithCustomErrorHandling();
	
	HttpEntity<Object> request = new HttpEntity<Object>(httpHeaders);

//	return rt.getForEntity(this.getUriString(category, method, targetEndpoint, id, null), Object.class);
	return rt.exchange(this.getUriString(category, method, targetEndpoint, id, null), HttpMethod.GET, request, Object.class);
    }

    public create (category: String, method: HttpMethod, targetEndpoint: String, model: Entitet<?, ?>, httpHeaders: HttpHeaders): ResponseEntity<?> {
	RestTemplate rt = this.getRestTemplateWithCustomErrorHandling();
	
	/**
	 * pri instanciranju HttpEntity klase nudi izmedju ostalog konstruktor koji prima body i header-e
	 * */
	HttpEntity<Object> request = new HttpEntity<Object>(model, httpHeaders);

//	return rt.postForEntity(this.getUriString(category, method, targetEndpoint, "", null), model, Object.class);
	return rt.exchange(this.getUriString(category, method, targetEndpoint, "", null), HttpMethod.POST, request, Object.class);
    }

    public update (category: String, method: HttpMethod, targetEndpoint: String, model: Entitet<?, ?>, httpHeaders: HttpHeaders): void {
	RestTemplate rt = this.getRestTemplateWithCustomErrorHandling();

	/**
	 * pri instanciranju HttpEntity klase nudi izmedju ostalog konstruktor koji prima body i header-e
	 * */
	HttpEntity<Object> request = new HttpEntity<Object>(model, httpHeaders);
	
//	rt.put(this.getUriString(category, method, targetEndpoint, "", null), model);
	rt.exchange(this.getUriString(category, method, targetEndpoint, "", null), HttpMethod.PUT, request, Object.class);
    }

    public delete (category: String, method: HttpMethod, targetEndpoint: String, id: String, httpHeaders: HttpHeaders): void {
	RestTemplate rt = this.getRestTemplateWithCustomErrorHandling();

	HttpEntity<Object> request = new HttpEntity<Object>(httpHeaders);
	
//	rt.delete(this.getUriString(category, method, targetEndpoint, id, null));
	rt.exchange(this.getUriString(category, method, targetEndpoint, id, null), HttpMethod.DELETE, request, Object.class);
    }

    public getRestTemplateWithCustomErrorHandling (): RestTemplate {
	RestTemplateBuilder builder = new RestTemplateBuilder();
	RestTemplate rt = builder.errorHandler(new RestTemplateResponseErrorHandler()).build();
	return rt;
    }

    public getUriString (category: String, method: HttpMethod, targetEndpoint: String, id: String, pageable: Pageable): String {
	UriComponentsBuilder builder = UriComponentsBuilder.newInstance().scheme(description.getScheme())
		.host(description.getHost()).path(description.getEndpointUrl(category, method, targetEndpoint) + id);

	if (pageable != null) {
	    builder.queryParam("page", pageable.getPageNumber()).queryParam("size", pageable.getPageSize());

	    if (pageable.getSort().isSorted()) {
		Iterator<Sort.Order> iterator = pageable.getSort().iterator();
		while (iterator.hasNext()) {
		    Sort.Order sort = iterator.next();
		    String sortValue = sort.getProperty();
		    if (sort.isDescending())
			sortValue += ",desc";
		    else
			sortValue += ",asc";

//			System.out.println(sort.toString());
//			System.out.println(sort.getProperty());
//			System.out.println(sort.isDescending());

		    builder.queryParam("sort", sortValue);
		}
	    }
	}

	UriComponents uriComponents = builder.build();

	return uriComponents.toUriString();
    }


}
