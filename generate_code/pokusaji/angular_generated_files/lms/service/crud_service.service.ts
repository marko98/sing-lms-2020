
export abstract class CrudService<T extends Entitet, ID extends Serializable> {

    protected repository: CrudRepository;
        


    public getRepository = (): CrudRepository => {
        return this.repository;
    }

    public setRepository = (repository: CrudRepository): void => {
        this.repository = repository;
    }
    

    public findAll (pageable: Pageable): Page<T> {
	return this.repository.findAll(pageable);
    }

    public findAll (): Iterable<T> {
	return this.repository.findAll();
    }

    public findOne (id: ID): Optional<T> {
	return this.repository.findOne(id);
    }

    public save (model: T): void {
	this.repository.save(model);
    }

    public delete (id: ID): void {
	this.repository.delete(id);
    }

    public delete (model: T): void {
	this.repository.delete(model);
    }


}
