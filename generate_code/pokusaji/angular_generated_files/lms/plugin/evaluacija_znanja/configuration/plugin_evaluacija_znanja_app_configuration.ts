
export class PluginEvaluacijaZnanjaAppConfiguration {




    public propertySourcesPlaceholderConfigurer (): PropertySourcesPlaceholderConfigurer {
	PropertySourcesPlaceholderConfigurer properties = new PropertySourcesPlaceholderConfigurer();
	properties.setLocation(new ClassPathResource("/lms/plugin/evaluacija_znanja/configuration/plugin.evaluacija_znanja.application.properties"));
	properties.setIgnoreResourceNotFound(false);
	return properties;
    }


}
