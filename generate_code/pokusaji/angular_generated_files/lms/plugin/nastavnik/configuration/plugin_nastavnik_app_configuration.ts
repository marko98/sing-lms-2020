
export class PluginNastavnikAppConfiguration {




    public propertySourcesPlaceholderConfigurer (): PropertySourcesPlaceholderConfigurer {
	PropertySourcesPlaceholderConfigurer properties = new PropertySourcesPlaceholderConfigurer();
	properties.setLocation(new ClassPathResource("/lms/plugin/nastavnik/configuration/plugin.nastavnik.application.properties"));
	properties.setIgnoreResourceNotFound(false);
	return properties;
    }


}
