
export class PluginAdministracijaAppConfiguration {




    public propertySourcesPlaceholderConfigurer (): PropertySourcesPlaceholderConfigurer {
	PropertySourcesPlaceholderConfigurer properties = new PropertySourcesPlaceholderConfigurer();
	properties.setLocation(new ClassPathResource("/lms/plugin/administracija/configuration/plugin.administracija.application.properties"));
	properties.setIgnoreResourceNotFound(false);
	return properties;
    }


}
