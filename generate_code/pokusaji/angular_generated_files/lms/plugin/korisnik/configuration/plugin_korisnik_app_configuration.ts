
export class PluginKorisnikAppConfiguration {




    public propertySourcesPlaceholderConfigurer (): PropertySourcesPlaceholderConfigurer {
	PropertySourcesPlaceholderConfigurer properties = new PropertySourcesPlaceholderConfigurer();
	properties.setLocation(new ClassPathResource("/lms/plugin/korisnik/configuration/plugin.korisnik.application.properties"));
	properties.setIgnoreResourceNotFound(false);
	return properties;
    }


}
