
export class PluginAdresaAppConfiguration {




    public propertySourcesPlaceholderConfigurer (): PropertySourcesPlaceholderConfigurer {
	PropertySourcesPlaceholderConfigurer properties = new PropertySourcesPlaceholderConfigurer();
	properties.setLocation(new ClassPathResource("/lms/plugin/adresa/configuration/plugin.adresa.application.properties"));
	properties.setIgnoreResourceNotFound(false);
	return properties;
    }


}
