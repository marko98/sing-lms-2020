
export class PluginPredmetAppConfiguration {




    public propertySourcesPlaceholderConfigurer (): PropertySourcesPlaceholderConfigurer {
	PropertySourcesPlaceholderConfigurer properties = new PropertySourcesPlaceholderConfigurer();
	properties.setLocation(new ClassPathResource("/lms/plugin/predmet/configuration/plugin.predmet.application.properties"));
	properties.setIgnoreResourceNotFound(false);
	return properties;
    }


}
