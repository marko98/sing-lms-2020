
export class PluginObavestenjeAppConfiguration {




    public propertySourcesPlaceholderConfigurer (): PropertySourcesPlaceholderConfigurer {
	PropertySourcesPlaceholderConfigurer properties = new PropertySourcesPlaceholderConfigurer();
	properties.setLocation(new ClassPathResource("/lms/plugin/obavestenje/configuration/plugin.obavestenje.application.properties"));
	properties.setIgnoreResourceNotFound(false);
	return properties;
    }


}
