
export class PluginStudentAppConfiguration {




    public propertySourcesPlaceholderConfigurer (): PropertySourcesPlaceholderConfigurer {
	PropertySourcesPlaceholderConfigurer properties = new PropertySourcesPlaceholderConfigurer();
	properties.setLocation(new ClassPathResource("/lms/plugin/student/configuration/plugin.student.application.properties"));
	properties.setIgnoreResourceNotFound(false);
	return properties;
    }


}
