
export class PluginStudentApp extends SpringBootServletInitializer {




    public static main (args[]: String): void {
	RestTemplate rt = new RestTemplate();
	
//	kreiramo opis plugin-a
	PluginDescription pluginDescription = new PluginDescription("Student", "Plugin za logicku celinu student", "http", "localhost:8110", new HashMap<String, HashMap<HttpMethod, HashMap<String, String>>>());

//	dodajemo kategorije koje plugin pruza
	ArrayList<String> categories = new ArrayList<String>();
	categories.add("istrazivacki_rad");
	categories.add("diplomski_rad");
	categories.add("istrazivacki_rad_student_na_studiji");
	categories.add("student_na_studiji");
	categories.add("student");
	categories.add("pohadjanje_predmeta");
	pluginDescription.setCategories(categories);
	
	for (String category : categories) {
//		dodajemo spisak endpoint-a u opis plugin-a
	    pluginDescription.getEndpoints().put(category, new HashMap<HttpMethod, HashMap<String, String>>());
	    pluginDescription.getEndpoints().get(category).put(HttpMethod.GET, new HashMap<String, String>());
	    pluginDescription.getEndpoints().get(category).get(HttpMethod.GET).putIfAbsent("findAll",
		    "/api/" + category);
	    pluginDescription.getEndpoints().get(category).get(HttpMethod.GET).putIfAbsent("findOne",
		    "/api/" + category + "/");

	    pluginDescription.getEndpoints().get(category).put(HttpMethod.POST, new HashMap<String, String>());
	    pluginDescription.getEndpoints().get(category).get(HttpMethod.POST).putIfAbsent("create",
		    "/api/" + category);

	    pluginDescription.getEndpoints().get(category).put(HttpMethod.PUT, new HashMap<String, String>());
	    pluginDescription.getEndpoints().get(category).get(HttpMethod.PUT).putIfAbsent("update",
		    "/api/" + category);

	    pluginDescription.getEndpoints().get(category).put(HttpMethod.DELETE, new HashMap<String, String>());
	    pluginDescription.getEndpoints().get(category).get(HttpMethod.DELETE).putIfAbsent("delete",
		    "/api/" + category + "/");
	}
	
//	registracija plugin-a na host-u
	rt.postForLocation("http://localhost:8080/api/plugins", pluginDescription);
	
//	pokretanje plugin-a
	SpringApplication.run(PluginStudentApp.class, args);
    }

    protected configure (builder: SpringApplicationBuilder): SpringApplicationBuilder {
		return builder.sources(PluginStudentApp.class);
    }


}
