
export class PluginNastavaAppConfiguration {




    public propertySourcesPlaceholderConfigurer (): PropertySourcesPlaceholderConfigurer {
	PropertySourcesPlaceholderConfigurer properties = new PropertySourcesPlaceholderConfigurer();
	properties.setLocation(new ClassPathResource("/lms/plugin/nastava/configuration/plugin.nastava.application.properties"));
	properties.setIgnoreResourceNotFound(false);
	return properties;
    }


}
