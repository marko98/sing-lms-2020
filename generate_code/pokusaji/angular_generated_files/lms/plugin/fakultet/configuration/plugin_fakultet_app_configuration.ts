
export class PluginFakultetAppConfiguration {




    public propertySourcesPlaceholderConfigurer (): PropertySourcesPlaceholderConfigurer {
	PropertySourcesPlaceholderConfigurer properties = new PropertySourcesPlaceholderConfigurer();
	properties.setLocation(new ClassPathResource("/lms/plugin/fakultet/configuration/plugin.fakultet.application.properties"));
	properties.setIgnoreResourceNotFound(false);
	return properties;
    }


}
