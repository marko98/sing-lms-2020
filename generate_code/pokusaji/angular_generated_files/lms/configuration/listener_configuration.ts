
export class ListenerConfiguration {

    private connectionFactory: ConnectionFactory;
        


    public getConnectionFactory = (): ConnectionFactory => {
        return this.connectionFactory;
    }

    public setConnectionFactory = (connectionFactory: ConnectionFactory): void => {
        this.connectionFactory = connectionFactory;
    }
    

    public jmsQueueListenerContainerFactory (configurer: DefaultJmsListenerContainerFactoryConfigurer, connectionFactory: ConnectionFactory): DefaultJmsListenerContainerFactory {
    /*
     * Definisanje bean-a za queue listener fabriku.
     */
	DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
	configurer.configure(factory, connectionFactory);

	/**
	 * po meni factory.setPubSubDomain(false); ce uciniti da ova fabrika proizvede
	 * JmsTemplate za queue
	 */
	factory.setPubSubDomain(false);
	return factory;
    }

    public jmsTopicListenerContainerFactory (configurer: DefaultJmsListenerContainerFactoryConfigurer, connectionFactory: ConnectionFactory): DefaultJmsListenerContainerFactory {
    /*
     * Definisanje bean-a za topic listener fabriku.
     */
	DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
	configurer.configure(factory, connectionFactory);

	/**
	 * po meni factory.setPubSubDomain(true); ce uciniti da ova fabrika proizvede
	 * JmsTemplate za topic
	 * 
	 * inace imamo i default-ni JmsListener za queue (koji nam nudi Spring Boot)
	 * kojem nismo u application.properties dodali spring.jms.pub-sub-domain=true,
	 * jer bi nam onda taj default-ni bio za topic (malo je ovo sada cudno jer je
	 * Ivan dole definisao bean i za default-ni JmsListener)
	 */
	factory.setPubSubDomain(true);
	return factory;
    }

    public jmsTemplate (): JmsTemplate {
    /*
     * Definisanje jms template bean-a za queue poruke. (Default-ni JmsTemplate)
     */
	JmsTemplate template = new JmsTemplate(connectionFactory);
	/**
	 * setujemo template.setPubSubDomain(false); na false, za svaki slucaj, iako je
	 * fabrika(connectionFactory) podesena da proizvodi JmsTemplate za queue
	 */
	template.setPubSubDomain(false);
	return template;
    }

    public jmsTopicTemplate (): JmsTemplate {
    /*
     * Definisanje jms template bean-a za topic poruke.
     */
	JmsTemplate template = new JmsTemplate(connectionFactory);
	/**
	 * setujemo template.setPubSubDomain(true); na true, jer je fabrika(connectionFactory) podesena
	 * da proizvodi JmsTemplate za queue
	 */
	template.setPubSubDomain(true);
	return template;
    }


}
