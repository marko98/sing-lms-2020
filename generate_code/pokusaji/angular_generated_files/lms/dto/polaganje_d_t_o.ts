
export class PolaganjeDTO implements DTO {

    private id: number;
        
    private bodovi: number;
        
    private napomena: string;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private pohadjanjePredmetaDTO: PohadjanjePredmetaDTO;
        
    private evaluacijaZnanjaDTO: EvaluacijaZnanjaDTO;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getBodovi = (): number => {
        return this.bodovi;
    }

    public setBodovi = (bodovi: number): void => {
        this.bodovi = bodovi;
    }
    
    public getNapomena = (): string => {
        return this.napomena;
    }

    public setNapomena = (napomena: string): void => {
        this.napomena = napomena;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getPohadjanjePredmetaDTO = (): PohadjanjePredmetaDTO => {
        return this.pohadjanjePredmetaDTO;
    }

    public setPohadjanjePredmetaDTO = (pohadjanjePredmetaDTO: PohadjanjePredmetaDTO): void => {
        this.pohadjanjePredmetaDTO = pohadjanjePredmetaDTO;
    }
    
    public getEvaluacijaZnanjaDTO = (): EvaluacijaZnanjaDTO => {
        return this.evaluacijaZnanjaDTO;
    }

    public setEvaluacijaZnanjaDTO = (evaluacijaZnanjaDTO: EvaluacijaZnanjaDTO): void => {
        this.evaluacijaZnanjaDTO = evaluacijaZnanjaDTO;
    }
    

    public constructor () {
    }


}
