
export class EvaluacijaZnanjaNacinEvaluacijeDTO implements DTO {

    private id: number;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private nacinEvaluacije: NacinEvaluacije;
        
    private evaluacijaZnanjaDTO: EvaluacijaZnanjaDTO;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getNacinEvaluacije = (): NacinEvaluacije => {
        return this.nacinEvaluacije;
    }

    public setNacinEvaluacije = (nacinEvaluacije: NacinEvaluacije): void => {
        this.nacinEvaluacije = nacinEvaluacije;
    }
    
    public getEvaluacijaZnanjaDTO = (): EvaluacijaZnanjaDTO => {
        return this.evaluacijaZnanjaDTO;
    }

    public setEvaluacijaZnanjaDTO = (evaluacijaZnanjaDTO: EvaluacijaZnanjaDTO): void => {
        this.evaluacijaZnanjaDTO = evaluacijaZnanjaDTO;
    }
    

    public constructor () {
    }


}
