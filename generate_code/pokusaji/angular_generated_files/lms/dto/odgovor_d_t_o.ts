
export class OdgovorDTO implements DTO {

    private id: number;
        
    private sadrzaj: string;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private pitanjeDTO: PitanjeDTO;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getSadrzaj = (): string => {
        return this.sadrzaj;
    }

    public setSadrzaj = (sadrzaj: string): void => {
        this.sadrzaj = sadrzaj;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getPitanjeDTO = (): PitanjeDTO => {
        return this.pitanjeDTO;
    }

    public setPitanjeDTO = (pitanjeDTO: PitanjeDTO): void => {
        this.pitanjeDTO = pitanjeDTO;
    }
    

    public constructor () {
    }


}
