
export class StudentNaStudijiDTO implements DTO {

    private id: number;
        
    private datumUpisa: Date;
        
    private brojIndeksa: string;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private studentDTO: StudentDTO;
        
    private pohadjanjaPredmetaDTO: PohadjanjePredmetaDTO[];
        
    private godinaStudijaDTO: GodinaStudijaDTO;
        
    private istrazivackiRadoviStudentNaStudijiDTO: IstrazivackiRadStudentNaStudijiDTO[];
        
    private diplomskiRadDTO: DiplomskiRadDTO;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getDatumUpisa = (): Date => {
        return this.datumUpisa;
    }

    public setDatumUpisa = (datumUpisa: Date): void => {
        this.datumUpisa = datumUpisa;
    }
    
    public getBrojIndeksa = (): string => {
        return this.brojIndeksa;
    }

    public setBrojIndeksa = (brojIndeksa: string): void => {
        this.brojIndeksa = brojIndeksa;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getStudentDTO = (): StudentDTO => {
        return this.studentDTO;
    }

    public setStudentDTO = (studentDTO: StudentDTO): void => {
        this.studentDTO = studentDTO;
    }
    
    public getPohadjanjaPredmetaDTO = (): PohadjanjePredmetaDTO[] => {
        return this.pohadjanjaPredmetaDTO;
    }

    public setPohadjanjaPredmetaDTO = (pohadjanjaPredmetaDTO: PohadjanjePredmetaDTO[]): void => {
        this.pohadjanjaPredmetaDTO = pohadjanjaPredmetaDTO;
    }
    
    public getGodinaStudijaDTO = (): GodinaStudijaDTO => {
        return this.godinaStudijaDTO;
    }

    public setGodinaStudijaDTO = (godinaStudijaDTO: GodinaStudijaDTO): void => {
        this.godinaStudijaDTO = godinaStudijaDTO;
    }
    
    public getIstrazivackiRadoviStudentNaStudijiDTO = (): IstrazivackiRadStudentNaStudijiDTO[] => {
        return this.istrazivackiRadoviStudentNaStudijiDTO;
    }

    public setIstrazivackiRadoviStudentNaStudijiDTO = (istrazivackiRadoviStudentNaStudijiDTO: IstrazivackiRadStudentNaStudijiDTO[]): void => {
        this.istrazivackiRadoviStudentNaStudijiDTO = istrazivackiRadoviStudentNaStudijiDTO;
    }
    
    public getDiplomskiRadDTO = (): DiplomskiRadDTO => {
        return this.diplomskiRadDTO;
    }

    public setDiplomskiRadDTO = (diplomskiRadDTO: DiplomskiRadDTO): void => {
        this.diplomskiRadDTO = diplomskiRadDTO;
    }
    

    public constructor () {
        this.pohadjanjaPredmetaDTO = [];
        this.istrazivackiRadoviStudentNaStudijiDTO = [];
    }

    public getPohadjanjePredmetaDTO (identifikacija: Identifikacija): PohadjanjePredmetaDTO {
	throw new Error("Not Implmented");
    }

    public getIstrazivackiRadStudentNaStudijiDTO (identifikacija: Identifikacija): IstrazivackiRadStudentNaStudijiDTO {
	throw new Error("Not Implmented");
    }


}
