
export class KontaktDTO implements DTO {

    private id: number;
        
    private mobilni: string;
        
    private fiksni: string;
        
    private version: number;
        
    private stanje: StanjeModela;
        
    private fakultetDTO: FakultetDTO;
        
    private univerzitetDTO: UniverzitetDTO;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getMobilni = (): string => {
        return this.mobilni;
    }

    public setMobilni = (mobilni: string): void => {
        this.mobilni = mobilni;
    }
    
    public getFiksni = (): string => {
        return this.fiksni;
    }

    public setFiksni = (fiksni: string): void => {
        this.fiksni = fiksni;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getFakultetDTO = (): FakultetDTO => {
        return this.fakultetDTO;
    }

    public setFakultetDTO = (fakultetDTO: FakultetDTO): void => {
        this.fakultetDTO = fakultetDTO;
    }
    
    public getUniverzitetDTO = (): UniverzitetDTO => {
        return this.univerzitetDTO;
    }

    public setUniverzitetDTO = (univerzitetDTO: UniverzitetDTO): void => {
        this.univerzitetDTO = univerzitetDTO;
    }
    

    public constructor () {
    }


}
