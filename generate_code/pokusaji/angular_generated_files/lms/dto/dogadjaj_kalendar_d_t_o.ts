
export class DogadjajKalendarDTO implements DTO {

    private id: number;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private kalendarDTO: KalendarDTO;
        
    private dogadjajDTO: DogadjajDTO;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getKalendarDTO = (): KalendarDTO => {
        return this.kalendarDTO;
    }

    public setKalendarDTO = (kalendarDTO: KalendarDTO): void => {
        this.kalendarDTO = kalendarDTO;
    }
    
    public getDogadjajDTO = (): DogadjajDTO => {
        return this.dogadjajDTO;
    }

    public setDogadjajDTO = (dogadjajDTO: DogadjajDTO): void => {
        this.dogadjajDTO = dogadjajDTO;
    }
    

    public constructor () {
    }


}
