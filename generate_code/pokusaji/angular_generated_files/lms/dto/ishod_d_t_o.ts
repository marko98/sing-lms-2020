
export class IshodDTO implements DTO {

    private id: number;
        
    private opis: string;
        
    private naslov: string;
        
    private putanjaZaSliku: string;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private realizacijaPredmetaDTO: RealizacijaPredmetaDTO;
        
    private evaluacijaZnanjaDTO: EvaluacijaZnanjaDTO;
        
    private nastavniMaterijaliDTO: NastavniMaterijalDTO[];
        
    private obrazovniCiljeviDTO: ObrazovniCiljDTO[];
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getOpis = (): string => {
        return this.opis;
    }

    public setOpis = (opis: string): void => {
        this.opis = opis;
    }
    
    public getNaslov = (): string => {
        return this.naslov;
    }

    public setNaslov = (naslov: string): void => {
        this.naslov = naslov;
    }
    
    public getPutanjaZaSliku = (): string => {
        return this.putanjaZaSliku;
    }

    public setPutanjaZaSliku = (putanjaZaSliku: string): void => {
        this.putanjaZaSliku = putanjaZaSliku;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getRealizacijaPredmetaDTO = (): RealizacijaPredmetaDTO => {
        return this.realizacijaPredmetaDTO;
    }

    public setRealizacijaPredmetaDTO = (realizacijaPredmetaDTO: RealizacijaPredmetaDTO): void => {
        this.realizacijaPredmetaDTO = realizacijaPredmetaDTO;
    }
    
    public getEvaluacijaZnanjaDTO = (): EvaluacijaZnanjaDTO => {
        return this.evaluacijaZnanjaDTO;
    }

    public setEvaluacijaZnanjaDTO = (evaluacijaZnanjaDTO: EvaluacijaZnanjaDTO): void => {
        this.evaluacijaZnanjaDTO = evaluacijaZnanjaDTO;
    }
    
    public getNastavniMaterijaliDTO = (): NastavniMaterijalDTO[] => {
        return this.nastavniMaterijaliDTO;
    }

    public setNastavniMaterijaliDTO = (nastavniMaterijaliDTO: NastavniMaterijalDTO[]): void => {
        this.nastavniMaterijaliDTO = nastavniMaterijaliDTO;
    }
    
    public getObrazovniCiljeviDTO = (): ObrazovniCiljDTO[] => {
        return this.obrazovniCiljeviDTO;
    }

    public setObrazovniCiljeviDTO = (obrazovniCiljeviDTO: ObrazovniCiljDTO[]): void => {
        this.obrazovniCiljeviDTO = obrazovniCiljeviDTO;
    }
    

    public constructor () {
        this.nastavniMaterijaliDTO = [];
        this.obrazovniCiljeviDTO = [];
    }

    public getObrazovniCiljDTO (identifikacija: Identifikacija): ObrazovniCiljDTO {
	throw new Error("Not Implmented");
    }

    public getNastavniMaterijalDTO (identifikacija: Identifikacija): NastavniMaterijalDTO {
	throw new Error("Not Implmented");
    }


}
