
export class AdresaDTO implements DTO {

    private id: number;
        
    private ulica: string;
        
    private broj: string;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private tip: TipAdrese;
        
    private gradDTO: GradDTO;
        
    private odeljenjeDTO: OdeljenjeDTO;
        
    private univerzitetDTO: UniverzitetDTO;
        
    private registrovaniKorisnikDTO: RegistrovaniKorisnikDTO;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getUlica = (): string => {
        return this.ulica;
    }

    public setUlica = (ulica: string): void => {
        this.ulica = ulica;
    }
    
    public getBroj = (): string => {
        return this.broj;
    }

    public setBroj = (broj: string): void => {
        this.broj = broj;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getTip = (): TipAdrese => {
        return this.tip;
    }

    public setTip = (tip: TipAdrese): void => {
        this.tip = tip;
    }
    
    public getGradDTO = (): GradDTO => {
        return this.gradDTO;
    }

    public setGradDTO = (gradDTO: GradDTO): void => {
        this.gradDTO = gradDTO;
    }
    
    public getOdeljenjeDTO = (): OdeljenjeDTO => {
        return this.odeljenjeDTO;
    }

    public setOdeljenjeDTO = (odeljenjeDTO: OdeljenjeDTO): void => {
        this.odeljenjeDTO = odeljenjeDTO;
    }
    
    public getUniverzitetDTO = (): UniverzitetDTO => {
        return this.univerzitetDTO;
    }

    public setUniverzitetDTO = (univerzitetDTO: UniverzitetDTO): void => {
        this.univerzitetDTO = univerzitetDTO;
    }
    
    public getRegistrovaniKorisnikDTO = (): RegistrovaniKorisnikDTO => {
        return this.registrovaniKorisnikDTO;
    }

    public setRegistrovaniKorisnikDTO = (registrovaniKorisnikDTO: RegistrovaniKorisnikDTO): void => {
        this.registrovaniKorisnikDTO = registrovaniKorisnikDTO;
    }
    

    public constructor () {
    }


}
