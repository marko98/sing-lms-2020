
export class DogadjajDTO implements DTO {

    private id: number;
        
    private pocetniDatum: Date;
        
    private krajnjiDatum: Date;
        
    private opis: string;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private dogadjajKalendariDTO: DogadjajKalendarDTO[];
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getPocetniDatum = (): Date => {
        return this.pocetniDatum;
    }

    public setPocetniDatum = (pocetniDatum: Date): void => {
        this.pocetniDatum = pocetniDatum;
    }
    
    public getKrajnjiDatum = (): Date => {
        return this.krajnjiDatum;
    }

    public setKrajnjiDatum = (krajnjiDatum: Date): void => {
        this.krajnjiDatum = krajnjiDatum;
    }
    
    public getOpis = (): string => {
        return this.opis;
    }

    public setOpis = (opis: string): void => {
        this.opis = opis;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getDogadjajKalendariDTO = (): DogadjajKalendarDTO[] => {
        return this.dogadjajKalendariDTO;
    }

    public setDogadjajKalendariDTO = (dogadjajKalendariDTO: DogadjajKalendarDTO[]): void => {
        this.dogadjajKalendariDTO = dogadjajKalendariDTO;
    }
    

    public constructor () {
        this.dogadjajKalendariDTO = [];
    }

    public getDogadjajKalendarDTO (identifikacija: Identifikacija): DogadjajKalendarDTO {
	throw new Error("Not Implmented");
    }


}
