
export class ObrazovniCiljDTO implements DTO {

    private id: number;
        
    private opis: string;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private realizacijaPredmetaDTO: RealizacijaPredmetaDTO;
        
    private ishodDTO: IshodDTO;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getOpis = (): string => {
        return this.opis;
    }

    public setOpis = (opis: string): void => {
        this.opis = opis;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getRealizacijaPredmetaDTO = (): RealizacijaPredmetaDTO => {
        return this.realizacijaPredmetaDTO;
    }

    public setRealizacijaPredmetaDTO = (realizacijaPredmetaDTO: RealizacijaPredmetaDTO): void => {
        this.realizacijaPredmetaDTO = realizacijaPredmetaDTO;
    }
    
    public getIshodDTO = (): IshodDTO => {
        return this.ishodDTO;
    }

    public setIshodDTO = (ishodDTO: IshodDTO): void => {
        this.ishodDTO = ishodDTO;
    }
    

    public constructor () {
    }


}
