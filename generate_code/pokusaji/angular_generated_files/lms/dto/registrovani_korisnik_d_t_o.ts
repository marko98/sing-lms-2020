
export class RegistrovaniKorisnikDTO implements DTO {

    private id: number;
        
    private korisnickoIme: string;
        
    private lozinka: string;
        
    private email: string;
        
    private datumRodjenja: Date;
        
    private jmbg: string;
        
    private ime: string;
        
    private prezime: string;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private adreseDTO: AdresaDTO[];
        
    private nastavnikDTO: NastavnikDTO;
        
    private studentskiSluzbenikDTO: StudentskiSluzbenikDTO;
        
    private administratorDTO: AdministratorDTO;
        
    private studentDTO: StudentDTO;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getKorisnickoIme = (): string => {
        return this.korisnickoIme;
    }

    public setKorisnickoIme = (korisnickoIme: string): void => {
        this.korisnickoIme = korisnickoIme;
    }
    
    public getLozinka = (): string => {
        return this.lozinka;
    }

    public setLozinka = (lozinka: string): void => {
        this.lozinka = lozinka;
    }
    
    public getEmail = (): string => {
        return this.email;
    }

    public setEmail = (email: string): void => {
        this.email = email;
    }
    
    public getDatumRodjenja = (): Date => {
        return this.datumRodjenja;
    }

    public setDatumRodjenja = (datumRodjenja: Date): void => {
        this.datumRodjenja = datumRodjenja;
    }
    
    public getJmbg = (): string => {
        return this.jmbg;
    }

    public setJmbg = (jmbg: string): void => {
        this.jmbg = jmbg;
    }
    
    public getIme = (): string => {
        return this.ime;
    }

    public setIme = (ime: string): void => {
        this.ime = ime;
    }
    
    public getPrezime = (): string => {
        return this.prezime;
    }

    public setPrezime = (prezime: string): void => {
        this.prezime = prezime;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getAdreseDTO = (): AdresaDTO[] => {
        return this.adreseDTO;
    }

    public setAdreseDTO = (adreseDTO: AdresaDTO[]): void => {
        this.adreseDTO = adreseDTO;
    }
    
    public getNastavnikDTO = (): NastavnikDTO => {
        return this.nastavnikDTO;
    }

    public setNastavnikDTO = (nastavnikDTO: NastavnikDTO): void => {
        this.nastavnikDTO = nastavnikDTO;
    }
    
    public getStudentskiSluzbenikDTO = (): StudentskiSluzbenikDTO => {
        return this.studentskiSluzbenikDTO;
    }

    public setStudentskiSluzbenikDTO = (studentskiSluzbenikDTO: StudentskiSluzbenikDTO): void => {
        this.studentskiSluzbenikDTO = studentskiSluzbenikDTO;
    }
    
    public getAdministratorDTO = (): AdministratorDTO => {
        return this.administratorDTO;
    }

    public setAdministratorDTO = (administratorDTO: AdministratorDTO): void => {
        this.administratorDTO = administratorDTO;
    }
    
    public getStudentDTO = (): StudentDTO => {
        return this.studentDTO;
    }

    public setStudentDTO = (studentDTO: StudentDTO): void => {
        this.studentDTO = studentDTO;
    }
    

    public constructor () {
        this.adreseDTO = [];
    }


}
