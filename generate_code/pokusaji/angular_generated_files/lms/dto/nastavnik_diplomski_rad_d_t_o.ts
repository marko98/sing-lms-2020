
export class NastavnikDiplomskiRadDTO implements DTO {

    private id: number;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private nastavnikUKomisijiDTO: NastavnikDTO;
        
    private diplomskiRadDTO: DiplomskiRadDTO;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getNastavnikUKomisijiDTO = (): NastavnikDTO => {
        return this.nastavnikUKomisijiDTO;
    }

    public setNastavnikUKomisijiDTO = (nastavnikUKomisijiDTO: NastavnikDTO): void => {
        this.nastavnikUKomisijiDTO = nastavnikUKomisijiDTO;
    }
    
    public getDiplomskiRadDTO = (): DiplomskiRadDTO => {
        return this.diplomskiRadDTO;
    }

    public setDiplomskiRadDTO = (diplomskiRadDTO: DiplomskiRadDTO): void => {
        this.diplomskiRadDTO = diplomskiRadDTO;
    }
    

    public constructor () {
    }


}
