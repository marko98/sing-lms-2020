
export class GodinaStudijaObavestenjeDTO implements DTO {

    private id: number;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private godinaStudijaDTO: GodinaStudijaDTO;
        
    private obavestenjeDTO: ObavestenjeDTO;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getGodinaStudijaDTO = (): GodinaStudijaDTO => {
        return this.godinaStudijaDTO;
    }

    public setGodinaStudijaDTO = (godinaStudijaDTO: GodinaStudijaDTO): void => {
        this.godinaStudijaDTO = godinaStudijaDTO;
    }
    
    public getObavestenjeDTO = (): ObavestenjeDTO => {
        return this.obavestenjeDTO;
    }

    public setObavestenjeDTO = (obavestenjeDTO: ObavestenjeDTO): void => {
        this.obavestenjeDTO = obavestenjeDTO;
    }
    

    public constructor () {
    }


}
