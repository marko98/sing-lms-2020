
export class PitanjeDTO implements DTO {

    private id: number;
        
    private oblast: string;
        
    private pitanje: string;
        
    private putanjaZaSliku: string;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private evaluacijaZnanjaDTO: EvaluacijaZnanjaDTO;
        
    private odgovoriDTO: OdgovorDTO[];
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getOblast = (): string => {
        return this.oblast;
    }

    public setOblast = (oblast: string): void => {
        this.oblast = oblast;
    }
    
    public getPitanje = (): string => {
        return this.pitanje;
    }

    public setPitanje = (pitanje: string): void => {
        this.pitanje = pitanje;
    }
    
    public getPutanjaZaSliku = (): string => {
        return this.putanjaZaSliku;
    }

    public setPutanjaZaSliku = (putanjaZaSliku: string): void => {
        this.putanjaZaSliku = putanjaZaSliku;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getEvaluacijaZnanjaDTO = (): EvaluacijaZnanjaDTO => {
        return this.evaluacijaZnanjaDTO;
    }

    public setEvaluacijaZnanjaDTO = (evaluacijaZnanjaDTO: EvaluacijaZnanjaDTO): void => {
        this.evaluacijaZnanjaDTO = evaluacijaZnanjaDTO;
    }
    
    public getOdgovoriDTO = (): OdgovorDTO[] => {
        return this.odgovoriDTO;
    }

    public setOdgovoriDTO = (odgovoriDTO: OdgovorDTO[]): void => {
        this.odgovoriDTO = odgovoriDTO;
    }
    

    public constructor () {
        this.odgovoriDTO = [];
    }

    public getOdgovorDTO (identifikacija: Identifikacija): OdgovorDTO {
	throw new Error("Not Implmented");
    }


}
