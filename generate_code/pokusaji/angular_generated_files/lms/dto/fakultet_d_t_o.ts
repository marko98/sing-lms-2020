
export class FakultetDTO implements DTO {

    private id: number;
        
    private naziv: string;
        
    private opis: string;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private odeljenjaDTO: OdeljenjeDTO[];
        
    private studijskiProgramiDTO: StudijskiProgramDTO[];
        
    private nastavniciFakultetDTO: NastavnikFakultetDTO[];
        
    private univerzitetDTO: UniverzitetDTO;
        
    private dekanDTO: NastavnikDTO;
        
    private kontaktiDTO: KontaktDTO[];
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getNaziv = (): string => {
        return this.naziv;
    }

    public setNaziv = (naziv: string): void => {
        this.naziv = naziv;
    }
    
    public getOpis = (): string => {
        return this.opis;
    }

    public setOpis = (opis: string): void => {
        this.opis = opis;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getOdeljenjaDTO = (): OdeljenjeDTO[] => {
        return this.odeljenjaDTO;
    }

    public setOdeljenjaDTO = (odeljenjaDTO: OdeljenjeDTO[]): void => {
        this.odeljenjaDTO = odeljenjaDTO;
    }
    
    public getStudijskiProgramiDTO = (): StudijskiProgramDTO[] => {
        return this.studijskiProgramiDTO;
    }

    public setStudijskiProgramiDTO = (studijskiProgramiDTO: StudijskiProgramDTO[]): void => {
        this.studijskiProgramiDTO = studijskiProgramiDTO;
    }
    
    public getNastavniciFakultetDTO = (): NastavnikFakultetDTO[] => {
        return this.nastavniciFakultetDTO;
    }

    public setNastavniciFakultetDTO = (nastavniciFakultetDTO: NastavnikFakultetDTO[]): void => {
        this.nastavniciFakultetDTO = nastavniciFakultetDTO;
    }
    
    public getUniverzitetDTO = (): UniverzitetDTO => {
        return this.univerzitetDTO;
    }

    public setUniverzitetDTO = (univerzitetDTO: UniverzitetDTO): void => {
        this.univerzitetDTO = univerzitetDTO;
    }
    
    public getDekanDTO = (): NastavnikDTO => {
        return this.dekanDTO;
    }

    public setDekanDTO = (dekanDTO: NastavnikDTO): void => {
        this.dekanDTO = dekanDTO;
    }
    
    public getKontaktiDTO = (): KontaktDTO[] => {
        return this.kontaktiDTO;
    }

    public setKontaktiDTO = (kontaktiDTO: KontaktDTO[]): void => {
        this.kontaktiDTO = kontaktiDTO;
    }
    

    public constructor () {
        this.odeljenjaDTO = [];
        this.studijskiProgramiDTO = [];
        this.nastavniciFakultetDTO = [];
        this.kontaktiDTO = [];
    }

    public getOdeljenjeDTO (identifikacija: Identifikacija): OdeljenjeDTO {
	throw new Error("Not Implmented");
    }

    public getStudijskiProgramDTO (identifikacija: Identifikacija): StudijskiProgramDTO {
	throw new Error("Not Implmented");
    }

    public getNastavnikFakultetDTO (identifikacija: Identifikacija): NastavnikFakultetDTO {
	throw new Error("Not Implmented");
    }


}
