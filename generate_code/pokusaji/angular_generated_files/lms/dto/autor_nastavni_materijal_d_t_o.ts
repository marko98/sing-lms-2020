
export class AutorNastavniMaterijalDTO implements DTO {

    private id: number;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private autorDTO: AutorDTO;
        
    private nastavniMaterijalDTO: NastavniMaterijalDTO;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getAutorDTO = (): AutorDTO => {
        return this.autorDTO;
    }

    public setAutorDTO = (autorDTO: AutorDTO): void => {
        this.autorDTO = autorDTO;
    }
    
    public getNastavniMaterijalDTO = (): NastavniMaterijalDTO => {
        return this.nastavniMaterijalDTO;
    }

    public setNastavniMaterijalDTO = (nastavniMaterijalDTO: NastavniMaterijalDTO): void => {
        this.nastavniMaterijalDTO = nastavniMaterijalDTO;
    }
    

    public constructor () {
    }


}
