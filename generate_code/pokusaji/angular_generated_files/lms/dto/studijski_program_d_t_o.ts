
export class StudijskiProgramDTO implements DTO {

    private id: number;
        
    private naziv: string;
        
    private brojGodinaStudija: number;
        
    private opis: string;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private fakultetDTO: FakultetDTO;
        
    private rukovodilacDTO: NastavnikDTO;
        
    private godineStudijaDTO: GodinaStudijaDTO[];
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getNaziv = (): string => {
        return this.naziv;
    }

    public setNaziv = (naziv: string): void => {
        this.naziv = naziv;
    }
    
    public getBrojGodinaStudija = (): number => {
        return this.brojGodinaStudija;
    }

    public setBrojGodinaStudija = (brojGodinaStudija: number): void => {
        this.brojGodinaStudija = brojGodinaStudija;
    }
    
    public getOpis = (): string => {
        return this.opis;
    }

    public setOpis = (opis: string): void => {
        this.opis = opis;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getFakultetDTO = (): FakultetDTO => {
        return this.fakultetDTO;
    }

    public setFakultetDTO = (fakultetDTO: FakultetDTO): void => {
        this.fakultetDTO = fakultetDTO;
    }
    
    public getRukovodilacDTO = (): NastavnikDTO => {
        return this.rukovodilacDTO;
    }

    public setRukovodilacDTO = (rukovodilacDTO: NastavnikDTO): void => {
        this.rukovodilacDTO = rukovodilacDTO;
    }
    
    public getGodineStudijaDTO = (): GodinaStudijaDTO[] => {
        return this.godineStudijaDTO;
    }

    public setGodineStudijaDTO = (godineStudijaDTO: GodinaStudijaDTO[]): void => {
        this.godineStudijaDTO = godineStudijaDTO;
    }
    

    public constructor () {
        this.godineStudijaDTO = [];
    }

    public getGodinaStudijaDTO (identifikacija: Identifikacija): GodinaStudijaDTO {
	throw new Error("Not Implmented");
    }


}
