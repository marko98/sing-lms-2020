
export class NastavnikDTO implements DTO {

    private id: number;
        
    private biografija: string;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private terminiNastaveDTO: TerminNastaveDTO[];
        
    private studijskiProgramDTO: StudijskiProgramDTO[];
        
    private fakultetiDTO: FakultetDTO[];
        
    private nastavnikFakultetiDTO: NastavnikFakultetDTO[];
        
    private univerzitetiDTO: UniverzitetDTO[];
        
    private tituleDTO: TitulaDTO[];
        
    private mentorDiplomskiRadoviDTO: DiplomskiRadDTO[];
        
    private registrovaniKorisnikDTO: RegistrovaniKorisnikDTO;
        
    private istrazivackiRadoviDTO: IstrazivackiRadDTO[];
        
    private nastavnikEvaluacijeZnanjaDTO: NastavnikEvaluacijaZnanjaDTO[];
        
    private konsultacijeDTO: KonsultacijaDTO[];
        
    private nastavnikDiplomskiRadoviDTO: NastavnikDiplomskiRadDTO[];
        
    private nastavnikNaRealizacijamaDTO: NastavnikNaRealizacijiDTO[];
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getBiografija = (): string => {
        return this.biografija;
    }

    public setBiografija = (biografija: string): void => {
        this.biografija = biografija;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getTerminiNastaveDTO = (): TerminNastaveDTO[] => {
        return this.terminiNastaveDTO;
    }

    public setTerminiNastaveDTO = (terminiNastaveDTO: TerminNastaveDTO[]): void => {
        this.terminiNastaveDTO = terminiNastaveDTO;
    }
    
    public getStudijskiProgramDTO = (): StudijskiProgramDTO[] => {
        return this.studijskiProgramDTO;
    }

    public setStudijskiProgramDTO = (studijskiProgramDTO: StudijskiProgramDTO[]): void => {
        this.studijskiProgramDTO = studijskiProgramDTO;
    }
    
    public getFakultetiDTO = (): FakultetDTO[] => {
        return this.fakultetiDTO;
    }

    public setFakultetiDTO = (fakultetiDTO: FakultetDTO[]): void => {
        this.fakultetiDTO = fakultetiDTO;
    }
    
    public getNastavnikFakultetiDTO = (): NastavnikFakultetDTO[] => {
        return this.nastavnikFakultetiDTO;
    }

    public setNastavnikFakultetiDTO = (nastavnikFakultetiDTO: NastavnikFakultetDTO[]): void => {
        this.nastavnikFakultetiDTO = nastavnikFakultetiDTO;
    }
    
    public getUniverzitetiDTO = (): UniverzitetDTO[] => {
        return this.univerzitetiDTO;
    }

    public setUniverzitetiDTO = (univerzitetiDTO: UniverzitetDTO[]): void => {
        this.univerzitetiDTO = univerzitetiDTO;
    }
    
    public getTituleDTO = (): TitulaDTO[] => {
        return this.tituleDTO;
    }

    public setTituleDTO = (tituleDTO: TitulaDTO[]): void => {
        this.tituleDTO = tituleDTO;
    }
    
    public getMentorDiplomskiRadoviDTO = (): DiplomskiRadDTO[] => {
        return this.mentorDiplomskiRadoviDTO;
    }

    public setMentorDiplomskiRadoviDTO = (mentorDiplomskiRadoviDTO: DiplomskiRadDTO[]): void => {
        this.mentorDiplomskiRadoviDTO = mentorDiplomskiRadoviDTO;
    }
    
    public getRegistrovaniKorisnikDTO = (): RegistrovaniKorisnikDTO => {
        return this.registrovaniKorisnikDTO;
    }

    public setRegistrovaniKorisnikDTO = (registrovaniKorisnikDTO: RegistrovaniKorisnikDTO): void => {
        this.registrovaniKorisnikDTO = registrovaniKorisnikDTO;
    }
    
    public getIstrazivackiRadoviDTO = (): IstrazivackiRadDTO[] => {
        return this.istrazivackiRadoviDTO;
    }

    public setIstrazivackiRadoviDTO = (istrazivackiRadoviDTO: IstrazivackiRadDTO[]): void => {
        this.istrazivackiRadoviDTO = istrazivackiRadoviDTO;
    }
    
    public getNastavnikEvaluacijeZnanjaDTO = (): NastavnikEvaluacijaZnanjaDTO[] => {
        return this.nastavnikEvaluacijeZnanjaDTO;
    }

    public setNastavnikEvaluacijeZnanjaDTO = (nastavnikEvaluacijeZnanjaDTO: NastavnikEvaluacijaZnanjaDTO[]): void => {
        this.nastavnikEvaluacijeZnanjaDTO = nastavnikEvaluacijeZnanjaDTO;
    }
    
    public getKonsultacijeDTO = (): KonsultacijaDTO[] => {
        return this.konsultacijeDTO;
    }

    public setKonsultacijeDTO = (konsultacijeDTO: KonsultacijaDTO[]): void => {
        this.konsultacijeDTO = konsultacijeDTO;
    }
    
    public getNastavnikDiplomskiRadoviDTO = (): NastavnikDiplomskiRadDTO[] => {
        return this.nastavnikDiplomskiRadoviDTO;
    }

    public setNastavnikDiplomskiRadoviDTO = (nastavnikDiplomskiRadoviDTO: NastavnikDiplomskiRadDTO[]): void => {
        this.nastavnikDiplomskiRadoviDTO = nastavnikDiplomskiRadoviDTO;
    }
    
    public getNastavnikNaRealizacijamaDTO = (): NastavnikNaRealizacijiDTO[] => {
        return this.nastavnikNaRealizacijamaDTO;
    }

    public setNastavnikNaRealizacijamaDTO = (nastavnikNaRealizacijamaDTO: NastavnikNaRealizacijiDTO[]): void => {
        this.nastavnikNaRealizacijamaDTO = nastavnikNaRealizacijamaDTO;
    }
    

    public constructor () {
        this.terminiNastaveDTO = [];
        this.studijskiProgramDTO = [];
        this.fakultetiDTO = [];
        this.nastavnikFakultetiDTO = [];
        this.univerzitetiDTO = [];
        this.tituleDTO = [];
        this.mentorDiplomskiRadoviDTO = [];
        this.istrazivackiRadoviDTO = [];
        this.nastavnikEvaluacijeZnanjaDTO = [];
        this.konsultacijeDTO = [];
        this.nastavnikDiplomskiRadoviDTO = [];
        this.nastavnikNaRealizacijamaDTO = [];
    }

    public getTitulaDTO (identifikacija: Identifikacija): TitulaDTO {
	throw new Error("Not Implmented");
    }

    public getNastavnikFakultetDTO (identifikacija: Identifikacija): NastavnikFakultetDTO {
	throw new Error("Not Implmented");
    }

    public getMentorDiplomskiRadDTO (identifikacija: Identifikacija): DiplomskiRadDTO {
	throw new Error("Not Implmented");
    }

    public getIstrazivackiRadDTO (identifikacija: Identifikacija): IstrazivackiRadDTO {
	throw new Error("Not Implmented");
    }

    public getKonsultacijaDTO (identifikacija: Identifikacija): KonsultacijaDTO {
	throw new Error("Not Implmented");
    }

    public getNastavnikEvaluacijaZnanjaDTO (identifikacija: Identifikacija): NastavnikEvaluacijaZnanjaDTO {
	throw new Error("Not Implmented");
    }

    public getTerminNastaveDTO (identifikacija: Identifikacija): TerminNastaveDTO {
	throw new Error("Not Implmented");
    }

    public getNastavnikDiplomskiRadDTO (identifikacija: Identifikacija): NastavnikDiplomskiRadDTO {
	throw new Error("Not Implmented");
    }

    public getNastavnikNaRealizacijiDTO (identifikacija: Identifikacija): NastavnikNaRealizacijiDTO {
	throw new Error("Not Implmented");
    }


}
