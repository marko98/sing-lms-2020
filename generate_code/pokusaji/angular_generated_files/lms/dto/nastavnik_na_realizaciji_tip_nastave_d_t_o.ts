
export class NastavnikNaRealizacijiTipNastaveDTO implements DTO {

    private id: number;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private tipNastave: TipNastave;
        
    private nastavnikNaRealizacijiDTO: NastavnikNaRealizacijiDTO;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getTipNastave = (): TipNastave => {
        return this.tipNastave;
    }

    public setTipNastave = (tipNastave: TipNastave): void => {
        this.tipNastave = tipNastave;
    }
    
    public getNastavnikNaRealizacijiDTO = (): NastavnikNaRealizacijiDTO => {
        return this.nastavnikNaRealizacijiDTO;
    }

    public setNastavnikNaRealizacijiDTO = (nastavnikNaRealizacijiDTO: NastavnikNaRealizacijiDTO): void => {
        this.nastavnikNaRealizacijiDTO = nastavnikNaRealizacijiDTO;
    }
    

    public constructor () {
    }


}
