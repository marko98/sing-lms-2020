
export class AutorDTO implements DTO {

    private id: number;
        
    private ime: string;
        
    private prezime: string;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private autorNastavniMaterijaliDTO: AutorNastavniMaterijalDTO[];
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getIme = (): string => {
        return this.ime;
    }

    public setIme = (ime: string): void => {
        this.ime = ime;
    }
    
    public getPrezime = (): string => {
        return this.prezime;
    }

    public setPrezime = (prezime: string): void => {
        this.prezime = prezime;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getAutorNastavniMaterijaliDTO = (): AutorNastavniMaterijalDTO[] => {
        return this.autorNastavniMaterijaliDTO;
    }

    public setAutorNastavniMaterijaliDTO = (autorNastavniMaterijaliDTO: AutorNastavniMaterijalDTO[]): void => {
        this.autorNastavniMaterijaliDTO = autorNastavniMaterijaliDTO;
    }
    

    public constructor () {
        this.autorNastavniMaterijaliDTO = [];
    }

    public getAutorNastavniMaterijalDTO (identifikacija: Identifikacija): AutorNastavniMaterijalDTO {
	throw new Error("Not Implmented");
    }


}
