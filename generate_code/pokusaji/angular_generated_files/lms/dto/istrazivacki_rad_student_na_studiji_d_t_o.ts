
export class IstrazivackiRadStudentNaStudijiDTO implements DTO {

    private id: number;
        
    private datum: Date;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private studentNaStudijiDTO: StudentNaStudijiDTO;
        
    private istrazivackiRadDTO: IstrazivackiRadDTO;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getDatum = (): Date => {
        return this.datum;
    }

    public setDatum = (datum: Date): void => {
        this.datum = datum;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getStudentNaStudijiDTO = (): StudentNaStudijiDTO => {
        return this.studentNaStudijiDTO;
    }

    public setStudentNaStudijiDTO = (studentNaStudijiDTO: StudentNaStudijiDTO): void => {
        this.studentNaStudijiDTO = studentNaStudijiDTO;
    }
    
    public getIstrazivackiRadDTO = (): IstrazivackiRadDTO => {
        return this.istrazivackiRadDTO;
    }

    public setIstrazivackiRadDTO = (istrazivackiRadDTO: IstrazivackiRadDTO): void => {
        this.istrazivackiRadDTO = istrazivackiRadDTO;
    }
    

    public constructor () {
    }


}
