
export class StudentskaSluzbaDTO implements DTO {

    private id: number;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private studentskiSluzbeniciDTO: StudentskiSluzbenikDTO[];
        
    private odeljenjeDTO: OdeljenjeDTO;
        
    private univerzitetDTO: UniverzitetDTO;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getStudentskiSluzbeniciDTO = (): StudentskiSluzbenikDTO[] => {
        return this.studentskiSluzbeniciDTO;
    }

    public setStudentskiSluzbeniciDTO = (studentskiSluzbeniciDTO: StudentskiSluzbenikDTO[]): void => {
        this.studentskiSluzbeniciDTO = studentskiSluzbeniciDTO;
    }
    
    public getOdeljenjeDTO = (): OdeljenjeDTO => {
        return this.odeljenjeDTO;
    }

    public setOdeljenjeDTO = (odeljenjeDTO: OdeljenjeDTO): void => {
        this.odeljenjeDTO = odeljenjeDTO;
    }
    
    public getUniverzitetDTO = (): UniverzitetDTO => {
        return this.univerzitetDTO;
    }

    public setUniverzitetDTO = (univerzitetDTO: UniverzitetDTO): void => {
        this.univerzitetDTO = univerzitetDTO;
    }
    

    public constructor () {
        this.studentskiSluzbeniciDTO = [];
    }

    public getStudentskiSluzbenikDTO (identifikacija: Identifikacija): StudentskiSluzbenikDTO {
	throw new Error("Not Implmented");
    }


}
