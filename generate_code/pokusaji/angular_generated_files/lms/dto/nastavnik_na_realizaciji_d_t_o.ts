
export class NastavnikNaRealizacijiDTO implements DTO {

    private id: number;
        
    private brojCasova: number;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private zvanje: Zvanje;
        
    private realizacijaPredmetaDTO: RealizacijaPredmetaDTO;
        
    private nastavnikNaRealizacijiTipoviNastaveDTO: NastavnikNaRealizacijiTipNastaveDTO[];
        
    private nastavnikDTO: NastavnikDTO;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getBrojCasova = (): number => {
        return this.brojCasova;
    }

    public setBrojCasova = (brojCasova: number): void => {
        this.brojCasova = brojCasova;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getZvanje = (): Zvanje => {
        return this.zvanje;
    }

    public setZvanje = (zvanje: Zvanje): void => {
        this.zvanje = zvanje;
    }
    
    public getRealizacijaPredmetaDTO = (): RealizacijaPredmetaDTO => {
        return this.realizacijaPredmetaDTO;
    }

    public setRealizacijaPredmetaDTO = (realizacijaPredmetaDTO: RealizacijaPredmetaDTO): void => {
        this.realizacijaPredmetaDTO = realizacijaPredmetaDTO;
    }
    
    public getNastavnikNaRealizacijiTipoviNastaveDTO = (): NastavnikNaRealizacijiTipNastaveDTO[] => {
        return this.nastavnikNaRealizacijiTipoviNastaveDTO;
    }

    public setNastavnikNaRealizacijiTipoviNastaveDTO = (nastavnikNaRealizacijiTipoviNastaveDTO: NastavnikNaRealizacijiTipNastaveDTO[]): void => {
        this.nastavnikNaRealizacijiTipoviNastaveDTO = nastavnikNaRealizacijiTipoviNastaveDTO;
    }
    
    public getNastavnikDTO = (): NastavnikDTO => {
        return this.nastavnikDTO;
    }

    public setNastavnikDTO = (nastavnikDTO: NastavnikDTO): void => {
        this.nastavnikDTO = nastavnikDTO;
    }
    

    public constructor () {
        this.nastavnikNaRealizacijiTipoviNastaveDTO = [];
    }

    public getNastavnikNaRealizacijiTipNastaveDTO (identifikacija: Identifikacija): NastavnikNaRealizacijiTipNastaveDTO {
	throw new Error("Not Implmented");
    }


}
