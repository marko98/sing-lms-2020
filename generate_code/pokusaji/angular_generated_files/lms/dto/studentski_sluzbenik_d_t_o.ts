
export class StudentskiSluzbenikDTO implements DTO {

    private id: number;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private registrovaniKorisnikDTO: RegistrovaniKorisnikDTO;
        
    private studentskaSluzbaDTO: StudentskaSluzbaDTO;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getRegistrovaniKorisnikDTO = (): RegistrovaniKorisnikDTO => {
        return this.registrovaniKorisnikDTO;
    }

    public setRegistrovaniKorisnikDTO = (registrovaniKorisnikDTO: RegistrovaniKorisnikDTO): void => {
        this.registrovaniKorisnikDTO = registrovaniKorisnikDTO;
    }
    
    public getStudentskaSluzbaDTO = (): StudentskaSluzbaDTO => {
        return this.studentskaSluzbaDTO;
    }

    public setStudentskaSluzbaDTO = (studentskaSluzbaDTO: StudentskaSluzbaDTO): void => {
        this.studentskaSluzbaDTO = studentskaSluzbaDTO;
    }
    

    public constructor () {
    }


}
