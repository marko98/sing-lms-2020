
export class DatumPohadjanjaPredmetaDTO implements DTO {

    private id: number;
        
    private datum: Date;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private pohadjanjePredmetaDTO: PohadjanjePredmetaDTO;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getDatum = (): Date => {
        return this.datum;
    }

    public setDatum = (datum: Date): void => {
        this.datum = datum;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getPohadjanjePredmetaDTO = (): PohadjanjePredmetaDTO => {
        return this.pohadjanjePredmetaDTO;
    }

    public setPohadjanjePredmetaDTO = (pohadjanjePredmetaDTO: PohadjanjePredmetaDTO): void => {
        this.pohadjanjePredmetaDTO = pohadjanjePredmetaDTO;
    }
    

    public constructor () {
    }


}
