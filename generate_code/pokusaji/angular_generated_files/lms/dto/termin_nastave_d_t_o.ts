
export class TerminNastaveDTO implements DTO {

    private id: number;
        
    private trajanje: number;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private tipNastave: TipNastave;
        
    private prostorijaDTO: ProstorijaDTO;
        
    private nastavnikDTO: NastavnikDTO;
        
    private realizacijaPredmetaDTO: RealizacijaPredmetaDTO;
        
    private vremeOdrzavanjaUNedeljiDTO: VremeOdrzavanjaUNedeljiDTO;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getTrajanje = (): number => {
        return this.trajanje;
    }

    public setTrajanje = (trajanje: number): void => {
        this.trajanje = trajanje;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getTipNastave = (): TipNastave => {
        return this.tipNastave;
    }

    public setTipNastave = (tipNastave: TipNastave): void => {
        this.tipNastave = tipNastave;
    }
    
    public getProstorijaDTO = (): ProstorijaDTO => {
        return this.prostorijaDTO;
    }

    public setProstorijaDTO = (prostorijaDTO: ProstorijaDTO): void => {
        this.prostorijaDTO = prostorijaDTO;
    }
    
    public getNastavnikDTO = (): NastavnikDTO => {
        return this.nastavnikDTO;
    }

    public setNastavnikDTO = (nastavnikDTO: NastavnikDTO): void => {
        this.nastavnikDTO = nastavnikDTO;
    }
    
    public getRealizacijaPredmetaDTO = (): RealizacijaPredmetaDTO => {
        return this.realizacijaPredmetaDTO;
    }

    public setRealizacijaPredmetaDTO = (realizacijaPredmetaDTO: RealizacijaPredmetaDTO): void => {
        this.realizacijaPredmetaDTO = realizacijaPredmetaDTO;
    }
    
    public getVremeOdrzavanjaUNedeljiDTO = (): VremeOdrzavanjaUNedeljiDTO => {
        return this.vremeOdrzavanjaUNedeljiDTO;
    }

    public setVremeOdrzavanjaUNedeljiDTO = (vremeOdrzavanjaUNedeljiDTO: VremeOdrzavanjaUNedeljiDTO): void => {
        this.vremeOdrzavanjaUNedeljiDTO = vremeOdrzavanjaUNedeljiDTO;
    }
    

    public constructor () {
    }


}
