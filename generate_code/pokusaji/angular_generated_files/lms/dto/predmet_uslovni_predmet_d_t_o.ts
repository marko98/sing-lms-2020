
export class PredmetUslovniPredmetDTO implements DTO {

    private id: number;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private uslovDTO: PredmetDTO;
        
    private predmetDTO: PredmetDTO;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getUslovDTO = (): PredmetDTO => {
        return this.uslovDTO;
    }

    public setUslovDTO = (uslovDTO: PredmetDTO): void => {
        this.uslovDTO = uslovDTO;
    }
    
    public getPredmetDTO = (): PredmetDTO => {
        return this.predmetDTO;
    }

    public setPredmetDTO = (predmetDTO: PredmetDTO): void => {
        this.predmetDTO = predmetDTO;
    }
    

    public constructor () {
    }


}
