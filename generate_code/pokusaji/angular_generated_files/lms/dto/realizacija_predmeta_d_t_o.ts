
export class RealizacijaPredmetaDTO implements DTO {

    private id: number;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private drugiObliciNastaveDTO: DrugiOblikNastaveDTO[];
        
    private ishodiDTO: IshodDTO[];
        
    private pohadjanjaPredmetaDTO: PohadjanjePredmetaDTO[];
        
    private obrazovniCiljeviDTO: ObrazovniCiljDTO[];
        
    private nastavniciNaRealizacijiDTO: NastavnikNaRealizacijiDTO[];
        
    private istrazivackiRadoviDTO: IstrazivackiRadDTO[];
        
    private evaluacijeZnanjaDTO: EvaluacijaZnanjaDTO[];
        
    private terminiNastaveDTO: TerminNastaveDTO[];
        
    private predmetDTO: PredmetDTO;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getDrugiObliciNastaveDTO = (): DrugiOblikNastaveDTO[] => {
        return this.drugiObliciNastaveDTO;
    }

    public setDrugiObliciNastaveDTO = (drugiObliciNastaveDTO: DrugiOblikNastaveDTO[]): void => {
        this.drugiObliciNastaveDTO = drugiObliciNastaveDTO;
    }
    
    public getIshodiDTO = (): IshodDTO[] => {
        return this.ishodiDTO;
    }

    public setIshodiDTO = (ishodiDTO: IshodDTO[]): void => {
        this.ishodiDTO = ishodiDTO;
    }
    
    public getPohadjanjaPredmetaDTO = (): PohadjanjePredmetaDTO[] => {
        return this.pohadjanjaPredmetaDTO;
    }

    public setPohadjanjaPredmetaDTO = (pohadjanjaPredmetaDTO: PohadjanjePredmetaDTO[]): void => {
        this.pohadjanjaPredmetaDTO = pohadjanjaPredmetaDTO;
    }
    
    public getObrazovniCiljeviDTO = (): ObrazovniCiljDTO[] => {
        return this.obrazovniCiljeviDTO;
    }

    public setObrazovniCiljeviDTO = (obrazovniCiljeviDTO: ObrazovniCiljDTO[]): void => {
        this.obrazovniCiljeviDTO = obrazovniCiljeviDTO;
    }
    
    public getNastavniciNaRealizacijiDTO = (): NastavnikNaRealizacijiDTO[] => {
        return this.nastavniciNaRealizacijiDTO;
    }

    public setNastavniciNaRealizacijiDTO = (nastavniciNaRealizacijiDTO: NastavnikNaRealizacijiDTO[]): void => {
        this.nastavniciNaRealizacijiDTO = nastavniciNaRealizacijiDTO;
    }
    
    public getIstrazivackiRadoviDTO = (): IstrazivackiRadDTO[] => {
        return this.istrazivackiRadoviDTO;
    }

    public setIstrazivackiRadoviDTO = (istrazivackiRadoviDTO: IstrazivackiRadDTO[]): void => {
        this.istrazivackiRadoviDTO = istrazivackiRadoviDTO;
    }
    
    public getEvaluacijeZnanjaDTO = (): EvaluacijaZnanjaDTO[] => {
        return this.evaluacijeZnanjaDTO;
    }

    public setEvaluacijeZnanjaDTO = (evaluacijeZnanjaDTO: EvaluacijaZnanjaDTO[]): void => {
        this.evaluacijeZnanjaDTO = evaluacijeZnanjaDTO;
    }
    
    public getTerminiNastaveDTO = (): TerminNastaveDTO[] => {
        return this.terminiNastaveDTO;
    }

    public setTerminiNastaveDTO = (terminiNastaveDTO: TerminNastaveDTO[]): void => {
        this.terminiNastaveDTO = terminiNastaveDTO;
    }
    
    public getPredmetDTO = (): PredmetDTO => {
        return this.predmetDTO;
    }

    public setPredmetDTO = (predmetDTO: PredmetDTO): void => {
        this.predmetDTO = predmetDTO;
    }
    

    public constructor () {
        this.drugiObliciNastaveDTO = [];
        this.ishodiDTO = [];
        this.pohadjanjaPredmetaDTO = [];
        this.obrazovniCiljeviDTO = [];
        this.nastavniciNaRealizacijiDTO = [];
        this.istrazivackiRadoviDTO = [];
        this.evaluacijeZnanjaDTO = [];
        this.terminiNastaveDTO = [];
    }

    public getDrugiOblikNastaveDTO (identifikacija: Identifikacija): DrugiOblikNastaveDTO {
	throw new Error("Not Implmented");
    }

    public getNastavnikNaRealizacijiDTO (identifikacija: Identifikacija): NastavnikNaRealizacijiDTO {
	throw new Error("Not Implmented");
    }

    public getPohadjanjePredmetaDTO (identifikacija: Identifikacija): PohadjanjePredmetaDTO {
	throw new Error("Not Implmented");
    }

    public getEvaluacijaZnanjaDTO (identifikacija: Identifikacija): EvaluacijaZnanjaDTO {
	throw new Error("Not Implmented");
    }

    public getObrazovniCiljDTO (identifikacija: Identifikacija): ObrazovniCiljDTO {
	throw new Error("Not Implmented");
    }

    public getTerminNastaveDTO (identifikacija: Identifikacija): TerminNastaveDTO {
	throw new Error("Not Implmented");
    }

    public getIshodDTO (identifikacija: Identifikacija): IshodDTO {
	throw new Error("Not Implmented");
    }

    public getIstrazivackiRadDTO (identifikacija: Identifikacija): IstrazivackiRadDTO {
	throw new Error("Not Implmented");
    }


}
