
export class DiplomskiRadDTO implements DTO {

    private id: number;
        
    private tema: string;
        
    private ocena: number;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private mentorDTO: NastavnikDTO;
        
    private nastavniciDiplomskiRadDTO: NastavnikDiplomskiRadDTO[];
        
    private studentNaStudijiDTO: StudentNaStudijiDTO;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getTema = (): string => {
        return this.tema;
    }

    public setTema = (tema: string): void => {
        this.tema = tema;
    }
    
    public getOcena = (): number => {
        return this.ocena;
    }

    public setOcena = (ocena: number): void => {
        this.ocena = ocena;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getMentorDTO = (): NastavnikDTO => {
        return this.mentorDTO;
    }

    public setMentorDTO = (mentorDTO: NastavnikDTO): void => {
        this.mentorDTO = mentorDTO;
    }
    
    public getNastavniciDiplomskiRadDTO = (): NastavnikDiplomskiRadDTO[] => {
        return this.nastavniciDiplomskiRadDTO;
    }

    public setNastavniciDiplomskiRadDTO = (nastavniciDiplomskiRadDTO: NastavnikDiplomskiRadDTO[]): void => {
        this.nastavniciDiplomskiRadDTO = nastavniciDiplomskiRadDTO;
    }
    
    public getStudentNaStudijiDTO = (): StudentNaStudijiDTO => {
        return this.studentNaStudijiDTO;
    }

    public setStudentNaStudijiDTO = (studentNaStudijiDTO: StudentNaStudijiDTO): void => {
        this.studentNaStudijiDTO = studentNaStudijiDTO;
    }
    

    public constructor () {
        this.nastavniciDiplomskiRadDTO = [];
    }

    public getNastavnikDiplomskiRadDTO (identifikacija: Identifikacija): NastavnikDiplomskiRadDTO {
	throw new Error("Not Implmented");
    }


}
