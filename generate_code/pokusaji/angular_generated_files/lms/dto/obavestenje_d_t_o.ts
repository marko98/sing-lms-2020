
export class ObavestenjeDTO implements DTO {

    private id: number;
        
    private naslov: string;
        
    private datum: Date;
        
    private sadrzaj: string;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private godineStudijaObavestenjeDTO: GodinaStudijaObavestenjeDTO[];
        
    private fajloviDTO: FajlDTO[];
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getNaslov = (): string => {
        return this.naslov;
    }

    public setNaslov = (naslov: string): void => {
        this.naslov = naslov;
    }
    
    public getDatum = (): Date => {
        return this.datum;
    }

    public setDatum = (datum: Date): void => {
        this.datum = datum;
    }
    
    public getSadrzaj = (): string => {
        return this.sadrzaj;
    }

    public setSadrzaj = (sadrzaj: string): void => {
        this.sadrzaj = sadrzaj;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getGodineStudijaObavestenjeDTO = (): GodinaStudijaObavestenjeDTO[] => {
        return this.godineStudijaObavestenjeDTO;
    }

    public setGodineStudijaObavestenjeDTO = (godineStudijaObavestenjeDTO: GodinaStudijaObavestenjeDTO[]): void => {
        this.godineStudijaObavestenjeDTO = godineStudijaObavestenjeDTO;
    }
    
    public getFajloviDTO = (): FajlDTO[] => {
        return this.fajloviDTO;
    }

    public setFajloviDTO = (fajloviDTO: FajlDTO[]): void => {
        this.fajloviDTO = fajloviDTO;
    }
    

    public constructor () {
        this.godineStudijaObavestenjeDTO = [];
        this.fajloviDTO = [];
    }

    public getGodinaStudijaObavestenjeDTO (identifikacija: Identifikacija): GodinaStudijaObavestenjeDTO {
	throw new Error("Not Implmented");
    }

    public getFajlDTO (identifikacija: Identifikacija): FajlDTO {
	throw new Error("Not Implmented");
    }


}
