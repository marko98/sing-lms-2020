
export class VremeOdrzavanjaUNedeljiDTO implements DTO {

    private id: number;
        
    private vreme: Time;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private konsultacijaDTO: KonsultacijaDTO;
        
    private dan: Dan;
        
    private terminNastaveDTO: TerminNastaveDTO;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getVreme = (): Time => {
        return this.vreme;
    }

    public setVreme = (vreme: Time): void => {
        this.vreme = vreme;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getKonsultacijaDTO = (): KonsultacijaDTO => {
        return this.konsultacijaDTO;
    }

    public setKonsultacijaDTO = (konsultacijaDTO: KonsultacijaDTO): void => {
        this.konsultacijaDTO = konsultacijaDTO;
    }
    
    public getDan = (): Dan => {
        return this.dan;
    }

    public setDan = (dan: Dan): void => {
        this.dan = dan;
    }
    
    public getTerminNastaveDTO = (): TerminNastaveDTO => {
        return this.terminNastaveDTO;
    }

    public setTerminNastaveDTO = (terminNastaveDTO: TerminNastaveDTO): void => {
        this.terminNastaveDTO = terminNastaveDTO;
    }
    

    public constructor () {
    }


}
