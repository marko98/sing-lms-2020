
export class GodinaStudijaDTO implements DTO {

    private id: number;
        
    private godina: Date;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private semestar: Semestar;
        
    private studijskiProgramDTO: StudijskiProgramDTO;
        
    private studentiNaStudijiDTO: StudentNaStudijiDTO[];
        
    private predmetiDTO: PredmetDTO[];
        
    private godinaStudijaObavestenjaDTO: GodinaStudijaObavestenjeDTO[];
        
    private konsultacijeDTO: KonsultacijaDTO[];
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getGodina = (): Date => {
        return this.godina;
    }

    public setGodina = (godina: Date): void => {
        this.godina = godina;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getSemestar = (): Semestar => {
        return this.semestar;
    }

    public setSemestar = (semestar: Semestar): void => {
        this.semestar = semestar;
    }
    
    public getStudijskiProgramDTO = (): StudijskiProgramDTO => {
        return this.studijskiProgramDTO;
    }

    public setStudijskiProgramDTO = (studijskiProgramDTO: StudijskiProgramDTO): void => {
        this.studijskiProgramDTO = studijskiProgramDTO;
    }
    
    public getStudentiNaStudijiDTO = (): StudentNaStudijiDTO[] => {
        return this.studentiNaStudijiDTO;
    }

    public setStudentiNaStudijiDTO = (studentiNaStudijiDTO: StudentNaStudijiDTO[]): void => {
        this.studentiNaStudijiDTO = studentiNaStudijiDTO;
    }
    
    public getPredmetiDTO = (): PredmetDTO[] => {
        return this.predmetiDTO;
    }

    public setPredmetiDTO = (predmetiDTO: PredmetDTO[]): void => {
        this.predmetiDTO = predmetiDTO;
    }
    
    public getGodinaStudijaObavestenjaDTO = (): GodinaStudijaObavestenjeDTO[] => {
        return this.godinaStudijaObavestenjaDTO;
    }

    public setGodinaStudijaObavestenjaDTO = (godinaStudijaObavestenjaDTO: GodinaStudijaObavestenjeDTO[]): void => {
        this.godinaStudijaObavestenjaDTO = godinaStudijaObavestenjaDTO;
    }
    
    public getKonsultacijeDTO = (): KonsultacijaDTO[] => {
        return this.konsultacijeDTO;
    }

    public setKonsultacijeDTO = (konsultacijeDTO: KonsultacijaDTO[]): void => {
        this.konsultacijeDTO = konsultacijeDTO;
    }
    

    public constructor () {
        this.studentiNaStudijiDTO = [];
        this.predmetiDTO = [];
        this.godinaStudijaObavestenjaDTO = [];
        this.konsultacijeDTO = [];
    }

    public getStudentNaStudijiDTO (identifikacija: Identifikacija): StudentNaStudijiDTO {
	throw new Error("Not Implmented");
    }

    public getKonsultacijaDTO (identifikacija: Identifikacija): KonsultacijaDTO {
	throw new Error("Not Implmented");
    }

    public getGodinaStudijaObavestenjeDTO (identifikacija: Identifikacija): GodinaStudijaObavestenjeDTO {
	throw new Error("Not Implmented");
    }

    public getPredmetDTO (identifikacija: Identifikacija): PredmetDTO {
	throw new Error("Not Implmented");
    }


}
