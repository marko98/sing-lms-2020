
export class IstrazivackiRadDTO implements DTO {

    private id: number;
        
    private tema: string;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private nastavnikDTO: NastavnikDTO;
        
    private realizacijaPredmetaDTO: RealizacijaPredmetaDTO;
        
    private istrazivackiRadStudentiNaStudijamaDTO: IstrazivackiRadStudentNaStudijiDTO[];
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getTema = (): string => {
        return this.tema;
    }

    public setTema = (tema: string): void => {
        this.tema = tema;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getNastavnikDTO = (): NastavnikDTO => {
        return this.nastavnikDTO;
    }

    public setNastavnikDTO = (nastavnikDTO: NastavnikDTO): void => {
        this.nastavnikDTO = nastavnikDTO;
    }
    
    public getRealizacijaPredmetaDTO = (): RealizacijaPredmetaDTO => {
        return this.realizacijaPredmetaDTO;
    }

    public setRealizacijaPredmetaDTO = (realizacijaPredmetaDTO: RealizacijaPredmetaDTO): void => {
        this.realizacijaPredmetaDTO = realizacijaPredmetaDTO;
    }
    
    public getIstrazivackiRadStudentiNaStudijamaDTO = (): IstrazivackiRadStudentNaStudijiDTO[] => {
        return this.istrazivackiRadStudentiNaStudijamaDTO;
    }

    public setIstrazivackiRadStudentiNaStudijamaDTO = (istrazivackiRadStudentiNaStudijamaDTO: IstrazivackiRadStudentNaStudijiDTO[]): void => {
        this.istrazivackiRadStudentiNaStudijamaDTO = istrazivackiRadStudentiNaStudijamaDTO;
    }
    

    public constructor () {
        this.istrazivackiRadStudentiNaStudijamaDTO = [];
    }

    public getIstrazivackiRadStudentNaStudijiDTO (identifikacija: Identifikacija): IstrazivackiRadStudentNaStudijiDTO {
	throw new Error("Not Implmented");
    }


}
