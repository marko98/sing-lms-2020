
export class ProstorijaDTO implements DTO {

    private id: number;
        
    private naziv: string;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private tip: TipProstorije;
        
    private odeljenjeDTO: OdeljenjeDTO;
        
    private terminiNastaveDTO: TerminNastaveDTO[];
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getNaziv = (): string => {
        return this.naziv;
    }

    public setNaziv = (naziv: string): void => {
        this.naziv = naziv;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getTip = (): TipProstorije => {
        return this.tip;
    }

    public setTip = (tip: TipProstorije): void => {
        this.tip = tip;
    }
    
    public getOdeljenjeDTO = (): OdeljenjeDTO => {
        return this.odeljenjeDTO;
    }

    public setOdeljenjeDTO = (odeljenjeDTO: OdeljenjeDTO): void => {
        this.odeljenjeDTO = odeljenjeDTO;
    }
    
    public getTerminiNastaveDTO = (): TerminNastaveDTO[] => {
        return this.terminiNastaveDTO;
    }

    public setTerminiNastaveDTO = (terminiNastaveDTO: TerminNastaveDTO[]): void => {
        this.terminiNastaveDTO = terminiNastaveDTO;
    }
    

    public constructor () {
        this.terminiNastaveDTO = [];
    }


}
