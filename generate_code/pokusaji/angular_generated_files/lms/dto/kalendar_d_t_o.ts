
export class KalendarDTO implements DTO {

    private id: number;
        
    private pocetniDatum: Date;
        
    private krajnjiDatum: Date;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private tipStudija: TipStudija;
        
    private univerzitetDTO: UniverzitetDTO;
        
    private dogadjajiKalendarDTO: DogadjajKalendarDTO[];
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getPocetniDatum = (): Date => {
        return this.pocetniDatum;
    }

    public setPocetniDatum = (pocetniDatum: Date): void => {
        this.pocetniDatum = pocetniDatum;
    }
    
    public getKrajnjiDatum = (): Date => {
        return this.krajnjiDatum;
    }

    public setKrajnjiDatum = (krajnjiDatum: Date): void => {
        this.krajnjiDatum = krajnjiDatum;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getTipStudija = (): TipStudija => {
        return this.tipStudija;
    }

    public setTipStudija = (tipStudija: TipStudija): void => {
        this.tipStudija = tipStudija;
    }
    
    public getUniverzitetDTO = (): UniverzitetDTO => {
        return this.univerzitetDTO;
    }

    public setUniverzitetDTO = (univerzitetDTO: UniverzitetDTO): void => {
        this.univerzitetDTO = univerzitetDTO;
    }
    
    public getDogadjajiKalendarDTO = (): DogadjajKalendarDTO[] => {
        return this.dogadjajiKalendarDTO;
    }

    public setDogadjajiKalendarDTO = (dogadjajiKalendarDTO: DogadjajKalendarDTO[]): void => {
        this.dogadjajiKalendarDTO = dogadjajiKalendarDTO;
    }
    

    public constructor () {
        this.dogadjajiKalendarDTO = [];
    }

    public getDogadjajKalendarDTO (identifikacija: Identifikacija): DogadjajKalendarDTO {
	throw new Error("Not Implmented");
    }


}
