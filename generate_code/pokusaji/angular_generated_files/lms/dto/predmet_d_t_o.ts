
export class PredmetDTO implements DTO {

    private id: number;
        
    private naziv: string;
        
    private obavezan: boolean;
        
    private brojPredavanja: number;
        
    private brojVezbi: number;
        
    private brojCasovaZaIstrazivackeRadove: number;
        
    private espb: number;
        
    private trajanjeUSemestrima: number;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private silabusDTO: SilabusDTO;
        
    private godinaStudijaDTO: GodinaStudijaDTO;
        
    private predmetTipoviDrugogOblikaNastaveDTO: PredmetTipDrugogOblikaNastaveDTO[];
        
    private predmetTipoviNastaveDTO: PredmetTipNastaveDTO[];
        
    private predmetiZaKojeSamUslovDTO: PredmetUslovniPredmetDTO[];
        
    private uslovniPredmetiDTO: PredmetUslovniPredmetDTO[];
        
    private realizacijaPredmetaDTO: RealizacijaPredmetaDTO;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getNaziv = (): string => {
        return this.naziv;
    }

    public setNaziv = (naziv: string): void => {
        this.naziv = naziv;
    }
    
    public getObavezan = (): boolean => {
        return this.obavezan;
    }

    public setObavezan = (obavezan: boolean): void => {
        this.obavezan = obavezan;
    }
    
    public getBrojPredavanja = (): number => {
        return this.brojPredavanja;
    }

    public setBrojPredavanja = (brojPredavanja: number): void => {
        this.brojPredavanja = brojPredavanja;
    }
    
    public getBrojVezbi = (): number => {
        return this.brojVezbi;
    }

    public setBrojVezbi = (brojVezbi: number): void => {
        this.brojVezbi = brojVezbi;
    }
    
    public getBrojCasovaZaIstrazivackeRadove = (): number => {
        return this.brojCasovaZaIstrazivackeRadove;
    }

    public setBrojCasovaZaIstrazivackeRadove = (brojCasovaZaIstrazivackeRadove: number): void => {
        this.brojCasovaZaIstrazivackeRadove = brojCasovaZaIstrazivackeRadove;
    }
    
    public getEspb = (): number => {
        return this.espb;
    }

    public setEspb = (espb: number): void => {
        this.espb = espb;
    }
    
    public getTrajanjeUSemestrima = (): number => {
        return this.trajanjeUSemestrima;
    }

    public setTrajanjeUSemestrima = (trajanjeUSemestrima: number): void => {
        this.trajanjeUSemestrima = trajanjeUSemestrima;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getSilabusDTO = (): SilabusDTO => {
        return this.silabusDTO;
    }

    public setSilabusDTO = (silabusDTO: SilabusDTO): void => {
        this.silabusDTO = silabusDTO;
    }
    
    public getGodinaStudijaDTO = (): GodinaStudijaDTO => {
        return this.godinaStudijaDTO;
    }

    public setGodinaStudijaDTO = (godinaStudijaDTO: GodinaStudijaDTO): void => {
        this.godinaStudijaDTO = godinaStudijaDTO;
    }
    
    public getPredmetTipoviDrugogOblikaNastaveDTO = (): PredmetTipDrugogOblikaNastaveDTO[] => {
        return this.predmetTipoviDrugogOblikaNastaveDTO;
    }

    public setPredmetTipoviDrugogOblikaNastaveDTO = (predmetTipoviDrugogOblikaNastaveDTO: PredmetTipDrugogOblikaNastaveDTO[]): void => {
        this.predmetTipoviDrugogOblikaNastaveDTO = predmetTipoviDrugogOblikaNastaveDTO;
    }
    
    public getPredmetTipoviNastaveDTO = (): PredmetTipNastaveDTO[] => {
        return this.predmetTipoviNastaveDTO;
    }

    public setPredmetTipoviNastaveDTO = (predmetTipoviNastaveDTO: PredmetTipNastaveDTO[]): void => {
        this.predmetTipoviNastaveDTO = predmetTipoviNastaveDTO;
    }
    
    public getPredmetiZaKojeSamUslovDTO = (): PredmetUslovniPredmetDTO[] => {
        return this.predmetiZaKojeSamUslovDTO;
    }

    public setPredmetiZaKojeSamUslovDTO = (predmetiZaKojeSamUslovDTO: PredmetUslovniPredmetDTO[]): void => {
        this.predmetiZaKojeSamUslovDTO = predmetiZaKojeSamUslovDTO;
    }
    
    public getUslovniPredmetiDTO = (): PredmetUslovniPredmetDTO[] => {
        return this.uslovniPredmetiDTO;
    }

    public setUslovniPredmetiDTO = (uslovniPredmetiDTO: PredmetUslovniPredmetDTO[]): void => {
        this.uslovniPredmetiDTO = uslovniPredmetiDTO;
    }
    
    public getRealizacijaPredmetaDTO = (): RealizacijaPredmetaDTO => {
        return this.realizacijaPredmetaDTO;
    }

    public setRealizacijaPredmetaDTO = (realizacijaPredmetaDTO: RealizacijaPredmetaDTO): void => {
        this.realizacijaPredmetaDTO = realizacijaPredmetaDTO;
    }
    

    public constructor () {
        this.predmetTipoviDrugogOblikaNastaveDTO = [];
        this.predmetTipoviNastaveDTO = [];
        this.predmetiZaKojeSamUslovDTO = [];
        this.uslovniPredmetiDTO = [];
    }

    public getPreduslov (identifikacija: Identifikacija): PredmetDTO {
	throw new Error("Not Implmented");
    }

    public getPredmetTipNastaveDTO (identifikacija: Identifikacija): PredmetTipNastaveDTO {
	throw new Error("Not Implmented");
    }

    public getPredmetTipDrugogOblikaNastaveDTO (identifikacija: Identifikacija): PredmetTipDrugogOblikaNastaveDTO {
	throw new Error("Not Implmented");
    }

    public getPredmetZaKojiSamUslovDTO (identifikacija: Identifikacija): PredmetUslovniPredmetDTO {
	throw new Error("Not Implmented");
    }

    public getUslovniPredmetDTO (identifikacija: Identifikacija): PredmetUslovniPredmetDTO {
	throw new Error("Not Implmented");
    }


}
