
export class FajlDTO implements DTO {

    private id: number;
        
    private opis: string;
        
    private url: string;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private naziv: string;
        
    private tip: string;
        
    private evaluacijaZnanjaDTO: EvaluacijaZnanjaDTO;
        
    private obavestenjeDTO: ObavestenjeDTO;
        
    private nastavniMaterijalDTO: NastavniMaterijalDTO;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getOpis = (): string => {
        return this.opis;
    }

    public setOpis = (opis: string): void => {
        this.opis = opis;
    }
    
    public getUrl = (): string => {
        return this.url;
    }

    public setUrl = (url: string): void => {
        this.url = url;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getNaziv = (): string => {
        return this.naziv;
    }

    public setNaziv = (naziv: string): void => {
        this.naziv = naziv;
    }
    
    public getTip = (): string => {
        return this.tip;
    }

    public setTip = (tip: string): void => {
        this.tip = tip;
    }
    
    public getEvaluacijaZnanjaDTO = (): EvaluacijaZnanjaDTO => {
        return this.evaluacijaZnanjaDTO;
    }

    public setEvaluacijaZnanjaDTO = (evaluacijaZnanjaDTO: EvaluacijaZnanjaDTO): void => {
        this.evaluacijaZnanjaDTO = evaluacijaZnanjaDTO;
    }
    
    public getObavestenjeDTO = (): ObavestenjeDTO => {
        return this.obavestenjeDTO;
    }

    public setObavestenjeDTO = (obavestenjeDTO: ObavestenjeDTO): void => {
        this.obavestenjeDTO = obavestenjeDTO;
    }
    
    public getNastavniMaterijalDTO = (): NastavniMaterijalDTO => {
        return this.nastavniMaterijalDTO;
    }

    public setNastavniMaterijalDTO = (nastavniMaterijalDTO: NastavniMaterijalDTO): void => {
        this.nastavniMaterijalDTO = nastavniMaterijalDTO;
    }
    

    public constructor () {
    }


}
