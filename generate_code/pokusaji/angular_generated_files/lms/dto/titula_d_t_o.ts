
export class TitulaDTO implements DTO {

    private id: number;
        
    private datumIzbora: Date;
        
    private datumPrestanka: Date;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private tip: TipTitule;
        
    private naucnaOblastDTO: NaucnaOblastDTO;
        
    private nastavnikDTO: NastavnikDTO;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getDatumIzbora = (): Date => {
        return this.datumIzbora;
    }

    public setDatumIzbora = (datumIzbora: Date): void => {
        this.datumIzbora = datumIzbora;
    }
    
    public getDatumPrestanka = (): Date => {
        return this.datumPrestanka;
    }

    public setDatumPrestanka = (datumPrestanka: Date): void => {
        this.datumPrestanka = datumPrestanka;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getTip = (): TipTitule => {
        return this.tip;
    }

    public setTip = (tip: TipTitule): void => {
        this.tip = tip;
    }
    
    public getNaucnaOblastDTO = (): NaucnaOblastDTO => {
        return this.naucnaOblastDTO;
    }

    public setNaucnaOblastDTO = (naucnaOblastDTO: NaucnaOblastDTO): void => {
        this.naucnaOblastDTO = naucnaOblastDTO;
    }
    
    public getNastavnikDTO = (): NastavnikDTO => {
        return this.nastavnikDTO;
    }

    public setNastavnikDTO = (nastavnikDTO: NastavnikDTO): void => {
        this.nastavnikDTO = nastavnikDTO;
    }
    

    public constructor () {
    }


}
