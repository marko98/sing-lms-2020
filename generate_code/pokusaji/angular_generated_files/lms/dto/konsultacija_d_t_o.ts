
export class KonsultacijaDTO implements DTO {

    private id: number;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private godinaStudijaDTO: GodinaStudijaDTO;
        
    private vremeOdrzavanjaUNedeljiDTO: VremeOdrzavanjaUNedeljiDTO;
        
    private nastavnikDTO: NastavnikDTO;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getGodinaStudijaDTO = (): GodinaStudijaDTO => {
        return this.godinaStudijaDTO;
    }

    public setGodinaStudijaDTO = (godinaStudijaDTO: GodinaStudijaDTO): void => {
        this.godinaStudijaDTO = godinaStudijaDTO;
    }
    
    public getVremeOdrzavanjaUNedeljiDTO = (): VremeOdrzavanjaUNedeljiDTO => {
        return this.vremeOdrzavanjaUNedeljiDTO;
    }

    public setVremeOdrzavanjaUNedeljiDTO = (vremeOdrzavanjaUNedeljiDTO: VremeOdrzavanjaUNedeljiDTO): void => {
        this.vremeOdrzavanjaUNedeljiDTO = vremeOdrzavanjaUNedeljiDTO;
    }
    
    public getNastavnikDTO = (): NastavnikDTO => {
        return this.nastavnikDTO;
    }

    public setNastavnikDTO = (nastavnikDTO: NastavnikDTO): void => {
        this.nastavnikDTO = nastavnikDTO;
    }
    

    public constructor () {
    }


}
