
export class EvaluacijaZnanjaDTO implements DTO {

    private id: number;
        
    private pocetak: Date;
        
    private trajanje: number;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private ispitniRok: IspitniRok;
        
    private nastavniciEvaluacijaZnanjaDTO: NastavnikEvaluacijaZnanjaDTO[];
        
    private tipEvaluacije: TipEvaluacije;
        
    private realizacijaPredmetaDTO: RealizacijaPredmetaDTO;
        
    private ishodDTO: IshodDTO;
        
    private fajloviDTO: FajlDTO[];
        
    private polaganjaDTO: PolaganjeDTO[];
        
    private pitanjaDTO: PitanjeDTO[];
        
    private evaluacijaZnanjaNaciniEvaluacijeDTO: EvaluacijaZnanjaNacinEvaluacijeDTO[];
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getPocetak = (): Date => {
        return this.pocetak;
    }

    public setPocetak = (pocetak: Date): void => {
        this.pocetak = pocetak;
    }
    
    public getTrajanje = (): number => {
        return this.trajanje;
    }

    public setTrajanje = (trajanje: number): void => {
        this.trajanje = trajanje;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getIspitniRok = (): IspitniRok => {
        return this.ispitniRok;
    }

    public setIspitniRok = (ispitniRok: IspitniRok): void => {
        this.ispitniRok = ispitniRok;
    }
    
    public getNastavniciEvaluacijaZnanjaDTO = (): NastavnikEvaluacijaZnanjaDTO[] => {
        return this.nastavniciEvaluacijaZnanjaDTO;
    }

    public setNastavniciEvaluacijaZnanjaDTO = (nastavniciEvaluacijaZnanjaDTO: NastavnikEvaluacijaZnanjaDTO[]): void => {
        this.nastavniciEvaluacijaZnanjaDTO = nastavniciEvaluacijaZnanjaDTO;
    }
    
    public getTipEvaluacije = (): TipEvaluacije => {
        return this.tipEvaluacije;
    }

    public setTipEvaluacije = (tipEvaluacije: TipEvaluacije): void => {
        this.tipEvaluacije = tipEvaluacije;
    }
    
    public getRealizacijaPredmetaDTO = (): RealizacijaPredmetaDTO => {
        return this.realizacijaPredmetaDTO;
    }

    public setRealizacijaPredmetaDTO = (realizacijaPredmetaDTO: RealizacijaPredmetaDTO): void => {
        this.realizacijaPredmetaDTO = realizacijaPredmetaDTO;
    }
    
    public getIshodDTO = (): IshodDTO => {
        return this.ishodDTO;
    }

    public setIshodDTO = (ishodDTO: IshodDTO): void => {
        this.ishodDTO = ishodDTO;
    }
    
    public getFajloviDTO = (): FajlDTO[] => {
        return this.fajloviDTO;
    }

    public setFajloviDTO = (fajloviDTO: FajlDTO[]): void => {
        this.fajloviDTO = fajloviDTO;
    }
    
    public getPolaganjaDTO = (): PolaganjeDTO[] => {
        return this.polaganjaDTO;
    }

    public setPolaganjaDTO = (polaganjaDTO: PolaganjeDTO[]): void => {
        this.polaganjaDTO = polaganjaDTO;
    }
    
    public getPitanjaDTO = (): PitanjeDTO[] => {
        return this.pitanjaDTO;
    }

    public setPitanjaDTO = (pitanjaDTO: PitanjeDTO[]): void => {
        this.pitanjaDTO = pitanjaDTO;
    }
    
    public getEvaluacijaZnanjaNaciniEvaluacijeDTO = (): EvaluacijaZnanjaNacinEvaluacijeDTO[] => {
        return this.evaluacijaZnanjaNaciniEvaluacijeDTO;
    }

    public setEvaluacijaZnanjaNaciniEvaluacijeDTO = (evaluacijaZnanjaNaciniEvaluacijeDTO: EvaluacijaZnanjaNacinEvaluacijeDTO[]): void => {
        this.evaluacijaZnanjaNaciniEvaluacijeDTO = evaluacijaZnanjaNaciniEvaluacijeDTO;
    }
    

    public constructor () {
        this.nastavniciEvaluacijaZnanjaDTO = [];
        this.fajloviDTO = [];
        this.polaganjaDTO = [];
        this.pitanjaDTO = [];
        this.evaluacijaZnanjaNaciniEvaluacijeDTO = [];
    }

    public getNastavnikEvaluacijaZnanjaDTO (identifikacija: Identifikacija): NastavnikEvaluacijaZnanjaDTO {
	throw new Error("Not Implmented");
    }

    public getPolaganjeDTO (identifikacija: Identifikacija): PolaganjeDTO {
	throw new Error("Not Implmented");
    }

    public getEvaluacijaZnanjaNacinEvaluacijeDTO (identifikacija: Identifikacija): EvaluacijaZnanjaNacinEvaluacijeDTO {
	throw new Error("Not Implmented");
    }

    public getFajlDTO (identifikacija: Identifikacija): FajlDTO {
	throw new Error("Not Implmented");
    }

    public getPitanjeDTO (identifikacija: Identifikacija): PitanjeDTO {
	throw new Error("Not Implmented");
    }


}
