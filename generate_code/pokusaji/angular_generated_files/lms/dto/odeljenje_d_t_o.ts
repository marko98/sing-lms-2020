
export class OdeljenjeDTO implements DTO {

    private id: number;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private tip: TipOdeljenja;
        
    private adresaDTO: AdresaDTO;
        
    private prostorijeDTO: ProstorijaDTO[];
        
    private fakultetDTO: FakultetDTO;
        
    private studentskaSluzbaDTO: StudentskaSluzbaDTO;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getTip = (): TipOdeljenja => {
        return this.tip;
    }

    public setTip = (tip: TipOdeljenja): void => {
        this.tip = tip;
    }
    
    public getAdresaDTO = (): AdresaDTO => {
        return this.adresaDTO;
    }

    public setAdresaDTO = (adresaDTO: AdresaDTO): void => {
        this.adresaDTO = adresaDTO;
    }
    
    public getProstorijeDTO = (): ProstorijaDTO[] => {
        return this.prostorijeDTO;
    }

    public setProstorijeDTO = (prostorijeDTO: ProstorijaDTO[]): void => {
        this.prostorijeDTO = prostorijeDTO;
    }
    
    public getFakultetDTO = (): FakultetDTO => {
        return this.fakultetDTO;
    }

    public setFakultetDTO = (fakultetDTO: FakultetDTO): void => {
        this.fakultetDTO = fakultetDTO;
    }
    
    public getStudentskaSluzbaDTO = (): StudentskaSluzbaDTO => {
        return this.studentskaSluzbaDTO;
    }

    public setStudentskaSluzbaDTO = (studentskaSluzbaDTO: StudentskaSluzbaDTO): void => {
        this.studentskaSluzbaDTO = studentskaSluzbaDTO;
    }
    

    public constructor () {
        this.prostorijeDTO = [];
    }

    public getProstorijaDTO (identifikacija: Identifikacija): ProstorijaDTO {
	throw new Error("Not Implmented");
    }


}
