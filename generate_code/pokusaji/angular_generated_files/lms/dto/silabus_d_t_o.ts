
export class SilabusDTO implements DTO {

    private id: number;
        
    private naslov: string;
        
    private opis: string;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private predmetDTO: PredmetDTO;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getNaslov = (): string => {
        return this.naslov;
    }

    public setNaslov = (naslov: string): void => {
        this.naslov = naslov;
    }
    
    public getOpis = (): string => {
        return this.opis;
    }

    public setOpis = (opis: string): void => {
        this.opis = opis;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getPredmetDTO = (): PredmetDTO => {
        return this.predmetDTO;
    }

    public setPredmetDTO = (predmetDTO: PredmetDTO): void => {
        this.predmetDTO = predmetDTO;
    }
    

    public constructor () {
    }


}
