
export class PohadjanjePredmetaDTO implements DTO {

    private id: number;
        
    private konacnaOcena: number;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private studentNaStudijiDTO: StudentNaStudijiDTO;
        
    private polaganjaDTO: PolaganjeDTO[];
        
    private realizacijaPredmetaDTO: RealizacijaPredmetaDTO;
        
    private prisustvoDTO: DatumPohadjanjaPredmetaDTO[];
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getKonacnaOcena = (): number => {
        return this.konacnaOcena;
    }

    public setKonacnaOcena = (konacnaOcena: number): void => {
        this.konacnaOcena = konacnaOcena;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getStudentNaStudijiDTO = (): StudentNaStudijiDTO => {
        return this.studentNaStudijiDTO;
    }

    public setStudentNaStudijiDTO = (studentNaStudijiDTO: StudentNaStudijiDTO): void => {
        this.studentNaStudijiDTO = studentNaStudijiDTO;
    }
    
    public getPolaganjaDTO = (): PolaganjeDTO[] => {
        return this.polaganjaDTO;
    }

    public setPolaganjaDTO = (polaganjaDTO: PolaganjeDTO[]): void => {
        this.polaganjaDTO = polaganjaDTO;
    }
    
    public getRealizacijaPredmetaDTO = (): RealizacijaPredmetaDTO => {
        return this.realizacijaPredmetaDTO;
    }

    public setRealizacijaPredmetaDTO = (realizacijaPredmetaDTO: RealizacijaPredmetaDTO): void => {
        this.realizacijaPredmetaDTO = realizacijaPredmetaDTO;
    }
    
    public getPrisustvoDTO = (): DatumPohadjanjaPredmetaDTO[] => {
        return this.prisustvoDTO;
    }

    public setPrisustvoDTO = (prisustvoDTO: DatumPohadjanjaPredmetaDTO[]): void => {
        this.prisustvoDTO = prisustvoDTO;
    }
    

    public constructor () {
        this.polaganjaDTO = [];
        this.prisustvoDTO = [];
    }

    public getPolaganjeDTO (identifikacija: Identifikacija): PolaganjeDTO {
	throw new Error("Not Implmented");
    }

    public getDatumPohadjanjaPredmetaDTO (identifikacija: Identifikacija): DatumPohadjanjaPredmetaDTO {
	throw new Error("Not Implmented");
    }


}
