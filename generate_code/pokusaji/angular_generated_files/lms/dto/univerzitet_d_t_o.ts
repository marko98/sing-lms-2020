
export class UniverzitetDTO implements DTO {

    private id: number;
        
    private naziv: string;
        
    private datumOsnivanja: Date;
        
    private opis: string;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private adreseDTO: AdresaDTO[];
        
    private fakultetiDTO: FakultetDTO[];
        
    private kalendariDTO: KalendarDTO[];
        
    private rektorDTO: NastavnikDTO;
        
    private studentskaSluzbaDTO: StudentskaSluzbaDTO;
        
    private kontaktiDTO: KontaktDTO[];
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getNaziv = (): string => {
        return this.naziv;
    }

    public setNaziv = (naziv: string): void => {
        this.naziv = naziv;
    }
    
    public getDatumOsnivanja = (): Date => {
        return this.datumOsnivanja;
    }

    public setDatumOsnivanja = (datumOsnivanja: Date): void => {
        this.datumOsnivanja = datumOsnivanja;
    }
    
    public getOpis = (): string => {
        return this.opis;
    }

    public setOpis = (opis: string): void => {
        this.opis = opis;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getAdreseDTO = (): AdresaDTO[] => {
        return this.adreseDTO;
    }

    public setAdreseDTO = (adreseDTO: AdresaDTO[]): void => {
        this.adreseDTO = adreseDTO;
    }
    
    public getFakultetiDTO = (): FakultetDTO[] => {
        return this.fakultetiDTO;
    }

    public setFakultetiDTO = (fakultetiDTO: FakultetDTO[]): void => {
        this.fakultetiDTO = fakultetiDTO;
    }
    
    public getKalendariDTO = (): KalendarDTO[] => {
        return this.kalendariDTO;
    }

    public setKalendariDTO = (kalendariDTO: KalendarDTO[]): void => {
        this.kalendariDTO = kalendariDTO;
    }
    
    public getRektorDTO = (): NastavnikDTO => {
        return this.rektorDTO;
    }

    public setRektorDTO = (rektorDTO: NastavnikDTO): void => {
        this.rektorDTO = rektorDTO;
    }
    
    public getStudentskaSluzbaDTO = (): StudentskaSluzbaDTO => {
        return this.studentskaSluzbaDTO;
    }

    public setStudentskaSluzbaDTO = (studentskaSluzbaDTO: StudentskaSluzbaDTO): void => {
        this.studentskaSluzbaDTO = studentskaSluzbaDTO;
    }
    
    public getKontaktiDTO = (): KontaktDTO[] => {
        return this.kontaktiDTO;
    }

    public setKontaktiDTO = (kontaktiDTO: KontaktDTO[]): void => {
        this.kontaktiDTO = kontaktiDTO;
    }
    

    public constructor () {
        this.adreseDTO = [];
        this.fakultetiDTO = [];
        this.kalendariDTO = [];
        this.kontaktiDTO = [];
    }

    public getAdresaDTO (identifikacija: Identifikacija): AdresaDTO {
	throw new Error("Not Implmented");
    }

    public getFakultetDTO (identifikacija: Identifikacija): FakultetDTO {
	throw new Error("Not Implmented");
    }

    public getKalendarDTO (identifikacija: Identifikacija): KalendarDTO {
	throw new Error("Not Implmented");
    }


}
