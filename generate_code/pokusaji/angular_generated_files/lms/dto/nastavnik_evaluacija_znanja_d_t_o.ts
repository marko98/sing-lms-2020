
export class NastavnikEvaluacijaZnanjaDTO implements DTO {

    private id: number;
        
    private datum: Date;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private evaluacijaZnanjaDTO: EvaluacijaZnanjaDTO;
        
    private nastavnikDTO: NastavnikDTO;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getDatum = (): Date => {
        return this.datum;
    }

    public setDatum = (datum: Date): void => {
        this.datum = datum;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getEvaluacijaZnanjaDTO = (): EvaluacijaZnanjaDTO => {
        return this.evaluacijaZnanjaDTO;
    }

    public setEvaluacijaZnanjaDTO = (evaluacijaZnanjaDTO: EvaluacijaZnanjaDTO): void => {
        this.evaluacijaZnanjaDTO = evaluacijaZnanjaDTO;
    }
    
    public getNastavnikDTO = (): NastavnikDTO => {
        return this.nastavnikDTO;
    }

    public setNastavnikDTO = (nastavnikDTO: NastavnikDTO): void => {
        this.nastavnikDTO = nastavnikDTO;
    }
    

    public constructor () {
    }


}
