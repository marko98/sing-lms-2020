
export class NastavniMaterijalDTO implements DTO {

    private id: number;
        
    private naziv: string;
        
    private datum: Date;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private ishodDTO: IshodDTO;
        
    private fajloviDTO: FajlDTO[];
        
    private autoriNastavniMaterijalDTO: AutorNastavniMaterijalDTO[];
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getNaziv = (): string => {
        return this.naziv;
    }

    public setNaziv = (naziv: string): void => {
        this.naziv = naziv;
    }
    
    public getDatum = (): Date => {
        return this.datum;
    }

    public setDatum = (datum: Date): void => {
        this.datum = datum;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getIshodDTO = (): IshodDTO => {
        return this.ishodDTO;
    }

    public setIshodDTO = (ishodDTO: IshodDTO): void => {
        this.ishodDTO = ishodDTO;
    }
    
    public getFajloviDTO = (): FajlDTO[] => {
        return this.fajloviDTO;
    }

    public setFajloviDTO = (fajloviDTO: FajlDTO[]): void => {
        this.fajloviDTO = fajloviDTO;
    }
    
    public getAutoriNastavniMaterijalDTO = (): AutorNastavniMaterijalDTO[] => {
        return this.autoriNastavniMaterijalDTO;
    }

    public setAutoriNastavniMaterijalDTO = (autoriNastavniMaterijalDTO: AutorNastavniMaterijalDTO[]): void => {
        this.autoriNastavniMaterijalDTO = autoriNastavniMaterijalDTO;
    }
    

    public constructor () {
        this.fajloviDTO = [];
        this.autoriNastavniMaterijalDTO = [];
    }

    public getFajlDTO (identifikacija: Identifikacija): FajlDTO {
	throw new Error("Not Implmented");
    }

    public getAutorNastavniMaterijalDTO (identifikacija: Identifikacija): AutorNastavniMaterijalDTO {
	throw new Error("Not Implmented");
    }


}
