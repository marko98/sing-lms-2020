
export class DrugiOblikNastaveDTO implements DTO {

    private id: number;
        
    private datum: Date;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private tipDrugogOblikaNastave: TipDrugogOblikaNastave;
        
    private realizacijaPredmetaDTO: RealizacijaPredmetaDTO;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getDatum = (): Date => {
        return this.datum;
    }

    public setDatum = (datum: Date): void => {
        this.datum = datum;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getTipDrugogOblikaNastave = (): TipDrugogOblikaNastave => {
        return this.tipDrugogOblikaNastave;
    }

    public setTipDrugogOblikaNastave = (tipDrugogOblikaNastave: TipDrugogOblikaNastave): void => {
        this.tipDrugogOblikaNastave = tipDrugogOblikaNastave;
    }
    
    public getRealizacijaPredmetaDTO = (): RealizacijaPredmetaDTO => {
        return this.realizacijaPredmetaDTO;
    }

    public setRealizacijaPredmetaDTO = (realizacijaPredmetaDTO: RealizacijaPredmetaDTO): void => {
        this.realizacijaPredmetaDTO = realizacijaPredmetaDTO;
    }
    

    public constructor () {
    }


}
