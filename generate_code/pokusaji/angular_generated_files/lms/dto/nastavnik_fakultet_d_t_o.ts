
export class NastavnikFakultetDTO implements DTO {

    private id: number;
        
    private stanje: StanjeModela;
        
    private datum: Date;
        
    private version: number;
        
    private fakultetDTO: FakultetDTO;
        
    private nastavnikDTO: NastavnikDTO;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getDatum = (): Date => {
        return this.datum;
    }

    public setDatum = (datum: Date): void => {
        this.datum = datum;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getFakultetDTO = (): FakultetDTO => {
        return this.fakultetDTO;
    }

    public setFakultetDTO = (fakultetDTO: FakultetDTO): void => {
        this.fakultetDTO = fakultetDTO;
    }
    
    public getNastavnikDTO = (): NastavnikDTO => {
        return this.nastavnikDTO;
    }

    public setNastavnikDTO = (nastavnikDTO: NastavnikDTO): void => {
        this.nastavnikDTO = nastavnikDTO;
    }
    

    public constructor () {
    }


}
