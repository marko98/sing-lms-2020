import { AutorNastavniMaterijal } from './autor_nastavni_materijal.model';

export class Autor implements Entitet {

    private id: number;
        
    private ime: string;
        
    private prezime: string;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private autorNastavniMaterijali: AutorNastavniMaterijal[];
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getIme = (): string => {
        return this.ime;
    }

    public setIme = (ime: string): void => {
        this.ime = ime;
    }
    
    public getPrezime = (): string => {
        return this.prezime;
    }

    public setPrezime = (prezime: string): void => {
        this.prezime = prezime;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getAutorNastavniMaterijali = (): AutorNastavniMaterijal[] => {
        return this.autorNastavniMaterijali;
    }

    public setAutorNastavniMaterijali = (autorNastavniMaterijali: AutorNastavniMaterijal[]): void => {
        this.autorNastavniMaterijali = autorNastavniMaterijali;
    }
    

    public constructor () {
        this.autorNastavniMaterijali = [];
    }

    public getAutorNastavniMaterijal (identifikacija: Identifikacija): AutorNastavniMaterijal {
	throw new Error("Not Implmented");
    }


}
