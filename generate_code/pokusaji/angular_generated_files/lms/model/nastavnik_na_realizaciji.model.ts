import { RealizacijaPredmeta } from './realizacija_predmeta.model';
import { Zvanje } from './zvanje.enum';
import { NastavnikNaRealizacijiTipNastave } from './nastavnik_na_realizaciji_tip_nastave.model';
import { Nastavnik } from './nastavnik.model';

export class NastavnikNaRealizaciji implements Entitet {

    private id: number;
        
    private brojCasova: number;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private realizacijaPredmeta: RealizacijaPredmeta;
        
    private zvanje: Zvanje;
        
    private nastavnikNaRealizacijiTipoviNastave: NastavnikNaRealizacijiTipNastave[];
        
    private nastavnik: Nastavnik;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getBrojCasova = (): number => {
        return this.brojCasova;
    }

    public setBrojCasova = (brojCasova: number): void => {
        this.brojCasova = brojCasova;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getRealizacijaPredmeta = (): RealizacijaPredmeta => {
        return this.realizacijaPredmeta;
    }

    public setRealizacijaPredmeta = (realizacijaPredmeta: RealizacijaPredmeta): void => {
        this.realizacijaPredmeta = realizacijaPredmeta;
    }
    
    public getZvanje = (): Zvanje => {
        return this.zvanje;
    }

    public setZvanje = (zvanje: Zvanje): void => {
        this.zvanje = zvanje;
    }
    
    public getNastavnikNaRealizacijiTipoviNastave = (): NastavnikNaRealizacijiTipNastave[] => {
        return this.nastavnikNaRealizacijiTipoviNastave;
    }

    public setNastavnikNaRealizacijiTipoviNastave = (nastavnikNaRealizacijiTipoviNastave: NastavnikNaRealizacijiTipNastave[]): void => {
        this.nastavnikNaRealizacijiTipoviNastave = nastavnikNaRealizacijiTipoviNastave;
    }
    
    public getNastavnik = (): Nastavnik => {
        return this.nastavnik;
    }

    public setNastavnik = (nastavnik: Nastavnik): void => {
        this.nastavnik = nastavnik;
    }
    

    public constructor () {
        this.nastavnikNaRealizacijiTipoviNastave = [];
    }

    public getNastavnikNaRealizacijiTipNastave (identifikacija: Identifikacija): NastavnikNaRealizacijiTipNastave {
	throw new Error("Not Implmented");
    }


}
