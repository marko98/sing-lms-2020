import { Odeljenje } from './odeljenje.model';
import { Univerzitet } from './univerzitet.model';
import { Nastavnik } from './nastavnik.model';
import { NastavnikFakultet } from './nastavnik_fakultet.model';
import { StudijskiProgram } from './studijski_program.model';
import { Kontakt } from './kontakt.model';

export class Fakultet implements Entitet {

    private id: number;
        
    private naziv: string;
        
    private opis: string;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private odeljenja: Odeljenje[];
        
    private univerzitet: Univerzitet;
        
    private dekan: Nastavnik;
        
    private nastavniciFakultet: NastavnikFakultet[];
        
    private studijskiProgrami: StudijskiProgram[];
        
    private kontakti: Kontakt[];
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getNaziv = (): string => {
        return this.naziv;
    }

    public setNaziv = (naziv: string): void => {
        this.naziv = naziv;
    }
    
    public getOpis = (): string => {
        return this.opis;
    }

    public setOpis = (opis: string): void => {
        this.opis = opis;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getOdeljenja = (): Odeljenje[] => {
        return this.odeljenja;
    }

    public setOdeljenja = (odeljenja: Odeljenje[]): void => {
        this.odeljenja = odeljenja;
    }
    
    public getUniverzitet = (): Univerzitet => {
        return this.univerzitet;
    }

    public setUniverzitet = (univerzitet: Univerzitet): void => {
        this.univerzitet = univerzitet;
    }
    
    public getDekan = (): Nastavnik => {
        return this.dekan;
    }

    public setDekan = (dekan: Nastavnik): void => {
        this.dekan = dekan;
    }
    
    public getNastavniciFakultet = (): NastavnikFakultet[] => {
        return this.nastavniciFakultet;
    }

    public setNastavniciFakultet = (nastavniciFakultet: NastavnikFakultet[]): void => {
        this.nastavniciFakultet = nastavniciFakultet;
    }
    
    public getStudijskiProgrami = (): StudijskiProgram[] => {
        return this.studijskiProgrami;
    }

    public setStudijskiProgrami = (studijskiProgrami: StudijskiProgram[]): void => {
        this.studijskiProgrami = studijskiProgrami;
    }
    
    public getKontakti = (): Kontakt[] => {
        return this.kontakti;
    }

    public setKontakti = (kontakti: Kontakt[]): void => {
        this.kontakti = kontakti;
    }
    

    public constructor () {
        this.odeljenja = [];
        this.nastavniciFakultet = [];
        this.studijskiProgrami = [];
        this.kontakti = [];
    }

    public getOdeljenje (identifikacija: Identifikacija): Odeljenje {
	throw new Error("Not Implmented");
    }

    public getStudijskiProgram (identifikacija: Identifikacija): StudijskiProgram {
	throw new Error("Not Implmented");
    }

    public getNastavnikFakultet (identifikacija: Identifikacija): NastavnikFakultet {
	throw new Error("Not Implmented");
    }

    public getKontakt (identifikacija: Identifikacija): Kontakt {
	throw new Error("Not Implmented");
    }


}
