import { PohadjanjePredmeta } from './pohadjanje_predmeta.model';
import { EvaluacijaZnanja } from './evaluacija_znanja.model';

export class Polaganje implements Entitet {

    private id: number;
        
    private bodovi: number;
        
    private napomena: string;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private pohadjanjePredmeta: PohadjanjePredmeta;
        
    private evaluacijaZnanja: EvaluacijaZnanja;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getBodovi = (): number => {
        return this.bodovi;
    }

    public setBodovi = (bodovi: number): void => {
        this.bodovi = bodovi;
    }
    
    public getNapomena = (): string => {
        return this.napomena;
    }

    public setNapomena = (napomena: string): void => {
        this.napomena = napomena;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getPohadjanjePredmeta = (): PohadjanjePredmeta => {
        return this.pohadjanjePredmeta;
    }

    public setPohadjanjePredmeta = (pohadjanjePredmeta: PohadjanjePredmeta): void => {
        this.pohadjanjePredmeta = pohadjanjePredmeta;
    }
    
    public getEvaluacijaZnanja = (): EvaluacijaZnanja => {
        return this.evaluacijaZnanja;
    }

    public setEvaluacijaZnanja = (evaluacijaZnanja: EvaluacijaZnanja): void => {
        this.evaluacijaZnanja = evaluacijaZnanja;
    }
    

    public constructor () {
    }


}
