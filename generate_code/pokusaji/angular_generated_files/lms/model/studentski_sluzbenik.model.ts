import { RegistrovaniKorisnik } from './registrovani_korisnik.model';
import { StudentskaSluzba } from './studentska_sluzba.model';

export class StudentskiSluzbenik implements Entitet {

    private id: number;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private registrovaniKorisnik: RegistrovaniKorisnik;
        
    private studentskaSluzba: StudentskaSluzba;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getRegistrovaniKorisnik = (): RegistrovaniKorisnik => {
        return this.registrovaniKorisnik;
    }

    public setRegistrovaniKorisnik = (registrovaniKorisnik: RegistrovaniKorisnik): void => {
        this.registrovaniKorisnik = registrovaniKorisnik;
    }
    
    public getStudentskaSluzba = (): StudentskaSluzba => {
        return this.studentskaSluzba;
    }

    public setStudentskaSluzba = (studentskaSluzba: StudentskaSluzba): void => {
        this.studentskaSluzba = studentskaSluzba;
    }
    

    public constructor () {
    }


}
