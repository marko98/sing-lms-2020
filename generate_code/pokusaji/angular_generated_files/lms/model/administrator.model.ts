import { RegistrovaniKorisnik } from './registrovani_korisnik.model';

export class Administrator implements Entitet {

    private id: number;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private registrovaniKorisnik: RegistrovaniKorisnik;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getRegistrovaniKorisnik = (): RegistrovaniKorisnik => {
        return this.registrovaniKorisnik;
    }

    public setRegistrovaniKorisnik = (registrovaniKorisnik: RegistrovaniKorisnik): void => {
        this.registrovaniKorisnik = registrovaniKorisnik;
    }
    

    public constructor () {
    }


}
