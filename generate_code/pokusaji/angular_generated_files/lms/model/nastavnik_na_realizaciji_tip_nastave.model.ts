import { NastavnikNaRealizaciji } from './nastavnik_na_realizaciji.model';
import { TipNastave } from './tip_nastave.enum';

export class NastavnikNaRealizacijiTipNastave implements Entitet {

    private id: number;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private nastavnikNaRealizaciji: NastavnikNaRealizaciji;
        
    private tipNastave: TipNastave;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getNastavnikNaRealizaciji = (): NastavnikNaRealizaciji => {
        return this.nastavnikNaRealizaciji;
    }

    public setNastavnikNaRealizaciji = (nastavnikNaRealizaciji: NastavnikNaRealizaciji): void => {
        this.nastavnikNaRealizaciji = nastavnikNaRealizaciji;
    }
    
    public getTipNastave = (): TipNastave => {
        return this.tipNastave;
    }

    public setTipNastave = (tipNastave: TipNastave): void => {
        this.tipNastave = tipNastave;
    }
    

    public constructor () {
    }


}
