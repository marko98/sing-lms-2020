import { NastavniMaterijal } from './nastavni_materijal.model';
import { Autor } from './autor.model';

export class AutorNastavniMaterijal implements Entitet {

    private id: number;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private nastavniMaterijal: NastavniMaterijal;
        
    private autor: Autor;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getNastavniMaterijal = (): NastavniMaterijal => {
        return this.nastavniMaterijal;
    }

    public setNastavniMaterijal = (nastavniMaterijal: NastavniMaterijal): void => {
        this.nastavniMaterijal = nastavniMaterijal;
    }
    
    public getAutor = (): Autor => {
        return this.autor;
    }

    public setAutor = (autor: Autor): void => {
        this.autor = autor;
    }
    

    public constructor () {
    }


}
