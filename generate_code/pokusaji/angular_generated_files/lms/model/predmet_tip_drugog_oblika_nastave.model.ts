import { Predmet } from './predmet.model';
import { TipDrugogOblikaNastave } from './tip_drugog_oblika_nastave.enum';

export class PredmetTipDrugogOblikaNastave implements Entitet {

    private id: number;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private predmet: Predmet;
        
    private tipDrugogOblikaNastave: TipDrugogOblikaNastave;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getPredmet = (): Predmet => {
        return this.predmet;
    }

    public setPredmet = (predmet: Predmet): void => {
        this.predmet = predmet;
    }
    
    public getTipDrugogOblikaNastave = (): TipDrugogOblikaNastave => {
        return this.tipDrugogOblikaNastave;
    }

    public setTipDrugogOblikaNastave = (tipDrugogOblikaNastave: TipDrugogOblikaNastave): void => {
        this.tipDrugogOblikaNastave = tipDrugogOblikaNastave;
    }
    

    public constructor () {
    }


}
