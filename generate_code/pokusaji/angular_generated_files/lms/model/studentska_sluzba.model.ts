import { StudentskiSluzbenik } from './studentski_sluzbenik.model';
import { Univerzitet } from './univerzitet.model';
import { Odeljenje } from './odeljenje.model';

export class StudentskaSluzba implements Entitet {

    private id: number;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private studentskiSluzbenici: StudentskiSluzbenik[];
        
    private univerzitet: Univerzitet;
        
    private odeljenje: Odeljenje;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getStudentskiSluzbenici = (): StudentskiSluzbenik[] => {
        return this.studentskiSluzbenici;
    }

    public setStudentskiSluzbenici = (studentskiSluzbenici: StudentskiSluzbenik[]): void => {
        this.studentskiSluzbenici = studentskiSluzbenici;
    }
    
    public getUniverzitet = (): Univerzitet => {
        return this.univerzitet;
    }

    public setUniverzitet = (univerzitet: Univerzitet): void => {
        this.univerzitet = univerzitet;
    }
    
    public getOdeljenje = (): Odeljenje => {
        return this.odeljenje;
    }

    public setOdeljenje = (odeljenje: Odeljenje): void => {
        this.odeljenje = odeljenje;
    }
    

    public constructor () {
        this.studentskiSluzbenici = [];
    }

    public getStudentskiSluzbenik (identifikacija: Identifikacija): StudentskiSluzbenik {
	throw new Error("Not Implmented");
    }


}
