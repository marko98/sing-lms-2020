import { Fakultet } from './fakultet.model';
import { Nastavnik } from './nastavnik.model';
import { GodinaStudija } from './godina_studija.model';

export class StudijskiProgram implements Entitet {

    private id: number;
        
    private naziv: string;
        
    private brojGodinaStudija: number;
        
    private opis: string;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private fakultet: Fakultet;
        
    private rukovodilac: Nastavnik;
        
    private godineStudija: GodinaStudija[];
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getNaziv = (): string => {
        return this.naziv;
    }

    public setNaziv = (naziv: string): void => {
        this.naziv = naziv;
    }
    
    public getBrojGodinaStudija = (): number => {
        return this.brojGodinaStudija;
    }

    public setBrojGodinaStudija = (brojGodinaStudija: number): void => {
        this.brojGodinaStudija = brojGodinaStudija;
    }
    
    public getOpis = (): string => {
        return this.opis;
    }

    public setOpis = (opis: string): void => {
        this.opis = opis;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getFakultet = (): Fakultet => {
        return this.fakultet;
    }

    public setFakultet = (fakultet: Fakultet): void => {
        this.fakultet = fakultet;
    }
    
    public getRukovodilac = (): Nastavnik => {
        return this.rukovodilac;
    }

    public setRukovodilac = (rukovodilac: Nastavnik): void => {
        this.rukovodilac = rukovodilac;
    }
    
    public getGodineStudija = (): GodinaStudija[] => {
        return this.godineStudija;
    }

    public setGodineStudija = (godineStudija: GodinaStudija[]): void => {
        this.godineStudija = godineStudija;
    }
    

    public constructor () {
        this.godineStudija = [];
    }

    public getGodinaStudija (identifikacija: Identifikacija): GodinaStudija {
	throw new Error("Not Implmented");
    }


}
