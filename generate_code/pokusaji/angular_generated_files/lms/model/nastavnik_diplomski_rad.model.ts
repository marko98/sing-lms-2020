import { Nastavnik } from './nastavnik.model';
import { DiplomskiRad } from './diplomski_rad.model';

export class NastavnikDiplomskiRad implements Entitet {

    private id: number;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private nastavnikUKomisiji: Nastavnik;
        
    private diplomskiRad: DiplomskiRad;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getNastavnikUKomisiji = (): Nastavnik => {
        return this.nastavnikUKomisiji;
    }

    public setNastavnikUKomisiji = (nastavnikUKomisiji: Nastavnik): void => {
        this.nastavnikUKomisiji = nastavnikUKomisiji;
    }
    
    public getDiplomskiRad = (): DiplomskiRad => {
        return this.diplomskiRad;
    }

    public setDiplomskiRad = (diplomskiRad: DiplomskiRad): void => {
        this.diplomskiRad = diplomskiRad;
    }
    

    public constructor () {
    }


}
