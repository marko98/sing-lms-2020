import { Konsultacija } from './konsultacija.model';
import { Dan } from './dan.enum';
import { TerminNastave } from './termin_nastave.model';

export class VremeOdrzavanjaUNedelji implements Entitet {

    private id: number;
        
    private vreme: Time;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private konsultacija: Konsultacija;
        
    private dan: Dan;
        
    private terminNastave: TerminNastave;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getVreme = (): Time => {
        return this.vreme;
    }

    public setVreme = (vreme: Time): void => {
        this.vreme = vreme;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getKonsultacija = (): Konsultacija => {
        return this.konsultacija;
    }

    public setKonsultacija = (konsultacija: Konsultacija): void => {
        this.konsultacija = konsultacija;
    }
    
    public getDan = (): Dan => {
        return this.dan;
    }

    public setDan = (dan: Dan): void => {
        this.dan = dan;
    }
    
    public getTerminNastave = (): TerminNastave => {
        return this.terminNastave;
    }

    public setTerminNastave = (terminNastave: TerminNastave): void => {
        this.terminNastave = terminNastave;
    }
    

    public constructor () {
    }


}
