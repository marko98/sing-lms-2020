import { GodinaStudija } from './godina_studija.model';
import { Silabus } from './silabus.model';
import { PredmetTipDrugogOblikaNastave } from './predmet_tip_drugog_oblika_nastave.model';
import { PredmetTipNastave } from './predmet_tip_nastave.model';
import { PredmetUslovniPredmet } from './predmet_uslovni_predmet.model';
import { PredmetUslovniPredmet } from './predmet_uslovni_predmet.model';
import { RealizacijaPredmeta } from './realizacija_predmeta.model';

export class Predmet implements Entitet {

    private id: number;
        
    private naziv: string;
        
    private obavezan: boolean;
        
    private brojPredavanja: number;
        
    private brojVezbi: number;
        
    private brojCasovaZaIstrazivackeRadove: number;
        
    private espb: number;
        
    private trajanjeUSemestrima: number;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private godinaStudija: GodinaStudija;
        
    private silabus: Silabus;
        
    private predmetTipoviDrugogOblikaNastave: PredmetTipDrugogOblikaNastave[];
        
    private predmetTipoviNastave: PredmetTipNastave[];
        
    private predmetiZaKojeSamUslov: PredmetUslovniPredmet[];
        
    private uslovniPredmeti: PredmetUslovniPredmet[];
        
    private realizacijaPredmeta: RealizacijaPredmeta;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getNaziv = (): string => {
        return this.naziv;
    }

    public setNaziv = (naziv: string): void => {
        this.naziv = naziv;
    }
    
    public getObavezan = (): boolean => {
        return this.obavezan;
    }

    public setObavezan = (obavezan: boolean): void => {
        this.obavezan = obavezan;
    }
    
    public getBrojPredavanja = (): number => {
        return this.brojPredavanja;
    }

    public setBrojPredavanja = (brojPredavanja: number): void => {
        this.brojPredavanja = brojPredavanja;
    }
    
    public getBrojVezbi = (): number => {
        return this.brojVezbi;
    }

    public setBrojVezbi = (brojVezbi: number): void => {
        this.brojVezbi = brojVezbi;
    }
    
    public getBrojCasovaZaIstrazivackeRadove = (): number => {
        return this.brojCasovaZaIstrazivackeRadove;
    }

    public setBrojCasovaZaIstrazivackeRadove = (brojCasovaZaIstrazivackeRadove: number): void => {
        this.brojCasovaZaIstrazivackeRadove = brojCasovaZaIstrazivackeRadove;
    }
    
    public getEspb = (): number => {
        return this.espb;
    }

    public setEspb = (espb: number): void => {
        this.espb = espb;
    }
    
    public getTrajanjeUSemestrima = (): number => {
        return this.trajanjeUSemestrima;
    }

    public setTrajanjeUSemestrima = (trajanjeUSemestrima: number): void => {
        this.trajanjeUSemestrima = trajanjeUSemestrima;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getGodinaStudija = (): GodinaStudija => {
        return this.godinaStudija;
    }

    public setGodinaStudija = (godinaStudija: GodinaStudija): void => {
        this.godinaStudija = godinaStudija;
    }
    
    public getSilabus = (): Silabus => {
        return this.silabus;
    }

    public setSilabus = (silabus: Silabus): void => {
        this.silabus = silabus;
    }
    
    public getPredmetTipoviDrugogOblikaNastave = (): PredmetTipDrugogOblikaNastave[] => {
        return this.predmetTipoviDrugogOblikaNastave;
    }

    public setPredmetTipoviDrugogOblikaNastave = (predmetTipoviDrugogOblikaNastave: PredmetTipDrugogOblikaNastave[]): void => {
        this.predmetTipoviDrugogOblikaNastave = predmetTipoviDrugogOblikaNastave;
    }
    
    public getPredmetTipoviNastave = (): PredmetTipNastave[] => {
        return this.predmetTipoviNastave;
    }

    public setPredmetTipoviNastave = (predmetTipoviNastave: PredmetTipNastave[]): void => {
        this.predmetTipoviNastave = predmetTipoviNastave;
    }
    
    public getPredmetiZaKojeSamUslov = (): PredmetUslovniPredmet[] => {
        return this.predmetiZaKojeSamUslov;
    }

    public setPredmetiZaKojeSamUslov = (predmetiZaKojeSamUslov: PredmetUslovniPredmet[]): void => {
        this.predmetiZaKojeSamUslov = predmetiZaKojeSamUslov;
    }
    
    public getUslovniPredmeti = (): PredmetUslovniPredmet[] => {
        return this.uslovniPredmeti;
    }

    public setUslovniPredmeti = (uslovniPredmeti: PredmetUslovniPredmet[]): void => {
        this.uslovniPredmeti = uslovniPredmeti;
    }
    
    public getRealizacijaPredmeta = (): RealizacijaPredmeta => {
        return this.realizacijaPredmeta;
    }

    public setRealizacijaPredmeta = (realizacijaPredmeta: RealizacijaPredmeta): void => {
        this.realizacijaPredmeta = realizacijaPredmeta;
    }
    

    public constructor () {
        this.predmetTipoviDrugogOblikaNastave = [];
        this.predmetTipoviNastave = [];
        this.predmetiZaKojeSamUslov = [];
        this.uslovniPredmeti = [];
    }

    public getPreduslov (identifikacija: Identifikacija): Predmet {
	throw new Error("Not Implmented");
    }

    public getPredmetTipNastave (identifikacija: Identifikacija): PredmetTipNastave {
	throw new Error("Not Implmented");
    }

    public getPredmetTipDrugogOblikaNastave (identifikacija: Identifikacija): PredmetTipDrugogOblikaNastave {
	throw new Error("Not Implmented");
    }

    public getPredmetZaKojiSamUslov (identifikacija: Identifikacija): PredmetUslovniPredmet {
	throw new Error("Not Implmented");
    }

    public getUslovniPredmet (identifikacija: Identifikacija): PredmetUslovniPredmet {
	throw new Error("Not Implmented");
    }


}
