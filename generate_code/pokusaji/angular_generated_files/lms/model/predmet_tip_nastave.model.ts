import { Predmet } from './predmet.model';
import { TipNastave } from './tip_nastave.enum';

export class PredmetTipNastave implements Entitet {

    private id: number;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private predmet: Predmet;
        
    private tipNastave: TipNastave;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getPredmet = (): Predmet => {
        return this.predmet;
    }

    public setPredmet = (predmet: Predmet): void => {
        this.predmet = predmet;
    }
    
    public getTipNastave = (): TipNastave => {
        return this.tipNastave;
    }

    public setTipNastave = (tipNastave: TipNastave): void => {
        this.tipNastave = tipNastave;
    }
    

    public constructor () {
    }


}
