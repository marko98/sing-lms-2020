import { PohadjanjePredmeta } from './pohadjanje_predmeta.model';

export class DatumPohadjanjaPredmeta implements Entitet {

    private id: number;
        
    private stanje: StanjeModela;
        
    private datum: Date;
        
    private version: number;
        
    private pohadjanjePredmeta: PohadjanjePredmeta;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getDatum = (): Date => {
        return this.datum;
    }

    public setDatum = (datum: Date): void => {
        this.datum = datum;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getPohadjanjePredmeta = (): PohadjanjePredmeta => {
        return this.pohadjanjePredmeta;
    }

    public setPohadjanjePredmeta = (pohadjanjePredmeta: PohadjanjePredmeta): void => {
        this.pohadjanjePredmeta = pohadjanjePredmeta;
    }
    

    public constructor () {
    }


}
