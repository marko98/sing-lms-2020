import { TipProstorije } from './tip_prostorije.enum';
import { TerminNastave } from './termin_nastave.model';
import { Odeljenje } from './odeljenje.model';

export class Prostorija implements Entitet {

    private id: number;
        
    private naziv: string;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private tip: TipProstorije;
        
    private terminiNastave: TerminNastave[];
        
    private odeljenje: Odeljenje;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getNaziv = (): string => {
        return this.naziv;
    }

    public setNaziv = (naziv: string): void => {
        this.naziv = naziv;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getTip = (): TipProstorije => {
        return this.tip;
    }

    public setTip = (tip: TipProstorije): void => {
        this.tip = tip;
    }
    
    public getTerminiNastave = (): TerminNastave[] => {
        return this.terminiNastave;
    }

    public setTerminiNastave = (terminiNastave: TerminNastave[]): void => {
        this.terminiNastave = terminiNastave;
    }
    
    public getOdeljenje = (): Odeljenje => {
        return this.odeljenje;
    }

    public setOdeljenje = (odeljenje: Odeljenje): void => {
        this.odeljenje = odeljenje;
    }
    

    public constructor () {
        this.terminiNastave = [];
    }


}
