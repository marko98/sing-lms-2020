import { Grad } from './grad.model';

export class Drzava implements Entitet {

    private id: number;
        
    private naziv: string;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private gradovi: Grad[];
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getNaziv = (): string => {
        return this.naziv;
    }

    public setNaziv = (naziv: string): void => {
        this.naziv = naziv;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getGradovi = (): Grad[] => {
        return this.gradovi;
    }

    public setGradovi = (gradovi: Grad[]): void => {
        this.gradovi = gradovi;
    }
    

    public constructor () {
        this.gradovi = [];
    }


}
