import { EvaluacijaZnanja } from './evaluacija_znanja.model';
import { NacinEvaluacije } from './nacin_evaluacije.enum';

export class EvaluacijaZnanjaNacinEvaluacije implements Entitet {

    private id: number;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private evaluacijaZnanja: EvaluacijaZnanja;
        
    private nacinEvaluacije: NacinEvaluacije;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getEvaluacijaZnanja = (): EvaluacijaZnanja => {
        return this.evaluacijaZnanja;
    }

    public setEvaluacijaZnanja = (evaluacijaZnanja: EvaluacijaZnanja): void => {
        this.evaluacijaZnanja = evaluacijaZnanja;
    }
    
    public getNacinEvaluacije = (): NacinEvaluacije => {
        return this.nacinEvaluacije;
    }

    public setNacinEvaluacije = (nacinEvaluacije: NacinEvaluacije): void => {
        this.nacinEvaluacije = nacinEvaluacije;
    }
    

    public constructor () {
    }


}
