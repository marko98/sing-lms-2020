import { Fakultet } from './fakultet.model';
import { Nastavnik } from './nastavnik.model';

export class NastavnikFakultet implements Entitet {

    private id: number;
        
    private stanje: StanjeModela;
        
    private datum: Date;
        
    private version: number;
        
    private fakultet: Fakultet;
        
    private nastavnik: Nastavnik;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getDatum = (): Date => {
        return this.datum;
    }

    public setDatum = (datum: Date): void => {
        this.datum = datum;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getFakultet = (): Fakultet => {
        return this.fakultet;
    }

    public setFakultet = (fakultet: Fakultet): void => {
        this.fakultet = fakultet;
    }
    
    public getNastavnik = (): Nastavnik => {
        return this.nastavnik;
    }

    public setNastavnik = (nastavnik: Nastavnik): void => {
        this.nastavnik = nastavnik;
    }
    

    public constructor () {
    }


}
