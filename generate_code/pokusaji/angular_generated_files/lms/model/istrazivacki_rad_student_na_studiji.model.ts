import { IstrazivackiRad } from './istrazivacki_rad.model';
import { StudentNaStudiji } from './student_na_studiji.model';

export class IstrazivackiRadStudentNaStudiji implements Entitet {

    private id: number;
        
    private datum: Date;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private istrazivackiRad: IstrazivackiRad;
        
    private studentNaStudiji: StudentNaStudiji;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getDatum = (): Date => {
        return this.datum;
    }

    public setDatum = (datum: Date): void => {
        this.datum = datum;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getIstrazivackiRad = (): IstrazivackiRad => {
        return this.istrazivackiRad;
    }

    public setIstrazivackiRad = (istrazivackiRad: IstrazivackiRad): void => {
        this.istrazivackiRad = istrazivackiRad;
    }
    
    public getStudentNaStudiji = (): StudentNaStudiji => {
        return this.studentNaStudiji;
    }

    public setStudentNaStudiji = (studentNaStudiji: StudentNaStudiji): void => {
        this.studentNaStudiji = studentNaStudiji;
    }
    

    public constructor () {
    }


}
