import { Predmet } from './predmet.model';
import { Predmet } from './predmet.model';

export class PredmetUslovniPredmet implements Entitet {

    private id: number;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private uslov: Predmet;
        
    private predmet: Predmet;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getUslov = (): Predmet => {
        return this.uslov;
    }

    public setUslov = (uslov: Predmet): void => {
        this.uslov = uslov;
    }
    
    public getPredmet = (): Predmet => {
        return this.predmet;
    }

    public setPredmet = (predmet: Predmet): void => {
        this.predmet = predmet;
    }
    

    public constructor () {
    }


}
