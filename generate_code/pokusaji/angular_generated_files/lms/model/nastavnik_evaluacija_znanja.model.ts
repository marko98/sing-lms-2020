import { EvaluacijaZnanja } from './evaluacija_znanja.model';
import { Nastavnik } from './nastavnik.model';

export class NastavnikEvaluacijaZnanja implements Entitet {

    private id: number;
        
    private datum: Date;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private evaluacijaZnanja: EvaluacijaZnanja;
        
    private nastavnik: Nastavnik;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getDatum = (): Date => {
        return this.datum;
    }

    public setDatum = (datum: Date): void => {
        this.datum = datum;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getEvaluacijaZnanja = (): EvaluacijaZnanja => {
        return this.evaluacijaZnanja;
    }

    public setEvaluacijaZnanja = (evaluacijaZnanja: EvaluacijaZnanja): void => {
        this.evaluacijaZnanja = evaluacijaZnanja;
    }
    
    public getNastavnik = (): Nastavnik => {
        return this.nastavnik;
    }

    public setNastavnik = (nastavnik: Nastavnik): void => {
        this.nastavnik = nastavnik;
    }
    

    public constructor () {
    }


}
