import { GodinaStudijaObavestenje } from './godina_studija_obavestenje.model';
import { Fajl } from './fajl.model';

export class Obavestenje implements Entitet {

    private id: number;
        
    private naslov: string;
        
    private datum: Date;
        
    private sadrzaj: string;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private godineStudijaObavestenje: GodinaStudijaObavestenje[];
        
    private fajlovi: Fajl[];
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getNaslov = (): string => {
        return this.naslov;
    }

    public setNaslov = (naslov: string): void => {
        this.naslov = naslov;
    }
    
    public getDatum = (): Date => {
        return this.datum;
    }

    public setDatum = (datum: Date): void => {
        this.datum = datum;
    }
    
    public getSadrzaj = (): string => {
        return this.sadrzaj;
    }

    public setSadrzaj = (sadrzaj: string): void => {
        this.sadrzaj = sadrzaj;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getGodineStudijaObavestenje = (): GodinaStudijaObavestenje[] => {
        return this.godineStudijaObavestenje;
    }

    public setGodineStudijaObavestenje = (godineStudijaObavestenje: GodinaStudijaObavestenje[]): void => {
        this.godineStudijaObavestenje = godineStudijaObavestenje;
    }
    
    public getFajlovi = (): Fajl[] => {
        return this.fajlovi;
    }

    public setFajlovi = (fajlovi: Fajl[]): void => {
        this.fajlovi = fajlovi;
    }
    

    public constructor () {
        this.godineStudijaObavestenje = [];
        this.fajlovi = [];
    }

    public getGodinaStudijaObavestenje (identifikacija: Identifikacija): GodinaStudijaObavestenje {
	throw new Error("Not Implmented");
    }

    public getFajl (identifikacija: Identifikacija): Fajl {
	throw new Error("Not Implmented");
    }


}
