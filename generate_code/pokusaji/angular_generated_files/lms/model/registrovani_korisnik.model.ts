import { Adresa } from './adresa.model';
import { Nastavnik } from './nastavnik.model';
import { Administrator } from './administrator.model';
import { StudentskiSluzbenik } from './studentski_sluzbenik.model';
import { Student } from './student.model';

export class RegistrovaniKorisnik implements Entitet {

    private id: number;
        
    private korisnickoIme: string;
        
    private lozinka: string;
        
    private email: string;
        
    private datumRodjenja: Date;
        
    private jmbg: string;
        
    private ime: string;
        
    private prezime: string;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private adrese: Adresa[];
        
    private nastavnik: Nastavnik;
        
    private administrator: Administrator;
        
    private studentskiSluzbenik: StudentskiSluzbenik;
        
    private student: Student;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getKorisnickoIme = (): string => {
        return this.korisnickoIme;
    }

    public setKorisnickoIme = (korisnickoIme: string): void => {
        this.korisnickoIme = korisnickoIme;
    }
    
    public getLozinka = (): string => {
        return this.lozinka;
    }

    public setLozinka = (lozinka: string): void => {
        this.lozinka = lozinka;
    }
    
    public getEmail = (): string => {
        return this.email;
    }

    public setEmail = (email: string): void => {
        this.email = email;
    }
    
    public getDatumRodjenja = (): Date => {
        return this.datumRodjenja;
    }

    public setDatumRodjenja = (datumRodjenja: Date): void => {
        this.datumRodjenja = datumRodjenja;
    }
    
    public getJmbg = (): string => {
        return this.jmbg;
    }

    public setJmbg = (jmbg: string): void => {
        this.jmbg = jmbg;
    }
    
    public getIme = (): string => {
        return this.ime;
    }

    public setIme = (ime: string): void => {
        this.ime = ime;
    }
    
    public getPrezime = (): string => {
        return this.prezime;
    }

    public setPrezime = (prezime: string): void => {
        this.prezime = prezime;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getAdrese = (): Adresa[] => {
        return this.adrese;
    }

    public setAdrese = (adrese: Adresa[]): void => {
        this.adrese = adrese;
    }
    
    public getNastavnik = (): Nastavnik => {
        return this.nastavnik;
    }

    public setNastavnik = (nastavnik: Nastavnik): void => {
        this.nastavnik = nastavnik;
    }
    
    public getAdministrator = (): Administrator => {
        return this.administrator;
    }

    public setAdministrator = (administrator: Administrator): void => {
        this.administrator = administrator;
    }
    
    public getStudentskiSluzbenik = (): StudentskiSluzbenik => {
        return this.studentskiSluzbenik;
    }

    public setStudentskiSluzbenik = (studentskiSluzbenik: StudentskiSluzbenik): void => {
        this.studentskiSluzbenik = studentskiSluzbenik;
    }
    
    public getStudent = (): Student => {
        return this.student;
    }

    public setStudent = (student: Student): void => {
        this.student = student;
    }
    

    public constructor () {
        this.adrese = [];
    }


}
