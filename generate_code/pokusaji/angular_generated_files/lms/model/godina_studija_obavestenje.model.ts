import { GodinaStudija } from './godina_studija.model';
import { Obavestenje } from './obavestenje.model';

export class GodinaStudijaObavestenje implements Entitet {

    private id: number;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private godinaStudija: GodinaStudija;
        
    private obavestenje: Obavestenje;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getGodinaStudija = (): GodinaStudija => {
        return this.godinaStudija;
    }

    public setGodinaStudija = (godinaStudija: GodinaStudija): void => {
        this.godinaStudija = godinaStudija;
    }
    
    public getObavestenje = (): Obavestenje => {
        return this.obavestenje;
    }

    public setObavestenje = (obavestenje: Obavestenje): void => {
        this.obavestenje = obavestenje;
    }
    

    public constructor () {
    }


}
