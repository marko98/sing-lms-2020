import { Kalendar } from './kalendar.model';
import { Dogadjaj } from './dogadjaj.model';

export class DogadjajKalendar implements Entitet {

    private id: number;
        
    private stanje: StanjeModela;
        
    private version: number;
        
    private kalendar: Kalendar;
        
    private dogadjaj: Dogadjaj;
        


    public getId = (): number => {
        return this.id;
    }

    public setId = (id: number): void => {
        this.id = id;
    }
    
    public getStanje = (): StanjeModela => {
        return this.stanje;
    }

    public setStanje = (stanje: StanjeModela): void => {
        this.stanje = stanje;
    }
    
    public getVersion = (): number => {
        return this.version;
    }

    public setVersion = (version: number): void => {
        this.version = version;
    }
    
    public getKalendar = (): Kalendar => {
        return this.kalendar;
    }

    public setKalendar = (kalendar: Kalendar): void => {
        this.kalendar = kalendar;
    }
    
    public getDogadjaj = (): Dogadjaj => {
        return this.dogadjaj;
    }

    public setDogadjaj = (dogadjaj: Dogadjaj): void => {
        this.dogadjaj = dogadjaj;
    }
    

    public constructor () {
    }


}
