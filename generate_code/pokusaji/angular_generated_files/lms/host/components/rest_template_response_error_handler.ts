
export class RestTemplateResponseErrorHandler implements ResponseErrorHandler {




    public hasError (httpResponse: ClientHttpResponse): boolean {
	return (httpResponse.getStatusCode().series() == Series.CLIENT_ERROR
		|| httpResponse.getStatusCode().series() == Series.SERVER_ERROR);
    }

    public handleError (httpResponse: ClientHttpResponse): void {

	if (httpResponse.getStatusCode().series() == HttpStatus.Series.SERVER_ERROR) {
	    // handle SERVER_ERROR
	    
//	    potencijalno mesto za log serverskih gresaka
	} else if (httpResponse.getStatusCode().series() == HttpStatus.Series.CLIENT_ERROR) {
	    // handle CLIENT_ERROR

//	    potencijalno mesto za log klijentskih gresaka
	}
    }


}
