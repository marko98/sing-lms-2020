
export class HostApp extends SpringBootServletInitializer {




    public static main (args[]: String): void {
	SpringApplication.run(HostApp.class, args);
    }

    protected configure (builder: SpringApplicationBuilder): SpringApplicationBuilder {
		return builder.sources(HostApp.class);
    }


}
