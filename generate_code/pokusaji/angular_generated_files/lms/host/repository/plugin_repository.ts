
export class PluginRepository {

    private pluginRepository: HashMap<String, ArrayList<Plugin>> = new HashMap<String, ArrayList<Plugin>>();
        


    public getPluginRepository = (): HashMap<String, ArrayList<Plugin>> => {
        return this.pluginRepository;
    }

    public setPluginRepository = (pluginRepository: HashMap<String, ArrayList<Plugin>>): void => {
        this.pluginRepository = pluginRepository;
    }
    

    public constructor () {
    }

    public registerPlugin (pluginDescription: PluginDescription): void {
	for (String category : pluginDescription.getCategories()) {

	    if (pluginRepository.get(category) == null) {
		pluginRepository.put(category, new ArrayList<Plugin>());
	    }
	    Plugin plugin = new Plugin(pluginDescription);
	    pluginRepository.get(category).add(plugin);

	    System.out.println(
		    "Plugin: " + pluginDescription.getName() + " , kategorija: " + category + " - registrovan\n");

	}
    }

    public getPlugins (category: String): ArrayList<Plugin> {
	return pluginRepository.get(category);
    }


}
