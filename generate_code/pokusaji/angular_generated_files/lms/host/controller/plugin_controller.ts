
export class PluginController {

    private pr: PluginRepository;
        


    public getPr = (): PluginRepository => {
        return this.pr;
    }

    public setPr = (pr: PluginRepository): void => {
        this.pr = pr;
    }
    

    public registerPlugin (pluginDescription: PluginDescription;@RequestBody): ResponseEntity<Object> {
	pr.registerPlugin(pluginDescription);
	return new ResponseEntity<Object>(HttpStatus.OK);
    }


}
