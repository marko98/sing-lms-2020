
export abstract class HostCrudController<T extends Entitet<T, ID>, ID extends Serializable> {

    protected pr: PluginRepository;
        
    protected category: string;
        


    public getPr = (): PluginRepository => {
        return this.pr;
    }

    public setPr = (pr: PluginRepository): void => {
        this.pr = pr;
    }
    
    public getCategory = (): string => {
        return this.category;
    }

    public setCategory = (category: string): void => {
        this.category = category;
    }
    

    public findAll (pageable: Pageable, httpHeaders: HttpHeaders;@RequestHeader): ResponseEntity<?> {
	// http://localhost:8080/api/adresa?page=0&size=2&sort=tip,neki_drugi_property,desc&sort=id

	/*
	 * Zahtev za dobavljanje svih adresa delegira se prvom registrovanom pluginu u
	 * kategoriji address. Ovom pluginu se upucuje get zahtev na endpoint pod
	 * nazivom getAll.
	 */
	Plugin plugin = pr.getPlugins(this.category).get(0);
	return plugin.findAll(this.category, HttpMethod.GET, "findAll", pageable, httpHeaders);
    }

    public findOne (id: ID;@PathVariable("id"), httpHeaders: HttpHeaders;@RequestHeader): ResponseEntity<?> {

	Plugin plugin = pr.getPlugins(this.category).get(0);
	return plugin.findOne(this.category, HttpMethod.GET, "findOne", id.toString(), httpHeaders);
    }

    public create (model: T;@RequestBody, httpHeaders: HttpHeaders;@RequestHeader): ResponseEntity<?> {
	Plugin plugin = pr.getPlugins(this.category).get(0);
	return plugin.create(this.category, HttpMethod.POST, "create", model, httpHeaders);
    }

    public update (model: T;@RequestBody, httpHeaders: HttpHeaders;@RequestHeader): ResponseEntity<?> {
	Plugin plugin = pr.getPlugins(this.category).get(0);
	plugin.update(this.category, HttpMethod.PUT, "update", model, httpHeaders);
	return plugin.findOne(this.category, HttpMethod.GET, "findOne", model.getId().toString(), httpHeaders);
    }

    public delete (id: ID;@PathVariable("id"), httpHeaders: HttpHeaders;@RequestHeader): ResponseEntity<?> {
	Plugin plugin = pr.getPlugins(this.category).get(0);
	plugin.delete(this.category, HttpMethod.DELETE, "delete", id.toString(), httpHeaders);
	
//	tu zbog logickog brisanja ako je implementirano
	return plugin.findOne(this.category, HttpMethod.GET, "findOne", id.toString(), httpHeaders);
    }

    public delete (model: T;@RequestBody, httpHeaders: HttpHeaders;@RequestHeader): ResponseEntity<?> {
	Plugin plugin = pr.getPlugins(this.category).get(0);
	plugin.delete(this.category, HttpMethod.DELETE, "delete", model.getId().toString(), httpHeaders);
	
//	tu zbog logickog brisanja ako je implementirano
	return plugin.findOne(this.category, HttpMethod.GET, "findOne", model.getId().toString(), httpHeaders);
    }


}
