
export class HostAppConfiguration {




    public propertySourcesPlaceholderConfigurer (): PropertySourcesPlaceholderConfigurer {
	PropertySourcesPlaceholderConfigurer properties = new PropertySourcesPlaceholderConfigurer();
	properties.setLocation(new ClassPathResource("/lms/host/configuration/host.application.properties"));
	properties.setIgnoreResourceNotFound(false);
	return properties;
    }


}
