
export class HibernateSessionFactory {

    private hibernateFactory: SessionFactory;
        


    public getHibernateFactory = (): SessionFactory => {
        return this.hibernateFactory;
    }

    public setHibernateFactory = (hibernateFactory: SessionFactory): void => {
        this.hibernateFactory = hibernateFactory;
    }
    

    public getSession (): Session {
	Session session;
	try {
	    session = this.hibernateFactory.getCurrentSession();
	} catch (HibernateException e) {
	    session = this.hibernateFactory.openSession();
	}
	
	return session;
    }

    public setSessionFactory (factory: EntityManagerFactory): void {
      if(factory.unwrap(SessionFactory.class) == null){
        throw new NullPointerException("factory is not a hibernate factory");
      }
      this.hibernateFactory = factory.unwrap(SessionFactory.class);
    }

    public onDestroy (): void {
	this.hibernateFactory.close();
    }

    public closeSession (session: Session): boolean {
	if (session == null)
	    return false;
	session.getTransaction().commit();
	session.close();
	return true;
    }

    public getNewSession (): Session {
	return this.hibernateFactory.openSession();
    }


}
