
export abstract class CrudRepository<T extends Entitet, ID extends Serializable> {

    protected repository: PagingAndSortingRepository<T, ID>;
        


    public getRepository = (): PagingAndSortingRepository<T, ID> => {
        return this.repository;
    }

    public setRepository = (repository: PagingAndSortingRepository<T, ID>): void => {
        this.repository = repository;
    }
    

    public findAll (pageable: Pageable): Page<T> {
	return this.repository.findAll(pageable);
    }

    public findAll (): Iterable<T> {
	return this.repository.findAll();
    }

    public findOne (id: ID): Optional<T> {
	return this.repository.findById(id);
    }

    public save (model: T): void {
	this.repository.save(model);
    }

    public delete (id: ID): void {
	this.repository.deleteById(id);
    }

    public delete (model: T): void {
	this.repository.delete(model);
    }


}
