
export abstract class CrudController<T extends Entitet, ID extends Serializable> {

    protected service: CrudService;
        


    public getService = (): CrudService => {
        return this.service;
    }

    public setService = (service: CrudService): void => {
        this.service = service;
    }
    

    public findAll (pageable: Pageable): ResponseEntity<Page<DTO>> {
	System.out.println("Pageable: " + pageable); // Page request [number: 0, size 20, sort: UNSORTED]

//	get Page<T>
	Page<T> page = this.service.findAll(pageable);

//	map T to DTO and get Page<DTO>
	Page<DTO> pageDTO = page.map(t -> t.getDTO());

	return new ResponseEntity<Page<DTO>>(pageDTO, HttpStatus.OK);
    }

    public findOne (id: ID;@PathVariable("id")): ResponseEntity<?> {
	Optional<T> t = this.service.findOne(id);

	if (t.isPresent())
	    return new ResponseEntity<DTO>(t.get().getDTO(), HttpStatus.OK);
	else
	    return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
    }

    public create (model: T;@RequestBody): ResponseEntity<?> {
//	check for id
	if (model.getId() != null) {
//		id exists, lets try to find model with given id
	    Optional<T> t = this.service.findOne(model.getId());

	    if (t.isEmpty()) {
//			we didn't find model, lets create one and return DTO
		this.service.save(model);
		return new ResponseEntity<DTO>(model.getDTO(), HttpStatus.CREATED);
	    } else {
//			model is found, lets return HttpStatus.CONFLICT
		return new ResponseEntity<Void>(HttpStatus.CONFLICT);
	    }
	} else {
//		id doesn't exist, so model doesn't exist lets create one and return DTO
	    this.service.save(model);
	    return new ResponseEntity<DTO>(model.getDTO(), HttpStatus.CREATED);
	}
    }

    public update (model: T;@RequestBody): ResponseEntity<?> {
//	check for id
	if (model.getId() != null) {
//		id exists, lets try to find model
	    Optional<T> t = this.service.findOne(model.getId());

	    if (t.isPresent()) {
//			model is found, lets update it and return DTO
		t.get().update(model);
		this.service.save(t.get());
		return new ResponseEntity<DTO>(t.get().getDTO(), HttpStatus.OK);
	    } else {
//			we didn't find model, lets return HttpStatus.NOT_FOUND
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	    }
	} else {
//		id doesn't exists, let return HttpStatus.NOT_FOUND
	    return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
    }

    public delete (id: ID;@PathVariable("id")): ResponseEntity<?> {
//	try to find model
	Optional<T> t = this.service.findOne(id);

	if (t.isPresent()) {
//		model is found, lets delete it and return HttpStatus.OK
	    this.service.delete(t.get());
	    return new ResponseEntity<Void>(HttpStatus.OK);
	} else {
//		we didn't find model, lets return HttpStatus.NOT_FOUND
	    return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
    }

    public delete (model: T;@RequestBody): ResponseEntity<?> {
//	check for id
	if (model.getId() != null) {
//		id exists, lets try to find model
	    Optional<T> t = this.service.findOne(model.getId());

	    if (t.isPresent()) {
//			model is found, lets delete it and return HttpStatus.OK
		this.service.delete(t.get());
		return new ResponseEntity<Void>(HttpStatus.OK);
	    } else {
//			we didn't find model, lets return HttpStatus.NOT_FOUND
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	    }
	} else {
//		id doesn't exists, let return HttpStatus.NOT_FOUND
	    return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
    }


}
