
export class CoreApp {




    public static main (args[]: String): void {
//	host
	HostApp.main(args);

//	plugins
	PluginAdresaApp.main(args);
	PluginAdministracijaApp.main(args);
	PluginEvaluacijaZnanjaApp.main(args);
	PluginFakultetApp.main(args);
	PluginKorisnikApp.main(args);
	PluginNastavaApp.main(args);
	PluginNastavnikApp.main(args);
	PluginObavestenjeApp.main(args);
	PluginPredmetApp.main(args);
	PluginStudentApp.main(args);
    }


}
