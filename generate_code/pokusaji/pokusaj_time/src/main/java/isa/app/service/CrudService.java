package isa.app.service;

import java.io.Serializable;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import isa.app.model.Entity;
import isa.app.repository.CrudRepository;

public abstract class CrudService<T extends Entity<T, ID>, ID extends Serializable> {

    @Autowired
    protected CrudRepository<T, ID> repository;

    public Page<T> findAll(Pageable pageable) {
	return this.repository.findAll(pageable);
    }

    public Iterable<T> findAll() {
	return this.repository.findAll();
    }

    public Optional<T> findOne(ID id) {
	return this.repository.findOne(id);
    }

    public void save(T model) {
	this.repository.save(model);
    }

    public void delete(ID id) {
	this.repository.delete(id);
    }

    public void delete(T model) {
	this.repository.delete(model);
    }

}