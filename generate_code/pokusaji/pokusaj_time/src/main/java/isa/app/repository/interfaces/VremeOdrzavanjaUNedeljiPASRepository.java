package isa.app.repository.interfaces;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import isa.app.model.VremeOdrzavanjaUNedelji;

@Repository
public interface VremeOdrzavanjaUNedeljiPASRepository
	extends PagingAndSortingRepository<VremeOdrzavanjaUNedelji, Long> {

}
