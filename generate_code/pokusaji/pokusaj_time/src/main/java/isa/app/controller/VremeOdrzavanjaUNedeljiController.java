package isa.app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import isa.app.model.VremeOdrzavanjaUNedelji;

@Controller
@RequestMapping("/api/vreme_odrzavanja_u_nedelji")
public class VremeOdrzavanjaUNedeljiController extends CrudController<VremeOdrzavanjaUNedelji, Long> {

}
