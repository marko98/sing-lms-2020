package isa.app.model;

import java.sql.Time;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import isa.app.dto.DTO;
import isa.app.dto.VremeOdrzavanjaUNedeljiDTO;

@Entity
public class VremeOdrzavanjaUNedelji implements isa.app.model.Entity<VremeOdrzavanjaUNedelji, Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    private Dan dan;
    
    private Time time;
    
    public VremeOdrzavanjaUNedelji () {}

    public VremeOdrzavanjaUNedelji(Dan dan, Time time) {
	super();
	this.dan = dan;
	this.time = time;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Dan getDan() {
        return dan;
    }

    public void setDan(Dan dan) {
        this.dan = dan;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }    
    
    @Override
    public DTO getDTO() {
        VremeOdrzavanjaUNedeljiDTO vremeOdrzavanjaUNedeljiDTO = new VremeOdrzavanjaUNedeljiDTO(id, dan, time);
        return vremeOdrzavanjaUNedeljiDTO;
    }
    
    @Override
    public DTO getDTOinsideDTO() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void update(VremeOdrzavanjaUNedelji entity) {
        // TODO Auto-generated method stub
        
    }
}
