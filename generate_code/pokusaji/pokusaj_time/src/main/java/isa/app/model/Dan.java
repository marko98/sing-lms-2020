package isa.app.model;

public enum Dan {
    PONEDELJAK,
    UTORAK,
    SREDA,
    CETVRTAK,
    PETAK,
    SUBOTA,
    NEDELJA
}
