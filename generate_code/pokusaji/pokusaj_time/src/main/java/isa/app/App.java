package isa.app;

import java.sql.Time;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jms.annotation.EnableJms;

@SpringBootApplication
@ComponentScan(basePackages = { "isa.app" })
@EntityScan(basePackages = { "isa.app.model" })
@EnableJpaRepositories("isa.app.repository")
@EnableJms
public class App extends SpringBootServletInitializer {

    public static void main(String args[]) {
	
	Time t = Time.valueOf("15:30:45");
	System.out.println(t);
	
	SpringApplication.run(App.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
	return builder.sources(App.class);
    }

}