package isa.app.repository;

import org.springframework.stereotype.Repository;

import isa.app.model.VremeOdrzavanjaUNedelji;

@Repository
public class VremeOdrzavanjaUNedeljiRepository extends CrudRepository<VremeOdrzavanjaUNedelji, Long> {

}
