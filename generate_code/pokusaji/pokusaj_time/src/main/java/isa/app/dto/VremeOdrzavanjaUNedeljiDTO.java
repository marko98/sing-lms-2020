package isa.app.dto;

import java.sql.Time;

import isa.app.model.Dan;

public class VremeOdrzavanjaUNedeljiDTO extends DTO {
    /**
     * 
     */
    private static final long serialVersionUID = 5963955277706406755L;

    private Long id;

    private Dan dan;

    private Time time;

    public VremeOdrzavanjaUNedeljiDTO() {
	super();
    }

    public VremeOdrzavanjaUNedeljiDTO(Long id, Dan dan, Time time) {
	super();
	this.id = id;
	this.dan = dan;
	this.time = time;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Dan getDan() {
	return dan;
    }

    public void setDan(Dan dan) {
	this.dan = dan;
    }

    public Time getTime() {
	return time;
    }

    public void setTime(Time time) {
	this.time = time;
    }

}
