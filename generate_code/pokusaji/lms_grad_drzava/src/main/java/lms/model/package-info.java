//@AnyMetaDef(name= "StudentMetaDef", metaType = "string", idType = "long",
//    metaValues = {
//            @MetaValue(value = "B", targetEntity = Boy.class),
//            @MetaValue(value = "G", targetEntity = Girl.class)
//    }
//)

@AnyMetaDef(name = "GradMetaDef", metaType = "string", idType = "long", metaValues = {
	@MetaValue(value = "GRAD", targetEntity = Grad.class) })

@AnyMetaDef(name = "DrzavaMetaDef", metaType = "string", idType = "long", metaValues = {
	@MetaValue(value = "DRZAVA", targetEntity = Drzava.class) })

package lms.model;

import org.hibernate.annotations.AnyMetaDef;
import org.hibernate.annotations.MetaValue;
