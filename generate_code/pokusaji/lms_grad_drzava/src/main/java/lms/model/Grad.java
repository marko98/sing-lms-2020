package lms.model;

import javax.persistence.Column;
import javax.persistence.JoinColumn;

import org.hibernate.annotations.Any;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lms.dto.GradDTO;
import lms.model.interfaces.DrzavaInterfejs;
import lms.model.interfaces.GradInterfejs;

@javax.persistence.Entity
@JsonIgnoreProperties({ "type" })
public class Grad implements GradInterfejs<Grad, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -8176727761313763932L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    private Long id;

    private String naziv;

//    @javax.persistence.ManyToOne
    @Any(metaDef = "DrzavaMetaDef", metaColumn = @Column(name = "drzava_type"))
    @JoinColumn(name = "drzava_id")
    private DrzavaInterfejs<?, ?> drzava;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getNaziv() {
	return this.naziv;
    }

    public void setNaziv(String naziv) {
	this.naziv = naziv;
    }

    public DrzavaInterfejs<?, ?> getDrzava() {
	return this.drzava;
    }

    public void setDrzava(DrzavaInterfejs<?, ?> drzava) {
	this.drzava = drzava;
    }

    public Grad() {
	super();
    }   

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((drzava == null) ? 0 : drzava.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((naziv == null) ? 0 : naziv.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Grad other = (Grad) obj;
	if (drzava == null) {
	    if (other.drzava != null)
		return false;
	} else if (!drzava.equals(other.drzava))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (naziv == null) {
	    if (other.naziv != null)
		return false;
	} else if (!naziv.equals(other.naziv))
	    return false;
	return true;
    }

    @Override
    public GradDTO getDTO() {
        GradDTO gradDTO = new GradDTO();
        gradDTO.setId(id);
        gradDTO.setNaziv(naziv);
        
        if(drzava != null)
            gradDTO.setDrzava(drzava.getDTOinsideDTO());
        
        return gradDTO;
    }
    
    @Override
    public GradDTO getDTOinsideDTO() {
	GradDTO gradDTO = new GradDTO();
        gradDTO.setId(id);
        gradDTO.setNaziv(naziv);
        
        return gradDTO;
    }
    
    @Override
    public void update(Grad entitet) {
        this.setNaziv(entitet.getNaziv());
        this.setDrzava(entitet.getDrzava());        
    }

}