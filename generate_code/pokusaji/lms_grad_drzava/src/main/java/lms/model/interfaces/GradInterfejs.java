package lms.model.interfaces;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import lms.model.Grad;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({ @Type(value = Grad.class, name = "grad")})
public interface GradInterfejs<T, ID extends Serializable> extends Entitet<T, ID> {

    public String getNaziv();

    public void setNaziv(String naziv);

    public DrzavaInterfejs<?, ?> getDrzava();

    public void setDrzava(DrzavaInterfejs<?, ?> drzava);

}