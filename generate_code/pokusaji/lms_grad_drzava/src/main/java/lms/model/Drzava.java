package lms.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;

import org.hibernate.annotations.ManyToAny;

import lms.dto.DrzavaDTO;
import lms.dto.interfaces.DTO;
import lms.interfaces.Identifikacija;
import lms.model.interfaces.DrzavaInterfejs;
import lms.model.interfaces.GradInterfejs;

@javax.persistence.Entity
public class Drzava implements DrzavaInterfejs<Drzava, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -1936133683717182670L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    private Long id;

    private String naziv;

    @ManyToAny(metaDef = "GradMetaDef", metaColumn = @Column(name = "grad_type"))
    @JoinTable(name = "drzava_grad", joinColumns = @JoinColumn(name = "drzava_id"), inverseJoinColumns = @JoinColumn(name = "grad_id"))
    private java.util.List<GradInterfejs<?, ?>> gradovi;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getNaziv() {
	return this.naziv;
    }

    public void setNaziv(String naziv) {
	this.naziv = naziv;
    }

    public java.util.List<GradInterfejs<?, ?>> getGradovi() {
	return this.gradovi;
    }

    public void setGradovi(java.util.List<GradInterfejs<?, ?>> gradovi) {
	this.gradovi = gradovi;
    }

    @Override
    public boolean addGrad(GradInterfejs<?, ?> grad) {
	if (grad.getId() != null) {
	    Iterator<GradInterfejs<?, ?>> iterator = gradovi.iterator();
	    while (iterator.hasNext()) {
		GradInterfejs<?, ?> g = iterator.next();
		if (grad.getId().equals(g)) {
//			grad vec postoji
		    return false;
		}
	    }
	}

	this.gradovi.add(grad);
	return true;
    }

    @Override
    public boolean removeGrad(Identifikacija<?> identifikacija) {
	Iterator<GradInterfejs<?, ?>> iterator = gradovi.iterator();
	while (iterator.hasNext()) {
	    GradInterfejs<?, ?> grad = iterator.next();
	    if (grad.getId().equals(identifikacija.getId())) {
		iterator.remove();
		return true;
	    }
	}
	return false;
    }

    @Override
    public GradInterfejs<?, ?> getGrad(Identifikacija<?> identifikacija) {
	Iterator<GradInterfejs<?, ?>> iterator = gradovi.iterator();
	while (iterator.hasNext()) {
	    GradInterfejs<?, ?> grad = iterator.next();
	    if (grad.getId().equals(identifikacija.getId())) {
		return grad;
	    }
	}
	return null;
    }

    public Drzava() {
	super();
	this.gradovi = new java.util.ArrayList<>();
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((gradovi == null) ? 0 : gradovi.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((naziv == null) ? 0 : naziv.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Drzava other = (Drzava) obj;
	if (gradovi == null) {
	    if (other.gradovi != null)
		return false;
	} else if (!gradovi.equals(other.gradovi))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (naziv == null) {
	    if (other.naziv != null)
		return false;
	} else if (!naziv.equals(other.naziv))
	    return false;
	return true;
    }

    @Override
    public DrzavaDTO getDTO() {
	DrzavaDTO drzavaDTO = new DrzavaDTO();
	drzavaDTO.setId(id);
	drzavaDTO.setNaziv(naziv);

	List<DTO<?>> gradoviDTO = new ArrayList<>();
	for (GradInterfejs<?, ?> grad : gradovi) {
	    gradoviDTO.add(grad.getDTOinsideDTO());
	}
	drzavaDTO.setGradovi(gradoviDTO);

	return drzavaDTO;
    }

    @Override
    public DrzavaDTO getDTOinsideDTO() {
	DrzavaDTO drzavaDTO = new DrzavaDTO();
	drzavaDTO.setId(id);
	drzavaDTO.setNaziv(naziv);

	return drzavaDTO;
    }

    @Override
    public void update(Drzava entitet) {
	this.setNaziv(entitet.getNaziv());
    }

}