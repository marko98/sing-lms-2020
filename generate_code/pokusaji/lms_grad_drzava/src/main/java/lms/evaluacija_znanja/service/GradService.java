package lms.evaluacija_znanja.service;

import lms.model.Grad;
import lms.service.CrudService;

import org.springframework.stereotype.Service;

@Service
public class GradService extends CrudService<Grad, Long> {

}
