package lms.evaluacija_znanja.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.Drzava;

@Controller
@RequestMapping(path = "/api/drzava")
public class DrzavaController extends CrudController<Drzava, Long> {

}
