package lms.evaluacija_znanja.repository.interfaces;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.Drzava;

@Repository
public interface DrzavaPASRepository extends PagingAndSortingRepository<Drzava, Long> {

}
