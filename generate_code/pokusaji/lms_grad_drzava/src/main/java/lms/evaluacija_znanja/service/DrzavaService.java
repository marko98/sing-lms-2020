package lms.evaluacija_znanja.service;

import org.springframework.stereotype.Service;

import lms.model.Drzava;
import lms.service.CrudService;

@Service
public class DrzavaService extends CrudService<Drzava, Long> {

}
