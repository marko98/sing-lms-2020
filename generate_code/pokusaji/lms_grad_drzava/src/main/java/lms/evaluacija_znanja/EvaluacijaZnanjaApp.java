package lms.evaluacija_znanja;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jms.annotation.EnableJms;

//@AnyMetaDefs({
//    @AnyMetaDef(name = "GradMetaDef", metaType = "string", idType="long", metaValues = {
//	    @MetaValue(value = "GRAD", targetEntity = Grad.class)
//    }),
//    @AnyMetaDef(name = "DrzavaMetaDef", metaType = "string", idType="long", metaValues = {
//	    @MetaValue(value = "DRZAVA", targetEntity = Drzava.class)
//    })
//})

@SpringBootApplication
@ComponentScan(basePackages = { "lms" })
@EntityScan(basePackages = { "lms.model" })
@EnableJpaRepositories("lms")
@EnableJms
public class EvaluacijaZnanjaApp extends SpringBootServletInitializer {

    public static void main(String args[]) {
	SpringApplication.run(EvaluacijaZnanjaApp.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
	return builder.sources(EvaluacijaZnanjaApp.class);
    }

}
