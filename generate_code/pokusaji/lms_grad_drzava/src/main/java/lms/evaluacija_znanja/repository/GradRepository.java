package lms.evaluacija_znanja.repository;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import lms.model.Grad;
import lms.model.interfaces.DrzavaInterfejs;
import lms.repository.CrudRepository;

@Repository
public class GradRepository extends CrudRepository<Grad, Long> {

    @Override
    public void save(Grad model) {

//	    get session
	Session session = this.hibernateSessionFactory.getSession();

//	    begin transaction
	session.beginTransaction();

//	    CREATE -> id ne postoji
	if (model.getId() == null) {

//		proveri da li je prosledjena drzava i da li postoji ako jeste
	    if (model.getDrzava() != null && model.getDrzava().getId() != null) {
		DrzavaInterfejs<?, ?> drzavaIzBaze = session.get(model.getDrzava().getClass(),
			model.getDrzava().getId());
		if (drzavaIzBaze != null) {
//			drzava postoji u bazi, posto se grad tek kreira save-uj ga i dodaj u drzavu
		    session.save(model);
		    drzavaIzBaze.addGrad(model);
		} else {
//			drzava ne postoji u bazi, ne mozemo je kreirati
//		    DrzavaInterfejs<?, ?> drzava = model.getDrzava();
//		    model.setDrzava(null);
//		    drzava.setId(null);
//		    session.save(drzava);
//
//		    model.setDrzava(drzava);
//		    session.save(model);
//
//		    drzava.addGrad(model);
//		    session.save(drzava);

		    model.setDrzava(null);
		    session.save(model);
		}
	    } else {
		this.repository.save(model);
	    }

	} else {
//		UPDATE -> id postoji
	    Grad gradIzBaze = session.get(Grad.class, model.getId());

//	    	0) model nema drzavu ni gradIzBaze nema drzavu -> sve ok
	    if (model.getDrzava() == null && gradIzBaze.getDrzava() == null) {
		gradIzBaze.update(model);
		session.save(gradIzBaze);
	    }
//		1) model nema drzavu, a gradIzBaze ima -> raskaciti grad od drzave
	    else if (model.getDrzava() == null && gradIzBaze.getDrzava() != null) {
		gradIzBaze.getDrzava().removeGrad(gradIzBaze);
//			session.save(gradIzBaze.getDrzava());
		gradIzBaze.update(model);
		session.save(gradIzBaze);
	    }
//		2) model ima drzavu, a gradIzBaze nema
	    else if (model.getDrzava() != null && gradIzBaze.getDrzava() == null) {
//		proveri da li drzava postoji u bazi
		DrzavaInterfejs<?, ?> drzavaIzBaze = session.get(model.getDrzava().getClass(),
			model.getDrzava().getId());
		if (drzavaIzBaze != null) {
//			drzava postoji u bazi
//			da li drzava ima ovaj grad
		    if (drzavaIzBaze.getGrad(model) != null) {
//			    ima ga, sve je ok
			gradIzBaze.update(model);
			session.save(gradIzBaze);
		    } else {
//			    nema ga, dodaj grad
			drzavaIzBaze.addGrad(model);
			session.save(drzavaIzBaze);

			gradIzBaze.update(model);
			session.save(gradIzBaze);
		    }
		} else {
//			drzava ne posotji u bazi ne mozemo je kreirati
		    model.setDrzava(null);
		    gradIzBaze.update(model);
		    session.save(gradIzBaze);
		}
	    }
//		3) model ima drzavu i gradIzBaze ima drzavu -> sve ok
	    else if (model.getDrzava() != null && gradIzBaze.getDrzava() != null) {
		gradIzBaze.update(model);
		session.save(gradIzBaze);
	    }
	}

//	    commit transaction
	session.getTransaction().commit();

//	    close session
	session.close();

    }

    @Override
    public void delete(Grad gradIzBaze) {

//	    get session
	Session session = this.hibernateSessionFactory.getSession();

//	    begin transaction
	session.beginTransaction();

	if (gradIzBaze.getDrzava() != null) {
//	    pripada drzavi

//	    proveri da li se nalazi medju njenim gradovima
	    DrzavaInterfejs<?, ?> drzava = gradIzBaze.getDrzava();
	    if (drzava.getGrad(gradIzBaze) != null) {
//		nalazi se obrisi je i update-uj grad
		drzava.removeGrad(gradIzBaze);

//		session.save(drzava); // treba ovako zbog sesija
	    } else {
//		ne nalazi se sve ok
	    }
	    ;
	} else {
//	    ne pripada drzavi sve ok
	}

	this.repository.delete(gradIzBaze);

//	    commit transaction
	session.getTransaction().commit();

//	    close session
	session.close();
    }

}
