package lms.evaluacija_znanja.repository;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import lms.interfaces.Identifikacija;
import lms.model.Drzava;
import lms.model.Grad;
import lms.model.interfaces.GradInterfejs;
import lms.repository.CrudRepository;

@Repository
public class DrzavaRepository extends CrudRepository<Drzava, Long> {

    @Override
    public void save(Drzava model) {
//	    get session
	Session session = this.hibernateSessionFactory.getSession();

//	    begin transaction
	session.beginTransaction();

//	id == null -> CREATE
	if (model.getId() == null) {
//		proveri da li ima gradove poslate uz sebe
	    for (GradInterfejs<?, ?> grad : model.getGradovi()) {

//		proveri da li grad postoji u bazi
		GradInterfejs<?, ?> gradIzBaze = session.get(grad.getClass(), grad.getId());
		if (gradIzBaze != null) {
//		    postoji u bazi

//			proveri da li pripada nekoj drzavi
		    if (gradIzBaze.getDrzava() != null) {
//			    nekome pripada
			gradIzBaze.getDrzava().removeGrad((Identifikacija<?>) gradIzBaze);
//			    razdvoji drzavu od grada
			session.save(gradIzBaze.getDrzava());
		    } else {
//			    nikome ne pripada sve ok
		    }

//		    postavi da grad pripada ovoj drzavi
		    gradIzBaze.setDrzava(model);
		    session.save(gradIzBaze);
		} else {
//		    ne postoji u bazi
		    grad.setDrzava(model);
//		    update-ovace mu i id
		    session.save(grad);
		}

	    }

	    this.repository.save(model);
	}
//	id != null -> UPDATE
	else {
//	    jos nije uradjeno
	}

//	    commit transaction
	session.getTransaction().commit();

//	    close session
	session.close();
    }

    @Override
    public void delete(Drzava drzavaIzBaze) {
//	    get session
	Session session = this.hibernateSessionFactory.getSession();

//	    begin transaction
	session.beginTransaction();

	for (GradInterfejs<?, ?> grad : drzavaIzBaze.getGradovi()) {
	    grad.setDrzava(null);
	}

	this.repository.delete(drzavaIzBaze);

//	    commit transaction
	session.getTransaction().commit();

//	    close session
	session.close();

    }

}
