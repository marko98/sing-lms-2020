package lms.hbsf;

import javax.annotation.PreDestroy;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import lms.model.Drzava;
import lms.model.Grad;

@Component
@Scope("singleton")
public class HibernateSessionFactory {
	private SessionFactory sessionFactory;

	public HibernateSessionFactory() {
		super();
		this.sessionFactory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Drzava.class)
				.addAnnotatedClass(Grad.class)
				.buildSessionFactory();
		
//		get session
		Session session = this.sessionFactory.getCurrentSession();
		
//		begin transaction
		session.beginTransaction();
		
//		-------------------- CRUD --------------------------------------------------
		
//		commit transaction
		session.getTransaction().commit();
		
//		close session
		session.close();
	}
	
	public Session getSession() {
		return this.sessionFactory.getCurrentSession();
	}
	
	@PreDestroy
	public void onDestroy() {
		System.out.println("HibernateSessionFactory bean destroyed");
		this.sessionFactory.close();
	}

}
