package lms.dto;

import lms.dto.interfaces.DTO;

public class GradDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -7671501016948388804L;

    private Long id;

    private String naziv;

    private DTO<?> drzava;

    public GradDTO(Long id, String naziv, DTO<?> drzava) {
	super();
	this.id = id;
	this.naziv = naziv;
	this.drzava = drzava;
    }

    public GradDTO() {
	super();
    }

    @Override
    public Long getId() {
	return this.id;
    }

    @Override
    public void setId(Long id) {
	this.id = id;

    }

    public String getNaziv() {
	return naziv;
    }

    public void setNaziv(String naziv) {
	this.naziv = naziv;
    }

    public DTO<?> getDrzava() {
	return drzava;
    }

    public void setDrzava(DTO<?> drzava) {
	this.drzava = drzava;
    }

}
