package lms.service;

import org.springframework.stereotype.Service;

import lms.model.College;

@Service
public class CollegeService extends CrudService<College, Long> {

}
