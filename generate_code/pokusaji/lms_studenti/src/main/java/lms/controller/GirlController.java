package lms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.model.Girl;

@Controller
@RequestMapping(path = "/api/girl")
public class GirlController extends CrudController<Girl, Long> {

}
