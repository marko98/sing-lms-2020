package lms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.model.Boy;

@Controller
@RequestMapping(path = "/api/boy")
public class BoyController extends CrudController<Boy, Long> {

}
