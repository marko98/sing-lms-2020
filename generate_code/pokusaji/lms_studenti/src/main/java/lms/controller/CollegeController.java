package lms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.model.College;

@Controller
@RequestMapping(path = "/api/college")
public class CollegeController extends CrudController<College, Long> {

}
