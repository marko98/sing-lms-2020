package lms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.model.Grad;

@Controller
@RequestMapping(path = "/api/grad")
public class GradController extends CrudController<Grad, Long> {

}
