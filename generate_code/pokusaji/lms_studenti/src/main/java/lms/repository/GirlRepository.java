package lms.repository;

import org.springframework.stereotype.Repository;

import lms.model.Girl;

@Repository
public class GirlRepository extends CrudRepository<Girl, Long> {

}
