package lms.repository;

import org.springframework.stereotype.Repository;

import lms.model.College;

@Repository
public class CollegeRepository extends CrudRepository<College, Long> {

}
