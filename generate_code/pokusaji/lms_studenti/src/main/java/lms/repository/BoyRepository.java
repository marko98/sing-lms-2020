package lms.repository;

import org.springframework.stereotype.Repository;

import lms.model.Boy;

@Repository
public class BoyRepository extends CrudRepository<Boy, Long> {

}
