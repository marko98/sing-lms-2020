package lms.repository.interfaces;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.Boy;

@Repository
public interface BoyPASRepository extends PagingAndSortingRepository<Boy, Long> {

}
