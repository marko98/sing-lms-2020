package lms.repository;

import org.springframework.stereotype.Repository;

import lms.model.Grad;

@Repository
public class GradRepository extends CrudRepository<Grad, Long> {

}
