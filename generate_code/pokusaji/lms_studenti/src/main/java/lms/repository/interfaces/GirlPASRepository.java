package lms.repository.interfaces;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.Girl;

@Repository
public interface GirlPASRepository extends PagingAndSortingRepository<Girl, Long> {

}
