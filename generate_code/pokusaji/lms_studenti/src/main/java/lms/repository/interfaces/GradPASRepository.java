package lms.repository.interfaces;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.Grad;

@Repository
public interface GradPASRepository extends PagingAndSortingRepository<Grad, Long> {

}
