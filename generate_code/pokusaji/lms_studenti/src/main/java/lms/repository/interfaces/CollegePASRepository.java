package lms.repository.interfaces;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.College;

@Repository
public interface CollegePASRepository extends PagingAndSortingRepository<College, Long> {

}
