package lms.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Table;

import org.hibernate.annotations.AnyMetaDef;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.ManyToAny;
import org.hibernate.annotations.MetaValue;

import lms.dto.interfaces.DTO;
import lms.model.interfaces.Entitet;
import lms.model.interfaces.Student;

@Entity
@Table(name = "college")
public class College implements Entitet<College, Long> {
    /**
     * 
     */
    private static final long serialVersionUID = 8041463353287916701L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name")
    private String name;
    
    
    @ManyToAny(metaDef = "StudentMetaDef", metaColumn = @Column(name = "student_gender"))
//    @Cascade({ org.hibernate.annotations.CascadeType.ALL })
//    @AnyMetaDef(name = "StudentMetaDef", metaType = "string", idType = "long", metaValues = {
//	    @MetaValue(value = "B", targetEntity = Boy.class), @MetaValue(value = "G", targetEntity = Girl.class) })
    @JoinTable(name = "college_students", joinColumns = @JoinColumn(name = "college_id"), inverseJoinColumns = @JoinColumn(name = "student_id"))
    private List<Student<?, ?>> students = new ArrayList<>();

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public List<Student<?, ?>> getStudents() {
	return students;
    }

    public void setStudents(List<Student<?, ?>> students) {
	this.students = students;
    }

    @Override
    public DTO<Long> getDTO() {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public DTO<Long> getDTOinsideDTO() {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public void update(College entitet) {
	// TODO Auto-generated method stub

    }

}
