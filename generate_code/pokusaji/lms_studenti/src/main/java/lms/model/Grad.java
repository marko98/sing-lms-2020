package lms.model;

import javax.persistence.Column;
import javax.persistence.JoinColumn;

import org.hibernate.annotations.Any;
import org.hibernate.annotations.AnyMetaDef;
import org.hibernate.annotations.MetaValue;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lms.evaluacija_znanja.dto.DrzavaDTO;
import lms.evaluacija_znanja.dto.GradDTO;
import lms.model.interfaces.DrzavaInterfejs;
import lms.model.interfaces.GradInterfejs;

@javax.persistence.Entity
@JsonIgnoreProperties({ "type" })
public class Grad implements GradInterfejs<Grad, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -8176727761313763932L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    private Long id;

    private String naziv;

//    @javax.persistence.ManyToOne
    @Any(metaDef = "DrzavaMetaDef", metaColumn = @Column(name = "drzava_type"))
    @AnyMetaDef(name = "DrzavaMetaDef", metaType = "string", idType="long", metaValues = {
	    @MetaValue(value = "DRZAVA", targetEntity = Drzava.class)
    })
    @JoinColumn(name = "drzava_id")
    private DrzavaInterfejs<?, ?> drzava;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getNaziv() {
	return this.naziv;
    }

    public void setNaziv(String naziv) {
	this.naziv = naziv;
    }

    public DrzavaInterfejs<?, ?> getDrzava() {
	return this.drzava;
    }

    public void setDrzava(DrzavaInterfejs<?, ?> drzava) {
	this.drzava = drzava;
    }

    public Grad() {
	super();
    }
    
    @Override
    public GradDTO getDTO() {
        GradDTO gradDTO = new GradDTO();
        gradDTO.setId(id);
        gradDTO.setNaziv(naziv);
        gradDTO.setDrzava((DrzavaDTO)drzava.getDTOinsideDTO());
        
        return gradDTO;
    }
    
    @Override
    public GradDTO getDTOinsideDTO() {
	GradDTO gradDTO = new GradDTO();
        gradDTO.setId(id);
        gradDTO.setNaziv(naziv);
        
        return gradDTO;
    }
    
    @Override
    public void update(Grad entitet) {
        // TODO Auto-generated method stub
        
    }

}