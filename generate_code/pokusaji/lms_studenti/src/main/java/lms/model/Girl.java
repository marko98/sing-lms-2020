package lms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lms.dto.interfaces.DTO;
import lms.model.interfaces.Student;

@Entity
@JsonIgnoreProperties({ "type" })
@Table(name = "girl")
public class Girl implements Student<Girl, Long> {
    /**
     * 
     */
    private static final long serialVersionUID = -7107879597850758746L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "age")
    private int age;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    @Override
    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    @Override
    public int getAge() {
	return age;
    }

    public void setAge(int age) {
	this.age = age;
    }

    @Override
    public DTO<Long> getDTO() {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public DTO<Long> getDTOinsideDTO() {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public void update(Girl entitet) {
	// TODO Auto-generated method stub

    }

}
