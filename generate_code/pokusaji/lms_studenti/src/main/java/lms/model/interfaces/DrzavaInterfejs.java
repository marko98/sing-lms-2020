package lms.model.interfaces;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import lms.interfaces.Identifikacija;
import lms.model.Drzava;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({ @Type(value = Drzava.class, name = "drzava") })
public interface DrzavaInterfejs<T, ID extends Serializable> extends Entitet<T, ID> {

    public void setNaziv(String naziv);

    public String getNaziv();

    public List<GradInterfejs<?, ?>> getGradovi();

    public void getGrad(Identifikacija<ID> identifikacija);

    public void setGradovi(List<GradInterfejs<?, ?>> gradovi);

    public boolean addGrad(GradInterfejs<?, ?> grad);

    public void removeGrad(Identifikacija<ID> identifikacija);

}