package lms.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;

import org.hibernate.annotations.AnyMetaDef;
import org.hibernate.annotations.ManyToAny;
import org.hibernate.annotations.MetaValue;

import lms.evaluacija_znanja.dto.DrzavaDTO;
import lms.evaluacija_znanja.dto.GradDTO;
import lms.interfaces.Identifikacija;
import lms.model.interfaces.DrzavaInterfejs;
import lms.model.interfaces.GradInterfejs;

@javax.persistence.Entity
public class Drzava implements DrzavaInterfejs<Drzava, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -1936133683717182670L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    private Long id;

    private String naziv;

    @ManyToAny(metaDef = "GradMetaDef", metaColumn = @Column(name = "grad_type"))
    @AnyMetaDef(name = "GradMetaDef", metaType = "string", idType = "long", metaValues = {
	    @MetaValue(value = "GRAD", targetEntity = Grad.class) })
    @JoinTable(name = "drzava_grad", joinColumns = @JoinColumn(name = "drzava_id"), inverseJoinColumns = @JoinColumn(name = "grad_id"))
    private java.util.List<GradInterfejs<?, ?>> gradovi;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getNaziv() {
	return this.naziv;
    }

    public void setNaziv(String naziv) {
	this.naziv = naziv;
    }

    public java.util.List<GradInterfejs<?, ?>> getGradovi() {
	return this.gradovi;
    }

    public void setGradovi(java.util.List<GradInterfejs<?, ?>> gradovi) {
	this.gradovi = gradovi;
    }

    @Override
    public boolean addGrad(GradInterfejs<?, ?> grad) {
	// TODO Auto-generated method stub
	return false;
    }

    @Override
    public void removeGrad(Identifikacija<Long> identifikacija) {
	// TODO Auto-generated method stub

    }

    @Override
    public void getGrad(Identifikacija<Long> identifikacija) {
	// TODO Auto-generated method stub

    }

    public Drzava() {
	super();
	this.gradovi = new java.util.ArrayList<>();
    }

    @Override
    public DrzavaDTO getDTO() {
	DrzavaDTO drzavaDTO = new DrzavaDTO();
	drzavaDTO.setId(id);
	drzavaDTO.setNaziv(naziv);
	
	List<GradDTO> gradoviDTO = new ArrayList<>(); 
	for (GradInterfejs<?, ?> grad : gradovi) {
	    gradoviDTO.add((GradDTO)grad.getDTOinsideDTO());
	}
	drzavaDTO.setGradovi(gradoviDTO);
	
	return drzavaDTO;
    }

    @Override
    public DrzavaDTO getDTOinsideDTO() {
	DrzavaDTO drzavaDTO = new DrzavaDTO();
	drzavaDTO.setId(id);
	drzavaDTO.setNaziv(naziv);
	
	return drzavaDTO;
    }

    @Override
    public void update(Drzava entitet) {
	// TODO Auto-generated method stub

    }

}