@AnyMetaDef(name= "StudentMetaDef", metaType = "string", idType = "long",
    metaValues = {
            @MetaValue(value = "B", targetEntity = Boy.class),
            @MetaValue(value = "G", targetEntity = Girl.class)
    }
)
package lms.model;

import org.hibernate.annotations.AnyMetaDef;
import org.hibernate.annotations.MetaValue;
