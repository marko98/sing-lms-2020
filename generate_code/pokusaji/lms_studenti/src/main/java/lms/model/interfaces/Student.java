package lms.model.interfaces;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import lms.model.Boy;
import lms.model.Girl;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({ @Type(value = Boy.class, name = "boy"), @Type(value = Girl.class, name = "girl") })
public interface Student<T, ID extends Serializable> extends Entitet<T, ID> {
    String getName();
    int getAge();
}
