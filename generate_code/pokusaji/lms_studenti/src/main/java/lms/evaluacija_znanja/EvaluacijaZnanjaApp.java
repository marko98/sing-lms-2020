package lms.evaluacija_znanja;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jms.annotation.EnableJms;

import lms.configuration.HibernateUtil;
import lms.model.Boy;
import lms.model.College;
import lms.model.Girl;
import lms.model.interfaces.Student;

//@AnyMetaDefs({
//    @AnyMetaDef(name = "GradMetaDef", metaType = "string", idType="long", metaValues = {
//	    @MetaValue(value = "GRAD", targetEntity = Grad.class)
//    }),
//    @AnyMetaDef(name = "DrzavaMetaDef", metaType = "string", idType="long", metaValues = {
//	    @MetaValue(value = "DRZAVA", targetEntity = Drzava.class)
//    })
//})

@SpringBootApplication
@ComponentScan(basePackages = { "lms" })
@EntityScan(basePackages = { "lms.model" })
@EnableJpaRepositories("lms.repository")
@EnableJms
public class EvaluacijaZnanjaApp extends SpringBootServletInitializer {

    public static void main(String args[]) {

//	Session session = HibernateUtil.getSessionFactory().openSession();
//	session.beginTransaction();
//	Boy boy = new Boy();
//	boy.setName("Mukesh");
//	boy.setAge(30);
//	session.persist(boy);
//
//	Girl girl = new Girl();
//	girl.setName("Neerja");
//	girl.setAge(25);
//	session.persist(girl);
//
//	College college = new College();
//	college.setName("ABCD");
//	List<Student> students = new ArrayList<>();
//	students.add(boy);
//	students.add(girl);
//	college.setStudents(students);
//	session.persist(college);
//
//	session.getTransaction().commit();
//
//	College col = (College) session.get(College.class, 1);
//	for (Student student : col.getStudents()) {
//	    System.out.println(student.getName() + ":" + student.getAge());
//	}
//	session.close();
//	HibernateUtil.closeSessionFactory();

	SpringApplication.run(EvaluacijaZnanjaApp.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
	return builder.sources(EvaluacijaZnanjaApp.class);
    }

}
