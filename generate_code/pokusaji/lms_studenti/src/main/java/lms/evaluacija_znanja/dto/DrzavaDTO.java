package lms.evaluacija_znanja.dto;

import java.util.List;

import lms.dto.interfaces.DrzavaDTOInterfejs;

public class DrzavaDTO implements DrzavaDTOInterfejs<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 8896412641855008940L;

    private Long id;

    private String naziv;

    private java.util.List<GradDTO> gradovi;

    public DrzavaDTO() {
	super();
    }

    public DrzavaDTO(Long id, String naziv, List<GradDTO> gradovi) {
	super();
	this.id = id;
	this.naziv = naziv;
	this.gradovi = gradovi;
    }

    @Override
    public Long getId() {
	return this.id;
    }

    @Override
    public void setId(Long id) {
	this.id = id;

    }

    public String getNaziv() {
	return naziv;
    }

    public void setNaziv(String naziv) {
	this.naziv = naziv;
    }

    public java.util.List<GradDTO> getGradovi() {
	return gradovi;
    }

    public void setGradovi(java.util.List<GradDTO> gradovi) {
	this.gradovi = gradovi;
    }

}
