package lms.evaluacija_znanja.dto;

import lms.dto.interfaces.GradDTOInterfejs;

public class GradDTO implements GradDTOInterfejs<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -7671501016948388804L;

    private Long id;

    private String naziv;

    private DrzavaDTO drzava;

    public GradDTO(Long id, String naziv, DrzavaDTO drzava) {
	super();
	this.id = id;
	this.naziv = naziv;
	this.drzava = drzava;
    }

    public GradDTO() {
	super();
    }

    @Override
    public Long getId() {
	return this.id;
    }

    @Override
    public void setId(Long id) {
	this.id = id;

    }

    public String getNaziv() {
	return naziv;
    }

    public void setNaziv(String naziv) {
	this.naziv = naziv;
    }

    public DrzavaDTO getDrzava() {
	return drzava;
    }

    public void setDrzava(DrzavaDTO drzava) {
	this.drzava = drzava;
    }

}
