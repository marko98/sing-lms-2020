package lms.dto.interfaces;

import lms.interfaces.Identifikacija;

public interface DTO<ID> extends Identifikacija<ID> {

}