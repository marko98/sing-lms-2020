package plugin.model;



public class PluginDescription {

    private String name;
        
    private List<String> categories;
        
    private String description;
        
    private String scheme;
        
    private String host;
        
    private HashMap<String, HashMap<HttpMethod, HashMap<String, String>>> endpoints = new HashMap<String, HashMap<HttpMethod, HashMap<String, String>>>();
        


    public String getName () {
        return this.name;
    }

    public void setName (String name) {
        this.name = name;
    }
    
    public List<String> getCategories () {
        return this.categories;
    }

    public void setCategories (List<String> categories) {
        this.categories = categories;
    }
    
    public String getDescription () {
        return this.description;
    }

    public void setDescription (String description) {
        this.description = description;
    }
    
    public String getScheme () {
        return this.scheme;
    }

    public void setScheme (String scheme) {
        this.scheme = scheme;
    }
    
    public String getHost () {
        return this.host;
    }

    public void setHost (String host) {
        this.host = host;
    }
    
    public HashMap<String, HashMap<HttpMethod, HashMap<String, String>>> getEndpoints () {
        return this.endpoints;
    }

    public void setEndpoints (HashMap<String, HashMap<HttpMethod, HashMap<String, String>>> endpoints) {
        this.endpoints = endpoints;
    }
    


    public PluginDescription () {
        super();
    }

    public PluginDescription () {
	super();
	this.name = name;
	this.categories = categories;
	this.description = description;
	this.scheme = scheme;
	this.host = host;
	this.endpoints = endpoints;
}

    public PluginDescription () {
	super();
	this.name = name;
	this.categories = new ArrayList<String>();
	this.description = description;
	this.scheme = scheme;
	this.host = host;
	this.endpoints = endpoints;
}

    public String getUrl () {
	return this.scheme + "://" + this.host;
    }

    public String getEndpointUrl (String category, HttpMethod method, String endpointName) {
	HashMap<String, String> existingEndpoitns = this.endpoints.get(category).get(method);
	if (existingEndpoitns != null) {
	    return existingEndpoitns.get(endpointName);
	}
	return null;
    }

}