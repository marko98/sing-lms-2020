package plugin.model;



public class Plugin {

    private PluginDescription description;
        


    public PluginDescription getDescription () {
        return this.description;
    }

    public void setDescription (PluginDescription description) {
        this.description = description;
    }
    


    public Plugin () {
        super();
    }

    public Plugin () {
	this.description = description;
}

    public ResponseEntity<?> findAll (String category, HttpMethod method, String targetEndpoint, Pageable pageable, HttpHeaders httpHeaders) {
	RestTemplate rt = this.getRestTemplateWithCustomErrorHandling();
	
	HttpEntity<Object> request = new HttpEntity<Object>(httpHeaders);
	
//	return rt.getForEntity(this.getUriString(category, method, targetEndpoint, "", pageable), Object.class);
	return rt.exchange(this.getUriString(category, method, targetEndpoint, "", pageable), HttpMethod.GET, request, Object.class);
    }

    public ResponseEntity<?> findOne (String category, HttpMethod method, String targetEndpoint, String id, HttpHeaders httpHeaders) {
	RestTemplate rt = this.getRestTemplateWithCustomErrorHandling();
	
	HttpEntity<Object> request = new HttpEntity<Object>(httpHeaders);

//	return rt.getForEntity(this.getUriString(category, method, targetEndpoint, id, null), Object.class);
	return rt.exchange(this.getUriString(category, method, targetEndpoint, id, null), HttpMethod.GET, request, Object.class);
    }

    public ResponseEntity<?> create (String category, HttpMethod method, String targetEndpoint, Entitet<?, ?> model, HttpHeaders httpHeaders) {
	RestTemplate rt = this.getRestTemplateWithCustomErrorHandling();
	
	/**
	 * pri instanciranju HttpEntity klase nudi izmedju ostalog konstruktor koji prima body i header-e
	 * */
	HttpEntity<Object> request = new HttpEntity<Object>(model, httpHeaders);

//	return rt.postForEntity(this.getUriString(category, method, targetEndpoint, "", null), model, Object.class);
	return rt.exchange(this.getUriString(category, method, targetEndpoint, "", null), HttpMethod.POST, request, Object.class);
    }

    public void update (String category, HttpMethod method, String targetEndpoint, Entitet<?, ?> model, HttpHeaders httpHeaders) {
	RestTemplate rt = this.getRestTemplateWithCustomErrorHandling();

	/**
	 * pri instanciranju HttpEntity klase nudi izmedju ostalog konstruktor koji prima body i header-e
	 * */
	HttpEntity<Object> request = new HttpEntity<Object>(model, httpHeaders);
	
//	rt.put(this.getUriString(category, method, targetEndpoint, "", null), model);
	rt.exchange(this.getUriString(category, method, targetEndpoint, "", null), HttpMethod.PUT, request, Object.class);
    }

    public void delete (String category, HttpMethod method, String targetEndpoint, String id, HttpHeaders httpHeaders) {
	RestTemplate rt = this.getRestTemplateWithCustomErrorHandling();

	HttpEntity<Object> request = new HttpEntity<Object>(httpHeaders);
	
//	rt.delete(this.getUriString(category, method, targetEndpoint, id, null));
	rt.exchange(this.getUriString(category, method, targetEndpoint, id, null), HttpMethod.DELETE, request, Object.class);
    }

    public RestTemplate getRestTemplateWithCustomErrorHandling () {
	RestTemplateBuilder builder = new RestTemplateBuilder();
	RestTemplate rt = builder.errorHandler(new RestTemplateResponseErrorHandler()).build();
	return rt;
    }

    public String getUriString (String category, HttpMethod method, String targetEndpoint, String id, Pageable pageable) {
	UriComponentsBuilder builder = UriComponentsBuilder.newInstance().scheme(description.getScheme())
		.host(description.getHost()).path(description.getEndpointUrl(category, method, targetEndpoint) + id);

	if (pageable != null) {
	    builder.queryParam("page", pageable.getPageNumber()).queryParam("size", pageable.getPageSize());

	    if (pageable.getSort().isSorted()) {
		Iterator<Sort.Order> iterator = pageable.getSort().iterator();
		while (iterator.hasNext()) {
		    Sort.Order sort = iterator.next();
		    String sortValue = sort.getProperty();
		    if (sort.isDescending())
			sortValue += ",desc";
		    else
			sortValue += ",asc";

//			System.out.println(sort.toString());
//			System.out.println(sort.getProperty());
//			System.out.println(sort.isDescending());

		    builder.queryParam("sort", sortValue);
		}
	    }
	}

	UriComponents uriComponents = builder.build();

	return uriComponents.toUriString();
    }

}