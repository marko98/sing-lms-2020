package lms.hbsf;


@Component
@Scope("singleton")

public class HibernateSessionFactory {

    private SessionFactory hibernateFactory;
        


    public SessionFactory getHibernateFactory () {
        return this.hibernateFactory;
    }

    public void setHibernateFactory (SessionFactory hibernateFactory) {
        this.hibernateFactory = hibernateFactory;
    }
    


    public Session getSession () {
	Session session;
	try {
	    session = this.hibernateFactory.getCurrentSession();
	} catch (HibernateException e) {
	    session = this.hibernateFactory.openSession();
	}
	
	return session;
    }

    @Autowired
    public void setSessionFactory (EntityManagerFactory factory) {
      if(factory.unwrap(SessionFactory.class) == null){
        throw new NullPointerException("factory is not a hibernate factory");
      }
      this.hibernateFactory = factory.unwrap(SessionFactory.class);
    }

    @PreDestroy
    public void onDestroy () {
	this.hibernateFactory.close();
    }

    public boolean closeSession (Session session) {
	if (session == null)
	    return false;
	session.getTransaction().commit();
	session.close();
	return true;
    }

    public Session getNewSession () {
	return this.hibernateFactory.openSession();
    }

}