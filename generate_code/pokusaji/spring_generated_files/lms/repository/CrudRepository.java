package lms.repository;


@Transactional


public abstract class CrudRepository<T extends Entitet, ID extends Serializable> {

    @Autowired
    protected PagingAndSortingRepository<T, ID> repository;
        


    public PagingAndSortingRepository<T, ID> getRepository () {
        return this.repository;
    }

    public void setRepository (PagingAndSortingRepository<T, ID> repository) {
        this.repository = repository;
    }
    


    public Page<T> findAll (Pageable pageable) {
	return this.repository.findAll(pageable);
    }

    public Iterable<T> findAll () {
	return this.repository.findAll();
    }

    public Optional<T> findOne (ID id) {
	return this.repository.findById(id);
    }

    public void save (T model) {
	this.repository.save(model);
    }

    public void delete (ID id) {
	this.repository.deleteById(id);
    }

    public void delete (T model) {
	this.repository.delete(model);
    }

}