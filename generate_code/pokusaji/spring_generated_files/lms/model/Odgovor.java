package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE odgovor SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "odgovor")
public class Odgovor implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "odgovor_id")
    private Long id;
        
    private String sadrzaj;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "pitanje_id")
    private Pitanje pitanje;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getSadrzaj () {
        return this.sadrzaj;
    }

    public void setSadrzaj (String sadrzaj) {
        this.sadrzaj = sadrzaj;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public Pitanje getPitanje () {
        return this.pitanje;
    }

    public void setPitanje (Pitanje pitanje) {
        this.pitanje = pitanje;
    }
    


    public Odgovor () {
        super();
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}