package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE diplomski_rad SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "diplomski_rad")
public class DiplomskiRad implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "diplomski_rad_id")
    private Long id;
        
    private String tema;
        
    private Integer ocena;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "nastavnik_id")
    private Nastavnik mentor;
        
    @javax.persistence.OneToMany(mappedBy="diplomskiRad")
    private java.util.List<NastavnikDiplomskiRad> nastavniciDiplomskiRad;
        
    @javax.persistence.OneToOne(mappedBy="diplomskiRad")
    private StudentNaStudiji studentNaStudiji;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getTema () {
        return this.tema;
    }

    public void setTema (String tema) {
        this.tema = tema;
    }
    
    public Integer getOcena () {
        return this.ocena;
    }

    public void setOcena (Integer ocena) {
        this.ocena = ocena;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public Nastavnik getMentor () {
        return this.mentor;
    }

    public void setMentor (Nastavnik mentor) {
        this.mentor = mentor;
    }
    
    public java.util.List<NastavnikDiplomskiRad> getNastavniciDiplomskiRad () {
        return this.nastavniciDiplomskiRad;
    }

    public void setNastavniciDiplomskiRad (java.util.List<NastavnikDiplomskiRad> nastavniciDiplomskiRad) {
        this.nastavniciDiplomskiRad = nastavniciDiplomskiRad;
    }
    
    public StudentNaStudiji getStudentNaStudiji () {
        return this.studentNaStudiji;
    }

    public void setStudentNaStudiji (StudentNaStudiji studentNaStudiji) {
        this.studentNaStudiji = studentNaStudiji;
    }
    


    public DiplomskiRad () {
        super();
        this.nastavniciDiplomskiRad = new java.util.ArrayList<>();
    }

    public NastavnikDiplomskiRad getNastavnikDiplomskiRad (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addNastavnikDiplomskiRad (NastavnikDiplomskiRad nastavnikDiplomskiRad) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeNastavnikDiplomskiRad (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}