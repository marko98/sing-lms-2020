package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE nastavnik_evaluacija_znanja SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "nastavnik_evaluacija_znanja")
public class NastavnikEvaluacijaZnanja implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "nastavnik_evaluacija_znanja_id")
    private Long id;
        
    private LocalDateTime datum;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "evaluacija_znanja_id")
    private EvaluacijaZnanja evaluacijaZnanja;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "nastavnik_id")
    private Nastavnik nastavnik;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public LocalDateTime getDatum () {
        return this.datum;
    }

    public void setDatum (LocalDateTime datum) {
        this.datum = datum;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public EvaluacijaZnanja getEvaluacijaZnanja () {
        return this.evaluacijaZnanja;
    }

    public void setEvaluacijaZnanja (EvaluacijaZnanja evaluacijaZnanja) {
        this.evaluacijaZnanja = evaluacijaZnanja;
    }
    
    public Nastavnik getNastavnik () {
        return this.nastavnik;
    }

    public void setNastavnik (Nastavnik nastavnik) {
        this.nastavnik = nastavnik;
    }
    


    public NastavnikEvaluacijaZnanja () {
        super();
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}