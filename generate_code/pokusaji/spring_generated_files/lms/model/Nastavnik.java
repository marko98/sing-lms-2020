package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE nastavnik SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "nastavnik")
public class Nastavnik implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "nastavnik_id")
    private Long id;
        
    private String biografija;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.OneToMany(mappedBy="rektor")
    private java.util.List<Univerzitet> univerziteti;
        
    @javax.persistence.OneToMany(mappedBy="dekan")
    private java.util.List<Fakultet> fakulteti;
        
    @javax.persistence.OneToMany(mappedBy="nastavnik")
    private java.util.List<NastavnikFakultet> nastavnikFakulteti;
        
    @javax.persistence.OneToMany(mappedBy="nastavnik")
    private java.util.List<TerminNastave> terminiNastave;
        
    @javax.persistence.OneToMany(mappedBy="rukovodilac")
    private java.util.List<StudijskiProgram> studijskiProgrami;
        
    @javax.persistence.OneToMany(mappedBy="nastavnik")
    private java.util.List<Titula> titule;
        
    @javax.persistence.OneToOne
    private RegistrovaniKorisnik registrovaniKorisnik;
        
    @javax.persistence.OneToMany(mappedBy="mentor")
    private java.util.List<DiplomskiRad> mentorDiplomskiRadovi;
        
    @javax.persistence.OneToMany(mappedBy="nastavnik")
    private java.util.List<IstrazivackiRad> istrazivackiRadovi;
        
    @javax.persistence.OneToMany(mappedBy="nastavnik")
    private java.util.List<Konsultacija> konsultacije;
        
    @javax.persistence.OneToMany(mappedBy="nastavnik")
    private java.util.List<NastavnikEvaluacijaZnanja> nastavnikEvaluacijeZnanja;
        
    @javax.persistence.OneToMany(mappedBy="nastavnikUKomisiji")
    private java.util.List<NastavnikDiplomskiRad> nastavnikDiplomskiRadovi;
        
    @javax.persistence.OneToMany(mappedBy="nastavnik")
    private java.util.List<NastavnikNaRealizaciji> nastavnikNaRealizacijama;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getBiografija () {
        return this.biografija;
    }

    public void setBiografija (String biografija) {
        this.biografija = biografija;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public java.util.List<Univerzitet> getUniverziteti () {
        return this.univerziteti;
    }

    public void setUniverziteti (java.util.List<Univerzitet> univerziteti) {
        this.univerziteti = univerziteti;
    }
    
    public java.util.List<Fakultet> getFakulteti () {
        return this.fakulteti;
    }

    public void setFakulteti (java.util.List<Fakultet> fakulteti) {
        this.fakulteti = fakulteti;
    }
    
    public java.util.List<NastavnikFakultet> getNastavnikFakulteti () {
        return this.nastavnikFakulteti;
    }

    public void setNastavnikFakulteti (java.util.List<NastavnikFakultet> nastavnikFakulteti) {
        this.nastavnikFakulteti = nastavnikFakulteti;
    }
    
    public java.util.List<TerminNastave> getTerminiNastave () {
        return this.terminiNastave;
    }

    public void setTerminiNastave (java.util.List<TerminNastave> terminiNastave) {
        this.terminiNastave = terminiNastave;
    }
    
    public java.util.List<StudijskiProgram> getStudijskiProgrami () {
        return this.studijskiProgrami;
    }

    public void setStudijskiProgrami (java.util.List<StudijskiProgram> studijskiProgrami) {
        this.studijskiProgrami = studijskiProgrami;
    }
    
    public java.util.List<Titula> getTitule () {
        return this.titule;
    }

    public void setTitule (java.util.List<Titula> titule) {
        this.titule = titule;
    }
    
    public RegistrovaniKorisnik getRegistrovaniKorisnik () {
        return this.registrovaniKorisnik;
    }

    public void setRegistrovaniKorisnik (RegistrovaniKorisnik registrovaniKorisnik) {
        this.registrovaniKorisnik = registrovaniKorisnik;
    }
    
    public java.util.List<DiplomskiRad> getMentorDiplomskiRadovi () {
        return this.mentorDiplomskiRadovi;
    }

    public void setMentorDiplomskiRadovi (java.util.List<DiplomskiRad> mentorDiplomskiRadovi) {
        this.mentorDiplomskiRadovi = mentorDiplomskiRadovi;
    }
    
    public java.util.List<IstrazivackiRad> getIstrazivackiRadovi () {
        return this.istrazivackiRadovi;
    }

    public void setIstrazivackiRadovi (java.util.List<IstrazivackiRad> istrazivackiRadovi) {
        this.istrazivackiRadovi = istrazivackiRadovi;
    }
    
    public java.util.List<Konsultacija> getKonsultacije () {
        return this.konsultacije;
    }

    public void setKonsultacije (java.util.List<Konsultacija> konsultacije) {
        this.konsultacije = konsultacije;
    }
    
    public java.util.List<NastavnikEvaluacijaZnanja> getNastavnikEvaluacijeZnanja () {
        return this.nastavnikEvaluacijeZnanja;
    }

    public void setNastavnikEvaluacijeZnanja (java.util.List<NastavnikEvaluacijaZnanja> nastavnikEvaluacijeZnanja) {
        this.nastavnikEvaluacijeZnanja = nastavnikEvaluacijeZnanja;
    }
    
    public java.util.List<NastavnikDiplomskiRad> getNastavnikDiplomskiRadovi () {
        return this.nastavnikDiplomskiRadovi;
    }

    public void setNastavnikDiplomskiRadovi (java.util.List<NastavnikDiplomskiRad> nastavnikDiplomskiRadovi) {
        this.nastavnikDiplomskiRadovi = nastavnikDiplomskiRadovi;
    }
    
    public java.util.List<NastavnikNaRealizaciji> getNastavnikNaRealizacijama () {
        return this.nastavnikNaRealizacijama;
    }

    public void setNastavnikNaRealizacijama (java.util.List<NastavnikNaRealizaciji> nastavnikNaRealizacijama) {
        this.nastavnikNaRealizacijama = nastavnikNaRealizacijama;
    }
    


    public Nastavnik () {
        super();
        this.univerziteti = new java.util.ArrayList<>();
        this.fakulteti = new java.util.ArrayList<>();
        this.nastavnikFakulteti = new java.util.ArrayList<>();
        this.terminiNastave = new java.util.ArrayList<>();
        this.studijskiProgrami = new java.util.ArrayList<>();
        this.titule = new java.util.ArrayList<>();
        this.mentorDiplomskiRadovi = new java.util.ArrayList<>();
        this.istrazivackiRadovi = new java.util.ArrayList<>();
        this.konsultacije = new java.util.ArrayList<>();
        this.nastavnikEvaluacijeZnanja = new java.util.ArrayList<>();
        this.nastavnikDiplomskiRadovi = new java.util.ArrayList<>();
        this.nastavnikNaRealizacijama = new java.util.ArrayList<>();
    }

    public Titula getTitula (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addTitula (Titula titula) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeTitula (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public NastavnikFakultet getNastavnikFakultet (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addNastavnikFakultet (NastavnikFakultet nastavnikFakultet) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeNastavnikFakultet (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public DiplomskiRad getMentorDiplomskiRad (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addMentorDiplomskiRad (DiplomskiRad mentorDiplomskiRad) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeMentorDiplomskiRad (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public IstrazivackiRad getIstrazivackiRad (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addIstrazivackiRad (IstrazivackiRad istrazivackiRad) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeIstrazivackiRad (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public Konsultacija getKonsultacija (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addKonsultacija (Konsultacija konsultacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeKonsultacija (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public NastavnikEvaluacijaZnanja getNastavnikEvaluacijaZnanja (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addNastavnikEvaluacijaZnanja (NastavnikEvaluacijaZnanja nastavnikEvaluacijaZnanja) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeNastavnikEvaluacijaZnanja (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public TerminNastave getTerminNastave (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addTerminNastave (TerminNastave terminNastave) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeTerminNastave (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public NastavnikDiplomskiRad getNastavnikDiplomskiRad (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addNastavnikDiplomskiRad (NastavnikDiplomskiRad nastavnikDiplomskiRad) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeNastavnikDiplomskiRad (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.NEAKTIVAN; // aktivira ga administrator
    }

    public NastavnikNaRealizaciji getNastavnikNaRealizaciji (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addNastavnikNaRealizaciji (NastavnikNaRealizaciji nastavnikNaRealizaciji) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeNastavnikNaRealizaciji (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

}