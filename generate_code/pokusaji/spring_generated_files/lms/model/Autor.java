package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE autor SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "autor")
public class Autor implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "autor_id")
    private Long id;
        
    private String ime;
        
    private String prezime;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.OneToMany(mappedBy="autor")
    private java.util.List<AutorNastavniMaterijal> autorNastavniMaterijali;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getIme () {
        return this.ime;
    }

    public void setIme (String ime) {
        this.ime = ime;
    }
    
    public String getPrezime () {
        return this.prezime;
    }

    public void setPrezime (String prezime) {
        this.prezime = prezime;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public java.util.List<AutorNastavniMaterijal> getAutorNastavniMaterijali () {
        return this.autorNastavniMaterijali;
    }

    public void setAutorNastavniMaterijali (java.util.List<AutorNastavniMaterijal> autorNastavniMaterijali) {
        this.autorNastavniMaterijali = autorNastavniMaterijali;
    }
    


    public Autor () {
        super();
        this.autorNastavniMaterijali = new java.util.ArrayList<>();
    }

    public AutorNastavniMaterijal getAutorNastavniMaterijal (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addAutorNastavniMaterijal (AutorNastavniMaterijal autorNastavniMaterijal) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeAutorNastavniMaterijal (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}