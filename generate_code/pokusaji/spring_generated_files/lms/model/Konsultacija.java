package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE konsultacija SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "konsultacija")
public class Konsultacija implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "konsultacija_id")
    private Long id;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "godina_studija_id")
    private GodinaStudija godinaStudija;
        
    @javax.persistence.OneToOne
    private VremeOdrzavanjaUNedelji vremeOdrzavanjaUNedelji;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "nastavnik_id")
    private Nastavnik nastavnik;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public GodinaStudija getGodinaStudija () {
        return this.godinaStudija;
    }

    public void setGodinaStudija (GodinaStudija godinaStudija) {
        this.godinaStudija = godinaStudija;
    }
    
    public VremeOdrzavanjaUNedelji getVremeOdrzavanjaUNedelji () {
        return this.vremeOdrzavanjaUNedelji;
    }

    public void setVremeOdrzavanjaUNedelji (VremeOdrzavanjaUNedelji vremeOdrzavanjaUNedelji) {
        this.vremeOdrzavanjaUNedelji = vremeOdrzavanjaUNedelji;
    }
    
    public Nastavnik getNastavnik () {
        return this.nastavnik;
    }

    public void setNastavnik (Nastavnik nastavnik) {
        this.nastavnik = nastavnik;
    }
    


    public Konsultacija () {
        super();
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}