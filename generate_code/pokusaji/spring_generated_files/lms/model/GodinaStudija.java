package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE godina_studija SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "godina_studija")
public class GodinaStudija implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "godina_studija_id")
    private Long id;
        
    private LocalDateTime godina;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.OneToMany(mappedBy="godinaStudija")
    private java.util.List<StudentNaStudiji> studentiNaStudiji;
        
    @Enumerated(EnumType.STRING)
    private Semestar semestar;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "studijski_program_id")
    private StudijskiProgram studijskiProgram;
        
    @javax.persistence.OneToMany(mappedBy="godinaStudija")
    private java.util.List<Konsultacija> konsultacije;
        
    @javax.persistence.OneToMany(mappedBy="godinaStudija")
    private java.util.List<GodinaStudijaObavestenje> godinaStudijaObavestenja;
        
    @javax.persistence.OneToMany(mappedBy="godinaStudija")
    private java.util.List<Predmet> predmeti;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public LocalDateTime getGodina () {
        return this.godina;
    }

    public void setGodina (LocalDateTime godina) {
        this.godina = godina;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public java.util.List<StudentNaStudiji> getStudentiNaStudiji () {
        return this.studentiNaStudiji;
    }

    public void setStudentiNaStudiji (java.util.List<StudentNaStudiji> studentiNaStudiji) {
        this.studentiNaStudiji = studentiNaStudiji;
    }
    
    public Semestar getSemestar () {
        return this.semestar;
    }

    public void setSemestar (Semestar semestar) {
        this.semestar = semestar;
    }
    
    public StudijskiProgram getStudijskiProgram () {
        return this.studijskiProgram;
    }

    public void setStudijskiProgram (StudijskiProgram studijskiProgram) {
        this.studijskiProgram = studijskiProgram;
    }
    
    public java.util.List<Konsultacija> getKonsultacije () {
        return this.konsultacije;
    }

    public void setKonsultacije (java.util.List<Konsultacija> konsultacije) {
        this.konsultacije = konsultacije;
    }
    
    public java.util.List<GodinaStudijaObavestenje> getGodinaStudijaObavestenja () {
        return this.godinaStudijaObavestenja;
    }

    public void setGodinaStudijaObavestenja (java.util.List<GodinaStudijaObavestenje> godinaStudijaObavestenja) {
        this.godinaStudijaObavestenja = godinaStudijaObavestenja;
    }
    
    public java.util.List<Predmet> getPredmeti () {
        return this.predmeti;
    }

    public void setPredmeti (java.util.List<Predmet> predmeti) {
        this.predmeti = predmeti;
    }
    


    public GodinaStudija () {
        super();
        this.studentiNaStudiji = new java.util.ArrayList<>();
        this.konsultacije = new java.util.ArrayList<>();
        this.godinaStudijaObavestenja = new java.util.ArrayList<>();
        this.predmeti = new java.util.ArrayList<>();
    }

    public StudentNaStudiji getStudentNaStudiji (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addStudentNaStudiji (StudentNaStudiji studentNaStudiji) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeStudentNaStudiji (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public Konsultacija getKonsultacija (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addKonsultacija (Konsultacija konsultacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeKonsultacija (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public GodinaStudijaObavestenje getGodinaStudijaObavestenje (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addGodinaStudijaObavestenje (GodinaStudijaObavestenje godinaStudijaObavestenje) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeGodinaStudijaObavestenje (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public Predmet getPredmet (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addPredmet (Predmet predmet) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removePredmet (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}