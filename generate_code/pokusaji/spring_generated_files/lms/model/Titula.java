package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE titula SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "titula")
public class Titula implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "titula_id")
    private Long id;
        
    private LocalDateTime datumIzbora;
        
    private LocalDateTime datumPrestanka;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @Enumerated(EnumType.STRING)
    private TipTitule tip;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "naucna_oblast_id")
    private NaucnaOblast naucnaOblast;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "nastavnik_id")
    private Nastavnik nastavnik;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public LocalDateTime getDatumIzbora () {
        return this.datumIzbora;
    }

    public void setDatumIzbora (LocalDateTime datumIzbora) {
        this.datumIzbora = datumIzbora;
    }
    
    public LocalDateTime getDatumPrestanka () {
        return this.datumPrestanka;
    }

    public void setDatumPrestanka (LocalDateTime datumPrestanka) {
        this.datumPrestanka = datumPrestanka;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public TipTitule getTip () {
        return this.tip;
    }

    public void setTip (TipTitule tip) {
        this.tip = tip;
    }
    
    public NaucnaOblast getNaucnaOblast () {
        return this.naucnaOblast;
    }

    public void setNaucnaOblast (NaucnaOblast naucnaOblast) {
        this.naucnaOblast = naucnaOblast;
    }
    
    public Nastavnik getNastavnik () {
        return this.nastavnik;
    }

    public void setNastavnik (Nastavnik nastavnik) {
        this.nastavnik = nastavnik;
    }
    


    public Titula () {
        super();
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}