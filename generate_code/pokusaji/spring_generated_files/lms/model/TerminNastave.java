package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE termin_nastave SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "termin_nastave")
public class TerminNastave implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "termin_nastave_id")
    private Long id;
        
    private Integer trajanje;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "prostorija_id")
    private Prostorija prostorija;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "nastavnik_id")
    private Nastavnik nastavnik;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "realizacija_predmeta_id")
    private RealizacijaPredmeta realizacijaPredmeta;
        
    @Enumerated(EnumType.STRING)
    private TipNastave tipNastave;
        
    @javax.persistence.OneToOne
    private VremeOdrzavanjaUNedelji vremeOdrzavanjaUNedelji;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public Integer getTrajanje () {
        return this.trajanje;
    }

    public void setTrajanje (Integer trajanje) {
        this.trajanje = trajanje;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public Prostorija getProstorija () {
        return this.prostorija;
    }

    public void setProstorija (Prostorija prostorija) {
        this.prostorija = prostorija;
    }
    
    public Nastavnik getNastavnik () {
        return this.nastavnik;
    }

    public void setNastavnik (Nastavnik nastavnik) {
        this.nastavnik = nastavnik;
    }
    
    public RealizacijaPredmeta getRealizacijaPredmeta () {
        return this.realizacijaPredmeta;
    }

    public void setRealizacijaPredmeta (RealizacijaPredmeta realizacijaPredmeta) {
        this.realizacijaPredmeta = realizacijaPredmeta;
    }
    
    public TipNastave getTipNastave () {
        return this.tipNastave;
    }

    public void setTipNastave (TipNastave tipNastave) {
        this.tipNastave = tipNastave;
    }
    
    public VremeOdrzavanjaUNedelji getVremeOdrzavanjaUNedelji () {
        return this.vremeOdrzavanjaUNedelji;
    }

    public void setVremeOdrzavanjaUNedelji (VremeOdrzavanjaUNedelji vremeOdrzavanjaUNedelji) {
        this.vremeOdrzavanjaUNedelji = vremeOdrzavanjaUNedelji;
    }
    


    public TerminNastave () {
        super();
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}