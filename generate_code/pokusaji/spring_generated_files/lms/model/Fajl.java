package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE fajl SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "fajl")
public class Fajl implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "fajl_id")
    private Long id;
        
    private String opis;
        
    private String url;
        
    private String naziv;
        
    private String tip;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "obavestenje_id")
    private Obavestenje obavestenje;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "evaluacija_znanja_id")
    private EvaluacijaZnanja evaluacijaZnanja;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "nastavni_materijal_id")
    private NastavniMaterijal nastavniMaterijal;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getOpis () {
        return this.opis;
    }

    public void setOpis (String opis) {
        this.opis = opis;
    }
    
    public String getUrl () {
        return this.url;
    }

    public void setUrl (String url) {
        this.url = url;
    }
    
    public String getNaziv () {
        return this.naziv;
    }

    public void setNaziv (String naziv) {
        this.naziv = naziv;
    }
    
    public String getTip () {
        return this.tip;
    }

    public void setTip (String tip) {
        this.tip = tip;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public Obavestenje getObavestenje () {
        return this.obavestenje;
    }

    public void setObavestenje (Obavestenje obavestenje) {
        this.obavestenje = obavestenje;
    }
    
    public EvaluacijaZnanja getEvaluacijaZnanja () {
        return this.evaluacijaZnanja;
    }

    public void setEvaluacijaZnanja (EvaluacijaZnanja evaluacijaZnanja) {
        this.evaluacijaZnanja = evaluacijaZnanja;
    }
    
    public NastavniMaterijal getNastavniMaterijal () {
        return this.nastavniMaterijal;
    }

    public void setNastavniMaterijal (NastavniMaterijal nastavniMaterijal) {
        this.nastavniMaterijal = nastavniMaterijal;
    }
    


    public Fajl () {
        super();
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}