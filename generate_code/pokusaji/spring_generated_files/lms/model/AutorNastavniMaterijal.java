package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE autor_nastavni_materijal SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "autor_nastavni_materijal")
public class AutorNastavniMaterijal implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "autor_nastavni_materijal_id")
    private Long id;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "nastavni_materijal_id")
    private NastavniMaterijal nastavniMaterijal;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "autor_id")
    private Autor autor;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public NastavniMaterijal getNastavniMaterijal () {
        return this.nastavniMaterijal;
    }

    public void setNastavniMaterijal (NastavniMaterijal nastavniMaterijal) {
        this.nastavniMaterijal = nastavniMaterijal;
    }
    
    public Autor getAutor () {
        return this.autor;
    }

    public void setAutor (Autor autor) {
        this.autor = autor;
    }
    


    public AutorNastavniMaterijal () {
        super();
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}