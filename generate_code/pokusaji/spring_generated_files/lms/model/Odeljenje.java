package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE odeljenje SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "odeljenje")
public class Odeljenje implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "odeljenje_id")
    private Long id;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @Enumerated(EnumType.STRING)
    private TipOdeljenja tip;
        
    @javax.persistence.OneToMany(mappedBy="odeljenje")
    private java.util.List<Prostorija> prostorije;
        
    @javax.persistence.OneToOne
    private Adresa adresa;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "fakultet_id")
    private Fakultet fakultet;
        
    @javax.persistence.OneToOne
    private StudentskaSluzba studentskaSluzba;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public TipOdeljenja getTip () {
        return this.tip;
    }

    public void setTip (TipOdeljenja tip) {
        this.tip = tip;
    }
    
    public java.util.List<Prostorija> getProstorije () {
        return this.prostorije;
    }

    public void setProstorije (java.util.List<Prostorija> prostorije) {
        this.prostorije = prostorije;
    }
    
    public Adresa getAdresa () {
        return this.adresa;
    }

    public void setAdresa (Adresa adresa) {
        this.adresa = adresa;
    }
    
    public Fakultet getFakultet () {
        return this.fakultet;
    }

    public void setFakultet (Fakultet fakultet) {
        this.fakultet = fakultet;
    }
    
    public StudentskaSluzba getStudentskaSluzba () {
        return this.studentskaSluzba;
    }

    public void setStudentskaSluzba (StudentskaSluzba studentskaSluzba) {
        this.studentskaSluzba = studentskaSluzba;
    }
    


    public Odeljenje () {
        super();
        this.prostorije = new java.util.ArrayList<>();
    }

    public Prostorija getProstorija (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addProstorija (Prostorija prostorija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeProstorija (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}