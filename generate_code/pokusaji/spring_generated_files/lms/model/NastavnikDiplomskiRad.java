package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE nastavnik_diplomski_rad SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "nastavnik_diplomski_rad")
public class NastavnikDiplomskiRad implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "nastavnik_diplomski_rad_id")
    private Long id;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "nastavnik_id")
    private Nastavnik nastavnikUKomisiji;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "diplomski_rad_id")
    private DiplomskiRad diplomskiRad;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public Nastavnik getNastavnikUKomisiji () {
        return this.nastavnikUKomisiji;
    }

    public void setNastavnikUKomisiji (Nastavnik nastavnikUKomisiji) {
        this.nastavnikUKomisiji = nastavnikUKomisiji;
    }
    
    public DiplomskiRad getDiplomskiRad () {
        return this.diplomskiRad;
    }

    public void setDiplomskiRad (DiplomskiRad diplomskiRad) {
        this.diplomskiRad = diplomskiRad;
    }
    


    public NastavnikDiplomskiRad () {
        super();
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}