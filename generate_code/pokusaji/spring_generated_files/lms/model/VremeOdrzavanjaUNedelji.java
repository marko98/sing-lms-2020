package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE vreme_odrzavanja_u_nedelji SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "vreme_odrzavanja_u_nedelji")
public class VremeOdrzavanjaUNedelji implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "vreme_odrzavanja_u_nedelji_id")
    private Long id;
        
    private Time vreme;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.OneToOne(mappedBy="vremeOdrzavanjaUNedelji")
    private Konsultacija konsultacija;
        
    @Enumerated(EnumType.STRING)
    private Dan dan;
        
    @javax.persistence.OneToOne(mappedBy="vremeOdrzavanjaUNedelji")
    private TerminNastave terminNastave;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public Time getVreme () {
        return this.vreme;
    }

    public void setVreme (Time vreme) {
        this.vreme = vreme;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public Konsultacija getKonsultacija () {
        return this.konsultacija;
    }

    public void setKonsultacija (Konsultacija konsultacija) {
        this.konsultacija = konsultacija;
    }
    
    public Dan getDan () {
        return this.dan;
    }

    public void setDan (Dan dan) {
        this.dan = dan;
    }
    
    public TerminNastave getTerminNastave () {
        return this.terminNastave;
    }

    public void setTerminNastave (TerminNastave terminNastave) {
        this.terminNastave = terminNastave;
    }
    


    public VremeOdrzavanjaUNedelji () {
        super();
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}