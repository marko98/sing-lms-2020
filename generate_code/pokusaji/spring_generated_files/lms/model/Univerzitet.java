package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE univerzitet SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "univerzitet")
public class Univerzitet implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "univerzitet_id")
    private Long id;
        
    private String naziv;
        
    private LocalDateTime datumOsnivanja;
        
    private String opis;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.OneToMany(mappedBy="univerzitet")
    private java.util.List<Adresa> adrese;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "nastavnik_id")
    private Nastavnik rektor;
        
    @javax.persistence.OneToMany(mappedBy="univerzitet")
    private java.util.List<Fakultet> fakulteti;
        
    @javax.persistence.OneToMany(mappedBy="univerzitet")
    private java.util.List<Kalendar> kalendari;
        
    @javax.persistence.OneToOne
    private StudentskaSluzba studentskaSluzba;
        
    @javax.persistence.OneToMany(mappedBy="univerzitet")
    private java.util.List<Kontakt> kontakti;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getNaziv () {
        return this.naziv;
    }

    public void setNaziv (String naziv) {
        this.naziv = naziv;
    }
    
    public LocalDateTime getDatumOsnivanja () {
        return this.datumOsnivanja;
    }

    public void setDatumOsnivanja (LocalDateTime datumOsnivanja) {
        this.datumOsnivanja = datumOsnivanja;
    }
    
    public String getOpis () {
        return this.opis;
    }

    public void setOpis (String opis) {
        this.opis = opis;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public java.util.List<Adresa> getAdrese () {
        return this.adrese;
    }

    public void setAdrese (java.util.List<Adresa> adrese) {
        this.adrese = adrese;
    }
    
    public Nastavnik getRektor () {
        return this.rektor;
    }

    public void setRektor (Nastavnik rektor) {
        this.rektor = rektor;
    }
    
    public java.util.List<Fakultet> getFakulteti () {
        return this.fakulteti;
    }

    public void setFakulteti (java.util.List<Fakultet> fakulteti) {
        this.fakulteti = fakulteti;
    }
    
    public java.util.List<Kalendar> getKalendari () {
        return this.kalendari;
    }

    public void setKalendari (java.util.List<Kalendar> kalendari) {
        this.kalendari = kalendari;
    }
    
    public StudentskaSluzba getStudentskaSluzba () {
        return this.studentskaSluzba;
    }

    public void setStudentskaSluzba (StudentskaSluzba studentskaSluzba) {
        this.studentskaSluzba = studentskaSluzba;
    }
    
    public java.util.List<Kontakt> getKontakti () {
        return this.kontakti;
    }

    public void setKontakti (java.util.List<Kontakt> kontakti) {
        this.kontakti = kontakti;
    }
    


    public Univerzitet () {
        super();
        this.adrese = new java.util.ArrayList<>();
        this.fakulteti = new java.util.ArrayList<>();
        this.kalendari = new java.util.ArrayList<>();
        this.kontakti = new java.util.ArrayList<>();
    }

    public Adresa getAdresa (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addAdresa (Adresa adresa) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeAdresa (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public Fakultet getFakultet (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addFakultet (Fakultet fakultet) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeFakultet (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public Kalendar getKalendar (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addKalendar (Kalendar kalendar) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeKalendar (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public Kontakt getKontakt (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addKontakt (Kontakt kontakt) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeKontakt (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}