package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE adresa SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "adresa")
public class Adresa implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "adresa_id")
    private Long id;
        
    private String ulica;
        
    private String broj;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "grad_id")
    private Grad grad;
        
    @Enumerated(EnumType.STRING)
    private TipAdrese tip;
        
    @javax.persistence.OneToOne(mappedBy="adresa")
    private Odeljenje odeljenje;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "univerzitet_id")
    private Univerzitet univerzitet;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "registrovani_korisnik_id")
    private RegistrovaniKorisnik registrovaniKorisnik;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getUlica () {
        return this.ulica;
    }

    public void setUlica (String ulica) {
        this.ulica = ulica;
    }
    
    public String getBroj () {
        return this.broj;
    }

    public void setBroj (String broj) {
        this.broj = broj;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public Grad getGrad () {
        return this.grad;
    }

    public void setGrad (Grad grad) {
        this.grad = grad;
    }
    
    public TipAdrese getTip () {
        return this.tip;
    }

    public void setTip (TipAdrese tip) {
        this.tip = tip;
    }
    
    public Odeljenje getOdeljenje () {
        return this.odeljenje;
    }

    public void setOdeljenje (Odeljenje odeljenje) {
        this.odeljenje = odeljenje;
    }
    
    public Univerzitet getUniverzitet () {
        return this.univerzitet;
    }

    public void setUniverzitet (Univerzitet univerzitet) {
        this.univerzitet = univerzitet;
    }
    
    public RegistrovaniKorisnik getRegistrovaniKorisnik () {
        return this.registrovaniKorisnik;
    }

    public void setRegistrovaniKorisnik (RegistrovaniKorisnik registrovaniKorisnik) {
        this.registrovaniKorisnik = registrovaniKorisnik;
    }
    


    public Adresa () {
        super();
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}