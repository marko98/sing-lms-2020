package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE predmet SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "predmet")
public class Predmet implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "predmet_id")
    private Long id;
        
    private String naziv;
        
    private boolean obavezan;
        
    private Integer brojPredavanja;
        
    private Integer brojVezbi;
        
    private Integer brojCasovaZaIstrazivackeRadove;
        
    private Integer espb;
        
    private Integer trajanjeUSemestrima;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "godina_studija_id")
    private GodinaStudija godinaStudija;
        
    @javax.persistence.OneToOne
    private Silabus silabus;
        
    @javax.persistence.OneToMany(mappedBy="predmet")
    private java.util.List<PredmetTipDrugogOblikaNastave> predmetTipoviDrugogOblikaNastave;
        
    @javax.persistence.OneToMany(mappedBy="predmet")
    private java.util.List<PredmetTipNastave> predmetTipoviNastave;
        
    @javax.persistence.OneToMany(mappedBy="uslov")
    private java.util.List<PredmetUslovniPredmet> predmetiZaKojeSamUslov;
        
    @javax.persistence.OneToMany(mappedBy="predmet")
    private java.util.List<PredmetUslovniPredmet> uslovniPredmeti;
        
    @javax.persistence.OneToOne(mappedBy="predmet")
    private RealizacijaPredmeta realizacijaPredmeta;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getNaziv () {
        return this.naziv;
    }

    public void setNaziv (String naziv) {
        this.naziv = naziv;
    }
    
    public boolean getObavezan () {
        return this.obavezan;
    }

    public void setObavezan (boolean obavezan) {
        this.obavezan = obavezan;
    }
    
    public Integer getBrojPredavanja () {
        return this.brojPredavanja;
    }

    public void setBrojPredavanja (Integer brojPredavanja) {
        this.brojPredavanja = brojPredavanja;
    }
    
    public Integer getBrojVezbi () {
        return this.brojVezbi;
    }

    public void setBrojVezbi (Integer brojVezbi) {
        this.brojVezbi = brojVezbi;
    }
    
    public Integer getBrojCasovaZaIstrazivackeRadove () {
        return this.brojCasovaZaIstrazivackeRadove;
    }

    public void setBrojCasovaZaIstrazivackeRadove (Integer brojCasovaZaIstrazivackeRadove) {
        this.brojCasovaZaIstrazivackeRadove = brojCasovaZaIstrazivackeRadove;
    }
    
    public Integer getEspb () {
        return this.espb;
    }

    public void setEspb (Integer espb) {
        this.espb = espb;
    }
    
    public Integer getTrajanjeUSemestrima () {
        return this.trajanjeUSemestrima;
    }

    public void setTrajanjeUSemestrima (Integer trajanjeUSemestrima) {
        this.trajanjeUSemestrima = trajanjeUSemestrima;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public GodinaStudija getGodinaStudija () {
        return this.godinaStudija;
    }

    public void setGodinaStudija (GodinaStudija godinaStudija) {
        this.godinaStudija = godinaStudija;
    }
    
    public Silabus getSilabus () {
        return this.silabus;
    }

    public void setSilabus (Silabus silabus) {
        this.silabus = silabus;
    }
    
    public java.util.List<PredmetTipDrugogOblikaNastave> getPredmetTipoviDrugogOblikaNastave () {
        return this.predmetTipoviDrugogOblikaNastave;
    }

    public void setPredmetTipoviDrugogOblikaNastave (java.util.List<PredmetTipDrugogOblikaNastave> predmetTipoviDrugogOblikaNastave) {
        this.predmetTipoviDrugogOblikaNastave = predmetTipoviDrugogOblikaNastave;
    }
    
    public java.util.List<PredmetTipNastave> getPredmetTipoviNastave () {
        return this.predmetTipoviNastave;
    }

    public void setPredmetTipoviNastave (java.util.List<PredmetTipNastave> predmetTipoviNastave) {
        this.predmetTipoviNastave = predmetTipoviNastave;
    }
    
    public java.util.List<PredmetUslovniPredmet> getPredmetiZaKojeSamUslov () {
        return this.predmetiZaKojeSamUslov;
    }

    public void setPredmetiZaKojeSamUslov (java.util.List<PredmetUslovniPredmet> predmetiZaKojeSamUslov) {
        this.predmetiZaKojeSamUslov = predmetiZaKojeSamUslov;
    }
    
    public java.util.List<PredmetUslovniPredmet> getUslovniPredmeti () {
        return this.uslovniPredmeti;
    }

    public void setUslovniPredmeti (java.util.List<PredmetUslovniPredmet> uslovniPredmeti) {
        this.uslovniPredmeti = uslovniPredmeti;
    }
    
    public RealizacijaPredmeta getRealizacijaPredmeta () {
        return this.realizacijaPredmeta;
    }

    public void setRealizacijaPredmeta (RealizacijaPredmeta realizacijaPredmeta) {
        this.realizacijaPredmeta = realizacijaPredmeta;
    }
    


    public Predmet () {
        super();
        this.predmetTipoviDrugogOblikaNastave = new java.util.ArrayList<>();
        this.predmetTipoviNastave = new java.util.ArrayList<>();
        this.predmetiZaKojeSamUslov = new java.util.ArrayList<>();
        this.uslovniPredmeti = new java.util.ArrayList<>();
    }

    public Predmet getPreduslov (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addPreduslov (Predmet preduslov) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removePreduslov (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public PredmetTipNastave getPredmetTipNastave (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addPredmetTipNastave (PredmetTipNastave predmetTipNastave) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removePredmetTipNastave (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public PredmetTipDrugogOblikaNastave getPredmetTipDrugogOblikaNastave (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addPredmetTipDrugogOblikaNastave (PredmetTipDrugogOblikaNastave predmetTipDrugogOblikaNastave) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removePredmetTipDrugogOblikaNastave (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public PredmetUslovniPredmet getPredmetZaKojiSamUslov (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addPredmetZaKojiSamUslov (PredmetUslovniPredmet predmetUslovniPredmet) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removePredmetZaKojiSamUslov (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public PredmetUslovniPredmet getUslovniPredmet (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addUslovniPredmet (PredmetUslovniPredmet predmetUslovniPredmet) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeUslovniPredmet (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}