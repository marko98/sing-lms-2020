package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE istrazivacki_rad_student_na_studiji SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "istrazivacki_rad_student_na_studiji")
public class IstrazivackiRadStudentNaStudiji implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "istrazivacki_rad_student_na_studiji_id")
    private Long id;
        
    private LocalDateTime datum;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "istrazivacki_rad_id")
    private IstrazivackiRad istrazivackiRad;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "student_na_studiji_id")
    private StudentNaStudiji studentNaStudiji;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public LocalDateTime getDatum () {
        return this.datum;
    }

    public void setDatum (LocalDateTime datum) {
        this.datum = datum;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public IstrazivackiRad getIstrazivackiRad () {
        return this.istrazivackiRad;
    }

    public void setIstrazivackiRad (IstrazivackiRad istrazivackiRad) {
        this.istrazivackiRad = istrazivackiRad;
    }
    
    public StudentNaStudiji getStudentNaStudiji () {
        return this.studentNaStudiji;
    }

    public void setStudentNaStudiji (StudentNaStudiji studentNaStudiji) {
        this.studentNaStudiji = studentNaStudiji;
    }
    


    public IstrazivackiRadStudentNaStudiji () {
        super();
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}