package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE kalendar SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "kalendar")
public class Kalendar implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "kalendar_id")
    private Long id;
        
    private LocalDateTime pocetniDatum;
        
    private LocalDateTime krajnjiDatum;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @Enumerated(EnumType.STRING)
    private TipStudija tipStudija;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "univerzitet_id")
    private Univerzitet univerzitet;
        
    @javax.persistence.OneToMany(mappedBy="kalendar")
    private java.util.List<DogadjajKalendar> dogadjajiKalendar;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public LocalDateTime getPocetniDatum () {
        return this.pocetniDatum;
    }

    public void setPocetniDatum (LocalDateTime pocetniDatum) {
        this.pocetniDatum = pocetniDatum;
    }
    
    public LocalDateTime getKrajnjiDatum () {
        return this.krajnjiDatum;
    }

    public void setKrajnjiDatum (LocalDateTime krajnjiDatum) {
        this.krajnjiDatum = krajnjiDatum;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public TipStudija getTipStudija () {
        return this.tipStudija;
    }

    public void setTipStudija (TipStudija tipStudija) {
        this.tipStudija = tipStudija;
    }
    
    public Univerzitet getUniverzitet () {
        return this.univerzitet;
    }

    public void setUniverzitet (Univerzitet univerzitet) {
        this.univerzitet = univerzitet;
    }
    
    public java.util.List<DogadjajKalendar> getDogadjajiKalendar () {
        return this.dogadjajiKalendar;
    }

    public void setDogadjajiKalendar (java.util.List<DogadjajKalendar> dogadjajiKalendar) {
        this.dogadjajiKalendar = dogadjajiKalendar;
    }
    


    public Kalendar () {
        super();
        this.dogadjajiKalendar = new java.util.ArrayList<>();
    }

    public DogadjajKalendar getDogadjajKalendar (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addDogadjajKalendar (DogadjajKalendar dogadjajKalendar) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeDogadjajKalendar (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}