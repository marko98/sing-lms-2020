package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE student SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "student")
public class Student implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "student_id")
    private Long id;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.OneToOne
    private RegistrovaniKorisnik registrovaniKorisnik;
        
    @javax.persistence.OneToMany(mappedBy="student")
    private java.util.List<StudentNaStudiji> studije;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public RegistrovaniKorisnik getRegistrovaniKorisnik () {
        return this.registrovaniKorisnik;
    }

    public void setRegistrovaniKorisnik (RegistrovaniKorisnik registrovaniKorisnik) {
        this.registrovaniKorisnik = registrovaniKorisnik;
    }
    
    public java.util.List<StudentNaStudiji> getStudije () {
        return this.studije;
    }

    public void setStudije (java.util.List<StudentNaStudiji> studije) {
        this.studije = studije;
    }
    


    public Student () {
        super();
        this.studije = new java.util.ArrayList<>();
    }

    public StudentNaStudiji getStudija (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addStudija (StudentNaStudiji studija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeStudija (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.NEAKTIVAN; // aktivira ga administrativno osoblje
    }

}