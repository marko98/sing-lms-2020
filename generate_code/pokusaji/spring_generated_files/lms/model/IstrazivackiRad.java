package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE istrazivacki_rad SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "istrazivacki_rad")
public class IstrazivackiRad implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "istrazivacki_rad_id")
    private Long id;
        
    private String tema;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.OneToMany(mappedBy="istrazivackiRad")
    private java.util.List<IstrazivackiRadStudentNaStudiji> istrazivackiRadStudentiNaStudijama;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "nastavnik_id")
    private Nastavnik nastavnik;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "realizacija_predmeta_id")
    private RealizacijaPredmeta realizacijaPredmeta;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getTema () {
        return this.tema;
    }

    public void setTema (String tema) {
        this.tema = tema;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public java.util.List<IstrazivackiRadStudentNaStudiji> getIstrazivackiRadStudentiNaStudijama () {
        return this.istrazivackiRadStudentiNaStudijama;
    }

    public void setIstrazivackiRadStudentiNaStudijama (java.util.List<IstrazivackiRadStudentNaStudiji> istrazivackiRadStudentiNaStudijama) {
        this.istrazivackiRadStudentiNaStudijama = istrazivackiRadStudentiNaStudijama;
    }
    
    public Nastavnik getNastavnik () {
        return this.nastavnik;
    }

    public void setNastavnik (Nastavnik nastavnik) {
        this.nastavnik = nastavnik;
    }
    
    public RealizacijaPredmeta getRealizacijaPredmeta () {
        return this.realizacijaPredmeta;
    }

    public void setRealizacijaPredmeta (RealizacijaPredmeta realizacijaPredmeta) {
        this.realizacijaPredmeta = realizacijaPredmeta;
    }
    


    public IstrazivackiRad () {
        super();
        this.istrazivackiRadStudentiNaStudijama = new java.util.ArrayList<>();
    }

    public IstrazivackiRadStudentNaStudiji getIstrazivackiRadStudentNaStudiji (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addIstrazivackiRadStudentNaStudiji (IstrazivackiRadStudentNaStudiji istrazivackiRadStudentNaStudiji) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeIstrazivackiRadStudentNaStudiji (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}