package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE nastavni_materijal SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "nastavni_materijal")
public class NastavniMaterijal implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "nastavni_materijal_id")
    private Long id;
        
    private String naziv;
        
    private LocalDateTime datum;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "ishod_id")
    private Ishod ishod;
        
    @javax.persistence.OneToMany(mappedBy="nastavniMaterijal")
    private java.util.List<Fajl> fajlovi;
        
    @javax.persistence.OneToMany(mappedBy="nastavniMaterijal")
    private java.util.List<AutorNastavniMaterijal> autoriNastavniMaterijal;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getNaziv () {
        return this.naziv;
    }

    public void setNaziv (String naziv) {
        this.naziv = naziv;
    }
    
    public LocalDateTime getDatum () {
        return this.datum;
    }

    public void setDatum (LocalDateTime datum) {
        this.datum = datum;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public Ishod getIshod () {
        return this.ishod;
    }

    public void setIshod (Ishod ishod) {
        this.ishod = ishod;
    }
    
    public java.util.List<Fajl> getFajlovi () {
        return this.fajlovi;
    }

    public void setFajlovi (java.util.List<Fajl> fajlovi) {
        this.fajlovi = fajlovi;
    }
    
    public java.util.List<AutorNastavniMaterijal> getAutoriNastavniMaterijal () {
        return this.autoriNastavniMaterijal;
    }

    public void setAutoriNastavniMaterijal (java.util.List<AutorNastavniMaterijal> autoriNastavniMaterijal) {
        this.autoriNastavniMaterijal = autoriNastavniMaterijal;
    }
    


    public NastavniMaterijal () {
        super();
        this.fajlovi = new java.util.ArrayList<>();
        this.autoriNastavniMaterijal = new java.util.ArrayList<>();
    }

    public Fajl getFajl (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addFajl (Fajl fajl) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeFajl (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public AutorNastavniMaterijal getAutorNastavniMaterijal (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addAutorNastavniMaterijal (AutorNastavniMaterijal autorNastavniMaterijal) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeAutorNastavniMaterijal (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}