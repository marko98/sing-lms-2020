package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE silabus SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "silabus")
public class Silabus implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "silabus_id")
    private Long id;
        
    private String naslov;
        
    private String opis;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.OneToOne(mappedBy="silabus")
    private Predmet predmet;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getNaslov () {
        return this.naslov;
    }

    public void setNaslov (String naslov) {
        this.naslov = naslov;
    }
    
    public String getOpis () {
        return this.opis;
    }

    public void setOpis (String opis) {
        this.opis = opis;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public Predmet getPredmet () {
        return this.predmet;
    }

    public void setPredmet (Predmet predmet) {
        this.predmet = predmet;
    }
    


    public Silabus () {
        super();
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}