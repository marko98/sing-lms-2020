package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE ishod SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "ishod")
public class Ishod implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "ishod_id")
    private Long id;
        
    private String opis;
        
    private String naslov;
        
    private String putanjaZaSliku;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "realizacija_predmeta_id")
    private RealizacijaPredmeta realizacijaPredmeta;
        
    @javax.persistence.OneToOne(mappedBy="ishod")
    private EvaluacijaZnanja evaluacijaZnanja;
        
    @javax.persistence.OneToMany(mappedBy="ishod")
    private java.util.List<ObrazovniCilj> obrazovniCiljevi;
        
    @javax.persistence.OneToMany(mappedBy="ishod")
    private java.util.List<NastavniMaterijal> nastavniMaterijali;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getOpis () {
        return this.opis;
    }

    public void setOpis (String opis) {
        this.opis = opis;
    }
    
    public String getNaslov () {
        return this.naslov;
    }

    public void setNaslov (String naslov) {
        this.naslov = naslov;
    }
    
    public String getPutanjaZaSliku () {
        return this.putanjaZaSliku;
    }

    public void setPutanjaZaSliku (String putanjaZaSliku) {
        this.putanjaZaSliku = putanjaZaSliku;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public RealizacijaPredmeta getRealizacijaPredmeta () {
        return this.realizacijaPredmeta;
    }

    public void setRealizacijaPredmeta (RealizacijaPredmeta realizacijaPredmeta) {
        this.realizacijaPredmeta = realizacijaPredmeta;
    }
    
    public EvaluacijaZnanja getEvaluacijaZnanja () {
        return this.evaluacijaZnanja;
    }

    public void setEvaluacijaZnanja (EvaluacijaZnanja evaluacijaZnanja) {
        this.evaluacijaZnanja = evaluacijaZnanja;
    }
    
    public java.util.List<ObrazovniCilj> getObrazovniCiljevi () {
        return this.obrazovniCiljevi;
    }

    public void setObrazovniCiljevi (java.util.List<ObrazovniCilj> obrazovniCiljevi) {
        this.obrazovniCiljevi = obrazovniCiljevi;
    }
    
    public java.util.List<NastavniMaterijal> getNastavniMaterijali () {
        return this.nastavniMaterijali;
    }

    public void setNastavniMaterijali (java.util.List<NastavniMaterijal> nastavniMaterijali) {
        this.nastavniMaterijali = nastavniMaterijali;
    }
    


    public Ishod () {
        super();
        this.obrazovniCiljevi = new java.util.ArrayList<>();
        this.nastavniMaterijali = new java.util.ArrayList<>();
    }

    public ObrazovniCilj getObrazovniCilj (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addObrazovniCilj (ObrazovniCilj obrazovniCilj) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeObrazovniCilj (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public NastavniMaterijal getNastavniMaterijal (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addNastavniMaterijal (NastavniMaterijal nastavniMaterijal) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeNastavniMaterijal (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}