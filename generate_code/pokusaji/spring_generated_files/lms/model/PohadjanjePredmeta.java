package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE pohadjanje_predmeta SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "pohadjanje_predmeta")
public class PohadjanjePredmeta implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "pohadjanje_predmeta_id")
    private Long id;
        
    private Integer konacnaOcena;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "student_na_studiji_id")
    private StudentNaStudiji studentNaStudiji;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "realizacija_predmeta_id")
    private RealizacijaPredmeta realizacijaPredmeta;
        
    @javax.persistence.OneToMany(mappedBy="pohadjanjePredmeta")
    private java.util.List<Polaganje> polaganja;
        
    @javax.persistence.OneToMany(mappedBy="pohadjanjePredmeta")
    private java.util.List<DatumPohadjanjaPredmeta> prisustvo;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public Integer getKonacnaOcena () {
        return this.konacnaOcena;
    }

    public void setKonacnaOcena (Integer konacnaOcena) {
        this.konacnaOcena = konacnaOcena;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public StudentNaStudiji getStudentNaStudiji () {
        return this.studentNaStudiji;
    }

    public void setStudentNaStudiji (StudentNaStudiji studentNaStudiji) {
        this.studentNaStudiji = studentNaStudiji;
    }
    
    public RealizacijaPredmeta getRealizacijaPredmeta () {
        return this.realizacijaPredmeta;
    }

    public void setRealizacijaPredmeta (RealizacijaPredmeta realizacijaPredmeta) {
        this.realizacijaPredmeta = realizacijaPredmeta;
    }
    
    public java.util.List<Polaganje> getPolaganja () {
        return this.polaganja;
    }

    public void setPolaganja (java.util.List<Polaganje> polaganja) {
        this.polaganja = polaganja;
    }
    
    public java.util.List<DatumPohadjanjaPredmeta> getPrisustvo () {
        return this.prisustvo;
    }

    public void setPrisustvo (java.util.List<DatumPohadjanjaPredmeta> prisustvo) {
        this.prisustvo = prisustvo;
    }
    


    public PohadjanjePredmeta () {
        super();
        this.polaganja = new java.util.ArrayList<>();
        this.prisustvo = new java.util.ArrayList<>();
    }

    public Polaganje getPolaganje (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addPolaganje (Polaganje polaganje) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removePolaganje (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public DatumPohadjanjaPredmeta getDatumPohadjanjaPredmeta (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addDatumPohadjanjaPredmeta (DatumPohadjanjaPredmeta datumPohadjanjaPredmeta) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeDatumPohadjanjaPredmeta (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}