package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE realizacija_predmeta SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "realizacija_predmeta")
public class RealizacijaPredmeta implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "realizacija_predmeta_id")
    private Long id;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.OneToMany(mappedBy="realizacijaPredmeta")
    private java.util.List<DrugiOblikNastave> drugiObliciNastave;
        
    @javax.persistence.OneToMany(mappedBy="realizacijaPredmeta")
    private java.util.List<NastavnikNaRealizaciji> nastavniciNaRealizaciji;
        
    @javax.persistence.OneToMany(mappedBy="realizacijaPredmeta")
    private java.util.List<PohadjanjePredmeta> pohadjanjaPredmeta;
        
    @javax.persistence.OneToMany(mappedBy="realizacijaPredmeta")
    private java.util.List<EvaluacijaZnanja> evaluacijeZnanja;
        
    @javax.persistence.OneToMany(mappedBy="realizacijaPredmeta")
    private java.util.List<ObrazovniCilj> obrazovniCiljevi;
        
    @javax.persistence.OneToMany(mappedBy="realizacijaPredmeta")
    private java.util.List<TerminNastave> terminiNastave;
        
    @javax.persistence.OneToMany(mappedBy="realizacijaPredmeta")
    private java.util.List<Ishod> ishodi;
        
    @javax.persistence.OneToMany(mappedBy="realizacijaPredmeta")
    private java.util.List<IstrazivackiRad> istrazivackiRadovi;
        
    @javax.persistence.OneToOne
    private Predmet predmet;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public java.util.List<DrugiOblikNastave> getDrugiObliciNastave () {
        return this.drugiObliciNastave;
    }

    public void setDrugiObliciNastave (java.util.List<DrugiOblikNastave> drugiObliciNastave) {
        this.drugiObliciNastave = drugiObliciNastave;
    }
    
    public java.util.List<NastavnikNaRealizaciji> getNastavniciNaRealizaciji () {
        return this.nastavniciNaRealizaciji;
    }

    public void setNastavniciNaRealizaciji (java.util.List<NastavnikNaRealizaciji> nastavniciNaRealizaciji) {
        this.nastavniciNaRealizaciji = nastavniciNaRealizaciji;
    }
    
    public java.util.List<PohadjanjePredmeta> getPohadjanjaPredmeta () {
        return this.pohadjanjaPredmeta;
    }

    public void setPohadjanjaPredmeta (java.util.List<PohadjanjePredmeta> pohadjanjaPredmeta) {
        this.pohadjanjaPredmeta = pohadjanjaPredmeta;
    }
    
    public java.util.List<EvaluacijaZnanja> getEvaluacijeZnanja () {
        return this.evaluacijeZnanja;
    }

    public void setEvaluacijeZnanja (java.util.List<EvaluacijaZnanja> evaluacijeZnanja) {
        this.evaluacijeZnanja = evaluacijeZnanja;
    }
    
    public java.util.List<ObrazovniCilj> getObrazovniCiljevi () {
        return this.obrazovniCiljevi;
    }

    public void setObrazovniCiljevi (java.util.List<ObrazovniCilj> obrazovniCiljevi) {
        this.obrazovniCiljevi = obrazovniCiljevi;
    }
    
    public java.util.List<TerminNastave> getTerminiNastave () {
        return this.terminiNastave;
    }

    public void setTerminiNastave (java.util.List<TerminNastave> terminiNastave) {
        this.terminiNastave = terminiNastave;
    }
    
    public java.util.List<Ishod> getIshodi () {
        return this.ishodi;
    }

    public void setIshodi (java.util.List<Ishod> ishodi) {
        this.ishodi = ishodi;
    }
    
    public java.util.List<IstrazivackiRad> getIstrazivackiRadovi () {
        return this.istrazivackiRadovi;
    }

    public void setIstrazivackiRadovi (java.util.List<IstrazivackiRad> istrazivackiRadovi) {
        this.istrazivackiRadovi = istrazivackiRadovi;
    }
    
    public Predmet getPredmet () {
        return this.predmet;
    }

    public void setPredmet (Predmet predmet) {
        this.predmet = predmet;
    }
    


    public RealizacijaPredmeta () {
        super();
        this.drugiObliciNastave = new java.util.ArrayList<>();
        this.nastavniciNaRealizaciji = new java.util.ArrayList<>();
        this.pohadjanjaPredmeta = new java.util.ArrayList<>();
        this.evaluacijeZnanja = new java.util.ArrayList<>();
        this.obrazovniCiljevi = new java.util.ArrayList<>();
        this.terminiNastave = new java.util.ArrayList<>();
        this.ishodi = new java.util.ArrayList<>();
        this.istrazivackiRadovi = new java.util.ArrayList<>();
    }

    public DrugiOblikNastave getDrugiOblikNastave (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addDrugiOblikNastave (DrugiOblikNastave drugiOblikNastave) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeDrugiOblikNastave (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public NastavnikNaRealizaciji getNastavnikNaRealizaciji (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addNastavnikNaRealizaciji (NastavnikNaRealizaciji nastavnikNaRealizaciji) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeNastavnikNaRealizaciji (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public PohadjanjePredmeta getPohadjanjePredmeta (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addPohadjanjePredmeta (PohadjanjePredmeta pohadjanjePredmeta) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removePohadjanjePredmeta (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public EvaluacijaZnanja getEvaluacijaZnanja (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addEvaluacijaZnanja (EvaluacijaZnanja evaluacijaZnanja) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeEvaluacijaZnanja (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public ObrazovniCilj getObrazovniCilj (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addObrazovniCilj (ObrazovniCilj obrazovniCilj) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeObrazovniCilj (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public TerminNastave getTerminNastave (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addTerminNastave (TerminNastave terminNastave) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeTerminNastave (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public Ishod getIshod (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addIshod (Ishod ishod) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeIshod (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public IstrazivackiRad getIstrazivackiRad (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addIstrazivackiRad (IstrazivackiRad istrazivackiRad) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeIstrazivackiRad (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}