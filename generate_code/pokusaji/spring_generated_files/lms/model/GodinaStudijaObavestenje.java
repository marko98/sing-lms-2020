package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE godina_studija_obavestenje SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "godina_studija_obavestenje")
public class GodinaStudijaObavestenje implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "godina_studija_obavestenje_id")
    private Long id;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "godina_studija_id")
    private GodinaStudija godinaStudija;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "obavestenje_id")
    private Obavestenje obavestenje;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public GodinaStudija getGodinaStudija () {
        return this.godinaStudija;
    }

    public void setGodinaStudija (GodinaStudija godinaStudija) {
        this.godinaStudija = godinaStudija;
    }
    
    public Obavestenje getObavestenje () {
        return this.obavestenje;
    }

    public void setObavestenje (Obavestenje obavestenje) {
        this.obavestenje = obavestenje;
    }
    


    public GodinaStudijaObavestenje () {
        super();
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}