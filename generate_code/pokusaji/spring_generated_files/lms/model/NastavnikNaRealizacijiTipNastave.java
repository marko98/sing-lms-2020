package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE nastavnik_na_realizaciji_tip_nastave SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "nastavnik_na_realizaciji_tip_nastave")
public class NastavnikNaRealizacijiTipNastave implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "nastavnik_na_realizaciji_tip_nastave_id")
    private Long id;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "nastavnik_na_realizaciji_id")
    private NastavnikNaRealizaciji nastavnikNaRealizaciji;
        
    @Enumerated(EnumType.STRING)
    private TipNastave tipNastave;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public NastavnikNaRealizaciji getNastavnikNaRealizaciji () {
        return this.nastavnikNaRealizaciji;
    }

    public void setNastavnikNaRealizaciji (NastavnikNaRealizaciji nastavnikNaRealizaciji) {
        this.nastavnikNaRealizaciji = nastavnikNaRealizaciji;
    }
    
    public TipNastave getTipNastave () {
        return this.tipNastave;
    }

    public void setTipNastave (TipNastave tipNastave) {
        this.tipNastave = tipNastave;
    }
    


    public NastavnikNaRealizacijiTipNastave () {
        super();
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}