package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE obavestenje SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "obavestenje")
public class Obavestenje implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "obavestenje_id")
    private Long id;
        
    private String naslov;
        
    private LocalDateTime datum;
        
    private String sadrzaj;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.OneToMany(mappedBy="obavestenje")
    private java.util.List<GodinaStudijaObavestenje> godineStudijaObavestenje;
        
    @javax.persistence.OneToMany(mappedBy="obavestenje")
    private java.util.List<Fajl> fajlovi;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getNaslov () {
        return this.naslov;
    }

    public void setNaslov (String naslov) {
        this.naslov = naslov;
    }
    
    public LocalDateTime getDatum () {
        return this.datum;
    }

    public void setDatum (LocalDateTime datum) {
        this.datum = datum;
    }
    
    public String getSadrzaj () {
        return this.sadrzaj;
    }

    public void setSadrzaj (String sadrzaj) {
        this.sadrzaj = sadrzaj;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public java.util.List<GodinaStudijaObavestenje> getGodineStudijaObavestenje () {
        return this.godineStudijaObavestenje;
    }

    public void setGodineStudijaObavestenje (java.util.List<GodinaStudijaObavestenje> godineStudijaObavestenje) {
        this.godineStudijaObavestenje = godineStudijaObavestenje;
    }
    
    public java.util.List<Fajl> getFajlovi () {
        return this.fajlovi;
    }

    public void setFajlovi (java.util.List<Fajl> fajlovi) {
        this.fajlovi = fajlovi;
    }
    


    public Obavestenje () {
        super();
        this.godineStudijaObavestenje = new java.util.ArrayList<>();
        this.fajlovi = new java.util.ArrayList<>();
    }

    public GodinaStudijaObavestenje getGodinaStudijaObavestenje (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addGodinaStudijaObavestenje (GodinaStudijaObavestenje godinaStudijaObavestenje) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeGodinaStudijaObavestenje (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public Fajl getFajl (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addFajl (Fajl fajl) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeFajl (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}