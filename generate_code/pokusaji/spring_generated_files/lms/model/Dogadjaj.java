package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE dogadjaj SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "dogadjaj")
public class Dogadjaj implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "dogadjaj_id")
    private Long id;
        
    private LocalDateTime pocetniDatum;
        
    private LocalDateTime krajnjiDatum;
        
    private String opis;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.OneToMany(mappedBy="dogadjaj")
    private java.util.List<DogadjajKalendar> dogadjajKalendari;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public LocalDateTime getPocetniDatum () {
        return this.pocetniDatum;
    }

    public void setPocetniDatum (LocalDateTime pocetniDatum) {
        this.pocetniDatum = pocetniDatum;
    }
    
    public LocalDateTime getKrajnjiDatum () {
        return this.krajnjiDatum;
    }

    public void setKrajnjiDatum (LocalDateTime krajnjiDatum) {
        this.krajnjiDatum = krajnjiDatum;
    }
    
    public String getOpis () {
        return this.opis;
    }

    public void setOpis (String opis) {
        this.opis = opis;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public java.util.List<DogadjajKalendar> getDogadjajKalendari () {
        return this.dogadjajKalendari;
    }

    public void setDogadjajKalendari (java.util.List<DogadjajKalendar> dogadjajKalendari) {
        this.dogadjajKalendari = dogadjajKalendari;
    }
    


    public Dogadjaj () {
        super();
        this.dogadjajKalendari = new java.util.ArrayList<>();
    }

    public DogadjajKalendar getDogadjajKalendar (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addDogadjajKalendar (DogadjajKalendar dogadjajKalendar) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeDogadjajKalendar (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}