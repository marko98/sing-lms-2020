package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE evaluacija_znanja_nacin_evaluacije SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "evaluacija_znanja_nacin_evaluacije")
public class EvaluacijaZnanjaNacinEvaluacije implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "evaluacija_znanja_nacin_evaluacije_id")
    private Long id;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "evaluacija_znanja_id")
    private EvaluacijaZnanja evaluacijaZnanja;
        
    @Enumerated(EnumType.STRING)
    private NacinEvaluacije nacinEvaluacije;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public EvaluacijaZnanja getEvaluacijaZnanja () {
        return this.evaluacijaZnanja;
    }

    public void setEvaluacijaZnanja (EvaluacijaZnanja evaluacijaZnanja) {
        this.evaluacijaZnanja = evaluacijaZnanja;
    }
    
    public NacinEvaluacije getNacinEvaluacije () {
        return this.nacinEvaluacije;
    }

    public void setNacinEvaluacije (NacinEvaluacije nacinEvaluacije) {
        this.nacinEvaluacije = nacinEvaluacije;
    }
    


    public EvaluacijaZnanjaNacinEvaluacije () {
        super();
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}