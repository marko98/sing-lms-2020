package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE pitanje SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "pitanje")
public class Pitanje implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "pitanje_id")
    private Long id;
        
    private String oblast;
        
    private String pitanje;
        
    private String putanjaZaSliku;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "evaluacija_znanja_id")
    private EvaluacijaZnanja evaluacijaZnanja;
        
    @javax.persistence.OneToMany(mappedBy="pitanje")
    private java.util.List<Odgovor> odgovori;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getOblast () {
        return this.oblast;
    }

    public void setOblast (String oblast) {
        this.oblast = oblast;
    }
    
    public String getPitanje () {
        return this.pitanje;
    }

    public void setPitanje (String pitanje) {
        this.pitanje = pitanje;
    }
    
    public String getPutanjaZaSliku () {
        return this.putanjaZaSliku;
    }

    public void setPutanjaZaSliku (String putanjaZaSliku) {
        this.putanjaZaSliku = putanjaZaSliku;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public EvaluacijaZnanja getEvaluacijaZnanja () {
        return this.evaluacijaZnanja;
    }

    public void setEvaluacijaZnanja (EvaluacijaZnanja evaluacijaZnanja) {
        this.evaluacijaZnanja = evaluacijaZnanja;
    }
    
    public java.util.List<Odgovor> getOdgovori () {
        return this.odgovori;
    }

    public void setOdgovori (java.util.List<Odgovor> odgovori) {
        this.odgovori = odgovori;
    }
    


    public Pitanje () {
        super();
        this.odgovori = new java.util.ArrayList<>();
    }

    public Odgovor getOdgovor (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addOdgovor (Odgovor odgovor) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeOdgovor (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}