package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE obrazovni_cilj SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "obrazovni_cilj")
public class ObrazovniCilj implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "obrazovni_cilj_id")
    private Long id;
        
    private String opis;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "realizacija_predmeta_id")
    private RealizacijaPredmeta realizacijaPredmeta;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "ishod_id")
    private Ishod ishod;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getOpis () {
        return this.opis;
    }

    public void setOpis (String opis) {
        this.opis = opis;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public RealizacijaPredmeta getRealizacijaPredmeta () {
        return this.realizacijaPredmeta;
    }

    public void setRealizacijaPredmeta (RealizacijaPredmeta realizacijaPredmeta) {
        this.realizacijaPredmeta = realizacijaPredmeta;
    }
    
    public Ishod getIshod () {
        return this.ishod;
    }

    public void setIshod (Ishod ishod) {
        this.ishod = ishod;
    }
    


    public ObrazovniCilj () {
        super();
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}