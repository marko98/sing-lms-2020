package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE grad SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "grad")
public class Grad implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "grad_id")
    private Long id;
        
    private String naziv;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "drzava_id")
    private Drzava drzava;
        
    @javax.persistence.OneToMany(mappedBy="grad")
    private java.util.List<Adresa> adrese;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getNaziv () {
        return this.naziv;
    }

    public void setNaziv (String naziv) {
        this.naziv = naziv;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public Drzava getDrzava () {
        return this.drzava;
    }

    public void setDrzava (Drzava drzava) {
        this.drzava = drzava;
    }
    
    public java.util.List<Adresa> getAdrese () {
        return this.adrese;
    }

    public void setAdrese (java.util.List<Adresa> adrese) {
        this.adrese = adrese;
    }
    


    public Grad () {
        super();
        this.adrese = new java.util.ArrayList<>();
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}