package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE evaluacija_znanja SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "evaluacija_znanja")
public class EvaluacijaZnanja implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "evaluacija_znanja_id")
    private Long id;
        
    private LocalDateTime pocetak;
        
    private Integer trajanje;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.OneToMany(mappedBy="evaluacijaZnanja")
    private java.util.List<NastavnikEvaluacijaZnanja> nastavniciEvaluacijaZnanja;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "realizacija_predmeta_id")
    private RealizacijaPredmeta realizacijaPredmeta;
        
    @javax.persistence.OneToMany(mappedBy="evaluacijaZnanja")
    private java.util.List<Polaganje> polaganja;
        
    @javax.persistence.OneToOne
    private Ishod ishod;
        
    @javax.persistence.OneToMany(mappedBy="evaluacijaZnanja")
    private java.util.List<Fajl> fajlovi;
        
    @Enumerated(EnumType.STRING)
    private TipEvaluacije tipEvaluacije;
        
    @javax.persistence.OneToMany(mappedBy="evaluacijaZnanja")
    private java.util.List<Pitanje> pitanja;
        
    @Enumerated(EnumType.STRING)
    private IspitniRok ispitniRok;
        
    @javax.persistence.OneToMany(mappedBy="evaluacijaZnanja")
    private java.util.List<EvaluacijaZnanjaNacinEvaluacije> evaluacijaZnanjaNaciniEvaluacije;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public LocalDateTime getPocetak () {
        return this.pocetak;
    }

    public void setPocetak (LocalDateTime pocetak) {
        this.pocetak = pocetak;
    }
    
    public Integer getTrajanje () {
        return this.trajanje;
    }

    public void setTrajanje (Integer trajanje) {
        this.trajanje = trajanje;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public java.util.List<NastavnikEvaluacijaZnanja> getNastavniciEvaluacijaZnanja () {
        return this.nastavniciEvaluacijaZnanja;
    }

    public void setNastavniciEvaluacijaZnanja (java.util.List<NastavnikEvaluacijaZnanja> nastavniciEvaluacijaZnanja) {
        this.nastavniciEvaluacijaZnanja = nastavniciEvaluacijaZnanja;
    }
    
    public RealizacijaPredmeta getRealizacijaPredmeta () {
        return this.realizacijaPredmeta;
    }

    public void setRealizacijaPredmeta (RealizacijaPredmeta realizacijaPredmeta) {
        this.realizacijaPredmeta = realizacijaPredmeta;
    }
    
    public java.util.List<Polaganje> getPolaganja () {
        return this.polaganja;
    }

    public void setPolaganja (java.util.List<Polaganje> polaganja) {
        this.polaganja = polaganja;
    }
    
    public Ishod getIshod () {
        return this.ishod;
    }

    public void setIshod (Ishod ishod) {
        this.ishod = ishod;
    }
    
    public java.util.List<Fajl> getFajlovi () {
        return this.fajlovi;
    }

    public void setFajlovi (java.util.List<Fajl> fajlovi) {
        this.fajlovi = fajlovi;
    }
    
    public TipEvaluacije getTipEvaluacije () {
        return this.tipEvaluacije;
    }

    public void setTipEvaluacije (TipEvaluacije tipEvaluacije) {
        this.tipEvaluacije = tipEvaluacije;
    }
    
    public java.util.List<Pitanje> getPitanja () {
        return this.pitanja;
    }

    public void setPitanja (java.util.List<Pitanje> pitanja) {
        this.pitanja = pitanja;
    }
    
    public IspitniRok getIspitniRok () {
        return this.ispitniRok;
    }

    public void setIspitniRok (IspitniRok ispitniRok) {
        this.ispitniRok = ispitniRok;
    }
    
    public java.util.List<EvaluacijaZnanjaNacinEvaluacije> getEvaluacijaZnanjaNaciniEvaluacije () {
        return this.evaluacijaZnanjaNaciniEvaluacije;
    }

    public void setEvaluacijaZnanjaNaciniEvaluacije (java.util.List<EvaluacijaZnanjaNacinEvaluacije> evaluacijaZnanjaNaciniEvaluacije) {
        this.evaluacijaZnanjaNaciniEvaluacije = evaluacijaZnanjaNaciniEvaluacije;
    }
    


    public EvaluacijaZnanja () {
        super();
        this.nastavniciEvaluacijaZnanja = new java.util.ArrayList<>();
        this.polaganja = new java.util.ArrayList<>();
        this.fajlovi = new java.util.ArrayList<>();
        this.pitanja = new java.util.ArrayList<>();
        this.evaluacijaZnanjaNaciniEvaluacije = new java.util.ArrayList<>();
    }

    public NastavnikEvaluacijaZnanja getNastavnikEvaluacijaZnanja (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addNastavnikEvaluacijaZnanja (NastavnikEvaluacijaZnanja nastavnikEvaluacijaZnanja) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeNastavnikEvaluacijaZnanja (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public Polaganje getPolaganje (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addPolaganje (Polaganje polaganje) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removePolaganje (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public EvaluacijaZnanjaNacinEvaluacije getEvaluacijaZnanjaNacinEvaluacije (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addEvaluacijaZnanjaNacinEvaluacije (EvaluacijaZnanjaNacinEvaluacije evaluacijaZnanjaNacinEvaluacije) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeEvaluacijaZnanjaNacinEvaluacije (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public Fajl getFajl (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addFajl (Fajl fajl) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeFajl (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public Pitanje getPitanje (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addPitanje (Pitanje pitanje) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removePitanje (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}