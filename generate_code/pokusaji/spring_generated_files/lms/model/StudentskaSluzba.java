package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE studentska_sluzba SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "studentska_sluzba")
public class StudentskaSluzba implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "studentska_sluzba_id")
    private Long id;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.OneToMany(mappedBy="studentskaSluzba")
    private java.util.List<StudentskiSluzbenik> studentskiSluzbenici;
        
    @javax.persistence.OneToOne(mappedBy="studentskaSluzba")
    private Univerzitet univerzitet;
        
    @javax.persistence.OneToOne(mappedBy="studentskaSluzba")
    private Odeljenje odeljenje;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public java.util.List<StudentskiSluzbenik> getStudentskiSluzbenici () {
        return this.studentskiSluzbenici;
    }

    public void setStudentskiSluzbenici (java.util.List<StudentskiSluzbenik> studentskiSluzbenici) {
        this.studentskiSluzbenici = studentskiSluzbenici;
    }
    
    public Univerzitet getUniverzitet () {
        return this.univerzitet;
    }

    public void setUniverzitet (Univerzitet univerzitet) {
        this.univerzitet = univerzitet;
    }
    
    public Odeljenje getOdeljenje () {
        return this.odeljenje;
    }

    public void setOdeljenje (Odeljenje odeljenje) {
        this.odeljenje = odeljenje;
    }
    


    public StudentskaSluzba () {
        super();
        this.studentskiSluzbenici = new java.util.ArrayList<>();
    }

    public StudentskiSluzbenik getStudentskiSluzbenik (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addStudentskiSluzbenik (StudentskiSluzbenik studentskiSluzbenik) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeStudentskiSluzbenik (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}