package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE studijski_program SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "studijski_program")
public class StudijskiProgram implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "studijski_program_id")
    private Long id;
        
    private String naziv;
        
    private Integer brojGodinaStudija;
        
    private String opis;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "fakultet_id")
    private Fakultet fakultet;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "nastavnik_id")
    private Nastavnik rukovodilac;
        
    @javax.persistence.OneToMany(mappedBy="studijskiProgram")
    private java.util.List<GodinaStudija> godineStudija;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getNaziv () {
        return this.naziv;
    }

    public void setNaziv (String naziv) {
        this.naziv = naziv;
    }
    
    public Integer getBrojGodinaStudija () {
        return this.brojGodinaStudija;
    }

    public void setBrojGodinaStudija (Integer brojGodinaStudija) {
        this.brojGodinaStudija = brojGodinaStudija;
    }
    
    public String getOpis () {
        return this.opis;
    }

    public void setOpis (String opis) {
        this.opis = opis;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public Fakultet getFakultet () {
        return this.fakultet;
    }

    public void setFakultet (Fakultet fakultet) {
        this.fakultet = fakultet;
    }
    
    public Nastavnik getRukovodilac () {
        return this.rukovodilac;
    }

    public void setRukovodilac (Nastavnik rukovodilac) {
        this.rukovodilac = rukovodilac;
    }
    
    public java.util.List<GodinaStudija> getGodineStudija () {
        return this.godineStudija;
    }

    public void setGodineStudija (java.util.List<GodinaStudija> godineStudija) {
        this.godineStudija = godineStudija;
    }
    


    public StudijskiProgram () {
        super();
        this.godineStudija = new java.util.ArrayList<>();
    }

    public GodinaStudija getGodinaStudija (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addGodinaStudija (GodinaStudija godinaStudija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeGodinaStudija (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}