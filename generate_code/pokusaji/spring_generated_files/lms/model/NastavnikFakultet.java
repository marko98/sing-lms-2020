package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE nastavnik_fakultet SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "nastavnik_fakultet")
public class NastavnikFakultet implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "nastavnik_fakultet_id")
    private Long id;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    private LocalDateTime datum;
        
    @Version
    private Long version;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "fakultet_id")
    private Fakultet fakultet;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "nastavnik_id")
    private Nastavnik nastavnik;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public LocalDateTime getDatum () {
        return this.datum;
    }

    public void setDatum (LocalDateTime datum) {
        this.datum = datum;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public Fakultet getFakultet () {
        return this.fakultet;
    }

    public void setFakultet (Fakultet fakultet) {
        this.fakultet = fakultet;
    }
    
    public Nastavnik getNastavnik () {
        return this.nastavnik;
    }

    public void setNastavnik (Nastavnik nastavnik) {
        this.nastavnik = nastavnik;
    }
    


    public NastavnikFakultet () {
        super();
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}