package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE nastavnik_na_realizaciji SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "nastavnik_na_realizaciji")
public class NastavnikNaRealizaciji implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "nastavnik_na_realizaciji_id")
    private Long id;
        
    private Integer brojCasova;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "realizacija_predmeta_id")
    private RealizacijaPredmeta realizacijaPredmeta;
        
    @Enumerated(EnumType.STRING)
    private Zvanje zvanje;
        
    @javax.persistence.OneToMany(mappedBy="nastavnikNaRealizaciji")
    private java.util.List<NastavnikNaRealizacijiTipNastave> nastavnikNaRealizacijiTipoviNastave;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "nastavnik_id")
    private Nastavnik nastavnik;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public Integer getBrojCasova () {
        return this.brojCasova;
    }

    public void setBrojCasova (Integer brojCasova) {
        this.brojCasova = brojCasova;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public RealizacijaPredmeta getRealizacijaPredmeta () {
        return this.realizacijaPredmeta;
    }

    public void setRealizacijaPredmeta (RealizacijaPredmeta realizacijaPredmeta) {
        this.realizacijaPredmeta = realizacijaPredmeta;
    }
    
    public Zvanje getZvanje () {
        return this.zvanje;
    }

    public void setZvanje (Zvanje zvanje) {
        this.zvanje = zvanje;
    }
    
    public java.util.List<NastavnikNaRealizacijiTipNastave> getNastavnikNaRealizacijiTipoviNastave () {
        return this.nastavnikNaRealizacijiTipoviNastave;
    }

    public void setNastavnikNaRealizacijiTipoviNastave (java.util.List<NastavnikNaRealizacijiTipNastave> nastavnikNaRealizacijiTipoviNastave) {
        this.nastavnikNaRealizacijiTipoviNastave = nastavnikNaRealizacijiTipoviNastave;
    }
    
    public Nastavnik getNastavnik () {
        return this.nastavnik;
    }

    public void setNastavnik (Nastavnik nastavnik) {
        this.nastavnik = nastavnik;
    }
    


    public NastavnikNaRealizaciji () {
        super();
        this.nastavnikNaRealizacijiTipoviNastave = new java.util.ArrayList<>();
    }

    public NastavnikNaRealizacijiTipNastave getNastavnikNaRealizacijiTipNastave (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addNastavnikNaRealizacijiTipNastave (NastavnikNaRealizacijiTipNastave nastavnikNaRealizacijiTipNastave) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeNastavnikNaRealizacijiTipNastave (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}