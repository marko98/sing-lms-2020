package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE registrovani_korisnik SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "registrovani_korisnik")
public class RegistrovaniKorisnik implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "registrovani_korisnik_id")
    private Long id;
        
    private String korisnickoIme;
        
    private String lozinka;
        
    private String email;
        
    private LocalDateTime datumRodjenja;
        
    private String jmbg;
        
    private String ime;
        
    private String prezime;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.OneToMany(mappedBy="registrovaniKorisnik")
    private java.util.List<Adresa> adrese;
        
    @javax.persistence.OneToOne(mappedBy="registrovaniKorisnik")
    private Nastavnik nastavnik;
        
    @javax.persistence.OneToOne(mappedBy="registrovaniKorisnik")
    private Administrator administrator;
        
    @javax.persistence.OneToOne(mappedBy="registrovaniKorisnik")
    private StudentskiSluzbenik studentskiSluzbenik;
        
    @javax.persistence.OneToOne(mappedBy="registrovaniKorisnik")
    private Student student;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getKorisnickoIme () {
        return this.korisnickoIme;
    }

    public void setKorisnickoIme (String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }
    
    public String getLozinka () {
        return this.lozinka;
    }

    public void setLozinka (String lozinka) {
        this.lozinka = lozinka;
    }
    
    public String getEmail () {
        return this.email;
    }

    public void setEmail (String email) {
        this.email = email;
    }
    
    public LocalDateTime getDatumRodjenja () {
        return this.datumRodjenja;
    }

    public void setDatumRodjenja (LocalDateTime datumRodjenja) {
        this.datumRodjenja = datumRodjenja;
    }
    
    public String getJmbg () {
        return this.jmbg;
    }

    public void setJmbg (String jmbg) {
        this.jmbg = jmbg;
    }
    
    public String getIme () {
        return this.ime;
    }

    public void setIme (String ime) {
        this.ime = ime;
    }
    
    public String getPrezime () {
        return this.prezime;
    }

    public void setPrezime (String prezime) {
        this.prezime = prezime;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public java.util.List<Adresa> getAdrese () {
        return this.adrese;
    }

    public void setAdrese (java.util.List<Adresa> adrese) {
        this.adrese = adrese;
    }
    
    public Nastavnik getNastavnik () {
        return this.nastavnik;
    }

    public void setNastavnik (Nastavnik nastavnik) {
        this.nastavnik = nastavnik;
    }
    
    public Administrator getAdministrator () {
        return this.administrator;
    }

    public void setAdministrator (Administrator administrator) {
        this.administrator = administrator;
    }
    
    public StudentskiSluzbenik getStudentskiSluzbenik () {
        return this.studentskiSluzbenik;
    }

    public void setStudentskiSluzbenik (StudentskiSluzbenik studentskiSluzbenik) {
        this.studentskiSluzbenik = studentskiSluzbenik;
    }
    
    public Student getStudent () {
        return this.student;
    }

    public void setStudent (Student student) {
        this.student = student;
    }
    


    public RegistrovaniKorisnik () {
        super();
        this.adrese = new java.util.ArrayList<>();
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}