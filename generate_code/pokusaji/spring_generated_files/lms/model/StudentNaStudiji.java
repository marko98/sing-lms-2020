package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE student_na_studiji SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "student_na_studiji")
public class StudentNaStudiji implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "student_na_studiji_id")
    private Long id;
        
    private LocalDateTime datumUpisa;
        
    private String brojIndeksa;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "student_id")
    private Student student;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "godina_studija_id")
    private GodinaStudija godinaStudija;
        
    @javax.persistence.OneToMany(mappedBy="studentNaStudiji")
    private java.util.List<PohadjanjePredmeta> pohadjanjaPredmeta;
        
    @javax.persistence.OneToMany(mappedBy="studentNaStudiji")
    private java.util.List<IstrazivackiRadStudentNaStudiji> istrazivackiRadoviStudentNaStudiji;
        
    @javax.persistence.OneToOne
    private DiplomskiRad diplomskiRad;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public LocalDateTime getDatumUpisa () {
        return this.datumUpisa;
    }

    public void setDatumUpisa (LocalDateTime datumUpisa) {
        this.datumUpisa = datumUpisa;
    }
    
    public String getBrojIndeksa () {
        return this.brojIndeksa;
    }

    public void setBrojIndeksa (String brojIndeksa) {
        this.brojIndeksa = brojIndeksa;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public Student getStudent () {
        return this.student;
    }

    public void setStudent (Student student) {
        this.student = student;
    }
    
    public GodinaStudija getGodinaStudija () {
        return this.godinaStudija;
    }

    public void setGodinaStudija (GodinaStudija godinaStudija) {
        this.godinaStudija = godinaStudija;
    }
    
    public java.util.List<PohadjanjePredmeta> getPohadjanjaPredmeta () {
        return this.pohadjanjaPredmeta;
    }

    public void setPohadjanjaPredmeta (java.util.List<PohadjanjePredmeta> pohadjanjaPredmeta) {
        this.pohadjanjaPredmeta = pohadjanjaPredmeta;
    }
    
    public java.util.List<IstrazivackiRadStudentNaStudiji> getIstrazivackiRadoviStudentNaStudiji () {
        return this.istrazivackiRadoviStudentNaStudiji;
    }

    public void setIstrazivackiRadoviStudentNaStudiji (java.util.List<IstrazivackiRadStudentNaStudiji> istrazivackiRadoviStudentNaStudiji) {
        this.istrazivackiRadoviStudentNaStudiji = istrazivackiRadoviStudentNaStudiji;
    }
    
    public DiplomskiRad getDiplomskiRad () {
        return this.diplomskiRad;
    }

    public void setDiplomskiRad (DiplomskiRad diplomskiRad) {
        this.diplomskiRad = diplomskiRad;
    }
    


    public StudentNaStudiji () {
        super();
        this.pohadjanjaPredmeta = new java.util.ArrayList<>();
        this.istrazivackiRadoviStudentNaStudiji = new java.util.ArrayList<>();
    }

    public PohadjanjePredmeta getPohadjanjePredmeta (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addPohadjanjePredmeta (PohadjanjePredmeta pohadjanjePredmeta) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removePohadjanjePredmeta (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public IstrazivackiRadStudentNaStudiji getIstrazivackiRadStudentNaStudiji (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addIstrazivackiRadStudentNaStudiji (IstrazivackiRadStudentNaStudiji istrazivackiRadStudentNaStudiji) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeIstrazivackiRadStudentNaStudiji (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}