package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE studentski_sluzbenik SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "studentski_sluzbenik")
public class StudentskiSluzbenik implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "studentski_sluzbenik_id")
    private Long id;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.OneToOne
    private RegistrovaniKorisnik registrovaniKorisnik;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "studentska_sluzba_id")
    private StudentskaSluzba studentskaSluzba;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public RegistrovaniKorisnik getRegistrovaniKorisnik () {
        return this.registrovaniKorisnik;
    }

    public void setRegistrovaniKorisnik (RegistrovaniKorisnik registrovaniKorisnik) {
        this.registrovaniKorisnik = registrovaniKorisnik;
    }
    
    public StudentskaSluzba getStudentskaSluzba () {
        return this.studentskaSluzba;
    }

    public void setStudentskaSluzba (StudentskaSluzba studentskaSluzba) {
        this.studentskaSluzba = studentskaSluzba;
    }
    


    public StudentskiSluzbenik () {
        super();
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.NEAKTIVAN; // aktivira ga administrator
    }

}