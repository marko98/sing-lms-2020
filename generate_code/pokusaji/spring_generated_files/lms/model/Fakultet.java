package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE fakultet SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "fakultet")
public class Fakultet implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "fakultet_id")
    private Long id;
        
    private String naziv;
        
    private String opis;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.OneToMany(mappedBy="fakultet")
    private java.util.List<Odeljenje> odeljenja;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "univerzitet_id")
    private Univerzitet univerzitet;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "nastavnik_id")
    private Nastavnik dekan;
        
    @javax.persistence.OneToMany(mappedBy="fakultet")
    private java.util.List<NastavnikFakultet> nastavniciFakultet;
        
    @javax.persistence.OneToMany(mappedBy="fakultet")
    private java.util.List<StudijskiProgram> studijskiProgrami;
        
    @javax.persistence.OneToMany(mappedBy="fakultet")
    private java.util.List<Kontakt> kontakti;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getNaziv () {
        return this.naziv;
    }

    public void setNaziv (String naziv) {
        this.naziv = naziv;
    }
    
    public String getOpis () {
        return this.opis;
    }

    public void setOpis (String opis) {
        this.opis = opis;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public java.util.List<Odeljenje> getOdeljenja () {
        return this.odeljenja;
    }

    public void setOdeljenja (java.util.List<Odeljenje> odeljenja) {
        this.odeljenja = odeljenja;
    }
    
    public Univerzitet getUniverzitet () {
        return this.univerzitet;
    }

    public void setUniverzitet (Univerzitet univerzitet) {
        this.univerzitet = univerzitet;
    }
    
    public Nastavnik getDekan () {
        return this.dekan;
    }

    public void setDekan (Nastavnik dekan) {
        this.dekan = dekan;
    }
    
    public java.util.List<NastavnikFakultet> getNastavniciFakultet () {
        return this.nastavniciFakultet;
    }

    public void setNastavniciFakultet (java.util.List<NastavnikFakultet> nastavniciFakultet) {
        this.nastavniciFakultet = nastavniciFakultet;
    }
    
    public java.util.List<StudijskiProgram> getStudijskiProgrami () {
        return this.studijskiProgrami;
    }

    public void setStudijskiProgrami (java.util.List<StudijskiProgram> studijskiProgrami) {
        this.studijskiProgrami = studijskiProgrami;
    }
    
    public java.util.List<Kontakt> getKontakti () {
        return this.kontakti;
    }

    public void setKontakti (java.util.List<Kontakt> kontakti) {
        this.kontakti = kontakti;
    }
    


    public Fakultet () {
        super();
        this.odeljenja = new java.util.ArrayList<>();
        this.nastavniciFakultet = new java.util.ArrayList<>();
        this.studijskiProgrami = new java.util.ArrayList<>();
        this.kontakti = new java.util.ArrayList<>();
    }

    public Odeljenje getOdeljenje (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addOdeljenje (Odeljenje odeljenje) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeOdeljenje (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public StudijskiProgram getStudijskiProgram (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addStudijskiProgram (StudijskiProgram studijskiProgram) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeStudijskiProgram (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public NastavnikFakultet getNastavnikFakultet (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addNastavnikFakultet (NastavnikFakultet nastavnikFakultet) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeNastavnikFakultet (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public Kontakt getKontakt (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addKontakt (Kontakt kontakt) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeKontakt (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}