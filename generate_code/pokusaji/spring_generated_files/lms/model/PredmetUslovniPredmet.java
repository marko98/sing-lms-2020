package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE predmet_uslovni_predmet SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "predmet_uslovni_predmet")
public class PredmetUslovniPredmet implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "predmet_uslovni_predmet_id")
    private Long id;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "predmet_id")
    private Predmet uslov;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "predmet_id")
    private Predmet predmet;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public Predmet getUslov () {
        return this.uslov;
    }

    public void setUslov (Predmet uslov) {
        this.uslov = uslov;
    }
    
    public Predmet getPredmet () {
        return this.predmet;
    }

    public void setPredmet (Predmet predmet) {
        this.predmet = predmet;
    }
    


    public PredmetUslovniPredmet () {
        super();
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}