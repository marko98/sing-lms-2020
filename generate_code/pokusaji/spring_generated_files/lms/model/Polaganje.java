package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE polaganje SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "polaganje")
public class Polaganje implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "polaganje_id")
    private Long id;
        
    private Integer bodovi;
        
    private String napomena;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "pohadjanje_predmeta_id")
    private PohadjanjePredmeta pohadjanjePredmeta;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "evaluacija_znanja_id")
    private EvaluacijaZnanja evaluacijaZnanja;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public Integer getBodovi () {
        return this.bodovi;
    }

    public void setBodovi (Integer bodovi) {
        this.bodovi = bodovi;
    }
    
    public String getNapomena () {
        return this.napomena;
    }

    public void setNapomena (String napomena) {
        this.napomena = napomena;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public PohadjanjePredmeta getPohadjanjePredmeta () {
        return this.pohadjanjePredmeta;
    }

    public void setPohadjanjePredmeta (PohadjanjePredmeta pohadjanjePredmeta) {
        this.pohadjanjePredmeta = pohadjanjePredmeta;
    }
    
    public EvaluacijaZnanja getEvaluacijaZnanja () {
        return this.evaluacijaZnanja;
    }

    public void setEvaluacijaZnanja (EvaluacijaZnanja evaluacijaZnanja) {
        this.evaluacijaZnanja = evaluacijaZnanja;
    }
    


    public Polaganje () {
        super();
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}