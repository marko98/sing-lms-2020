package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE prostorija SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "prostorija")
public class Prostorija implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "prostorija_id")
    private Long id;
        
    private String naziv;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @Enumerated(EnumType.STRING)
    private TipProstorije tip;
        
    @javax.persistence.OneToMany(mappedBy="prostorija")
    private java.util.List<TerminNastave> terminiNastave;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "odeljenje_id")
    private Odeljenje odeljenje;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getNaziv () {
        return this.naziv;
    }

    public void setNaziv (String naziv) {
        this.naziv = naziv;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public TipProstorije getTip () {
        return this.tip;
    }

    public void setTip (TipProstorije tip) {
        this.tip = tip;
    }
    
    public java.util.List<TerminNastave> getTerminiNastave () {
        return this.terminiNastave;
    }

    public void setTerminiNastave (java.util.List<TerminNastave> terminiNastave) {
        this.terminiNastave = terminiNastave;
    }
    
    public Odeljenje getOdeljenje () {
        return this.odeljenje;
    }

    public void setOdeljenje (Odeljenje odeljenje) {
        this.odeljenje = odeljenje;
    }
    


    public Prostorija () {
        super();
        this.terminiNastave = new java.util.ArrayList<>();
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}