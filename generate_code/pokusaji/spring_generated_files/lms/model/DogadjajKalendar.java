package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE dogadjaj_kalendar SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "dogadjaj_kalendar")
public class DogadjajKalendar implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "dogadjaj_kalendar_id")
    private Long id;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @Version
    private Long version;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "kalendar_id")
    private Kalendar kalendar;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "dogadjaj_id")
    private Dogadjaj dogadjaj;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public Kalendar getKalendar () {
        return this.kalendar;
    }

    public void setKalendar (Kalendar kalendar) {
        this.kalendar = kalendar;
    }
    
    public Dogadjaj getDogadjaj () {
        return this.dogadjaj;
    }

    public void setDogadjaj (Dogadjaj dogadjaj) {
        this.dogadjaj = dogadjaj;
    }
    


    public DogadjajKalendar () {
        super();
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}