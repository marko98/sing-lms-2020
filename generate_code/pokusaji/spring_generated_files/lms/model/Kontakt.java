package lms.model;


@javax.persistence.Entity

@SQLDelete(sql = "UPDATE kontakt SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "kontakt")
public class Kontakt implements Entitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "kontakt_id")
    private Long id;
        
    private String mobilni;
        
    private String fiksni;
        
    @Version
    private Long version;
        
    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "univerzitet_id")
    private Univerzitet univerzitet;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "fakultet_id")
    private Fakultet fakultet;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getMobilni () {
        return this.mobilni;
    }

    public void setMobilni (String mobilni) {
        this.mobilni = mobilni;
    }
    
    public String getFiksni () {
        return this.fiksni;
    }

    public void setFiksni (String fiksni) {
        this.fiksni = fiksni;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Univerzitet getUniverzitet () {
        return this.univerzitet;
    }

    public void setUniverzitet (Univerzitet univerzitet) {
        this.univerzitet = univerzitet;
    }
    
    public Fakultet getFakultet () {
        return this.fakultet;
    }

    public void setFakultet (Fakultet fakultet) {
        this.fakultet = fakultet;
    }
    


    public Kontakt () {
        super();
    }

    @PreRemove
    public void onDelete () {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist () {
	this.stanje = StanjeModela.AKTIVAN;
    }

}