package lms.service;


@Service

public abstract class CrudService<T extends Entitet, ID extends Serializable> {

    @Autowired
    protected CrudRepository repository;
        


    public CrudRepository getRepository () {
        return this.repository;
    }

    public void setRepository (CrudRepository repository) {
        this.repository = repository;
    }
    


    public Page<T> findAll (Pageable pageable) {
	return this.repository.findAll(pageable);
    }

    public Iterable<T> findAll () {
	return this.repository.findAll();
    }

    public Optional<T> findOne (ID id) {
	return this.repository.findOne(id);
    }

    public void save (T model) {
	this.repository.save(model);
    }

    public void delete (ID id) {
	this.repository.delete(id);
    }

    public void delete (T model) {
	this.repository.delete(model);
    }

}