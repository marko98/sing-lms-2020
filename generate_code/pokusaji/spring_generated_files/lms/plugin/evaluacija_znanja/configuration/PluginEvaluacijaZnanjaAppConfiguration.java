package lms.plugin.evaluacija_znanja.configuration;


@Configuration

public class PluginEvaluacijaZnanjaAppConfiguration {





    @Bean
    public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer () {
	PropertySourcesPlaceholderConfigurer properties = new PropertySourcesPlaceholderConfigurer();
	properties.setLocation(new ClassPathResource("/lms/plugin/evaluacija_znanja/configuration/plugin.evaluacija_znanja.application.properties"));
	properties.setIgnoreResourceNotFound(false);
	return properties;
    }

}