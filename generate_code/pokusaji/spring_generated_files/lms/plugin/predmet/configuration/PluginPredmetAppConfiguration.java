package lms.plugin.predmet.configuration;


@Configuration

public class PluginPredmetAppConfiguration {





    @Bean
    public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer () {
	PropertySourcesPlaceholderConfigurer properties = new PropertySourcesPlaceholderConfigurer();
	properties.setLocation(new ClassPathResource("/lms/plugin/predmet/configuration/plugin.predmet.application.properties"));
	properties.setIgnoreResourceNotFound(false);
	return properties;
    }

}