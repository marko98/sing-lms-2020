package lms.plugin.fakultet.configuration;


@Configuration

public class PluginFakultetAppConfiguration {





    @Bean
    public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer () {
	PropertySourcesPlaceholderConfigurer properties = new PropertySourcesPlaceholderConfigurer();
	properties.setLocation(new ClassPathResource("/lms/plugin/fakultet/configuration/plugin.fakultet.application.properties"));
	properties.setIgnoreResourceNotFound(false);
	return properties;
    }

}