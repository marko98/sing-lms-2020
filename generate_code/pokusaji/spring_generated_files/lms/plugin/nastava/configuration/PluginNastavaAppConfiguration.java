package lms.plugin.nastava.configuration;


@Configuration

public class PluginNastavaAppConfiguration {





    @Bean
    public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer () {
	PropertySourcesPlaceholderConfigurer properties = new PropertySourcesPlaceholderConfigurer();
	properties.setLocation(new ClassPathResource("/lms/plugin/nastava/configuration/plugin.nastava.application.properties"));
	properties.setIgnoreResourceNotFound(false);
	return properties;
    }

}