package lms.plugin.korisnik.configuration;


@Configuration

public class PluginKorisnikAppConfiguration {





    @Bean
    public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer () {
	PropertySourcesPlaceholderConfigurer properties = new PropertySourcesPlaceholderConfigurer();
	properties.setLocation(new ClassPathResource("/lms/plugin/korisnik/configuration/plugin.korisnik.application.properties"));
	properties.setIgnoreResourceNotFound(false);
	return properties;
    }

}