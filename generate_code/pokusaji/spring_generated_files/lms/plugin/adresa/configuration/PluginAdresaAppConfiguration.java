package lms.plugin.adresa.configuration;


@Configuration

public class PluginAdresaAppConfiguration {





    @Bean
    public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer () {
	PropertySourcesPlaceholderConfigurer properties = new PropertySourcesPlaceholderConfigurer();
	properties.setLocation(new ClassPathResource("/lms/plugin/adresa/configuration/plugin.adresa.application.properties"));
	properties.setIgnoreResourceNotFound(false);
	return properties;
    }

}