package lms.plugin.student.configuration;


@Configuration

public class PluginStudentAppConfiguration {





    @Bean
    public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer () {
	PropertySourcesPlaceholderConfigurer properties = new PropertySourcesPlaceholderConfigurer();
	properties.setLocation(new ClassPathResource("/lms/plugin/student/configuration/plugin.student.application.properties"));
	properties.setIgnoreResourceNotFound(false);
	return properties;
    }

}