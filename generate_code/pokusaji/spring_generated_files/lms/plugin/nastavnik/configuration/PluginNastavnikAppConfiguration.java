package lms.plugin.nastavnik.configuration;


@Configuration

public class PluginNastavnikAppConfiguration {





    @Bean
    public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer () {
	PropertySourcesPlaceholderConfigurer properties = new PropertySourcesPlaceholderConfigurer();
	properties.setLocation(new ClassPathResource("/lms/plugin/nastavnik/configuration/plugin.nastavnik.application.properties"));
	properties.setIgnoreResourceNotFound(false);
	return properties;
    }

}