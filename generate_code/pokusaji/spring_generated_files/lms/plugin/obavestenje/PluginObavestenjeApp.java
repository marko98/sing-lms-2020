package lms.plugin.obavestenje;


@SpringBootApplication
@ComponentScan(basePackages={"lms.repository", "lms.service", "lms.controller", "lms.hbsf", "lms.plugin.obavestenje"})
@EntityScan(basePackages={"lms.model"})
@EnableJpaRepositories("lms.plugin.obavestenje.repository")
@EnableJms

public class PluginObavestenjeApp extends SpringBootServletInitializer {





    public static void main (String args[]) {
	RestTemplate rt = new RestTemplate();
	
//	kreiramo opis plugin-a
	PluginDescription pluginDescription = new PluginDescription("Obavestenje", "Plugin za logicku celinu obavestenje", "http", "localhost:8108", new HashMap<String, HashMap<HttpMethod, HashMap<String, String>>>());

//	dodajemo kategorije koje plugin pruza
	ArrayList<String> categories = new ArrayList<String>();
	categories.add("ishod");
	categories.add("fajl");
	categories.add("obavestenje");
	categories.add("godina_studija_obavestenje");
	pluginDescription.setCategories(categories);
	
	for (String category : categories) {
//		dodajemo spisak endpoint-a u opis plugin-a
	    pluginDescription.getEndpoints().put(category, new HashMap<HttpMethod, HashMap<String, String>>());
	    pluginDescription.getEndpoints().get(category).put(HttpMethod.GET, new HashMap<String, String>());
	    pluginDescription.getEndpoints().get(category).get(HttpMethod.GET).putIfAbsent("findAll",
		    "/api/" + category);
	    pluginDescription.getEndpoints().get(category).get(HttpMethod.GET).putIfAbsent("findOne",
		    "/api/" + category + "/");

	    pluginDescription.getEndpoints().get(category).put(HttpMethod.POST, new HashMap<String, String>());
	    pluginDescription.getEndpoints().get(category).get(HttpMethod.POST).putIfAbsent("create",
		    "/api/" + category);

	    pluginDescription.getEndpoints().get(category).put(HttpMethod.PUT, new HashMap<String, String>());
	    pluginDescription.getEndpoints().get(category).get(HttpMethod.PUT).putIfAbsent("update",
		    "/api/" + category);

	    pluginDescription.getEndpoints().get(category).put(HttpMethod.DELETE, new HashMap<String, String>());
	    pluginDescription.getEndpoints().get(category).get(HttpMethod.DELETE).putIfAbsent("delete",
		    "/api/" + category + "/");
	}
	
//	registracija plugin-a na host-u
	rt.postForLocation("http://localhost:8080/api/plugins", pluginDescription);
	
//	pokretanje plugin-a
	SpringApplication.run(PluginObavestenjeApp.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure (SpringApplicationBuilder builder) {
		return builder.sources(PluginObavestenjeApp.class);
    }

}