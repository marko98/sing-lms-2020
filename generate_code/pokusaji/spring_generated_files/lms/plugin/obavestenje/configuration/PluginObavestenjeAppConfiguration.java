package lms.plugin.obavestenje.configuration;


@Configuration

public class PluginObavestenjeAppConfiguration {





    @Bean
    public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer () {
	PropertySourcesPlaceholderConfigurer properties = new PropertySourcesPlaceholderConfigurer();
	properties.setLocation(new ClassPathResource("/lms/plugin/obavestenje/configuration/plugin.obavestenje.application.properties"));
	properties.setIgnoreResourceNotFound(false);
	return properties;
    }

}