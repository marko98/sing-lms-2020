package lms.plugin.administracija.configuration;


@Configuration

public class PluginAdministracijaAppConfiguration {





    @Bean
    public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer () {
	PropertySourcesPlaceholderConfigurer properties = new PropertySourcesPlaceholderConfigurer();
	properties.setLocation(new ClassPathResource("/lms/plugin/administracija/configuration/plugin.administracija.application.properties"));
	properties.setIgnoreResourceNotFound(false);
	return properties;
    }

}