package lms.dto;



public class AdresaDTO implements DTO {

    private Long id;
        
    private String ulica;
        
    private String broj;
        
    private StanjeModela stanje;
        
    private Long version;
        
    private TipAdrese tip;
        
    private GradDTO gradDTO;
        
    private OdeljenjeDTO odeljenjeDTO;
        
    private UniverzitetDTO univerzitetDTO;
        
    private RegistrovaniKorisnikDTO registrovaniKorisnikDTO;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getUlica () {
        return this.ulica;
    }

    public void setUlica (String ulica) {
        this.ulica = ulica;
    }
    
    public String getBroj () {
        return this.broj;
    }

    public void setBroj (String broj) {
        this.broj = broj;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public TipAdrese getTip () {
        return this.tip;
    }

    public void setTip (TipAdrese tip) {
        this.tip = tip;
    }
    
    public GradDTO getGradDTO () {
        return this.gradDTO;
    }

    public void setGradDTO (GradDTO gradDTO) {
        this.gradDTO = gradDTO;
    }
    
    public OdeljenjeDTO getOdeljenjeDTO () {
        return this.odeljenjeDTO;
    }

    public void setOdeljenjeDTO (OdeljenjeDTO odeljenjeDTO) {
        this.odeljenjeDTO = odeljenjeDTO;
    }
    
    public UniverzitetDTO getUniverzitetDTO () {
        return this.univerzitetDTO;
    }

    public void setUniverzitetDTO (UniverzitetDTO univerzitetDTO) {
        this.univerzitetDTO = univerzitetDTO;
    }
    
    public RegistrovaniKorisnikDTO getRegistrovaniKorisnikDTO () {
        return this.registrovaniKorisnikDTO;
    }

    public void setRegistrovaniKorisnikDTO (RegistrovaniKorisnikDTO registrovaniKorisnikDTO) {
        this.registrovaniKorisnikDTO = registrovaniKorisnikDTO;
    }
    


    public AdresaDTO () {
        super();
    }

}