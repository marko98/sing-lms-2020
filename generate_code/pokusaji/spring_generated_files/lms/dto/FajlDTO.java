package lms.dto;



public class FajlDTO implements DTO {

    private Long id;
        
    private String opis;
        
    private String url;
        
    private StanjeModela stanje;
        
    private Long version;
        
    private String naziv;
        
    private String tip;
        
    private EvaluacijaZnanjaDTO evaluacijaZnanjaDTO;
        
    private ObavestenjeDTO obavestenjeDTO;
        
    private NastavniMaterijalDTO nastavniMaterijalDTO;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getOpis () {
        return this.opis;
    }

    public void setOpis (String opis) {
        this.opis = opis;
    }
    
    public String getUrl () {
        return this.url;
    }

    public void setUrl (String url) {
        this.url = url;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public String getNaziv () {
        return this.naziv;
    }

    public void setNaziv (String naziv) {
        this.naziv = naziv;
    }
    
    public String getTip () {
        return this.tip;
    }

    public void setTip (String tip) {
        this.tip = tip;
    }
    
    public EvaluacijaZnanjaDTO getEvaluacijaZnanjaDTO () {
        return this.evaluacijaZnanjaDTO;
    }

    public void setEvaluacijaZnanjaDTO (EvaluacijaZnanjaDTO evaluacijaZnanjaDTO) {
        this.evaluacijaZnanjaDTO = evaluacijaZnanjaDTO;
    }
    
    public ObavestenjeDTO getObavestenjeDTO () {
        return this.obavestenjeDTO;
    }

    public void setObavestenjeDTO (ObavestenjeDTO obavestenjeDTO) {
        this.obavestenjeDTO = obavestenjeDTO;
    }
    
    public NastavniMaterijalDTO getNastavniMaterijalDTO () {
        return this.nastavniMaterijalDTO;
    }

    public void setNastavniMaterijalDTO (NastavniMaterijalDTO nastavniMaterijalDTO) {
        this.nastavniMaterijalDTO = nastavniMaterijalDTO;
    }
    


    public FajlDTO () {
        super();
    }

}