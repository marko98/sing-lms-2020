package lms.dto;



public class DrzavaDTO implements DTO {

    private Long id;
        
    private String naziv;
        
    private StanjeModela stanje;
        
    private Long version;
        
    private java.util.List<GradDTO> gradoviDTO;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getNaziv () {
        return this.naziv;
    }

    public void setNaziv (String naziv) {
        this.naziv = naziv;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public java.util.List<GradDTO> getGradoviDTO () {
        return this.gradoviDTO;
    }

    public void setGradoviDTO (java.util.List<GradDTO> gradoviDTO) {
        this.gradoviDTO = gradoviDTO;
    }
    


    public DrzavaDTO () {
        super();
        this.gradoviDTO = new java.util.ArrayList<>();
    }

}