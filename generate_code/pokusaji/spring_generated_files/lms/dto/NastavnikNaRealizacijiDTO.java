package lms.dto;



public class NastavnikNaRealizacijiDTO implements DTO {

    private Long id;
        
    private Integer brojCasova;
        
    private StanjeModela stanje;
        
    private Long version;
        
    private Zvanje zvanje;
        
    private RealizacijaPredmetaDTO realizacijaPredmetaDTO;
        
    private java.util.List<NastavnikNaRealizacijiTipNastaveDTO> nastavnikNaRealizacijiTipoviNastaveDTO;
        
    private NastavnikDTO nastavnikDTO;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public Integer getBrojCasova () {
        return this.brojCasova;
    }

    public void setBrojCasova (Integer brojCasova) {
        this.brojCasova = brojCasova;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public Zvanje getZvanje () {
        return this.zvanje;
    }

    public void setZvanje (Zvanje zvanje) {
        this.zvanje = zvanje;
    }
    
    public RealizacijaPredmetaDTO getRealizacijaPredmetaDTO () {
        return this.realizacijaPredmetaDTO;
    }

    public void setRealizacijaPredmetaDTO (RealizacijaPredmetaDTO realizacijaPredmetaDTO) {
        this.realizacijaPredmetaDTO = realizacijaPredmetaDTO;
    }
    
    public java.util.List<NastavnikNaRealizacijiTipNastaveDTO> getNastavnikNaRealizacijiTipoviNastaveDTO () {
        return this.nastavnikNaRealizacijiTipoviNastaveDTO;
    }

    public void setNastavnikNaRealizacijiTipoviNastaveDTO (java.util.List<NastavnikNaRealizacijiTipNastaveDTO> nastavnikNaRealizacijiTipoviNastaveDTO) {
        this.nastavnikNaRealizacijiTipoviNastaveDTO = nastavnikNaRealizacijiTipoviNastaveDTO;
    }
    
    public NastavnikDTO getNastavnikDTO () {
        return this.nastavnikDTO;
    }

    public void setNastavnikDTO (NastavnikDTO nastavnikDTO) {
        this.nastavnikDTO = nastavnikDTO;
    }
    


    public NastavnikNaRealizacijiDTO () {
        super();
        this.nastavnikNaRealizacijiTipoviNastaveDTO = new java.util.ArrayList<>();
    }

    public NastavnikNaRealizacijiTipNastaveDTO getNastavnikNaRealizacijiTipNastaveDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addNastavnikNaRealizacijiTipNastaveDTO (NastavnikNaRealizacijiTipNastaveDTO nastavnikNaRealizacijiTipNastaveDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeNastavnikNaRealizacijiTipNastaveDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

}