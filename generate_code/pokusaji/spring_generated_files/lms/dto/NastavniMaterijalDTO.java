package lms.dto;



public class NastavniMaterijalDTO implements DTO {

    private Long id;
        
    private String naziv;
        
    private LocalDateTime datum;
        
    private StanjeModela stanje;
        
    private Long version;
        
    private IshodDTO ishodDTO;
        
    private java.util.List<FajlDTO> fajloviDTO;
        
    private java.util.List<AutorNastavniMaterijalDTO> autoriNastavniMaterijalDTO;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getNaziv () {
        return this.naziv;
    }

    public void setNaziv (String naziv) {
        this.naziv = naziv;
    }
    
    public LocalDateTime getDatum () {
        return this.datum;
    }

    public void setDatum (LocalDateTime datum) {
        this.datum = datum;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public IshodDTO getIshodDTO () {
        return this.ishodDTO;
    }

    public void setIshodDTO (IshodDTO ishodDTO) {
        this.ishodDTO = ishodDTO;
    }
    
    public java.util.List<FajlDTO> getFajloviDTO () {
        return this.fajloviDTO;
    }

    public void setFajloviDTO (java.util.List<FajlDTO> fajloviDTO) {
        this.fajloviDTO = fajloviDTO;
    }
    
    public java.util.List<AutorNastavniMaterijalDTO> getAutoriNastavniMaterijalDTO () {
        return this.autoriNastavniMaterijalDTO;
    }

    public void setAutoriNastavniMaterijalDTO (java.util.List<AutorNastavniMaterijalDTO> autoriNastavniMaterijalDTO) {
        this.autoriNastavniMaterijalDTO = autoriNastavniMaterijalDTO;
    }
    


    public NastavniMaterijalDTO () {
        super();
        this.fajloviDTO = new java.util.ArrayList<>();
        this.autoriNastavniMaterijalDTO = new java.util.ArrayList<>();
    }

    public FajlDTO getFajlDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addFajlDTO (FajlDTO fajlDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeFajlDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public AutorNastavniMaterijalDTO getAutorNastavniMaterijalDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addAutorNastavniMaterijalDTO (AutorNastavniMaterijalDTO autorNastavniMaterijalDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeAutorNastavniMaterijalDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

}