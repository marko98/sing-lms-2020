package lms.dto;



public class NastavnikFakultetDTO implements DTO {

    private Long id;
        
    private StanjeModela stanje;
        
    private LocalDateTime datum;
        
    private Long version;
        
    private FakultetDTO fakultetDTO;
        
    private NastavnikDTO nastavnikDTO;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public LocalDateTime getDatum () {
        return this.datum;
    }

    public void setDatum (LocalDateTime datum) {
        this.datum = datum;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public FakultetDTO getFakultetDTO () {
        return this.fakultetDTO;
    }

    public void setFakultetDTO (FakultetDTO fakultetDTO) {
        this.fakultetDTO = fakultetDTO;
    }
    
    public NastavnikDTO getNastavnikDTO () {
        return this.nastavnikDTO;
    }

    public void setNastavnikDTO (NastavnikDTO nastavnikDTO) {
        this.nastavnikDTO = nastavnikDTO;
    }
    


    public NastavnikFakultetDTO () {
        super();
    }

}