package lms.dto;



public class GradDTO implements DTO {

    private Long id;
        
    private String naziv;
        
    private StanjeModela stanje;
        
    private Long version;
        
    private DrzavaDTO drzavaDTO;
        
    private java.util.List<AdresaDTO> adreseDTO;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getNaziv () {
        return this.naziv;
    }

    public void setNaziv (String naziv) {
        this.naziv = naziv;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public DrzavaDTO getDrzavaDTO () {
        return this.drzavaDTO;
    }

    public void setDrzavaDTO (DrzavaDTO drzavaDTO) {
        this.drzavaDTO = drzavaDTO;
    }
    
    public java.util.List<AdresaDTO> getAdreseDTO () {
        return this.adreseDTO;
    }

    public void setAdreseDTO (java.util.List<AdresaDTO> adreseDTO) {
        this.adreseDTO = adreseDTO;
    }
    


    public GradDTO () {
        super();
        this.adreseDTO = new java.util.ArrayList<>();
    }

}