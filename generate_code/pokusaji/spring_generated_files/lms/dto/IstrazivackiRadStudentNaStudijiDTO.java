package lms.dto;



public class IstrazivackiRadStudentNaStudijiDTO implements DTO {

    private Long id;
        
    private LocalDateTime datum;
        
    private StanjeModela stanje;
        
    private Long version;
        
    private StudentNaStudijiDTO studentNaStudijiDTO;
        
    private IstrazivackiRadDTO istrazivackiRadDTO;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public LocalDateTime getDatum () {
        return this.datum;
    }

    public void setDatum (LocalDateTime datum) {
        this.datum = datum;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public StudentNaStudijiDTO getStudentNaStudijiDTO () {
        return this.studentNaStudijiDTO;
    }

    public void setStudentNaStudijiDTO (StudentNaStudijiDTO studentNaStudijiDTO) {
        this.studentNaStudijiDTO = studentNaStudijiDTO;
    }
    
    public IstrazivackiRadDTO getIstrazivackiRadDTO () {
        return this.istrazivackiRadDTO;
    }

    public void setIstrazivackiRadDTO (IstrazivackiRadDTO istrazivackiRadDTO) {
        this.istrazivackiRadDTO = istrazivackiRadDTO;
    }
    


    public IstrazivackiRadStudentNaStudijiDTO () {
        super();
    }

}