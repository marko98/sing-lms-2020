package lms.dto;



public class GodinaStudijaDTO implements DTO {

    private Long id;
        
    private LocalDateTime godina;
        
    private StanjeModela stanje;
        
    private Long version;
        
    private Semestar semestar;
        
    private StudijskiProgramDTO studijskiProgramDTO;
        
    private java.util.List<StudentNaStudijiDTO> studentiNaStudijiDTO;
        
    private java.util.List<PredmetDTO> predmetiDTO;
        
    private java.util.List<GodinaStudijaObavestenjeDTO> godinaStudijaObavestenjaDTO;
        
    private java.util.List<KonsultacijaDTO> konsultacijeDTO;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public LocalDateTime getGodina () {
        return this.godina;
    }

    public void setGodina (LocalDateTime godina) {
        this.godina = godina;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public Semestar getSemestar () {
        return this.semestar;
    }

    public void setSemestar (Semestar semestar) {
        this.semestar = semestar;
    }
    
    public StudijskiProgramDTO getStudijskiProgramDTO () {
        return this.studijskiProgramDTO;
    }

    public void setStudijskiProgramDTO (StudijskiProgramDTO studijskiProgramDTO) {
        this.studijskiProgramDTO = studijskiProgramDTO;
    }
    
    public java.util.List<StudentNaStudijiDTO> getStudentiNaStudijiDTO () {
        return this.studentiNaStudijiDTO;
    }

    public void setStudentiNaStudijiDTO (java.util.List<StudentNaStudijiDTO> studentiNaStudijiDTO) {
        this.studentiNaStudijiDTO = studentiNaStudijiDTO;
    }
    
    public java.util.List<PredmetDTO> getPredmetiDTO () {
        return this.predmetiDTO;
    }

    public void setPredmetiDTO (java.util.List<PredmetDTO> predmetiDTO) {
        this.predmetiDTO = predmetiDTO;
    }
    
    public java.util.List<GodinaStudijaObavestenjeDTO> getGodinaStudijaObavestenjaDTO () {
        return this.godinaStudijaObavestenjaDTO;
    }

    public void setGodinaStudijaObavestenjaDTO (java.util.List<GodinaStudijaObavestenjeDTO> godinaStudijaObavestenjaDTO) {
        this.godinaStudijaObavestenjaDTO = godinaStudijaObavestenjaDTO;
    }
    
    public java.util.List<KonsultacijaDTO> getKonsultacijeDTO () {
        return this.konsultacijeDTO;
    }

    public void setKonsultacijeDTO (java.util.List<KonsultacijaDTO> konsultacijeDTO) {
        this.konsultacijeDTO = konsultacijeDTO;
    }
    


    public GodinaStudijaDTO () {
        super();
        this.studentiNaStudijiDTO = new java.util.ArrayList<>();
        this.predmetiDTO = new java.util.ArrayList<>();
        this.godinaStudijaObavestenjaDTO = new java.util.ArrayList<>();
        this.konsultacijeDTO = new java.util.ArrayList<>();
    }

    public StudentNaStudijiDTO getStudentNaStudijiDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addStudentNaStudijiDTO (StudentNaStudijiDTO studentNaStudijiDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeStudentNaStudijiDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public KonsultacijaDTO getKonsultacijaDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addKonsultacijaDTO (KonsultacijaDTO konsultacijaDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeKonsultacijaDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public GodinaStudijaObavestenjeDTO getGodinaStudijaObavestenjeDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addGodinaStudijaObavestenjeDTO (GodinaStudijaObavestenjeDTO godinaStudijaObavestenjeDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeGodinaStudijaObavestenjeDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public PredmetDTO getPredmetDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addPredmetDTO (PredmetDTO predmetDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removePredmetDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

}