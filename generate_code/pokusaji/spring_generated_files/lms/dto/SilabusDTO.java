package lms.dto;



public class SilabusDTO implements DTO {

    private Long id;
        
    private String naslov;
        
    private String opis;
        
    private StanjeModela stanje;
        
    private Long version;
        
    private PredmetDTO predmetDTO;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getNaslov () {
        return this.naslov;
    }

    public void setNaslov (String naslov) {
        this.naslov = naslov;
    }
    
    public String getOpis () {
        return this.opis;
    }

    public void setOpis (String opis) {
        this.opis = opis;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public PredmetDTO getPredmetDTO () {
        return this.predmetDTO;
    }

    public void setPredmetDTO (PredmetDTO predmetDTO) {
        this.predmetDTO = predmetDTO;
    }
    


    public SilabusDTO () {
        super();
    }

}