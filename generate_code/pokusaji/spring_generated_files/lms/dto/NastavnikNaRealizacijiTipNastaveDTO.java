package lms.dto;



public class NastavnikNaRealizacijiTipNastaveDTO implements DTO {

    private Long id;
        
    private StanjeModela stanje;
        
    private Long version;
        
    private TipNastave tipNastave;
        
    private NastavnikNaRealizacijiDTO nastavnikNaRealizacijiDTO;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public TipNastave getTipNastave () {
        return this.tipNastave;
    }

    public void setTipNastave (TipNastave tipNastave) {
        this.tipNastave = tipNastave;
    }
    
    public NastavnikNaRealizacijiDTO getNastavnikNaRealizacijiDTO () {
        return this.nastavnikNaRealizacijiDTO;
    }

    public void setNastavnikNaRealizacijiDTO (NastavnikNaRealizacijiDTO nastavnikNaRealizacijiDTO) {
        this.nastavnikNaRealizacijiDTO = nastavnikNaRealizacijiDTO;
    }
    


    public NastavnikNaRealizacijiTipNastaveDTO () {
        super();
    }

}