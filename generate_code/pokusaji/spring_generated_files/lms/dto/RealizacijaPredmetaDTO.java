package lms.dto;



public class RealizacijaPredmetaDTO implements DTO {

    private Long id;
        
    private StanjeModela stanje;
        
    private Long version;
        
    private java.util.List<DrugiOblikNastaveDTO> drugiObliciNastaveDTO;
        
    private java.util.List<IshodDTO> ishodiDTO;
        
    private java.util.List<PohadjanjePredmetaDTO> pohadjanjaPredmetaDTO;
        
    private java.util.List<ObrazovniCiljDTO> obrazovniCiljeviDTO;
        
    private java.util.List<NastavnikNaRealizacijiDTO> nastavniciNaRealizacijiDTO;
        
    private java.util.List<IstrazivackiRadDTO> istrazivackiRadoviDTO;
        
    private java.util.List<EvaluacijaZnanjaDTO> evaluacijeZnanjaDTO;
        
    private java.util.List<TerminNastaveDTO> terminiNastaveDTO;
        
    private PredmetDTO predmetDTO;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public java.util.List<DrugiOblikNastaveDTO> getDrugiObliciNastaveDTO () {
        return this.drugiObliciNastaveDTO;
    }

    public void setDrugiObliciNastaveDTO (java.util.List<DrugiOblikNastaveDTO> drugiObliciNastaveDTO) {
        this.drugiObliciNastaveDTO = drugiObliciNastaveDTO;
    }
    
    public java.util.List<IshodDTO> getIshodiDTO () {
        return this.ishodiDTO;
    }

    public void setIshodiDTO (java.util.List<IshodDTO> ishodiDTO) {
        this.ishodiDTO = ishodiDTO;
    }
    
    public java.util.List<PohadjanjePredmetaDTO> getPohadjanjaPredmetaDTO () {
        return this.pohadjanjaPredmetaDTO;
    }

    public void setPohadjanjaPredmetaDTO (java.util.List<PohadjanjePredmetaDTO> pohadjanjaPredmetaDTO) {
        this.pohadjanjaPredmetaDTO = pohadjanjaPredmetaDTO;
    }
    
    public java.util.List<ObrazovniCiljDTO> getObrazovniCiljeviDTO () {
        return this.obrazovniCiljeviDTO;
    }

    public void setObrazovniCiljeviDTO (java.util.List<ObrazovniCiljDTO> obrazovniCiljeviDTO) {
        this.obrazovniCiljeviDTO = obrazovniCiljeviDTO;
    }
    
    public java.util.List<NastavnikNaRealizacijiDTO> getNastavniciNaRealizacijiDTO () {
        return this.nastavniciNaRealizacijiDTO;
    }

    public void setNastavniciNaRealizacijiDTO (java.util.List<NastavnikNaRealizacijiDTO> nastavniciNaRealizacijiDTO) {
        this.nastavniciNaRealizacijiDTO = nastavniciNaRealizacijiDTO;
    }
    
    public java.util.List<IstrazivackiRadDTO> getIstrazivackiRadoviDTO () {
        return this.istrazivackiRadoviDTO;
    }

    public void setIstrazivackiRadoviDTO (java.util.List<IstrazivackiRadDTO> istrazivackiRadoviDTO) {
        this.istrazivackiRadoviDTO = istrazivackiRadoviDTO;
    }
    
    public java.util.List<EvaluacijaZnanjaDTO> getEvaluacijeZnanjaDTO () {
        return this.evaluacijeZnanjaDTO;
    }

    public void setEvaluacijeZnanjaDTO (java.util.List<EvaluacijaZnanjaDTO> evaluacijeZnanjaDTO) {
        this.evaluacijeZnanjaDTO = evaluacijeZnanjaDTO;
    }
    
    public java.util.List<TerminNastaveDTO> getTerminiNastaveDTO () {
        return this.terminiNastaveDTO;
    }

    public void setTerminiNastaveDTO (java.util.List<TerminNastaveDTO> terminiNastaveDTO) {
        this.terminiNastaveDTO = terminiNastaveDTO;
    }
    
    public PredmetDTO getPredmetDTO () {
        return this.predmetDTO;
    }

    public void setPredmetDTO (PredmetDTO predmetDTO) {
        this.predmetDTO = predmetDTO;
    }
    


    public RealizacijaPredmetaDTO () {
        super();
        this.drugiObliciNastaveDTO = new java.util.ArrayList<>();
        this.ishodiDTO = new java.util.ArrayList<>();
        this.pohadjanjaPredmetaDTO = new java.util.ArrayList<>();
        this.obrazovniCiljeviDTO = new java.util.ArrayList<>();
        this.nastavniciNaRealizacijiDTO = new java.util.ArrayList<>();
        this.istrazivackiRadoviDTO = new java.util.ArrayList<>();
        this.evaluacijeZnanjaDTO = new java.util.ArrayList<>();
        this.terminiNastaveDTO = new java.util.ArrayList<>();
    }

    public DrugiOblikNastaveDTO getDrugiOblikNastaveDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addDrugiOblikNastaveDTO (DrugiOblikNastaveDTO drugiOblikNastaveDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeDrugiOblikNastaveDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public NastavnikNaRealizacijiDTO getNastavnikNaRealizacijiDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addNastavnikNaRealizacijiDTO (NastavnikNaRealizacijiDTO nastavnikNaRealizacijiDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeNastavnikNaRealizacijiDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public PohadjanjePredmetaDTO getPohadjanjePredmetaDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addPohadjanjePredmetaDTO (PohadjanjePredmetaDTO pohadjanjePredmetaDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removePohadjanjePredmetaDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public EvaluacijaZnanjaDTO getEvaluacijaZnanjaDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addEvaluacijaZnanjaDTO (EvaluacijaZnanjaDTO evaluacijaZnanjaDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeEvaluacijaZnanjaDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public ObrazovniCiljDTO getObrazovniCiljDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addObrazovniCiljDTO (ObrazovniCiljDTO obrazovniCiljDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeObrazovniCiljDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public TerminNastaveDTO getTerminNastaveDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addTerminNastaveDTO (TerminNastaveDTO terminNastaveDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeTerminNastaveDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public IshodDTO getIshodDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addIshodDTO (IshodDTO ishodDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeIshodDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public IstrazivackiRadDTO getIstrazivackiRadDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addIstrazivackiRadDTO (IstrazivackiRadDTO istrazivackiRadDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeIstrazivackiRadDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

}