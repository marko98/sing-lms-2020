package lms.dto;



public class PohadjanjePredmetaDTO implements DTO {

    private Long id;
        
    private Integer konacnaOcena;
        
    private StanjeModela stanje;
        
    private Long version;
        
    private StudentNaStudijiDTO studentNaStudijiDTO;
        
    private java.util.List<PolaganjeDTO> polaganjaDTO;
        
    private RealizacijaPredmetaDTO realizacijaPredmetaDTO;
        
    private java.util.List<DatumPohadjanjaPredmetaDTO> prisustvoDTO;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public Integer getKonacnaOcena () {
        return this.konacnaOcena;
    }

    public void setKonacnaOcena (Integer konacnaOcena) {
        this.konacnaOcena = konacnaOcena;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public StudentNaStudijiDTO getStudentNaStudijiDTO () {
        return this.studentNaStudijiDTO;
    }

    public void setStudentNaStudijiDTO (StudentNaStudijiDTO studentNaStudijiDTO) {
        this.studentNaStudijiDTO = studentNaStudijiDTO;
    }
    
    public java.util.List<PolaganjeDTO> getPolaganjaDTO () {
        return this.polaganjaDTO;
    }

    public void setPolaganjaDTO (java.util.List<PolaganjeDTO> polaganjaDTO) {
        this.polaganjaDTO = polaganjaDTO;
    }
    
    public RealizacijaPredmetaDTO getRealizacijaPredmetaDTO () {
        return this.realizacijaPredmetaDTO;
    }

    public void setRealizacijaPredmetaDTO (RealizacijaPredmetaDTO realizacijaPredmetaDTO) {
        this.realizacijaPredmetaDTO = realizacijaPredmetaDTO;
    }
    
    public java.util.List<DatumPohadjanjaPredmetaDTO> getPrisustvoDTO () {
        return this.prisustvoDTO;
    }

    public void setPrisustvoDTO (java.util.List<DatumPohadjanjaPredmetaDTO> prisustvoDTO) {
        this.prisustvoDTO = prisustvoDTO;
    }
    


    public PohadjanjePredmetaDTO () {
        super();
        this.polaganjaDTO = new java.util.ArrayList<>();
        this.prisustvoDTO = new java.util.ArrayList<>();
    }

    public PolaganjeDTO getPolaganjeDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addPolaganjeDTO (PolaganjeDTO polaganjeDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removePolaganjeDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public DatumPohadjanjaPredmetaDTO getDatumPohadjanjaPredmetaDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addDatumPohadjanjaPredmetaDTO (DatumPohadjanjaPredmetaDTO datumPohadjanjaPredmetaDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeDatumPohadjanjaPredmetaDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

}