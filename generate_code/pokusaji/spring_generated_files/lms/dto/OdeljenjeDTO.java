package lms.dto;



public class OdeljenjeDTO implements DTO {

    private Long id;
        
    private StanjeModela stanje;
        
    private Long version;
        
    private TipOdeljenja tip;
        
    private AdresaDTO adresaDTO;
        
    private java.util.List<ProstorijaDTO> prostorijeDTO;
        
    private FakultetDTO fakultetDTO;
        
    private StudentskaSluzbaDTO studentskaSluzbaDTO;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public TipOdeljenja getTip () {
        return this.tip;
    }

    public void setTip (TipOdeljenja tip) {
        this.tip = tip;
    }
    
    public AdresaDTO getAdresaDTO () {
        return this.adresaDTO;
    }

    public void setAdresaDTO (AdresaDTO adresaDTO) {
        this.adresaDTO = adresaDTO;
    }
    
    public java.util.List<ProstorijaDTO> getProstorijeDTO () {
        return this.prostorijeDTO;
    }

    public void setProstorijeDTO (java.util.List<ProstorijaDTO> prostorijeDTO) {
        this.prostorijeDTO = prostorijeDTO;
    }
    
    public FakultetDTO getFakultetDTO () {
        return this.fakultetDTO;
    }

    public void setFakultetDTO (FakultetDTO fakultetDTO) {
        this.fakultetDTO = fakultetDTO;
    }
    
    public StudentskaSluzbaDTO getStudentskaSluzbaDTO () {
        return this.studentskaSluzbaDTO;
    }

    public void setStudentskaSluzbaDTO (StudentskaSluzbaDTO studentskaSluzbaDTO) {
        this.studentskaSluzbaDTO = studentskaSluzbaDTO;
    }
    


    public OdeljenjeDTO () {
        super();
        this.prostorijeDTO = new java.util.ArrayList<>();
    }

    public ProstorijaDTO getProstorijaDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addProstorijaDTO (ProstorijaDTO prostorijaDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeProstorijaDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

}