package lms.dto;



public class GodinaStudijaObavestenjeDTO implements DTO {

    private Long id;
        
    private StanjeModela stanje;
        
    private Long version;
        
    private GodinaStudijaDTO godinaStudijaDTO;
        
    private ObavestenjeDTO obavestenjeDTO;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public GodinaStudijaDTO getGodinaStudijaDTO () {
        return this.godinaStudijaDTO;
    }

    public void setGodinaStudijaDTO (GodinaStudijaDTO godinaStudijaDTO) {
        this.godinaStudijaDTO = godinaStudijaDTO;
    }
    
    public ObavestenjeDTO getObavestenjeDTO () {
        return this.obavestenjeDTO;
    }

    public void setObavestenjeDTO (ObavestenjeDTO obavestenjeDTO) {
        this.obavestenjeDTO = obavestenjeDTO;
    }
    


    public GodinaStudijaObavestenjeDTO () {
        super();
    }

}