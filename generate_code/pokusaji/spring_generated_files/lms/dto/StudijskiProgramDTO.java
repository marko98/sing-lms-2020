package lms.dto;



public class StudijskiProgramDTO implements DTO {

    private Long id;
        
    private String naziv;
        
    private Integer brojGodinaStudija;
        
    private String opis;
        
    private StanjeModela stanje;
        
    private Long version;
        
    private FakultetDTO fakultetDTO;
        
    private NastavnikDTO rukovodilacDTO;
        
    private java.util.List<GodinaStudijaDTO> godineStudijaDTO;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getNaziv () {
        return this.naziv;
    }

    public void setNaziv (String naziv) {
        this.naziv = naziv;
    }
    
    public Integer getBrojGodinaStudija () {
        return this.brojGodinaStudija;
    }

    public void setBrojGodinaStudija (Integer brojGodinaStudija) {
        this.brojGodinaStudija = brojGodinaStudija;
    }
    
    public String getOpis () {
        return this.opis;
    }

    public void setOpis (String opis) {
        this.opis = opis;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public FakultetDTO getFakultetDTO () {
        return this.fakultetDTO;
    }

    public void setFakultetDTO (FakultetDTO fakultetDTO) {
        this.fakultetDTO = fakultetDTO;
    }
    
    public NastavnikDTO getRukovodilacDTO () {
        return this.rukovodilacDTO;
    }

    public void setRukovodilacDTO (NastavnikDTO rukovodilacDTO) {
        this.rukovodilacDTO = rukovodilacDTO;
    }
    
    public java.util.List<GodinaStudijaDTO> getGodineStudijaDTO () {
        return this.godineStudijaDTO;
    }

    public void setGodineStudijaDTO (java.util.List<GodinaStudijaDTO> godineStudijaDTO) {
        this.godineStudijaDTO = godineStudijaDTO;
    }
    


    public StudijskiProgramDTO () {
        super();
        this.godineStudijaDTO = new java.util.ArrayList<>();
    }

    public GodinaStudijaDTO getGodinaStudijaDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addGodinaStudijaDTO (GodinaStudijaDTO godinaStudijaDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeGodinaStudijaDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

}