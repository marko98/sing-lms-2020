package lms.dto;



public class StudentNaStudijiDTO implements DTO {

    private Long id;
        
    private LocalDateTime datumUpisa;
        
    private String brojIndeksa;
        
    private StanjeModela stanje;
        
    private Long version;
        
    private StudentDTO studentDTO;
        
    private java.util.List<PohadjanjePredmetaDTO> pohadjanjaPredmetaDTO;
        
    private GodinaStudijaDTO godinaStudijaDTO;
        
    private java.util.List<IstrazivackiRadStudentNaStudijiDTO> istrazivackiRadoviStudentNaStudijiDTO;
        
    private DiplomskiRadDTO diplomskiRadDTO;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public LocalDateTime getDatumUpisa () {
        return this.datumUpisa;
    }

    public void setDatumUpisa (LocalDateTime datumUpisa) {
        this.datumUpisa = datumUpisa;
    }
    
    public String getBrojIndeksa () {
        return this.brojIndeksa;
    }

    public void setBrojIndeksa (String brojIndeksa) {
        this.brojIndeksa = brojIndeksa;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public StudentDTO getStudentDTO () {
        return this.studentDTO;
    }

    public void setStudentDTO (StudentDTO studentDTO) {
        this.studentDTO = studentDTO;
    }
    
    public java.util.List<PohadjanjePredmetaDTO> getPohadjanjaPredmetaDTO () {
        return this.pohadjanjaPredmetaDTO;
    }

    public void setPohadjanjaPredmetaDTO (java.util.List<PohadjanjePredmetaDTO> pohadjanjaPredmetaDTO) {
        this.pohadjanjaPredmetaDTO = pohadjanjaPredmetaDTO;
    }
    
    public GodinaStudijaDTO getGodinaStudijaDTO () {
        return this.godinaStudijaDTO;
    }

    public void setGodinaStudijaDTO (GodinaStudijaDTO godinaStudijaDTO) {
        this.godinaStudijaDTO = godinaStudijaDTO;
    }
    
    public java.util.List<IstrazivackiRadStudentNaStudijiDTO> getIstrazivackiRadoviStudentNaStudijiDTO () {
        return this.istrazivackiRadoviStudentNaStudijiDTO;
    }

    public void setIstrazivackiRadoviStudentNaStudijiDTO (java.util.List<IstrazivackiRadStudentNaStudijiDTO> istrazivackiRadoviStudentNaStudijiDTO) {
        this.istrazivackiRadoviStudentNaStudijiDTO = istrazivackiRadoviStudentNaStudijiDTO;
    }
    
    public DiplomskiRadDTO getDiplomskiRadDTO () {
        return this.diplomskiRadDTO;
    }

    public void setDiplomskiRadDTO (DiplomskiRadDTO diplomskiRadDTO) {
        this.diplomskiRadDTO = diplomskiRadDTO;
    }
    


    public StudentNaStudijiDTO () {
        super();
        this.pohadjanjaPredmetaDTO = new java.util.ArrayList<>();
        this.istrazivackiRadoviStudentNaStudijiDTO = new java.util.ArrayList<>();
    }

    public PohadjanjePredmetaDTO getPohadjanjePredmetaDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addPohadjanjePredmetaDTO (PohadjanjePredmetaDTO pohadjanjePredmetaDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removePohadjanjePredmetaDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public IstrazivackiRadStudentNaStudijiDTO getIstrazivackiRadStudentNaStudijiDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addIstrazivackiRadStudentNaStudijiDTO (IstrazivackiRadStudentNaStudijiDTO istrazivackiRadStudentNaStudijiDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeIstrazivackiRadStudentNaStudijiDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

}