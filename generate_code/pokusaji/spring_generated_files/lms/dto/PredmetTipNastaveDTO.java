package lms.dto;



public class PredmetTipNastaveDTO implements DTO {

    private Long id;
        
    private StanjeModela stanje;
        
    private Long version;
        
    private TipNastave tipNastave;
        
    private PredmetDTO predmetDTO;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public TipNastave getTipNastave () {
        return this.tipNastave;
    }

    public void setTipNastave (TipNastave tipNastave) {
        this.tipNastave = tipNastave;
    }
    
    public PredmetDTO getPredmetDTO () {
        return this.predmetDTO;
    }

    public void setPredmetDTO (PredmetDTO predmetDTO) {
        this.predmetDTO = predmetDTO;
    }
    


    public PredmetTipNastaveDTO () {
        super();
    }

}