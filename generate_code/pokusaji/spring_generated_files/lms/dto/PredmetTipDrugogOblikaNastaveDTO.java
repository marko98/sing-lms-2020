package lms.dto;



public class PredmetTipDrugogOblikaNastaveDTO implements DTO {

    private Long id;
        
    private StanjeModela stanje;
        
    private Long version;
        
    private TipDrugogOblikaNastave tipDrugogOblikaNastave;
        
    private PredmetDTO predmetDTO;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public TipDrugogOblikaNastave getTipDrugogOblikaNastave () {
        return this.tipDrugogOblikaNastave;
    }

    public void setTipDrugogOblikaNastave (TipDrugogOblikaNastave tipDrugogOblikaNastave) {
        this.tipDrugogOblikaNastave = tipDrugogOblikaNastave;
    }
    
    public PredmetDTO getPredmetDTO () {
        return this.predmetDTO;
    }

    public void setPredmetDTO (PredmetDTO predmetDTO) {
        this.predmetDTO = predmetDTO;
    }
    


    public PredmetTipDrugogOblikaNastaveDTO () {
        super();
    }

}