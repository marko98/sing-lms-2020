package lms.dto;



public class AutorDTO implements DTO {

    private Long id;
        
    private String ime;
        
    private String prezime;
        
    private StanjeModela stanje;
        
    private Long version;
        
    private java.util.List<AutorNastavniMaterijalDTO> autorNastavniMaterijaliDTO;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getIme () {
        return this.ime;
    }

    public void setIme (String ime) {
        this.ime = ime;
    }
    
    public String getPrezime () {
        return this.prezime;
    }

    public void setPrezime (String prezime) {
        this.prezime = prezime;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public java.util.List<AutorNastavniMaterijalDTO> getAutorNastavniMaterijaliDTO () {
        return this.autorNastavniMaterijaliDTO;
    }

    public void setAutorNastavniMaterijaliDTO (java.util.List<AutorNastavniMaterijalDTO> autorNastavniMaterijaliDTO) {
        this.autorNastavniMaterijaliDTO = autorNastavniMaterijaliDTO;
    }
    


    public AutorDTO () {
        super();
        this.autorNastavniMaterijaliDTO = new java.util.ArrayList<>();
    }

    public AutorNastavniMaterijalDTO getAutorNastavniMaterijalDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addAutorNastavniMaterijalDTO (AutorNastavniMaterijalDTO autorNastavniMaterijalDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeAutorNastavniMaterijalDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

}