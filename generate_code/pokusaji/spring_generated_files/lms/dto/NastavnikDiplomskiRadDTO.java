package lms.dto;



public class NastavnikDiplomskiRadDTO implements DTO {

    private Long id;
        
    private StanjeModela stanje;
        
    private Long version;
        
    private NastavnikDTO nastavnikUKomisijiDTO;
        
    private DiplomskiRadDTO diplomskiRadDTO;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public NastavnikDTO getNastavnikUKomisijiDTO () {
        return this.nastavnikUKomisijiDTO;
    }

    public void setNastavnikUKomisijiDTO (NastavnikDTO nastavnikUKomisijiDTO) {
        this.nastavnikUKomisijiDTO = nastavnikUKomisijiDTO;
    }
    
    public DiplomskiRadDTO getDiplomskiRadDTO () {
        return this.diplomskiRadDTO;
    }

    public void setDiplomskiRadDTO (DiplomskiRadDTO diplomskiRadDTO) {
        this.diplomskiRadDTO = diplomskiRadDTO;
    }
    


    public NastavnikDiplomskiRadDTO () {
        super();
    }

}