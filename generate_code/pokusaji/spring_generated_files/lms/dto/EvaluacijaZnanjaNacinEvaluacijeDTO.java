package lms.dto;



public class EvaluacijaZnanjaNacinEvaluacijeDTO implements DTO {

    private Long id;
        
    private StanjeModela stanje;
        
    private Long version;
        
    private NacinEvaluacije nacinEvaluacije;
        
    private EvaluacijaZnanjaDTO evaluacijaZnanjaDTO;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public NacinEvaluacije getNacinEvaluacije () {
        return this.nacinEvaluacije;
    }

    public void setNacinEvaluacije (NacinEvaluacije nacinEvaluacije) {
        this.nacinEvaluacije = nacinEvaluacije;
    }
    
    public EvaluacijaZnanjaDTO getEvaluacijaZnanjaDTO () {
        return this.evaluacijaZnanjaDTO;
    }

    public void setEvaluacijaZnanjaDTO (EvaluacijaZnanjaDTO evaluacijaZnanjaDTO) {
        this.evaluacijaZnanjaDTO = evaluacijaZnanjaDTO;
    }
    


    public EvaluacijaZnanjaNacinEvaluacijeDTO () {
        super();
    }

}