package lms.dto;



public class KonsultacijaDTO implements DTO {

    private Long id;
        
    private StanjeModela stanje;
        
    private Long version;
        
    private GodinaStudijaDTO godinaStudijaDTO;
        
    private VremeOdrzavanjaUNedeljiDTO vremeOdrzavanjaUNedeljiDTO;
        
    private NastavnikDTO nastavnikDTO;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public GodinaStudijaDTO getGodinaStudijaDTO () {
        return this.godinaStudijaDTO;
    }

    public void setGodinaStudijaDTO (GodinaStudijaDTO godinaStudijaDTO) {
        this.godinaStudijaDTO = godinaStudijaDTO;
    }
    
    public VremeOdrzavanjaUNedeljiDTO getVremeOdrzavanjaUNedeljiDTO () {
        return this.vremeOdrzavanjaUNedeljiDTO;
    }

    public void setVremeOdrzavanjaUNedeljiDTO (VremeOdrzavanjaUNedeljiDTO vremeOdrzavanjaUNedeljiDTO) {
        this.vremeOdrzavanjaUNedeljiDTO = vremeOdrzavanjaUNedeljiDTO;
    }
    
    public NastavnikDTO getNastavnikDTO () {
        return this.nastavnikDTO;
    }

    public void setNastavnikDTO (NastavnikDTO nastavnikDTO) {
        this.nastavnikDTO = nastavnikDTO;
    }
    


    public KonsultacijaDTO () {
        super();
    }

}