package lms.dto;



public class TitulaDTO implements DTO {

    private Long id;
        
    private LocalDateTime datumIzbora;
        
    private LocalDateTime datumPrestanka;
        
    private StanjeModela stanje;
        
    private Long version;
        
    private TipTitule tip;
        
    private NaucnaOblastDTO naucnaOblastDTO;
        
    private NastavnikDTO nastavnikDTO;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public LocalDateTime getDatumIzbora () {
        return this.datumIzbora;
    }

    public void setDatumIzbora (LocalDateTime datumIzbora) {
        this.datumIzbora = datumIzbora;
    }
    
    public LocalDateTime getDatumPrestanka () {
        return this.datumPrestanka;
    }

    public void setDatumPrestanka (LocalDateTime datumPrestanka) {
        this.datumPrestanka = datumPrestanka;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public TipTitule getTip () {
        return this.tip;
    }

    public void setTip (TipTitule tip) {
        this.tip = tip;
    }
    
    public NaucnaOblastDTO getNaucnaOblastDTO () {
        return this.naucnaOblastDTO;
    }

    public void setNaucnaOblastDTO (NaucnaOblastDTO naucnaOblastDTO) {
        this.naucnaOblastDTO = naucnaOblastDTO;
    }
    
    public NastavnikDTO getNastavnikDTO () {
        return this.nastavnikDTO;
    }

    public void setNastavnikDTO (NastavnikDTO nastavnikDTO) {
        this.nastavnikDTO = nastavnikDTO;
    }
    


    public TitulaDTO () {
        super();
    }

}