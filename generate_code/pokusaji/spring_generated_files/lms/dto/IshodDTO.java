package lms.dto;



public class IshodDTO implements DTO {

    private Long id;
        
    private String opis;
        
    private String naslov;
        
    private String putanjaZaSliku;
        
    private StanjeModela stanje;
        
    private Long version;
        
    private RealizacijaPredmetaDTO realizacijaPredmetaDTO;
        
    private EvaluacijaZnanjaDTO evaluacijaZnanjaDTO;
        
    private java.util.List<NastavniMaterijalDTO> nastavniMaterijaliDTO;
        
    private java.util.List<ObrazovniCiljDTO> obrazovniCiljeviDTO;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getOpis () {
        return this.opis;
    }

    public void setOpis (String opis) {
        this.opis = opis;
    }
    
    public String getNaslov () {
        return this.naslov;
    }

    public void setNaslov (String naslov) {
        this.naslov = naslov;
    }
    
    public String getPutanjaZaSliku () {
        return this.putanjaZaSliku;
    }

    public void setPutanjaZaSliku (String putanjaZaSliku) {
        this.putanjaZaSliku = putanjaZaSliku;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public RealizacijaPredmetaDTO getRealizacijaPredmetaDTO () {
        return this.realizacijaPredmetaDTO;
    }

    public void setRealizacijaPredmetaDTO (RealizacijaPredmetaDTO realizacijaPredmetaDTO) {
        this.realizacijaPredmetaDTO = realizacijaPredmetaDTO;
    }
    
    public EvaluacijaZnanjaDTO getEvaluacijaZnanjaDTO () {
        return this.evaluacijaZnanjaDTO;
    }

    public void setEvaluacijaZnanjaDTO (EvaluacijaZnanjaDTO evaluacijaZnanjaDTO) {
        this.evaluacijaZnanjaDTO = evaluacijaZnanjaDTO;
    }
    
    public java.util.List<NastavniMaterijalDTO> getNastavniMaterijaliDTO () {
        return this.nastavniMaterijaliDTO;
    }

    public void setNastavniMaterijaliDTO (java.util.List<NastavniMaterijalDTO> nastavniMaterijaliDTO) {
        this.nastavniMaterijaliDTO = nastavniMaterijaliDTO;
    }
    
    public java.util.List<ObrazovniCiljDTO> getObrazovniCiljeviDTO () {
        return this.obrazovniCiljeviDTO;
    }

    public void setObrazovniCiljeviDTO (java.util.List<ObrazovniCiljDTO> obrazovniCiljeviDTO) {
        this.obrazovniCiljeviDTO = obrazovniCiljeviDTO;
    }
    


    public IshodDTO () {
        super();
        this.nastavniMaterijaliDTO = new java.util.ArrayList<>();
        this.obrazovniCiljeviDTO = new java.util.ArrayList<>();
    }

    public ObrazovniCiljDTO getObrazovniCiljDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addObrazovniCiljDTO (ObrazovniCiljDTO obrazovniCiljDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeObrazovniCiljDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public NastavniMaterijalDTO getNastavniMaterijalDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addNastavniMaterijalDTO (NastavniMaterijalDTO nastavniMaterijalDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeNastavniMaterijalDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

}