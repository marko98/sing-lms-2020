package lms.dto;



public class DiplomskiRadDTO implements DTO {

    private Long id;
        
    private String tema;
        
    private Integer ocena;
        
    private StanjeModela stanje;
        
    private Long version;
        
    private NastavnikDTO mentorDTO;
        
    private java.util.List<NastavnikDiplomskiRadDTO> nastavniciDiplomskiRadDTO;
        
    private StudentNaStudijiDTO studentNaStudijiDTO;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getTema () {
        return this.tema;
    }

    public void setTema (String tema) {
        this.tema = tema;
    }
    
    public Integer getOcena () {
        return this.ocena;
    }

    public void setOcena (Integer ocena) {
        this.ocena = ocena;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public NastavnikDTO getMentorDTO () {
        return this.mentorDTO;
    }

    public void setMentorDTO (NastavnikDTO mentorDTO) {
        this.mentorDTO = mentorDTO;
    }
    
    public java.util.List<NastavnikDiplomskiRadDTO> getNastavniciDiplomskiRadDTO () {
        return this.nastavniciDiplomskiRadDTO;
    }

    public void setNastavniciDiplomskiRadDTO (java.util.List<NastavnikDiplomskiRadDTO> nastavniciDiplomskiRadDTO) {
        this.nastavniciDiplomskiRadDTO = nastavniciDiplomskiRadDTO;
    }
    
    public StudentNaStudijiDTO getStudentNaStudijiDTO () {
        return this.studentNaStudijiDTO;
    }

    public void setStudentNaStudijiDTO (StudentNaStudijiDTO studentNaStudijiDTO) {
        this.studentNaStudijiDTO = studentNaStudijiDTO;
    }
    


    public DiplomskiRadDTO () {
        super();
        this.nastavniciDiplomskiRadDTO = new java.util.ArrayList<>();
    }

    public NastavnikDiplomskiRadDTO getNastavnikDiplomskiRadDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addNastavnikDiplomskiRadDTO (NastavnikDiplomskiRadDTO nastavnikDiplomskiRadDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeNastavnikDiplomskiRadDTO (Identifikacija identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

}