package lms.host.configuration;


@Configuration

public class HostAppConfiguration {





    @Bean
    public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer () {
	PropertySourcesPlaceholderConfigurer properties = new PropertySourcesPlaceholderConfigurer();
	properties.setLocation(new ClassPathResource("/lms/host/configuration/host.application.properties"));
	properties.setIgnoreResourceNotFound(false);
	return properties;
    }

}