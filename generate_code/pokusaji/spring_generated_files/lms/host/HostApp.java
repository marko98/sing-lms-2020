package lms.host;


@SpringBootApplication
@ComponentScan(basePackages={"lms.repository", "lms.service", "lms.controller", "lms.hbsf", "lms.host"})

public class HostApp extends SpringBootServletInitializer {





    public static void main (String args[]) {
	SpringApplication.run(HostApp.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure (SpringApplicationBuilder builder) {
		return builder.sources(HostApp.class);
    }

}