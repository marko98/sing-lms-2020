package lms.host.repository;


@Repository

public class PluginRepository {

    private HashMap<String, ArrayList<Plugin>> pluginRepository = new HashMap<String, ArrayList<Plugin>>();
        


    public HashMap<String, ArrayList<Plugin>> getPluginRepository () {
        return this.pluginRepository;
    }

    public void setPluginRepository (HashMap<String, ArrayList<Plugin>> pluginRepository) {
        this.pluginRepository = pluginRepository;
    }
    


    public PluginRepository () {
        super();
    }

    public void registerPlugin (PluginDescription pluginDescription) {
	for (String category : pluginDescription.getCategories()) {

	    if (pluginRepository.get(category) == null) {
		pluginRepository.put(category, new ArrayList<Plugin>());
	    }
	    Plugin plugin = new Plugin(pluginDescription);
	    pluginRepository.get(category).add(plugin);

	    System.out.println(
		    "Plugin: " + pluginDescription.getName() + " , kategorija: " + category + " - registrovan\n");

	}
    }

    public ArrayList<Plugin> getPlugins (String category) {
	return pluginRepository.get(category);
    }

}