package lms.host.controller;


@Controller
@Scope("singleton")
@RequestMapping(path = "api/plugins")

public class PluginController {

    @Autowired
    private PluginRepository pr;
        


    public PluginRepository getPr () {
        return this.pr;
    }

    public void setPr (PluginRepository pr) {
        this.pr = pr;
    }
    


    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<Object> registerPlugin (@RequestBody PluginDescription pluginDescription) {
	pr.registerPlugin(pluginDescription);
	return new ResponseEntity<Object>(HttpStatus.OK);
    }

}