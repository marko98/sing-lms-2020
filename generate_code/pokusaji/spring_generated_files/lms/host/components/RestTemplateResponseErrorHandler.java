package lms.host.components;


@Component

public class RestTemplateResponseErrorHandler implements ResponseErrorHandler {





    @Override
    public boolean hasError (ClientHttpResponse httpResponse) {
	return (httpResponse.getStatusCode().series() == Series.CLIENT_ERROR
		|| httpResponse.getStatusCode().series() == Series.SERVER_ERROR);
    }

    @Override
    public void handleError (ClientHttpResponse httpResponse) {

	if (httpResponse.getStatusCode().series() == HttpStatus.Series.SERVER_ERROR) {
	    // handle SERVER_ERROR
	    
//	    potencijalno mesto za log serverskih gresaka
	} else if (httpResponse.getStatusCode().series() == HttpStatus.Series.CLIENT_ERROR) {
	    // handle CLIENT_ERROR

//	    potencijalno mesto za log klijentskih gresaka
	}
    }

}