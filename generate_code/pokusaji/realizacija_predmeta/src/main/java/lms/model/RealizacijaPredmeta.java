package lms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

import org.hibernate.annotations.Any;
import org.hibernate.annotations.AnyMetaDef;
import org.hibernate.annotations.MetaValue;

import lms.dto.RealizacijaPredmetaDTO;
import lms.dto.interfaces.DTO;
import lms.dto.interfaces.EvaluacijaZnanjaInterfaceDTO;
import lms.model.interfaces.Entitet;
import lms.model.interfaces.EvaluacijaZnanjaInterface;

@Entity
public class RealizacijaPredmeta implements Entitet<RealizacijaPredmeta, Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Any(metaColumn = @Column(name = "evaluacija_znanja_type"))
    @AnyMetaDef(idType = "long", metaType = "string", metaValues = {
	    @MetaValue(targetEntity = EvaluacijaZnanja.class, value = "EVALUACIJA_ZNANJA"),
	    @MetaValue(targetEntity = Ispit.class, value = "ISPIT")
    })
    @JoinColumn(name = "evaluacija_znanja_id")
    private EvaluacijaZnanjaInterface<?, ?> evaluacijaZnanja;

    public RealizacijaPredmeta() {
	super();
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public EvaluacijaZnanjaInterface<?, ?> getEvaluacijaZnanja() {
	return evaluacijaZnanja;
    }

    public void setEvaluacijaZnanja(EvaluacijaZnanjaInterface<?, ?> evaluacijaZnanja) {
	this.evaluacijaZnanja = evaluacijaZnanja;
    }

    @Override
    public DTO getDTO() {
	RealizacijaPredmetaDTO realizacijaPredmetaDTO = new RealizacijaPredmetaDTO();

	realizacijaPredmetaDTO.setId(id);
	realizacijaPredmetaDTO.setEvaluacijaZnanjaInterfaceDTO(
		(EvaluacijaZnanjaInterfaceDTO) evaluacijaZnanja.getDTOinsideDTO());

	return realizacijaPredmetaDTO;
    }

    @Override
    public DTO getDTOinsideDTO() {
	RealizacijaPredmetaDTO realizacijaPredmetaDTO = new RealizacijaPredmetaDTO();

	realizacijaPredmetaDTO.setId(id);

	return realizacijaPredmetaDTO;
    }

    @Override
    public void update(RealizacijaPredmeta entity) {
	// TODO Auto-generated method stub

    }

}
