package lms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lms.dto.IspitDTO;
import lms.model.enums.IspitniRok;
import lms.model.interfaces.Entitet;
import lms.model.interfaces.EvaluacijaZnanjaInterface;

@Entity
@JsonIgnoreProperties({ "type" })
public class Ispit implements Entitet<Ispit, Long>, EvaluacijaZnanjaInterface<Ispit, Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    private String name;
    
    private IspitniRok ispitniRok;

    public Ispit() {
	super();
    }

    public Ispit(String name, IspitniRok ispitniRok) {
	super();
	this.name = name;
	this.ispitniRok = ispitniRok;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public IspitniRok getIspitniRok() {
        return ispitniRok;
    }

    public void setIspitniRok(IspitniRok ispitniRok) {
        this.ispitniRok = ispitniRok;
    }
    
    @Override
    public IspitDTO getDTO() {
        return new IspitDTO(id, name, ispitniRok);
    }
    
    @Override
    public IspitDTO getDTOinsideDTO() {
        return new IspitDTO(id, name, ispitniRok);
    }
    
    @Override
    public void update(Ispit entity) {
        // TODO Auto-generated method stub
        
    }
    
}
