package lms.model.interfaces;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import lms.model.EvaluacijaZnanja;
import lms.model.Ispit;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({ @Type(value = EvaluacijaZnanja.class, name = "evaluacijaZnanja"),
	@Type(value = Ispit.class, name = "ispit") })
public interface EvaluacijaZnanjaInterface<T, ID extends Serializable> extends Entitet<T, ID> {
    public String getName();
}
