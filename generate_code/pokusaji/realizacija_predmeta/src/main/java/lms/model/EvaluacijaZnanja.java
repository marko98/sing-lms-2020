package lms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lms.dto.EvaluacijaZnanjaDTO;
import lms.model.interfaces.Entitet;
import lms.model.interfaces.EvaluacijaZnanjaInterface;

@Entity
@JsonIgnoreProperties({ "type" })
public class EvaluacijaZnanja
	implements Entitet<EvaluacijaZnanja, Long>, EvaluacijaZnanjaInterface<EvaluacijaZnanja, Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    public EvaluacijaZnanja() {
	super();
    }

    public EvaluacijaZnanja(String name) {
	super();
	this.name = name;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    @Override
    public EvaluacijaZnanjaDTO getDTO() {
        return new EvaluacijaZnanjaDTO(id, name);
    }

    @Override
    public EvaluacijaZnanjaDTO getDTOinsideDTO() {
        return new EvaluacijaZnanjaDTO(id, name);
    }
    
    @Override
    public void update(EvaluacijaZnanja entity) {
        // TODO Auto-generated method stub
        
    }

}
