package lms.interfaces;

public interface Identifikacija<ID> {
    public ID getId();
    public void setId(ID id);
}
