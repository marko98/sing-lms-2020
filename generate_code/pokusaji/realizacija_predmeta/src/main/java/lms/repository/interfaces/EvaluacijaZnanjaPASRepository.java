package lms.repository.interfaces;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.EvaluacijaZnanja;

@Repository
public interface EvaluacijaZnanjaPASRepository extends PagingAndSortingRepository<EvaluacijaZnanja, Long>{

}
