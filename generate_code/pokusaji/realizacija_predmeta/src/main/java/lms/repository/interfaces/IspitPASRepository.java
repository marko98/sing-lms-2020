package lms.repository.interfaces;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.Ispit;

@Repository
public interface IspitPASRepository extends PagingAndSortingRepository<Ispit, Long> {

}
