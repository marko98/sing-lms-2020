package lms.repository;

import lms.model.RealizacijaPredmeta;

import org.springframework.stereotype.Repository;

@Repository
public class RealizacijaPredmetaRepository extends CrudRepository<RealizacijaPredmeta, Long> {

}
