package lms.repository.interfaces;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.RealizacijaPredmeta;

@Repository
public interface RealizacijaPredmetaPASRepository extends PagingAndSortingRepository<RealizacijaPredmeta, Long> {

}
