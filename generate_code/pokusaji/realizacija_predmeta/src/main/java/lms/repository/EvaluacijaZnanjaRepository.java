package lms.repository;

import org.springframework.stereotype.Repository;

import lms.model.EvaluacijaZnanja;

@Repository
public class EvaluacijaZnanjaRepository extends CrudRepository<EvaluacijaZnanja, Long> {

}
