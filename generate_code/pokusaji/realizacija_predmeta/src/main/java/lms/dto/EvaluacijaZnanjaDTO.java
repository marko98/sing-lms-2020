package lms.dto;

import lms.dto.interfaces.EvaluacijaZnanjaInterfaceDTO;

public class EvaluacijaZnanjaDTO implements EvaluacijaZnanjaInterfaceDTO {
    /**
     * 
     */
    private static final long serialVersionUID = -6411149825656697348L;

    private Long id;

    private String name;

    public EvaluacijaZnanjaDTO() {
	super();
    }

    public EvaluacijaZnanjaDTO(Long id, String name) {
	super();
	this.id = id;
	this.name = name;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

}
