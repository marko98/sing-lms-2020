package lms.dto;

import lms.dto.interfaces.EvaluacijaZnanjaInterfaceDTO;
import lms.model.enums.IspitniRok;

public class IspitDTO implements EvaluacijaZnanjaInterfaceDTO {

    /**
     * 
     */
    private static final long serialVersionUID = 4006604954259400912L;

    private Long id;

    private String name;

    private IspitniRok ispitniRok;

    public IspitDTO() {
	super();
    }

    public IspitDTO(Long id, String name, IspitniRok ispitniRok) {
	super();
	this.id = id;
	this.name = name;
	this.ispitniRok = ispitniRok;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public IspitniRok getIspitniRok() {
	return ispitniRok;
    }

    public void setIspitniRok(IspitniRok ispitniRok) {
	this.ispitniRok = ispitniRok;
    }

}
