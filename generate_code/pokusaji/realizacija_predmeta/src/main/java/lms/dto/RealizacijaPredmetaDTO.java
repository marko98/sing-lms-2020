package lms.dto;

import lms.dto.interfaces.DTO;
import lms.dto.interfaces.EvaluacijaZnanjaInterfaceDTO;

public class RealizacijaPredmetaDTO implements DTO {
    /**
     * 
     */
    private static final long serialVersionUID = 7874573280019553182L;

    private Long id;

    private EvaluacijaZnanjaInterfaceDTO evaluacijaZnanjaInterfaceDTO;

    public RealizacijaPredmetaDTO() {
	super();
    }

    public RealizacijaPredmetaDTO(Long id, EvaluacijaZnanjaInterfaceDTO evaluacijaZnanjaInterfaceDTO) {
	super();
	this.id = id;
	this.evaluacijaZnanjaInterfaceDTO = evaluacijaZnanjaInterfaceDTO;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public EvaluacijaZnanjaInterfaceDTO getEvaluacijaZnanjaInterfaceDTO() {
	return evaluacijaZnanjaInterfaceDTO;
    }

    public void setEvaluacijaZnanjaInterfaceDTO(EvaluacijaZnanjaInterfaceDTO evaluacijaZnanjaInterfaceDTO) {
	this.evaluacijaZnanjaInterfaceDTO = evaluacijaZnanjaInterfaceDTO;
    }

}
