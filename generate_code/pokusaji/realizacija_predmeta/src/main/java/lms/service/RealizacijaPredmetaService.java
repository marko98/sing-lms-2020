package lms.service;

import lms.model.RealizacijaPredmeta;

import org.springframework.stereotype.Service;

@Service
public class RealizacijaPredmetaService extends CrudService<RealizacijaPredmeta, Long> {

}
