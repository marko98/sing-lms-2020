package lms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.model.EvaluacijaZnanja;

@Controller
@RequestMapping(path = "/api/evaluacija_znanja")
public class EvaluacijaZnanjaController extends CrudController<EvaluacijaZnanja, Long> {

}
