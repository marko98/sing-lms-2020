package lms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.model.RealizacijaPredmeta;

@Controller
@RequestMapping(path = "/api/realizacija_predmeta")
public class RealizacijaPredmetaController extends CrudController<RealizacijaPredmeta, Long> {

}
