package lms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.model.Ispit;

@Controller
@RequestMapping(path = "/api/ispit")
public class IspitController extends CrudController<Ispit, Long> {

}
