import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { ContentTypeInterceptor } from './shared/interceptor/content-type.interceptor';
import { CudService, CUD_SERVICE_NAME } from './shared/service/cud.service';
import { AdresaFactory } from './shared/pattern/creational/factory/adresa-factory.model';
import { GradFactory } from './shared/pattern/creational/factory/grad-factory.model';
import { RegistrovaniKorisnikFactory } from './shared/pattern/creational/factory/registrovani-korisnik-factory.model';
import { DrzavaFactory } from './shared/pattern/creational/factory/drzava-factory.model';
import { StudentFactory } from './shared/pattern/creational/factory/student-factory.model';

@NgModule({
  providers: [
    {
      provide: CUD_SERVICE_NAME.STUDENT_CUD_SERVICE,
      useFactory: (httpClient: HttpClient) =>
        new CudService(
          httpClient,
          'api/student-microservice/student',
          new StudentFactory()
        ),
      deps: [HttpClient],
    },
    {
      provide: CUD_SERVICE_NAME.ADRESA_CUD_SERVICE,
      useFactory: (httpClient: HttpClient) =>
        new CudService(
          httpClient,
          'api/adresa-microservice/adresa',
          new AdresaFactory()
        ),
      deps: [HttpClient],
    },
    {
      provide: CUD_SERVICE_NAME.GRAD_CUD_SERVICE,
      useFactory: (httpClient: HttpClient) =>
        new CudService(
          httpClient,
          'api/adresa-microservice/grad',
          new GradFactory()
        ),
      deps: [HttpClient],
    },
    {
      provide: CUD_SERVICE_NAME.REGISTROVANI_KORISNIK_CUD_SERVICE,
      useFactory: (httpClient: HttpClient) =>
        new CudService(
          httpClient,
          'api/korisnik-microservice/registrovani_korisnik',
          new RegistrovaniKorisnikFactory()
        ),
      deps: [HttpClient],
    },
    {
      provide: CUD_SERVICE_NAME.DRZAVA_CUD_SERVICE,
      useFactory: (httpClient: HttpClient) =>
        new CudService(
          httpClient,
          'api/adresa-microservice/drzava',
          new DrzavaFactory()
        ),
      deps: [HttpClient],
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ContentTypeInterceptor,
      multi: true,
    },
  ],
})
export class CoreModule {}
