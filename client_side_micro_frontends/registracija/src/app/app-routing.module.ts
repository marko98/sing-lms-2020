import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmptyRouteComponent } from './empty-route/empty-route.component';
import { APP_BASE_HREF } from '@angular/common';
import { RegistrovaniKorisnikComponent } from './registrovani-korisnik/registrovani-korisnik.component';
import { StudentComponent } from './student/student.component';

const routes: Routes = [
  {
    path: 'mf_1',
    children: [
      {
        path: 'registrovani_korisnik_registracija',
        component: RegistrovaniKorisnikComponent,
      },
      {
        path: 'student_registracija',
        component: StudentComponent,
      },
      {
        pathMatch: 'full',
        path: '',
        redirectTo: 'registrovani_korisnik_registracija',
      },
      { path: '**', redirectTo: 'registrovani_korisnik_registracija' },
    ],
  },
  { path: '**', component: EmptyRouteComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [{ provide: APP_BASE_HREF, useValue: '' }],
})
export class AppRoutingModule {}
