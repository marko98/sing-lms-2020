import { Component, OnInit, Inject } from '@angular/core';
import {
  Form,
  FormRow,
  MyAutocompleteInterface,
  ROW_ITEM_TYPE,
  Autocomplete,
  FormFieldInputTextInterface,
  FORM_FIELD_INPUT_TYPE,
  VALIDATOR_NAMES,
  FormFieldInputNumberInterface,
  AT_LEAST_ONE_DIGIT_REGEX,
  ButtonInterface,
  MAT_BUTTON_TYPE,
  BUTTON_TYPE,
  MAT_COLOR,
  FormFieldInputPasswordInterface,
  AT_LEAST_ONE_LOWER_CASE_CHARACTER_ONE_UPPER_CASE_CHARACTER_ONE_DIGIT_ONE_SPECIAL_CHARACTER_REGEX,
  DatepickerInterface,
  NUMBERS_ONLY_REGEX,
  FormFieldInputInterface,
  SelectInterface,
} from 'my-angular-form';
import { RegistrovaniKorisnikFactory } from '../shared/pattern/creational/factory/registrovani-korisnik-factory.model';
import { AdresaFactory } from '../shared/pattern/creational/factory/adresa-factory.model';
import { NgForm, Validators } from '@angular/forms';
import { AdresaModelInterface } from '../shared/model/interface/adresa.model-interface';
import { RegistrovaniKorisnikModelInterface } from '../shared/model/interface/registrovani-korisnik.model-interface';
import { Grad } from '../shared/model/entity/grad.model';
import { PageDataSource, OptionItem } from 'autocomplete';
import { BehaviorSubject } from 'rxjs';
import { TipAdrese } from '../shared/enum/tip_adrese.enum';
import { CudService, CUD_SERVICE_NAME } from '../shared/service/cud.service';
import { RegistrovaniKorisnik } from '../shared/model/entity/registrovani_korisnik.model';
import {
  PageableReadFactoryService,
  DRZAVA_PAGEABLE_READ_SERVICE_MODEL_INTERFACE,
  GRAD_PAGEABLE_READ_SERVICE_MODEL_INTERFACE,
} from '../shared/service/pageable-read-factory.service';
import { Student } from '../shared/model/entity/student.model';
import { StudentFactory } from '../shared/pattern/creational/factory/student-factory.model';
import { StudentModelInterface } from '../shared/model/interface/student.model-interface';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css'],
})
export class StudentComponent implements OnInit {
  public showForm: boolean = false;
  public form: Form;

  private _formRowAddNazivUliceBroj: FormRow;
  private _brojDodateAdrese: number = 0;

  // factories
  private _registrovaniKorisnikFactory: RegistrovaniKorisnikFactory = new RegistrovaniKorisnikFactory();
  private _adresaFactory: AdresaFactory = new AdresaFactory();
  private _studentFactory: StudentFactory = new StudentFactory();

  constructor(
    @Inject(CUD_SERVICE_NAME.REGISTROVANI_KORISNIK_CUD_SERVICE)
    private _registrovaniKorisnikCudService: CudService<
      RegistrovaniKorisnik,
      number
    >,
    @Inject(CUD_SERVICE_NAME.ADRESA_CUD_SERVICE)
    private _adresaCudService: CudService<RegistrovaniKorisnik, number>,
    @Inject(CUD_SERVICE_NAME.STUDENT_CUD_SERVICE)
    private _studentCudService: CudService<Student, number>,
    private _pageableReadFactoryService: PageableReadFactoryService,
    private _router: Router
  ) {}

  public onSubmit = (ngForm: NgForm): void => {
    // console.log(ngForm.value);

    if (ngForm.valid) {
      let registrovaniKorisnik = this._registrovaniKorisnikFactory.build({
        ime: ngForm.value.ime,
        prezime: ngForm.value.prezime,
        korisnickoIme: ngForm.value.korisnickoIme,
        lozinka: ngForm.value.lozinka,
        datumRodjenja: ngForm.value.datumRodjenja.toUTCString(),
        jmbg: ngForm.value.jmbg,
        email: ngForm.value.email,
      });

      // console.log('UTC date - Date: ', <Date>ngForm.value.datumRodjenja);
      // console.log(
      //   'UTC date - string: ',
      //   (<Date>ngForm.value.datumRodjenja).toUTCString()
      // );
      // console.log(
      //   'Local date - string: ',
      //   (<Date>ngForm.value.datumRodjenja).toLocaleString()
      // );
      // console.log(
      //   'Local time - string: ',
      //   (<Date>ngForm.value.datumRodjenja).toLocaleTimeString()
      // );

      this._registrovaniKorisnikCudService.save(registrovaniKorisnik).subscribe(
        (
          registrovaniKorisnikModelInterface: RegistrovaniKorisnikModelInterface
        ) => {
          // console.log(registrovaniKorisnikModelInterface);

          this._studentCudService
            .save(
              this._studentFactory.build({
                registrovaniKorisnikDTO: registrovaniKorisnikModelInterface,
              })
            )
            .subscribe((studentDTO: StudentModelInterface) => {
              // console.log(studentDTO);
            });

          let adrese: AdresaModelInterface[] = [];
          for (let kljuc in ngForm.value) {
            if (kljuc.includes('adresaUlica')) {
              let index = kljuc.split(' ')[1];

              adrese.push({
                ulica: ngForm.value[kljuc],
                broj: ngForm.value['adresaBroj ' + index],
                tip: ngForm.value['tipAdrese ' + index],
                gradDTO:
                  ngForm.value['grad ' + index] instanceof Grad
                    ? ngForm.value['grad ' + index]
                    : undefined,
                registrovaniKorisnikDTO: registrovaniKorisnikModelInterface,
              });
            }
          }
          // console.log(ngForm.value.drzava);
          // console.log(ngForm.value.grad);

          adrese.forEach((adresaDTO: AdresaModelInterface) => {
            // console.log(adresaDTO);

            let adresa = this._adresaFactory.build(adresaDTO);
            // console.log(JSON.stringify(adresa));

            this._adresaCudService
              .save(adresa)
              .subscribe((adresaDTO: AdresaModelInterface) => {
                // console.log(adresaDTO);
              });
          });
        },
        (err: HttpErrorResponse) => {
          window.alert(err.message);
        },
        () => {
          window.alert(
            'Registracija studenta uspesna, za dalje informacije obratite se nekom od studentskih sluzbenika'
          );
          this._router.navigate(['/mf_2_nk/univerziteti']);
        }
      );
    }
  };

  public onCancel = (): void => {
    this.showForm = false;
  };

  public onAddDrzavaGradNazivUliceBrojFormRow = (): void => {
    if (this.form) {
      // za svaki autocomplete instanciramo zaseban servis
      let drzavaPageabeleReadService = this._pageableReadFactoryService.getPageableReadService(
        DRZAVA_PAGEABLE_READ_SERVICE_MODEL_INTERFACE
      );
      let gradPageabeleReadService = this._pageableReadFactoryService.getPageableReadService(
        GRAD_PAGEABLE_READ_SERVICE_MODEL_INTERFACE
      );

      let formRowDrzava = new FormRow();
      this.form.addChild(formRowDrzava);
      formRowDrzava.addChildInterface(<MyAutocompleteInterface>{
        type: ROW_ITEM_TYPE.AUTOCOMPLETE,
        controlName: 'drzava ' + this._brojDodateAdrese,
        disabled: false,
        validators: [],
        sortingOptions: ['naziv'],
        inputPlaceHolder: 'Izaberite drzavu',
        pageDataSource: new PageDataSource(drzavaPageabeleReadService),
        criteria: '',
        hiddenSubject: new BehaviorSubject(false),
        appearance: 'outline',
      });
      let drzavaAutocomplete: Autocomplete = <Autocomplete>(
        formRowDrzava.getChildren()[0]
      );

      let formRowGrad = new FormRow();
      this.form.addChild(formRowGrad);
      formRowGrad.addChildInterface(<MyAutocompleteInterface>{
        type: ROW_ITEM_TYPE.AUTOCOMPLETE,
        controlName: 'grad ' + this._brojDodateAdrese,
        disabled: false,
        validators: [],
        sortingOptions: ['naziv'],
        inputPlaceHolder: 'Izaberite grad',
        pageDataSource: new PageDataSource(gradPageabeleReadService),
        criteria: '',
        hiddenSubject: new BehaviorSubject(true),
        appearance: 'outline',
      });
      let gradAutocomplete: Autocomplete = <Autocomplete>(
        formRowGrad.getChildren()[0]
      );

      // subscribe-ujemo se odmah na promene autocomplet-a od kojeg zavisimo
      drzavaAutocomplete
        .getValueSubject()
        .subscribe((optionItem: OptionItem) => {
          // console.log(optionItem);
          if (optionItem) {
            // console.log(optionItem);
            gradAutocomplete
              .getPageDataSource()
              .changePath(
                'api/adresa-microservice/grad/drzava/' +
                  optionItem.getEntitet().getId().toString()
              );
            gradAutocomplete
              .getPageDataSource()
              .changeCriteriaPath(
                'api/adresa-microservice/grad/drzava/' +
                  optionItem.getEntitet().getId().toString() +
                  '/naziv/'
              );
            gradAutocomplete.getHiddenSubject().next(false);
          } else {
            gradPageabeleReadService.resetPathAndCriteriaPathToDefault();
            gradAutocomplete.setValue(undefined);
            gradAutocomplete
              .getValueSubject()
              .next(gradAutocomplete.getValue());
            gradAutocomplete.setCriteria('');
            gradAutocomplete.getHiddenSubject().next(true);
          }
        });

      let formRowAdresaStanovanjaBroj = new FormRow();
      this.form.addChild(formRowAdresaStanovanjaBroj);
      formRowAdresaStanovanjaBroj.addChildInterface(<
        FormFieldInputTextInterface
      >{
        type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
        formFieldInputType: FORM_FIELD_INPUT_TYPE.TEXT,
        controlName: 'adresaUlica ' + this._brojDodateAdrese,
        labelName: 'Naziv Vase ulice:',
        appearance: 'outline',
        placeholder: 'unesite naziv Vase ulice',
        validators: [
          {
            message: 'ulica je obavezna',
            name: VALIDATOR_NAMES.REQUIRED,
            validatorFn: Validators.required,
          },
        ],
      });

      formRowAdresaStanovanjaBroj.addChildInterface(<
        FormFieldInputNumberInterface
      >(<FormFieldInputTextInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
        formFieldInputType: FORM_FIELD_INPUT_TYPE.TEXT,
        controlName: 'adresaBroj ' + this._brojDodateAdrese,
        labelName: 'Broj kuce/zgrade:',
        appearance: 'outline',
        placeholder: 'unesite broj Vase kuce/zgrade',
        validators: [
          {
            message: 'broj kuce/zgrade je obavezan',
            name: VALIDATOR_NAMES.REQUIRED,
            validatorFn: Validators.required,
          },
          {
            message: 'broj kuce/zgrade mora sadrzati cifru',
            name: VALIDATOR_NAMES.PATTERN,
            validatorFn: Validators.pattern(AT_LEAST_ONE_DIGIT_REGEX),
          },
        ],
      }));

      let formRowTipAdrese = new FormRow();
      this.form.addChild(formRowTipAdrese);
      formRowTipAdrese.addChildInterface(<SelectInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
        controlName: 'tipAdrese ' + this._brojDodateAdrese,
        labelName: 'Izaberite tip adrese',
        multiple: false,
        optionValues: [
          { value: TipAdrese.PRIMARNA, textToShow: 'primarna' },
          { value: TipAdrese.SEKUNDARNA, textToShow: 'sekundarna' },
          { value: TipAdrese.SEKUNDARNA, textToShow: 'ostalo' },
        ],
        validators: [
          {
            message: 'tip adrese je obavezan',
            name: VALIDATOR_NAMES.REQUIRED,
            validatorFn: Validators.required,
          },
        ],
      });

      formRowTipAdrese.addChildInterface(<ButtonInterface>{
        type: ROW_ITEM_TYPE.BUTTON,
        controlName: 'obrisi adresu',
        color: MAT_COLOR.EMPTY,
        matButtonType: MAT_BUTTON_TYPE.RAISED,
        buttonType: BUTTON_TYPE.BUTTON,
        disabled: false,
        functionToExecute: () => {
          this.form.removeChild(formRowGrad);
          this.form.removeChild(formRowDrzava);
          this.form.removeChild(formRowAdresaStanovanjaBroj);
          this.form.removeChild(formRowTipAdrese);
        },
        context: 'obrisi adresu',
      });

      this.addFormRowAddDrzavaGradNazivUliceBrojFormRow();

      this._brojDodateAdrese += 1;
    }
  };

  public addFormRowAddDrzavaGradNazivUliceBrojFormRow = (): void => {
    if (this._formRowAddNazivUliceBroj) {
      this.form.removeChild(this._formRowAddNazivUliceBroj);
      this._formRowAddNazivUliceBroj = undefined;
    }
    this._formRowAddNazivUliceBroj = new FormRow();
    this.form.addChild(this._formRowAddNazivUliceBroj);
    this._formRowAddNazivUliceBroj.addChildInterface(<ButtonInterface>{
      type: ROW_ITEM_TYPE.BUTTON,
      controlName: 'dodaj adresu',
      color: MAT_COLOR.EMPTY,
      matButtonType: MAT_BUTTON_TYPE.RAISED,
      buttonType: BUTTON_TYPE.BUTTON,
      disabled: false,
      functionToExecute: this.onAddDrzavaGradNazivUliceBrojFormRow,
      context: 'dodaj adresu',
    });
  };

  public onInitForm = (): void => {
    this.form = new Form({
      showSubmitButton: true,
      submitButtonText: 'potvrdi',
      showCancelButton: true,
      cancelButtonText: 'odustani',
      showResetButton: true,
      resetButtonText: 'isprazni formu',
      submitOnlyIfFormValid: true,
    });

    let formRowImePrezime = new FormRow();
    this.form.addChild(formRowImePrezime);
    formRowImePrezime.addChildInterface(<FormFieldInputTextInterface>{
      type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
      formFieldInputType: FORM_FIELD_INPUT_TYPE.TEXT,
      controlName: 'ime',
      labelName: 'Vase ime:',
      appearance: 'outline',
      placeholder: 'unesite Vase ime',
      validators: [
        {
          message: 'ime je obavezno',
          name: VALIDATOR_NAMES.REQUIRED,
          validatorFn: Validators.required,
        },
      ],
    });
    formRowImePrezime.addChildInterface(<FormFieldInputTextInterface>{
      type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
      formFieldInputType: FORM_FIELD_INPUT_TYPE.TEXT,
      controlName: 'prezime',
      labelName: 'Vase prezime:',
      appearance: 'outline',
      placeholder: 'unesite Vase prezime',
      validators: [
        {
          message: 'prezime je obavezno',
          name: VALIDATOR_NAMES.REQUIRED,
          validatorFn: Validators.required,
        },
      ],
    });

    let formRowKorisnickoImeLozinka = new FormRow();
    this.form.addChild(formRowKorisnickoImeLozinka);
    formRowKorisnickoImeLozinka.addChildInterface(<FormFieldInputTextInterface>{
      type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
      formFieldInputType: FORM_FIELD_INPUT_TYPE.TEXT,
      controlName: 'korisnickoIme',
      labelName: 'Vase korisnicko ime:',
      appearance: 'outline',
      placeholder: 'unesite Vase korisnicko ime',
      validators: [
        {
          message: 'korisnicko ime je obavezno',
          name: VALIDATOR_NAMES.REQUIRED,
          validatorFn: Validators.required,
        },
        {
          message: 'korisnicko ime mora biti najmanje 5 karaktera dugacko',
          name: VALIDATOR_NAMES.MIN_LENGTH,
          validatorFn: Validators.minLength(5),
          length: 5,
        },
      ],
      showHintAboutMinMaxLength: true,
    });

    formRowKorisnickoImeLozinka.addChildInterface(<
      FormFieldInputPasswordInterface
    >{
      type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
      formFieldInputType: FORM_FIELD_INPUT_TYPE.PASSWORD,
      controlName: 'lozinka',
      labelName: 'Vasa lozinka',
      appearance: 'outline',
      placeholder: 'unesite Vasu lozinku',
      validators: [
        {
          name: VALIDATOR_NAMES.REQUIRED,
          message: 'lozinka je obazvezna',
          validatorFn: Validators.required,
        },
        {
          message: 'lozinka mora biti najmanje 6 karaktera dugacka',
          name: VALIDATOR_NAMES.MIN_LENGTH,
          validatorFn: Validators.minLength(6),
          length: 6,
        },
        {
          message:
            'lozinka mora imati bar jedno malo slovo, jedno veliko slovo, jedan broj i specijalan karakter',
          name: VALIDATOR_NAMES.PATTERN,
          validatorFn: Validators.pattern(
            AT_LEAST_ONE_LOWER_CASE_CHARACTER_ONE_UPPER_CASE_CHARACTER_ONE_DIGIT_ONE_SPECIAL_CHARACTER_REGEX
          ),
        },
      ],
      matImages: 'menu',
      showPasswordInMs: 1500,
      showHintAboutMinMaxLength: true,
    });

    let formRowDatumRodjenjaJmbg = new FormRow();
    this.form.addChild(formRowDatumRodjenjaJmbg);
    formRowDatumRodjenjaJmbg.addChildInterface(<DatepickerInterface>{
      type: ROW_ITEM_TYPE.DATEPICKER,
      labelName: 'Vas datum rodjenja',
      controlName: 'datumRodjenja',
      color: MAT_COLOR.PRIMARY,
      min: new Date(1900, 0, 1),
      max: new Date(new Date().getFullYear() - 18, 0, 1),
      startAt: new Date(new Date().getFullYear() - 20, 0, 1),
      validators: [
        {
          message: 'Date is required',
          name: VALIDATOR_NAMES.REQUIRED,
          validatorFn: Validators.required,
        },
      ],
      appearance: 'outline',
      timepicker: {
        buttonAlign: 'left',
        /**
         * min i max pisi u formatu:
         * min: 10:00 AM
         * max: 03:00 PM
         */
        min: undefined,
        max: undefined,
        /**
         * Set a default value and time for a timepicker. The format of the time is in 24 hours notation 23:00.
         * A Date string won't work.
         */
        defaultValue: '00:00',
        labelName: 'Izaberite vreme',
        disabled: false,
      },
    });

    formRowDatumRodjenjaJmbg.addChildInterface(<FormFieldInputTextInterface>{
      type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
      formFieldInputType: FORM_FIELD_INPUT_TYPE.TEXT,
      controlName: 'jmbg',
      labelName: 'Vas jmbg:',
      appearance: 'outline',
      placeholder: 'unesite Vas jmbg',
      validators: [
        {
          message: 'jmbg je obazvezan',
          name: VALIDATOR_NAMES.REQUIRED,
          validatorFn: Validators.required,
        },
        {
          message: 'jmbg mora biti 13 karaktera dugacak',
          name: VALIDATOR_NAMES.MIN_LENGTH,
          validatorFn: Validators.minLength(13),
          length: 13,
        },
        {
          message: 'jmbg mora biti 13 karaktera dugacak',
          name: VALIDATOR_NAMES.MAX_LENGTH,
          validatorFn: Validators.maxLength(13),
          length: 13,
        },
        {
          message: 'jmbg nije ispravan',
          name: VALIDATOR_NAMES.PATTERN,
          validatorFn: Validators.pattern(NUMBERS_ONLY_REGEX),
        },
      ],
      showHintAboutMinMaxLength: true,
    });

    let formRowEmail = new FormRow();
    this.form.addChild(formRowEmail);
    formRowEmail.addChildInterface(<FormFieldInputInterface>{
      type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
      formFieldInputType: FORM_FIELD_INPUT_TYPE.EMAIL,
      controlName: 'email',
      labelName: 'Vasa email adresa',
      appearance: 'outline',
      validators: [
        {
          message: 'email nije ispravan',
          name: VALIDATOR_NAMES.EMAIL,
          validatorFn: Validators.email,
        },
        {
          message: 'email je obavezan',
          name: VALIDATOR_NAMES.REQUIRED,
          validatorFn: Validators.required,
        },
      ],
    });

    this.onAddDrzavaGradNazivUliceBrojFormRow();

    // this.form.disableAllRowItems();

    this.showForm = true;
  };

  ngOnInit(): void {
    this.onInitForm();

    console.log('StudentComponent init');
  }

  ngOnDestroy(): void {
    console.log('StudentComponent destroyed');
  }
}
