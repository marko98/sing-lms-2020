import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from './material.module';
import { MyAngularFormModule } from 'my-angular-form';
import { RegistrovaniKorisnikComponent } from '../registrovani-korisnik/registrovani-korisnik.component';
import { StudentComponent } from '../student/student.component';
import { AssetUrlPipe } from './pipe/asset-url-pipe.pipe';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [RegistrovaniKorisnikComponent, StudentComponent, AssetUrlPipe],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MaterialModule,
    MyAngularFormModule,
    RouterModule,
  ],
  exports: [
    RegistrovaniKorisnikComponent,
    StudentComponent,

    AssetUrlPipe,

    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MaterialModule,
    MyAngularFormModule,
    RouterModule,
  ],
  entryComponents: [],
})
export class SharedModule {}
