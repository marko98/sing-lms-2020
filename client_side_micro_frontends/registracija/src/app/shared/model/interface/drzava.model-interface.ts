import { ENTITET_MODEL_INTERFACE } from 'autocomplete';

export declare interface DrzavaModelInterface extends ENTITET_MODEL_INTERFACE {
  naziv: string;
}
