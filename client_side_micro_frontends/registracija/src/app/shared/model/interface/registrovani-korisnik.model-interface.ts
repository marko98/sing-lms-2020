import { ENTITET_MODEL_INTERFACE } from 'autocomplete';
import { AdresaModelInterface } from './adresa.model-interface';

export declare interface RegistrovaniKorisnikModelInterface
  extends ENTITET_MODEL_INTERFACE {
  korisnickoIme: string;
  lozinka: string;
  email: string;
  datumRodjenja: string;
  jmbg: string;
  ime: string;
  prezime: string;
}
