import { ENTITET_MODEL_INTERFACE } from 'autocomplete';
import { Grad } from '../entity/grad.model';
import { TipAdrese } from '../../enum/tip_adrese.enum';
import { GradModelInterface } from './grad.model-interface';
import { RegistrovaniKorisnikModelInterface } from './registrovani-korisnik.model-interface';

export declare interface AdresaModelInterface extends ENTITET_MODEL_INTERFACE {
  ulica: string;
  broj: string;
  tip: TipAdrese;
  gradDTO: GradModelInterface;
  registrovaniKorisnikDTO: RegistrovaniKorisnikModelInterface;
}
