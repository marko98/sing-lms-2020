import { ENTITET_MODEL_INTERFACE } from 'autocomplete';
import { RegistrovaniKorisnikModelInterface } from './registrovani-korisnik.model-interface';

export declare interface StudentModelInterface extends ENTITET_MODEL_INTERFACE {
  registrovaniKorisnikDTO: RegistrovaniKorisnikModelInterface;
}
