import { ENTITET_MODEL_INTERFACE } from 'autocomplete';
import { DrzavaModelInterface } from './drzava.model-interface';

export declare interface GradModelInterface extends ENTITET_MODEL_INTERFACE {
  drzavaDTO: DrzavaModelInterface;
  naziv: string;
}
