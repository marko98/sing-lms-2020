import { Grad } from './grad.model';
import { Entitet, StanjeModela } from 'autocomplete';
import { TipAdrese } from '../../enum/tip_adrese.enum';
import { RegistrovaniKorisnik } from './registrovani_korisnik.model';

export class Adresa implements Entitet<Adresa, number> {
  private id: number;

  private ulica: string;

  private broj: string;

  private stanje: StanjeModela;

  private version: number;

  private grad: Grad;

  private tip: TipAdrese;

  private registrovaniKorisnik: RegistrovaniKorisnik;

  /**
   * ima i univerzitet, ali unutar ovog mikroservisa nam nece trebati
   */

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getUlica = (): string => {
    return this.ulica;
  };

  public setUlica = (ulica: string): void => {
    this.ulica = ulica;
  };

  public getBroj = (): string => {
    return this.broj;
  };

  public setBroj = (broj: string): void => {
    this.broj = broj;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getGrad = (): Grad => {
    return this.grad;
  };

  public setGrad = (grad: Grad): void => {
    this.grad = grad;
  };

  public getTip = (): TipAdrese => {
    return this.tip;
  };

  public setTip = (tip: TipAdrese): void => {
    this.tip = tip;
  };

  public getRegistrovaniKorisnik = (): RegistrovaniKorisnik => {
    return this.registrovaniKorisnik;
  };

  public setRegistrovaniKorisnik = (
    registrovaniKorisnik: RegistrovaniKorisnik
  ): void => {
    this.registrovaniKorisnik = registrovaniKorisnik;
  };

  public constructor() {}

  // interface OptionItem

  getContextToShow(): string {
    return this.ulica + ' ' + this.broj;
  }

  getEntitet = (): this => {
    return this;
  };
}
