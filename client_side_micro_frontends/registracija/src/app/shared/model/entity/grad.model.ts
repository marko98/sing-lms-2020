import { Drzava } from './drzava.model';
import { Entitet, StanjeModela } from 'autocomplete';

export class Grad implements Entitet<Grad, number> {
  private id: number;

  private naziv: string;

  private stanje: StanjeModela;

  private version: number;

  private drzava: Drzava;

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getNaziv = (): string => {
    return this.naziv;
  };

  public setNaziv = (naziv: string): void => {
    this.naziv = naziv;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getDrzava = (): Drzava => {
    return this.drzava;
  };

  public setDrzava = (drzava: Drzava): void => {
    this.drzava = drzava;
  };

  public constructor() {}

  public getContextToShow = (): string => {
    return this.naziv;
  };

  public getEntitet = (): this => {
    return this;
  };
}
