import { Entitet, OptionItem, StanjeModela } from 'autocomplete';

export class Drzava implements Entitet<Drzava, number>, OptionItem {
  private id: number;

  private naziv: string;

  private stanje: StanjeModela;

  private version: number;

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getNaziv = (): string => {
    return this.naziv;
  };

  public setNaziv = (naziv: string): void => {
    this.naziv = naziv;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public constructor() {}

  // interface OptionItem

  getContextToShow = (): string => {
    return this.naziv;
  };

  getEntitet = (): this => {
    return this;
  };
}
