import { NgModule } from '@angular/core';

import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';

const material = [MatButtonModule, MatFormFieldModule];

@NgModule({
  imports: [...material],
  exports: [...material],
})
export class MaterialModule {}
