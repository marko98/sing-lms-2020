import { Injectable, InjectionToken, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  EntitetFactory,
  Entitet,
  PageableReadServiceInterface,
} from 'autocomplete';
import { SCHEMA, HOST, PORT, LOCALHOST } from '../const';
import { PageableReadService } from './pageable-read.service';
import { Drzava } from '../model/entity/drzava.model';
import { DrzavaFactory } from '../pattern/creational/factory/drzava-factory.model';
import { Grad } from '../model/entity/grad.model';
import { GradFactory } from '../pattern/creational/factory/grad-factory.model';

export const DRZAVA_PAGEABLE_READ_SERVICE_MODEL_INTERFACE: PAGEABLE_READ_SERVICE_MODEL_INTERFACE = {
  path: 'api/adresa-microservice/drzava/',
  pathCriteria: 'api/adresa-microservice/drzava/naziv',
  entitet: new Drzava(),
  entitetFactory: new DrzavaFactory(),
};

export const GRAD_PAGEABLE_READ_SERVICE_MODEL_INTERFACE: PAGEABLE_READ_SERVICE_MODEL_INTERFACE = {
  path: 'api/adresa-microservice/grad/',
  pathCriteria: 'api/adresa-microservice/grad/naziv',
  entitet: new Grad(),
  entitetFactory: new GradFactory(),
};

export declare interface PAGEABLE_READ_SERVICE_MODEL_INTERFACE {
  path: string;
  pathCriteria: string;
  entitet: Entitet<any, number>;
  entitetFactory: EntitetFactory<any, any>;
}

@Injectable({ providedIn: 'root' })
export class PageableReadFactoryService {
  constructor(private _httpClient: HttpClient) {}

  getPageableReadService = (
    pageableReadServiceModelInterface: PAGEABLE_READ_SERVICE_MODEL_INTERFACE
  ): PageableReadServiceInterface<
    typeof pageableReadServiceModelInterface.entitet,
    number
  > => {
    const PAGEABLE_READ_SERVICE_TOKEN = new InjectionToken<
      PageableReadServiceInterface<
        typeof pageableReadServiceModelInterface.entitet,
        number
      >
    >('Manually constructed PageableReadService', {
      factory: () =>
        new PageableReadService<
          typeof pageableReadServiceModelInterface.entitet,
          number
        >(
          this._httpClient,
          SCHEMA,
          LOCALHOST,
          PORT,
          pageableReadServiceModelInterface.path,
          pageableReadServiceModelInterface.pathCriteria,
          pageableReadServiceModelInterface.entitetFactory
        ),
    });

    const injector = Injector.create({
      providers: [
        {
          provide: PAGEABLE_READ_SERVICE_TOKEN,
          useFactory: () =>
            new PageableReadService<
              typeof pageableReadServiceModelInterface.entitet,
              number
            >(
              this._httpClient,
              SCHEMA,
              LOCALHOST,
              PORT,
              pageableReadServiceModelInterface.path,
              pageableReadServiceModelInterface.pathCriteria,
              pageableReadServiceModelInterface.entitetFactory
            ),
        },
      ],
    });

    return injector.get(PAGEABLE_READ_SERVICE_TOKEN);
  };
}
