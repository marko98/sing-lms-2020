import { CudServiceInterface } from '../cud.service';
import { Entitet, PageableReadServiceInterface } from 'autocomplete';

export declare interface PageableCrudInterface<T extends Entitet<T, ID>, ID> {
  getCudService(): CudServiceInterface<T, ID>;
  getPageableReadServiceService(): PageableReadServiceInterface<T, ID>;
}
