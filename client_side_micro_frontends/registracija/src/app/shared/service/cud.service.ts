import { HttpClient } from '@angular/common/http';

// const
import { SCHEMA, HOST, PORT, LOCALHOST } from '../const';

// model
import { Observable as CustomObservable } from 'autocomplete';

// interface
import {
  Entitet,
  EntitetFactory,
  Identifikacija,
  ENTITET_MODEL_INTERFACE,
} from 'autocomplete';
import { Observable } from 'rxjs';
import { tap, share } from 'rxjs/operators';

export const CUD_SERVICE_NAME = {
  ADRESA_CUD_SERVICE: 'ADRESA_CUD_SERVICE',
  GRAD_CUD_SERVICE: 'GRAD_CUD_SERVICE',
  DRZAVA_CUD_SERVICE: 'DRZAVA_CUD_SERVICE',
  REGISTROVANI_KORISNIK_CUD_SERVICE: 'REGISTROVANI_KORISNIK_CUD_SERVICE',
  STUDENT_CUD_SERVICE: 'STUDENT_CUD_SERVICE',
};

export declare interface CudServiceInterface<T extends Entitet<T, ID>, ID> {
  save(entitet: Entitet<T, ID>): Observable<ENTITET_MODEL_INTERFACE>;
  deleteByIdentificator(identifikacija: Identifikacija<ID>): void;
  delete(identifikacija: Identifikacija<ID>): void;
}

export class CudService<T extends Entitet<T, ID>, ID> extends CustomObservable
  implements CudServiceInterface<T, ID> {
  protected url: string = SCHEMA + '://' + LOCALHOST + ':' + PORT + '/';
  protected entiteti: Entitet<T, ID>[] = [];

  public context: string = '';

  constructor(
    protected httpClient: HttpClient,
    path: string,
    protected factory: EntitetFactory<T, ID>
  ) {
    super();
    this.url += path;

    this.context = path;
  }

  // public getAll = (): Entitet<T, ID>[] => {
  //   // vracamo kopiju
  //   return this.entiteti.slice();
  // };

  // public getOne = (identifikacija: Identifikacija<ID>): Entitet<T, ID> => {
  //   return this.entiteti.find(
  //     (entitet: Entitet<T, ID>) => entitet.getId() === identifikacija.getId()
  //   );
  // };

  // // read all
  // protected findAll = (): void => {
  //   this.httpClient.get<ENTITET_MODEL_INTERFACE[]>(this.url).subscribe(
  //     (entiteti_modeli_interfejsi: ENTITET_MODEL_INTERFACE[]) => {
  //       this.entiteti = entiteti_modeli_interfejsi.map(
  //         (entitet_model_interfejs: ENTITET_MODEL_INTERFACE) => {
  //           return this.factory.build(entitet_model_interfejs);
  //         }
  //       );
  //       this.notify();
  //     },
  //     (err) => {
  //       this.showError(err);
  //     }
  //   );
  // };

  // //   read one
  // public findOne = (
  //   identifikacija: Identifikacija<ID>
  // ): import('rxjs').Observable<ENTITET_MODEL_INTERFACE> => {
  //   return this.httpClient.get<ENTITET_MODEL_INTERFACE>(
  //     this.url + '/' + identifikacija.getId().toString()
  //   );
  // };

  // create and update
  public save = (
    entitet: Entitet<T, ID>
  ): Observable<ENTITET_MODEL_INTERFACE> => {
    // pronadjimo entitet sa datim id ukoliko postoji
    if (entitet.getId()) {
      let observable: Observable<ENTITET_MODEL_INTERFACE> = this.httpClient
        .get<ENTITET_MODEL_INTERFACE>(
          this.url + '/' + entitet.getId().toString()
        )
        .pipe(share());

      observable.subscribe(
        (entitetModelInterfejs: ENTITET_MODEL_INTERFACE) => {
          // entitet je pronadjen, update-ujemo ga
          let entitet: Entitet<T, ID> = this.factory.build(
            entitetModelInterfejs
          );
          this.httpClient
            .put(
              this.url + '/' + entitet.getId().toString(),
              JSON.stringify(entitet)
            )
            .subscribe(
              (updatedEntitetModelInterfejs: ENTITET_MODEL_INTERFACE) => {
                let updatedEntitet: Entitet<T, ID> = this.factory.build(
                  updatedEntitetModelInterfejs
                );
                this.entiteti.filter((entitet) => {
                  if (entitet.getId() === updatedEntitet.getId())
                    entitet = updatedEntitet;
                  return true;
                });
                this.notify();
              },
              (err) => {
                this.showError(err);
              }
            );
        },
        (err) => {
          // if (err.status == 404) {
          //   // entitet nije pronadjen, kreiramo ga
          //   observable = this.httpClient
          //     .post(this.url, JSON.stringify(entitet))
          //     .pipe(share());

          //   observable.subscribe(
          //     (entitetModelInterfejs: ENTITET_MODEL_INTERFACE) => {
          //       this.entiteti.push(this.factory.build(entitetModelInterfejs));
          //       this.notify();
          //     },
          //     (err) => {
          //       this.showError(err);
          //     }
          //   );
          // } else {
          //   this.showError(err);
          // }
          this.showError(err);
        }
      );

      return observable;
    } else {
      // id ne postoji, kreiramo entitet
      let observable: Observable<ENTITET_MODEL_INTERFACE> = this.httpClient
        .post(this.url, JSON.stringify(entitet))
        .pipe(
          tap(() => {
            // console.log('saljem zahtev');
          }),
          share()
        );

      observable.subscribe(
        (entitetModelInterfejs: ENTITET_MODEL_INTERFACE) => {
          this.entiteti.push(this.factory.build(entitetModelInterfejs));
          this.notify();
        },
        (err) => {
          this.showError(err);
        }
      );
      return observable;
    }
  };

  // delete by id
  public deleteByIdentificator = (identifikacija: Identifikacija<ID>): void => {
    this.httpClient
      .delete(this.url + '/' + identifikacija.getId().toString())
      .subscribe(
        (result) => {
          console.log(result);
          this.entiteti = this.entiteti.filter(
            (entitet: T) => entitet.getId() !== identifikacija.getId()
          );
          this.notify();
        },
        (err) => {
          this.showError(err);
        }
      );
  };

  //   delete by model
  public delete = (identifikacija: Identifikacija<ID>): void => {
    this.deleteByIdentificator(identifikacija);
  };

  protected showError = (err: any): void => {
    console.log(err);
  };
}
