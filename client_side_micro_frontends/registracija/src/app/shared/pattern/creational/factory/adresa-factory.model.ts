import { EntitetFactory } from 'autocomplete';
import { Adresa } from 'src/app/shared/model/entity/adresa.model';
import { AdresaModelInterface } from 'src/app/shared/model/interface/adresa.model-interface';
import { GradFactory } from './grad-factory.model';
import { RegistrovaniKorisnikFactory } from './registrovani-korisnik-factory.model';

export class AdresaFactory implements EntitetFactory<Adresa, number> {
  private _gradFactory: GradFactory = new GradFactory();
  private _registrovaniKorisnik: RegistrovaniKorisnikFactory = new RegistrovaniKorisnikFactory();

  build = (adresaModelInterfejs?: AdresaModelInterface): Adresa => {
    let adresa: Adresa = new Adresa();

    if (adresaModelInterfejs) {
      adresa.setId(adresaModelInterfejs.id);
      adresa.setStanje(adresaModelInterfejs.stanje);
      adresa.setVersion(adresaModelInterfejs.version);
      adresa.setBroj(adresaModelInterfejs.broj);
      adresa.setTip(adresaModelInterfejs.tip);
      adresa.setUlica(adresaModelInterfejs.ulica);

      // klasa za sebe, zato koristimo njegovu fabriku
      if (adresaModelInterfejs.registrovaniKorisnikDTO)
        adresa.setRegistrovaniKorisnik(
          this._registrovaniKorisnik.build(
            adresaModelInterfejs.registrovaniKorisnikDTO
          )
        );
      if (adresaModelInterfejs.gradDTO)
        adresa.setGrad(this._gradFactory.build(adresaModelInterfejs.gradDTO));
    }

    return adresa;
  };
}
