import { EntitetFactory } from 'autocomplete';
import { RegistrovaniKorisnikFactory } from './registrovani-korisnik-factory.model';
import { Student } from 'src/app/shared/model/entity/student.model';
import { StudentModelInterface } from 'src/app/shared/model/interface/student.model-interface';

export class StudentFactory implements EntitetFactory<Student, number> {
  private _registrovaniKorisnik: RegistrovaniKorisnikFactory = new RegistrovaniKorisnikFactory();

  build = (studentModelInterfejs?: StudentModelInterface): Student => {
    let student: Student = new Student();

    if (studentModelInterfejs) {
      student.setId(studentModelInterfejs.id);
      student.setStanje(studentModelInterfejs.stanje);
      student.setVersion(studentModelInterfejs.version);

      // klasa za sebe, zato koristimo njegovu fabriku
      if (studentModelInterfejs.registrovaniKorisnikDTO)
        student.setRegistrovaniKorisnik(
          this._registrovaniKorisnik.build(
            studentModelInterfejs.registrovaniKorisnikDTO
          )
        );
    }

    return student;
  };
}
