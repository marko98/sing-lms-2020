import { EntitetFactory } from 'autocomplete';
import { RegistrovaniKorisnik } from 'src/app/shared/model/entity/registrovani_korisnik.model';
import { RegistrovaniKorisnikModelInterface } from 'src/app/shared/model/interface/registrovani-korisnik.model-interface';

export class RegistrovaniKorisnikFactory
  implements EntitetFactory<RegistrovaniKorisnik, number> {
  build = (
    registrovaniKorisnikModelInterface?: RegistrovaniKorisnikModelInterface
  ): RegistrovaniKorisnik => {
    let regKorisnik = new RegistrovaniKorisnik();

    if (registrovaniKorisnikModelInterface) {
      regKorisnik.setId(registrovaniKorisnikModelInterface.id);
      regKorisnik.setStanje(registrovaniKorisnikModelInterface.stanje);
      regKorisnik.setVersion(registrovaniKorisnikModelInterface.version);

      // bice vreme po grinicu, a mi prilikom prikazivanja neka ga prebacimo u lokalno (this.datumRodjenja.toLocaleString())
      let datumRodjenja = new Date(
        registrovaniKorisnikModelInterface.datumRodjenja
      );
      regKorisnik.setDatumRodjenja(datumRodjenja);
      regKorisnik.setEmail(registrovaniKorisnikModelInterface.email);
      regKorisnik.setIme(registrovaniKorisnikModelInterface.ime);
      regKorisnik.setJmbg(registrovaniKorisnikModelInterface.jmbg);
      regKorisnik.setKorisnickoIme(
        registrovaniKorisnikModelInterface.korisnickoIme
      );
      regKorisnik.setLozinka(registrovaniKorisnikModelInterface.lozinka);
      regKorisnik.setPrezime(registrovaniKorisnikModelInterface.prezime);
    }

    return regKorisnik;
  };
}
