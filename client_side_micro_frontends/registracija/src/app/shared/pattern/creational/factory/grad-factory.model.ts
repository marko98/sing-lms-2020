import { EntitetFactory } from 'autocomplete';
import { Grad } from 'src/app/shared/model/entity/grad.model';
import { GradModelInterface } from 'src/app/shared/model/interface/grad.model-interface';
import { DrzavaFactory } from './drzava-factory.model';

export class GradFactory implements EntitetFactory<Grad, number> {
  private _drzavaFactory: DrzavaFactory = new DrzavaFactory();

  build = (gradModelInterfejs?: GradModelInterface): Grad => {
    let grad: Grad = new Grad();

    if (gradModelInterfejs) {
      grad.setId(gradModelInterfejs.id);
      grad.setNaziv(gradModelInterfejs.naziv);
      grad.setStanje(gradModelInterfejs.stanje);
      grad.setVersion(gradModelInterfejs.version);

      grad.setDrzava(this._drzavaFactory.build(gradModelInterfejs.drzavaDTO));
    }

    return grad;
  };
}
