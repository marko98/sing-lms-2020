import { EntitetFactory } from 'autocomplete';
import { Drzava } from 'src/app/shared/model/entity/drzava.model';
import { DrzavaModelInterface } from 'src/app/shared/model/interface/drzava.model-interface';

export class DrzavaFactory implements EntitetFactory<Drzava, number> {
  build = (drzavaModelInterfejs?: DrzavaModelInterface): Drzava => {
    let drzava: Drzava = new Drzava();

    if (drzavaModelInterfejs) {
      drzava.setId(drzavaModelInterfejs.id);
      drzava.setNaziv(drzavaModelInterfejs.naziv);
      drzava.setStanje(drzavaModelInterfejs.stanje);
      drzava.setVersion(drzavaModelInterfejs.version);
    }

    return drzava;
  };
}
