import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ChangeDetectorRef,
} from '@angular/core';
import { PrijemnoSanduceService } from '../shared/service/prijemno_sanduce.service';
import { Subscription, BehaviorSubject } from 'rxjs';
import { AuthService } from '../shared/service/auth.service';

@Component({
  selector: 'app-prijemno-sanduce',
  templateUrl: './prijemno-sanduce.component.html',
  styleUrls: ['./prijemno-sanduce.component.css'],
})
export class PrijemnoSanduceComponent implements OnInit, OnDestroy {
  public connectionEstablished: boolean = false;
  public receivedMessages: string[] = [];
  public receivedRemoteMediaStreams: MediaStream[] = [];
  public isCameraOn: boolean = false;

  private _peerConnection: RTCPeerConnection;
  private _sendDataChannel: RTCDataChannel;
  private _receiveDataChannel: RTCDataChannel;
  private _mediaStreamSubscription: Subscription;
  private _mediaStream: MediaStream;
  private _rtcRtpSenders: RTCRtpSender[] = [];

  constructor(
    private _prijemnoSanduce: PrijemnoSanduceService,
    private _cdr: ChangeDetectorRef,
    private _authService: AuthService
  ) {
    this._peerConnection = new RTCPeerConnection();
    this._subscribeToWs();
    this._setListenersOnPeerConnection();
    this._setupRTCSendDataChannel();
  }

  public onSendMessage = (textAreaEl: HTMLTextAreaElement): void => {
    // console.log(textAreaEl.value);
    if (this._sendDataChannel) {
      let msg =
        this._prijemnoSanduce.authUser.korisnickoIme + ': ' + textAreaEl.value;
      this._sendDataChannel.send(msg);
      this.receivedMessages.push(msg);
      textAreaEl.value = '';
    }
  };

  public onLogout = (): void => {
    this.onDisconnectPeers();
    this._authService.onLogout();
  };

  public onCloseRemoteMediaStream = (mediaStream) => {
    this.receivedRemoteMediaStreams = this.receivedRemoteMediaStreams.filter(
      (ms) => ms !== mediaStream
    );
  };

  public onCameraOn = (videoEl: HTMLVideoElement): void => {
    if (this._mediaStream) {
      videoEl.srcObject = this._mediaStream;
      if (this._peerConnection)
        this._mediaStream.getTracks().forEach((track) => {
          // console.log(track);
          this._rtcRtpSenders.push(
            this._peerConnection.addTrack(track, this._mediaStream)
          );
        });
      this.isCameraOn = true;
      if (this.connectionEstablished) this.onCreateAndSendOffer();
    }
  };

  public onCameraOff = (videoEl: HTMLVideoElement): void => {
    videoEl.srcObject = undefined;
    this.isCameraOn = false;
    this._rtcRtpSenders.forEach((rtcRtpSender) => {
      this._peerConnection.removeTrack(rtcRtpSender);
    });
  };

  public onDisconnectPeers = (): void => {
    // Close the RTCDataChannels if they're open.

    if (this._sendDataChannel) this._sendDataChannel.close();
    if (this._receiveDataChannel) this._receiveDataChannel.close();

    // Close the RTCPeerConnections
    if (this._peerConnection) this._peerConnection.close();

    this._sendDataChannel = undefined;
    this._receiveDataChannel = undefined;
    this._peerConnection = undefined;

    this.connectionEstablished = false;

    this.receivedRemoteMediaStreams = [];
  };

  public onCreateAndSendOffer = (): void => {
    if (!this._peerConnection) {
      this._peerConnection = new RTCPeerConnection();
      this._setListenersOnPeerConnection();
      this._setupRTCSendDataChannel();
    }
    this._peerConnection.createOffer().then(
      (offer) => {
        // When we set the local description on the peerConnection, it triggers an icecandidate event
        this._peerConnection.setLocalDescription(offer);
        console.log('poslata ponuda');
        this._prijemnoSanduce.ws.next({ event: 'offer', data: offer });
      },
      (err) => {
        console.log('Failed creating an offer, err: ', err);
      }
    );
  };

  private _setupRTCSendDataChannel = (): void => {
    this._sendDataChannel = this._peerConnection.createDataChannel(
      'sendDataChannel'
    );

    this._sendDataChannel.onerror = (error) => {
      console.log('Error: ', error);
    };

    this._sendDataChannel.onclose = this._handleSendChannelStatusChange;

    this._sendDataChannel.onopen = this._handleSendChannelStatusChange;

    this._sendDataChannel.onmessage = (event: MessageEvent) => {
      console.log('ja poslao poruku: ', event.data);
    };
  };

  private _setListenersOnPeerConnection = (): void => {
    // Handling ICE Candidates
    /**
     * The icecandidate event triggers again with an empty candidate string when all the candidates are gathered.
     *
     * We must pass this candidate object as well to the remote peer. We pass this empty candidate string to ensure that the remote
     * peer knows that all the icecandidate objects are gathered.
     *
     * Also, the same event is triggered again to indicate that the ICE candidate gathering is complete with the value of candidate
     * object set to null on the event. This need not be passed on to the remote peer.
     */
    this._peerConnection.onicecandidate = (event) => {
      if (event.candidate) {
        this._prijemnoSanduce.ws.next({
          event: 'candidate',
          data: event.candidate,
        });
      }
    };

    // prilikom kreiranja data channel-a od strane nekog drugog peer-a
    this._peerConnection.ondatachannel = (event: RTCDataChannelEvent) => {
      this._receiveDataChannel = event.channel;
      // na primljenu poruku
      this._receiveDataChannel.onmessage = (event: MessageEvent) => {
        // console.log('primljena je poruka: ', event.data);
        this.receivedMessages.push(event.data);
        // console.log(this.receivedMessages);

        this._cdr.detectChanges();
      };

      this._receiveDataChannel.onopen = this._handleReceiveChannelStatusChange;

      this._receiveDataChannel.onclose = this._handleReceiveChannelStatusChange;
    };

    this._peerConnection.ontrack = (event: RTCTrackEvent) => {
      console.log(event);
      let remoteMediaStream = this.receivedRemoteMediaStreams.find(
        (r) => r.id === event.streams[0].id
      );
      if (remoteMediaStream) {
        remoteMediaStream.addTrack(event.track);
      } else {
        remoteMediaStream = event.streams[0];
        remoteMediaStream.addTrack(event.track);
        this.receivedRemoteMediaStreams.push(remoteMediaStream);
      }
    };
  };

  private _handleSendChannelStatusChange = (event) => {
    if (this._sendDataChannel) {
      let state = this._sendDataChannel.readyState;
      if (state === 'connecting') {
        console.log('Send data channel is connecting');
      } else if (state === 'open') {
        console.log('Send data channel is opened');
      } else if (state === 'closing') {
        console.log('Send data channel is closing');
      } else if (state === 'closed') {
        console.log('Send data channel is closed');
      }
    }
  };

  private _handleReceiveChannelStatusChange = (event) => {
    if (this._receiveDataChannel) {
      let state = this._receiveDataChannel.readyState;
      if (state === 'connecting') {
        console.log('Receive data channel is connecting');
      } else if (state === 'open') {
        console.log('Receive data channel is opened');
      } else if (state === 'closing') {
        console.log('Receive data channel is closing');
      } else if (state === 'closed') {
        console.log('Receive data channel is closed');
      }
    }
  };

  private _subscribeToWs = (): void => {
    this._prijemnoSanduce.ws.subscribe(
      (msg) => {
        // console.log(msg);
        try {
          let data = JSON.parse(msg.data);
          if (data.event === 'offer') {
            // Receiving the Offer
            console.log('primljena je ponuda');
            /**
             * When the other peer receives the offer, it must set it as the remote description.
             * In addition, it must generate an answer, which is sent to the initiating peer
             */
            this._peerConnection.setRemoteDescription(
              new RTCSessionDescription(data.data)
            );
            this._peerConnection
              .createAnswer()
              .then((answer) => {
                this._peerConnection.setLocalDescription(answer);
                this.connectionEstablished = true;
                console.log('poslat je odgovor');
                this._prijemnoSanduce.ws.next({
                  event: 'answer',
                  data: answer,
                });
              })
              .catch((err) => console.log(err));
          } else if (data.event === 'candidate') {
            // Receiving the ICE Candidate
            console.log('primljen je strani candidate');
            /**
             * we need to process the ICE candidate sent by the other peer.
             *
             * The remote peer, upon receiving this candidate, should add it to its candidate pool:
             */
            this._peerConnection.addIceCandidate(
              new RTCIceCandidate(data.data)
            );
          } else if (data.event === 'answer') {
            console.log('primljen je odgovor');
            // Receiving the Answer
            /**
             * the initiating peer receives the answer and sets it as the remote description
             */
            this._peerConnection.setRemoteDescription(
              new RTCSessionDescription(data.data)
            );
            this.connectionEstablished = true;
          }
        } catch {}
      },
      (err) => {
        console.log(err);
      },
      () => {
        console.log('ws completed');
        this.connectionEstablished = false;
      }
    );
  };

  ngOnInit(): void {
    this._mediaStreamSubscription = this._prijemnoSanduce.mediaStream.subscribe(
      (mediaStream: MediaStream) => {
        this._mediaStream = mediaStream;
      },
      (err) => {
        console.log(err);
      }
    );
    console.log('PrijemnoSanduceComponent init');
  }

  ngOnDestroy(): void {
    if (this._mediaStreamSubscription)
      this._mediaStreamSubscription.unsubscribe();
    console.log('PrijemnoSanduceComponent init');
  }
}
