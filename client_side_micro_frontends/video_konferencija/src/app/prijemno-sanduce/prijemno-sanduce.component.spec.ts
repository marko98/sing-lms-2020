import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrijemnoSanduceComponent } from './prijemno-sanduce.component';

describe('PrijemnoSanduceComponent', () => {
  let component: PrijemnoSanduceComponent;
  let fixture: ComponentFixture<PrijemnoSanduceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrijemnoSanduceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrijemnoSanduceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
