import { Component, OnInit, OnDestroy } from '@angular/core';
import * as RecordRTC from 'recordrtc';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';
import { WS_MESSAGE_KEYS } from '../shared/model/enum/ws_message_keys.enum';
import { WebSocketMessage } from 'rxjs/internal/observable/dom/WebSocketSubject';
import { saveAs } from 'file-saver';
import { decode } from 'utf8';

const constraints: MediaStreamConstraints = {
  video: true,
  audio: { echoCancellation: true },
};

// npm install -D @types/dom-mediacapture-record
declare var MediaRecorder: any;

@Component({
  selector: 'app-streaming',
  templateUrl: './streaming.component.html',
  styleUrls: ['./streaming.component.css'],
})
export class StreamingComponent implements OnInit, OnDestroy {
  public stream: MediaStream;

  private _mediaRecorder: any;
  private _chunks: any[] = [];
  private _interval;
  private _ws;
  private _wsUrl: string = 'ws://localhost:8102/0_video';

  constructor() {}

  public onPlayLocalVideo = (videoElement: HTMLVideoElement): void => {
    videoElement.srcObject = this.stream;
  };

  public onStopLocalVideo = (videoElement: HTMLVideoElement): void => {
    videoElement.srcObject = undefined;
  };

  private _onDataAvailable = (e) => {
    let blob: Blob = e.data;
    // console.log(blob);
    // this._chunks.push(blob);

    // let chunkSize = 50000;
    // for (let start = 0; start < blob.size; start += chunkSize) {
    //   const chunk = blob.slice(start, start + chunkSize + 1);
    //   let fr = new FileReader();
    //   fr.readAsDataURL(chunk);
    //   fr.onloadend = () => {
    //     let base64Data: string = <string>fr.result;
    //     // console.log(base64Data);
    //     this._ws.next(base64Data);
    //   };
    // }

    let fr = new FileReader();
    fr.readAsDataURL(blob);
    fr.onloadend = () => {
      let base64Data: string = <string>fr.result;
      // console.log(base64Data);
      // console.log(typeof base64Data);
      // console.log('saljem');
      this._ws.next(base64Data);
      // atob(base64Data.split(',')[2]);
      // saveAs(this._base64ToBlob(base64Data.split(',')[2], blob.type), 'hi.mp4');
    };
  };

  private _onStartRecording = (): void => {
    if (this.stream) {
      this._mediaRecorder = new MediaRecorder(this.stream, {
        mimeType: 'video/webm;codecs=vp8',
      });
      this._mediaRecorder.ondataavailable = this._onDataAvailable;

      this._mediaRecorder.start();
      console.log(this._mediaRecorder.state);
      this._interval = setInterval(() => {
        this._mediaRecorder.requestData();
      }, 10);
    }
  };

  private _onStopRecording = (): void => {
    this._mediaRecorder = undefined;
    clearInterval(this._interval);
    this._ws.complete();
  };

  public onStartStreamVideo = (): void => {
    this._ws = webSocket({
      url: this._wsUrl,
      deserializer: (msg) => msg,
    });

    this._ws.subscribe(
      (msg) => {
        console.log(msg.data);
        try {
          if (msg.data === 'hello from /0_video') {
            // console.log(
            //   WS_MESSAGE_KEYS.ON_CONNECTED,
            //   (<any>JSON.parse(msg.data)).message[WS_MESSAGE_KEYS.ON_CONNECTED]
            // );

            this._onStartRecording();
          }
        } catch (error) {
          console.log(error);
        }
      },
      (err) => {
        console.log(err);
        if (this._mediaRecorder) this._onStopRecording();
      },
      () => {
        console.log('ws complete');
        if (this._mediaRecorder) this._onStopRecording();
      }
    );
  };

  public onStopStreamVideo = (): void => {
    this._onStopRecording();
  };

  public getUserMedia = (): void => {
    navigator.mediaDevices
      .getUserMedia(constraints)
      .then((stream: MediaStream) => {
        this.stream = stream;
      })
      .catch((error) => {
        console.error('Error accessing media devices.', error);
        window.alert(error);
      });
  };

  ngOnInit(): void {
    console.log('StreamingComponent init');
  }

  ngOnDestroy(): void {
    console.log('StreamingComponent destroyed');
  }
}
