import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef,
  AfterViewInit,
} from '@angular/core';
import { WS_MESSAGE_KEYS } from '../shared/model/enum/ws_message_keys.enum';
import { webSocket } from 'rxjs/webSocket';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-viewing',
  templateUrl: './viewing.component.html',
  styleUrls: ['./viewing.component.css'],
})
export class ViewingComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('filmic', { static: true }) video: ElementRef;
  private _ws;
  private _wsUrl: string = 'ws://localhost:8102/0_video';

  private _mediaSource: MediaSource;
  private _url;
  private _blobs: ArrayBuffer[] = [];
  private _sourceBuffer: SourceBuffer = null;

  constructor() {}

  private _base64ToBlob(b64Data, contentType = '', sliceSize = 512): Blob {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);
      const byteNumbers = new Array(slice.length);

      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  private _subscribeToWS = (): void => {
    this._ws = webSocket({
      url: this._wsUrl,
      deserializer: (msg) => msg,
    });

    this._ws.subscribe(
      (msg) => {
        // console.log(msg.data);
        try {
          // if ((<string>msg.data).includes('"data', 0)) {
          let m = msg.data.slice(1, msg.data.length - 1);
          // console.log(m);
          // atob(m.split(',')[2]);
          // saveAs(
          //   this._base64ToBlob(m.split(',')[2], m.split(',')[0]),
          //   'hi.mp4'
          // );
          // console.log('stigao paket');
          let blob = this._base64ToBlob(m.split(',')[2], m.split(',')[0]);
          blob.arrayBuffer().then(
            (arrayBuffer: ArrayBuffer) => {
              this._blobs.push(arrayBuffer);
              this._appendToSourceBuffer();
            },
            (err) => {
              console.log(err);
            }
          );
          // }
        } catch (error) {
          // console.log(error);
        }
      },
      (err) => {
        console.log(err);
      },
      () => {
        console.log('ws complete');
      }
    );
  };

  private _appendToSourceBuffer = (): void => {
    if (
      this._mediaSource.readyState === 'open' &&
      this._sourceBuffer &&
      this._sourceBuffer.updating === false
    ) {
      let arrayBuffer: ArrayBuffer = this._blobs.shift();
      if (arrayBuffer) {
        this._sourceBuffer.appendBuffer(arrayBuffer);
        (<HTMLVideoElement>this.video.nativeElement).play();
      }
    }
    // Limit the total buffer size to 20 minutes
    // This way we don't run out of RAM
    if (
      this.video.nativeElement.buffered.length &&
      this.video.nativeElement.buffered.end(0) -
        this.video.nativeElement.buffered.start(0) >
        1200
    ) {
      this._sourceBuffer.remove(
        0,
        this.video.nativeElement.buffered.end(0) - 1200
      );
    }
  };

  ngOnInit(): void {
    console.log('ViewingComponent init');
  }

  ngAfterViewInit() {
    // 1. Create a `MediaSource`
    this._mediaSource = new MediaSource();

    // 2. Create an object URL from the `MediaSource`
    this._url = URL.createObjectURL(this._mediaSource);

    // 3. Set the video's `src` to the object URL
    this.video.nativeElement.src = this._url;

    // 4. On the `sourceopen` event, create a `SourceBuffer`
    (<MediaSource>this._mediaSource).onsourceopen = (event) => {
      // NOTE: Browsers are VERY picky about the codec being EXACTLY
      // right here. Make sure you know which codecs you're using!
      this._sourceBuffer = this._mediaSource.addSourceBuffer(
        'video/webm; codecs="opus,vp8"'
      );
      // console.log('_sourceBuffer', this._sourceBuffer);

      // If we requested any video data prior to setting up the SourceBuffer,
      // we want to make sure we only append one blob at a time
      // this._sourceBuffer.addEventListener(
      //   'updateend',
      //   this._appendToSourceBuffer
      // );
      this._sourceBuffer.onupdateend = this._appendToSourceBuffer;
    };

    this._subscribeToWS();
  }

  ngOnDestroy(): void {
    console.log('ViewingComponent destroyed');
  }
}
