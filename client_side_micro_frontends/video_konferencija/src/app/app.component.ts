import { Component } from '@angular/core';
import { AuthService } from './shared/service/auth.service';
import { MICRO_FRONTENDS_PREFIX } from './shared/const';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'kva-k2';

  constructor(private _authService: AuthService) {
    this._authService.setAfterLoginRoute(
      MICRO_FRONTENDS_PREFIX.MF_6 + '/poruke'
    );
    this._authService.setAfterLogoutRoute(
      MICRO_FRONTENDS_PREFIX.MF_4 + '/login'
    );
    this._authService.onAutoLogin();
  }
}
