import {
  Component,
  OnInit,
  OnDestroy,
  AfterViewInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { Sanduce } from '../sanducadi.component';
import {
  VideoService,
  VideoMessage,
} from 'src/app/shared/service/video.service';
import { take } from 'rxjs/operators';
import { Subscription } from 'rxjs';

const constraints: MediaStreamConstraints = {
  video: true,
  audio: { echoCancellation: true },
};

@Component({
  selector: 'app-sanduce',
  templateUrl: './sanduce.component.html',
  styleUrls: ['./sanduce.component.css'],
})
export class SanduceComponent
  implements OnInit, OnDestroy, AfterViewInit, OnChanges {
  @Input() sanduce: Sanduce;
  @Input() videoMessage: VideoMessage;

  @Output() onCallStartEvent: EventEmitter<void> = new EventEmitter();
  @Output() onCallEndEvent: EventEmitter<void> = new EventEmitter();

  public userBusy: boolean = false;

  private _userBusySubscription: Subscription;

  constructor(private _videoService: VideoService) {}

  public onCallStart = (): void => {
    this.onCallStartEvent.next();
  };

  public onCallEnd = (): void => {
    this.onCallEndEvent.next();
  };

  ngOnInit(): void {
    this._userBusySubscription = this._videoService.userBusy.subscribe(
      (userBusy: boolean) => {
        this.userBusy = userBusy;
      }
    );
    console.log('SanduceComponent destroyed');
  }

  ngAfterViewInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes);
  }

  ngOnDestroy(): void {
    if (this._userBusySubscription) this._userBusySubscription.unsubscribe();
    console.log('SanduceComponent destroyed');
  }
}
