import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SanduceComponent } from './sanduce.component';

describe('SanduceComponent', () => {
  let component: SanduceComponent;
  let fixture: ComponentFixture<SanduceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SanduceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SanduceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
