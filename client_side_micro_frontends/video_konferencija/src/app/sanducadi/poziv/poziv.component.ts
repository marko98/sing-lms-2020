import {
  Component,
  OnInit,
  OnDestroy,
  Inject,
  ViewChild,
  ElementRef,
  AfterViewInit,
} from '@angular/core';
import { Subscription } from 'rxjs';
import {
  VideoService,
  IncomingCallData,
} from 'src/app/shared/service/video.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Sanduce } from '../sanducadi.component';

const constraints: MediaStreamConstraints = {
  video: true,
  audio: { echoCancellation: true },
};

@Component({
  selector: 'app-poziv',
  templateUrl: './poziv.component.html',
  styleUrls: ['./poziv.component.css'],
})
export class PozivComponent implements OnInit, OnDestroy, AfterViewInit {
  public isAccepted: boolean = false;

  private _acceptIncomingCallDataSubscription: Subscription;
  private _userMediaSubscription: Subscription;
  private _incomingCallDataSubscription: Subscription;

  private _mediaSource: MediaSource;
  private _url;
  private _blobs: ArrayBuffer[] = [];
  private _sourceBuffer: SourceBuffer = null;

  private _video: HTMLVideoElement;

  constructor(
    private _videoService: VideoService,
    public dialogRef: MatDialogRef<PozivComponent, Sanduce>,
    @Inject(MAT_DIALOG_DATA) public sanduce: Sanduce
  ) {}

  public onAcceptIncomingCall = (video: HTMLVideoElement): void => {
    this._video = video;

    // 1. Create a `MediaSource`
    this._mediaSource = new MediaSource();

    // 2. Create an object URL from the `MediaSource`
    this._url = URL.createObjectURL(this._mediaSource);

    // 3. Set the video's `src` to the object URL
    this._video.src = this._url;

    // 4. On the `sourceopen` event, create a `SourceBuffer`
    (<MediaSource>this._mediaSource).onsourceopen = (event) => {
      // NOTE: Browsers are VERY picky about the codec being EXACTLY
      // right here. Make sure you know which codecs you're using!
      this._sourceBuffer = this._mediaSource.addSourceBuffer(
        'video/webm; codecs="opus,vp8"'
      );

      // this._sourceBuffer = this._mediaSource.addSourceBuffer(
      //   'video/webm; codecs="opus,vp8"'
      // );
      // console.log('_sourceBuffer', this._sourceBuffer);

      // If we requested any video data prior to setting up the SourceBuffer,
      // we want to make sure we only append one blob at a time
      // this._sourceBuffer.addEventListener(
      //   'updateend',
      //   this._appendToSourceBuffer
      // );
      this._sourceBuffer.onupdateend = this._appendToSourceBuffer;
    };

    this._videoService.onAcceptIncomingCall(this.sanduce);
  };

  public onDeclineIncomingCall = (): void => {
    this._videoService.onDeclineIncomingCall(this.sanduce);
    this.dialogRef.close(this.sanduce);
  };

  public onCallEnd = (): void => {
    this._videoService.onCallEnd(this.sanduce);
    this.dialogRef.close(this.sanduce);
  };

  private _onIncomingCallData = (incomingCallData: IncomingCallData): void => {
    console.log('incomingCallData: ', incomingCallData);

    if (incomingCallData && this._video)
      (<Blob>incomingCallData.videoMessage.data).arrayBuffer().then(
        (arrayBuffer: ArrayBuffer) => {
          this._blobs.push(arrayBuffer);
          this._appendToSourceBuffer();
        },
        (err) => {
          console.log(err);
        }
      );
  };

  private _appendToSourceBuffer = (): void => {
    if (
      this._mediaSource.readyState === 'open' &&
      this._sourceBuffer &&
      this._sourceBuffer.updating === false
    ) {
      let arrayBuffer: ArrayBuffer = this._blobs.shift();
      if (arrayBuffer) {
        this._sourceBuffer.appendBuffer(arrayBuffer);
        (<HTMLVideoElement>this._video).play();
      }
    }
    // Limit the total buffer size to 20 minutes
    // This way we don't run out of RAM
    if (
      this._video.buffered.length &&
      this._video.buffered.end(0) - this._video.buffered.start(0) > 1200
    ) {
      this._sourceBuffer.remove(0, this._video.buffered.end(0) - 1200);
    }
  };

  ngOnInit(): void {
    this._incomingCallDataSubscription = this._videoService.incomingCallData.subscribe(
      this._onIncomingCallData
    );

    this._acceptIncomingCallDataSubscription = this._videoService.acceptIncomingCallData.subscribe(
      (isAccepted: boolean) => {
        this.isAccepted = isAccepted;
      }
    );
  }

  ngAfterViewInit(): void {}

  ngOnDestroy(): void {
    if (this._incomingCallDataSubscription)
      this._incomingCallDataSubscription.unsubscribe();
    if (this._acceptIncomingCallDataSubscription)
      this._acceptIncomingCallDataSubscription.unsubscribe();

    if (this._userMediaSubscription) this._userMediaSubscription.unsubscribe();
  }
}
