import {
  Component,
  OnInit,
  OnDestroy,
  AfterViewInit,
  Inject,
} from '@angular/core';
import { webSocket } from 'rxjs/webSocket';
import { SCHEMA_WS, LOCALHOST } from '../shared/const';
import {
  VideoService,
  VideoMessage,
  IncomingCallData,
} from '../shared/service/video.service';
import { UIService } from '../shared/service/ui.service';
import { Subscription } from 'rxjs';
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatDialog,
} from '@angular/material/dialog';
import { take } from 'rxjs/operators';
import { PozivComponent } from './poziv/poziv.component';

export declare interface Sanduce {
  id: number;
  naziv: string;
}

@Component({
  selector: 'app-sanducadi',
  templateUrl: './sanducadi.component.html',
  styleUrls: ['./sanducadi.component.css'],
})
export class SanducadiComponent implements OnInit, OnDestroy, AfterViewInit {
  public sanducici: Sanduce[] = [];
  public userBusy: boolean = false;

  private _sanduciciSubscription: Subscription;
  private _userBusySubscription: Subscription;
  private _incomingCallEventSubscription: Subscription;

  private _dialogRef: MatDialogRef<PozivComponent, Sanduce>;

  constructor(
    private _videoService: VideoService,
    private _dialog: MatDialog
  ) {}

  public onCallStart = (sanduce: Sanduce): void => {
    this._videoService.onCallStart(sanduce);
  };

  public onCallEnd = (sanduce: Sanduce): void => {
    this._videoService.onCallEnd(sanduce);
  };

  private _onIncomingCallEvent = (sanduce: Sanduce): void => {
    if (!this.userBusy && !this._dialogRef) {
      this._openDialog(sanduce);
    }
  };

  private _openDialog = (sanduce: Sanduce): void => {
    this._dialogRef = this._dialog.open(PozivComponent, {
      // width: '250px',
      data: sanduce,
    });

    this._dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed');
      console.log('result: ', result);

      this._dialogRef = undefined;
    });
  };

  ngOnInit(): void {
    this._incomingCallEventSubscription = this._videoService.incomingCallEvent.subscribe(
      this._onIncomingCallEvent
    );

    this._sanduciciSubscription = this._videoService.sanducici.subscribe(
      (sanducici: Sanduce[]) => {
        this.sanducici = sanducici;
      }
    );

    this._userBusySubscription = this._videoService.userBusy.subscribe(
      (userBusy: boolean) => {
        this.userBusy = userBusy;
      }
    );
    console.log('SanducadiComponent destroyed');
  }

  ngAfterViewInit(): void {}

  ngOnDestroy(): void {
    if (this._sanduciciSubscription) this._sanduciciSubscription.unsubscribe();
    if (this._userBusySubscription) this._userBusySubscription.unsubscribe();

    if (this._incomingCallEventSubscription)
      this._incomingCallEventSubscription.unsubscribe();
    console.log('SanducadiComponent destroyed');
  }
}
