import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SanducadiComponent } from './sanducadi.component';

describe('SanducadiComponent', () => {
  let component: SanducadiComponent;
  let fixture: ComponentFixture<SanducadiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SanducadiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SanducadiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
