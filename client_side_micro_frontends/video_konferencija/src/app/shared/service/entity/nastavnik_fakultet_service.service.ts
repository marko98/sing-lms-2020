import { CrudService } from '../crud.service';
import {
  PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
  WS_PATH,
} from '../../const';
import { Injectable } from '@angular/core';
import { NastavnikFakultet } from '../../model/entity/nastavnik_fakultet.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { NastavnikFakultetFactory } from '../../model/patterns/creational/factory_method/nastavnik_fakultet.factory';
import { CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';

export const NASTAVNIK_FAKULTET_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: NastavnikFakultetFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.NASTAVNIK,
  path: PATH.NASTAVNIK_FAKULTET,
  microservicePort: MICROSERVICE_PORT.NASTAVNIK,
  wsPath: WS_PATH.NASTAVNIK_FAKULTET,
};
@Injectable({ providedIn: 'root' })
export class NastavnikFakultetService extends CrudService<
  NastavnikFakultet,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      NastavnikFakultetFactory.getInstance(),
      MICROSERVICE_NAME.NASTAVNIK,
      PATH.NASTAVNIK_FAKULTET,
      MICROSERVICE_PORT.NASTAVNIK,
      WS_PATH.NASTAVNIK_FAKULTET
    );
    this._findAll();
  }
}
