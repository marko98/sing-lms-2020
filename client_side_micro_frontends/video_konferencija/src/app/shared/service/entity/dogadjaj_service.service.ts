import { CrudService } from '../crud.service';
import {
  PATH,
  WS_PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
} from '../../const';
import { Injectable } from '@angular/core';
import { Dogadjaj } from '../../model/entity/dogadjaj.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { DogadjajFactory } from '../../model/patterns/creational/factory_method/dogadjaj.factory';
import { CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';

export const DOGADJAJ_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: DogadjajFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.NASTAVA,
  path: PATH.DOGADJAJ,
  microservicePort: MICROSERVICE_PORT.NASTAVA,
  wsPath: WS_PATH.DOGADJAJ,
};
@Injectable({ providedIn: 'root' })
export class DogadjajService extends CrudService<Dogadjaj, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      DogadjajFactory.getInstance(),
      MICROSERVICE_NAME.NASTAVA,
      PATH.DOGADJAJ,
      MICROSERVICE_PORT.NASTAVA,
      WS_PATH.DOGADJAJ
    );
    this._findAll();
  }
}
