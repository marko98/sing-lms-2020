import { CrudService, CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';
import {
  PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
  WS_PATH,
} from '../../const';
import { Injectable } from '@angular/core';
import { PohadjanjePredmeta } from '../../model/entity/pohadjanje_predmeta.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { PohadjanjePredmetaFactory } from '../../model/patterns/creational/factory_method/pohadjanje_predmeta.factory';

export const POHADJANJE_PREDMETA_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: PohadjanjePredmetaFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.STUDENT,
  path: PATH.POHADJANJE_PREDMETA,
  microservicePort: MICROSERVICE_PORT.STUDENT,
  wsPath: WS_PATH.POHADJANJE_PREDMETA,
};

@Injectable({ providedIn: 'root' })
export class PohadjanjePredmetaService extends CrudService<
  PohadjanjePredmeta,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      PohadjanjePredmetaFactory.getInstance(),
      MICROSERVICE_NAME.STUDENT,
      PATH.POHADJANJE_PREDMETA,
      MICROSERVICE_PORT.STUDENT,
      WS_PATH.POHADJANJE_PREDMETA
    );
    this._findAll();
  }
}
