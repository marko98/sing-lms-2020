import { CrudService, CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';
import {
  PATH,
  WS_PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
} from '../../const';
import { Injectable } from '@angular/core';
import { VremeOdrzavanjaUNedelji } from '../../model/entity/vreme_odrzavanja_u_nedelji.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { VremeOdrzavanjaUNedeljiFactory } from '../../model/patterns/creational/factory_method/vreme_odrzavanja_u_nedelji.factory';

export const VREME_ODRZAVANJA_U_NEDELJI_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: VremeOdrzavanjaUNedeljiFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.NASTAVA,
  path: PATH.VREME_ODRZAVANJA_U_NEDELJI,
  microservicePort: MICROSERVICE_PORT.NASTAVA,
  wsPath: WS_PATH.VREME_ODRZAVANJA_U_NEDELJI,
};

@Injectable({ providedIn: 'root' })
export class VremeOdrzavanjaUNedeljiService extends CrudService<
  VremeOdrzavanjaUNedelji,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      VremeOdrzavanjaUNedeljiFactory.getInstance(),
      MICROSERVICE_NAME.NASTAVA,
      PATH.VREME_ODRZAVANJA_U_NEDELJI,
      MICROSERVICE_PORT.NASTAVA,
      WS_PATH.VREME_ODRZAVANJA_U_NEDELJI
    );

    this._findAll();
  }
}
