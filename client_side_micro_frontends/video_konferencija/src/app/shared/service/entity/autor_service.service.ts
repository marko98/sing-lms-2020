import { CrudService } from '../crud.service';
import {
  PATH,
  WS_PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
} from '../../const';
import { Injectable } from '@angular/core';
import { Autor } from '../../model/entity/autor.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { AutorFactory } from '../../model/patterns/creational/factory_method/autor.factory';
import { CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';

export const AUTOR_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: AutorFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.NASTAVA,
  path: PATH.AUTOR,
  microservicePort: MICROSERVICE_PORT.NASTAVA,
  wsPath: WS_PATH.AUTOR,
};
@Injectable({ providedIn: 'root' })
export class AutorService extends CrudService<Autor, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      AutorFactory.getInstance(),
      MICROSERVICE_NAME.NASTAVA,
      PATH.AUTOR,
      MICROSERVICE_PORT.NASTAVA,
      WS_PATH.AUTOR
    );
    this._findAll();
  }
}
