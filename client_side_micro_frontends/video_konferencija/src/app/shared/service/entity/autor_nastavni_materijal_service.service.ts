import { CrudService } from '../crud.service';
import {
  PATH,
  WS_PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
} from '../../const';
import { Injectable } from '@angular/core';
import { AutorNastavniMaterijal } from '../../model/entity/autor_nastavni_materijal.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { AutorNastavniMaterijalFactory } from '../../model/patterns/creational/factory_method/autor_nastavni_materijal.factory';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { AutorNastavniMaterijalDTO } from '../../model/dto/autor_nastavni_materijal.dto';
import { CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';

export const AUTOR_NASTAVNI_MATERIJAL_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: AutorNastavniMaterijalFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.NASTAVA,
  path: PATH.AUTOR_NASTAVNI_MATERIJAL,
  microservicePort: MICROSERVICE_PORT.NASTAVA,
  wsPath: WS_PATH.AUTOR_NASTAVNI_MATERIJAL,
};
@Injectable({ providedIn: 'root' })
export class AutorNastavniMaterijalService extends CrudService<
  AutorNastavniMaterijal,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      AutorNastavniMaterijalFactory.getInstance(),
      MICROSERVICE_NAME.NASTAVA,
      PATH.AUTOR_NASTAVNI_MATERIJAL,
      MICROSERVICE_PORT.NASTAVA,
      WS_PATH.AUTOR_NASTAVNI_MATERIJAL
    );
    this._findAll();
  }

  public findAllByNastavniMaterijalId = (
    id: number
  ): Observable<AutorNastavniMaterijal[]> => {
    // console.log(this._url + '/nastavni_materijal/' + id);
    return this._httpClient
      .get<AutorNastavniMaterijalDTO[]>(this._url + '/nastavni_materijal/' + id)
      .pipe(
        map((entiteti) =>
          entiteti.map((e) => <AutorNastavniMaterijal>this._factory.build(e))
        ),
        catchError(this._onHandleError)
      );
  };
}
