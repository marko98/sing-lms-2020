import { CrudService } from '../crud.service';
import {
  PATH,
  WS_PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
} from '../../const';
import { Injectable } from '@angular/core';
import { GodinaStudijaObavestenje } from '../../model/entity/godina_studija_obavestenje.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { GodinaStudijaObavestenjeFactory } from '../../model/patterns/creational/factory_method/godina_studija_obavestenje.factory';
import { CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';

export const GODINA_STUDIJA_OBAVESTENJE_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: GodinaStudijaObavestenjeFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.OBAVESTENJE,
  path: PATH.GODINA_STUDIJA_OBAVESTENJE,
  microservicePort: MICROSERVICE_PORT.OBAVESTENJE,
  wsPath: WS_PATH.GODINA_STUDIJA_OBAVESTENJE,
};
@Injectable({ providedIn: 'root' })
export class GodinaStudijaObavestenjeService extends CrudService<
  GodinaStudijaObavestenje,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      GodinaStudijaObavestenjeFactory.getInstance(),
      MICROSERVICE_NAME.OBAVESTENJE,
      PATH.GODINA_STUDIJA_OBAVESTENJE,
      MICROSERVICE_PORT.OBAVESTENJE,
      WS_PATH.GODINA_STUDIJA_OBAVESTENJE
    );
    this._findAll();
  }
}
