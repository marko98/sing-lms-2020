import { CrudService } from '../crud.service';
import {
  PATH,
  WS_PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
} from '../../const';
import { Injectable } from '@angular/core';
import { IstrazivackiRadStudentNaStudiji } from '../../model/entity/istrazivacki_rad_student_na_studiji.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { IstrazivackiRadStudentNaStudijiFactory } from '../../model/patterns/creational/factory_method/istrazivacki_rad_student_na_studiji.factory';
import { CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';

export const ISTRAZIVACKI_RAD_STUDENT_NA_STUDIJI_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: IstrazivackiRadStudentNaStudijiFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.STUDENT,
  path: PATH.ISTRAZIVACKI_RAD_STUDENT_NA_STUDIJI,
  microservicePort: MICROSERVICE_PORT.STUDENT,
  wsPath: WS_PATH.ISTRAZIVACKI_RAD_STUDENT_NA_STUDIJI,
};
@Injectable({ providedIn: 'root' })
export class IstrazivackiRadStudentNaStudijiService extends CrudService<
  IstrazivackiRadStudentNaStudiji,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      IstrazivackiRadStudentNaStudijiFactory.getInstance(),
      MICROSERVICE_NAME.STUDENT,
      PATH.ISTRAZIVACKI_RAD_STUDENT_NA_STUDIJI,
      MICROSERVICE_PORT.STUDENT,
      WS_PATH.ISTRAZIVACKI_RAD_STUDENT_NA_STUDIJI
    );
    this._findAll();
  }
}
