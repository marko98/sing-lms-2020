import { CrudService } from '../crud.service';
import {
  PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
  WS_PATH,
} from '../../const';
import { Injectable } from '@angular/core';
import { Titula } from '../../model/entity/titula.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { TitulaFactory } from '../../model/patterns/creational/factory_method/titula.factory';
import { CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';

export const TITULA_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: TitulaFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.NASTAVNIK,
  path: PATH.TITULA,
  microservicePort: MICROSERVICE_PORT.NASTAVNIK,
  wsPath: WS_PATH.TITULA,
};

@Injectable({ providedIn: 'root' })
export class TitulaService extends CrudService<Titula, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      TitulaFactory.getInstance(),
      MICROSERVICE_NAME.NASTAVNIK,
      PATH.TITULA,
      MICROSERVICE_PORT.NASTAVNIK,
      WS_PATH.TITULA
    );
    this._findAll();
  }
}
