import { CrudService, CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';
import {
  PATH,
  WS_PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
} from '../../const';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { KontaktFactory } from '../../model/patterns/creational/factory_method/kontakt.factory';
import { Kontakt } from '../../model/entity/kontakt.model';

export const KONTAKT_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: KontaktFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.FAKULTET,
  path: PATH.KONTAKT,
  microservicePort: MICROSERVICE_PORT.FAKULTET,
  wsPath: WS_PATH.KONTAKT,
};

@Injectable({ providedIn: 'root' })
export class KontaktService extends CrudService<Kontakt, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      KontaktFactory.getInstance(),
      MICROSERVICE_NAME.FAKULTET,
      PATH.KONTAKT,
      MICROSERVICE_PORT.FAKULTET,
      WS_PATH.KONTAKT
    );
    this._findAll();
  }
}
