import { CrudService, CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';
import {
  PATH,
  WS_PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
} from '../../const';
import { Injectable } from '@angular/core';
import { StudentNaStudiji } from '../../model/entity/student_na_studiji.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { StudentNaStudijiFactory } from '../../model/patterns/creational/factory_method/student_na_studiji.factory';
import { StudentNaStudijiDTO } from '../../model/dto/student_na_studiji.dto';
import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';

export const STUDENT_NA_STUDIJI_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: StudentNaStudijiFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.STUDENT,
  path: PATH.STUDENT_NA_STUDIJI,
  microservicePort: MICROSERVICE_PORT.STUDENT,
  wsPath: WS_PATH.STUDENT_NA_STUDIJI,
};

@Injectable({ providedIn: 'root' })
export class StudentNaStudijiService extends CrudService<
  StudentNaStudiji,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      StudentNaStudijiFactory.getInstance(),
      MICROSERVICE_NAME.STUDENT,
      PATH.STUDENT_NA_STUDIJI,
      MICROSERVICE_PORT.STUDENT,
      WS_PATH.STUDENT_NA_STUDIJI
    );
    this._findAll();
  }

  public findOneByPohadjanjePredmetaId = (
    id: number
  ): Observable<StudentNaStudiji> => {
    // console.log(this._url + '/pohadjanje_predmeta/' + id);
    return this._httpClient
      .get<StudentNaStudijiDTO>(this._url + '/pohadjanje_predmeta/' + id)
      .pipe(
        map((entitet) => <StudentNaStudiji>this._factory.build(entitet)),
        catchError(this._onHandleError)
      );
  };
}
