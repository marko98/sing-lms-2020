import { Injectable, EventEmitter } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { AuthService, AuthUserModelInterface } from './auth.service';
import { SCHEMA_WS, LOCALHOST } from '../const';
import { Sanduce } from 'src/app/sanducadi/sanducadi.component';
import { webSocket } from 'rxjs/webSocket';
import { UIService } from './ui.service';

// npm install -D @types/dom-mediacapture-record
declare var MediaRecorder: any;

export declare interface VideoMessage {
  fromUsername: string;
  data: string | Blob;
}

export declare interface IncomingCallData {
  videoMessage: VideoMessage;
  box: Sanduce;
}

const constraints: MediaStreamConstraints = {
  video: true,
  audio: { echoCancellation: true },
};

@Injectable({ providedIn: 'root' })
export class VideoService {
  public sanducici: BehaviorSubject<Sanduce[]> = new BehaviorSubject([]);
  public userBusy: BehaviorSubject<boolean> = new BehaviorSubject(false);
  public incomingCallData: BehaviorSubject<
    IncomingCallData
  > = new BehaviorSubject(undefined);
  public incomingCallEvent: EventEmitter<Sanduce> = new EventEmitter();
  public acceptIncomingCallData: BehaviorSubject<boolean> = new BehaviorSubject(
    false
  );
  public mediaStream: BehaviorSubject<MediaStream> = new BehaviorSubject(
    undefined
  );

  private _wsForBoxes: { [key: string]: any } = {};
  private _wsUrl: string = SCHEMA_WS + '://' + LOCALHOST + ':' + 8102 + '/';

  private _mediaRecorder: any;
  private _interval;
  private _ws;

  private _authUser: AuthUserModelInterface;

  private _timeoutForResponse: any;

  private _amIStartingCall: BehaviorSubject<boolean> = new BehaviorSubject(
    false
  );

  private _declinedBoxCalls: {
    sanduce: Sanduce;
    timeoutZaIzbacivanje: any;
  }[] = [];

  constructor(
    private _authService: AuthService,
    private _uiService: UIService
  ) {
    this._authService
      .getUser()
      .subscribe((authUser: AuthUserModelInterface) => {
        this._authUser = authUser;
        // console.log(this._authUser);

        this.sanducici.subscribe(this._onSanduciciNext);
        this._getSanducici();
      });

    // this._getUserMedia(constraints);

    this._amIStartingCall.subscribe((amIStartingCall) => {
      console.log('amIStartingCall: ', amIStartingCall);
    });
  }

  private _getUserMedia = (constraints: MediaStreamConstraints): void => {
    navigator.mediaDevices
      .getUserMedia(constraints)
      .then((stream: MediaStream) => {
        // console.log('Stream:: ', stream);
        this.mediaStream.next(stream);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  private _onDataAvailable = (e) => {
    let blob: Blob = e.data;
    // console.log(blob);
    // this._chunks.push(blob);

    // let chunkSize = 50000;
    // for (let start = 0; start < blob.size; start += chunkSize) {
    //   const chunk = blob.slice(start, start + chunkSize + 1);
    //   let fr = new FileReader();
    //   fr.readAsDataURL(chunk);
    //   fr.onloadend = () => {
    //     let base64Data: string = <string>fr.result;
    //     // console.log(base64Data);
    //     this._ws.next(base64Data);
    //   };
    // }

    let fr = new FileReader();
    fr.readAsDataURL(blob);
    fr.onloadend = () => {
      let base64Data: string = <string>fr.result;
      // console.log(base64Data);
      // console.log(typeof base64Data);
      // console.log('saljem');
      let videoMessage: VideoMessage = {
        fromUsername: this._authUser.korisnickoIme,
        data: base64Data,
      };
      this._ws.next(videoMessage);
      // atob(base64Data.split(',')[2]);
      // saveAs(this._base64ToBlob(base64Data.split(',')[2], blob.type), 'hi.mp4');
    };
  };

  private _onStartRecording = (ws): void => {
    if (this.mediaStream.getValue()) {
      if (ws && this._authUser) {
        this._ws = ws;

        this._mediaRecorder = new MediaRecorder(this.mediaStream.getValue(), {
          mimeType: 'video/webm;codecs="vp8,opus"',
        });
        // 'video/webm; codecs="opus,vp8"'

        console.log('recording started');

        this._mediaRecorder.ondataavailable = this._onDataAvailable;

        this._mediaRecorder.start();
        //   console.log(this._mediaRecorder.state);
        this._interval = setInterval(() => {
          this._mediaRecorder.requestData();
        }, 10);
      }
    } else {
      console.log('stream undefined');
    }
  };

  private _onStopRecording = (): void => {
    this._mediaRecorder = undefined;
    clearInterval(this._interval);
    // this._ws.complete();
  };

  private _base64ToBlob(b64Data, contentType = '', sliceSize = 512): Blob {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);
      const byteNumbers = new Array(slice.length);

      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  private _getSanducici = (): void => {
    //   dohvatimo sanducice na koje je pretplacen ulogovani korisnik
    // .... dobavljanje sanducica
    this.sanducici.next([{ id: 0, naziv: 'put za madjaro' }]);
  };

  private _onSanduciciNext = (sanducici: Sanduce[]) => {
    sanducici.forEach((sanduce) => {
      this._onSubscribeToMessageAndVideoBox(sanduce);
    });
  };

  // 1) pretplati se na sve sanducice na kojima si pretplacen ako imas dozvolu
  private _onSubscribeToMessageAndVideoBox = (sanduce: Sanduce): void => {
    // let wsForMessage = webSocket({
    //   url: this._wsUrl + boxName + '_message',
    //   deserializer: (msg) => msg,
    // });
    // wsForMessage.subscribe(
    //   (msg) => {
    //     console.log(msg);
    //   },
    //   (err) => {
    //     console.log(err);
    //   },
    //   () => {
    //     console.log(this._wsUrl + boxName + '_message' + ' - completed');
    //   }
    // );
    // this._wsForBoxes[boxName + '_message'] = wsForMessage;

    // samo ako vec ne postoji trazeni ws unutar _wsForBoxes
    if (!this._wsForBoxes[sanduce.id + '_video']) {
      let wsForVideo = webSocket({
        url: this._wsUrl + sanduce.id + '_video',
        deserializer: (msg) => msg,
      });
      this._wsForBoxes[sanduce.id + '_video'] = wsForVideo;
      wsForVideo.subscribe(
        (msg) => {
          //   if (!this._wsForBoxes[sanduce.id + '_video']) {
          //     // dobili smo neki odgovor, znaci veza je uspostavljena
          //     this._wsForBoxes[sanduce.id + '_video'] = wsForVideo;
          //   }
          try {
            let videoMessage: VideoMessage = JSON.parse(msg.data);

            // proveravamo da li je stigla poruka koja pripada nekom od sanduceta ubacenih u declinedBoxCalls
            let s: {
              sanduce: Sanduce;
              timeoutZaIzbacivanje?: any;
            } = this._declinedBoxCalls.find(
              (s: { sanduce: Sanduce; timeoutZaIzbacivanje?: any }) =>
                s.sanduce.id === sanduce.id
            );
            if (s) {
              if (s.timeoutZaIzbacivanje) clearTimeout(s.timeoutZaIzbacivanje);
              console.log(s.sanduce.id, 'timeout postavljen');
              s.timeoutZaIzbacivanje = setTimeout(() => {
                console.log(
                  s.sanduce.id,
                  'sanduce izbaceno iz declinedBoxCalls'
                );
                this._declinedBoxCalls = this._declinedBoxCalls.filter(
                  (s: { sanduce: Sanduce; timeoutZaIzbacivanje?: any }) =>
                    s.sanduce.id !== sanduce.id
                );
              }, 4000);
            }

            if (videoMessage.data !== 'data:') {
              // data ima sadrzaj
              if (this.userBusy.getValue()) {
                //  zauzet sam
                //  da li ja zovem ili ne(ili pricam)?
                if (this._amIStartingCall.getValue()) {
                  // dobijam odgovor na svoj poziv
                  if (this._timeoutForResponse) {
                    clearTimeout(this._timeoutForResponse);
                  }

                  videoMessage.data = this._base64ToBlob(
                    (<string>videoMessage.data).split(',')[2],
                    (<string>videoMessage.data).split(',')[0]
                  );
                  //   console.log({
                  //     videoMessage: videoMessage,
                  //     box: sanduce,
                  //   });
                  this.incomingCallData.next({
                    videoMessage: videoMessage,
                    box: sanduce,
                  });
                } else {
                  videoMessage.data = this._base64ToBlob(
                    (<string>videoMessage.data).split(',')[2],
                    (<string>videoMessage.data).split(',')[0]
                  );

                  //   console.log({
                  //     videoMessage: videoMessage,
                  //     box: sanduce,
                  //   });
                  this.incomingCallData.next({
                    videoMessage: videoMessage,
                    box: sanduce,
                  });
                }
              } else {
                //  nisam zauzet
                //   da li zelim da prihvatim poziv?
                if (
                  !this._declinedBoxCalls.find(
                    (s: { sanduce: Sanduce; timeoutZaIzbacivanje?: any }) =>
                      s.sanduce.id === sanduce.id
                  )
                ) {
                  // posalji event za dolazeci poziv da se prihvati ili odbije
                  if (!this.userBusy.getValue()) {
                    this.incomingCallEvent.emit(sanduce);
                  }
                }
              }
            }
          } catch (error) {
            // console.log(error);
          }
        },
        (err) => {
          console.log(err);
        },
        () => {
          // ws konekcija je zavrsena
          this._wsForBoxes[sanduce.id + '_video'] = undefined;

          console.log(this._wsUrl + sanduce.id + '_video' + ' - completed');
        }
      );
    }
  };

  public onCallStart = (sanduce: Sanduce): void => {
    if (!this.userBusy.getValue()) {
      let ws = this._wsForBoxes[sanduce.id + '_video'];
      if (ws) {
        // ws za sanduce postoji, uspesno smo se bili povezali na ws protokol

        if (this.mediaStream.getValue()) {
          // zapocni poziv
          this._amIStartingCall.next(true);

          this.acceptIncomingCallData.next(true);
          this.userBusy.next(true);

          // postavljamo timeout za cekanje na response
          this._timeoutForResponse = setTimeout(() => {
            this.onCallEnd(sanduce);
          }, 10000);
          console.log('postavljen timeout ', this._timeoutForResponse);

          console.log(
            'user busy: ' + this.userBusy.getValue(),
            ', sanduce: ',
            sanduce
          );
          this._onStartRecording(ws);
        } else {
          window.alert('stream undefined');
        }
      } else {
        window.alert('ws je zatvoren');
      }
    } else {
      window.alert('Vec ste u jednom razgovoru');
    }
  };

  public onCallEnd = (sanduce: Sanduce): void => {
    if (this.userBusy.getValue()) {
      // zavrsi poziv

      this._onStopRecording();
      this._declinedBoxCalls.push({
        sanduce: sanduce,
        timeoutZaIzbacivanje: setTimeout(() => {
          this._declinedBoxCalls = this._declinedBoxCalls.filter(
            (s: { sanduce: Sanduce; timeoutZaIzbacivanje?: any }) =>
              s.sanduce.id !== sanduce.id
          );
        }, 4000),
      });
      this.userBusy.next(false);
      this._amIStartingCall.next(false);
      this.acceptIncomingCallData.next(false);

      console.log(
        'user busy: ' + this.userBusy.getValue(),
        ', sanduce: ',
        sanduce ? sanduce : 'nije prosledjeno metodi'
      );
    } else {
      window.alert('Niste ni u jednom razgovoru');
    }
  };

  public onAcceptIncomingCall = (sanduce: Sanduce): void => {
    let ws = this._wsForBoxes[sanduce.id + '_video'];
    if (ws) {
      this.acceptIncomingCallData.next(true);
      this.userBusy.next(true);
      this._onStartRecording(ws);
    } else {
      window.alert('ws je zatvoren');
    }
  };

  public onDeclineIncomingCall = (sanduce: Sanduce): void => {
    console.log('onDeclineIncomingCall');
    this._declinedBoxCalls.push({
      sanduce: sanduce,
      timeoutZaIzbacivanje: setTimeout(() => {
        this._declinedBoxCalls = this._declinedBoxCalls.filter(
          (s: { sanduce: Sanduce; timeoutZaIzbacivanje?: any }) =>
            s.sanduce.id !== sanduce.id
        );
      }, 4000),
    });

    this.userBusy.next(false);
    this.acceptIncomingCallData.next(false);
  };
}
