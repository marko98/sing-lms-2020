import { Injectable } from '@angular/core';
import { SCHEMA_WS, LOCALHOST, PORT } from '../const';
import { AuthUserModelInterface, AuthService } from './auth.service';
import { UIService } from './ui.service';
import { BehaviorSubject } from 'rxjs';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';

const constraints: MediaStreamConstraints = {
  video: true,
  audio: { echoCancellation: true },
};

const constraints2: MediaStreamConstraints = {
  video: {
    frameRate: {
      ideal: 10,
      max: 15,
    },
    width: 1280,
    height: 720,
    facingMode: 'user',
  },
  audio: { echoCancellation: true },
};

@Injectable({ providedIn: 'root' })
export class PrijemnoSanduceService {
  public mediaStream: BehaviorSubject<MediaStream> = new BehaviorSubject(
    undefined
  );

  public authUser: AuthUserModelInterface;
  private _wsUrl =
    SCHEMA_WS + '://' + LOCALHOST + ':' + 8102 + '/' + 'poruka_video';
  public ws: WebSocketSubject<any>;

  constructor(
    private _authService: AuthService,
    private _uiService: UIService
  ) {
    this._authService
      .getUser()
      .subscribe((authUser: AuthUserModelInterface) => {
        this.authUser = authUser;
        // console.log(this._authUser);

        this._getUserMedia(constraints);
      });

    this.ws = webSocket({
      url: this._wsUrl,
      deserializer: (msg) => msg,
    });
  }

  private _getUserMedia = (constraints: MediaStreamConstraints): void => {
    navigator.mediaDevices
      .getUserMedia(constraints)
      .then((stream: MediaStream) => {
        // console.log('Stream:: ', stream);
        this.mediaStream.next(stream);
      })
      .catch((error) => {
        console.log(error);
      });
  };
}
