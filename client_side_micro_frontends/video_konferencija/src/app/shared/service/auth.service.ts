import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { SCHEMA_HTTP, LOCALHOST, PORT, ZUUL_PREFIX } from '../const';
import { catchError, tap } from 'rxjs/operators';
import { BehaviorSubject, throwError, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { UIService } from './ui.service';

export declare interface TokenModelInterface {
  token: string;
}

export declare interface AuthUserModelInterface {
  korisnickoIme: string;
  role: Set<string>;
  expirationDate: Date;
}

@Injectable({ providedIn: 'root' })
export class AuthService {
  private _url: string =
    SCHEMA_HTTP + '://' + LOCALHOST + ':' + PORT + '/' + ZUUL_PREFIX + '/';
  private _user: BehaviorSubject<AuthUserModelInterface> = new BehaviorSubject(
    undefined
  );
  private _token: string = '';
  private _tokenExpirationTimer: any;
  private _afterLoginRoute: string;
  private _afterLogoutRoute: string;

  constructor(
    private _httpClient: HttpClient,
    private _router: Router,
    private _uiService: UIService
  ) {}

  public getUser = (): BehaviorSubject<AuthUserModelInterface> => {
    return this._user;
  };

  public getToken = (): string => {
    return this._token;
  };

  public onLogin = (
    username: string,
    password: string,
    microserviceName: string,
    path: string
  ): Observable<TokenModelInterface> => {
    this._checkAfterLoginAndAfterLogoutRoute();

    return this._httpClient
      .post<TokenModelInterface>(this._url + microserviceName + '/' + path, {
        korisnickoIme: username,
        lozinka: password,
      })
      .pipe(
        catchError(this._onHandleError),
        tap(this._onHandleAuthentification)
      );
  };

  private _onHandleError = (resErr: HttpErrorResponse): Observable<never> => {
    let errorMessage = 'An unknown error happened.';

    if (!resErr.error || !resErr.error.error) {
      return throwError(errorMessage);
    }

    // moguci slucajevi
    switch (resErr.error.error.message) {
      case 'INVALID_PASSWORD':
        errorMessage = 'This password is not correct.';
        break;
    }

    this._uiService.showSnackbar(errorMessage, null, 1500);
    return throwError(errorMessage);
  };

  private _onHandleAuthentification = (
    tokenModelInterface: TokenModelInterface
  ): void => {
    this._token = tokenModelInterface.token;
    let decodedToken = JSON.parse(
      atob(tokenModelInterface.token.split('.')[1])
    );

    // console.log(decodedToken);

    const expirationTimeMs = +decodedToken.exp * 1000 - decodedToken.created;
    const expirationDate = new Date(new Date().getTime() + expirationTimeMs);
    this._user.next({
      korisnickoIme: decodedToken.sub,
      role: new Set(decodedToken.role.map((r) => r.authority)),
      expirationDate: expirationDate,
    });

    // console.log(this._user.value);

    this.onAutoLogout(expirationTimeMs);
    localStorage.setItem(
      'ulogovaniKorisnik',
      JSON.stringify({
        ...this._user.value,
        role: Array.from(this._user.value.role),
      })
    );
    localStorage.setItem('token', JSON.stringify(this._token));
    this._router.navigate([this._afterLoginRoute]);
  };

  public onAutoLogout = (expirationTimeMs: number): void => {
    this._checkAfterLoginAndAfterLogoutRoute();

    setTimeout(() => {
      this.onLogout();
    }, expirationTimeMs);
  };

  public onLogout = (): void => {
    this._checkAfterLoginAndAfterLogoutRoute();

    this._user.next(undefined);
    this._token = '';
    this._router.navigate([this._afterLogoutRoute]);
    localStorage.removeItem('ulogovaniKorisnik');
    localStorage.removeItem('token');
    if (this._tokenExpirationTimer) {
      clearTimeout(this._tokenExpirationTimer);
    }
    this._tokenExpirationTimer = undefined;
  };

  public onAutoLogin = (): void => {
    this._checkAfterLoginAndAfterLogoutRoute();

    const user: AuthUserModelInterface = JSON.parse(
      localStorage.getItem('ulogovaniKorisnik')
    );

    if (!user) return;

    user.expirationDate = new Date(user.expirationDate);
    user.role = new Set(user.role);

    const token = JSON.parse(localStorage.getItem('token'));
    this._token = token;

    this._user.next(user);
    const expirationTimeMs =
      user.expirationDate.getTime() - new Date().getTime();
    this.onAutoLogout(expirationTimeMs);
    // this._router.navigate([this._afterLoginRoute]);
  };

  public getAfterLoginRoute = (): string => {
    return this._afterLoginRoute;
  };

  public setAfterLoginRoute = (afterLoginRoute: string): void => {
    this._afterLoginRoute = afterLoginRoute;
  };

  public getAfterLogoutRoute = (): string => {
    return this._afterLogoutRoute;
  };

  public setAfterLogoutRoute = (afterLogoutRoute: string): void => {
    this._afterLogoutRoute = afterLogoutRoute;
  };

  private _checkAfterLoginAndAfterLogoutRoute = (): void => {
    if (!this._afterLogoutRoute || !this._afterLoginRoute)
      throw new Error('Both AfterLoginRoute and AfterLogoutRoute must be set');
  };
}
