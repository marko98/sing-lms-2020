import { Injectable } from '@angular/core';
import * as html2pdf from 'html2pdf.js';

@Injectable({ providedIn: 'root' })
export class PdfService {
  private _options = {
    margin: 1,
    filename: 'file' + '.pdf',
    image: { type: 'jpg' }, //pdf, webp
    html2canvas: { scale: 2 },
    jsPDF: { orientation: 'landscape', format: 'letter' },
  };

  public exportToPDF = (fileName: string, htmlElement: Element): void => {
    this._options.filename = fileName + '.pdf';

    html2pdf().from(htmlElement).set(this._options).save();
  };
}
