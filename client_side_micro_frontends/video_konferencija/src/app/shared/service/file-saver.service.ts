import { Injectable } from '@angular/core';
import { saveAs } from 'file-saver';

/**
 * npm i file-saver
 * npm i @types/file-saver
 */

@Injectable({ providedIn: 'root' })
export class FileSaverService {
  public onSaveFile = (
    data: string,
    fileType: string,
    fileName: string
  ): void => {
    let blob = new Blob([data], { type: fileType });
    saveAs(blob, fileName);
  };
}
