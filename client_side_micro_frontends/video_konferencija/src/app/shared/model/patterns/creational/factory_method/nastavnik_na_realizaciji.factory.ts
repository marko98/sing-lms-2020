import { NastavnikNaRealizaciji } from '../../../entity/nastavnik_na_realizaciji.model';
import { NastavnikNaRealizacijiDTO } from '../../../dto/nastavnik_na_realizaciji.dto';
import { RealizacijaPredmetaFactory } from './realizacija_predmeta.factory';
import { NastavnikNaRealizacijiTipNastaveFactory } from './nastavnik_na_realizaciji_tip_nastave.factory';
import { Factory } from './factory.model';
import { NastavnikFactory } from './nastavnik.factory';

export class NastavnikNaRealizacijiFactory extends Factory {
  public static _instance: NastavnikNaRealizacijiFactory;
  public static getInstance = (): NastavnikNaRealizacijiFactory => {
    if (!NastavnikNaRealizacijiFactory._instance)
      NastavnikNaRealizacijiFactory._instance = new NastavnikNaRealizacijiFactory();
    return <NastavnikNaRealizacijiFactory>(
      NastavnikNaRealizacijiFactory._instance
    );
  };

  private constructor() {
    super();
  }
  build(dto: NastavnikNaRealizacijiDTO): NastavnikNaRealizaciji {
    let nastavnikNaRealizaciji = new NastavnikNaRealizaciji();
    nastavnikNaRealizaciji.setId(dto.id);
    nastavnikNaRealizaciji.setBrojCasova(dto.brojCasova);
    nastavnikNaRealizaciji.setStanje(dto.stanje);
    nastavnikNaRealizaciji.setVersion(dto.version);
    nastavnikNaRealizaciji.setZvanje(dto.zvanje);

    if (dto.realizacijaPredmetaDTO)
      nastavnikNaRealizaciji.setRealizacijaPredmeta(
        RealizacijaPredmetaFactory.getInstance().build(
          dto.realizacijaPredmetaDTO
        )
      );

    if (dto.nastavnikDTO)
      nastavnikNaRealizaciji.setNastavnik(
        NastavnikFactory.getInstance().build(dto.nastavnikDTO)
      );

    if (dto.nastavnikNaRealizacijiTipoviNastaveDTO)
      nastavnikNaRealizaciji.setNastavnikNaRealizacijiTipoviNastave(
        dto.nastavnikNaRealizacijiTipoviNastaveDTO.map((t) =>
          NastavnikNaRealizacijiTipNastaveFactory.getInstance().build(t)
        )
      );

    return nastavnikNaRealizaciji;
  }
}
