import { RealizacijaPredmeta } from '../../../entity/realizacija_predmeta.model';
import { RealizacijaPredmetaDTO } from '../../../dto/realizacija_predmeta.dto';
import { PredmetFactory } from './predmet.factory';
import { DrugiOblikNastaveFactory } from './drugi_oblik_nastave.factory';
import { EvaluacijaZnanjaFactory } from './evaluacija_znanja.factory';
import { IshodFactory } from './ishod.factory';
import { IstrazivackiRadFactory } from './istrazivacki_rad.factory';
import { NastavnikNaRealizacijiFactory } from './nastavnik_na_realizaciji.factory';
import { ObrazovniCiljFactory } from './obrazovni_cilj.factory';
import { PohadjanjePredmetaFactory } from './pohadjanje_predmeta.factory';
import { TerminNastaveFactory } from './termin_nastave.factory';
import { Factory } from './factory.model';

export class RealizacijaPredmetaFactory extends Factory {
  public static _instance: RealizacijaPredmetaFactory;
  public static getInstance = (): RealizacijaPredmetaFactory => {
    if (!RealizacijaPredmetaFactory._instance)
      RealizacijaPredmetaFactory._instance = new RealizacijaPredmetaFactory();
    return <RealizacijaPredmetaFactory>RealizacijaPredmetaFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: RealizacijaPredmetaDTO): RealizacijaPredmeta {
    let realizacijaPredmeta = new RealizacijaPredmeta();

    realizacijaPredmeta.setId(dto.id);
    realizacijaPredmeta.setStanje(dto.stanje);
    realizacijaPredmeta.setVersion(dto.version);

    if (dto.predmetDTO)
      realizacijaPredmeta.setPredmet(
        PredmetFactory.getInstance().build(dto.predmetDTO)
      );

    if (dto.drugiObliciNastaveDTO)
      realizacijaPredmeta.setDrugiObliciNastave(
        dto.drugiObliciNastaveDTO.map((t) =>
          DrugiOblikNastaveFactory.getInstance().build(t)
        )
      );

    if (dto.evaluacijeZnanjaDTO)
      realizacijaPredmeta.setEvaluacijeZnanja(
        dto.evaluacijeZnanjaDTO.map((t) =>
          EvaluacijaZnanjaFactory.getInstance().build(t)
        )
      );

    if (dto.ishodiDTO)
      realizacijaPredmeta.setIshodi(
        dto.ishodiDTO.map((t) => IshodFactory.getInstance().build(t))
      );

    if (dto.istrazivackiRadoviDTO)
      realizacijaPredmeta.setIstrazivackiRadovi(
        dto.istrazivackiRadoviDTO.map((t) =>
          IstrazivackiRadFactory.getInstance().build(t)
        )
      );

    if (dto.nastavniciNaRealizacijiDTO)
      realizacijaPredmeta.setNastavniciNaRealizaciji(
        dto.nastavniciNaRealizacijiDTO.map((t) =>
          NastavnikNaRealizacijiFactory.getInstance().build(t)
        )
      );

    if (dto.obrazovniCiljeviDTO)
      realizacijaPredmeta.setObrazovniCiljevi(
        dto.obrazovniCiljeviDTO.map((t) =>
          ObrazovniCiljFactory.getInstance().build(t)
        )
      );

    if (dto.pohadjanjaPredmetaDTO)
      realizacijaPredmeta.setPohadjanjaPredmeta(
        dto.pohadjanjaPredmetaDTO.map((t) =>
          PohadjanjePredmetaFactory.getInstance().build(t)
        )
      );

    if (dto.terminiNastaveDTO)
      realizacijaPredmeta.setTerminiNastave(
        dto.terminiNastaveDTO.map((t) =>
          TerminNastaveFactory.getInstance().build(t)
        )
      );

    return realizacijaPredmeta;
  }
}
