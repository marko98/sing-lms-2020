import { StudentskiSluzbenik } from '../../../entity/studentski_sluzbenik.model';
import { StudentskiSluzbenikDTO } from '../../../dto/studentski_sluzbenik.dto';
import { RegistrovaniKorisnikFactory } from './registrovani_korisnik.factory';
import { StudentskaSluzbaFactory } from './studentska_sluzba.factory';
import { Factory } from './factory.model';

export class StudentskiSluzbenikFactory extends Factory {
  public static _instance: StudentskiSluzbenikFactory;
  public static getInstance = (): StudentskiSluzbenikFactory => {
    if (!StudentskiSluzbenikFactory._instance)
      StudentskiSluzbenikFactory._instance = new StudentskiSluzbenikFactory();
    return <StudentskiSluzbenikFactory>StudentskiSluzbenikFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: StudentskiSluzbenikDTO): StudentskiSluzbenik {
    let studentskiSluzbenik = new StudentskiSluzbenik();
    studentskiSluzbenik.setId(dto.id);
    studentskiSluzbenik.setStanje(dto.stanje);
    studentskiSluzbenik.setVersion(dto.version);

    if (dto.registrovaniKorisnikDTO)
      studentskiSluzbenik.setRegistrovaniKorisnik(
        RegistrovaniKorisnikFactory.getInstance().build(
          dto.registrovaniKorisnikDTO
        )
      );
    if (dto.studentskaSluzbaDTO)
      studentskiSluzbenik.setStudentskaSluzba(
        StudentskaSluzbaFactory.getInstance().build(dto.studentskaSluzbaDTO)
      );

    return studentskiSluzbenik;
  }
}
