import { Silabus } from '../../../entity/silabus.model';
import { SilabusDTO } from '../../../dto/silabus.dto';
import { PredmetFactory } from './predmet.factory';
import { Factory } from './factory.model';

export class SilabusFactory extends Factory {
  public static _instance: SilabusFactory;
  public static getInstance = (): SilabusFactory => {
    if (!SilabusFactory._instance)
      SilabusFactory._instance = new SilabusFactory();
    return <SilabusFactory>SilabusFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: SilabusDTO): Silabus {
    let silabus = new Silabus();

    silabus.setId(dto.id);
    silabus.setNaslov(dto.naslov);
    silabus.setOpis(dto.opis);
    silabus.setStanje(dto.stanje);
    silabus.setVersion(dto.version);

    if (dto.predmetDTO)
      silabus.setPredmet(PredmetFactory.getInstance().build(dto.predmetDTO));

    return silabus;
  }
}
