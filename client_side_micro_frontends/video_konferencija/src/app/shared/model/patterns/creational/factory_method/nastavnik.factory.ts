import { Nastavnik } from '../../../entity/nastavnik.model';
import { NastavnikDTO } from '../../../dto/nastavnik.dto';
import { RegistrovaniKorisnikFactory } from './registrovani_korisnik.factory';
import { DiplomskiRadFactory } from './diplomski_rad.factory';
import { FakultetFactory } from './fakultet.factory';
import { IstrazivackiRadFactory } from './istrazivacki_rad.factory';
import { NastavnikDiplomskiRadFactory } from './nastavnik_diplomski_rad.factory';
import { KonsultacijaFactory } from './konsultacija.factory';
import { NastavnikEvaluacijaZnanjaFactory } from './nastavnik_evaluacija_znanja.factory';
import { NastavnikFakultetFactory } from './nastavnik_fakultet.factory';
import { StudijskiProgramFactory } from './studijski_program.factory';
import { TerminNastaveFactory } from './termin_nastave.factory';
import { TitulaFactory } from './titula.factory';
import { UniverzitetFactory } from './univerzitet.factory';
import { Factory } from './factory.model';
import { NastavnikNaRealizacijiFactory } from './nastavnik_na_realizaciji.factory';

export class NastavnikFactory extends Factory {
  public static _instance: NastavnikFactory;

  public static getInstance = (): NastavnikFactory => {
    if (!NastavnikFactory._instance)
      NastavnikFactory._instance = new NastavnikFactory();
    return <NastavnikFactory>NastavnikFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: NastavnikDTO): Nastavnik {
    let nastavnik = new Nastavnik();
    nastavnik.setId(dto.id);
    nastavnik.setBiografija(dto.biografija);
    nastavnik.setStanje(dto.stanje);
    nastavnik.setVersion(dto.version);

    if (dto.registrovaniKorisnikDTO)
      nastavnik.setRegistrovaniKorisnik(
        RegistrovaniKorisnikFactory.getInstance().build(
          dto.registrovaniKorisnikDTO
        )
      );

    if (dto.nastavnikDiplomskiRadoviDTO)
      nastavnik.setNastavnikDiplomskiRadovi(
        dto.nastavnikDiplomskiRadoviDTO.map((t) =>
          NastavnikDiplomskiRadFactory.getInstance().build(t)
        )
      );

    if (dto.mentorDiplomskiRadoviDTO)
      nastavnik.setMentorDiplomskiRadovi(
        dto.mentorDiplomskiRadoviDTO.map((t) =>
          DiplomskiRadFactory.getInstance().build(t)
        )
      );

    if (dto.fakultetiDTO)
      nastavnik.setFakulteti(
        dto.fakultetiDTO.map((t) => FakultetFactory.getInstance().build(t))
      );

    if (dto.istrazivackiRadoviDTO)
      nastavnik.setIstrazivackiRadovi(
        dto.istrazivackiRadoviDTO.map((t) =>
          IstrazivackiRadFactory.getInstance().build(t)
        )
      );

    if (dto.konsultacijeDTO)
      nastavnik.setKonsultacije(
        dto.konsultacijeDTO.map((t) =>
          KonsultacijaFactory.getInstance().build(t)
        )
      );

    if (dto.nastavnikEvaluacijeZnanjaDTO)
      nastavnik.setNastavnikEvaluacijeZnanja(
        dto.nastavnikEvaluacijeZnanjaDTO.map((t) =>
          NastavnikEvaluacijaZnanjaFactory.getInstance().build(t)
        )
      );

    if (dto.nastavnikFakultetiDTO)
      nastavnik.setNastavnikFakulteti(
        dto.nastavnikFakultetiDTO.map((t) =>
          NastavnikFakultetFactory.getInstance().build(t)
        )
      );

    if (dto.studijskiProgramDTO)
      nastavnik.setStudijskiProgrami(
        dto.studijskiProgramDTO.map((t) =>
          StudijskiProgramFactory.getInstance().build(t)
        )
      );

    if (dto.terminiNastaveDTO)
      nastavnik.setTerminiNastave(
        dto.terminiNastaveDTO.map((t) =>
          TerminNastaveFactory.getInstance().build(t)
        )
      );

    if (dto.tituleDTO)
      nastavnik.setTitule(
        dto.tituleDTO.map((t) => TitulaFactory.getInstance().build(t))
      );

    if (dto.univerzitetiDTO)
      nastavnik.setUniverziteti(
        dto.univerzitetiDTO.map((t) =>
          UniverzitetFactory.getInstance().build(t)
        )
      );

    if (dto.nastavnikNaRealizacijamaDTO)
      nastavnik.setNastavnikNaRealizacijama(
        dto.nastavnikNaRealizacijamaDTO.map((t) =>
          NastavnikNaRealizacijiFactory.getInstance().build(t)
        )
      );

    return nastavnik;
  }
}
