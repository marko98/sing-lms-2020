import { NaucnaOblast } from '../../../entity/naucna_oblast.model';
import { NaucnaOblastDTO } from '../../../dto/naucna_oblast.dto';
import { TitulaFactory } from './titula.factory';
import { Factory } from './factory.model';

export class NaucnaOblastFactory extends Factory {
  public static _instance: NaucnaOblastFactory;
  public static getInstance = (): NaucnaOblastFactory => {
    if (!NaucnaOblastFactory._instance)
      NaucnaOblastFactory._instance = new NaucnaOblastFactory();
    return <NaucnaOblastFactory>NaucnaOblastFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: NaucnaOblastDTO): NaucnaOblast {
    let naucnaOblast = new NaucnaOblast();
    naucnaOblast.setId(dto.id);
    naucnaOblast.setNaziv(dto.naziv);
    naucnaOblast.setStanje(dto.stanje);
    naucnaOblast.setVersion(dto.version);

    if (dto.titule)
      naucnaOblast.setTitule(
        dto.titule.map((t) => TitulaFactory.getInstance().build(t))
      );

    return naucnaOblast;
  }
}
