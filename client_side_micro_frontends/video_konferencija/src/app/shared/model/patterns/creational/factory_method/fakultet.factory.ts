import { Fakultet } from '../../../entity/fakultet.model';
import { FakultetDTO } from '../../../dto/fakultet.dto';
import { NastavnikFactory } from './nastavnik.factory';
import { UniverzitetFactory } from './univerzitet.factory';
import { NastavnikFakultetFactory } from './nastavnik_fakultet.factory';
import { OdeljenjeFactory } from './odeljenje.factory';
import { StudijskiProgramFactory } from './studijski_program.factory';
import { Factory } from './factory.model';
import { KontaktFactory } from './kontakt.factory';

export class FakultetFactory extends Factory {
  public static _instance: FakultetFactory;
  public static getInstance = (): FakultetFactory => {
    if (!FakultetFactory._instance)
      FakultetFactory._instance = new FakultetFactory();
    return <FakultetFactory>FakultetFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: FakultetDTO): Fakultet {
    let fakultet = new Fakultet();
    fakultet.setId(dto.id);
    fakultet.setNaziv(dto.naziv);
    fakultet.setStanje(dto.stanje);
    fakultet.setVersion(dto.version);
    fakultet.setOpis(dto.opis);

    if (dto.dekanDTO)
      fakultet.setDekan(NastavnikFactory.getInstance().build(dto.dekanDTO));
    if (dto.univerzitetDTO)
      fakultet.setUniverzitet(
        UniverzitetFactory.getInstance().build(dto.univerzitetDTO)
      );

    if (dto.nastavniciFakultetDTO)
      fakultet.setNastavniciFakultet(
        dto.nastavniciFakultetDTO.map((t) =>
          NastavnikFakultetFactory.getInstance().build(t)
        )
      );

    if (dto.odeljenjaDTO)
      fakultet.setOdeljenja(
        dto.odeljenjaDTO.map((t) => OdeljenjeFactory.getInstance().build(t))
      );

    if (dto.studijskiProgramiDTO)
      fakultet.setStudijskiProgrami(
        dto.studijskiProgramiDTO.map((t) =>
          StudijskiProgramFactory.getInstance().build(t)
        )
      );

    if (dto.kontaktiDTO)
      fakultet.setKontakti(
        dto.kontaktiDTO.map((t) => KontaktFactory.getInstance().build(t))
      );

    return fakultet;
  }
}
