import { Univerzitet } from '../../../entity/univerzitet.model';
import { UniverzitetDTO } from '../../../dto/univerzitet.dto';
import { NastavnikFactory } from './nastavnik.factory';
import { StudentskaSluzbaFactory } from './studentska_sluzba.factory';
import { AdresaFactory } from './adresa.factory';
import { FakultetFactory } from './fakultet.factory';
import { KalendarFactory } from './kalendar.factory';
import { Factory } from './factory.model';
import { KontaktFactory } from './kontakt.factory';

export class UniverzitetFactory extends Factory {
  public static _instance: UniverzitetFactory;

  public static getInstance = (): UniverzitetFactory => {
    if (!UniverzitetFactory._instance)
      UniverzitetFactory._instance = new UniverzitetFactory();
    return <UniverzitetFactory>UniverzitetFactory._instance;
  };

  private constructor() {
    super();
  }

  public build = (dto: UniverzitetDTO): Univerzitet => {
    let univerzitet = new Univerzitet();
    univerzitet.setId(dto.id);
    univerzitet.setDatumOsnivanja(new Date(dto.datumOsnivanja));
    univerzitet.setNaziv(dto.naziv);
    univerzitet.setStanje(dto.stanje);
    univerzitet.setVersion(dto.version);
    univerzitet.setOpis(dto.opis);

    if (dto.rektorDTO)
      univerzitet.setRektor(
        NastavnikFactory.getInstance().build(dto.rektorDTO)
      );

    if (dto.studentskaSluzbaDTO)
      univerzitet.setStudentskaSluzba(
        StudentskaSluzbaFactory.getInstance().build(dto.studentskaSluzbaDTO)
      );

    if (dto.adreseDTO)
      univerzitet.setAdrese(
        dto.adreseDTO.map((a) => AdresaFactory.getInstance().build(a))
      );

    if (dto.fakultetiDTO)
      univerzitet.setFakulteti(
        dto.fakultetiDTO.map((f) => FakultetFactory.getInstance().build(f))
      );

    if (dto.kalendariDTO)
      univerzitet.setKalendari(
        dto.kalendariDTO.map((k) => KalendarFactory.getInstance().build(k))
      );

    if (dto.kontaktiDTO)
      univerzitet.setKontakti(
        dto.kontaktiDTO.map((t) => KontaktFactory.getInstance().build(t))
      );

    return univerzitet;
  };
}
