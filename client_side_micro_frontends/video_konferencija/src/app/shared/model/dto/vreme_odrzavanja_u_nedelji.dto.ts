import { EntitetDTO } from './entitet.dto';
import { KonsultacijaDTO } from './konsultacija.dto';
import { Dan } from '../enum/dan.enum';

export declare interface VremeOdrzavanjaUNedeljiDTO extends EntitetDTO {
  vreme: string;
  konsultacijaDTO: KonsultacijaDTO;
  dan: Dan;
}
