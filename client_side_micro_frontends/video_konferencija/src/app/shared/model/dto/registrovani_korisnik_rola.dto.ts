import { EntitetDTO } from './entitet.dto';
import { RegistrovaniKorisnikDTO } from './registrovani_korisnik.dto';
import { RolaDTO } from './rola.dto';

export declare interface RegistrovaniKorisnikRolaDTO extends EntitetDTO {
  registrovaniKorisnikDTO: RegistrovaniKorisnikDTO;
  rolaDTO: RolaDTO;
}
