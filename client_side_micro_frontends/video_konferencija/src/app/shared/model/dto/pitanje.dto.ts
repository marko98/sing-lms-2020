import { EntitetDTO } from './entitet.dto';
import { EvaluacijaZnanjaDTO } from './evaluacija_znanja.dto';
import { OdgovorDTO } from './odgovor.dto';

export declare interface PitanjeDTO extends EntitetDTO {
  oblast: string;
  pitanje: string;
  putanjaZaSliku: string;
  evaluacijaZnanjaDTO: EvaluacijaZnanjaDTO;
  odgovoriDTO: OdgovorDTO[];
}
