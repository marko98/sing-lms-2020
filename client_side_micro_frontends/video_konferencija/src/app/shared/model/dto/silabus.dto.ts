import { EntitetDTO } from './entitet.dto';
import { PredmetDTO } from './predmet.dto';

export declare interface SilabusDTO extends EntitetDTO {
  naslov: string;
  opis: string;
  predmetDTO: PredmetDTO;
}
