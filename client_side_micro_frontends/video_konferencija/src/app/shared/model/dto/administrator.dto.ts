import { EntitetDTO } from './entitet.dto';
import { RegistrovaniKorisnikDTO } from './registrovani_korisnik.dto';
export declare interface AdministratorDTO extends EntitetDTO {
  registrovaniKorisnikDTO: RegistrovaniKorisnikDTO;
}
