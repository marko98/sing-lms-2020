import { EntitetDTO } from './entitet.dto';
import { TipNastave } from '../enum/tip_nastave.enum';
import { PredmetDTO } from './predmet.dto';

export declare interface PredmetTipNastaveDTO extends EntitetDTO {
  tipNastave: TipNastave;
  predmetDTO: PredmetDTO;
}
