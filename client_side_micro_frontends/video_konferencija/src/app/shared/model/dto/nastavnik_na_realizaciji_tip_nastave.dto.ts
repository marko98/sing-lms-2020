import { EntitetDTO } from './entitet.dto';
import { TipNastave } from '../enum/tip_nastave.enum';
import { NastavnikNaRealizacijiDTO } from './nastavnik_na_realizaciji.dto';

export declare interface NastavnikNaRealizacijiTipNastaveDTO
  extends EntitetDTO {
  tipNastave: TipNastave;
  nastavnikNaRealizacijiDTO: NastavnikNaRealizacijiDTO;
}
