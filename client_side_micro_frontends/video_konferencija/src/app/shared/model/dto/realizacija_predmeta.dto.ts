import { EntitetDTO } from './entitet.dto';
import { DrugiOblikNastaveDTO } from './drugi_oblik_nastave.dto';
import { IshodDTO } from './ishod.dto';
import { PohadjanjePredmetaDTO } from './pohadjanje_predmeta.dto';
import { ObrazovniCiljDTO } from './obrazovni_cilj.dto';
import { NastavnikNaRealizacijiDTO } from './nastavnik_na_realizaciji.dto';
import { IstrazivackiRadDTO } from './istrazivacki_rad.dto';
import { EvaluacijaZnanjaDTO } from './evaluacija_znanja.dto';
import { TerminNastaveDTO } from './termin_nastave.dto';
import { PredmetDTO } from './predmet.dto';

export declare interface RealizacijaPredmetaDTO extends EntitetDTO {
  drugiObliciNastaveDTO: DrugiOblikNastaveDTO[];
  ishodiDTO: IshodDTO[];
  pohadjanjaPredmetaDTO: PohadjanjePredmetaDTO[];
  obrazovniCiljeviDTO: ObrazovniCiljDTO[];
  nastavniciNaRealizacijiDTO: NastavnikNaRealizacijiDTO[];
  istrazivackiRadoviDTO: IstrazivackiRadDTO[];
  evaluacijeZnanjaDTO: EvaluacijaZnanjaDTO[];
  terminiNastaveDTO: TerminNastaveDTO[];
  predmetDTO: PredmetDTO;
}
