import { EntitetDTO } from './entitet.dto';
import { EvaluacijaZnanjaDTO } from './evaluacija_znanja.dto';
import { NacinEvaluacije } from '../enum/nacin_evaluacije.enum';

export declare interface EvaluacijaZnanjaNacinEvaluacijeDTO extends EntitetDTO {
  nacinEvaluacije: NacinEvaluacije;
  evaluacijaZnanjaDTO: EvaluacijaZnanjaDTO;
}
