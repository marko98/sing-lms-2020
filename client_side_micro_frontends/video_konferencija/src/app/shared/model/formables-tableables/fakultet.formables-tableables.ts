import { StanjeModela } from '../enum/stanje_modela.enum';
import { CrudServiceForModel } from '../../service/crud.service-for-model';
import { Subject } from 'rxjs';
import {
  Form,
  FormRow,
  ROW_ITEM_TYPE,
  FormFieldInputNumberInterface,
  FORM_FIELD_INPUT_TYPE,
  SelectInterface,
  VALIDATOR_NAMES,
  TextareaInterface,
  FormFieldInputTextInterface,
} from 'my-angular-form';
import { Validators } from '@angular/forms';
import { HeaderValue, Table } from 'my-angular-table';
import { Formable } from '../interface/formable.interface';
import { Tableable } from '../interface/tableable.interface';
import { Fakultet } from '../entity/fakultet.model';
import { FAKULTET_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/fakultet_service.service';
import { take } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { Univerzitet } from '../entity/univerzitet.model';
import { UNIVERZITET_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/univerzitet_service.service';
import { Nastavnik } from '../entity/nastavnik.model';
import { NASTAVNIK_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/nastavnik_service.service';

export class FakultetFormableTableable implements Formable, Tableable {
  private _formSubject: Subject<Form> = new Subject();

  public getFormSubject = (): Subject<Form> => {
    return this._formSubject;
  };

  constructor(private _entitet: Fakultet) {}

  public getForm = (
    csfm: CrudServiceForModel,
    isFormForCreation: boolean,
    isFormForDetails: boolean = false,
    isFormForUpdate: boolean = false,
    disableAllFields: boolean = false
  ): void => {
    // private id: number;+

    // private naziv: string;+

    // private opis: string;+

    // private stanje: StanjeModela;+

    // private version: number;+

    // private odeljenja: Odeljenje[];+

    // private univerzitet: Univerzitet;+

    // private dekan: Nastavnik;+

    // private nastavniciFakultet: NastavnikFakultet[];+

    // private studijskiProgrami: StudijskiProgram[];+

    // private kontakti: Kontakt[];+

    let form = new Form({
      showSubmitButton: !disableAllFields,
      submitButtonText: 'potvrdi',
      showCancelButton: !disableAllFields,
      cancelButtonText: 'odustani',
      showResetButton: isFormForCreation,
      resetButtonText: 'resetuj',
      submitOnlyIfFormValid: true,
    });

    if (!isFormForCreation) {
      let rowId = new FormRow();
      form.addChild(rowId);
      rowId.addChildInterface(<FormFieldInputNumberInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
        formFieldInputType: FORM_FIELD_INPUT_TYPE.NUMBER,
        controlName: 'id',
        labelName: 'Id',
        defaultValue: this._entitet.getId(),
        disabled: true,
      });

      // console.log(StanjeModela.AKTIVAN);
      // console.log(typeof StanjeModela.AKTIVAN);

      let rowStanjeModela = new FormRow();
      form.addChild(rowStanjeModela);
      rowStanjeModela.addChildInterface(<SelectInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
        controlName: 'stanje',
        labelName: 'Stanje modela',
        defaultValue: this._entitet.getStanje(),
        optionValues: [
          { value: StanjeModela.AKTIVAN, textToShow: 'AKTIVAN' },
          { value: StanjeModela.NEAKTIVAN, textToShow: 'NEAKTIVAN' },
        ],
        disabled: disableAllFields,
        validators: [
          {
            message: 'Stanje modela je obavezno',
            name: VALIDATOR_NAMES.REQUIRED,
            validatorFn: Validators.required,
          },
        ],
      });
    }

    let rowNaziv = new FormRow();
    form.addChild(rowNaziv);
    rowNaziv.addChildInterface(<FormFieldInputTextInterface>{
      type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
      formFieldInputType: FORM_FIELD_INPUT_TYPE.TEXT,
      controlName: 'naziv',
      labelName: 'Naziv fakulteta',
      defaultValue: this._entitet.getNaziv(),
      appearance: 'outline',
      placeholder: 'unesite naziv fakulteta',
      disabled: disableAllFields,
      validators: [
        {
          message: 'naziv fakulteta je obavezan',
          name: VALIDATOR_NAMES.REQUIRED,
          validatorFn: Validators.required,
        },
      ],
    });

    let rowOpis = new FormRow();
    form.addChild(rowOpis);
    rowOpis.addChildInterface(<TextareaInterface>{
      type: ROW_ITEM_TYPE.FORM_FIELD_TEXTAREA,
      controlName: 'opis',
      labelName: 'Opis fakulteta',
      defaultValue: this._entitet.getOpis(),
      validators: [
        {
          message: 'opis mora imati vise od 9 karaktera',
          name: VALIDATOR_NAMES.MIN_LENGTH,
          validatorFn: Validators.minLength(10),
          length: 10,
        },
      ],
      showHintAboutMinMaxLength: true,
      requiredOption: {
        required: true,
        hideRequiredMarker: true,
      },
      disabled: disableAllFields,
    });

    let univerzitetService = csfm.getCrudServiceForModel<Univerzitet, number>(
      UNIVERZITET_SERVICE_FOR_MODEL_INTERFACE
    );

    univerzitetService
      .findAll()
      .pipe(take(1))
      .subscribe(
        (entiteti: Univerzitet[]) => {
          for (let index = 0; index < entiteti.length; index++) {
            if (
              entiteti[index].getId().toString() ===
              this._entitet.getUniverzitet()?.getId().toString()
            ) {
              this._entitet.setUniverzitet(entiteti[index]);
              break;
            }
          }

          if (isFormForCreation) {
            // entiteti = entiteti.filter((r) => !r.getUniverzitet());
          } else if (isFormForUpdate) {
            // entiteti = entiteti.filter((r) => !r.getUniverzitet());
            // entiteti.push(this._entitet.getUniverzitet());
          } else if (isFormForDetails) {
            if (
              this._entitet.getUniverzitet() &&
              !entiteti.find(
                (e) =>
                  e.getId().toString() ===
                  this._entitet.getUniverzitet().getId().toString()
              )
            )
              entiteti.push(this._entitet.getUniverzitet());
          }

          // if (!(entiteti.length > 0)) {
          //   this._formSubject.error(
          //     new Error(
          //       'Sve studentske sluzbe vec pripadaju nekim univerzitetima'
          //     )
          //   );
          //   return;
          // }

          let rowEntitet = new FormRow();
          form.addChild(rowEntitet);
          rowEntitet.addChildInterface(<SelectInterface>{
            type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
            controlName: 'univerzitet',
            labelName: 'Univerzitet',
            defaultValue: this._entitet.getUniverzitet(),
            optionValues: entiteti.map((d) => {
              return {
                value: d,
                textToShow: 'id: ' + d.getId().toString() + ', ' + d.getNaziv(),
              };
            }),
            disabled: disableAllFields,
          });
          this._formSubject.next(form);
          // --------------------------------------
          let nastavnikService = csfm.getCrudServiceForModel<Nastavnik, number>(
            NASTAVNIK_SERVICE_FOR_MODEL_INTERFACE
          );

          nastavnikService
            .findAll()
            .pipe(take(1))
            .subscribe(
              (entiteti: Nastavnik[]) => {
                for (let index = 0; index < entiteti.length; index++) {
                  if (
                    entiteti[index].getId().toString() ===
                    this._entitet.getDekan()?.getId().toString()
                  ) {
                    this._entitet.setDekan(entiteti[index]);
                    break;
                  }
                }

                if (isFormForCreation) {
                  // entiteti = entiteti.filter((r) => !r.getUniverzitet());
                } else if (isFormForUpdate) {
                  // entiteti = entiteti.filter((r) => !r.getUniverzitet());
                  // entiteti.push(this._entitet.getDekan());
                } else if (isFormForDetails) {
                  if (
                    this._entitet.getDekan() &&
                    !entiteti.find(
                      (e) =>
                        e.getId().toString() ===
                        this._entitet.getDekan().getId().toString()
                    )
                  )
                    entiteti.push(this._entitet.getDekan());
                }

                // if (!(entiteti.length > 0)) {
                //   this._formSubject.error(
                //     new Error(
                //       'Sve studentske sluzbe vec pripadaju nekim univerzitetima'
                //     )
                //   );
                //   return;
                // }

                let rowEntitet = new FormRow();
                form.addChild(rowEntitet);
                rowEntitet.addChildInterface(<SelectInterface>{
                  type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
                  controlName: 'dekan',
                  labelName: 'Dekan fakulteta',
                  defaultValue: this._entitet.getDekan(),
                  optionValues: entiteti.map((d) => {
                    return {
                      value: d,
                      textToShow: d.getId().toString(),
                    };
                  }),
                  disabled: disableAllFields,
                });
                this._formSubject.next(form);
                this._formSubject.complete();
              },
              (err: HttpErrorResponse) => {
                this._formSubject.error(err);
              }
            );
          this._formSubject.next(form);
          this._formSubject.complete();
          // ------------------------------------
        },
        (err: HttpErrorResponse) => {
          this._formSubject.error(err);
        }
      );
  };

  public save = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    let fakultetService = csfm.getCrudServiceForModel<Fakultet, number>(
      FAKULTET_SERVICE_FOR_MODEL_INTERFACE
    );
    fakultetService
      .save(entitetDict)
      .pipe(take(1))
      .subscribe(
        (entitet) => {},
        (err: HttpErrorResponse) => {
          subject.error(err);
        },
        () => {
          subject.complete();
        }
      );

    return subject;
  };

  public update = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    let fakultetService = csfm.getCrudServiceForModel<Fakultet, number>(
      FAKULTET_SERVICE_FOR_MODEL_INTERFACE
    );
    fakultetService
      .update(entitetDict)
      .pipe(take(1))
      .subscribe(
        (entitet) => {},
        (err: HttpErrorResponse) => {
          subject.error(err);
        },
        () => {
          subject.complete();
        }
      );

    return subject;
  };

  public getHeaderValue = (): HeaderValue => {
    return {
      headerItems: [
        { context: 'id' },
        { context: 'stanje modela' },
        { context: 'naziv' },
        { context: 'opis' },
        { context: 'univerzitet' },
        { context: 'dekan' },
        { context: '' },
        { context: '' },
        { context: '' },
      ],
    };
  };

  public addRowToTable = (
    table: Table,
    detaljiFn: Function = () => {},
    izmeniFn: Function = () => {},
    obrisiFn: Function = () => {}
  ): void => {
    try {
      table.addChildValue({
        data: this._entitet,
        rowItems: [
          {
            context: this._entitet.getId(),
          },
          {
            context: this._entitet.getStanje(),
          },
          {
            context: this._entitet.getNaziv(),
          },
          {
            context: this._entitet.getOpis(),
          },
          {
            context: this._entitet.getUniverzitet()
              ? this._entitet.getUniverzitet().getNaziv()
              : '/',
          },
          {
            context: this._entitet.getDekan()
              ? this._entitet.getDekan().getId().toString()
              : '/',
          },
          {
            context: 'detalji',
            button: { function: detaljiFn },
          },
          {
            context: 'izmeni',
            button: { function: izmeniFn },
          },
          {
            context:
              this._entitet.getStanje() !== StanjeModela.OBRISAN
                ? 'obrisi'
                : '',
            button:
              this._entitet.getStanje() !== StanjeModela.OBRISAN
                ? { function: obrisiFn }
                : undefined,
          },
        ],
      });
    } catch (error) {
      console.log(error);
    }
  };
}
