import { StanjeModela } from '../enum/stanje_modela.enum';
import { CrudServiceForModel } from '../../service/crud.service-for-model';
import { Subject } from 'rxjs';
import {
  Form,
  FormRow,
  ROW_ITEM_TYPE,
  FormFieldInputNumberInterface,
  FORM_FIELD_INPUT_TYPE,
  SelectInterface,
  VALIDATOR_NAMES,
  FormFieldInputTextInterface,
  TextareaInterface,
} from 'my-angular-form';
import { Validators } from '@angular/forms';
import { HeaderValue, Table } from 'my-angular-table';
import { Formable } from '../interface/formable.interface';
import { Tableable } from '../interface/tableable.interface';
import { StudijskiProgram } from '../entity/studijski_program.model';
import { STUDIJSKI_PROGRAM_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/studijski_program_service.service';
import { take } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { Nastavnik } from '../entity/nastavnik.model';
import { NASTAVNIK_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/nastavnik_service.service';
import { Fakultet } from '../entity/fakultet.model';
import { FAKULTET_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/fakultet_service.service';

export class StudijskiProgramFormableTableable implements Formable, Tableable {
  private _formSubject: Subject<Form> = new Subject();

  public getFormSubject = (): Subject<Form> => {
    return this._formSubject;
  };

  constructor(private _entitet: StudijskiProgram) {}

  public getForm = (
    csfm: CrudServiceForModel,
    isFormForCreation: boolean,
    isFormForDetails: boolean = false,
    isFormForUpdate: boolean = false,
    disableAllFields: boolean = false
  ): void => {
    let form = new Form({
      showSubmitButton: !disableAllFields,
      submitButtonText: 'potvrdi',
      showCancelButton: !disableAllFields,
      cancelButtonText: 'odustani',
      showResetButton: isFormForCreation,
      resetButtonText: 'resetuj',
      submitOnlyIfFormValid: true,
    });

    // private id: number;+

    // private naziv: string;+

    // private brojGodinaStudija: number;+

    // private stanje: StanjeModela;+

    // private version: number;+

    // private fakultet: Fakultet;+

    // private rukovodilac: Nastavnik;+

    // private godineStudija: GodinaStudija[];+

    // private opis: string;+

    if (!isFormForCreation) {
      let rowId = new FormRow();
      form.addChild(rowId);
      rowId.addChildInterface(<FormFieldInputNumberInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
        formFieldInputType: FORM_FIELD_INPUT_TYPE.NUMBER,
        controlName: 'id',
        labelName: 'Id',
        defaultValue: this._entitet.getId(),
        disabled: true,
      });

      // console.log(StanjeModela.AKTIVAN);
      // console.log(typeof StanjeModela.AKTIVAN);

      let rowStanjeModela = new FormRow();
      form.addChild(rowStanjeModela);
      rowStanjeModela.addChildInterface(<SelectInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
        controlName: 'stanje',
        labelName: 'Stanje modela',
        defaultValue: this._entitet.getStanje(),
        optionValues: [
          { value: StanjeModela.AKTIVAN, textToShow: 'AKTIVAN' },
          { value: StanjeModela.NEAKTIVAN, textToShow: 'NEAKTIVAN' },
        ],
        disabled: disableAllFields,
        validators: [
          {
            message: 'Stanje modela je obavezno',
            name: VALIDATOR_NAMES.REQUIRED,
            validatorFn: Validators.required,
          },
        ],
      });
    }

    let rowNaziv = new FormRow();
    form.addChild(rowNaziv);
    rowNaziv.addChildInterface(<FormFieldInputTextInterface>{
      type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
      formFieldInputType: FORM_FIELD_INPUT_TYPE.TEXT,
      controlName: 'naziv',
      labelName: 'Naziv studijskog programa',
      defaultValue: this._entitet.getNaziv(),
      appearance: 'outline',
      placeholder: 'unesite naziv studijskog programa',
      disabled: disableAllFields,
      validators: [
        {
          message: 'naziv studijskog programa je obavezan',
          name: VALIDATOR_NAMES.REQUIRED,
          validatorFn: Validators.required,
        },
      ],
    });

    let rowBrojGodinaStudija = new FormRow();
    form.addChild(rowBrojGodinaStudija);
    rowBrojGodinaStudija.addChildInterface(<FormFieldInputNumberInterface>{
      type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
      formFieldInputType: FORM_FIELD_INPUT_TYPE.NUMBER,
      controlName: 'brojGodinaStudija',
      labelName: 'Trajanje u godinama',
      step: 1,
      textSuffix: 'god.',
      defaultValue: this._entitet.getBrojGodinaStudija(),
      disabled: disableAllFields,
      validators: [
        {
          message: 'studijski program mora da traje najmanje 3 godine',
          name: VALIDATOR_NAMES.MIN,
          validatorFn: Validators.min(3),
        },
        {
          message: 'studijski program moze da traje najvise 6 godina',
          name: VALIDATOR_NAMES.MAX,
          validatorFn: Validators.max(6),
        },
      ],
    });

    let rowOpis = new FormRow();
    form.addChild(rowOpis);
    rowOpis.addChildInterface(<TextareaInterface>{
      type: ROW_ITEM_TYPE.FORM_FIELD_TEXTAREA,
      controlName: 'opis',
      labelName: 'Opis studijskog programa',
      defaultValue: this._entitet.getOpis(),
      validators: [
        {
          message: 'opis mora imati vise od 9 karaktera',
          name: VALIDATOR_NAMES.MIN_LENGTH,
          validatorFn: Validators.minLength(10),
          length: 10,
        },
      ],
      showHintAboutMinMaxLength: true,
      requiredOption: {
        required: true,
        hideRequiredMarker: true,
      },
      disabled: disableAllFields,
    });

    let nastavnikService = csfm.getCrudServiceForModel<Nastavnik, number>(
      NASTAVNIK_SERVICE_FOR_MODEL_INTERFACE
    );

    nastavnikService
      .findAll()
      .pipe(take(1))
      .subscribe(
        (entiteti: Nastavnik[]) => {
          for (let index = 0; index < entiteti.length; index++) {
            if (
              entiteti[index].getId().toString() ===
              this._entitet.getRukovodilac()?.getId().toString()
            ) {
              this._entitet.setRukovodilac(entiteti[index]);
              break;
            }
          }

          if (isFormForCreation) {
            // entiteti = entiteti.filter((r) => !r.getUniverzitet());
          } else if (isFormForUpdate) {
            // entiteti = entiteti.filter((r) => !r.getUniverzitet());
            // entiteti.push(this._entitet.getRukovodilac());
          } else if (isFormForDetails) {
            if (
              this._entitet.getRukovodilac() &&
              !entiteti.find(
                (e) =>
                  e.getId().toString() ===
                  this._entitet.getRukovodilac().getId().toString()
              )
            )
              entiteti.push(this._entitet.getRukovodilac());
          }

          // if (!(entiteti.length > 0)) {
          //   this._formSubject.error(
          //     new Error(
          //       'Sve studentske sluzbe vec pripadaju nekim univerzitetima'
          //     )
          //   );
          //   return;
          // } else {
          let rowEntitet = new FormRow();
          form.addChild(rowEntitet);
          rowEntitet.addChildInterface(<SelectInterface>{
            type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
            controlName: 'rukovodilac',
            labelName: 'Rukovodilac',
            defaultValue: this._entitet.getRukovodilac(),
            optionValues: entiteti.map((d) => {
              return {
                value: d,
                textToShow: d.getId().toString(),
              };
            }),
            disabled: disableAllFields,
          });
          // }

          this._formSubject.next(form);
          // --------------------------------------
          let fakultetService = csfm.getCrudServiceForModel<Fakultet, number>(
            FAKULTET_SERVICE_FOR_MODEL_INTERFACE
          );

          fakultetService
            .findAll()
            .pipe(take(1))
            .subscribe(
              (entiteti: Fakultet[]) => {
                for (let index = 0; index < entiteti.length; index++) {
                  if (
                    entiteti[index].getId().toString() ===
                    this._entitet.getFakultet()?.getId().toString()
                  ) {
                    this._entitet.setFakultet(entiteti[index]);
                    break;
                  }
                }

                if (isFormForCreation) {
                  // entiteti = entiteti.filter((r) => !r.getUniverzitet());
                } else if (isFormForUpdate) {
                  // entiteti = entiteti.filter((r) => !r.getUniverzitet());
                  // entiteti.push(this._entitet.getFakultet());
                } else if (isFormForDetails) {
                  if (
                    this._entitet.getFakultet() &&
                    !entiteti.find(
                      (e) =>
                        e.getId().toString() ===
                        this._entitet.getFakultet().getId().toString()
                    )
                  )
                    entiteti.push(this._entitet.getFakultet());
                }

                // if (!(entiteti.length > 0)) {
                //   this._formSubject.error(
                //     new Error(
                //       'Sve studentske sluzbe vec pripadaju nekim univerzitetima'
                //     )
                //   );
                //   return;
                // } else {
                let rowEntitet = new FormRow();
                form.addChild(rowEntitet);
                rowEntitet.addChildInterface(<SelectInterface>{
                  type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
                  controlName: 'fakultet',
                  labelName: 'Fakultet',
                  defaultValue: this._entitet.getFakultet(),
                  optionValues: entiteti.map((d) => {
                    return {
                      value: d,
                      textToShow:
                        'id: ' + d.getId().toString() + ', ' + d.getNaziv(),
                    };
                  }),
                  disabled: disableAllFields,
                });
                // }

                this._formSubject.next(form);
                this._formSubject.complete();
              },
              (err: HttpErrorResponse) => {
                this._formSubject.error(err);
              }
            );
          // ------------------------------------
        },
        (err: HttpErrorResponse) => {
          this._formSubject.error(err);
        }
      );
  };

  public save = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    let studijskiProgramService = csfm.getCrudServiceForModel<
      StudijskiProgram,
      number
    >(STUDIJSKI_PROGRAM_SERVICE_FOR_MODEL_INTERFACE);
    studijskiProgramService
      .save(entitetDict)
      .pipe(take(1))
      .subscribe(
        (entitet) => {},
        (err: HttpErrorResponse) => {
          subject.error(err);
        },
        () => {
          subject.complete();
        }
      );

    return subject;
  };

  public update = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    let studijskiProgramService = csfm.getCrudServiceForModel<
      StudijskiProgram,
      number
    >(STUDIJSKI_PROGRAM_SERVICE_FOR_MODEL_INTERFACE);
    studijskiProgramService
      .update(entitetDict)
      .pipe(take(1))
      .subscribe(
        (entitet) => {},
        (err: HttpErrorResponse) => {
          subject.error(err);
        },
        () => {
          subject.complete();
        }
      );

    return subject;
  };

  public getHeaderValue = (): HeaderValue => {
    return {
      headerItems: [
        { context: 'id' },
        { context: 'stanje modela' },
        { context: 'naziv' },
        { context: 'trajanje u godinama' },
        { context: 'opis' },
        { context: 'rukovodilac' },
        { context: 'fakultet' },
        { context: '' },
        { context: '' },
        { context: '' },
      ],
    };
  };

  public addRowToTable = (
    table: Table,
    detaljiFn: Function = () => {},
    izmeniFn: Function = () => {},
    obrisiFn: Function = () => {}
  ): void => {
    try {
      table.addChildValue({
        data: this._entitet,
        rowItems: [
          {
            context: this._entitet.getId(),
          },
          {
            context: this._entitet.getStanje(),
          },
          {
            context: this._entitet.getNaziv(),
          },
          {
            context: this._entitet.getBrojGodinaStudija(),
          },
          {
            context: this._entitet.getOpis(),
          },
          {
            context: this._entitet.getRukovodilac()
              ? this._entitet.getRukovodilac().getId()
              : '/',
          },
          {
            context: this._entitet.getFakultet()
              ? 'id: ' +
                this._entitet.getFakultet().getId() +
                ', ' +
                this._entitet.getFakultet().getNaziv()
              : '/',
          },
          {
            context: 'detalji',
            button: { function: detaljiFn },
          },
          {
            context: 'izmeni',
            button: { function: izmeniFn },
          },
          {
            context:
              this._entitet.getStanje() !== StanjeModela.OBRISAN
                ? 'obrisi'
                : '',
            button:
              this._entitet.getStanje() !== StanjeModela.OBRISAN
                ? { function: obrisiFn }
                : undefined,
          },
        ],
      });
    } catch (error) {
      console.log(error);
    }
  };
}
