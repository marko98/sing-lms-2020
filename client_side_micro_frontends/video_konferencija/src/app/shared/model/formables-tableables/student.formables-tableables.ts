import { StanjeModela } from '../enum/stanje_modela.enum';
import { CrudServiceForModel } from '../../service/crud.service-for-model';
import { Subject } from 'rxjs';
import {
  Form,
  FormRow,
  ROW_ITEM_TYPE,
  FormFieldInputNumberInterface,
  FORM_FIELD_INPUT_TYPE,
  SelectInterface,
  VALIDATOR_NAMES,
} from 'my-angular-form';
import { Validators } from '@angular/forms';
import { HeaderValue, Table } from 'my-angular-table';
import { Formable } from '../interface/formable.interface';
import { Tableable } from '../interface/tableable.interface';
import { Student } from '../entity/student.model';
import { STUDENT_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/student_service.service';
import { take } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { RegistrovaniKorisnik } from '../entity/registrovani_korisnik.model';
import { REGISTROVANI_KORISNIK_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/registrovani_korisnik_service.service';
import { StudentNaStudiji } from '../entity/student_na_studiji.model';
import { STUDENT_NA_STUDIJI_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/student_na_studiji_service.service';

export class StudentFormableTableable implements Formable, Tableable {
  private _formSubject: Subject<Form> = new Subject();

  public getFormSubject = (): Subject<Form> => {
    return this._formSubject;
  };

  constructor(private _entitet: Student) {}

  public getForm = (
    csfm: CrudServiceForModel,
    isFormForCreation: boolean,
    isFormForDetails: boolean = false,
    isFormForUpdate: boolean = false,
    disableAllFields: boolean = false
  ): void => {
    let form = new Form({
      showSubmitButton: !disableAllFields,
      submitButtonText: 'potvrdi',
      showCancelButton: !disableAllFields,
      cancelButtonText: 'odustani',
      showResetButton: isFormForCreation,
      resetButtonText: 'resetuj',
      submitOnlyIfFormValid: true,
    });

    // private id: number;+

    // private stanje: StanjeModela;+

    // private version: number;+

    // private registrovaniKorisnik: RegistrovaniKorisnik;+

    // private studije: StudentNaStudiji[];+

    if (!isFormForCreation) {
      let rowId = new FormRow();
      form.addChild(rowId);
      rowId.addChildInterface(<FormFieldInputNumberInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
        formFieldInputType: FORM_FIELD_INPUT_TYPE.NUMBER,
        controlName: 'id',
        labelName: 'Id',
        defaultValue: this._entitet.getId(),
        disabled: true,
      });

      // console.log(StanjeModela.AKTIVAN);
      // console.log(typeof StanjeModela.AKTIVAN);

      let rowStanjeModela = new FormRow();
      form.addChild(rowStanjeModela);
      rowStanjeModela.addChildInterface(<SelectInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
        controlName: 'stanje',
        labelName: 'Stanje modela',
        defaultValue: this._entitet.getStanje(),
        optionValues: [
          { value: StanjeModela.AKTIVAN, textToShow: 'AKTIVAN' },
          { value: StanjeModela.NEAKTIVAN, textToShow: 'NEAKTIVAN' },
        ],
        disabled: disableAllFields,
        validators: [
          {
            message: 'Stanje modela je obavezno',
            name: VALIDATOR_NAMES.REQUIRED,
            validatorFn: Validators.required,
          },
        ],
      });
    }

    let registrovaniKorisnikService = csfm.getCrudServiceForModel<
      RegistrovaniKorisnik,
      number
    >(REGISTROVANI_KORISNIK_SERVICE_FOR_MODEL_INTERFACE);

    registrovaniKorisnikService
      .findAll()
      .pipe(take(1))
      .subscribe(
        (entiteti: RegistrovaniKorisnik[]) => {
          for (let index = 0; index < entiteti.length; index++) {
            if (
              entiteti[index].getId().toString() ===
              this._entitet.getRegistrovaniKorisnik()?.getId().toString()
            ) {
              this._entitet.setRegistrovaniKorisnik(entiteti[index]);
              break;
            }
          }

          if (isFormForCreation) {
            entiteti = entiteti.filter((r) => !r.getStudent());
          } else if (isFormForUpdate) {
            entiteti = entiteti.filter((r) => !r.getStudent());
            entiteti.push(this._entitet.getRegistrovaniKorisnik());
          } else if (isFormForDetails) {
            if (
              this._entitet.getRegistrovaniKorisnik() &&
              !entiteti.find(
                (e) =>
                  e.getId().toString() ===
                  this._entitet.getRegistrovaniKorisnik().getId().toString()
              )
            )
              entiteti.push(this._entitet.getRegistrovaniKorisnik());
          }

          if (!(entiteti.length > 0)) {
            this._formSubject.error(
              new Error('Svi registrovani korinsici su vec i studenti')
            );
            return;
          } else {
            let rowEntitet = new FormRow();
            form.addChild(rowEntitet);
            rowEntitet.addChildInterface(<SelectInterface>{
              type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
              controlName: 'registrovaniKorisnik',
              labelName: 'Registrovani korisnik',
              defaultValue: this._entitet.getRegistrovaniKorisnik(),
              optionValues: entiteti.map((d) => {
                return {
                  value: d,
                  textToShow:
                    'id: ' +
                    d.getId().toString() +
                    ', ' +
                    d.getIme() +
                    ' ' +
                    d.getPrezime(),
                };
              }),
              disabled: disableAllFields,
            });
          }

          this._formSubject.next(form);
          this._formSubject.complete();
        },
        (err: HttpErrorResponse) => {
          this._formSubject.error(err);
        }
      );
  };

  public save = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    let studentService = csfm.getCrudServiceForModel<Student, number>(
      STUDENT_SERVICE_FOR_MODEL_INTERFACE
    );
    studentService
      .save(entitetDict)
      .pipe(take(1))
      .subscribe(
        (entitet) => {},
        (err: HttpErrorResponse) => {
          subject.error(err);
        },
        () => {
          subject.complete();
        }
      );

    return subject;
  };

  public update = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    let studentService = csfm.getCrudServiceForModel<Student, number>(
      STUDENT_SERVICE_FOR_MODEL_INTERFACE
    );
    studentService
      .update(entitetDict)
      .pipe(take(1))
      .subscribe(
        (entitet) => {},
        (err: HttpErrorResponse) => {
          subject.error(err);
        },
        () => {
          subject.complete();
        }
      );

    return subject;
  };

  public getHeaderValue = (): HeaderValue => {
    return {
      headerItems: [
        { context: 'id' },
        { context: 'stanje modela' },
        { context: 'registrovani korisnik' },
        { context: '' },
        { context: '' },
        { context: '' },
      ],
    };
  };

  public addRowToTable = (
    table: Table,
    detaljiFn: Function = () => {},
    izmeniFn: Function = () => {},
    obrisiFn: Function = () => {}
  ): void => {
    try {
      table.addChildValue({
        data: this._entitet,
        rowItems: [
          {
            context: this._entitet.getId(),
          },
          {
            context: this._entitet.getStanje(),
          },
          {
            context: this._entitet.getRegistrovaniKorisnik()
              ? this._entitet.getRegistrovaniKorisnik().getIme() +
                ' ' +
                this._entitet.getRegistrovaniKorisnik().getPrezime()
              : '/',
          },
          {
            context: 'detalji',
            button: { function: detaljiFn },
          },
          {
            context: 'izmeni',
            button: { function: izmeniFn },
          },
          {
            context:
              this._entitet.getStanje() !== StanjeModela.OBRISAN
                ? 'obrisi'
                : '',
            button:
              this._entitet.getStanje() !== StanjeModela.OBRISAN
                ? { function: obrisiFn }
                : undefined,
          },
        ],
      });
    } catch (error) {
      console.log(error);
    }
  };
}
