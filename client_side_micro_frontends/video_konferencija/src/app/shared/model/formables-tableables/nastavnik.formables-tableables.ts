import { StanjeModela } from '../enum/stanje_modela.enum';
import { CrudServiceForModel } from '../../service/crud.service-for-model';
import { Subject } from 'rxjs';
import {
  Form,
  FormRow,
  ROW_ITEM_TYPE,
  FormFieldInputNumberInterface,
  FORM_FIELD_INPUT_TYPE,
  SelectInterface,
  VALIDATOR_NAMES,
  TextareaInterface,
} from 'my-angular-form';
import { Validators } from '@angular/forms';
import { HeaderValue, Table } from 'my-angular-table';
import { Formable } from '../interface/formable.interface';
import { Tableable } from '../interface/tableable.interface';
import { Nastavnik } from '../entity/nastavnik.model';
import { take } from 'rxjs/operators';
import { NASTAVNIK_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/nastavnik_service.service';
import { HttpErrorResponse } from '@angular/common/http';
import { RegistrovaniKorisnik } from '../entity/registrovani_korisnik.model';
import { REGISTROVANI_KORISNIK_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/registrovani_korisnik_service.service';

export class NastavnikFormableTableable implements Formable, Tableable {
  private _formSubject: Subject<Form> = new Subject();

  public getFormSubject = (): Subject<Form> => {
    return this._formSubject;
  };

  constructor(private _entitet: Nastavnik) {}

  public getForm = (
    csfm: CrudServiceForModel,
    isFormForCreation: boolean,
    isFormForDetails: boolean = false,
    isFormForUpdate: boolean = false,
    disableAllFields: boolean = false
  ): void => {
    let form = new Form({
      showSubmitButton: !disableAllFields,
      submitButtonText: 'potvrdi',
      showCancelButton: !disableAllFields,
      cancelButtonText: 'odustani',
      showResetButton: isFormForCreation,
      resetButtonText: 'resetuj',
      submitOnlyIfFormValid: true,
    });

    // private id: number;+

    // private biografija: string;+

    // private stanje: StanjeModela;+

    // private version: number;+

    // private univerziteti: Univerzitet[];+

    // private fakulteti: Fakultet[];+

    // private nastavnikFakulteti: NastavnikFakultet[];+

    // private terminiNastave: TerminNastave[];+

    // private studijskiProgrami: StudijskiProgram[];+

    // private titule: Titula[];+

    // private registrovaniKorisnik: RegistrovaniKorisnik;+

    // private mentorDiplomskiRadovi: DiplomskiRad[];+

    // private istrazivackiRadovi: IstrazivackiRad[];+

    // private konsultacije: Konsultacija[];+

    // private nastavnikEvaluacijeZnanja: NastavnikEvaluacijaZnanja[];+

    // private nastavnikDiplomskiRadovi: NastavnikDiplomskiRad[];+

    // private nastavnikNaRealizacijama: NastavnikNaRealizaciji[];+

    if (!isFormForCreation) {
      let rowId = new FormRow();
      form.addChild(rowId);
      rowId.addChildInterface(<FormFieldInputNumberInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
        formFieldInputType: FORM_FIELD_INPUT_TYPE.NUMBER,
        controlName: 'id',
        labelName: 'Id',
        defaultValue: this._entitet.getId(),
        disabled: true,
      });

      // console.log(StanjeModela.AKTIVAN);
      // console.log(typeof StanjeModela.AKTIVAN);

      let rowStanjeModela = new FormRow();
      form.addChild(rowStanjeModela);
      rowStanjeModela.addChildInterface(<SelectInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
        controlName: 'stanje',
        labelName: 'Stanje modela',
        defaultValue: this._entitet.getStanje(),
        optionValues: [
          { value: StanjeModela.AKTIVAN, textToShow: 'AKTIVAN' },
          { value: StanjeModela.NEAKTIVAN, textToShow: 'NEAKTIVAN' },
        ],
        disabled: disableAllFields,
        validators: [
          {
            message: 'Stanje modela je obavezno',
            name: VALIDATOR_NAMES.REQUIRED,
            validatorFn: Validators.required,
          },
        ],
      });
    }

    let rowBiografija = new FormRow();
    form.addChild(rowBiografija);
    rowBiografija.addChildInterface(<TextareaInterface>{
      type: ROW_ITEM_TYPE.FORM_FIELD_TEXTAREA,
      controlName: 'biografija',
      labelName: 'Biografija',
      defaultValue: this._entitet.getBiografija(),
      validators: [
        {
          message: 'biografija mora imati vise od 9 karaktera',
          name: VALIDATOR_NAMES.MIN_LENGTH,
          validatorFn: Validators.minLength(10),
          length: 10,
        },
      ],
      showHintAboutMinMaxLength: true,
      requiredOption: {
        required: true,
        hideRequiredMarker: true,
      },
      disabled: disableAllFields,
    });

    let registrovaniKorisnikService = csfm.getCrudServiceForModel<
      RegistrovaniKorisnik,
      number
    >(REGISTROVANI_KORISNIK_SERVICE_FOR_MODEL_INTERFACE);

    registrovaniKorisnikService
      .findAll()
      .pipe(take(1))
      .subscribe(
        (entiteti: RegistrovaniKorisnik[]) => {
          for (let index = 0; index < entiteti.length; index++) {
            if (
              entiteti[index].getId().toString() ===
              this._entitet.getRegistrovaniKorisnik()?.getId().toString()
            ) {
              this._entitet.setRegistrovaniKorisnik(entiteti[index]);
              break;
            }
          }

          if (isFormForCreation) {
            entiteti = entiteti.filter((r) => !r.getNastavnik());
          } else if (isFormForUpdate) {
            entiteti = entiteti.filter((r) => !r.getNastavnik());
            entiteti.push(this._entitet.getRegistrovaniKorisnik());
          } else if (isFormForDetails) {
            if (
              this._entitet.getRegistrovaniKorisnik() &&
              !entiteti.find(
                (e) =>
                  e.getId().toString() ===
                  this._entitet.getRegistrovaniKorisnik().getId().toString()
              )
            )
              entiteti.push(this._entitet.getRegistrovaniKorisnik());
          }

          if (!(entiteti.length > 0)) {
            this._formSubject.error(
              new Error('Svi registrovani korisnici su i nastavnici')
            );
            return;
          } else {
            let rowEntitet = new FormRow();
            form.addChild(rowEntitet);
            rowEntitet.addChildInterface(<SelectInterface>{
              type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
              controlName: 'registrovaniKorisnik',
              labelName: 'Registrovani korisnik',
              defaultValue: this._entitet.getRegistrovaniKorisnik(),
              optionValues: entiteti.map((d) => {
                return {
                  value: d,
                  textToShow:
                    'id: ' +
                    d.getId().toString() +
                    ', ' +
                    d.getIme() +
                    ' ' +
                    d.getPrezime(),
                };
              }),
              disabled: disableAllFields,
            });
          }

          this._formSubject.next(form);
          this._formSubject.complete();
        },
        (err: HttpErrorResponse) => {
          this._formSubject.error(err);
        }
      );
  };

  public save = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    let nastavnikService = csfm.getCrudServiceForModel<Nastavnik, number>(
      NASTAVNIK_SERVICE_FOR_MODEL_INTERFACE
    );
    nastavnikService
      .save(entitetDict)
      .pipe(take(1))
      .subscribe(
        (entitet) => {},
        (err: HttpErrorResponse) => {
          subject.error(err);
        },
        () => {
          subject.complete();
        }
      );

    return subject;
  };

  public update = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    let nastavnikService = csfm.getCrudServiceForModel<Nastavnik, number>(
      NASTAVNIK_SERVICE_FOR_MODEL_INTERFACE
    );
    nastavnikService
      .update(entitetDict)
      .pipe(take(1))
      .subscribe(
        (entitet) => {},
        (err: HttpErrorResponse) => {
          subject.error(err);
        },
        () => {
          subject.complete();
        }
      );

    return subject;
  };

  public getHeaderValue = (): HeaderValue => {
    return {
      headerItems: [
        { context: 'id' },
        { context: 'stanje modela' },
        { context: 'biografija' },
        { context: 'registrovani korisnik' },
        { context: '' },
        { context: '' },
        { context: '' },
      ],
    };
  };

  public addRowToTable = (
    table: Table,
    detaljiFn: Function = () => {},
    izmeniFn: Function = () => {},
    obrisiFn: Function = () => {}
  ): void => {
    try {
      table.addChildValue({
        data: this._entitet,
        rowItems: [
          {
            context: this._entitet.getId(),
          },
          {
            context: this._entitet.getStanje(),
          },
          {
            context: this._entitet.getBiografija(),
          },
          {
            context: this._entitet.getRegistrovaniKorisnik()
              ? this._entitet.getRegistrovaniKorisnik().getIme() +
                ' ' +
                this._entitet.getRegistrovaniKorisnik().getPrezime()
              : '/',
          },
          {
            context: 'detalji',
            button: { function: detaljiFn },
          },
          {
            context: 'izmeni',
            button: { function: izmeniFn },
          },
          {
            context:
              this._entitet.getStanje() !== StanjeModela.OBRISAN
                ? 'obrisi'
                : '',
            button:
              this._entitet.getStanje() !== StanjeModela.OBRISAN
                ? { function: obrisiFn }
                : undefined,
          },
        ],
      });
    } catch (error) {
      console.log(error);
    }
  };
}
