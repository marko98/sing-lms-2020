import { StanjeModela } from '../enum/stanje_modela.enum';
import { CrudServiceForModel } from '../../service/crud.service-for-model';
import { Subject } from 'rxjs';
import {
  Form,
  FormRow,
  ROW_ITEM_TYPE,
  FormFieldInputNumberInterface,
  FORM_FIELD_INPUT_TYPE,
  SelectInterface,
  VALIDATOR_NAMES,
} from 'my-angular-form';
import { Validators } from '@angular/forms';
import { HeaderValue, Table } from 'my-angular-table';
import { Formable } from '../interface/formable.interface';
import { Tableable } from '../interface/tableable.interface';
import { StudentskaSluzba } from '../entity/studentska_sluzba.model';
import { Univerzitet } from '../entity/univerzitet.model';
import { take } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { UNIVERZITET_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/univerzitet_service.service';
import { STUDENTSKA_SLUZBA_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/studentska_sluzba_service.service';
import { Odeljenje } from '../entity/odeljenje.model';
import { ODELJENJE_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/odeljenje_service.service';

export class StudentskaSluzbaFormableTableable implements Formable, Tableable {
  private _formSubject: Subject<Form> = new Subject();

  public getFormSubject = (): Subject<Form> => {
    return this._formSubject;
  };

  constructor(private _entitet: StudentskaSluzba) {}

  public getForm = (
    csfm: CrudServiceForModel,
    isFormForCreation: boolean,
    isFormForDetails: boolean = false,
    isFormForUpdate: boolean = false,
    disableAllFields: boolean = false
  ): void => {
    // private id: number;+

    // private stanje: StanjeModela;+

    // private version: number;+

    // private studentskiSluzbenici: StudentskiSluzbenik[];+

    // private univerzitet: Univerzitet;

    // private odeljenje: Odeljenje;

    let form = new Form({
      showSubmitButton: !disableAllFields,
      submitButtonText: 'potvrdi',
      showCancelButton: !disableAllFields,
      cancelButtonText: 'odustani',
      showResetButton: isFormForCreation,
      resetButtonText: 'resetuj',
      submitOnlyIfFormValid: true,
    });

    if (!isFormForCreation) {
      let rowId = new FormRow();
      form.addChild(rowId);
      rowId.addChildInterface(<FormFieldInputNumberInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
        formFieldInputType: FORM_FIELD_INPUT_TYPE.NUMBER,
        controlName: 'id',
        labelName: 'Id',
        defaultValue: this._entitet.getId(),
        disabled: true,
      });

      // console.log(StanjeModela.AKTIVAN);
      // console.log(typeof StanjeModela.AKTIVAN);

      let rowStanjeModela = new FormRow();
      form.addChild(rowStanjeModela);
      rowStanjeModela.addChildInterface(<SelectInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
        controlName: 'stanje',
        labelName: 'Stanje modela',
        defaultValue: this._entitet.getStanje(),
        optionValues: [
          { value: StanjeModela.AKTIVAN, textToShow: 'AKTIVAN' },
          { value: StanjeModela.NEAKTIVAN, textToShow: 'NEAKTIVAN' },
        ],
        disabled: disableAllFields,
        validators: [
          {
            message: 'Stanje modela je obavezno',
            name: VALIDATOR_NAMES.REQUIRED,
            validatorFn: Validators.required,
          },
        ],
      });
    }

    let univerzitetService = csfm.getCrudServiceForModel<Univerzitet, number>(
      UNIVERZITET_SERVICE_FOR_MODEL_INTERFACE
    );

    univerzitetService
      .findAll()
      .pipe(take(1))
      .subscribe(
        (univerziteti: Univerzitet[]) => {
          for (let index = 0; index < univerziteti.length; index++) {
            if (
              univerziteti[index].getId().toString() ===
              this._entitet.getUniverzitet()?.getId().toString()
            ) {
              this._entitet.setUniverzitet(univerziteti[index]);
              break;
            }
          }

          if (isFormForCreation) {
            // da li studentska sluzba moze da ima vise univerziteta?
            univerziteti = univerziteti.filter((r) => !r.getStudentskaSluzba());
          } else if (isFormForUpdate) {
            // da li studentska sluzba moze da ima vise univerziteta?
            univerziteti = univerziteti.filter((r) => !r.getStudentskaSluzba());
            univerziteti.push(this._entitet.getUniverzitet());
          } else if (isFormForDetails) {
            if (
              this._entitet.getUniverzitet() &&
              !univerziteti.find(
                (e) =>
                  e.getId().toString() ===
                  this._entitet.getUniverzitet().getId().toString()
              )
            )
              univerziteti.push(this._entitet.getUniverzitet());
          }

          if (!(univerziteti.length > 0)) {
            this._formSubject.error(
              new Error('Svi univerziteti vec imaju svoje studentske sluzbe')
            );
            return;
          }

          let rowEntitet = new FormRow();
          form.addChild(rowEntitet);
          rowEntitet.addChildInterface(<SelectInterface>{
            type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
            controlName: 'univerzitet',
            labelName: 'Univerzitet',
            defaultValue: this._entitet.getUniverzitet(),
            optionValues: univerziteti.map((d) => {
              return {
                value: d,
                textToShow: d.getNaziv(),
              };
            }),
            disabled: disableAllFields,
          });
          this._formSubject.next(form);
          // ------------------------------------------------------------------
          let odeljenjeService = csfm.getCrudServiceForModel<Odeljenje, number>(
            ODELJENJE_SERVICE_FOR_MODEL_INTERFACE
          );

          odeljenjeService
            .findAll()
            .pipe(take(1))
            .subscribe(
              (odeljenja: Odeljenje[]) => {
                for (let index = 0; index < odeljenja.length; index++) {
                  if (
                    odeljenja[index].getId().toString() ===
                    this._entitet.getOdeljenje()?.getId().toString()
                  ) {
                    this._entitet.setOdeljenje(odeljenja[index]);
                    break;
                  }
                }

                if (isFormForCreation) {
                  // da li studentska sluzba moze da ima vise univerziteta?
                  odeljenja = odeljenja.filter((r) => !r.getStudentskaSluzba());
                } else if (isFormForUpdate) {
                  // da li studentska sluzba moze da ima vise univerziteta?
                  odeljenja = odeljenja.filter((r) => !r.getStudentskaSluzba());
                  odeljenja.push(this._entitet.getOdeljenje());
                } else if (isFormForDetails) {
                  if (
                    this._entitet.getOdeljenje() &&
                    !odeljenja.find(
                      (e) =>
                        e.getId().toString() ===
                        this._entitet.getOdeljenje().getId().toString()
                    )
                  )
                    odeljenja.push(this._entitet.getOdeljenje());
                }

                if (!(odeljenja.length > 0)) {
                  this._formSubject.error(
                    new Error('Sve odeljenja vec imaju svoje studentske sluzbe')
                  );
                  return;
                }

                let rowEntitet = new FormRow();
                form.addChild(rowEntitet);
                rowEntitet.addChildInterface(<SelectInterface>{
                  type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
                  controlName: 'odeljenje',
                  labelName: 'Odeljenje',
                  defaultValue: this._entitet.getOdeljenje(),
                  optionValues: odeljenja.map((d) => {
                    return {
                      value: d,
                      textToShow: d.getId().toString(),
                    };
                  }),
                  disabled: disableAllFields,
                });
                this._formSubject.next(form);
                this._formSubject.complete();
              },
              (err: HttpErrorResponse) => {
                this._formSubject.error(err);
              }
            );
          // ------------------------------------------------------------------
        },
        (err: HttpErrorResponse) => {
          this._formSubject.error(err);
        }
      );
  };

  public save = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    let studentskaSluzbaService = csfm.getCrudServiceForModel<
      StudentskaSluzba,
      number
    >(STUDENTSKA_SLUZBA_SERVICE_FOR_MODEL_INTERFACE);
    studentskaSluzbaService
      .save(entitetDict)
      .pipe(take(1))
      .subscribe(
        (entitet) => {},
        (err: HttpErrorResponse) => {
          subject.error(err);
        },
        () => {
          subject.complete();
        }
      );

    return subject;
  };

  public update = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    let studentskaSluzbaService = csfm.getCrudServiceForModel<
      StudentskaSluzba,
      number
    >(STUDENTSKA_SLUZBA_SERVICE_FOR_MODEL_INTERFACE);
    studentskaSluzbaService
      .update(entitetDict)
      .pipe(take(1))
      .subscribe(
        (entitet) => {},
        (err: HttpErrorResponse) => {
          subject.error(err);
        },
        () => {
          subject.complete();
        }
      );

    return subject;
  };

  public getHeaderValue = (): HeaderValue => {
    return {
      headerItems: [
        { context: 'id' },
        { context: 'stanje modela' },
        { context: 'univerzitet' },
        { context: 'odeljenje' },
        { context: '' },
        { context: '' },
        { context: '' },
      ],
    };
  };

  public addRowToTable = (
    table: Table,
    detaljiFn: Function = () => {},
    izmeniFn: Function = () => {},
    obrisiFn: Function = () => {}
  ): void => {
    try {
      table.addChildValue({
        data: this._entitet,
        rowItems: [
          {
            context: this._entitet.getId(),
          },
          {
            context: this._entitet.getStanje(),
          },
          {
            context: this._entitet.getUniverzitet()
              ? this._entitet.getUniverzitet().getId() +
                ' ' +
                this._entitet.getUniverzitet().getNaziv()
              : '/',
          },
          {
            context: this._entitet.getOdeljenje()
              ? this._entitet.getOdeljenje().getId()
              : '/',
          },
          {
            context: 'detalji',
            button: { function: detaljiFn },
          },
          {
            context: 'izmeni',
            button: { function: izmeniFn },
          },
          {
            context:
              this._entitet.getStanje() !== StanjeModela.OBRISAN
                ? 'obrisi'
                : '',
            button:
              this._entitet.getStanje() !== StanjeModela.OBRISAN
                ? { function: obrisiFn }
                : undefined,
          },
        ],
      });
    } catch (error) {
      console.log(error);
    }
  };
}
