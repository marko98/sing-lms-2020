import { StanjeModela } from '../enum/stanje_modela.enum';
import { CrudServiceForModel } from '../../service/crud.service-for-model';
import { Subject } from 'rxjs';
import {
  Form,
  FormRow,
  ROW_ITEM_TYPE,
  FormFieldInputNumberInterface,
  FORM_FIELD_INPUT_TYPE,
  SelectInterface,
  VALIDATOR_NAMES,
} from 'my-angular-form';
import { Validators } from '@angular/forms';
import { HeaderValue, Table } from 'my-angular-table';
import { Odeljenje } from '../entity/odeljenje.model';
import { Formable } from '../interface/formable.interface';
import { Tableable } from '../interface/tableable.interface';
import { ODELJENJE_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/odeljenje_service.service';
import { take } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { Adresa } from '../entity/adresa.model';
import { ADRESA_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/adresa_service.service';
import { TipOdeljenja } from '../enum/tip_odeljenja.enum';
import { Fakultet } from '../entity/fakultet.model';
import { FAKULTET_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/fakultet_service.service';
import { STUDENTSKA_SLUZBA_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/studentska_sluzba_service.service';
import { StudentskaSluzba } from '../entity/studentska_sluzba.model';

export class OdeljenjeFormableTableable implements Formable, Tableable {
  private _formSubject: Subject<Form> = new Subject();

  public getFormSubject = (): Subject<Form> => {
    return this._formSubject;
  };

  constructor(private _entitet: Odeljenje) {}

  public getForm = (
    csfm: CrudServiceForModel,
    isFormForCreation: boolean,
    isFormForDetails: boolean = false,
    isFormForUpdate: boolean = false,
    disableAllFields: boolean = false
  ): void => {
    // private id: number;+

    // private stanje: StanjeModela;+

    // private version: number;+

    // private tip: TipOdeljenja;+

    // private prostorije: Prostorija[];+

    // private adresa: Adresa;+

    // private fakultet: Fakultet;+

    // private studentskaSluzba: StudentskaSluzba;

    let form = new Form({
      showSubmitButton: !disableAllFields,
      submitButtonText: 'potvrdi',
      showCancelButton: !disableAllFields,
      cancelButtonText: 'odustani',
      showResetButton: isFormForCreation,
      resetButtonText: 'resetuj',
      submitOnlyIfFormValid: true,
    });

    if (!isFormForCreation) {
      let rowId = new FormRow();
      form.addChild(rowId);
      rowId.addChildInterface(<FormFieldInputNumberInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
        formFieldInputType: FORM_FIELD_INPUT_TYPE.NUMBER,
        controlName: 'id',
        labelName: 'Id',
        defaultValue: this._entitet.getId(),
        disabled: true,
      });

      // console.log(StanjeModela.AKTIVAN);
      // console.log(typeof StanjeModela.AKTIVAN);

      let rowStanjeModela = new FormRow();
      form.addChild(rowStanjeModela);
      rowStanjeModela.addChildInterface(<SelectInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
        controlName: 'stanje',
        labelName: 'Stanje modela',
        defaultValue: this._entitet.getStanje(),
        optionValues: [
          { value: StanjeModela.AKTIVAN, textToShow: 'AKTIVAN' },
          { value: StanjeModela.NEAKTIVAN, textToShow: 'NEAKTIVAN' },
        ],
        disabled: disableAllFields,
        validators: [
          {
            message: 'Stanje modela je obavezno',
            name: VALIDATOR_NAMES.REQUIRED,
            validatorFn: Validators.required,
          },
        ],
      });
    }

    let rowTipOdeljenja = new FormRow();
    form.addChild(rowTipOdeljenja);
    rowTipOdeljenja.addChildInterface(<SelectInterface>{
      type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
      controlName: 'tip',
      labelName: 'Tip odeljenja',
      defaultValue: this._entitet.getTip(),
      optionValues: [
        { value: TipOdeljenja.CENTAR, textToShow: TipOdeljenja.CENTAR },
        {
          value: TipOdeljenja.ISTURENO_ODELJENJE,
          textToShow: TipOdeljenja.ISTURENO_ODELJENJE,
        },
      ],
      disabled: disableAllFields,
      validators: [
        {
          message: 'Tip odeljenja je obavezan',
          name: VALIDATOR_NAMES.REQUIRED,
          validatorFn: Validators.required,
        },
      ],
    });

    let adresaService = csfm.getCrudServiceForModel<Adresa, number>(
      ADRESA_SERVICE_FOR_MODEL_INTERFACE
    );

    adresaService
      .findAll()
      .pipe(take(1))
      .subscribe(
        (entiteti: Adresa[]) => {
          for (let index = 0; index < entiteti.length; index++) {
            if (
              entiteti[index].getId().toString() ===
              this._entitet.getAdresa()?.getId().toString()
            ) {
              this._entitet.setAdresa(entiteti[index]);
              break;
            }
          }

          if (isFormForCreation) {
            entiteti = entiteti.filter((r) => !r.getUniverzitet());
          } else if (isFormForUpdate) {
            entiteti = entiteti.filter((r) => !r.getUniverzitet());
            entiteti.push(this._entitet.getAdresa());
          } else if (isFormForDetails) {
            if (
              this._entitet.getAdresa() &&
              !entiteti.find(
                (e) =>
                  e.getId().toString() ===
                  this._entitet.getAdresa().getId().toString()
              )
            )
              entiteti.push(this._entitet.getAdresa());
          }

          if (!(entiteti.length > 0)) {
            this._formSubject.error(
              new Error('Sve adrese vec pripadaju nekim odeljenjima')
            );
            return;
          }

          let rowEntitet = new FormRow();
          form.addChild(rowEntitet);
          rowEntitet.addChildInterface(<SelectInterface>{
            type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
            controlName: 'adresa',
            labelName: 'Adresa odeljenja',
            defaultValue: this._entitet.getAdresa(),
            optionValues: entiteti.map((d) => {
              return {
                value: d,
                textToShow:
                  'id: ' +
                  d.getId().toString() +
                  ', ' +
                  d.getUlica() +
                  ' ' +
                  d.getBroj(),
              };
            }),
            disabled: disableAllFields,
          });
          this._formSubject.next(form);
          // -------------------------------------
          let fakultetService = csfm.getCrudServiceForModel<Fakultet, number>(
            FAKULTET_SERVICE_FOR_MODEL_INTERFACE
          );

          fakultetService
            .findAll()
            .pipe(take(1))
            .subscribe(
              (entiteti: Fakultet[]) => {
                for (let index = 0; index < entiteti.length; index++) {
                  if (
                    entiteti[index].getId().toString() ===
                    this._entitet.getFakultet()?.getId().toString()
                  ) {
                    this._entitet.setFakultet(entiteti[index]);
                    break;
                  }
                }

                if (isFormForCreation) {
                  // entiteti = entiteti.filter((r) => !r.getUniverzitet());
                } else if (isFormForUpdate) {
                  // entiteti = entiteti.filter((r) => !r.getUniverzitet());
                  // entiteti.push(this._entitet.getFakultet());
                } else if (isFormForDetails) {
                  if (
                    this._entitet.getFakultet() &&
                    !entiteti.find(
                      (e) =>
                        e.getId().toString() ===
                        this._entitet.getFakultet().getId().toString()
                    )
                  )
                    entiteti.push(this._entitet.getFakultet());
                }

                // if (!(entiteti.length > 0)) {
                //   this._formSubject.error(
                //     new Error('Sve adrese vec pripadaju nekim odeljenjima')
                //   );
                //   return;
                // }

                let rowEntitet = new FormRow();
                form.addChild(rowEntitet);
                rowEntitet.addChildInterface(<SelectInterface>{
                  type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
                  controlName: 'fakultet',
                  labelName: 'Fakultet kojem odeljenje pripada',
                  defaultValue: this._entitet.getFakultet(),
                  optionValues: entiteti.map((d) => {
                    return {
                      value: d,
                      textToShow:
                        'id: ' + d.getId().toString() + ', ' + d.getNaziv(),
                    };
                  }),
                  disabled: disableAllFields,
                });
                this._formSubject.next(form);
                // -------------------------------------
                let studentskaSluzbaService = csfm.getCrudServiceForModel<
                  Fakultet,
                  number
                >(STUDENTSKA_SLUZBA_SERVICE_FOR_MODEL_INTERFACE);

                studentskaSluzbaService
                  .findAll()
                  .pipe(take(1))
                  .subscribe(
                    (entiteti: StudentskaSluzba[]) => {
                      for (let index = 0; index < entiteti.length; index++) {
                        if (
                          entiteti[index].getId().toString() ===
                          this._entitet
                            .getStudentskaSluzba()
                            ?.getId()
                            .toString()
                        ) {
                          this._entitet.setStudentskaSluzba(entiteti[index]);
                          break;
                        }
                      }

                      if (isFormForCreation) {
                        entiteti = entiteti.filter((r) => !r.getOdeljenje());
                      } else if (isFormForUpdate) {
                        entiteti = entiteti.filter((r) => !r.getOdeljenje());
                        entiteti.push(this._entitet.getStudentskaSluzba());
                      } else if (isFormForDetails) {
                        if (
                          this._entitet.getStudentskaSluzba() &&
                          !entiteti.find(
                            (e) =>
                              e.getId().toString() ===
                              this._entitet
                                .getStudentskaSluzba()
                                .getId()
                                .toString()
                          )
                        )
                          entiteti.push(this._entitet.getStudentskaSluzba());
                      }

                      if (!(entiteti.length > 0)) {
                        this._formSubject.error(
                          new Error(
                            'Sve studentske sluzbe vec pripadaju nekim odeljenjima'
                          )
                        );
                        return;
                      }

                      let rowEntitet = new FormRow();
                      form.addChild(rowEntitet);
                      rowEntitet.addChildInterface(<SelectInterface>{
                        type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
                        controlName: 'studentskaSluzba',
                        labelName: 'Studentska sluzba koja pripada odeljenju',
                        defaultValue: this._entitet.getStudentskaSluzba(),
                        optionValues: entiteti.map((d) => {
                          return {
                            value: d,
                            textToShow: 'id: ' + d.getId().toString(),
                          };
                        }),
                        disabled: disableAllFields,
                      });
                      this._formSubject.next(form);
                      this._formSubject.complete();
                    },
                    (err: HttpErrorResponse) => {
                      this._formSubject.error(err);
                    }
                  );
                // ------------------------------------------
              },
              (err: HttpErrorResponse) => {
                this._formSubject.error(err);
              }
            );
          // ------------------------------------------
        },
        (err: HttpErrorResponse) => {
          this._formSubject.error(err);
        }
      );
  };

  public save = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    let odeljenjeService = csfm.getCrudServiceForModel<Odeljenje, number>(
      ODELJENJE_SERVICE_FOR_MODEL_INTERFACE
    );
    odeljenjeService
      .save(entitetDict)
      .pipe(take(1))
      .subscribe(
        (entitet) => {},
        (err: HttpErrorResponse) => {
          subject.error(err);
        },
        () => {
          subject.complete();
        }
      );

    return subject;
  };

  public update = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    let odeljenjeService = csfm.getCrudServiceForModel<Odeljenje, number>(
      ODELJENJE_SERVICE_FOR_MODEL_INTERFACE
    );
    odeljenjeService
      .update(entitetDict)
      .pipe(take(1))
      .subscribe(
        (entitet) => {},
        (err: HttpErrorResponse) => {
          subject.error(err);
        },
        () => {
          subject.complete();
        }
      );

    return subject;
  };

  public getHeaderValue = (): HeaderValue => {
    return {
      headerItems: [
        { context: 'id' },
        { context: 'stanje modela' },
        { context: 'tip odeljenja' },
        { context: 'adresa' },
        { context: 'fakultet' },
        { context: 'studentska sluzba' },
        { context: '' },
        { context: '' },
        { context: '' },
      ],
    };
  };

  public addRowToTable = (
    table: Table,
    detaljiFn: Function = () => {},
    izmeniFn: Function = () => {},
    obrisiFn: Function = () => {}
  ): void => {
    try {
      table.addChildValue({
        data: this._entitet,
        rowItems: [
          {
            context: this._entitet.getId(),
          },
          {
            context: this._entitet.getStanje(),
          },
          {
            context: this._entitet.getTip() ? this._entitet.getTip() : '/',
          },
          {
            context: this._entitet.getAdresa()
              ? this._entitet.getAdresa().getUlica() +
                ' ' +
                this._entitet.getAdresa().getBroj()
              : '/',
          },
          {
            context: this._entitet.getFakultet()
              ? this._entitet.getFakultet().getNaziv()
              : '/',
          },
          {
            context: this._entitet.getStudentskaSluzba()
              ? this._entitet.getStudentskaSluzba().getId().toString()
              : '/',
          },
          {
            context: 'detalji',
            button: { function: detaljiFn },
          },
          {
            context: 'izmeni',
            button: { function: izmeniFn },
          },
          {
            context:
              this._entitet.getStanje() !== StanjeModela.OBRISAN
                ? 'obrisi'
                : '',
            button:
              this._entitet.getStanje() !== StanjeModela.OBRISAN
                ? { function: obrisiFn }
                : undefined,
          },
        ],
      });
    } catch (error) {
      console.log(error);
    }
  };
}
