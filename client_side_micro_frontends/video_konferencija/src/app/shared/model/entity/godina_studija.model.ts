import { StudentNaStudiji } from './student_na_studiji.model';
import { StudijskiProgram } from './studijski_program.model';
import { Konsultacija } from './konsultacija.model';
import { GodinaStudijaObavestenje } from './godina_studija_obavestenje.model';
import { Predmet } from './predmet.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';
import { Semestar } from '../enum/semestar.enum';
import { Identifikacija } from '../interface/identifikacija.interface';
import { CrudServiceForModel } from '../../service/crud.service-for-model';
import { Subject } from 'rxjs';
import {
  Form,
  FormRow,
  ROW_ITEM_TYPE,
  FormFieldInputNumberInterface,
  FORM_FIELD_INPUT_TYPE,
  SelectInterface,
  VALIDATOR_NAMES,
} from 'my-angular-form';
import { Validators } from '@angular/forms';
import { HeaderValue, Table } from 'my-angular-table';
import { XmlExportable } from '../../service/xml.service';

export class GodinaStudija
  implements Entitet<GodinaStudija, number>, XmlExportable {
  private id: number;

  private godina: Date;

  private stanje: StanjeModela;

  private version: number;

  private studentiNaStudiji: StudentNaStudiji[];

  private semestar: Semestar;

  private studijskiProgram: StudijskiProgram;

  private konsultacije: Konsultacija[];

  private godinaStudijaObavestenja: GodinaStudijaObavestenje[];

  private predmeti: Predmet[];

  private _formSubject: Subject<Form> = new Subject();

  public getFormSubject = (): Subject<Form> => {
    return this._formSubject;
  };

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getGodina = (): Date => {
    return this.godina;
  };

  public setGodina = (godina: Date): void => {
    this.godina = godina;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getStudentiNaStudiji = (): StudentNaStudiji[] => {
    return this.studentiNaStudiji;
  };

  public setStudentiNaStudiji = (
    studentiNaStudiji: StudentNaStudiji[]
  ): void => {
    this.studentiNaStudiji = studentiNaStudiji;
  };

  public getSemestar = (): Semestar => {
    return this.semestar;
  };

  public setSemestar = (semestar: Semestar): void => {
    this.semestar = semestar;
  };

  public getStudijskiProgram = (): StudijskiProgram => {
    return this.studijskiProgram;
  };

  public setStudijskiProgram = (studijskiProgram: StudijskiProgram): void => {
    this.studijskiProgram = studijskiProgram;
  };

  public getKonsultacije = (): Konsultacija[] => {
    return this.konsultacije;
  };

  public setKonsultacije = (konsultacije: Konsultacija[]): void => {
    this.konsultacije = konsultacije;
  };

  public getGodinaStudijaObavestenja = (): GodinaStudijaObavestenje[] => {
    return this.godinaStudijaObavestenja;
  };

  public setGodinaStudijaObavestenja = (
    godinaStudijaObavestenja: GodinaStudijaObavestenje[]
  ): void => {
    this.godinaStudijaObavestenja = godinaStudijaObavestenja;
  };

  public getPredmeti = (): Predmet[] => {
    return this.predmeti;
  };

  public setPredmeti = (predmeti: Predmet[]): void => {
    this.predmeti = predmeti;
  };

  public constructor() {
    this.studentiNaStudiji = [];
    this.konsultacije = [];
    this.godinaStudijaObavestenja = [];
    this.predmeti = [];
  }

  public getStudentNaStudiji(
    identifikacija: Identifikacija<number>
  ): StudentNaStudiji {
    throw new Error('Not Implmented');
  }

  public getKonsultacija(identifikacija: Identifikacija<number>): Konsultacija {
    throw new Error('Not Implmented');
  }

  public getGodinaStudijaObavestenje(
    identifikacija: Identifikacija<number>
  ): GodinaStudijaObavestenje {
    throw new Error('Not Implmented');
  }

  public getPredmet(identifikacija: Identifikacija<number>): Predmet {
    throw new Error('Not Implmented');
  }

  public convertToJSON = (
    shouldHaveOwnKey: boolean = true,
    rootKeyFromOutsideJSON?: string
  ): object => {
    // private id: number;+

    // private godina: Date;+

    // private stanje: StanjeModela;+

    // private version: number;-

    // private studentiNaStudiji: StudentNaStudiji[];+

    // private semestar: Semestar;+

    // private studijskiProgram: StudijskiProgram;+

    // private konsultacije: Konsultacija[];-

    // private godinaStudijaObavestenja: GodinaStudijaObavestenje[];-

    // private predmeti: Predmet[];-
    if (shouldHaveOwnKey) {
      return {
        id: this.id,
        stanje: this.stanje,
        godina: this.godina.toLocaleDateString(),
        semestar: this.semestar,
        studentiNaStudiji: this.studentiNaStudiji.map((s) =>
          rootKeyFromOutsideJSON &&
          rootKeyFromOutsideJSON === 'studentNaStudiji'
            ? ''
            : s
            ? s.convertToJSON(false, 'godinaStudija')
            : ''
        ),
        studijskiProgram:
          rootKeyFromOutsideJSON &&
          rootKeyFromOutsideJSON === 'studijskiProgram'
            ? ''
            : this.studijskiProgram
            ? this.studijskiProgram.convertToJSON(true, 'godinaStudija')
            : '',
      };
    } else {
      return {
        godinaStudija: {
          id: this.id,
          stanje: this.stanje,
          godina: this.godina.toLocaleDateString(),
          semestar: this.semestar,
          studentiNaStudiji: this.studentiNaStudiji.map((s) =>
            rootKeyFromOutsideJSON &&
            rootKeyFromOutsideJSON === 'studentNaStudiji'
              ? ''
              : s
              ? s.convertToJSON(false, 'godinaStudija')
              : ''
          ),
          studijskiProgram:
            rootKeyFromOutsideJSON &&
            rootKeyFromOutsideJSON === 'studijskiProgram'
              ? ''
              : this.studijskiProgram
              ? this.studijskiProgram.convertToJSON(true, 'godinaStudija')
              : '',
        },
      };
    }
  };
}
