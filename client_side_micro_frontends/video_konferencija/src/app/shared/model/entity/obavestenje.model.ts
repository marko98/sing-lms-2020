import { GodinaStudijaObavestenje } from './godina_studija_obavestenje.model';
import { Fajl } from './fajl.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';
import { Identifikacija } from '../interface/identifikacija.interface';
import { CrudServiceForModel } from '../../service/crud.service-for-model';
import { Subject } from 'rxjs';
import {
  Form,
  FormRow,
  ROW_ITEM_TYPE,
  FormFieldInputNumberInterface,
  FORM_FIELD_INPUT_TYPE,
  SelectInterface,
  VALIDATOR_NAMES,
} from 'my-angular-form';
import { Validators } from '@angular/forms';
import { HeaderValue, Table } from 'my-angular-table';

export class Obavestenje implements Entitet<Obavestenje, number> {
  private id: number;

  private naslov: string;

  private datum: Date;

  private sadrzaj: string;

  private stanje: StanjeModela;

  private version: number;

  private godineStudijaObavestenje: GodinaStudijaObavestenje[];

  private fajlovi: Fajl[];

  private _formSubject: Subject<Form> = new Subject();

  public getFormSubject = (): Subject<Form> => {
    return this._formSubject;
  };

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getNaslov = (): string => {
    return this.naslov;
  };

  public setNaslov = (naslov: string): void => {
    this.naslov = naslov;
  };

  public getDatum = (): Date => {
    return this.datum;
  };

  public setDatum = (datum: Date): void => {
    this.datum = datum;
  };

  public getSadrzaj = (): string => {
    return this.sadrzaj;
  };

  public setSadrzaj = (sadrzaj: string): void => {
    this.sadrzaj = sadrzaj;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getGodineStudijaObavestenje = (): GodinaStudijaObavestenje[] => {
    return this.godineStudijaObavestenje;
  };

  public setGodineStudijaObavestenje = (
    godineStudijaObavestenje: GodinaStudijaObavestenje[]
  ): void => {
    this.godineStudijaObavestenje = godineStudijaObavestenje;
  };

  public getFajlovi = (): Fajl[] => {
    return this.fajlovi;
  };

  public setFajlovi = (fajlovi: Fajl[]): void => {
    this.fajlovi = fajlovi;
  };

  public constructor() {
    this.godineStudijaObavestenje = [];
    this.fajlovi = [];
  }

  public getGodinaStudijaObavestenje(
    identifikacija: Identifikacija<number>
  ): GodinaStudijaObavestenje {
    throw new Error('Not Implmented');
  }

  public getFajl(identifikacija: Identifikacija<number>): Fajl {
    throw new Error('Not Implmented');
  }

  public getForm = (
    csfm: CrudServiceForModel,
    isFormForCreation: boolean,
    disableAllFields: boolean = false
  ): Subject<Form> => {
    let subject: Subject<Form> = new Subject();

    let form = new Form({
      showSubmitButton: !disableAllFields,
      submitButtonText: 'potvrdi',
      showCancelButton: !disableAllFields,
      cancelButtonText: 'odustani',
      showResetButton: isFormForCreation,
      resetButtonText: 'resetuj',
      submitOnlyIfFormValid: true,
    });

    if (!isFormForCreation) {
      let rowId = new FormRow();
      form.addChild(rowId);
      rowId.addChildInterface(<FormFieldInputNumberInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
        formFieldInputType: FORM_FIELD_INPUT_TYPE.NUMBER,
        controlName: 'id',
        labelName: 'Id',
        defaultValue: this.id,
        disabled: true,
      });

      // console.log(StanjeModela.AKTIVAN);
      // console.log(typeof StanjeModela.AKTIVAN);

      let rowStanjeModela = new FormRow();
      form.addChild(rowStanjeModela);
      rowStanjeModela.addChildInterface(<SelectInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
        controlName: 'stanje',
        labelName: 'Stanje modela',
        defaultValue: this.stanje,
        optionValues: [
          { value: StanjeModela.AKTIVAN, textToShow: 'AKTIVAN' },
          { value: StanjeModela.NEAKTIVAN, textToShow: 'NEAKTIVAN' },
        ],
        disabled: disableAllFields,
        validators: [
          {
            message: 'Stanje modela je obavezno',
            name: VALIDATOR_NAMES.REQUIRED,
            validatorFn: Validators.required,
          },
        ],
      });
    }

    return subject;
  };

  public save = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    // let gradService = csfm.getCrudServiceForModel<Grad, number>(
    //   GRAD_SERVICE_FOR_MODEL_INTERFACE
    // );
    // gradService
    //   .save(entitetDict)
    //   .pipe(take(1))
    //   .subscribe(
    //     (entitet) => {},
    //     (err: HttpErrorResponse) => {
    //       subject.error(err);
    //     },
    //     () => {
    //       subject.complete();
    //     }
    //   );

    return subject;
  };

  public update = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    // let gradService = csfm.getCrudServiceForModel<Grad, number>(
    //   GRAD_SERVICE_FOR_MODEL_INTERFACE
    // );
    // gradService
    //   .update(entitetDict)
    //   .pipe(take(1))
    //   .subscribe(
    //     (entitet) => {},
    //     (err: HttpErrorResponse) => {
    //       subject.error(err);
    //     },
    //     () => {
    //       subject.complete();
    //     }
    //   );

    return subject;
  };

  public getHeaderValue = (): HeaderValue => {
    return {
      headerItems: [
        { context: 'id' },
        { context: 'stanje modela' },
        // { context: 'naziv' },
        // { context: 'drzava' },
        { context: '' },
        { context: '' },
        { context: '' },
      ],
    };
  };

  public addRowToTable = (
    table: Table,
    detaljiFn: Function = () => {},
    izmeniFn: Function = () => {},
    obrisiFn: Function = () => {}
  ): void => {
    try {
      table.addChildValue({
        data: this,
        rowItems: [
          {
            context: this.id,
          },
          {
            context: this.stanje,
          },
          // {
          //   context: this.naziv,
          // },
          // {
          //   context: this.drzava ? this.drzava.getNaziv() : '/',
          // },
          // {
          //   context: 'detalji',
          //   button: { function: detaljiFn },
          // },
          // {
          //   context: 'izmeni',
          //   button: { function: izmeniFn },
          // },
          // {
          //   context: this.stanje !== StanjeModela.OBRISAN ? 'obrisi' : '',
          //   button:
          //     this.stanje !== StanjeModela.OBRISAN
          //       ? { function: obrisiFn }
          //       : undefined,
          // },
        ],
      });
    } catch (error) {
      console.log(error);
    }
  };
}
