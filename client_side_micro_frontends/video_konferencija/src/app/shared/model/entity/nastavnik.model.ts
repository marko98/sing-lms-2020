import { Univerzitet } from './univerzitet.model';
import { Fakultet } from './fakultet.model';
import { NastavnikFakultet } from './nastavnik_fakultet.model';
import { TerminNastave } from './termin_nastave.model';
import { StudijskiProgram } from './studijski_program.model';
import { Titula } from './titula.model';
import { RegistrovaniKorisnik } from './registrovani_korisnik.model';
import { DiplomskiRad } from './diplomski_rad.model';
import { IstrazivackiRad } from './istrazivacki_rad.model';
import { Konsultacija } from './konsultacija.model';
import { NastavnikEvaluacijaZnanja } from './nastavnik_evaluacija_znanja.model';
import { NastavnikDiplomskiRad } from './nastavnik_diplomski_rad.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';
import { Identifikacija } from '../interface/identifikacija.interface';
import { NastavnikNaRealizaciji } from './nastavnik_na_realizaciji.model';
import { CrudServiceForModel } from '../../service/crud.service-for-model';
import { Subject } from 'rxjs';
import {
  Form,
  FormRow,
  ROW_ITEM_TYPE,
  FormFieldInputNumberInterface,
  FORM_FIELD_INPUT_TYPE,
  SelectInterface,
  VALIDATOR_NAMES,
} from 'my-angular-form';
import { Validators } from '@angular/forms';
import { HeaderValue, Table } from 'my-angular-table';
import { XmlExportable } from '../../service/xml.service';

export class Nastavnik implements Entitet<Nastavnik, number>, XmlExportable {
  private id: number;

  private biografija: string;

  private stanje: StanjeModela;

  private version: number;

  private univerziteti: Univerzitet[];

  private fakulteti: Fakultet[];

  private nastavnikFakulteti: NastavnikFakultet[];

  private terminiNastave: TerminNastave[];

  private studijskiProgrami: StudijskiProgram[];

  private titule: Titula[];

  private registrovaniKorisnik: RegistrovaniKorisnik;

  private mentorDiplomskiRadovi: DiplomskiRad[];

  private istrazivackiRadovi: IstrazivackiRad[];

  private konsultacije: Konsultacija[];

  private nastavnikEvaluacijeZnanja: NastavnikEvaluacijaZnanja[];

  private nastavnikDiplomskiRadovi: NastavnikDiplomskiRad[];

  private nastavnikNaRealizacijama: NastavnikNaRealizaciji[];

  private _formSubject: Subject<Form> = new Subject();

  public getFormSubject = (): Subject<Form> => {
    return this._formSubject;
  };

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getBiografija = (): string => {
    return this.biografija;
  };

  public setBiografija = (biografija: string): void => {
    this.biografija = biografija;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getUniverziteti = (): Univerzitet[] => {
    return this.univerziteti;
  };

  public setUniverziteti = (univerziteti: Univerzitet[]): void => {
    this.univerziteti = univerziteti;
  };

  public getFakulteti = (): Fakultet[] => {
    return this.fakulteti;
  };

  public setFakulteti = (fakulteti: Fakultet[]): void => {
    this.fakulteti = fakulteti;
  };

  public getNastavnikFakulteti = (): NastavnikFakultet[] => {
    return this.nastavnikFakulteti;
  };

  public setNastavnikFakulteti = (
    nastavnikFakulteti: NastavnikFakultet[]
  ): void => {
    this.nastavnikFakulteti = nastavnikFakulteti;
  };

  public getTerminiNastave = (): TerminNastave[] => {
    return this.terminiNastave;
  };

  public setTerminiNastave = (terminiNastave: TerminNastave[]): void => {
    this.terminiNastave = terminiNastave;
  };

  public getStudijskiProgrami = (): StudijskiProgram[] => {
    return this.studijskiProgrami;
  };

  public setStudijskiProgrami = (
    studijskiProgrami: StudijskiProgram[]
  ): void => {
    this.studijskiProgrami = studijskiProgrami;
  };

  public getTitule = (): Titula[] => {
    return this.titule;
  };

  public setTitule = (titule: Titula[]): void => {
    this.titule = titule;
  };

  public getRegistrovaniKorisnik = (): RegistrovaniKorisnik => {
    return this.registrovaniKorisnik;
  };

  public setRegistrovaniKorisnik = (
    registrovaniKorisnik: RegistrovaniKorisnik
  ): void => {
    this.registrovaniKorisnik = registrovaniKorisnik;
  };

  public getMentorDiplomskiRadovi = (): DiplomskiRad[] => {
    return this.mentorDiplomskiRadovi;
  };

  public setMentorDiplomskiRadovi = (
    mentorDiplomskiRadovi: DiplomskiRad[]
  ): void => {
    this.mentorDiplomskiRadovi = mentorDiplomskiRadovi;
  };

  public getIstrazivackiRadovi = (): IstrazivackiRad[] => {
    return this.istrazivackiRadovi;
  };

  public setIstrazivackiRadovi = (
    istrazivackiRadovi: IstrazivackiRad[]
  ): void => {
    this.istrazivackiRadovi = istrazivackiRadovi;
  };

  public getKonsultacije = (): Konsultacija[] => {
    return this.konsultacije;
  };

  public setKonsultacije = (konsultacije: Konsultacija[]): void => {
    this.konsultacije = konsultacije;
  };

  public getNastavnikEvaluacijeZnanja = (): NastavnikEvaluacijaZnanja[] => {
    return this.nastavnikEvaluacijeZnanja;
  };

  public setNastavnikEvaluacijeZnanja = (
    nastavnikEvaluacijeZnanja: NastavnikEvaluacijaZnanja[]
  ): void => {
    this.nastavnikEvaluacijeZnanja = nastavnikEvaluacijeZnanja;
  };

  public getNastavnikDiplomskiRadovi = (): NastavnikDiplomskiRad[] => {
    return this.nastavnikDiplomskiRadovi;
  };

  public setNastavnikDiplomskiRadovi = (
    nastavnikDiplomskiRadovi: NastavnikDiplomskiRad[]
  ): void => {
    this.nastavnikDiplomskiRadovi = nastavnikDiplomskiRadovi;
  };

  public getNastavnikNaRealizacijama = (): NastavnikNaRealizaciji[] => {
    return this.nastavnikNaRealizacijama;
  };

  public setNastavnikNaRealizacijama = (
    nastavnikNaRealizacijama: NastavnikNaRealizaciji[]
  ): void => {
    this.nastavnikNaRealizacijama = nastavnikNaRealizacijama;
  };

  public constructor() {
    this.univerziteti = [];
    this.fakulteti = [];
    this.nastavnikFakulteti = [];
    this.terminiNastave = [];
    this.studijskiProgrami = [];
    this.titule = [];
    this.mentorDiplomskiRadovi = [];
    this.istrazivackiRadovi = [];
    this.konsultacije = [];
    this.nastavnikEvaluacijeZnanja = [];
    this.nastavnikDiplomskiRadovi = [];
    this.nastavnikNaRealizacijama = [];
  }

  public getTitula(identifikacija: Identifikacija<number>): Titula {
    throw new Error('Not Implmented');
  }

  public getNastavnikFakultet(
    identifikacija: Identifikacija<number>
  ): NastavnikFakultet {
    throw new Error('Not Implmented');
  }

  public getMentorDiplomskiRad(
    identifikacija: Identifikacija<number>
  ): DiplomskiRad {
    throw new Error('Not Implmented');
  }

  public getIstrazivackiRad(
    identifikacija: Identifikacija<number>
  ): IstrazivackiRad {
    throw new Error('Not Implmented');
  }

  public getKonsultacija(identifikacija: Identifikacija<number>): Konsultacija {
    throw new Error('Not Implmented');
  }

  public getNastavnikEvaluacijaZnanja(
    identifikacija: Identifikacija<number>
  ): NastavnikEvaluacijaZnanja {
    throw new Error('Not Implmented');
  }

  public getTerminNastave(
    identifikacija: Identifikacija<number>
  ): TerminNastave {
    throw new Error('Not Implmented');
  }

  public getNastavnikDiplomskiRad(
    identifikacija: Identifikacija<number>
  ): NastavnikDiplomskiRad {
    throw new Error('Not Implmented');
  }

  public getNastavnikNaRealizaciji(
    identifikacija: Identifikacija<number>
  ): NastavnikNaRealizaciji {
    throw new Error('Not Implmented');
  }

  public convertToJSON = (
    shouldHaveOwnKey: boolean = true,
    rootKeyFromOutsideJSON?: string
  ): object => {
    // private id: number;+

    // private biografija: string;+

    // private stanje: StanjeModela;+

    // private version: number;-

    // private univerziteti: Univerzitet[];

    // private fakulteti: Fakultet[];

    // private nastavnikFakulteti: NastavnikFakultet[];

    // private terminiNastave: TerminNastave[];

    // private studijskiProgrami: StudijskiProgram[];

    // private titule: Titula[];+

    // private registrovaniKorisnik: RegistrovaniKorisnik;+

    // private mentorDiplomskiRadovi: DiplomskiRad[];

    // private istrazivackiRadovi: IstrazivackiRad[];

    // private konsultacije: Konsultacija[];

    // private nastavnikEvaluacijeZnanja: NastavnikEvaluacijaZnanja[];

    // private nastavnikDiplomskiRadovi: NastavnikDiplomskiRad[];

    // private nastavnikNaRealizacijama: NastavnikNaRealizaciji[];

    if (shouldHaveOwnKey) {
      return {
        id: this.id,
        stanje: this.stanje,
        biografija: this.biografija,
        registrovaniKorisnik:
          rootKeyFromOutsideJSON &&
          rootKeyFromOutsideJSON === 'registrovaniKorisnik'
            ? ''
            : this.registrovaniKorisnik
            ? this.registrovaniKorisnik.convertToJSON(true, 'nastavnik')
            : '',
        titule: this.titule.map((t) =>
          rootKeyFromOutsideJSON && rootKeyFromOutsideJSON === 'titula'
            ? ''
            : t
            ? t.convertToJSON(false, 'nastavnik')
            : ''
        ),
      };
    } else {
      return {
        nastavnik: {
          id: this.id,
          stanje: this.stanje,
          biografija: this.biografija,
          registrovaniKorisnik:
            rootKeyFromOutsideJSON &&
            rootKeyFromOutsideJSON === 'registrovaniKorisnik'
              ? ''
              : this.registrovaniKorisnik
              ? this.registrovaniKorisnik.convertToJSON(true, 'nastavnik')
              : '',
          titule: this.titule.map((t) =>
            rootKeyFromOutsideJSON && rootKeyFromOutsideJSON === 'titula'
              ? ''
              : t
              ? t.convertToJSON(false, 'nastavnik')
              : ''
          ),
        },
      };
    }
  };
}
