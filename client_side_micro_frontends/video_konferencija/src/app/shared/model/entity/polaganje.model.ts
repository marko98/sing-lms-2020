import { PohadjanjePredmeta } from './pohadjanje_predmeta.model';
import { EvaluacijaZnanja } from './evaluacija_znanja.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';

import { CrudServiceForModel } from '../../service/crud.service-for-model';
import { Subject } from 'rxjs';
import {
  Form,
  FormRow,
  ROW_ITEM_TYPE,
  FormFieldInputNumberInterface,
  FORM_FIELD_INPUT_TYPE,
  SelectInterface,
  VALIDATOR_NAMES,
} from 'my-angular-form';
import { Validators } from '@angular/forms';
import { HeaderValue, Table } from 'my-angular-table';
import { XmlExportable } from '../../service/xml.service';
export class Polaganje implements Entitet<Polaganje, number>, XmlExportable {
  private id: number;

  private bodovi: number;

  private napomena: string;

  private stanje: StanjeModela;

  private version: number;

  private pohadjanjePredmeta: PohadjanjePredmeta;

  private evaluacijaZnanja: EvaluacijaZnanja;

  private _formSubject: Subject<Form> = new Subject();

  public getFormSubject = (): Subject<Form> => {
    return this._formSubject;
  };

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getBodovi = (): number => {
    return this.bodovi;
  };

  public setBodovi = (bodovi: number): void => {
    this.bodovi = bodovi;
  };

  public getNapomena = (): string => {
    return this.napomena;
  };

  public setNapomena = (napomena: string): void => {
    this.napomena = napomena;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getPohadjanjePredmeta = (): PohadjanjePredmeta => {
    return this.pohadjanjePredmeta;
  };

  public setPohadjanjePredmeta = (
    pohadjanjePredmeta: PohadjanjePredmeta
  ): void => {
    this.pohadjanjePredmeta = pohadjanjePredmeta;
  };

  public getEvaluacijaZnanja = (): EvaluacijaZnanja => {
    return this.evaluacijaZnanja;
  };

  public setEvaluacijaZnanja = (evaluacijaZnanja: EvaluacijaZnanja): void => {
    this.evaluacijaZnanja = evaluacijaZnanja;
  };

  public constructor() {}

  public convertToJSON = (
    shouldHaveOwnKey: boolean = true,
    rootKeyFromOutsideJSON?: string
  ): object => {
    // private id: number;+

    // private bodovi: number;+

    // private napomena: string;+

    // private stanje: StanjeModela;+

    // private version: number;-

    // private pohadjanjePredmeta: PohadjanjePredmeta;+

    // private evaluacijaZnanja: EvaluacijaZnanja;+

    if (shouldHaveOwnKey) {
      return {
        id: this.id,
        stanje: this.stanje,
        bodovi: this.bodovi,
        napomena: this.napomena,
        evaluacijaZnanja:
          rootKeyFromOutsideJSON &&
          rootKeyFromOutsideJSON === 'evaluacijaZnanja'
            ? ''
            : this.evaluacijaZnanja
            ? this.evaluacijaZnanja.convertToJSON(true, 'polaganje')
            : '',
        pohadjanjePredmeta:
          rootKeyFromOutsideJSON &&
          rootKeyFromOutsideJSON === 'pohadjanjePredmeta'
            ? ''
            : this.pohadjanjePredmeta
            ? this.pohadjanjePredmeta.convertToJSON(true, 'polaganje')
            : '',
      };
    } else {
      return {
        polaganje: {
          id: this.id,
          stanje: this.stanje,
          bodovi: this.bodovi,
          napomena: this.napomena,
          evaluacijaZnanja:
            rootKeyFromOutsideJSON &&
            rootKeyFromOutsideJSON === 'evaluacijaZnanja'
              ? ''
              : this.evaluacijaZnanja
              ? this.evaluacijaZnanja.convertToJSON(true, 'polaganje')
              : '',
          pohadjanjePredmeta:
            rootKeyFromOutsideJSON &&
            rootKeyFromOutsideJSON === 'pohadjanjePredmeta'
              ? ''
              : this.pohadjanjePredmeta
              ? this.pohadjanjePredmeta.convertToJSON(true, 'polaganje')
              : '',
        },
      };
    }
  };
}
