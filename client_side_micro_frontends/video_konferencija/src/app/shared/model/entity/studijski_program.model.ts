import { Fakultet } from './fakultet.model';
import { Nastavnik } from './nastavnik.model';
import { GodinaStudija } from './godina_studija.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';
import { Identifikacija } from '../interface/identifikacija.interface';
import { CrudServiceForModel } from '../../service/crud.service-for-model';
import { Subject } from 'rxjs';
import {
  Form,
  FormRow,
  ROW_ITEM_TYPE,
  FormFieldInputNumberInterface,
  FORM_FIELD_INPUT_TYPE,
  SelectInterface,
  VALIDATOR_NAMES,
} from 'my-angular-form';
import { Validators } from '@angular/forms';
import { HeaderValue, Table } from 'my-angular-table';
import { XmlExportable } from '../../service/xml.service';

export class StudijskiProgram
  implements Entitet<StudijskiProgram, number>, XmlExportable {
  private id: number;

  private naziv: string;

  private brojGodinaStudija: number;

  private stanje: StanjeModela;

  private version: number;

  private fakultet: Fakultet;

  private rukovodilac: Nastavnik;

  private godineStudija: GodinaStudija[];

  private opis: string;

  private _formSubject: Subject<Form> = new Subject();

  public getFormSubject = (): Subject<Form> => {
    return this._formSubject;
  };

  public getOpis = (): string => {
    return this.opis;
  };

  public setOpis = (opis: string): void => {
    this.opis = opis;
  };

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getNaziv = (): string => {
    return this.naziv;
  };

  public setNaziv = (naziv: string): void => {
    this.naziv = naziv;
  };

  public getBrojGodinaStudija = (): number => {
    return this.brojGodinaStudija;
  };

  public setBrojGodinaStudija = (brojGodinaStudija: number): void => {
    this.brojGodinaStudija = brojGodinaStudija;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getFakultet = (): Fakultet => {
    return this.fakultet;
  };

  public setFakultet = (fakultet: Fakultet): void => {
    this.fakultet = fakultet;
  };

  public getRukovodilac = (): Nastavnik => {
    return this.rukovodilac;
  };

  public setRukovodilac = (rukovodilac: Nastavnik): void => {
    this.rukovodilac = rukovodilac;
  };

  public getGodineStudija = (): GodinaStudija[] => {
    return this.godineStudija;
  };

  public setGodineStudija = (godineStudija: GodinaStudija[]): void => {
    this.godineStudija = godineStudija;
  };

  public constructor() {
    this.godineStudija = [];
  }

  public getGodinaStudija(
    identifikacija: Identifikacija<number>
  ): GodinaStudija {
    throw new Error('Not Implmented');
  }

  public convertToJSON = (
    shouldHaveOwnKey: boolean = true,
    rootKeyFromOutsideJSON?: string
  ): object => {
    // private id: number;+

    // private naziv: string;+

    // private brojGodinaStudija: number;+

    // private stanje: StanjeModela;+

    // private version: number;-

    // private fakultet: Fakultet;+

    // private rukovodilac: Nastavnik;

    // private godineStudija: GodinaStudija[];+

    // private opis: string;
    if (shouldHaveOwnKey) {
      return {
        id: this.id,
        stanje: this.stanje,
        naziv: this.naziv,
        brojGodinaStudija: this.brojGodinaStudija,
        godineStudija: this.godineStudija.map((s) =>
          rootKeyFromOutsideJSON && rootKeyFromOutsideJSON === 'godinaStudija'
            ? ''
            : s
            ? s.convertToJSON(false, 'studijskiProgram')
            : ''
        ),
        fakultet:
          rootKeyFromOutsideJSON && rootKeyFromOutsideJSON === 'fakultet'
            ? ''
            : this.fakultet
            ? this.fakultet.convertToJSON(true, 'studijskiProgram')
            : '',
      };
    } else {
      return {
        studijskiProgram: {
          id: this.id,
          stanje: this.stanje,
          naziv: this.naziv,
          brojGodinaStudija: this.brojGodinaStudija,
          godineStudija: this.godineStudija.map((s) =>
            rootKeyFromOutsideJSON && rootKeyFromOutsideJSON === 'godinaStudija'
              ? ''
              : s
              ? s.convertToJSON(false, 'studijskiProgram')
              : ''
          ),
          fakultet:
            rootKeyFromOutsideJSON && rootKeyFromOutsideJSON === 'fakultet'
              ? ''
              : this.fakultet
              ? this.fakultet.convertToJSON(true, 'studijskiProgram')
              : '',
        },
      };
    }
  };
}
