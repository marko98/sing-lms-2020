import { Form } from 'my-angular-form';
import { CrudServiceForModel } from '../../service/crud.service-for-model';
import { Subject } from 'rxjs';

export declare interface Formable {
  getForm(
    csfm: CrudServiceForModel,
    isFormForCreation: boolean,
    isFormForDetails?: boolean,
    isFormForUpdate?: boolean,
    disableAllFields?: boolean
  ): void;
  getFormSubject(): Subject<Form>;

  save(csfm: CrudServiceForModel, entitetDict: any): Subject<void>;
  update(csfm: CrudServiceForModel, entitetDict: any): Subject<void>;
}
