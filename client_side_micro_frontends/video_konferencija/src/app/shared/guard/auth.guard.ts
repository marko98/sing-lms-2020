import { Injectable } from '@angular/core';
import {
  CanActivate,
  CanActivateChild,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService, AuthUserModelInterface } from '../service/auth.service';
import { take, map } from 'rxjs/operators';
import { MICRO_FRONTENDS_PREFIX } from '../const';
import { UIService } from '../service/ui.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate, CanActivateChild {
  constructor(
    private _authService: AuthService,
    private _router: Router,
    private _uiService: UIService
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    // console.log('CanActivate ', next.data.requiredRoles);
    return this._authService.getUser().pipe(
      take(1),
      map((authUser: AuthUserModelInterface) => {
        // console.log(authUser);
        if (authUser) {
          const requiredRoles = next.data.requiredRoles;
          // console.log(requiredRoles);
          // console.log(authUser.role);
          // console.log(this.intersection(requiredRoles, authUser.role).size > 0);
          if (this.intersection(requiredRoles, authUser.role).size > 0) {
            return true;
          } else {
            this._uiService.showSnackbar('Zabranjen pristup', null, 1000);
            return false;
          }
        } else {
          return this._router.createUrlTree([
            MICRO_FRONTENDS_PREFIX.MF_4,
            'login',
          ]);
        }
      })
    );
  }
  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    // console.log('canActivateChild ', next.data.requiredRoles);
    return this._authService.getUser().pipe(
      take(1),
      map((authUser: AuthUserModelInterface) => {
        // console.log(authUser);
        if (authUser) {
          const requiredRoles = next.data.requiredRoles;
          if (this.intersection(requiredRoles, authUser.role).size > 0) {
            return true;
          } else {
            return false;
          }
        } else {
          return this._router.createUrlTree([
            MICRO_FRONTENDS_PREFIX.MF_4,
            'login',
          ]);
        }
      })
    );
  }

  private intersection = (a: Set<any>, b: Set<any>): Set<any> => {
    return new Set([...a].filter((x) => b.has(x)));
  };

  private diference = (a: Set<any>, b: Set<any>): Set<any> => {
    return new Set([...a].filter((x) => !b.has(x)));
  };
}
