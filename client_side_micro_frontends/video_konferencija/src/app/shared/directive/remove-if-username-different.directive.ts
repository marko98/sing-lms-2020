import { Directive, ElementRef, Input, OnInit } from '@angular/core';
import { AuthService, AuthUserModelInterface } from '../service/auth.service';
import { take } from 'rxjs/operators';

@Directive({
  selector: '[appRemoveIfUsernameDifferent]',
})
export class RemoveIfUsernameDifferentDirective implements OnInit {
  @Input() public username: string;

  constructor(private _elRef: ElementRef, private _authService: AuthService) {}

  ngOnInit() {
    console.log(this.username);
    this._authService
      .getUser()
      .pipe(take(1))
      .subscribe((authUser: AuthUserModelInterface) => {
        if (authUser) {
          if (authUser.korisnickoIme !== this.username) {
            (<HTMLElement>this._elRef.nativeElement).parentNode.removeChild(
              this._elRef.nativeElement
            );
          }
        }
      });
  }
}
