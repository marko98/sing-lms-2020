import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MaterialModule } from './material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MyAngularFormModule } from 'my-angular-form';
import { MyAngularTableModule } from 'my-angular-table';
import { RemoveDirective } from './directive/remove.directive';
import { AssetUrlPipe } from './pipe/asset-url-pipe.pipe';
import { StreamingComponent } from '../streaming/streaming.component';
import { ViewingComponent } from '../viewing/viewing.component';
import { ModelObrisanDirective } from './directive/model-obrisan.directive';
import { SanducadiComponent } from '../sanducadi/sanducadi.component';
import { SanduceComponent } from '../sanducadi/sanduce/sanduce.component';
import { PozivComponent } from '../sanducadi/poziv/poziv.component';
import { PrijemnoSanduceComponent } from '../prijemno-sanduce/prijemno-sanduce.component';
import { RemoveIfUsernameDifferentDirective } from './directive/remove-if-username-different.directive';

@NgModule({
  declarations: [
    RemoveDirective,
    ModelObrisanDirective,
    AssetUrlPipe,
    StreamingComponent,
    ViewingComponent,
    SanducadiComponent,
    SanduceComponent,
    PozivComponent,
    PrijemnoSanduceComponent,
    RemoveIfUsernameDifferentDirective,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MaterialModule,
    FlexLayoutModule,
    DragDropModule,
    MyAngularFormModule,
    MyAngularTableModule,
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MaterialModule,
    FlexLayoutModule,
    DragDropModule,
    MyAngularFormModule,
    MyAngularTableModule,

    RemoveDirective,
    ModelObrisanDirective,
    RemoveIfUsernameDifferentDirective,
    AssetUrlPipe,

    StreamingComponent,
    ViewingComponent,
    SanducadiComponent,
    SanduceComponent,
    PozivComponent,
    PrijemnoSanduceComponent,
  ],
})
export class SharedModule {}
