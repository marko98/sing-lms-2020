import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmptyRouteComponent } from './empty-route/empty-route.component';
import { APP_BASE_HREF } from '@angular/common';
import { StreamingComponent } from './streaming/streaming.component';
import { ViewingComponent } from './viewing/viewing.component';
import { SanducadiComponent } from './sanducadi/sanducadi.component';
import { AuthGuard } from './shared/guard/auth.guard';
import { ROLES } from './shared/const';
import { PrijemnoSanduceComponent } from './prijemno-sanduce/prijemno-sanduce.component';

const routes: Routes = [
  {
    path: 'mf_6',
    data: {
      requiredRoles: [],
    },
    children: [
      {
        path: 'poruke',
        pathMatch: 'full',
        data: {
          requiredRoles: [ROLES.ROLE_REGISTROVANI_KORISNIK],
        },
        component: SanducadiComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'prijemno_sanduce',
        pathMatch: 'full',
        data: {
          requiredRoles: [ROLES.ROLE_REGISTROVANI_KORISNIK],
        },
        component: PrijemnoSanduceComponent,
        canActivate: [AuthGuard],
      },
      { path: 'streaming', pathMatch: 'full', component: StreamingComponent },
      { path: 'viewing', pathMatch: 'full', component: ViewingComponent },
      { path: '**', redirectTo: 'prijemno_sanduce' },
    ],
  },
  { path: '**', component: EmptyRouteComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [{ provide: APP_BASE_HREF, useValue: '' }],
})
export class AppRoutingModule {}
