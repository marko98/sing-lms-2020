import { Injectable, InjectionToken, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UIService } from './ui.service';
import { CrudService } from './crud.service';
import { Entitet } from '../model/interface/entitet.interface';
import { CRUD_SERVICE_FOR_MODEL_INTERFACE } from './crud.service';

@Injectable({ providedIn: 'root' })
export class CrudServiceForModel {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {}

  public getCrudServiceForModel<T extends Entitet<T, ID>, ID>(
    csfmi: CRUD_SERVICE_FOR_MODEL_INTERFACE
  ): CrudService<T, ID> {
    const CRUD_SERVICE_TOKEN = new InjectionToken<CrudService<T, ID>>(
      'Manually constructed CrudService',
      {
        factory: () =>
          new CrudService<T, ID>(
            this._httpClient,
            this._uiService,
            csfmi.factory,
            csfmi.microserviceName,
            csfmi.path,
            csfmi.microservicePort,
            csfmi.wsPath
          ),
      }
    );

    const injector = Injector.create({
      providers: [
        {
          provide: CRUD_SERVICE_TOKEN,
          useFactory: () =>
            new CrudService<T, ID>(
              this._httpClient,
              this._uiService,
              csfmi.factory,
              csfmi.microserviceName,
              csfmi.path,
              csfmi.microservicePort,
              csfmi.wsPath
            ),
        },
      ],
    });

    return injector.get(CRUD_SERVICE_TOKEN);
  }
}
