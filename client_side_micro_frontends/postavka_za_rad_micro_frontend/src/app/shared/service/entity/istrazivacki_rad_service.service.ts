import { CrudService } from '../crud.service';
import {
  PATH,
  WS_PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
} from '../../const';
import { Injectable } from '@angular/core';
import { IstrazivackiRad } from '../../model/entity/istrazivacki_rad.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { IstrazivackiRadFactory } from '../../model/patterns/creational/factory_method/istrazivacki_rad.factory';
import { CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';

export const ISTRAZIVACKI_RAD_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: IstrazivackiRadFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.STUDENT,
  path: PATH.ISTRAZIVACKI_RAD,
  microservicePort: MICROSERVICE_PORT.STUDENT,
  wsPath: WS_PATH.ISTRAZIVACKI_RAD,
};
@Injectable({ providedIn: 'root' })
export class IstrazivackiRadService extends CrudService<
  IstrazivackiRad,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      IstrazivackiRadFactory.getInstance(),
      MICROSERVICE_NAME.STUDENT,
      PATH.ISTRAZIVACKI_RAD,
      MICROSERVICE_PORT.STUDENT,
      WS_PATH.ISTRAZIVACKI_RAD
    );
    this._findAll();
  }
}
