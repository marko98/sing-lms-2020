import {
  PATH,
  WS_PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
} from '../../const';
import { Injectable } from '@angular/core';
import { CrudService, CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';
import { Administrator } from '../../model/entity/administrator.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { AdministratorFactory } from '../../model/patterns/creational/factory_method/administrator.factory';

export const ADMINISTRATOR_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: AdministratorFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.ADMINISTRACIJA,
  path: PATH.ADMINISTRATOR,
  microservicePort: MICROSERVICE_PORT.ADMINISTRACIJA,
  wsPath: WS_PATH.ADMINISTRATOR,
};

@Injectable({ providedIn: 'root' })
export class AdministratorService extends CrudService<Administrator, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      AdministratorFactory.getInstance(),
      MICROSERVICE_NAME.ADMINISTRACIJA,
      PATH.ADMINISTRATOR,
      MICROSERVICE_PORT.ADMINISTRACIJA,
      WS_PATH.ADMINISTRATOR
    );
    this._findAll();
  }
}
