import { CrudService, CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';
import {
  PATH,
  WS_PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
} from '../../const';
import { Injectable } from '@angular/core';
import { Drzava } from '../../model/entity/drzava.model';

import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { DrzavaFactory } from '../../model/patterns/creational/factory_method/drzava.factory';

export const DRZAVA_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: DrzavaFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.ADRESA,
  path: PATH.DRZAVA,
  microservicePort: MICROSERVICE_PORT.ADRESA,
  wsPath: WS_PATH.DRZAVA,
};

@Injectable({ providedIn: 'root' })
export class DrzavaService extends CrudService<Drzava, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      DrzavaFactory.getInstance(),
      MICROSERVICE_NAME.ADRESA,
      PATH.DRZAVA,
      MICROSERVICE_PORT.ADRESA,
      WS_PATH.DRZAVA
    );
    this._findAll();
  }
}
