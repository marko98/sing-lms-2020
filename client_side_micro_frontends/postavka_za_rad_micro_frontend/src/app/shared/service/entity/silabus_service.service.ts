import { CrudService } from '../crud.service';
import {
  PATH,
  WS_PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
} from '../../const';
import { Injectable } from '@angular/core';
import { Silabus } from '../../model/entity/silabus.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { SilabusFactory } from '../../model/patterns/creational/factory_method/silabus.factory';
import { CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';

export const SILABUS_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: SilabusFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.PREDMET,
  path: PATH.SILABUS,
  microservicePort: MICROSERVICE_PORT.PREDMET,
  wsPath: WS_PATH.SILABUS,
};
@Injectable({ providedIn: 'root' })
export class SilabusService extends CrudService<Silabus, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      SilabusFactory.getInstance(),
      MICROSERVICE_NAME.PREDMET,
      PATH.SILABUS,
      MICROSERVICE_PORT.PREDMET,
      WS_PATH.SILABUS
    );
    this._findAll();
  }
}
