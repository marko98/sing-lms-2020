import { CrudService, CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';
import {
  PATH,
  WS_PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
} from '../../const';
import { Injectable } from '@angular/core';
import { Nastavnik } from '../../model/entity/nastavnik.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { NastavnikFactory } from '../../model/patterns/creational/factory_method/nastavnik.factory';

export const NASTAVNIK_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: NastavnikFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.NASTAVNIK,
  path: PATH.NASTAVNIK,
  microservicePort: MICROSERVICE_PORT.NASTAVNIK,
  wsPath: WS_PATH.NASTAVNIK,
};

@Injectable({ providedIn: 'root' })
export class NastavnikService extends CrudService<Nastavnik, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      NastavnikFactory.getInstance(),
      MICROSERVICE_NAME.NASTAVNIK,
      PATH.NASTAVNIK,
      MICROSERVICE_PORT.NASTAVNIK,
      WS_PATH.NASTAVNIK
    );
    this._findAll();
  }
}
