import { CrudService, CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';
import {
  PATH,
  WS_PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
} from '../../const';
import { Injectable } from '@angular/core';
import { Fakultet } from '../../model/entity/fakultet.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { FakultetFactory } from '../../model/patterns/creational/factory_method/fakultet.factory';

export const FAKULTET_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: FakultetFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.FAKULTET,
  path: PATH.FAKULTET,
  microservicePort: MICROSERVICE_PORT.FAKULTET,
  wsPath: WS_PATH.FAKULTET,
};

@Injectable({ providedIn: 'root' })
export class FakultetService extends CrudService<Fakultet, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      FakultetFactory.getInstance(),
      MICROSERVICE_NAME.FAKULTET,
      PATH.FAKULTET,
      MICROSERVICE_PORT.FAKULTET,
      WS_PATH.FAKULTET
    );
    this._findAll();
  }
}
