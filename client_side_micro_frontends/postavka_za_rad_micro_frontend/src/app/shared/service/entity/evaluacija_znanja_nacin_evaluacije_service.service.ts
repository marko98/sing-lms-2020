import { CrudService } from '../crud.service';
import {
  PATH,
  WS_PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
} from '../../const';
import { Injectable } from '@angular/core';
import { EvaluacijaZnanjaNacinEvaluacije } from '../../model/entity/evaluacija_znanja_nacin_evaluacije.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { EvaluacijaZnanjaNacinEvaluacijeFactory } from '../../model/patterns/creational/factory_method/evaluacija_znanja_nacin_evaluacije.factory';
import { CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';

export const EVALUACIJA_ZNANJA_NACIN_EVALUACIJE_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: EvaluacijaZnanjaNacinEvaluacijeFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.EVALUACIJA_ZNANJA,
  path: PATH.EVALUACIJA_ZNANJA_NACIN_EVALUACIJE,
  microservicePort: MICROSERVICE_PORT.EVALUACIJA_ZNANJA,
  wsPath: WS_PATH.EVALUACIJA_ZNANJA_NACIN_EVALUACIJE,
};
@Injectable({ providedIn: 'root' })
export class EvaluacijaZnanjaNacinEvaluacijeService extends CrudService<
  EvaluacijaZnanjaNacinEvaluacije,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      EvaluacijaZnanjaNacinEvaluacijeFactory.getInstance(),
      MICROSERVICE_NAME.EVALUACIJA_ZNANJA,
      PATH.EVALUACIJA_ZNANJA_NACIN_EVALUACIJE,
      MICROSERVICE_PORT.EVALUACIJA_ZNANJA,
      WS_PATH.EVALUACIJA_ZNANJA_NACIN_EVALUACIJE
    );
    this._findAll();
  }
}
