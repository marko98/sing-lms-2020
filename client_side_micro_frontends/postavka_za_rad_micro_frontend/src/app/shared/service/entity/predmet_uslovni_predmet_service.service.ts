import { CrudService } from '../crud.service';
import {
  PATH,
  MICROSERVICE_PORT,
  WS_PATH,
  MICROSERVICE_NAME,
} from '../../const';
import { Injectable } from '@angular/core';
import { PredmetUslovniPredmet } from '../../model/entity/predmet_uslovni_predmet.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { PredmetUslovniPredmetFactory } from '../../model/patterns/creational/factory_method/predmet_uslovni_predmet.factory';
import { CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';

export const PREDMET_USLOVNI_PREDMET_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: PredmetUslovniPredmetFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.PREDMET,
  path: PATH.PREDMET_USLOVNI_PREDMET,
  microservicePort: MICROSERVICE_PORT.PREDMET,
  wsPath: WS_PATH.PREDMET_USLOVNI_PREDMET,
};
@Injectable({ providedIn: 'root' })
export class PredmetUslovniPredmetService extends CrudService<
  PredmetUslovniPredmet,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      PredmetUslovniPredmetFactory.getInstance(),
      MICROSERVICE_NAME.PREDMET,
      PATH.PREDMET_USLOVNI_PREDMET,
      MICROSERVICE_PORT.PREDMET,
      WS_PATH.PREDMET_USLOVNI_PREDMET
    );
    this._findAll();
  }
}
