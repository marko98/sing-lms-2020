import { CrudService } from '../crud.service';
import {
  PATH,
  WS_PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
} from '../../const';
import { Injectable } from '@angular/core';
import { NastavnikEvaluacijaZnanja } from '../../model/entity/nastavnik_evaluacija_znanja.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { NastavnikEvaluacijaZnanjaFactory } from '../../model/patterns/creational/factory_method/nastavnik_evaluacija_znanja.factory';
import { CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';

export const NASTAVNIK_EVALUACIJA_ZNANJA_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: NastavnikEvaluacijaZnanjaFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.NASTAVNIK,
  path: PATH.NASTAVNIK_EVALUACIJA_ZNANJA,
  microservicePort: MICROSERVICE_PORT.NASTAVNIK,
  wsPath: WS_PATH.NASTAVNIK_EVALUACIJA_ZNANJA,
};
@Injectable({ providedIn: 'root' })
export class NastavnikEvaluacijaZnanjaService extends CrudService<
  NastavnikEvaluacijaZnanja,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      NastavnikEvaluacijaZnanjaFactory.getInstance(),
      MICROSERVICE_NAME.NASTAVNIK,
      PATH.NASTAVNIK_EVALUACIJA_ZNANJA,
      MICROSERVICE_PORT.NASTAVNIK,
      WS_PATH.NASTAVNIK_EVALUACIJA_ZNANJA
    );
    this._findAll();
  }
}
