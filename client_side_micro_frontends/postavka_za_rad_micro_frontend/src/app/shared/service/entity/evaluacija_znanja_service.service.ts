import { CrudService, CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';
import {
  PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
  WS_PATH,
} from '../../const';
import { Injectable } from '@angular/core';
import { EvaluacijaZnanja } from '../../model/entity/evaluacija_znanja.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { EvaluacijaZnanjaFactory } from '../../model/patterns/creational/factory_method/evaluacija_znanja.factory';

export const EVALUACIJA_ZNANJA_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: EvaluacijaZnanjaFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.EVALUACIJA_ZNANJA,
  path: PATH.EVALUACIJA_ZNANJA,
  microservicePort: MICROSERVICE_PORT.EVALUACIJA_ZNANJA,
  wsPath: WS_PATH.EVALUACIJA_ZNANJA,
};

@Injectable({ providedIn: 'root' })
export class EvaluacijaZnanjaService extends CrudService<
  EvaluacijaZnanja,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      EvaluacijaZnanjaFactory.getInstance(),
      MICROSERVICE_NAME.EVALUACIJA_ZNANJA,
      PATH.EVALUACIJA_ZNANJA,
      MICROSERVICE_PORT.EVALUACIJA_ZNANJA,
      WS_PATH.EVALUACIJA_ZNANJA
    );
    this._findAll();
  }
}
