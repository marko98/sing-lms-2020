import { CrudService, CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';
import {
  PATH,
  WS_PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
} from '../../const';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { RegistrovaniKorisnikRola } from '../../model/entity/registrovani_korisnik_rola.model';
import { RegistrovaniKorisnikRolaFactory } from '../../model/patterns/creational/factory_method/registrovani_korisnik_rola.factory';

export const REGISTROVANI_KORISNIK_ROLA_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: RegistrovaniKorisnikRolaFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.KORISNIK,
  path: PATH.REGISTROVANI_KORISNIK_ROLA,
  microservicePort: MICROSERVICE_PORT.KORISNIK,
  wsPath: WS_PATH.REGISTROVANI_KORISNIK_ROLA,
};

@Injectable({ providedIn: 'root' })
export class RegistrovaniKorisnikRolaService extends CrudService<
  RegistrovaniKorisnikRola,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      RegistrovaniKorisnikRolaFactory.getInstance(),
      MICROSERVICE_NAME.KORISNIK,
      PATH.REGISTROVANI_KORISNIK_ROLA,
      MICROSERVICE_PORT.KORISNIK,
      WS_PATH.REGISTROVANI_KORISNIK_ROLA
    );
    this._findAll();
  }
}
