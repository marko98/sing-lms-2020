import { CrudService, CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';
import {
  PATH,
  WS_PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
} from '../../const';
import { Injectable } from '@angular/core';
import { Grad } from '../../model/entity/grad.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { GradFactory } from '../../model/patterns/creational/factory_method/grad.factory';

export const GRAD_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: GradFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.ADRESA,
  path: PATH.GRAD,
  microservicePort: MICROSERVICE_PORT.ADRESA,
  wsPath: WS_PATH.GRAD,
};

@Injectable({ providedIn: 'root' })
export class GradService extends CrudService<Grad, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      GradFactory.getInstance(),
      MICROSERVICE_NAME.ADRESA,
      PATH.GRAD,
      MICROSERVICE_PORT.ADRESA,
      WS_PATH.GRAD
    );
    this._findAll();
  }
}
