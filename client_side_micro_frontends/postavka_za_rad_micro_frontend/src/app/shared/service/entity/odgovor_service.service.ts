import { CrudService } from '../crud.service';
import {
  PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
  WS_PATH,
} from '../../const';
import { Injectable } from '@angular/core';
import { Odgovor } from '../../model/entity/odgovor.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { OdgovorFactory } from '../../model/patterns/creational/factory_method/odgovor.factory';
import { CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';

export const ODGOVOR_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: OdgovorFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.EVALUACIJA_ZNANJA,
  path: PATH.ODGOVOR,
  microservicePort: MICROSERVICE_PORT.EVALUACIJA_ZNANJA,
  wsPath: WS_PATH.ODGOVOR,
};
@Injectable({ providedIn: 'root' })
export class OdgovorService extends CrudService<Odgovor, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      OdgovorFactory.getInstance(),
      MICROSERVICE_NAME.EVALUACIJA_ZNANJA,
      PATH.ODGOVOR,
      MICROSERVICE_PORT.EVALUACIJA_ZNANJA,
      WS_PATH.ODGOVOR
    );
    this._findAll();
  }
}
