import { CrudService, CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';
import {
  PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
  WS_PATH,
} from '../../const';
import { Injectable } from '@angular/core';
import { Prostorija } from '../../model/entity/prostorija.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { ProstorijaFactory } from '../../model/patterns/creational/factory_method/prostorija.factory';

export const PROSTORIJA_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: ProstorijaFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.NASTAVA,
  path: PATH.PROSTORIJA,
  microservicePort: MICROSERVICE_PORT.NASTAVA,
  wsPath: WS_PATH.PROSTORIJA,
};

@Injectable({ providedIn: 'root' })
export class ProstorijaService extends CrudService<Prostorija, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      ProstorijaFactory.getInstance(),
      MICROSERVICE_NAME.NASTAVA,
      PATH.PROSTORIJA,
      MICROSERVICE_PORT.NASTAVA,
      WS_PATH.PROSTORIJA
    );
    this._findAll();
  }
}
