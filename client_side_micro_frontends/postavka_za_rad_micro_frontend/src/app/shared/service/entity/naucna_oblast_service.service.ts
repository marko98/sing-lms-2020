import { CrudService } from '../crud.service';
import {
  PATH,
  MICROSERVICE_NAME,
  WS_PATH,
  MICROSERVICE_PORT,
} from '../../const';
import { Injectable } from '@angular/core';
import { NaucnaOblast } from '../../model/entity/naucna_oblast.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { NaucnaOblastFactory } from '../../model/patterns/creational/factory_method/naucna_oblast.factory';
import { CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';

export const NAUCNA_OBLAST_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: NaucnaOblastFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.NASTAVNIK,
  path: PATH.NAUCNA_OBLAST,
  microservicePort: MICROSERVICE_PORT.NASTAVNIK,
  wsPath: WS_PATH.NAUCNA_OBLAST,
};
@Injectable({ providedIn: 'root' })
export class NaucnaOblastService extends CrudService<NaucnaOblast, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      NaucnaOblastFactory.getInstance(),
      MICROSERVICE_NAME.NASTAVNIK,
      PATH.NAUCNA_OBLAST,
      MICROSERVICE_PORT.NASTAVNIK,
      WS_PATH.NAUCNA_OBLAST
    );
    this._findAll();
  }
}
