import { CrudService } from '../crud.service';
import {
  PATH,
  WS_PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
} from '../../const';
import { Injectable } from '@angular/core';
import { Konsultacija } from '../../model/entity/konsultacija.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { KonsultacijaFactory } from '../../model/patterns/creational/factory_method/konsultacija.factory';
import { CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';

export const KONSULTACIJA_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: KonsultacijaFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.NASTAVA,
  path: PATH.KONSULTACIJA,
  microservicePort: MICROSERVICE_PORT.NASTAVA,
  wsPath: WS_PATH.KONSULTACIJA,
};
@Injectable({ providedIn: 'root' })
export class KonsultacijaService extends CrudService<Konsultacija, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      KonsultacijaFactory.getInstance(),
      MICROSERVICE_NAME.NASTAVA,
      PATH.KONSULTACIJA,
      MICROSERVICE_PORT.NASTAVA,
      WS_PATH.KONSULTACIJA
    );
    this._findAll();
  }
}
