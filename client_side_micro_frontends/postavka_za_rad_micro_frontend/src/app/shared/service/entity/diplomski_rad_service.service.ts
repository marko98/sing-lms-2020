import { CrudService } from '../crud.service';
import {
  PATH,
  WS_PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
} from '../../const';
import { Injectable } from '@angular/core';
import { DiplomskiRad } from '../../model/entity/diplomski_rad.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { DiplomskiRadFactory } from '../../model/patterns/creational/factory_method/diplomski_rad.factory';
import { CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';

export const DIPLOMSKI_RAD_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: DiplomskiRadFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.STUDENT,
  path: PATH.DIPLOMSKI_RAD,
  microservicePort: MICROSERVICE_PORT.STUDENT,
  wsPath: WS_PATH.DIPLOMSKI_RAD,
};
@Injectable({ providedIn: 'root' })
export class DiplomskiRadService extends CrudService<DiplomskiRad, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      DiplomskiRadFactory.getInstance(),
      MICROSERVICE_NAME.STUDENT,
      PATH.DIPLOMSKI_RAD,
      MICROSERVICE_PORT.STUDENT,
      WS_PATH.DIPLOMSKI_RAD
    );
    this._findAll();
  }
}
