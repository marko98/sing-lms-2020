import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MaterialModule } from './material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MyAngularFormModule } from 'my-angular-form';
import { MyAngularTableModule } from 'my-angular-table';
import { RemoveDirective } from './directive/remove.directive';
import { AssetUrlPipe } from './pipe/asset-url-pipe.pipe';
import { AdministracijaComponent } from '../administracija/administracija.component';
import { ModelObrisanDirective } from './directive/model-obrisan.directive';
import { CreateComponent } from '../create/create.component';
import { UpdateComponent } from '../update/update.component';
import { DetailsComponent } from '../details/details.component';
import { LoginComponent } from '../login/login.component';
import { HomeComponent } from '../home/home.component';
import { ExportPodatakaComponent } from '../export-podataka/export-podataka.component';
import { RezultatiEvaluacijeComponent } from '../export-podataka/rezultati-evaluacije/rezultati-evaluacije.component';
import { StudentiComponent } from '../export-podataka/studenti/studenti.component';
import { NastavniciComponent } from '../export-podataka/nastavnici/nastavnici.component';

@NgModule({
  declarations: [
    RemoveDirective,
    AssetUrlPipe,
    AdministracijaComponent,
    ModelObrisanDirective,
    CreateComponent,
    UpdateComponent,
    DetailsComponent,
    LoginComponent,
    HomeComponent,
    ExportPodatakaComponent,
    RezultatiEvaluacijeComponent,
    StudentiComponent,
    NastavniciComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MaterialModule,
    FlexLayoutModule,
    DragDropModule,
    MyAngularFormModule,
    MyAngularTableModule,
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MaterialModule,
    FlexLayoutModule,
    DragDropModule,
    MyAngularFormModule,
    MyAngularTableModule,

    RemoveDirective,
    ModelObrisanDirective,
    AssetUrlPipe,
    AdministracijaComponent,
    CreateComponent,
    UpdateComponent,
    DetailsComponent,
    LoginComponent,
    HomeComponent,
    ExportPodatakaComponent,
    RezultatiEvaluacijeComponent,
    StudentiComponent,
    NastavniciComponent,
  ],
})
export class SharedModule {}
