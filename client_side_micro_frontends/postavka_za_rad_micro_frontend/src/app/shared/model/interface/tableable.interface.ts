import { Table, Row, HeaderValue } from 'my-angular-table';

export declare interface Tableable {
  getHeaderValue(): HeaderValue;
  addRowToTable(
    table: Table,
    detaljiFn?: Function,
    izmeniFn?: Function,
    obrisiFn?: Function
  ): void;
}
