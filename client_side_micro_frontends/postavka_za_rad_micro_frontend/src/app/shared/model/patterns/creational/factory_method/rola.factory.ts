import { Rola } from '../../../entity/rola.model';
import { RolaDTO } from '../../../dto/rola.dto';
import { RegistrovaniKorisnikRolaFactory } from './registrovani_korisnik_rola.factory';
import { Factory } from './factory.model';

export class RolaFactory extends Factory {
  public static _instance: RolaFactory;
  public static getInstance = (): RolaFactory => {
    if (!RolaFactory._instance) RolaFactory._instance = new RolaFactory();
    return <RolaFactory>RolaFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: RolaDTO): Rola {
    let rola = new Rola();
    rola.setId(dto.id);
    rola.setNaziv(dto.naziv);
    rola.setStanje(dto.stanje);
    rola.setVersion(dto.version);

    if (dto.registrovaniKorisniciRolaDTO)
      rola.setRegistrovaniKorisniciRola(
        dto.registrovaniKorisniciRolaDTO.map((t) =>
          RegistrovaniKorisnikRolaFactory.getInstance().build(t)
        )
      );
    return rola;
  }
}
