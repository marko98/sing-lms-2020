import { PohadjanjePredmeta } from '../../../entity/pohadjanje_predmeta.model';
import { PohadjanjePredmetaDTO } from '../../../dto/pohadjanje_predmeta.dto';
import { RealizacijaPredmetaFactory } from './realizacija_predmeta.factory';
import { StudentNaStudijiFactory } from './student_na_studiji.factory';
import { PolaganjeFactory } from './polaganje.factory';
import { DatumPohadjanjaPredmetaFactory } from './datum_pohadjanja_predmeta.factory';
import { Factory } from './factory.model';

export class PohadjanjePredmetaFactory extends Factory {
  public static _instance: PohadjanjePredmetaFactory;
  public static getInstance = (): PohadjanjePredmetaFactory => {
    if (!PohadjanjePredmetaFactory._instance)
      PohadjanjePredmetaFactory._instance = new PohadjanjePredmetaFactory();
    return <PohadjanjePredmetaFactory>PohadjanjePredmetaFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: PohadjanjePredmetaDTO): PohadjanjePredmeta {
    let pohadjanjePredmeta = new PohadjanjePredmeta();
    pohadjanjePredmeta.setId(dto.id);
    pohadjanjePredmeta.setKonacnaOcena(dto.konacnaOcena);
    pohadjanjePredmeta.setStanje(dto.stanje);
    pohadjanjePredmeta.setVersion(dto.version);

    if (dto.realizacijaPredmetaDTO)
      pohadjanjePredmeta.setRealizacijaPredmeta(
        RealizacijaPredmetaFactory.getInstance().build(
          dto.realizacijaPredmetaDTO
        )
      );
    if (dto.studentNaStudijiDTO)
      pohadjanjePredmeta.setStudentNaStudiji(
        StudentNaStudijiFactory.getInstance().build(dto.studentNaStudijiDTO)
      );

    if (dto.polaganjaDTO)
      pohadjanjePredmeta.setPolaganja(
        dto.polaganjaDTO.map((t) => PolaganjeFactory.getInstance().build(t))
      );

    if (dto.prisustvoDTO)
      pohadjanjePredmeta.setPrisustvo(
        dto.prisustvoDTO.map((t) =>
          DatumPohadjanjaPredmetaFactory.getInstance().build(t)
        )
      );

    return pohadjanjePredmeta;
  }
}
