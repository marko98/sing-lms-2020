import { Factory } from './factory.model';
import { Kontakt } from '../../../entity/kontakt.model';
import { KontaktDTO } from '../../../dto/kontakt.dto';
import { FakultetFactory } from './fakultet.factory';
import { UniverzitetFactory } from './univerzitet.factory';

export class KontaktFactory extends Factory {
  public static _instance: KontaktFactory;
  public static getInstance = (): KontaktFactory => {
    if (!KontaktFactory._instance)
      KontaktFactory._instance = new KontaktFactory();
    return <KontaktFactory>KontaktFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: KontaktDTO): Kontakt {
    let kontakt = new Kontakt();

    kontakt.setId(dto.id);
    kontakt.setVersion(dto.version);
    kontakt.setStanje(dto.stanje);
    kontakt.setMobilni(dto.mobilni);
    kontakt.setFiksni(dto.fiksni);

    if (dto.fakultetDTO)
      kontakt.setFakultet(FakultetFactory.getInstance().build(dto.fakultetDTO));

    if (dto.univerzitetDTO)
      kontakt.setUniverzitet(
        UniverzitetFactory.getInstance().build(dto.univerzitetDTO)
      );

    return kontakt;
  }
}
