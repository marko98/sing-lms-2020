import { Konsultacija } from '../../../entity/konsultacija.model';
import { KonsultacijaDTO } from '../../../dto/konsultacija.dto';
import { GodinaStudijaFactory } from './godina_studija.factory';
import { NastavnikFactory } from './nastavnik.factory';
import { VremeOdrzavanjaUNedeljiFactory } from './vreme_odrzavanja_u_nedelji.factory';
import { Factory } from './factory.model';

export class KonsultacijaFactory extends Factory {
  public static _instance: KonsultacijaFactory;
  public static getInstance = (): KonsultacijaFactory => {
    if (!KonsultacijaFactory._instance)
      KonsultacijaFactory._instance = new KonsultacijaFactory();
    return <KonsultacijaFactory>KonsultacijaFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: KonsultacijaDTO): Konsultacija {
    let konsultacija = new Konsultacija();
    konsultacija.setId(dto.id);
    konsultacija.setStanje(dto.stanje);
    konsultacija.setVersion(dto.version);

    if (dto.godinaStudijaDTO)
      konsultacija.setGodinaStudija(
        GodinaStudijaFactory.getInstance().build(dto.godinaStudijaDTO)
      );
    if (dto.nastavnikDTO)
      konsultacija.setNastavnik(
        NastavnikFactory.getInstance().build(dto.nastavnikDTO)
      );
    if (dto.vremeOdrzavanjaUNedeljiDTO)
      konsultacija.setVremeOdrzavanjaUNedelji(
        VremeOdrzavanjaUNedeljiFactory.getInstance().build(
          dto.vremeOdrzavanjaUNedeljiDTO
        )
      );

    return konsultacija;
  }
}
