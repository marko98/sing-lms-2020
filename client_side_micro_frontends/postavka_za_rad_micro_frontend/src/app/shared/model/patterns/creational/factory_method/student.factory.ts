import { StudentDTO } from '../../../dto/student.dto';
import { Student } from '../../../entity/student.model';
import { RegistrovaniKorisnikFactory } from './registrovani_korisnik.factory';
import { StudentNaStudijiFactory } from './student_na_studiji.factory';
import { Factory } from './factory.model';

export class StudentFactory extends Factory {
  public static _instance: StudentFactory;
  public static getInstance = (): StudentFactory => {
    if (!StudentFactory._instance)
      StudentFactory._instance = new StudentFactory();
    return <StudentFactory>StudentFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: StudentDTO): Student {
    let student = new Student();

    student.setId(dto.id);
    student.setStanje(dto.stanje);
    student.setVersion(dto.version);

    if (dto.registrovaniKorisnikDTO)
      student.setRegistrovaniKorisnik(
        RegistrovaniKorisnikFactory.getInstance().build(
          dto.registrovaniKorisnikDTO
        )
      );

    if (dto.studijeDTO)
      student.setStudije(
        dto.studijeDTO.map((t) =>
          StudentNaStudijiFactory.getInstance().build(t)
        )
      );

    return student;
  }
}
