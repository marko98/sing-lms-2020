import { AutorNastavniMaterijal } from '../../../entity/autor_nastavni_materijal.model';
import { AutorNastavniMaterijalDTO } from '../../../dto/autor_nastavni_materijal.dto';
import { AutorFactory } from './autor.factory';
import { NastavniMaterijalFactory } from './nastavni_materijal.factory';
import { Factory } from './factory.model';

export class AutorNastavniMaterijalFactory extends Factory {
  public static _instance: AutorNastavniMaterijalFactory;

  public static getInstance = (): AutorNastavniMaterijalFactory => {
    if (!AutorNastavniMaterijalFactory._instance)
      AutorNastavniMaterijalFactory._instance = new AutorNastavniMaterijalFactory();
    return <AutorNastavniMaterijalFactory>(
      AutorNastavniMaterijalFactory._instance
    );
  };

  private constructor() {
    super();
  }

  build(dto: AutorNastavniMaterijalDTO): AutorNastavniMaterijal {
    let autorNastavniMaterijal = new AutorNastavniMaterijal();
    autorNastavniMaterijal.setId(dto.id);
    autorNastavniMaterijal.setStanje(dto.stanje);
    autorNastavniMaterijal.setVersion(dto.version);

    if (dto.autorDTO)
      autorNastavniMaterijal.setAutor(
        AutorFactory.getInstance().build(dto.autorDTO)
      );
    if (dto.nastavniMaterijalDTO)
      autorNastavniMaterijal.setNastavniMaterijal(
        NastavniMaterijalFactory.getInstance().build(dto.nastavniMaterijalDTO)
      );

    return autorNastavniMaterijal;
  }
}
