import { VremeOdrzavanjaUNedelji } from '../../../entity/vreme_odrzavanja_u_nedelji.model';
import { VremeOdrzavanjaUNedeljiDTO } from '../../../dto/vreme_odrzavanja_u_nedelji.dto';
import { KonsultacijaFactory } from './konsultacija.factory';
import { Factory } from './factory.model';

export class VremeOdrzavanjaUNedeljiFactory extends Factory {
  public static _instance: VremeOdrzavanjaUNedeljiFactory;
  public static getInstance = (): VremeOdrzavanjaUNedeljiFactory => {
    if (!VremeOdrzavanjaUNedeljiFactory._instance)
      VremeOdrzavanjaUNedeljiFactory._instance = new VremeOdrzavanjaUNedeljiFactory();
    return <VremeOdrzavanjaUNedeljiFactory>(
      VremeOdrzavanjaUNedeljiFactory._instance
    );
  };

  private constructor() {
    super();
  }

  build(dto: VremeOdrzavanjaUNedeljiDTO): VremeOdrzavanjaUNedelji {
    let vremeOdrzavanjaUNedelji = new VremeOdrzavanjaUNedelji();
    vremeOdrzavanjaUNedelji.setId(dto.id);
    vremeOdrzavanjaUNedelji.setDan(dto.dan);
    vremeOdrzavanjaUNedelji.setStanje(dto.stanje);
    vremeOdrzavanjaUNedelji.setVersion(dto.version);
    vremeOdrzavanjaUNedelji.setVreme({
      hours: +dto.vreme.split(':')[0],
      minutes: +dto.vreme.split(':')[1],
    });

    if (dto.konsultacijaDTO)
      vremeOdrzavanjaUNedelji.setKonsultacija(
        KonsultacijaFactory.getInstance().build(dto.konsultacijaDTO)
      );

    return vremeOdrzavanjaUNedelji;
  }
}
