import { Dogadjaj } from '../../../entity/dogadjaj.model';
import { DogadjajDTO } from '../../../dto/dogadjaj.dto';
import { DogadjajKalendarFactory } from './dogadjaj_kalendar.factory';
import { Factory } from './factory.model';

export class DogadjajFactory extends Factory {
  public static _instance: DogadjajFactory;
  public static getInstance = (): DogadjajFactory => {
    if (!DogadjajFactory._instance)
      DogadjajFactory._instance = new DogadjajFactory();
    return <DogadjajFactory>DogadjajFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: DogadjajDTO): Dogadjaj {
    let dogadjaj = new Dogadjaj();
    dogadjaj.setId(dto.id);
    dogadjaj.setKrajnjiDatum(new Date(dto.krajnjiDatum));
    dogadjaj.setOpis(dto.opis);
    dogadjaj.setPocetniDatum(new Date(dto.pocetniDatum));
    dogadjaj.setStanje(dto.stanje);
    dogadjaj.setVersion(dto.version);

    if (dto.dogadjajKalendariDTO)
      dogadjaj.setDogadjajKalendari(
        dto.dogadjajKalendariDTO.map((t) =>
          DogadjajKalendarFactory.getInstance().build(t)
        )
      );

    return dogadjaj;
  }
}
