import { Pitanje } from '../../../entity/pitanje.model';
import { PitanjeDTO } from '../../../dto/pitanje.dto';
import { EvaluacijaZnanjaFactory } from './evaluacija_znanja.factory';
import { OdgovorFactory } from './odgovor.factory';
import { Factory } from './factory.model';

export class PitanjeFactory extends Factory {
  public static _instance: PitanjeFactory;
  public static getInstance = (): PitanjeFactory => {
    if (!PitanjeFactory._instance)
      PitanjeFactory._instance = new PitanjeFactory();
    return <PitanjeFactory>PitanjeFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: PitanjeDTO): Pitanje {
    let pitanje = new Pitanje();
    pitanje.setId(dto.id);
    pitanje.setOblast(dto.oblast);
    pitanje.setPitanje(dto.pitanje);
    pitanje.setPutanjaZaSliku(dto.putanjaZaSliku);
    pitanje.setStanje(dto.stanje);
    pitanje.setVersion(dto.version);

    if (dto.evaluacijaZnanjaDTO)
      pitanje.setEvaluacijaZnanja(
        EvaluacijaZnanjaFactory.getInstance().build(dto.evaluacijaZnanjaDTO)
      );

    if (dto.odgovoriDTO)
      pitanje.setOdgovori(
        dto.odgovoriDTO.map((t) => OdgovorFactory.getInstance().build(t))
      );

    return pitanje;
  }
}
