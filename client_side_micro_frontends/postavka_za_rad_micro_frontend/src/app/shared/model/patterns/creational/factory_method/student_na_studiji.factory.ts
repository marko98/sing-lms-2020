import { StudentNaStudiji } from '../../../entity/student_na_studiji.model';
import { StudentNaStudijiDTO } from '../../../dto/student_na_studiji.dto';
import { DiplomskiRadFactory } from './diplomski_rad.factory';
import { GodinaStudijaFactory } from './godina_studija.factory';
import { StudentFactory } from './student.factory';
import { IstrazivackiRadStudentNaStudijiFactory } from './istrazivacki_rad_student_na_studiji.factory';
import { PohadjanjePredmetaFactory } from './pohadjanje_predmeta.factory';
import { Factory } from './factory.model';

export class StudentNaStudijiFactory extends Factory {
  public static _instance: StudentNaStudijiFactory;
  public static getInstance = (): StudentNaStudijiFactory => {
    if (!StudentNaStudijiFactory._instance)
      StudentNaStudijiFactory._instance = new StudentNaStudijiFactory();
    return <StudentNaStudijiFactory>StudentNaStudijiFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: StudentNaStudijiDTO): StudentNaStudiji {
    let studentNaStudiji = new StudentNaStudiji();
    studentNaStudiji.setId(dto.id);
    studentNaStudiji.setBrojIndeksa(dto.brojIndeksa);
    studentNaStudiji.setDatumUpisa(new Date(dto.datumUpisa));
    studentNaStudiji.setStanje(dto.stanje);
    studentNaStudiji.setVersion(dto.version);

    if (dto.diplomskiRadDTO)
      studentNaStudiji.setDiplomskiRad(
        DiplomskiRadFactory.getInstance().build(dto.diplomskiRadDTO)
      );
    if (dto.godinaStudijaDTO)
      studentNaStudiji.setGodinaStudija(
        GodinaStudijaFactory.getInstance().build(dto.godinaStudijaDTO)
      );
    if (dto.studentDTO)
      studentNaStudiji.setStudent(
        StudentFactory.getInstance().build(dto.studentDTO)
      );

    if (dto.istrazivackiRadoviStudentNaStudijiDTO)
      studentNaStudiji.setIstrazivackiRadoviStudentNaStudiji(
        dto.istrazivackiRadoviStudentNaStudijiDTO.map((t) =>
          IstrazivackiRadStudentNaStudijiFactory.getInstance().build(t)
        )
      );

    if (dto.pohadjanjaPredmetaDTO)
      studentNaStudiji.setPohadjanjaPredmeta(
        dto.pohadjanjaPredmetaDTO.map((t) =>
          PohadjanjePredmetaFactory.getInstance().build(t)
        )
      );

    return studentNaStudiji;
  }
}
