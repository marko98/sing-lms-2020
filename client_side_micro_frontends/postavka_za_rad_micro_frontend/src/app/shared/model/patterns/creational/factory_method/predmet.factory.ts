import { Predmet } from '../../../entity/predmet.model';
import { PredmetDTO } from '../../../dto/predmet.dto';
import { GodinaStudijaFactory } from './godina_studija.factory';
import { RealizacijaPredmetaFactory } from './realizacija_predmeta.factory';
import { SilabusFactory } from './silabus.factory';
import { PredmetUslovniPredmetFactory } from './predmet_uslovni_predmet.factory';
import { PredmetTipDrugogOblikaNastaveFactory } from './predmet_tip_drugog_oblika_nastave.factory';
import { PredmetTipNastaveFactory } from './predmet_tip_nastave.factory';
import { Factory } from './factory.model';

export class PredmetFactory extends Factory {
  public static _instance: PredmetFactory;
  public static getInstance = (): PredmetFactory => {
    if (!PredmetFactory._instance)
      PredmetFactory._instance = new PredmetFactory();
    return <PredmetFactory>PredmetFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: PredmetDTO): Predmet {
    let predmet = new Predmet();
    predmet.setId(dto.id);
    predmet.setBrojCasovaZaIstrazivackeRadove(
      dto.brojCasovaZaIstrazivackeRadove
    );
    predmet.setBrojPredavanja(dto.brojPredavanja);
    predmet.setBrojVezbi(dto.brojVezbi);
    predmet.setEspb(dto.espb);
    predmet.setNaziv(dto.naziv);
    predmet.setObavezan(dto.obavezan);
    predmet.setStanje(dto.stanje);
    predmet.setTrajanjeUSemestrima(dto.trajanjeUSemestrima);
    predmet.setVersion(dto.version);

    if (dto.godinaStudijaDTO)
      predmet.setGodinaStudija(
        GodinaStudijaFactory.getInstance().build(dto.godinaStudijaDTO)
      );
    if (dto.realizacijaPredmetaDTO)
      predmet.setRealizacijaPredmeta(
        RealizacijaPredmetaFactory.getInstance().build(
          dto.realizacijaPredmetaDTO
        )
      );
    if (dto.silabusDTO)
      predmet.setSilabus(SilabusFactory.getInstance().build(dto.silabusDTO));

    if (dto.predmetiZaKojeSamUslovDTO)
      predmet.setPredmetiZaKojeSamUslov(
        dto.predmetiZaKojeSamUslovDTO.map((t) =>
          PredmetUslovniPredmetFactory.getInstance().build(t)
        )
      );

    if (dto.predmetTipoviDrugogOblikaNastaveDTO)
      predmet.setPredmetTipoviDrugogOblikaNastave(
        dto.predmetTipoviDrugogOblikaNastaveDTO.map((t) =>
          PredmetTipDrugogOblikaNastaveFactory.getInstance().build(t)
        )
      );

    if (dto.predmetTipoviNastaveDTO)
      predmet.setPredmetTipoviNastave(
        dto.predmetTipoviNastaveDTO.map((t) =>
          PredmetTipNastaveFactory.getInstance().build(t)
        )
      );

    if (dto.uslovniPredmetiDTO)
      predmet.setUslovniPredmeti(
        dto.uslovniPredmetiDTO.map((t) =>
          PredmetUslovniPredmetFactory.getInstance().build(t)
        )
      );

    return predmet;
  }
}
