import { NastavniMaterijal } from '../../../entity/nastavni_materijal.model';
import { NastavniMaterijalDTO } from '../../../dto/nastavni_materijal.dto';
import { IshodFactory } from './ishod.factory';
import { AutorNastavniMaterijalFactory } from './autor_nastavni_materijal.factory';
import { FajlFactory } from './fajl.factory';
import { Factory } from './factory.model';

export class NastavniMaterijalFactory extends Factory {
  public static _instance: NastavniMaterijalFactory;
  public static getInstance = (): NastavniMaterijalFactory => {
    if (!NastavniMaterijalFactory._instance)
      NastavniMaterijalFactory._instance = new NastavniMaterijalFactory();
    return <NastavniMaterijalFactory>NastavniMaterijalFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: NastavniMaterijalDTO): NastavniMaterijal {
    let nastavniMaterijal = new NastavniMaterijal();
    nastavniMaterijal.setId(dto.id);
    nastavniMaterijal.setDatum(new Date(dto.datum));
    nastavniMaterijal.setNaziv(dto.naziv);
    nastavniMaterijal.setStanje(dto.stanje);
    nastavniMaterijal.setVersion(dto.version);

    if (dto.ishodDTO)
      nastavniMaterijal.setIshod(
        IshodFactory.getInstance().build(dto.ishodDTO)
      );

    if (dto.autoriNastavniMaterijalDTO)
      nastavniMaterijal.setAutoriNastavniMaterijal(
        dto.autoriNastavniMaterijalDTO.map((t) =>
          AutorNastavniMaterijalFactory.getInstance().build(t)
        )
      );

    if (dto.fajloviDTO)
      nastavniMaterijal.setFajlovi(
        dto.fajloviDTO.map((t) => FajlFactory.getInstance().build(t))
      );

    return nastavniMaterijal;
  }
}
