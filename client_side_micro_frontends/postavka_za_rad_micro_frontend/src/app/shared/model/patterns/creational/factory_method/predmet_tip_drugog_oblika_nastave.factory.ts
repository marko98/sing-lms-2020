import { PredmetTipDrugogOblikaNastaveDTO } from '../../../dto/predmet_tip_drugog_oblika_nastave.dto';
import { PredmetTipDrugogOblikaNastave } from '../../../entity/predmet_tip_drugog_oblika_nastave.model';
import { PredmetFactory } from './predmet.factory';
import { Factory } from './factory.model';

export class PredmetTipDrugogOblikaNastaveFactory extends Factory {
  public static _instance: PredmetTipDrugogOblikaNastaveFactory;
  public static getInstance = (): PredmetTipDrugogOblikaNastaveFactory => {
    if (!PredmetTipDrugogOblikaNastaveFactory._instance)
      PredmetTipDrugogOblikaNastaveFactory._instance = new PredmetTipDrugogOblikaNastaveFactory();
    return <PredmetTipDrugogOblikaNastaveFactory>(
      PredmetTipDrugogOblikaNastaveFactory._instance
    );
  };

  private constructor() {
    super();
  }

  build(dto: PredmetTipDrugogOblikaNastaveDTO): PredmetTipDrugogOblikaNastave {
    let predmetTipDrugogOblikaNastave = new PredmetTipDrugogOblikaNastave();
    predmetTipDrugogOblikaNastave.setId(dto.id);
    predmetTipDrugogOblikaNastave.setStanje(dto.stanje);
    predmetTipDrugogOblikaNastave.setTipDrugogOblikaNastave(
      dto.tipDrugogOblikaNastave
    );
    predmetTipDrugogOblikaNastave.setVersion(dto.version);

    if (dto.predmetDTO)
      predmetTipDrugogOblikaNastave.setPredmet(
        PredmetFactory.getInstance().build(dto.predmetDTO)
      );
    return predmetTipDrugogOblikaNastave;
  }
}
