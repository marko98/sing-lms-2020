import { StanjeModela } from '../enum/stanje_modela.enum';
import { CrudServiceForModel } from '../../service/crud.service-for-model';
import { Subject } from 'rxjs';
import {
  Form,
  FormRow,
  ROW_ITEM_TYPE,
  FormFieldInputNumberInterface,
  FORM_FIELD_INPUT_TYPE,
  SelectInterface,
  VALIDATOR_NAMES,
  DatepickerInterface,
  MAT_COLOR,
} from 'my-angular-form';
import { Validators } from '@angular/forms';
import { HeaderValue, Table } from 'my-angular-table';
import { Formable } from '../interface/formable.interface';
import { Tableable } from '../interface/tableable.interface';
import { EvaluacijaZnanja } from '../entity/evaluacija_znanja.model';
import { take } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { EVALUACIJA_ZNANJA_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/evaluacija_znanja_service.service';
import { IspitniRok } from '../enum/ispitni_rok.enum';
import { TipEvaluacije } from '../enum/tip_evaluacije.enum';
import { RealizacijaPredmeta } from '../entity/realizacija_predmeta.model';
import { REALIZACIJA_PREDMETA_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/realizacija_predmeta_service.service';
import { Ishod } from '../entity/ishod.model';
import { ISHOD_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/ishod_service.service';

export class EvaluacijaZnanjaFormableTableable implements Formable, Tableable {
  private _formSubject: Subject<Form> = new Subject();

  public getFormSubject = (): Subject<Form> => {
    return this._formSubject;
  };

  constructor(private _entitet: EvaluacijaZnanja) {}

  public getForm = (
    csfm: CrudServiceForModel,
    isFormForCreation: boolean,
    isFormForDetails: boolean = false,
    isFormForUpdate: boolean = false,
    disableAllFields: boolean = false
  ): void => {
    let form = new Form({
      showSubmitButton: !disableAllFields,
      submitButtonText: 'potvrdi',
      showCancelButton: !disableAllFields,
      cancelButtonText: 'odustani',
      showResetButton: isFormForCreation,
      resetButtonText: 'resetuj',
      submitOnlyIfFormValid: true,
    });

    // private id: number;+

    // private pocetak: Date;+

    // private trajanje: number;+

    // private stanje: StanjeModela;+

    // private version: number;+

    // private nastavniciEvaluacijaZnanja: NastavnikEvaluacijaZnanja[];+

    // private realizacijaPredmeta: RealizacijaPredmeta;+

    // private polaganja: Polaganje[];+

    // private ishod: Ishod;+

    // private fajlovi: Fajl[];+

    // private tipEvaluacije: TipEvaluacije;+

    // private pitanja: Pitanje[];+

    // private ispitniRok: IspitniRok;+

    // private evaluacijaZnanjaNaciniEvaluacije: EvaluacijaZnanjaNacinEvaluacije[];+

    if (!isFormForCreation) {
      let rowId = new FormRow();
      form.addChild(rowId);
      rowId.addChildInterface(<FormFieldInputNumberInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
        formFieldInputType: FORM_FIELD_INPUT_TYPE.NUMBER,
        controlName: 'id',
        labelName: 'Id',
        defaultValue: this._entitet.getId(),
        disabled: true,
      });

      // console.log(StanjeModela.AKTIVAN);
      // console.log(typeof StanjeModela.AKTIVAN);

      let rowStanjeModela = new FormRow();
      form.addChild(rowStanjeModela);
      rowStanjeModela.addChildInterface(<SelectInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
        controlName: 'stanje',
        labelName: 'Stanje modela',
        defaultValue: this._entitet.getStanje(),
        optionValues: [
          { value: StanjeModela.AKTIVAN, textToShow: 'AKTIVAN' },
          { value: StanjeModela.NEAKTIVAN, textToShow: 'NEAKTIVAN' },
        ],
        disabled: disableAllFields,
        validators: [
          {
            message: 'Stanje modela je obavezno',
            name: VALIDATOR_NAMES.REQUIRED,
            validatorFn: Validators.required,
          },
        ],
      });
    }

    let rowPocetak = new FormRow();
    form.addChild(rowPocetak);
    rowPocetak.addChildInterface(<DatepickerInterface>{
      type: ROW_ITEM_TYPE.DATEPICKER,
      labelName: 'Datum evaluacije:',
      controlName: 'pocetak',
      color: MAT_COLOR.ACCENT,
      matIcon: 'date_range',
      disabled: disableAllFields,
      min: new Date(1950, 0, 1),
      startAt: new Date(2005, 0, 1),
      validators: [
        {
          message: 'datum evaluacije je obavezan',
          name: VALIDATOR_NAMES.REQUIRED,
          validatorFn: Validators.required,
        },
      ],
      defaultValue: new Date(this._entitet.getPocetak()),
      appearance: 'outline',
      // disabledDays: [DAY.MONDAY, DAY.SATURDAY],
      timepicker: {
        buttonAlign: 'left',
        /**
         * min i max pisi u formatu:
         * min: 10:00 AM
         * max: 03:00 PM
         */
        // min: '10:00 AM',
        // max: '03:00 PM',
        /**
         * Set a default value and time for a timepicker. The format of the time is in 24 hours notation 23:00.
         * A Date string won't work.
         */
        defaultValue:
          new Date(this._entitet.getPocetak()) && !isFormForCreation
            ? new Date(this._entitet.getPocetak()).getHours() +
              ':' +
              new Date(this._entitet.getPocetak()).getMinutes()
            : undefined,
        labelName: 'vreme pocetka evaluacije',
        disabled: disableAllFields,
      },
    });

    let rowTrajanje = new FormRow();
    form.addChild(rowTrajanje);
    rowTrajanje.addChildInterface(<FormFieldInputNumberInterface>{
      type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
      formFieldInputType: FORM_FIELD_INPUT_TYPE.NUMBER,
      controlName: 'trajanje',
      labelName: 'Trajanje u minutima',
      step: 1,
      textSuffix: 'min.',
      defaultValue: this._entitet.getTrajanje(),
      disabled: disableAllFields,
      validators: [
        {
          message: 'evaluacija znanja mora da traje najmanje 15 minuta',
          name: VALIDATOR_NAMES.MIN,
          validatorFn: Validators.min(15),
        },
        {
          message:
            'evaluacija znanja moze da traje najvise 315 minuta (7 casova po 45 minuta)',
          name: VALIDATOR_NAMES.MAX,
          validatorFn: Validators.max(315),
        },
      ],
    });

    let rowIspitniRok = new FormRow();
    form.addChild(rowIspitniRok);
    rowIspitniRok.addChildInterface(<SelectInterface>{
      type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
      controlName: 'ispitniRok',
      labelName: 'Ispitni rok:',
      defaultValue: this._entitet.getIspitniRok(),
      optionValues: [
        {
          value: IspitniRok.DRUGI_SEPTEMBARSKI,
          textToShow: IspitniRok.DRUGI_SEPTEMBARSKI.replace(
            '_',
            ' '
          ).toLowerCase(),
        },
        {
          value: IspitniRok.FEBRUARSKI,
          textToShow: IspitniRok.FEBRUARSKI.replace('_', ' ').toLowerCase(),
        },
        {
          value: IspitniRok.JANUARSKI,
          textToShow: IspitniRok.JANUARSKI.replace('_', ' ').toLowerCase(),
        },
        {
          value: IspitniRok.JULSKI,
          textToShow: IspitniRok.JULSKI.replace('_', ' ').toLowerCase(),
        },
        {
          value: IspitniRok.JUNSKI,
          textToShow: IspitniRok.JUNSKI.replace('_', ' ').toLowerCase(),
        },
        {
          value: IspitniRok.SEPTEMBARSKI,
          textToShow: IspitniRok.SEPTEMBARSKI.replace('_', ' ').toLowerCase(),
        },
      ],
      disabled: disableAllFields,
      validators: [
        {
          message: 'Ispitni rok je obavezan',
          name: VALIDATOR_NAMES.REQUIRED,
          validatorFn: Validators.required,
        },
      ],
    });

    let rowTipEvaluacije = new FormRow();
    form.addChild(rowTipEvaluacije);
    rowTipEvaluacije.addChildInterface(<SelectInterface>{
      type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
      controlName: 'tipEvaluacije',
      labelName: 'Tip evaluacije:',
      defaultValue: this._entitet.getTipEvaluacije(),
      optionValues: [
        {
          value: TipEvaluacije.ISPIT,
          textToShow: TipEvaluacije.ISPIT.replace('_', ' ').toLowerCase(),
        },
        {
          value: TipEvaluacije.KOLOKVIJUM,
          textToShow: TipEvaluacije.KOLOKVIJUM.replace('_', ' ').toLowerCase(),
        },
        {
          value: TipEvaluacije.TEST,
          textToShow: TipEvaluacije.TEST.replace('_', ' ').toLowerCase(),
        },
      ],
      disabled: disableAllFields,
      validators: [
        {
          message: 'Tip evaluacije je obavezan',
          name: VALIDATOR_NAMES.REQUIRED,
          validatorFn: Validators.required,
        },
      ],
    });

    let realizacijaPredmetaService = csfm.getCrudServiceForModel<
      RealizacijaPredmeta,
      number
    >(REALIZACIJA_PREDMETA_SERVICE_FOR_MODEL_INTERFACE);

    realizacijaPredmetaService
      .findAll()
      .pipe(take(1))
      .subscribe(
        (entiteti: RealizacijaPredmeta[]) => {
          for (let index = 0; index < entiteti.length; index++) {
            if (
              entiteti[index].getId().toString() ===
              this._entitet.getRealizacijaPredmeta()?.getId().toString()
            ) {
              this._entitet.setRealizacijaPredmeta(entiteti[index]);
              break;
            }
          }

          if (isFormForCreation) {
            // entiteti = entiteti.filter((r) => !r.getUniverzitet());
          } else if (isFormForUpdate) {
            // entiteti = entiteti.filter((r) => !r.getUniverzitet());
            // entiteti.push(this._entitet.getRealizacijaPredmeta());
          } else if (isFormForDetails) {
            if (
              this._entitet.getRealizacijaPredmeta() &&
              !entiteti.find(
                (e) =>
                  e.getId().toString() ===
                  this._entitet.getRealizacijaPredmeta().getId().toString()
              )
            )
              entiteti.push(this._entitet.getRealizacijaPredmeta());
          }

          // if (!(entiteti.length > 0)) {
          //   this._formSubject.error(
          //     new Error(
          //       'Sve studentske sluzbe vec pripadaju nekim univerzitetima'
          //     )
          //   );
          //   return;
          // } else {
          let rowEntitet = new FormRow();
          form.addChild(rowEntitet);
          rowEntitet.addChildInterface(<SelectInterface>{
            type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
            controlName: 'realizacijaPredmeta',
            labelName: 'Realizacija predmeta:',
            defaultValue: this._entitet.getRealizacijaPredmeta(),
            optionValues: entiteti.map((d) => {
              return {
                value: d,
                textToShow: d.getId().toString(),
              };
            }),
            disabled: disableAllFields,
          });
          // }

          this._formSubject.next(form);
          // --------------------------------------
          let ishodService = csfm.getCrudServiceForModel<Ishod, number>(
            ISHOD_SERVICE_FOR_MODEL_INTERFACE
          );

          ishodService
            .findAll()
            .pipe(take(1))
            .subscribe(
              (entiteti: Ishod[]) => {
                for (let index = 0; index < entiteti.length; index++) {
                  if (
                    entiteti[index].getId().toString() ===
                    this._entitet.getIshod()?.getId().toString()
                  ) {
                    this._entitet.setIshod(entiteti[index]);
                    break;
                  }
                }

                if (isFormForCreation) {
                  entiteti = entiteti.filter((r) => !r.getEvaluacijaZnanja());
                } else if (isFormForUpdate) {
                  entiteti = entiteti.filter((r) => !r.getEvaluacijaZnanja());
                  entiteti.push(this._entitet.getIshod());
                } else if (isFormForDetails) {
                  if (
                    this._entitet.getIshod() &&
                    !entiteti.find(
                      (e) =>
                        e.getId().toString() ===
                        this._entitet.getIshod().getId().toString()
                    )
                  )
                    entiteti.push(this._entitet.getIshod());
                }

                if (!(entiteti.length > 0)) {
                  this._formSubject.error(
                    new Error(
                      'Svi ishodi vec pripadaju nekim evaluacijama znanja'
                    )
                  );
                  return;
                } else {
                  let rowEntitet = new FormRow();
                  form.addChild(rowEntitet);
                  rowEntitet.addChildInterface(<SelectInterface>{
                    type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
                    controlName: 'ishod',
                    labelName: 'Ishod:',
                    defaultValue: this._entitet.getIshod(),
                    optionValues: entiteti.map((d) => {
                      return {
                        value: d,
                        textToShow: d.getId().toString(),
                      };
                    }),
                    disabled: disableAllFields,
                  });
                }

                this._formSubject.next(form);
                this._formSubject.complete();
              },
              (err: HttpErrorResponse) => {
                this._formSubject.error(err);
              }
            );
          // ------------------------------------
        },
        (err: HttpErrorResponse) => {
          this._formSubject.error(err);
        }
      );
  };

  public save = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    let evaluacijaZnanjaService = csfm.getCrudServiceForModel<
      EvaluacijaZnanja,
      number
    >(EVALUACIJA_ZNANJA_SERVICE_FOR_MODEL_INTERFACE);
    entitetDict.pocetak = new Date(entitetDict.pocetak.toUTCString());
    evaluacijaZnanjaService
      .save(entitetDict)
      .pipe(take(1))
      .subscribe(
        (entitet) => {},
        (err: HttpErrorResponse) => {
          subject.error(err);
        },
        () => {
          subject.complete();
        }
      );

    return subject;
  };

  public update = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    let evaluacijaZnanjaService = csfm.getCrudServiceForModel<
      EvaluacijaZnanja,
      number
    >(EVALUACIJA_ZNANJA_SERVICE_FOR_MODEL_INTERFACE);
    entitetDict.pocetak = new Date(entitetDict.pocetak.toUTCString());
    evaluacijaZnanjaService
      .update(entitetDict)
      .pipe(take(1))
      .subscribe(
        (entitet) => {},
        (err: HttpErrorResponse) => {
          subject.error(err);
        },
        () => {
          subject.complete();
        }
      );

    return subject;
  };

  public getHeaderValue = (): HeaderValue => {
    return {
      headerItems: [
        { context: 'id' },
        { context: 'stanje modela' },
        { context: 'datum' },
        { context: 'trajanje u minutima' },
        { context: 'ispitni rok' },
        { context: 'tip evaluacije' },
        { context: 'realizacija predmeta' },
        { context: 'ishod' },
        { context: '' },
        { context: '' },
        { context: '' },
      ],
    };
  };

  public addRowToTable = (
    table: Table,
    detaljiFn: Function = () => {},
    izmeniFn: Function = () => {},
    obrisiFn: Function = () => {}
  ): void => {
    try {
      table.addChildValue({
        data: this._entitet,
        rowItems: [
          {
            context: this._entitet.getId(),
          },
          {
            context: this._entitet.getStanje(),
          },
          {
            context: this._entitet.getPocetak().toLocaleDateString(),
          },
          {
            context: this._entitet.getTrajanje().toString(),
          },
          {
            context: this._entitet
              .getIspitniRok()
              .replace('_', ' ')
              .toLowerCase(),
          },
          {
            context: this._entitet
              .getTipEvaluacije()
              .replace('_', ' ')
              .toLowerCase(),
          },
          {
            context: this._entitet.getRealizacijaPredmeta()
              ? this._entitet.getRealizacijaPredmeta().getId()
              : '/',
          },
          {
            context: this._entitet.getIshod()
              ? this._entitet.getIshod().getOpis() +
                ', id: ' +
                this._entitet.getIshod().getId().toString()
              : '/',
          },
          {
            context: 'detalji',
            button: { function: detaljiFn },
          },
          {
            context: 'izmeni',
            button: { function: izmeniFn },
          },
          {
            context:
              this._entitet.getStanje() !== StanjeModela.OBRISAN
                ? 'obrisi'
                : '',
            button:
              this._entitet.getStanje() !== StanjeModela.OBRISAN
                ? { function: obrisiFn }
                : undefined,
          },
        ],
      });
    } catch (error) {
      console.log(error);
    }
  };
}
