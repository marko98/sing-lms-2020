import { StanjeModela } from '../enum/stanje_modela.enum';
import { CrudServiceForModel } from '../../service/crud.service-for-model';
import { Subject } from 'rxjs';
import {
  Form,
  FormRow,
  ROW_ITEM_TYPE,
  FormFieldInputNumberInterface,
  FORM_FIELD_INPUT_TYPE,
  SelectInterface,
  VALIDATOR_NAMES,
  FormFieldInputTextInterface,
  AT_LEAST_ONE_DIGIT_REGEX,
} from 'my-angular-form';
import { Validators } from '@angular/forms';
import { HeaderValue, Table } from 'my-angular-table';
import { Kontakt } from '../entity/kontakt.model';
import { Formable } from '../interface/formable.interface';
import { Tableable } from '../interface/tableable.interface';
import { KONTAKT_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/kontakt.service';
import { take } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { Univerzitet } from '../entity/univerzitet.model';
import { UNIVERZITET_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/univerzitet_service.service';
import { Fakultet } from '../entity/fakultet.model';
import { FAKULTET_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/fakultet_service.service';

export class KontaktFormableTableable implements Formable, Tableable {
  private _formSubject: Subject<Form> = new Subject();

  public getFormSubject = (): Subject<Form> => {
    return this._formSubject;
  };

  constructor(private _entitet: Kontakt) {}

  public getForm = (
    csfm: CrudServiceForModel,
    isFormForCreation: boolean,
    isFormForDetails: boolean = false,
    isFormForUpdate: boolean = false,
    disableAllFields: boolean = false
  ): void => {
    // private id: number;+

    // private mobilni: string;+

    // private fiksni: string;+

    // private version: number;+

    // private stanje: StanjeModela;+

    // private univerzitet: Univerzitet;+

    // private fakultet: Fakultet;+

    let form = new Form({
      showSubmitButton: !disableAllFields,
      submitButtonText: 'potvrdi',
      showCancelButton: !disableAllFields,
      cancelButtonText: 'odustani',
      showResetButton: isFormForCreation,
      resetButtonText: 'resetuj',
      submitOnlyIfFormValid: true,
    });

    if (!isFormForCreation) {
      let rowId = new FormRow();
      form.addChild(rowId);
      rowId.addChildInterface(<FormFieldInputNumberInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
        formFieldInputType: FORM_FIELD_INPUT_TYPE.NUMBER,
        controlName: 'id',
        labelName: 'Id',
        defaultValue: this._entitet.getId(),
        disabled: true,
      });

      // console.log(StanjeModela.AKTIVAN);
      // console.log(typeof StanjeModela.AKTIVAN);

      let rowStanjeModela = new FormRow();
      form.addChild(rowStanjeModela);
      rowStanjeModela.addChildInterface(<SelectInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
        controlName: 'stanje',
        labelName: 'Stanje modela',
        defaultValue: this._entitet.getStanje(),
        optionValues: [
          { value: StanjeModela.AKTIVAN, textToShow: 'AKTIVAN' },
          { value: StanjeModela.NEAKTIVAN, textToShow: 'NEAKTIVAN' },
        ],
        disabled: disableAllFields,
        validators: [
          {
            message: 'Stanje modela je obavezno',
            name: VALIDATOR_NAMES.REQUIRED,
            validatorFn: Validators.required,
          },
        ],
      });
    }

    let rowMobilni = new FormRow();
    form.addChild(rowMobilni);
    rowMobilni.addChildInterface(<FormFieldInputTextInterface>{
      type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
      formFieldInputType: FORM_FIELD_INPUT_TYPE.TEXT,
      controlName: 'mobilni',
      labelName: 'Broj mobilnog telefona',
      defaultValue: this._entitet.getMobilni(),
      appearance: 'outline',
      placeholder: 'unesite broj mobilnog telefona',
      disabled: disableAllFields,
      validators: [
        {
          message: 'broj mobilnog telefona je neispravan',
          name: VALIDATOR_NAMES.PATTERN,
          validatorFn: Validators.pattern(
            new RegExp(/^(\+381)?(\s|-)?6(([0-6]|[8-9])\d{7}|(77|78)\d{6}){1}$/)
          ),
        },
        {
          message: 'broj mobilnog telefona je obavezan',
          name: VALIDATOR_NAMES.REQUIRED,
          validatorFn: Validators.required,
        },
      ],
    });

    let rowFiksni = new FormRow();
    form.addChild(rowFiksni);
    rowFiksni.addChildInterface(<FormFieldInputTextInterface>{
      type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
      formFieldInputType: FORM_FIELD_INPUT_TYPE.TEXT,
      controlName: 'fiksni',
      labelName: 'Broj fiksnog telefona',
      defaultValue: this._entitet.getMobilni(),
      appearance: 'outline',
      placeholder: 'unesite broj fiksnog telefona',
      disabled: disableAllFields,
      validators: [
        {
          message: 'broj fiksnog telefona je neispravan',
          name: VALIDATOR_NAMES.PATTERN,
          validatorFn: Validators.pattern(
            new RegExp(/^(\+381)?(\s|-)?6(([0-6]|[8-9])\d{7}|(77|78)\d{6}){1}$/)
          ),
        },
        {
          message: 'broj fiksnog telefona je obavezan',
          name: VALIDATOR_NAMES.REQUIRED,
          validatorFn: Validators.required,
        },
      ],
    });

    // --------------------------------------
    let univerzitetService = csfm.getCrudServiceForModel<Univerzitet, number>(
      UNIVERZITET_SERVICE_FOR_MODEL_INTERFACE
    );

    univerzitetService
      .findAll()
      .pipe(take(1))
      .subscribe(
        (entiteti: Univerzitet[]) => {
          for (let index = 0; index < entiteti.length; index++) {
            if (
              entiteti[index].getId().toString() ===
              this._entitet.getUniverzitet()?.getId().toString()
            ) {
              this._entitet.setUniverzitet(entiteti[index]);
              break;
            }
          }

          if (isFormForCreation) {
            // entiteti = entiteti.filter((r) => !r.getUniverzitet());
          } else if (isFormForUpdate) {
            // entiteti = entiteti.filter((r) => !r.getUniverzitet());
            // entiteti.push(this._entitet.getUniverzitet());
          } else if (isFormForDetails) {
            if (
              this._entitet.getUniverzitet() &&
              !entiteti.find(
                (e) =>
                  e.getId().toString() ===
                  this._entitet.getUniverzitet().getId().toString()
              )
            )
              entiteti.push(this._entitet.getUniverzitet());
          }

          // if (!(entiteti.length > 0)) {
          //   this._formSubject.error(
          //     new Error(
          //       'neki error'
          //     )
          //   );
          //   return;
          // }

          let rowEntitet = new FormRow();
          form.addChild(rowEntitet);
          rowEntitet.addChildInterface(<SelectInterface>{
            type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
            controlName: 'univerzitet',
            labelName: 'Univerzitet',
            defaultValue: this._entitet.getUniverzitet(),
            optionValues: entiteti.map((d) => {
              return {
                value: d,
                textToShow: 'id: ' + d.getId().toString() + ', ' + d.getNaziv(),
              };
            }),
            disabled: disableAllFields,
          });
          this._formSubject.next(form);
          this._formSubject.complete();
        },
        (err: HttpErrorResponse) => {
          this._formSubject.error(err);
        }
      );
    this._formSubject.next(form);
    // --------------------------------------
    let fakultetService = csfm.getCrudServiceForModel<Fakultet, number>(
      FAKULTET_SERVICE_FOR_MODEL_INTERFACE
    );

    fakultetService
      .findAll()
      .pipe(take(1))
      .subscribe(
        (entiteti: Fakultet[]) => {
          for (let index = 0; index < entiteti.length; index++) {
            if (
              entiteti[index].getId().toString() ===
              this._entitet.getFakultet()?.getId().toString()
            ) {
              this._entitet.setFakultet(entiteti[index]);
              break;
            }
          }

          if (isFormForCreation) {
            // entiteti = entiteti.filter((r) => !r.getFakultet());
          } else if (isFormForUpdate) {
            // entiteti = entiteti.filter((r) => !r.getFakultet());
            // entiteti.push(this._entitet.getFakultet());
          } else if (isFormForDetails) {
            if (
              this._entitet.getFakultet() &&
              !entiteti.find(
                (e) =>
                  e.getId().toString() ===
                  this._entitet.getFakultet().getId().toString()
              )
            )
              entiteti.push(this._entitet.getFakultet());
          }

          // if (!(entiteti.length > 0)) {
          //   this._formSubject.error(
          //     new Error(
          //       'neki error'
          //     )
          //   );
          //   return;
          // }

          let rowEntitet = new FormRow();
          form.addChild(rowEntitet);
          rowEntitet.addChildInterface(<SelectInterface>{
            type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
            controlName: 'fakultet',
            labelName: 'Fakultet',
            defaultValue: this._entitet.getFakultet(),
            optionValues: entiteti.map((d) => {
              return {
                value: d,
                textToShow: 'id: ' + d.getId().toString() + ', ' + d.getNaziv(),
              };
            }),
            disabled: disableAllFields,
          });
          this._formSubject.next(form);
          this._formSubject.complete();
        },
        (err: HttpErrorResponse) => {
          this._formSubject.error(err);
        }
      );
    this._formSubject.next(form);
    this._formSubject.complete();
    // ------------------------------------
    // ------------------------------------
  };

  public save = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    let kontaktService = csfm.getCrudServiceForModel<Kontakt, number>(
      KONTAKT_SERVICE_FOR_MODEL_INTERFACE
    );
    kontaktService
      .save(entitetDict)
      .pipe(take(1))
      .subscribe(
        (entitet) => {},
        (err: HttpErrorResponse) => {
          subject.error(err);
        },
        () => {
          subject.complete();
        }
      );

    return subject;
  };

  public update = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    let kontaktService = csfm.getCrudServiceForModel<Kontakt, number>(
      KONTAKT_SERVICE_FOR_MODEL_INTERFACE
    );
    kontaktService
      .update(entitetDict)
      .pipe(take(1))
      .subscribe(
        (entitet) => {},
        (err: HttpErrorResponse) => {
          subject.error(err);
        },
        () => {
          subject.complete();
        }
      );

    return subject;
  };

  public getHeaderValue = (): HeaderValue => {
    return {
      headerItems: [
        { context: 'id' },
        { context: 'stanje modela' },
        { context: 'mobilni' },
        { context: 'fiksni' },
        { context: 'univerzitet' },
        { context: 'fakultet' },
        { context: '' },
        { context: '' },
        { context: '' },
      ],
    };
  };

  public addRowToTable = (
    table: Table,
    detaljiFn: Function = () => {},
    izmeniFn: Function = () => {},
    obrisiFn: Function = () => {}
  ): void => {
    try {
      table.addChildValue({
        data: this._entitet,
        rowItems: [
          {
            context: this._entitet.getId(),
          },
          {
            context: this._entitet.getStanje(),
          },
          {
            context: this._entitet.getMobilni(),
          },
          {
            context: this._entitet.getFiksni(),
          },
          {
            context: this._entitet.getUniverzitet()
              ? this._entitet.getUniverzitet().getNaziv()
              : '/',
          },
          {
            context: this._entitet.getFakultet()
              ? this._entitet.getFakultet().getNaziv()
              : '/',
          },
          {
            context: 'detalji',
            button: { function: detaljiFn },
          },
          {
            context: 'izmeni',
            button: { function: izmeniFn },
          },
          {
            context:
              this._entitet.getStanje() !== StanjeModela.OBRISAN
                ? 'obrisi'
                : '',
            button:
              this._entitet.getStanje() !== StanjeModela.OBRISAN
                ? { function: obrisiFn }
                : undefined,
          },
        ],
      });
    } catch (error) {
      console.log(error);
    }
  };
}
