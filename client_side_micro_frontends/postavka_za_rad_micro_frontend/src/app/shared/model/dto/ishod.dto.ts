import { EntitetDTO } from './entitet.dto';
import { RealizacijaPredmetaDTO } from './realizacija_predmeta.dto';
import { EvaluacijaZnanjaDTO } from './evaluacija_znanja.dto';
import { NastavniMaterijalDTO } from './nastavni_materijal.dto';
import { ObrazovniCiljDTO } from './obrazovni_cilj.dto';

export declare interface IshodDTO extends EntitetDTO {
  opis: string;
  naslov: string;
  putanjaZaSliku: string;
  realizacijaPredmetaDTO: RealizacijaPredmetaDTO;
  evaluacijaZnanjaDTO: EvaluacijaZnanjaDTO;
  nastavniMaterijaliDTO: NastavniMaterijalDTO[];
  obrazovniCiljeviDTO: ObrazovniCiljDTO[];
}
