import { EntitetDTO } from './entitet.dto';
import { PitanjeDTO } from './pitanje.dto';

export declare interface OdgovorDTO extends EntitetDTO {
  sadrzaj: string;
  pitanjeDTO: PitanjeDTO;
}
