import { EntitetDTO } from './entitet.dto';
import { FakultetDTO } from './fakultet.dto';
import { UniverzitetDTO } from './univerzitet.dto';

export declare interface KontaktDTO extends EntitetDTO {
  mobilni: string;
  fiksni: string;
  fakultetDTO: FakultetDTO;
  univerzitetDTO: UniverzitetDTO;
}
