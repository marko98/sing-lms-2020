import { EntitetDTO } from './entitet.dto';
import { TipStudija } from '../enum/tip_studija.enum';
import { UniverzitetDTO } from './univerzitet.dto';
import { DogadjajKalendarDTO } from './dogadjaj_kalendar.dto';

export declare interface KalendarDTO extends EntitetDTO {
  pocetniDatum: string;
  krajnjiDatum: string;
  tipStudija: TipStudija;
  univerzitetDTO: UniverzitetDTO;
  dogadjajiKalendarDTO: DogadjajKalendarDTO[];
}
