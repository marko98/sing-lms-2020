import { EntitetDTO } from './entitet.dto';
import { AutorNastavniMaterijalDTO } from './autor_nastavni_materijal.dto';

export declare interface AutorDTO extends EntitetDTO {
  ime: string;
  prezime: string;
  autorNastavniMaterijaliDTO: AutorNastavniMaterijalDTO[];
}
