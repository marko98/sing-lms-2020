import { EntitetDTO } from './entitet.dto';
import { TipOdeljenja } from '../enum/tip_odeljenja.enum';
import { AdresaDTO } from './adresa.dto';
import { ProstorijaDTO } from './prostorija.dto';
import { FakultetDTO } from './fakultet.dto';
import { StudentskaSluzbaDTO } from './studentska_sluzba.dto';

export declare interface OdeljenjeDTO extends EntitetDTO {
  tip: TipOdeljenja;
  adresaDTO: AdresaDTO;
  prostorijeDTO: ProstorijaDTO[];
  fakultetDTO: FakultetDTO;
  studentskaSluzbaDTO: StudentskaSluzbaDTO;
}
