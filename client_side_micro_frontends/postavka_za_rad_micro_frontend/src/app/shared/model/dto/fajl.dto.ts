import { EntitetDTO } from './entitet.dto';
import { EvaluacijaZnanjaDTO } from './evaluacija_znanja.dto';
import { ObavestenjeDTO } from './obavestenje.dto';
import { NastavniMaterijalDTO } from './nastavni_materijal.dto';

export declare interface FajlDTO extends EntitetDTO {
  opis: string;
  url: string;
  evaluacijaZnanjaDTO: EvaluacijaZnanjaDTO;
  obavestenjeDTO: ObavestenjeDTO;
  nastavniMaterijalDTO: NastavniMaterijalDTO;
  naziv: string;
  tip: string;
}
