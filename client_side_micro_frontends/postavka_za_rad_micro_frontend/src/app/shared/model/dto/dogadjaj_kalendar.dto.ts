import { EntitetDTO } from './entitet.dto';
import { DogadjajDTO } from './dogadjaj.dto';
import { KalendarDTO } from './kalendar.dto';

export declare interface DogadjajKalendarDTO extends EntitetDTO {
  kalendarDTO: KalendarDTO;
  dogadjajDTO: DogadjajDTO;
}
