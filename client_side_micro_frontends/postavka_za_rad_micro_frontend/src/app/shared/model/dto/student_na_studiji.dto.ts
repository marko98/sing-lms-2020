import { EntitetDTO } from './entitet.dto';
import { StudentDTO } from './student.dto';
import { PohadjanjePredmetaDTO } from './pohadjanje_predmeta.dto';
import { GodinaStudijaDTO } from './godina_studija.dto';
import { IstrazivackiRadStudentNaStudijiDTO } from './istrazivacki_rad_student_na_studiji.dto';
import { DiplomskiRadDTO } from './diplomski_rad.dto';

export declare interface StudentNaStudijiDTO extends EntitetDTO {
  datumUpisa: string;
  brojIndeksa: string;
  studentDTO: StudentDTO;
  pohadjanjaPredmetaDTO: PohadjanjePredmetaDTO[];
  godinaStudijaDTO: GodinaStudijaDTO;
  istrazivackiRadoviStudentNaStudijiDTO: IstrazivackiRadStudentNaStudijiDTO[];
  diplomskiRadDTO: DiplomskiRadDTO;
}
