import { EntitetDTO } from './entitet.dto';
import { NastavnikDTO } from './nastavnik.dto';
import { NastavnikDiplomskiRadDTO } from './nastavnik_diplomski_rad.dto';
import { StudentNaStudijiDTO } from './student_na_studiji.dto';

export declare interface DiplomskiRadDTO extends EntitetDTO {
  tema: string;
  ocena: number;
  mentorDTO: NastavnikDTO;
  nastavniciDiplomskiRadDTO: NastavnikDiplomskiRadDTO[];
  studentNaStudijiDTO: StudentNaStudijiDTO;
}
