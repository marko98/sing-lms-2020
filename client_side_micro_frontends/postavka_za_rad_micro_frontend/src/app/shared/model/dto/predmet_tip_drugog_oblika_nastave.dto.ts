import { EntitetDTO } from './entitet.dto';
import { TipDrugogOblikaNastave } from '../enum/tip_drugog_oblika_nastave.enum';
import { PredmetDTO } from './predmet.dto';

export declare interface PredmetTipDrugogOblikaNastaveDTO extends EntitetDTO {
  tipDrugogOblikaNastave: TipDrugogOblikaNastave;
  predmetDTO: PredmetDTO;
}
