import { EntitetDTO } from './entitet.dto';
import { RealizacijaPredmetaDTO } from './realizacija_predmeta.dto';
import { IshodDTO } from './ishod.dto';

export declare interface ObrazovniCiljDTO extends EntitetDTO {
  opis: string;
  realizacijaPredmetaDTO: RealizacijaPredmetaDTO;
  ishodDTO: IshodDTO;
}
