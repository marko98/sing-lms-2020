import { Adresa } from './adresa.model';
import { Nastavnik } from './nastavnik.model';
import { Administrator } from './administrator.model';
import { StudentskiSluzbenik } from './studentski_sluzbenik.model';
import { Student } from './student.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';
import { RegistrovaniKorisnikRola } from './registrovani_korisnik_rola.model';
import { CrudServiceForModel } from '../../service/crud.service-for-model';
import { Subject } from 'rxjs';
import {
  Form,
  FormRow,
  ROW_ITEM_TYPE,
  FormFieldInputNumberInterface,
  FORM_FIELD_INPUT_TYPE,
  SelectInterface,
  VALIDATOR_NAMES,
} from 'my-angular-form';
import { Validators } from '@angular/forms';
import { HeaderValue, Table } from 'my-angular-table';
import { XmlExportable } from '../../service/xml.service';

export class RegistrovaniKorisnik
  implements Entitet<RegistrovaniKorisnik, number>, XmlExportable {
  private id: number;

  private korisnickoIme: string;

  private lozinka: string;

  private email: string;

  private datumRodjenja: Date;

  private jmbg: string;

  private ime: string;

  private prezime: string;

  private stanje: StanjeModela;

  private version: number;

  private adrese: Adresa[];

  private nastavnik: Nastavnik;

  private administrator: Administrator;

  private studentskiSluzbenik: StudentskiSluzbenik;

  private student: Student;

  private registrovaniKorisnikRole: RegistrovaniKorisnikRola[];

  private _formSubject: Subject<Form> = new Subject();

  public getFormSubject = (): Subject<Form> => {
    return this._formSubject;
  };

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getKorisnickoIme = (): string => {
    return this.korisnickoIme;
  };

  public setKorisnickoIme = (korisnickoIme: string): void => {
    this.korisnickoIme = korisnickoIme;
  };

  public getLozinka = (): string => {
    return this.lozinka;
  };

  public setLozinka = (lozinka: string): void => {
    this.lozinka = lozinka;
  };

  public getEmail = (): string => {
    return this.email;
  };

  public setEmail = (email: string): void => {
    this.email = email;
  };

  public getDatumRodjenja = (): Date => {
    return this.datumRodjenja;
  };

  public setDatumRodjenja = (datumRodjenja: Date): void => {
    this.datumRodjenja = datumRodjenja;
  };

  public getJmbg = (): string => {
    return this.jmbg;
  };

  public setJmbg = (jmbg: string): void => {
    this.jmbg = jmbg;
  };

  public getIme = (): string => {
    return this.ime;
  };

  public setIme = (ime: string): void => {
    this.ime = ime;
  };

  public getPrezime = (): string => {
    return this.prezime;
  };

  public setPrezime = (prezime: string): void => {
    this.prezime = prezime;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getAdrese = (): Adresa[] => {
    return this.adrese;
  };

  public setAdrese = (adrese: Adresa[]): void => {
    this.adrese = adrese;
  };

  public getNastavnik = (): Nastavnik => {
    return this.nastavnik;
  };

  public setNastavnik = (nastavnik: Nastavnik): void => {
    this.nastavnik = nastavnik;
  };

  public getAdministrator = (): Administrator => {
    return this.administrator;
  };

  public setAdministrator = (administrator: Administrator): void => {
    this.administrator = administrator;
  };

  public getStudentskiSluzbenik = (): StudentskiSluzbenik => {
    return this.studentskiSluzbenik;
  };

  public setStudentskiSluzbenik = (
    studentskiSluzbenik: StudentskiSluzbenik
  ): void => {
    this.studentskiSluzbenik = studentskiSluzbenik;
  };

  public getStudent = (): Student => {
    return this.student;
  };

  public setStudent = (student: Student): void => {
    this.student = student;
  };

  public getRegistrovaniKorisnikRole = (): RegistrovaniKorisnikRola[] => {
    return this.registrovaniKorisnikRole;
  };

  public setRegistrovaniKorisnikRole = (
    registrovaniKorisnikRole: RegistrovaniKorisnikRola[]
  ): void => {
    this.registrovaniKorisnikRole = registrovaniKorisnikRole;
  };

  public constructor() {
    this.adrese = [];
    this.registrovaniKorisnikRole = [];
  }

  public convertToJSON = (
    shouldHaveOwnKey: boolean = true,
    rootKeyFromOutsideJSON?: string
  ): object => {
    // private id: number;+

    // private korisnickoIme: string;+

    // private lozinka: string;+

    // private email: string;+

    // private datumRodjenja: Date;+

    // private jmbg: string;+

    // private ime: string;+

    // private prezime: string;+

    // private stanje: StanjeModela;+

    // private version: number;-

    // private adrese: Adresa[];-

    // private nastavnik: Nastavnik;+

    // private administrator: Administrator;

    // private studentskiSluzbenik: StudentskiSluzbenik;

    // private student: Student;+

    // private registrovaniKorisnikRole: RegistrovaniKorisnikRola[];-

    if (shouldHaveOwnKey) {
      return {
        id: this.id,
        stanje: this.stanje,
        ime: this.ime,
        prezime: this.prezime,
        korisnickoIme: this.korisnickoIme,
        datumRodjenja: this.datumRodjenja.toLocaleDateString(),
        email: this.email,
        lozinka: this.lozinka,
        jmbg: this.jmbg,
        student:
          rootKeyFromOutsideJSON && rootKeyFromOutsideJSON === 'student'
            ? ''
            : this.student
            ? this.student.convertToJSON(true, 'registrovaniKorisnik')
            : '',
        nastavnik:
          rootKeyFromOutsideJSON && rootKeyFromOutsideJSON === 'nastavnik'
            ? ''
            : this.nastavnik
            ? this.nastavnik.convertToJSON(true, 'registrovaniKorisnik')
            : '',
      };
    } else {
      return {
        registrovaniKorisnik: {
          id: this.id,
          stanje: this.stanje,
          ime: this.ime,
          prezime: this.prezime,
          korisnickoIme: this.korisnickoIme,
          datumRodjenja: this.datumRodjenja.toLocaleDateString(),
          email: this.email,
          lozinka: this.lozinka,
          jmbg: this.jmbg,
          student:
            rootKeyFromOutsideJSON && rootKeyFromOutsideJSON === 'student'
              ? ''
              : this.student
              ? this.student.convertToJSON(true, 'registrovaniKorisnik')
              : '',
          nastavnik:
            rootKeyFromOutsideJSON && rootKeyFromOutsideJSON === 'nastavnik'
              ? ''
              : this.nastavnik
              ? this.nastavnik.convertToJSON(true, 'registrovaniKorisnik')
              : '',
        },
      };
    }
  };
}
