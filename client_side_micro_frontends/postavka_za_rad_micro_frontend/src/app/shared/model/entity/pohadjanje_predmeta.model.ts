import { StudentNaStudiji } from './student_na_studiji.model';
import { RealizacijaPredmeta } from './realizacija_predmeta.model';
import { Polaganje } from './polaganje.model';
import { DatumPohadjanjaPredmeta } from './datum_pohadjanja_predmeta.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';
import { Identifikacija } from '../interface/identifikacija.interface';
import { CrudServiceForModel } from '../../service/crud.service-for-model';
import { Subject } from 'rxjs';
import {
  Form,
  FormRow,
  ROW_ITEM_TYPE,
  FormFieldInputNumberInterface,
  FORM_FIELD_INPUT_TYPE,
  SelectInterface,
  VALIDATOR_NAMES,
} from 'my-angular-form';
import { Validators } from '@angular/forms';
import { HeaderValue, Table } from 'my-angular-table';
import { XmlExportable } from '../../service/xml.service';

export class PohadjanjePredmeta
  implements Entitet<PohadjanjePredmeta, number>, XmlExportable {
  private id: number;

  private konacnaOcena: number;

  private stanje: StanjeModela;

  private version: number;

  private studentNaStudiji: StudentNaStudiji;

  private realizacijaPredmeta: RealizacijaPredmeta;

  private polaganja: Polaganje[];

  private prisustvo: DatumPohadjanjaPredmeta[];

  private _formSubject: Subject<Form> = new Subject();

  public getFormSubject = (): Subject<Form> => {
    return this._formSubject;
  };

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getKonacnaOcena = (): number => {
    return this.konacnaOcena;
  };

  public setKonacnaOcena = (konacnaOcena: number): void => {
    this.konacnaOcena = konacnaOcena;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getStudentNaStudiji = (): StudentNaStudiji => {
    return this.studentNaStudiji;
  };

  public setStudentNaStudiji = (studentNaStudiji: StudentNaStudiji): void => {
    this.studentNaStudiji = studentNaStudiji;
  };

  public getRealizacijaPredmeta = (): RealizacijaPredmeta => {
    return this.realizacijaPredmeta;
  };

  public setRealizacijaPredmeta = (
    realizacijaPredmeta: RealizacijaPredmeta
  ): void => {
    this.realizacijaPredmeta = realizacijaPredmeta;
  };

  public getPolaganja = (): Polaganje[] => {
    return this.polaganja;
  };

  public setPolaganja = (polaganja: Polaganje[]): void => {
    this.polaganja = polaganja;
  };

  public getPrisustvo = (): DatumPohadjanjaPredmeta[] => {
    return this.prisustvo;
  };

  public setPrisustvo = (prisustvo: DatumPohadjanjaPredmeta[]): void => {
    this.prisustvo = prisustvo;
  };

  public constructor() {
    this.polaganja = [];
    this.prisustvo = [];
  }

  public getPolaganje(identifikacija: Identifikacija<number>): Polaganje {
    throw new Error('Not Implmented');
  }

  public getDatumPohadjanjaPredmeta(
    identifikacija: Identifikacija<number>
  ): DatumPohadjanjaPredmeta {
    throw new Error('Not Implmented');
  }

  public convertToJSON = (
    shouldHaveOwnKey: boolean = true,
    rootKeyFromOutsideJSON?: string
  ): object => {
    // private id: number;+

    // private konacnaOcena: number;+

    // private stanje: StanjeModela;+

    // private version: number;-

    // private studentNaStudiji: StudentNaStudiji;+

    // private realizacijaPredmeta: RealizacijaPredmeta;+

    // private polaganja: Polaganje[];-

    // private prisustvo: DatumPohadjanjaPredmeta[];-

    if (shouldHaveOwnKey) {
      return {
        id: this.id,
        stanje: this.stanje,
        konacnaOcena: this.konacnaOcena,
        studentNaStudiji:
          rootKeyFromOutsideJSON &&
          rootKeyFromOutsideJSON === 'studentNaStudiji'
            ? ''
            : this.studentNaStudiji
            ? this.studentNaStudiji.convertToJSON(true, 'pohadjanjePredmeta')
            : '',
        realizacijaPredmeta:
          rootKeyFromOutsideJSON &&
          rootKeyFromOutsideJSON === 'realizacijaPredmeta'
            ? ''
            : this.realizacijaPredmeta
            ? this.realizacijaPredmeta.convertToJSON(true, 'pohadjanjePredmeta')
            : '',
      };
    } else {
      return {
        pohadjanjePredmeta: {
          id: this.id,
          stanje: this.stanje,
          konacnaOcena: this.konacnaOcena,
          studentNaStudiji:
            rootKeyFromOutsideJSON &&
            rootKeyFromOutsideJSON === 'studentNaStudiji'
              ? ''
              : this.studentNaStudiji
              ? this.studentNaStudiji.convertToJSON(true, 'pohadjanjePredmeta')
              : '',
          realizacijaPredmeta:
            rootKeyFromOutsideJSON &&
            rootKeyFromOutsideJSON === 'realizacijaPredmeta'
              ? ''
              : this.realizacijaPredmeta
              ? this.realizacijaPredmeta.convertToJSON(
                  true,
                  'pohadjanjePredmeta'
                )
              : '',
        },
      };
    }
  };
}
