import { Adresa } from './adresa.model';
import { Nastavnik } from './nastavnik.model';
import { Fakultet } from './fakultet.model';
import { Kalendar } from './kalendar.model';
import { StudentskaSluzba } from './studentska_sluzba.model';
import { Kontakt } from './kontakt.model';
import { Identifikacija } from '../interface/identifikacija.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';
import { Entitet } from '../interface/entitet.interface';
import { CrudServiceForModel } from '../../service/crud.service-for-model';
import { Subject } from 'rxjs';
import {
  Form,
  FormRow,
  ROW_ITEM_TYPE,
  FormFieldInputNumberInterface,
  FORM_FIELD_INPUT_TYPE,
  SelectInterface,
  VALIDATOR_NAMES,
} from 'my-angular-form';
import { Validators } from '@angular/forms';
import { HeaderValue, Table } from 'my-angular-table';

export class Univerzitet implements Entitet<Univerzitet, number> {
  private id: number;

  private naziv: string;

  private datumOsnivanja: Date;

  private opis: string;

  private stanje: StanjeModela;

  private version: number;

  private adrese: Adresa[];

  private rektor: Nastavnik;

  private fakulteti: Fakultet[];

  private kalendari: Kalendar[];

  private studentskaSluzba: StudentskaSluzba;

  private kontakti: Kontakt[];

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getNaziv = (): string => {
    return this.naziv;
  };

  public setNaziv = (naziv: string): void => {
    this.naziv = naziv;
  };

  public getDatumOsnivanja = (): Date => {
    return this.datumOsnivanja;
  };

  public setDatumOsnivanja = (datumOsnivanja: Date): void => {
    this.datumOsnivanja = datumOsnivanja;
  };

  public getOpis = (): string => {
    return this.opis;
  };

  public setOpis = (opis: string): void => {
    this.opis = opis;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getAdrese = (): Adresa[] => {
    return this.adrese;
  };

  public setAdrese = (adrese: Adresa[]): void => {
    this.adrese = adrese;
  };

  public getRektor = (): Nastavnik => {
    return this.rektor;
  };

  public setRektor = (rektor: Nastavnik): void => {
    this.rektor = rektor;
  };

  public getFakulteti = (): Fakultet[] => {
    return this.fakulteti;
  };

  public setFakulteti = (fakulteti: Fakultet[]): void => {
    this.fakulteti = fakulteti;
  };

  public getKalendari = (): Kalendar[] => {
    return this.kalendari;
  };

  public setKalendari = (kalendari: Kalendar[]): void => {
    this.kalendari = kalendari;
  };

  public getStudentskaSluzba = (): StudentskaSluzba => {
    return this.studentskaSluzba;
  };

  public setStudentskaSluzba = (studentskaSluzba: StudentskaSluzba): void => {
    this.studentskaSluzba = studentskaSluzba;
  };

  public getKontakti = (): Kontakt[] => {
    return this.kontakti;
  };

  public setKontakti = (kontakti: Kontakt[]): void => {
    this.kontakti = kontakti;
  };

  public constructor() {
    this.adrese = [];
    this.fakulteti = [];
    this.kalendari = [];
    this.kontakti = [];
  }

  public getAdresa(identifikacija: Identifikacija<number>): Adresa {
    throw new Error('Not Implmented');
  }

  public getFakultet(identifikacija: Identifikacija<number>): Fakultet {
    throw new Error('Not Implmented');
  }

  public getKalendar(identifikacija: Identifikacija<number>): Kalendar {
    throw new Error('Not Implmented');
  }

  public getKontakt(identifikacija: Identifikacija<number>): Kontakt {
    throw new Error('Not Implmented');
  }
}
