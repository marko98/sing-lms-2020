import { Odeljenje } from './odeljenje.model';
import { Univerzitet } from './univerzitet.model';
import { Nastavnik } from './nastavnik.model';
import { NastavnikFakultet } from './nastavnik_fakultet.model';
import { StudijskiProgram } from './studijski_program.model';
import { Kontakt } from './kontakt.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';
import { Identifikacija } from '../interface/identifikacija.interface';
import { CrudServiceForModel } from '../../service/crud.service-for-model';
import { Subject } from 'rxjs';
import {
  Form,
  FormRow,
  ROW_ITEM_TYPE,
  FormFieldInputNumberInterface,
  FORM_FIELD_INPUT_TYPE,
  SelectInterface,
  VALIDATOR_NAMES,
} from 'my-angular-form';
import { Validators } from '@angular/forms';
import { HeaderValue, Table } from 'my-angular-table';
import { XmlExportable } from '../../service/xml.service';

export class Fakultet implements Entitet<Fakultet, number>, XmlExportable {
  private id: number;

  private naziv: string;

  private opis: string;

  private stanje: StanjeModela;

  private version: number;

  private odeljenja: Odeljenje[];

  private univerzitet: Univerzitet;

  private dekan: Nastavnik;

  private nastavniciFakultet: NastavnikFakultet[];

  private studijskiProgrami: StudijskiProgram[];

  private kontakti: Kontakt[];

  private _formSubject: Subject<Form> = new Subject();

  public getFormSubject = (): Subject<Form> => {
    return this._formSubject;
  };

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getNaziv = (): string => {
    return this.naziv;
  };

  public setNaziv = (naziv: string): void => {
    this.naziv = naziv;
  };

  public getOpis = (): string => {
    return this.opis;
  };

  public setOpis = (opis: string): void => {
    this.opis = opis;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getOdeljenja = (): Odeljenje[] => {
    return this.odeljenja;
  };

  public setOdeljenja = (odeljenja: Odeljenje[]): void => {
    this.odeljenja = odeljenja;
  };

  public getUniverzitet = (): Univerzitet => {
    return this.univerzitet;
  };

  public setUniverzitet = (univerzitet: Univerzitet): void => {
    this.univerzitet = univerzitet;
  };

  public getDekan = (): Nastavnik => {
    return this.dekan;
  };

  public setDekan = (dekan: Nastavnik): void => {
    this.dekan = dekan;
  };

  public getNastavniciFakultet = (): NastavnikFakultet[] => {
    return this.nastavniciFakultet;
  };

  public setNastavniciFakultet = (
    nastavniciFakultet: NastavnikFakultet[]
  ): void => {
    this.nastavniciFakultet = nastavniciFakultet;
  };

  public getStudijskiProgrami = (): StudijskiProgram[] => {
    return this.studijskiProgrami;
  };

  public setStudijskiProgrami = (
    studijskiProgrami: StudijskiProgram[]
  ): void => {
    this.studijskiProgrami = studijskiProgrami;
  };

  public getKontakti = (): Kontakt[] => {
    return this.kontakti;
  };

  public setKontakti = (kontakti: Kontakt[]): void => {
    this.kontakti = kontakti;
  };

  public constructor() {
    this.odeljenja = [];
    this.nastavniciFakultet = [];
    this.studijskiProgrami = [];
    this.kontakti = [];
  }

  public getOdeljenje(identifikacija: Identifikacija<number>): Odeljenje {
    throw new Error('Not Implmented');
  }

  public getStudijskiProgram(
    identifikacija: Identifikacija<number>
  ): StudijskiProgram {
    throw new Error('Not Implmented');
  }

  public getNastavnikFakultet(
    identifikacija: Identifikacija<number>
  ): NastavnikFakultet {
    throw new Error('Not Implmented');
  }

  public getKontakt(identifikacija: Identifikacija<number>): Kontakt {
    throw new Error('Not Implmented');
  }

  public convertToJSON = (
    shouldHaveOwnKey: boolean = true,
    rootKeyFromOutsideJSON?: string
  ): object => {
    // private id: number;+

    // private naziv: string;+

    // private opis: string;+

    // private stanje: StanjeModela;+

    // private version: number;-

    // private odeljenja: Odeljenje[];

    // private univerzitet: Univerzitet;

    // private dekan: Nastavnik;

    // private nastavniciFakultet: NastavnikFakultet[];

    // private studijskiProgrami: StudijskiProgram[];+

    // private kontakti: Kontakt[];
    if (shouldHaveOwnKey) {
      return {
        id: this.id,
        stanje: this.stanje,
        naziv: this.naziv,
        opis: this.opis,
        studijskiProgrami: this.studijskiProgrami.map((s) =>
          rootKeyFromOutsideJSON &&
          rootKeyFromOutsideJSON === 'studijskiProgram'
            ? ''
            : s
            ? s.convertToJSON(false, 'fakultet')
            : ''
        ),
      };
    } else {
      return {
        fakultet: {
          id: this.id,
          stanje: this.stanje,
          naziv: this.naziv,
          opis: this.opis,
          studijskiProgrami: this.studijskiProgrami.map((s) =>
            rootKeyFromOutsideJSON &&
            rootKeyFromOutsideJSON === 'studijskiProgram'
              ? ''
              : s
              ? s.convertToJSON(false, 'fakultet')
              : ''
          ),
        },
      };
    }
  };
}
