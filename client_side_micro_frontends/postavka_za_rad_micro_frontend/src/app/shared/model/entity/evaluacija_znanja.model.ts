import { NastavnikEvaluacijaZnanja } from './nastavnik_evaluacija_znanja.model';
import { RealizacijaPredmeta } from './realizacija_predmeta.model';
import { Polaganje } from './polaganje.model';
import { Ishod } from './ishod.model';
import { Fajl } from './fajl.model';
import { Pitanje } from './pitanje.model';
import { EvaluacijaZnanjaNacinEvaluacije } from './evaluacija_znanja_nacin_evaluacije.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';
import { TipEvaluacije } from '../enum/tip_evaluacije.enum';
import { IspitniRok } from '../enum/ispitni_rok.enum';
import { Identifikacija } from '../interface/identifikacija.interface';
import { XmlExportable } from '../../service/xml.service';

export class EvaluacijaZnanja
  implements Entitet<EvaluacijaZnanja, number>, XmlExportable {
  private id: number;

  private pocetak: Date;

  private trajanje: number;

  private stanje: StanjeModela;

  private version: number;

  private nastavniciEvaluacijaZnanja: NastavnikEvaluacijaZnanja[];

  private realizacijaPredmeta: RealizacijaPredmeta;

  private polaganja: Polaganje[];

  private ishod: Ishod;

  private fajlovi: Fajl[];

  private tipEvaluacije: TipEvaluacije;

  private pitanja: Pitanje[];

  private ispitniRok: IspitniRok;

  private evaluacijaZnanjaNaciniEvaluacije: EvaluacijaZnanjaNacinEvaluacije[];

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getPocetak = (): Date => {
    return this.pocetak;
  };

  public setPocetak = (pocetak: Date): void => {
    this.pocetak = pocetak;
  };

  public getTrajanje = (): number => {
    return this.trajanje;
  };

  public setTrajanje = (trajanje: number): void => {
    this.trajanje = trajanje;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getNastavniciEvaluacijaZnanja = (): NastavnikEvaluacijaZnanja[] => {
    return this.nastavniciEvaluacijaZnanja;
  };

  public setNastavniciEvaluacijaZnanja = (
    nastavniciEvaluacijaZnanja: NastavnikEvaluacijaZnanja[]
  ): void => {
    this.nastavniciEvaluacijaZnanja = nastavniciEvaluacijaZnanja;
  };

  public getRealizacijaPredmeta = (): RealizacijaPredmeta => {
    return this.realizacijaPredmeta;
  };

  public setRealizacijaPredmeta = (
    realizacijaPredmeta: RealizacijaPredmeta
  ): void => {
    this.realizacijaPredmeta = realizacijaPredmeta;
  };

  public getPolaganja = (): Polaganje[] => {
    return this.polaganja;
  };

  public setPolaganja = (polaganja: Polaganje[]): void => {
    this.polaganja = polaganja;
  };

  public getIshod = (): Ishod => {
    return this.ishod;
  };

  public setIshod = (ishod: Ishod): void => {
    this.ishod = ishod;
  };

  public getFajlovi = (): Fajl[] => {
    return this.fajlovi;
  };

  public setFajlovi = (fajlovi: Fajl[]): void => {
    this.fajlovi = fajlovi;
  };

  public getTipEvaluacije = (): TipEvaluacije => {
    return this.tipEvaluacije;
  };

  public setTipEvaluacije = (tipEvaluacije: TipEvaluacije): void => {
    this.tipEvaluacije = tipEvaluacije;
  };

  public getPitanja = (): Pitanje[] => {
    return this.pitanja;
  };

  public setPitanja = (pitanja: Pitanje[]): void => {
    this.pitanja = pitanja;
  };

  public getIspitniRok = (): IspitniRok => {
    return this.ispitniRok;
  };

  public setIspitniRok = (ispitniRok: IspitniRok): void => {
    this.ispitniRok = ispitniRok;
  };

  public getEvaluacijaZnanjaNaciniEvaluacije = (): EvaluacijaZnanjaNacinEvaluacije[] => {
    return this.evaluacijaZnanjaNaciniEvaluacije;
  };

  public setEvaluacijaZnanjaNaciniEvaluacije = (
    evaluacijaZnanjaNaciniEvaluacije: EvaluacijaZnanjaNacinEvaluacije[]
  ): void => {
    this.evaluacijaZnanjaNaciniEvaluacije = evaluacijaZnanjaNaciniEvaluacije;
  };

  public constructor() {
    this.nastavniciEvaluacijaZnanja = [];
    this.polaganja = [];
    this.fajlovi = [];
    this.pitanja = [];
    this.evaluacijaZnanjaNaciniEvaluacije = [];
  }

  public getNastavnikEvaluacijaZnanja(
    identifikacija: Identifikacija<number>
  ): NastavnikEvaluacijaZnanja {
    throw new Error('Not Implmented');
  }

  public getPolaganje(identifikacija: Identifikacija<number>): Polaganje {
    throw new Error('Not Implmented');
  }

  public getEvaluacijaZnanjaNacinEvaluacije(
    identifikacija: Identifikacija<number>
  ): EvaluacijaZnanjaNacinEvaluacije {
    throw new Error('Not Implmented');
  }

  public getFajl(identifikacija: Identifikacija<number>): Fajl {
    throw new Error('Not Implmented');
  }

  public getPitanje(identifikacija: Identifikacija<number>): Pitanje {
    throw new Error('Not Implmented');
  }

  public convertToJSON = (
    shouldHaveOwnKey: boolean = true,
    rootKeyFromOutsideJSON?: string
  ): object => {
    // private id: number;+
    // private pocetak: Date;+
    // private trajanje: number;+
    // private stanje: StanjeModela;+
    // private version: number;-
    // private nastavniciEvaluacijaZnanja: NastavnikEvaluacijaZnanja[];-
    // private realizacijaPredmeta: RealizacijaPredmeta;
    // private polaganja: Polaganje[];+
    // private ishod: Ishod;
    // private fajlovi: Fajl[];-
    // private tipEvaluacije: TipEvaluacije;
    // private pitanja: Pitanje[];-
    // private ispitniRok: IspitniRok;
    // private evaluacijaZnanjaNaciniEvaluacije: EvaluacijaZnanjaNacinEvaluacije[];-

    if (shouldHaveOwnKey) {
      return {
        id: this.id,
        pocetak: this.pocetak.toLocaleDateString(),
        trajanje: this.trajanje,
        stanje: this.stanje,
        realizacijaPredmeta:
          rootKeyFromOutsideJSON &&
          rootKeyFromOutsideJSON === 'realizacijaPredmeta'
            ? ''
            : this.realizacijaPredmeta
            ? this.realizacijaPredmeta.convertToJSON(true, 'evaluacijaZnanja')
            : '',
        polaganja: this.polaganja.map((p) =>
          rootKeyFromOutsideJSON && rootKeyFromOutsideJSON === 'polaganje'
            ? ''
            : p
            ? p.convertToJSON(false, 'evaluacijaZnanja')
            : ''
        ),
      };
    } else {
      return {
        evaluacijaZnanja: {
          id: this.id,
          pocetak: this.pocetak.toLocaleDateString(),
          trajanje: this.trajanje,
          stanje: this.stanje,
          realizacijaPredmeta:
            rootKeyFromOutsideJSON &&
            rootKeyFromOutsideJSON === 'realizacijaPredmeta'
              ? ''
              : this.realizacijaPredmeta
              ? this.realizacijaPredmeta.convertToJSON(true, 'evaluacijaZnanja')
              : '',
          polaganja: this.polaganja.map((p) =>
            rootKeyFromOutsideJSON && rootKeyFromOutsideJSON === 'polaganje'
              ? ''
              : p
              ? p.convertToJSON(false, 'evaluacijaZnanja')
              : ''
          ),
        },
      };
    }
  };
}
