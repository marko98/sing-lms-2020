import { RegistrovaniKorisnik } from './registrovani_korisnik.model';
import { StudentNaStudiji } from './student_na_studiji.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';
import { Identifikacija } from '../interface/identifikacija.interface';
import { CrudServiceForModel } from '../../service/crud.service-for-model';
import { Subject } from 'rxjs';
import {
  Form,
  FormRow,
  ROW_ITEM_TYPE,
  FormFieldInputNumberInterface,
  FORM_FIELD_INPUT_TYPE,
  SelectInterface,
  VALIDATOR_NAMES,
} from 'my-angular-form';
import { Validators } from '@angular/forms';
import { HeaderValue, Table } from 'my-angular-table';
import { XmlExportable } from '../../service/xml.service';

export class Student implements Entitet<Student, number>, XmlExportable {
  private id: number;

  private stanje: StanjeModela;

  private version: number;

  private registrovaniKorisnik: RegistrovaniKorisnik;

  private studije: StudentNaStudiji[];

  private _formSubject: Subject<Form> = new Subject();

  public getFormSubject = (): Subject<Form> => {
    return this._formSubject;
  };

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getRegistrovaniKorisnik = (): RegistrovaniKorisnik => {
    return this.registrovaniKorisnik;
  };

  public setRegistrovaniKorisnik = (
    registrovaniKorisnik: RegistrovaniKorisnik
  ): void => {
    this.registrovaniKorisnik = registrovaniKorisnik;
  };

  public getStudije = (): StudentNaStudiji[] => {
    return this.studije;
  };

  public setStudije = (studije: StudentNaStudiji[]): void => {
    this.studije = studije;
  };

  public constructor() {
    this.studije = [];
  }

  public getStudija(identifikacija: Identifikacija<number>): StudentNaStudiji {
    throw new Error('Not Implmented');
  }

  public convertToJSON = (
    shouldHaveOwnKey: boolean = true,
    rootKeyFromOutsideJSON?: string
  ): object => {
    // private id: number;+

    // private stanje: StanjeModela;+

    // private version: number;-

    // private registrovaniKorisnik: RegistrovaniKorisnik;+

    // private studije: StudentNaStudiji[];+

    if (shouldHaveOwnKey) {
      return {
        id: this.id,
        stanje: this.stanje,
        registrovaniKorisnik:
          rootKeyFromOutsideJSON &&
          rootKeyFromOutsideJSON === 'registrovaniKorisnik'
            ? ''
            : this.registrovaniKorisnik
            ? this.registrovaniKorisnik.convertToJSON(true, 'student')
            : '',
        studije: this.studije.map((s) =>
          rootKeyFromOutsideJSON &&
          rootKeyFromOutsideJSON === 'studentNaStudiji'
            ? ''
            : s
            ? s.convertToJSON(false, 'student')
            : ''
        ),
      };
    } else {
      return {
        student: {
          id: this.id,
          stanje: this.stanje,
          registrovaniKorisnik:
            rootKeyFromOutsideJSON &&
            rootKeyFromOutsideJSON === 'registrovaniKorisnik'
              ? ''
              : this.registrovaniKorisnik
              ? this.registrovaniKorisnik.convertToJSON(true, 'student')
              : '',
          studije: this.studije.map((s) =>
            rootKeyFromOutsideJSON &&
            rootKeyFromOutsideJSON === 'studentNaStudiji'
              ? ''
              : s
              ? s.convertToJSON(false, 'student')
              : ''
          ),
        },
      };
    }
  };
}
