import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Entitet } from 'src/app/shared/model/interface/entitet.interface';
import { RealizacijaPredmeta } from 'src/app/shared/model/entity/realizacija_predmeta.model';
import { RealizacijaPredmetaService } from 'src/app/shared/service/entity/realizacija_predmeta_service.service';

@Injectable({ providedIn: 'root' })
export class RealizacijaPredmetaResolver
  implements Resolve<Entitet<RealizacijaPredmeta, number>> {
  constructor(
    private _realizacijaPredmetaService: RealizacijaPredmetaService
  ) {}

  resolve(
    route: ActivatedRouteSnapshot
  ):
    | Observable<Entitet<RealizacijaPredmeta, number>>
    | Promise<Entitet<RealizacijaPredmeta, number>>
    | Entitet<RealizacijaPredmeta, number> {
    return this._realizacijaPredmetaService.findOne({
      id: route.paramMap.get('id'),
    });
  }
}
