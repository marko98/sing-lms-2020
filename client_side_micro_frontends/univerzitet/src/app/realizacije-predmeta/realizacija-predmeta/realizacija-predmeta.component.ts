import { Component, OnInit, OnDestroy } from '@angular/core';
import { RealizacijaPredmeta } from 'src/app/shared/model/entity/realizacija_predmeta.model';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { NastavniMaterijal } from 'src/app/shared/model/entity/nastavni_materijal.model';
import { NastavniMaterijalService } from 'src/app/shared/service/entity/nastavni_materijal_service.service';
import { Ishod } from 'src/app/shared/model/entity/ishod.model';
import { take, tap } from 'rxjs/operators';
import { AutorNastavniMaterijalService } from 'src/app/shared/service/entity/autor_nastavni_materijal_service.service';
import { AutorNastavniMaterijal } from 'src/app/shared/model/entity/autor_nastavni_materijal.model';
import { Fajl } from 'src/app/shared/model/entity/fajl.model';
import { NastavnikNaRealizaciji } from 'src/app/shared/model/entity/nastavnik_na_realizaciji.model';
import { NastavnikNaRealizacijiService } from 'src/app/shared/service/entity/nastavnik_na_realizaciji_service.service';
import { RegistrovaniKorisnikService } from 'src/app/shared/service/entity/registrovani_korisnik_service.service';
import { NastavnikService } from 'src/app/shared/service/entity/nastavnik_service.service';
import { RegistrovaniKorisnik } from 'src/app/shared/model/entity/registrovani_korisnik.model';

@Component({
  selector: 'app-realizacija-predmeta',
  templateUrl: './realizacija-predmeta.component.html',
  styleUrls: ['./realizacija-predmeta.component.css'],
})
export class RealizacijaPredmetaComponent implements OnInit, OnDestroy {
  public realizacijaPredmeta: RealizacijaPredmeta;
  public nastavniciNaRealizaciji: NastavnikNaRealizaciji[];

  public waitingRequests: number = 0;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _location: Location,
    private _nastavniMaterijalService: NastavniMaterijalService,
    private _autorNastavniMaterijalService: AutorNastavniMaterijalService,
    private _nastavnikNaRealizacijiService: NastavnikNaRealizacijiService,
    private _registrovaniKorisnikService: RegistrovaniKorisnikService
  ) {}

  public onGoBack = (): void => {
    this._location.back();
  };

  public onDownloadFajl = (fajl: Fajl): void => {
    // console.log(fajl.getUrl());
    window.open(fajl.getUrl());
  };

  ngOnInit(): void {
    this.realizacijaPredmeta = this._activatedRoute.snapshot.data.realizacijaPredmeta;
    // console.log(this.realizacijaPredmeta);

    this.realizacijaPredmeta.getIshodi().forEach((ishod: Ishod) => {
      this._nastavniMaterijalService
        .findAllByIshodId(ishod.getId())
        .pipe(
          take(1),
          tap(() => {
            this.waitingRequests++;
          })
        )
        .subscribe(
          (nastavniMaterijali: NastavniMaterijal[]) => {
            ishod.setNastavniMaterijali(
              nastavniMaterijali
                .filter((e) => (<any>e).getStanje().toString() !== 'OBRISAN')
                .map((nm) => {
                  this._autorNastavniMaterijalService
                    .findAllByNastavniMaterijalId(nm.getId())
                    .pipe(
                      take(1),
                      tap(() => {
                        this.waitingRequests++;
                      })
                    )
                    .subscribe(
                      (anm: AutorNastavniMaterijal[]) => {
                        nm.setAutoriNastavniMaterijal(anm);
                      },
                      (err) => {},
                      () => {
                        this.waitingRequests--;
                      }
                    );
                  nm.setFajlovi(
                    nm
                      .getFajlovi()
                      .filter(
                        (e) => (<any>e).getStanje().toString() !== 'OBRISAN'
                      )
                  );
                  return nm;
                })
            );
          },
          (err) => {},
          () => {
            this.waitingRequests--;
          }
        );
    });

    this._nastavnikNaRealizacijiService
      .findAllByRealizacijaPredmetaId(this.realizacijaPredmeta.getId())
      .pipe(take(1))
      .subscribe((nastavniciNaRealizaciji: NastavnikNaRealizaciji[]) => {
        this.nastavniciNaRealizaciji = nastavniciNaRealizaciji
          .filter((e) => (<any>e).getStanje().toString() !== 'OBRISAN')
          .map((nnr) => {
            this._registrovaniKorisnikService
              .findOneByNastavnikId(nnr.getNastavnik().getId())
              .pipe(take(1))
              .subscribe((rk: RegistrovaniKorisnik) => {
                nnr.getNastavnik().setRegistrovaniKorisnik(rk);
              });
            return nnr;
          });
        // console.log(this.nastavniciNaRealizaciji);
      });

    console.log('RealizacijaPredmetaComponent init');
  }

  ngOnDestroy(): void {
    console.log('RealizacijaPredmetaComponent destroyed');
  }
}
