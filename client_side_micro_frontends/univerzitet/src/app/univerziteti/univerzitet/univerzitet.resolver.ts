import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Univerzitet } from 'src/app/shared/model/entity/univerzitet.model';
import { UniverzitetService } from 'src/app/shared/service/entity/univerzitet_service.service';
import { take } from 'rxjs/operators';
import { Entitet } from 'src/app/shared/model/interface/entitet.interface';

@Injectable({ providedIn: 'root' })
export class UniverzitetResolver
  implements Resolve<Entitet<Univerzitet, number>> {
  constructor(private _univerzitetService: UniverzitetService) {}

  resolve(
    route: ActivatedRouteSnapshot
  ):
    | Observable<Entitet<Univerzitet, number>>
    | Promise<Entitet<Univerzitet, number>>
    | Entitet<Univerzitet, number> {
    return this._univerzitetService.findOne({ id: route.paramMap.get('id') });
  }
}
