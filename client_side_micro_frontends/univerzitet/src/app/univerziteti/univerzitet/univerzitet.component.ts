import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Univerzitet } from 'src/app/shared/model/entity/univerzitet.model';
import { UIService } from 'src/app/shared/service/ui.service';
import { Adresa } from 'src/app/shared/model/entity/adresa.model';
import { Nastavnik } from 'src/app/shared/model/entity/nastavnik.model';
import { NastavnikService } from 'src/app/shared/service/entity/nastavnik_service.service';
import { take } from 'rxjs/operators';
import { Location } from '@angular/common';
import { Table } from 'my-angular-table';
import { Fakultet } from 'src/app/shared/model/entity/fakultet.model';
import { MICRO_FRONTENDS_PREFIX } from '../../shared/const';

@Component({
  selector: 'app-univerzitet',
  templateUrl: './univerzitet.component.html',
  styleUrls: ['./univerzitet.component.css'],
})
export class UniverzitetComponent implements OnInit, OnDestroy {
  public univerzitet: Univerzitet;
  public rektor: Nastavnik;
  public table: Table;

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _uiService: UIService,
    private _nastavnikService: NastavnikService,
    private _location: Location
  ) {}

  public onGoBack = (): void => {
    this._location.back();
  };

  public showFull = (context: string): void => {
    this._uiService.showSnackbar(context, null, 1000);
  };

  private _showMore = (fakulteti: Fakultet[]): void => {
    this._router.navigate(
      [
        MICRO_FRONTENDS_PREFIX.MF_2_NK,
        'fakulteti',
        fakulteti[0].getId().toString(),
      ],
      {
        relativeTo: this._activatedRoute,
      }
    );
  };

  private _onTablePopulate = (): void => {
    this.univerzitet
      .getFakulteti()
      .filter((e) => (<any>e).getStanje().toString() !== 'OBRISAN')
      .forEach((f) => {
        this.table.addChildValue({
          data: f,
          rowItems: [
            {
              context: f.getNaziv(),
              imgSrc: 'https://img.icons8.com/dusk/2x/student-center.png',
            },
            {
              context: 'o fakultetu',
              button: {
                function: this._showMore,
              },
              imgSrc: 'https://img.icons8.com/plasticine/2x/info.png',
            },
          ],
        });
      });
  };

  private _onTableInit = (): void => {
    this.table = new Table({
      header: { headerItems: [{ context: 'naziv' }, { context: '' }] },
      contextMenu: {
        contextMenuItems: [
          {
            context: 'o fakultetu',
            imgSrc: 'https://img.icons8.com/plasticine/2x/info.png',
            function: this._showMore,
          },
        ],
      },
    });

    this._onTablePopulate();
  };

  ngOnInit(): void {
    this.univerzitet = this._activatedRoute.snapshot.data.univerzitet;

    this._onTableInit();

    this._nastavnikService
      .findOne({ id: this.univerzitet.getRektor()?.getId() })
      .pipe(take(1))
      .subscribe((rektor: Nastavnik) => {
        this.rektor = rektor;
      });

    console.log('UniverzitetComponent init');
  }

  ngOnDestroy(): void {
    console.log('UniverzitetComponent destroyed');
  }
}
