import { Component, OnInit, OnDestroy } from '@angular/core';
import { UniverzitetService } from '../shared/service/entity/univerzitet_service.service';
import { Subscription } from 'rxjs';
import { Univerzitet } from '../shared/model/entity/univerzitet.model';
import { Table } from 'my-angular-table';
import { Router, ActivatedRoute } from '@angular/router';
import { MICRO_FRONTENDS_PREFIX } from '../shared/const';

@Component({
  selector: 'app-univerziteti',
  templateUrl: './univerziteti.component.html',
  styleUrls: ['./univerziteti.component.css'],
})
export class UniverzitetiComponent implements OnInit, OnDestroy {
  public univerziteti: Univerzitet[];
  public table: Table;

  private _univerzitetSubscription: Subscription;

  constructor(
    private _univerzitetService: UniverzitetService,
    private _router: Router,
    private _aRoute: ActivatedRoute
  ) {}

  public onGoToStudentRegistracija = (): void => {
    this._router.navigate([
      MICRO_FRONTENDS_PREFIX.MF_1,
      'student_registracija',
    ]);
  };

  public onGoToLogin = (): void => {
    this._router.navigate([MICRO_FRONTENDS_PREFIX.MF_4, 'login']);
  };

  public onGoToRegistrovaniKorisnikRegistracija = (): void => {
    this._router.navigate([
      MICRO_FRONTENDS_PREFIX.MF_1,
      'registrovani_korisnik_registracija',
    ]);
  };

  private _showMore = (univerziteti: Univerzitet[]): void => {
    this._router.navigate([univerziteti[0].getId().toString()], {
      relativeTo: this._aRoute,
    });
  };

  private _onInitTable = (): void => {
    this.table = new Table({
      header: {
        headerItems: [
          { context: 'naziv' },
          { context: 'datum osnivanja' },
          { context: '' },
        ],
      },
      contextMenu: {
        contextMenuItems: [
          {
            context: 'vidi vise',
            function: this._showMore,
            imgSrc: 'https://img.icons8.com/plasticine/2x/info.png',
          },
        ],
      },
    });

    this._populateTable();
  };

  private _populateTable = (): void => {
    this.univerziteti
      .filter((e) => (<any>e).getStanje().toString() !== 'OBRISAN')
      .forEach((f) => {
        this.table.addChildValue({
          data: f,
          rowItems: [
            {
              context: f.getNaziv(),
              imgSrc: 'https://img.icons8.com/dusk/2x/university.png',
            },
            { context: f.getDatumOsnivanja().toLocaleString().split(',')[0] },
            {
              context: 'o univerzitetu',
              button: { function: this._showMore },
              imgSrc: 'https://img.icons8.com/plasticine/2x/info.png',
            },
          ],
        });
      });
  };

  ngOnInit(): void {
    this._univerzitetSubscription = this._univerzitetService
      .getEntiteti()
      .subscribe((univerziteti: Univerzitet[]) => {
        setTimeout(() => {
          this.univerziteti = univerziteti;
          this._onInitTable();
        }, 400);
      });
    console.log('UniverzitetiComponent init');
  }

  ngOnDestroy(): void {
    if (this._univerzitetSubscription)
      this._univerzitetSubscription.unsubscribe();
    console.log('UniverzitetiComponent destroyed');
  }
}
