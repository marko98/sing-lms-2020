import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmptyRouteComponent } from './empty-route/empty-route.component';
import { APP_BASE_HREF } from '@angular/common';
import { UniverzitetiComponent } from './univerziteti/univerziteti.component';
import { UniverzitetComponent } from './univerziteti/univerzitet/univerzitet.component';
import { UniverzitetResolver } from './univerziteti/univerzitet/univerzitet.resolver';
import { FakultetComponent } from './fakulteti/fakultet/fakultet.component';
import { FakultetResolver } from './fakulteti/fakultet/fakultet.resolver';
import { StudijskiProgramComponent } from './studijski-programi/studijski-program/studijski-program.component';
import { StudijskiProgramResolver } from './studijski-programi/studijski-program/studijski-program.resolver';
import { PredmetComponent } from './predmeti/predmet/predmet.component';
import { PredmetResolver } from './predmeti/predmet/predmet.resolver';
import { RealizacijaPredmetaComponent } from './realizacije-predmeta/realizacija-predmeta/realizacija-predmeta.component';
import { RealizacijaPredmetaResolver } from './realizacije-predmeta/realizacija-predmeta/realizacija-predmeta.resolver';

// const routes: Routes = [
//   {
//     path: 'univerziteti',
//     children: [
//       {
//         path: ':id',
//         resolve: { univerzitet: UniverzitetResolver },
//         children: [
//           {
//             path: 'fakulteti/:id',
//             resolve: { fakultet: FakultetResolver },
//             children: [
//               {
//                 path: 'studijski_programi/:id',
//                 resolve: { studijskiProgram: StudijskiProgramResolver },
//                 children: [
//                   {
//                     path: 'predmeti/:id',
//                     component: PredmetComponent,
//                     resolve: { predmet: PredmetResolver },
//                   },
//                   {
//                     path: '',
//                     pathMatch: 'full',
//                     component: StudijskiProgramComponent,
//                   },
//                 ],
//               },
//               {
//                 path: '',
//                 pathMatch: 'full',
//                 component: FakultetComponent,
//               },
//             ],
//           },
//           {
//             path: '',
//             pathMatch: 'full',
//             component: UniverzitetComponent,
//           },
//         ],
//       },
//       {
//         path: '',
//         pathMatch: 'full',
//         component: UniverzitetiComponent,
//       },
//     ],
//   },
//   { path: '**', component: EmptyRouteComponent },
// ];

const routes: Routes = [
  {
    path: 'mf_2_nk',
    children: [
      {
        path: 'univerziteti',
        children: [
          {
            path: ':id',
            component: UniverzitetComponent,
            resolve: { univerzitet: UniverzitetResolver },
          },
          { path: '', pathMatch: 'full', component: UniverzitetiComponent },
        ],
      },
      {
        path: 'fakulteti',
        children: [
          {
            path: ':id',
            component: FakultetComponent,
            resolve: { fakultet: FakultetResolver },
          },
          { path: '', pathMatch: 'full', redirectTo: '/mf_2_nk/univerziteti' },
        ],
      },
      {
        path: 'studijski_programi',
        children: [
          {
            path: ':id',
            component: StudijskiProgramComponent,
            resolve: { studijskiProgram: StudijskiProgramResolver },
          },
          { path: '', pathMatch: 'full', redirectTo: '/mf_2_nk/univerziteti' },
        ],
      },
      {
        path: 'predmeti',
        children: [
          {
            path: ':id',
            component: PredmetComponent,
            resolve: { predmet: PredmetResolver },
          },
          { path: '', pathMatch: 'full', redirectTo: '/mf_2_nk/univerziteti' },
        ],
      },
      {
        path: 'realizacije_predmeta',
        children: [
          {
            path: ':id',
            component: RealizacijaPredmetaComponent,
            resolve: { realizacijaPredmeta: RealizacijaPredmetaResolver },
          },
          { path: '', pathMatch: 'full', redirectTo: '/mf_2_nk/univerziteti' },
        ],
      },
      { path: '**', redirectTo: 'univerziteti' },
    ],
  },
  { path: '**', component: EmptyRouteComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [{ provide: APP_BASE_HREF, useValue: '' }],
})
export class AppRoutingModule {}
