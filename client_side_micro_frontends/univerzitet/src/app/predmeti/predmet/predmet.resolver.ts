import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Entitet } from 'src/app/shared/model/interface/entitet.interface';
import { Predmet } from 'src/app/shared/model/entity/predmet.model';
import { PredmetService } from 'src/app/shared/service/entity/predmet_service.service';

@Injectable({ providedIn: 'root' })
export class PredmetResolver implements Resolve<Entitet<Predmet, number>> {
  constructor(private _predmetService: PredmetService) {}

  resolve(
    route: ActivatedRouteSnapshot
  ):
    | Observable<Entitet<Predmet, number>>
    | Promise<Entitet<Predmet, number>>
    | Entitet<Predmet, number> {
    return this._predmetService.findOne({ id: route.paramMap.get('id') });
  }
}
