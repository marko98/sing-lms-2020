import { Component, OnInit, OnDestroy } from '@angular/core';
import { Predmet } from 'src/app/shared/model/entity/predmet.model';
import { ActivatedRoute, Router } from '@angular/router';
import { RealizacijaPredmeta } from 'src/app/shared/model/entity/realizacija_predmeta.model';
import { RealizacijaPredmetaService } from 'src/app/shared/service/entity/realizacija_predmeta_service.service';
import { take } from 'rxjs/operators';
import { MICRO_FRONTENDS_PREFIX } from 'src/app/shared/const';
import { Location } from '@angular/common';

@Component({
  selector: 'app-predmet',
  templateUrl: './predmet.component.html',
  styleUrls: ['./predmet.component.css'],
})
export class PredmetComponent implements OnInit, OnDestroy {
  public predmet: Predmet;
  public realizacijaPredmeta: RealizacijaPredmeta;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _realizacijaPredmeta: RealizacijaPredmetaService,
    private _router: Router,
    private _location: Location
  ) {}

  public onGoToRealizaicijaPredmeta = (id: number): void => {
    this._router.navigate([
      MICRO_FRONTENDS_PREFIX.MF_2_NK,
      'realizacije_predmeta',
      id.toString(),
    ]);
  };

  public onGoBack = (): void => {
    this._location.back();
  };

  ngOnInit(): void {
    this.predmet = this._activatedRoute.snapshot.data.predmet;
    console.log(this.predmet);

    this._realizacijaPredmeta
      .findOne({ id: this.predmet.getRealizacijaPredmeta().getId() })
      .pipe(take(1))
      .subscribe((realizacijaPredmeta) => {
        this.realizacijaPredmeta = <RealizacijaPredmeta>realizacijaPredmeta;
        console.log(this.realizacijaPredmeta);
      });

    console.log('PredmetComponent init');
  }

  ngOnDestroy(): void {
    console.log('PredmetComponent destroyed');
  }
}
