import { Injectable } from '@angular/core';
import {
  CanActivate,
  CanActivateChild,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService, AuthUserModelInterface } from '../service/auth.service';
import { take, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate, CanActivateChild {
  constructor(private _authService: AuthService, private _router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    // console.log('CanActivate ', next.data.requiredRoles);
    return this._authService.getUser().pipe(
      take(1),
      map((authUser: AuthUserModelInterface) => {
        // console.log(authUser);
        if (authUser) {
          const requiredRoles = next.data.requiredRoles;
          if (this.intersection(requiredRoles, authUser.roles).size > 0) {
            return true;
          } else {
            return false;
          }
        } else {
          return this._router.createUrlTree(['/login']);
        }
      })
    );
  }
  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    // console.log('canActivateChild ', next.data.requiredRoles);
    return this._authService.getUser().pipe(
      take(1),
      map((authUser: AuthUserModelInterface) => {
        // console.log(authUser);
        if (authUser) {
          const requiredRoles = next.data.requiredRoles;
          if (this.intersection(requiredRoles, authUser.roles).size > 0) {
            return true;
          } else {
            return false;
          }
        } else {
          return this._router.createUrlTree(['/login']);
        }
      })
    );
  }

  private intersection = (a: Set<any>, b: Set<any>): Set<any> => {
    return new Set([...a].filter((x) => b.has(x)));
  };

  private diference = (a: Set<any>, b: Set<any>): Set<any> => {
    return new Set([...a].filter((x) => !b.has(x)));
  };
}
