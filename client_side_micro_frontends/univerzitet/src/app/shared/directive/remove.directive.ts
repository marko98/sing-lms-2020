import { Directive, ElementRef, Input, OnInit } from '@angular/core';
import { AuthService, AuthUserModelInterface } from '../service/auth.service';
import { take } from 'rxjs/operators';

@Directive({
  selector: '[appRemove]',
})
export class RemoveDirective implements OnInit {
  @Input() public reqRoles: string[] = [];
  private _reqRoles: Set<string>;

  constructor(private _elRef: ElementRef, private _authService: AuthService) {}

  ngOnInit() {
    this._reqRoles = new Set(this.reqRoles);

    this._authService
      .getUser()
      .pipe(take(1))
      .subscribe((authUser: AuthUserModelInterface) => {
        if (authUser) {
          if (!(this.intersection(this._reqRoles, authUser.roles).size > 0)) {
            (<HTMLElement>this._elRef.nativeElement).parentNode.removeChild(
              this._elRef.nativeElement
            );
          }
        } else {
          (<HTMLElement>this._elRef.nativeElement).parentNode.removeChild(
            this._elRef.nativeElement
          );
        }
      });
  }

  private intersection = (a: Set<any>, b: Set<any>): Set<any> => {
    return new Set([...a].filter((x) => b.has(x)));
  };
}
