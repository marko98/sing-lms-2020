import { SCHEMA, PORT, LOCALHOST, ZUUL_PREFIX } from '../const';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { share, tap, map, catchError, filter } from 'rxjs/operators';
import { UIService } from './ui.service';
import { Identifikacija } from '../model/interface/identifikacija.interface';
import { EntitetDTO } from '../model/dto/entitet.dto';
import { Entitet } from '../model/interface/entitet.interface';
import { Factory } from '../model/patterns/creational/factory_method/factory.model';
import { ID as MyID } from '../model/interface/id.interface';

export declare interface CRUD_SERVICE_FOR_MODEL_INTERFACE {
  factory: Factory;
  microservice: string;
  path: string;
}

export class CrudService<T extends Entitet<T, ID>, ID> {
  protected _url: string =
    SCHEMA + '://' + LOCALHOST + ':' + PORT + '/' + ZUUL_PREFIX + '/';
  protected _entiteti: BehaviorSubject<Entitet<T, ID>[]> = new BehaviorSubject(
    []
  );

  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService,
    protected _factory: Factory,
    microservice: string,
    path: string
  ) {
    this._url += microservice + '/' + path;
  }

  public getEntiteti = (): BehaviorSubject<Entitet<T, ID>[]> => {
    return this._entiteti;
  };

  // read all
  protected _findAll = (): Observable<Entitet<T, ID>[]> => {
    let observable = this._httpClient.get<EntitetDTO[]>(this._url).pipe(
      share(),
      map((entiteti) => entiteti.map((e) => this._factory.build(e))),
      catchError(this._onHandleError)
    );

    observable.subscribe((entiteti: T[]) => {
      this._entiteti.next(
        entiteti.filter((e) => (<any>e).getStanje().toString() !== 'OBRISAN')
      );
    }, this._onHandleError);

    return observable;
  };

  public findAll = (): Observable<Entitet<T, ID>[]> => {
    let observable = this._httpClient.get<EntitetDTO[]>(this._url).pipe(
      share(),
      map((entiteti) => entiteti.map((e) => this._factory.build(e))),
      catchError(this._onHandleError)
    );

    observable.subscribe((entiteti: T[]) => {
      this._entiteti.next(
        entiteti.filter((e) => (<any>e).getStanje().toString() !== 'OBRISAN')
      );
    }, this._onHandleError);

    return observable;
  };

  //   read one
  public findOne = (identifikacija: MyID): Observable<Entitet<T, ID>> => {
    return this._httpClient
      .get<EntitetDTO>(this._url + '/' + identifikacija.id.toString())
      .pipe(
        map((e) => this._factory.build(e)),
        catchError(this._onHandleError)
      );
  };

  public save = (entitet: any): Observable<Entitet<T, ID>> => {
    let observable: Observable<Entitet<T, ID>> = this._httpClient
      .post<EntitetDTO>(this._url, entitet)
      .pipe(
        share(),
        map((e) => this._factory.build(e)),
        catchError(this._onHandleError)
      );

    observable.subscribe((t: Entitet<T, ID>) => {
      let entiteti = this._entiteti.getValue();
      entiteti.push(t);

      // console.log(entiteti);
      this._entiteti.next(entiteti);
    }, this._onHandleError);
    return observable;
  };

  public update = (entitet: any): Observable<Entitet<T, ID>> => {
    let observable: Observable<Entitet<T, ID>> = this._httpClient
      .put<EntitetDTO>(this._url, entitet)
      .pipe(
        share(),
        map((e) => this._factory.build(e)),
        catchError(this._onHandleError)
      );

    observable.subscribe((updatedEntitet: Entitet<T, ID>) => {
      // console.log(updatedEntitet);
      let entiteti = [];

      this._entiteti.getValue().forEach((entitet: Entitet<T, ID>) => {
        if (entitet.getId().toString() === updatedEntitet.getId().toString()) {
          entiteti.push(updatedEntitet);
        } else {
          entiteti.push(entitet);
        }
      });

      // console.log(entiteti);
      this._entiteti.next(entiteti);
    }, this._onHandleError);

    return observable;
  };

  // delete by id
  public deleteByIdentificator = (
    identifikacija: Identifikacija<ID>
  ): Observable<void> => {
    let observable = this._httpClient
      .delete<void>(this._url + '/' + identifikacija.getId().toString())
      .pipe(share(), catchError(this._onHandleError));

    observable.subscribe((result) => {
      let entiteti = this._entiteti
        .getValue()
        .filter(
          (t: T) => t.getId().toString() !== identifikacija.getId().toString()
        );

      // console.log(entiteti);
      this._entiteti.next(entiteti);
    }, this._onHandleError);

    return observable;
  };

  //   delete by model
  public delete = (identifikacija: Identifikacija<ID>): Observable<void> => {
    return this.deleteByIdentificator(identifikacija);
  };

  protected _onHandleError = (resErr: HttpErrorResponse): Observable<never> => {
    let errorMessage = 'An unknown error happened.';

    if (!resErr.error || !resErr.error.error) {
      return throwError(errorMessage);
    }

    // moguci slucajevi
    switch (resErr.error.error.message) {
      case 'INVALID_PASSWORD':
        errorMessage = 'This password is not correct.';
        break;
    }

    this._uiService.showSnackbar(errorMessage, null, 1500);
    return throwError(errorMessage);
  };
}
