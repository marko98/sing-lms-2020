import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { SCHEMA, LOCALHOST, PORT, ZUUL_PREFIX } from '../const';
import { catchError, tap } from 'rxjs/operators';
import { BehaviorSubject, throwError, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { UIService } from './ui.service';

export declare interface TokenModelInterface {
  token: string;
}

export declare interface AuthUserModelInterface {
  username: string;
  roles: Set<string>;
  expirationDate: Date;
}

@Injectable({ providedIn: 'root' })
export class AuthService {
  private _url: string =
    SCHEMA + '://' + LOCALHOST + ':' + PORT + '/' + ZUUL_PREFIX + '/';
  private _user: BehaviorSubject<AuthUserModelInterface> = new BehaviorSubject(
    undefined
  );
  private _token: string = '';
  private _tokenExpirationTimer: any;

  constructor(
    private _httpClient: HttpClient,
    private _router: Router,
    private _uiService: UIService
  ) {}

  public getUser = (): BehaviorSubject<AuthUserModelInterface> => {
    return this._user;
  };

  public getToken = (): string => {
    return this._token;
  };

  public onLogin = (
    username: string,
    password: string
  ): Observable<TokenModelInterface> => {
    return this._httpClient
      .post<TokenModelInterface>(this._url + 'api/login', {
        username: username,
        password: password,
      })
      .pipe(
        catchError(this._onHandleError),
        tap(this._onHandleAuthentification)
      );
  };

  private _onHandleError = (resErr: HttpErrorResponse): Observable<never> => {
    let errorMessage = 'An unknown error happened.';

    if (!resErr.error || !resErr.error.error) {
      return throwError(errorMessage);
    }

    // moguci slucajevi
    switch (resErr.error.error.message) {
      case 'INVALID_PASSWORD':
        errorMessage = 'This password is not correct.';
        break;
    }

    this._uiService.showSnackbar(errorMessage, null, 1500);
    return throwError(errorMessage);
  };

  private _onHandleAuthentification = (
    tokenModelInterface: TokenModelInterface
  ): void => {
    this._token = tokenModelInterface.token;
    let decodedToken = JSON.parse(
      atob(tokenModelInterface.token.split('.')[1])
    );

    console.log(decodedToken);

    const expirationTimeMs = +decodedToken.exp * 1000 - decodedToken.created;
    const expirationDate = new Date(new Date().getTime() + expirationTimeMs);
    this._user.next({
      username: decodedToken.sub,
      roles: new Set(decodedToken.roles),
      expirationDate: expirationDate,
    });

    // console.log(this._user.value);

    this.onAutoLogout(expirationTimeMs);
    localStorage.setItem(
      'userData',
      JSON.stringify({
        ...this._user.value,
        roles: Array.from(this._user.value.roles),
      })
    );
    localStorage.setItem('token', JSON.stringify(this._token));
    this._router.navigate(['/home']);
  };

  public onAutoLogout = (expirationTimeMs: number): void => {
    setTimeout(() => {
      this.onLogout();
    }, expirationTimeMs);
  };

  public onLogout = (): void => {
    this._user.next(undefined);
    this._token = '';
    this._router.navigate(['/login']);
    localStorage.removeItem('userData');
    localStorage.removeItem('token');
    if (this._tokenExpirationTimer) {
      clearTimeout(this._tokenExpirationTimer);
    }
    this._tokenExpirationTimer = undefined;
  };

  public onAutoLogin = (): void => {
    const user: AuthUserModelInterface = JSON.parse(
      localStorage.getItem('userData')
    );

    if (!user) return;

    user.expirationDate = new Date(user.expirationDate);
    user.roles = new Set(user.roles);

    const token = JSON.parse(localStorage.getItem('token'));
    this._token = token;

    this._user.next(user);
    const expirationTimeMs =
      user.expirationDate.getTime() - new Date().getTime();
    this.onAutoLogout(expirationTimeMs);
    // this._router.navigate(['/home']);
  };
}
