import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { Rola } from '../../model/entity/rola.model';
import { RolaFactory } from '../../model/patterns/creational/factory_method/rola.factory';

@Injectable({ providedIn: 'root' })
export class RolaService extends CrudService<Rola, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      RolaFactory.getInstance(),
      MICROSERVICE.KORISNIK,
      PATH.ROLA
    );
    this._findAll();
  }
}
