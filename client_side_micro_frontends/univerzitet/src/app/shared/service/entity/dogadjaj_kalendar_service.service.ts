import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { DogadjajKalendar } from '../../model/entity/dogadjaj_kalendar.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { DogadjajKalendarFactory } from '../../model/patterns/creational/factory_method/dogadjaj_kalendar.factory';

@Injectable({ providedIn: 'root' })
export class DogadjajKalendarService extends CrudService<
  DogadjajKalendar,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      DogadjajKalendarFactory.getInstance(),
      MICROSERVICE.NASTAVA,
      PATH.DOGADJAJ_KALENDAR
    );
    this._findAll();
  }
}
