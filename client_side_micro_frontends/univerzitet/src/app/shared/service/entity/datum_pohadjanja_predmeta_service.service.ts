import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { DatumPohadjanjaPredmeta } from '../../model/entity/datum_pohadjanja_predmeta.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { DatumPohadjanjaPredmetaFactory } from '../../model/patterns/creational/factory_method/datum_pohadjanja_predmeta.factory';

@Injectable({ providedIn: 'root' })
export class DatumPohadjanjaPredmetaService extends CrudService<
  DatumPohadjanjaPredmeta,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      DatumPohadjanjaPredmetaFactory.getInstance(),
      MICROSERVICE.STUDENT,
      PATH.DATUM_POHADJANJA_PREDMETA
    );
    this._findAll();
  }
}
