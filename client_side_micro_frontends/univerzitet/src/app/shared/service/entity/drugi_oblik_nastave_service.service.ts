import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { DrugiOblikNastave } from '../../model/entity/drugi_oblik_nastave.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { DrugiOblikNastaveFactory } from '../../model/patterns/creational/factory_method/drugi_oblik_nastave.factory';

@Injectable({ providedIn: 'root' })
export class DrugiOblikNastaveService extends CrudService<
  DrugiOblikNastave,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      DrugiOblikNastaveFactory.getInstance(),
      MICROSERVICE.NASTAVA,
      PATH.DRUGI_OBLIK_NASTAVE
    );
    this._findAll();
  }
}
