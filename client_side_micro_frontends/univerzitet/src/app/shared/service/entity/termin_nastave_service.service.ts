import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { TerminNastave } from '../../model/entity/termin_nastave.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { TerminNastaveFactory } from '../../model/patterns/creational/factory_method/termin_nastave.factory';

@Injectable({ providedIn: 'root' })
export class TerminNastaveService extends CrudService<TerminNastave, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      TerminNastaveFactory.getInstance(),
      MICROSERVICE.NASTAVA,
      PATH.TERMIN_NASTAVE
    );
    this._findAll();
  }
}
