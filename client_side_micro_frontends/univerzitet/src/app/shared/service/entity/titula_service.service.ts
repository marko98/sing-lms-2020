import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { Titula } from '../../model/entity/titula.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { TitulaFactory } from '../../model/patterns/creational/factory_method/titula.factory';

@Injectable({ providedIn: 'root' })
export class TitulaService extends CrudService<Titula, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      TitulaFactory.getInstance(),
      MICROSERVICE.NASTAVNIK,
      PATH.TITULA
    );
    this._findAll();
  }
}
