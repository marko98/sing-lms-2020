import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { CrudService } from '../crud.service';
import { Administrator } from '../../model/entity/administrator.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { AdministratorFactory } from '../../model/patterns/creational/factory_method/administrator.factory';

@Injectable({ providedIn: 'root' })
export class AdministratorService extends CrudService<Administrator, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      AdministratorFactory.getInstance(),
      MICROSERVICE.ADMINISTRACIJA,
      PATH.ADMINISTRATOR
    );
    this._findAll();
  }
}
