import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { Autor } from '../../model/entity/autor.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { AutorFactory } from '../../model/patterns/creational/factory_method/autor.factory';

@Injectable({ providedIn: 'root' })
export class AutorService extends CrudService<Autor, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      AutorFactory.getInstance(),
      MICROSERVICE.NASTAVA,
      PATH.AUTOR
    );
    this._findAll();
  }
}
