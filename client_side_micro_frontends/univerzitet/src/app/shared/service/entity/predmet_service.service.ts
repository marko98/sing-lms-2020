import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { Predmet } from '../../model/entity/predmet.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { PredmetFactory } from '../../model/patterns/creational/factory_method/predmet.factory';

@Injectable({ providedIn: 'root' })
export class PredmetService extends CrudService<Predmet, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      PredmetFactory.getInstance(),
      MICROSERVICE.PREDMET,
      PATH.PREDMET
    );
    this._findAll();
  }
}
