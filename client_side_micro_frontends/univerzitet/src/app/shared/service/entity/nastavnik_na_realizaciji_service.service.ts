import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { NastavnikNaRealizaciji } from '../../model/entity/nastavnik_na_realizaciji.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { NastavnikNaRealizacijiFactory } from '../../model/patterns/creational/factory_method/nastavnik_na_realizaciji.factory';
import { NastavnikNaRealizacijiDTO } from '../../model/dto/nastavnik_na_realizaciji.dto';
import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class NastavnikNaRealizacijiService extends CrudService<
  NastavnikNaRealizaciji,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      NastavnikNaRealizacijiFactory.getInstance(),
      MICROSERVICE.NASTAVNIK,
      PATH.NASTAVNIK_NA_REALIZACIJI
    );
    this._findAll();
  }

  public findAllByRealizacijaPredmetaId = (
    id: number
  ): Observable<NastavnikNaRealizaciji[]> => {
    // console.log(this._url + '/realizacija_predmeta/' + id);
    return this._httpClient
      .get<NastavnikNaRealizacijiDTO[]>(
        this._url + '/realizacija_predmeta/' + id
      )
      .pipe(
        map((entiteti) =>
          entiteti.map((e) => <NastavnikNaRealizaciji>this._factory.build(e))
        ),
        catchError(this._onHandleError)
      );
  };
}
