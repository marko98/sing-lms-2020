import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { Nastavnik } from '../../model/entity/nastavnik.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { NastavnikFactory } from '../../model/patterns/creational/factory_method/nastavnik.factory';

@Injectable({ providedIn: 'root' })
export class NastavnikService extends CrudService<Nastavnik, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      NastavnikFactory.getInstance(),
      MICROSERVICE.NASTAVNIK,
      PATH.NASTAVNIK
    );
    this._findAll();
  }
}
