import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { Dogadjaj } from '../../model/entity/dogadjaj.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { DogadjajFactory } from '../../model/patterns/creational/factory_method/dogadjaj.factory';

@Injectable({ providedIn: 'root' })
export class DogadjajService extends CrudService<Dogadjaj, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      DogadjajFactory.getInstance(),
      MICROSERVICE.NASTAVA,
      PATH.DOGADJAJ
    );
    this._findAll();
  }
}
