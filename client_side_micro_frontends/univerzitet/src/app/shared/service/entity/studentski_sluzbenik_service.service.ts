import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { StudentskiSluzbenik } from '../../model/entity/studentski_sluzbenik.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { StudentskiSluzbenikFactory } from '../../model/patterns/creational/factory_method/studentski_sluzbenik.factory';

@Injectable({ providedIn: 'root' })
export class StudentskiSluzbenikService extends CrudService<
  StudentskiSluzbenik,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      StudentskiSluzbenikFactory.getInstance(),
      MICROSERVICE.ADMINISTRACIJA,
      PATH.STUDENTSKI_SLUZBENIK
    );
    this._findAll();
  }
}
