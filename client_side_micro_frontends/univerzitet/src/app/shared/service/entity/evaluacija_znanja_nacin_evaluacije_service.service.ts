import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { EvaluacijaZnanjaNacinEvaluacije } from '../../model/entity/evaluacija_znanja_nacin_evaluacije.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { EvaluacijaZnanjaNacinEvaluacijeFactory } from '../../model/patterns/creational/factory_method/evaluacija_znanja_nacin_evaluacije.factory';
@Injectable({ providedIn: 'root' })
export class EvaluacijaZnanjaNacinEvaluacijeService extends CrudService<
  EvaluacijaZnanjaNacinEvaluacije,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      EvaluacijaZnanjaNacinEvaluacijeFactory.getInstance(),
      MICROSERVICE.EVALUACIJA_ZNANJA,
      PATH.EVALUACIJA_ZNANJA_NACIN_EVALUACIJE
    );
    this._findAll();
  }
}
