import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { Fajl } from '../../model/entity/fajl.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { FajlFactory } from '../../model/patterns/creational/factory_method/fajl.factory';

@Injectable({ providedIn: 'root' })
export class FajlService extends CrudService<Fajl, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      FajlFactory.getInstance(),
      MICROSERVICE.OBAVESTENJE,
      PATH.FAJL
    );
    this._findAll();
  }
}
