import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { EvaluacijaZnanja } from '../../model/entity/evaluacija_znanja.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { EvaluacijaZnanjaFactory } from '../../model/patterns/creational/factory_method/evaluacija_znanja.factory';

@Injectable({ providedIn: 'root' })
export class EvaluacijaZnanjaService extends CrudService<
  EvaluacijaZnanja,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      EvaluacijaZnanjaFactory.getInstance(),
      MICROSERVICE.EVALUACIJA_ZNANJA,
      PATH.EVALUACIJA_ZNANJA
    );
    this._findAll();
  }
}
