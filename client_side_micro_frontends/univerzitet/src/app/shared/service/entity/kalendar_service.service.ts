import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { Kalendar } from '../../model/entity/kalendar.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { KalendarFactory } from '../../model/patterns/creational/factory_method/kalendar.factory';

@Injectable({ providedIn: 'root' })
export class KalendarService extends CrudService<Kalendar, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      KalendarFactory.getInstance(),
      MICROSERVICE.NASTAVA,
      PATH.KALENDAR
    );
    this._findAll();
  }
}
