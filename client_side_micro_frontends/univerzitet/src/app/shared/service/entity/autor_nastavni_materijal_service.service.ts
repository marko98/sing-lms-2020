import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { AutorNastavniMaterijal } from '../../model/entity/autor_nastavni_materijal.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { AutorNastavniMaterijalFactory } from '../../model/patterns/creational/factory_method/autor_nastavni_materijal.factory';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { AutorNastavniMaterijalDTO } from '../../model/dto/autor_nastavni_materijal.dto';

@Injectable({ providedIn: 'root' })
export class AutorNastavniMaterijalService extends CrudService<
  AutorNastavniMaterijal,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      AutorNastavniMaterijalFactory.getInstance(),
      MICROSERVICE.NASTAVA,
      PATH.AUTOR_NASTAVNI_MATERIJAL
    );
    this._findAll();
  }

  public findAllByNastavniMaterijalId = (
    id: number
  ): Observable<AutorNastavniMaterijal[]> => {
    // console.log(this._url + '/nastavni_materijal/' + id);
    return this._httpClient
      .get<AutorNastavniMaterijalDTO[]>(this._url + '/nastavni_materijal/' + id)
      .pipe(
        map((entiteti) =>
          entiteti.map((e) => <AutorNastavniMaterijal>this._factory.build(e))
        ),
        catchError(this._onHandleError)
      );
  };
}
