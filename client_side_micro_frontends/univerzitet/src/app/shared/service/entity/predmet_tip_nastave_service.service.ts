import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { PredmetTipNastave } from '../../model/entity/predmet_tip_nastave.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { PredmetTipNastaveFactory } from '../../model/patterns/creational/factory_method/predmet_tip_nastave.factory';

@Injectable({ providedIn: 'root' })
export class PredmetTipNastaveService extends CrudService<
  PredmetTipNastave,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      PredmetTipNastaveFactory.getInstance(),
      MICROSERVICE.PREDMET,
      PATH.PREDMET_TIP_NASTAVE
    );
    this._findAll();
  }
}
