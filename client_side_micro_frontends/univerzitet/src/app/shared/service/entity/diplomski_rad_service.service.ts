import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { DiplomskiRad } from '../../model/entity/diplomski_rad.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { DiplomskiRadFactory } from '../../model/patterns/creational/factory_method/diplomski_rad.factory';

@Injectable({ providedIn: 'root' })
export class DiplomskiRadService extends CrudService<DiplomskiRad, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      DiplomskiRadFactory.getInstance(),
      MICROSERVICE.STUDENT,
      PATH.DIPLOMSKI_RAD
    );
    this._findAll();
  }
}
