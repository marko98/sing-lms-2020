import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { StudijskiProgram } from '../../model/entity/studijski_program.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { StudijskiProgramFactory } from '../../model/patterns/creational/factory_method/studijski_program.factory';

@Injectable({ providedIn: 'root' })
export class StudijskiProgramService extends CrudService<
  StudijskiProgram,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      StudijskiProgramFactory.getInstance(),
      MICROSERVICE.NASTAVA,
      PATH.STUDIJSKI_PROGRAM
    );
    this._findAll();
  }
}
