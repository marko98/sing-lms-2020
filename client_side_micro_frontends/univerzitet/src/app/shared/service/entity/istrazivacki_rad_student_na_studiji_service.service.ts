import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { IstrazivackiRadStudentNaStudiji } from '../../model/entity/istrazivacki_rad_student_na_studiji.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { IstrazivackiRadStudentNaStudijiFactory } from '../../model/patterns/creational/factory_method/istrazivacki_rad_student_na_studiji.factory';

@Injectable({ providedIn: 'root' })
export class IstrazivackiRadStudentNaStudijiService extends CrudService<
  IstrazivackiRadStudentNaStudiji,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      IstrazivackiRadStudentNaStudijiFactory.getInstance(),
      MICROSERVICE.STUDENT,
      PATH.ISTRAZIVACKI_RAD_STUDENT_NA_STUDIJI
    );
    this._findAll();
  }
}
