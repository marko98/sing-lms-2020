import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { Silabus } from '../../model/entity/silabus.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { SilabusFactory } from '../../model/patterns/creational/factory_method/silabus.factory';

@Injectable({ providedIn: 'root' })
export class SilabusService extends CrudService<Silabus, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      SilabusFactory.getInstance(),
      MICROSERVICE.PREDMET,
      PATH.SILABUS
    );
    this._findAll();
  }
}
