import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { Pitanje } from '../../model/entity/pitanje.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { PitanjeFactory } from '../../model/patterns/creational/factory_method/pitanje.factory';

@Injectable({ providedIn: 'root' })
export class PitanjeService extends CrudService<Pitanje, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      PitanjeFactory.getInstance(),
      MICROSERVICE.EVALUACIJA_ZNANJA,
      PATH.PITANJE
    );
    this._findAll();
  }
}
