import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { RegistrovaniKorisnikRola } from '../../model/entity/registrovani_korisnik_rola.model';
import { RegistrovaniKorisnikRolaFactory } from '../../model/patterns/creational/factory_method/registrovani_korisnik_rola.factory';

@Injectable({ providedIn: 'root' })
export class RegistrovaniKorisnikRolaService extends CrudService<
  RegistrovaniKorisnikRola,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      RegistrovaniKorisnikRolaFactory.getInstance(),
      MICROSERVICE.KORISNIK,
      PATH.REGISTROVANI_KORISNIK_ROLA
    );
    this._findAll();
  }
}
