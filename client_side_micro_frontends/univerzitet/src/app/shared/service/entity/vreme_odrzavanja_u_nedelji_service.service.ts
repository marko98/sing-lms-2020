import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { VremeOdrzavanjaUNedelji } from '../../model/entity/vreme_odrzavanja_u_nedelji.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { VremeOdrzavanjaUNedeljiFactory } from '../../model/patterns/creational/factory_method/vreme_odrzavanja_u_nedelji.factory';

@Injectable({ providedIn: 'root' })
export class VremeOdrzavanjaUNedeljiService extends CrudService<
  VremeOdrzavanjaUNedelji,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      VremeOdrzavanjaUNedeljiFactory.getInstance(),
      MICROSERVICE.NASTAVA,
      PATH.VREME_ODRZAVANJA_U_NEDELJI
    );

    this._findAll();
  }
}
