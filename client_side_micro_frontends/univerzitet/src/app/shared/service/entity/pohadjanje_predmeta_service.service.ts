import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { PohadjanjePredmeta } from '../../model/entity/pohadjanje_predmeta.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { PohadjanjePredmetaFactory } from '../../model/patterns/creational/factory_method/pohadjanje_predmeta.factory';

@Injectable({ providedIn: 'root' })
export class PohadjanjePredmetaService extends CrudService<
  PohadjanjePredmeta,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      PohadjanjePredmetaFactory.getInstance(),
      MICROSERVICE.STUDENT,
      PATH.POHADJANJE_PREDMETA
    );
    this._findAll();
  }
}
