import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { Ishod } from '../../model/entity/ishod.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { IshodFactory } from '../../model/patterns/creational/factory_method/ishod.factory';
import { Observable } from 'rxjs';
import { IshodDTO } from '../../model/dto/ishod.dto';
import { map, catchError } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class IshodService extends CrudService<Ishod, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      IshodFactory.getInstance(),
      MICROSERVICE.OBAVESTENJE,
      PATH.ISHOD
    );
    this._findAll();
  }

  public findAllByRealizacijaPredmetaId = (id: number): Observable<Ishod[]> => {
    // console.log(this._url + '/realizacija_predmeta/' + id);
    return this._httpClient
      .get<IshodDTO[]>(this._url + '/realizacija_predmeta/' + id)
      .pipe(
        map((entiteti) => entiteti.map((e) => <Ishod>this._factory.build(e))),
        catchError(this._onHandleError)
      );
  };
}
