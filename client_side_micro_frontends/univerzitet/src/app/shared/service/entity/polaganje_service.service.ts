import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { Polaganje } from '../../model/entity/polaganje.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { PolaganjeFactory } from '../../model/patterns/creational/factory_method/polaganje.factory';

@Injectable({ providedIn: 'root' })
export class PolaganjeService extends CrudService<Polaganje, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      PolaganjeFactory.getInstance(),
      MICROSERVICE.EVALUACIJA_ZNANJA,
      PATH.POLAGANJE
    );
    this._findAll();
  }
}
