import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { Grad } from '../../model/entity/grad.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { GradFactory } from '../../model/patterns/creational/factory_method/grad.factory';

@Injectable({ providedIn: 'root' })
export class GradService extends CrudService<Grad, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      GradFactory.getInstance(),
      MICROSERVICE.ADRESA,
      PATH.GRAD
    );
    this._findAll();
  }
}
