import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { Odeljenje } from '../../model/entity/odeljenje.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { OdeljenjeFactory } from '../../model/patterns/creational/factory_method/odeljenje.factory';
import { Observable } from 'rxjs';
import { OdeljenjeDTO } from '../../model/dto/odeljenje.dto';
import { share, map, catchError } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class OdeljenjeService extends CrudService<Odeljenje, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      OdeljenjeFactory.getInstance(),
      MICROSERVICE.FAKULTET,
      PATH.ODELJENJE
    );
    this._findAll();
  }

  public findAllByFakultetId = (id: number): Observable<Odeljenje[]> => {
    // console.log(this._url + '/fakultet/' + id);
    return this._httpClient
      .get<OdeljenjeDTO[]>(this._url + '/fakultet/' + id)
      .pipe(
        map((entiteti) =>
          entiteti.map((e) => <Odeljenje>this._factory.build(e))
        ),
        catchError(this._onHandleError)
      );
  };
}
