import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { Konsultacija } from '../../model/entity/konsultacija.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { KonsultacijaFactory } from '../../model/patterns/creational/factory_method/konsultacija.factory';

@Injectable({ providedIn: 'root' })
export class KonsultacijaService extends CrudService<Konsultacija, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      KonsultacijaFactory.getInstance(),
      MICROSERVICE.NASTAVA,
      PATH.KONSULTACIJA
    );
    this._findAll();
  }
}
