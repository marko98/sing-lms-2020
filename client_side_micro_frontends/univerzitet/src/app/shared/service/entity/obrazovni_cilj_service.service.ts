import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { ObrazovniCilj } from '../../model/entity/obrazovni_cilj.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { ObrazovniCiljFactory } from '../../model/patterns/creational/factory_method/obrazovni_cilj.factory';

@Injectable({ providedIn: 'root' })
export class ObrazovniCiljService extends CrudService<ObrazovniCilj, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      ObrazovniCiljFactory.getInstance(),
      MICROSERVICE.PREDMET,
      PATH.OBRAZOVNI_CILJ
    );
    this._findAll();
  }
}
