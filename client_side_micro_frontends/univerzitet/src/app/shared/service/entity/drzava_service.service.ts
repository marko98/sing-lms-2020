import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { Drzava } from '../../model/entity/drzava.model';

import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { DrzavaFactory } from '../../model/patterns/creational/factory_method/drzava.factory';
@Injectable({ providedIn: 'root' })
export class DrzavaService extends CrudService<Drzava, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      DrzavaFactory.getInstance(),
      MICROSERVICE.ADRESA,
      PATH.DRZAVA
    );
    this._findAll();
  }
}
