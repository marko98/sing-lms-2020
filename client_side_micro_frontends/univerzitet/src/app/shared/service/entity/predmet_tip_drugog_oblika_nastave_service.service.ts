import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { PredmetTipDrugogOblikaNastave } from '../../model/entity/predmet_tip_drugog_oblika_nastave.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { PredmetTipDrugogOblikaNastaveFactory } from '../../model/patterns/creational/factory_method/predmet_tip_drugog_oblika_nastave.factory';

@Injectable({ providedIn: 'root' })
export class PredmetTipDrugogOblikaNastaveService extends CrudService<
  PredmetTipDrugogOblikaNastave,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      PredmetTipDrugogOblikaNastaveFactory.getInstance(),
      MICROSERVICE.PREDMET,
      PATH.PREDMET_TIP_DRUGOG_OBLIKA_NASTAVE
    );
    this._findAll();
  }
}
