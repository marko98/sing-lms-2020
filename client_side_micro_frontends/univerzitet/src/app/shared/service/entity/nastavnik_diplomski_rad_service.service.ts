import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { NastavnikDiplomskiRad } from '../../model/entity/nastavnik_diplomski_rad.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { NastavnikDiplomskiRadFactory } from '../../model/patterns/creational/factory_method/nastavnik_diplomski_rad.factory';

@Injectable({ providedIn: 'root' })
export class NastavnikDiplomskiRadService extends CrudService<
  NastavnikDiplomskiRad,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      NastavnikDiplomskiRadFactory.getInstance(),
      MICROSERVICE.NASTAVNIK,
      PATH.NASTAVNIK_DIPLOMSKI_RAD
    );
    this._findAll();
  }
}
