import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { Fakultet } from '../../model/entity/fakultet.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { FakultetFactory } from '../../model/patterns/creational/factory_method/fakultet.factory';
@Injectable({ providedIn: 'root' })
export class FakultetService extends CrudService<Fakultet, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      FakultetFactory.getInstance(),
      MICROSERVICE.FAKULTET,
      PATH.FAKULTET
    );
    this._findAll();
  }
}
