import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { PredmetUslovniPredmet } from '../../model/entity/predmet_uslovni_predmet.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { PredmetUslovniPredmetFactory } from '../../model/patterns/creational/factory_method/predmet_uslovni_predmet.factory';

@Injectable({ providedIn: 'root' })
export class PredmetUslovniPredmetService extends CrudService<
  PredmetUslovniPredmet,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      PredmetUslovniPredmetFactory.getInstance(),
      MICROSERVICE.PREDMET,
      PATH.PREDMET_USLOVNI_PREDMET
    );
    this._findAll();
  }
}
