import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { RealizacijaPredmeta } from '../../model/entity/realizacija_predmeta.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { RealizacijaPredmetaFactory } from '../../model/patterns/creational/factory_method/realizacija_predmeta.factory';

@Injectable({ providedIn: 'root' })
export class RealizacijaPredmetaService extends CrudService<
  RealizacijaPredmeta,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      RealizacijaPredmetaFactory.getInstance(),
      MICROSERVICE.PREDMET,
      PATH.REALIZACIJA_PREDMETA
    );
    this._findAll();
  }
}
