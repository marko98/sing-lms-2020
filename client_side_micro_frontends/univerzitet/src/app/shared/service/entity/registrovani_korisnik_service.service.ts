import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { RegistrovaniKorisnik } from '../../model/entity/registrovani_korisnik.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { RegistrovaniKorisnikFactory } from '../../model/patterns/creational/factory_method/registrovani_korisnik.factory';
import { Observable } from 'rxjs';
import { RegistrovaniKorisnikDTO } from '../../model/dto/registrovani_korisnik.dto';
import { map, catchError } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class RegistrovaniKorisnikService extends CrudService<
  RegistrovaniKorisnik,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      RegistrovaniKorisnikFactory.getInstance(),
      MICROSERVICE.KORISNIK,
      PATH.REGISTROVANI_KORISNIK
    );
    this._findAll();
  }

  //   read one
  public findOneByNastavnikId = (
    id: number
  ): Observable<RegistrovaniKorisnik> => {
    return this._httpClient
      .get<RegistrovaniKorisnikDTO>(this._url + '/nastavnik/' + id.toString())
      .pipe(
        map((e) => <RegistrovaniKorisnik>this._factory.build(e)),
        catchError(this._onHandleError)
      );
  };
}
