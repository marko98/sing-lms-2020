import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { NaucnaOblast } from '../../model/entity/naucna_oblast.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { NaucnaOblastFactory } from '../../model/patterns/creational/factory_method/naucna_oblast.factory';

@Injectable({ providedIn: 'root' })
export class NaucnaOblastService extends CrudService<NaucnaOblast, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      NaucnaOblastFactory.getInstance(),
      MICROSERVICE.NASTAVNIK,
      PATH.NAUCNA_OBLAST
    );
    this._findAll();
  }
}
