import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { Student } from '../../model/entity/student.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { StudentFactory } from '../../model/patterns/creational/factory_method/student.factory';

@Injectable({ providedIn: 'root' })
export class StudentService extends CrudService<Student, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      StudentFactory.getInstance(),
      MICROSERVICE.STUDENT,
      PATH.STUDENT
    );
    this._findAll();
  }
}
