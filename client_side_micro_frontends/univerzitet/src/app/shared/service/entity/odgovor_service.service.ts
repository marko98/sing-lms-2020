import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { Odgovor } from '../../model/entity/odgovor.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { OdgovorFactory } from '../../model/patterns/creational/factory_method/odgovor.factory';

@Injectable({ providedIn: 'root' })
export class OdgovorService extends CrudService<Odgovor, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      OdgovorFactory.getInstance(),
      MICROSERVICE.EVALUACIJA_ZNANJA,
      PATH.ODGOVOR
    );
    this._findAll();
  }
}
