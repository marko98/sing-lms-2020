import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { StudentskaSluzba } from '../../model/entity/studentska_sluzba.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { StudentskaSluzbaFactory } from '../../model/patterns/creational/factory_method/studentska_sluzba.factory';

@Injectable({ providedIn: 'root' })
export class StudentskaSluzbaService extends CrudService<
  StudentskaSluzba,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      StudentskaSluzbaFactory.getInstance(),
      MICROSERVICE.ADMINISTRACIJA,
      PATH.STUDENTSKA_SLUZBA
    );
    this._findAll();
  }
}
