import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { IstrazivackiRad } from '../../model/entity/istrazivacki_rad.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { IstrazivackiRadFactory } from '../../model/patterns/creational/factory_method/istrazivacki_rad.factory';

@Injectable({ providedIn: 'root' })
export class IstrazivackiRadService extends CrudService<
  IstrazivackiRad,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      IstrazivackiRadFactory.getInstance(),
      MICROSERVICE.STUDENT,
      PATH.ISTRAZIVACKI_RAD
    );
    this._findAll();
  }
}
