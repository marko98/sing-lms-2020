import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { GodinaStudija } from '../../model/entity/godina_studija.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { GodinaStudijaFactory } from '../../model/patterns/creational/factory_method/godina_studija.factory';
import { Observable } from 'rxjs';
import { GodinaStudijaDTO } from '../../model/dto/godina_studija.dto';
import { map, catchError } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class GodinaStudijaService extends CrudService<GodinaStudija, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      GodinaStudijaFactory.getInstance(),
      MICROSERVICE.NASTAVA,
      PATH.GODINA_STUDIJA
    );
    this._findAll();
  }

  public findAllByStudijskiProgramId = (
    id: number
  ): Observable<GodinaStudija[]> => {
    // console.log(this._url + '/studijski_program/' + id);
    return this._httpClient
      .get<GodinaStudijaDTO[]>(this._url + '/studijski_program/' + id)
      .pipe(
        map((entiteti) =>
          entiteti.map((e) => <GodinaStudija>this._factory.build(e))
        ),
        catchError(this._onHandleError)
      );
  };
}
