import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { NastavnikFakultet } from '../../model/entity/nastavnik_fakultet.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { NastavnikFakultetFactory } from '../../model/patterns/creational/factory_method/nastavnik_fakultet.factory';

@Injectable({ providedIn: 'root' })
export class NastavnikFakultetService extends CrudService<
  NastavnikFakultet,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      NastavnikFakultetFactory.getInstance(),
      MICROSERVICE.NASTAVNIK,
      PATH.NASTAVNIK_FAKULTET
    );
    this._findAll();
  }
}
