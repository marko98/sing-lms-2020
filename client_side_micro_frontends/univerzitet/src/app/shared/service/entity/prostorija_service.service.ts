import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { Prostorija } from '../../model/entity/prostorija.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { ProstorijaFactory } from '../../model/patterns/creational/factory_method/prostorija.factory';

@Injectable({ providedIn: 'root' })
export class ProstorijaService extends CrudService<Prostorija, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      ProstorijaFactory.getInstance(),
      MICROSERVICE.NASTAVA,
      PATH.PROSTORIJA
    );
    this._findAll();
  }
}
