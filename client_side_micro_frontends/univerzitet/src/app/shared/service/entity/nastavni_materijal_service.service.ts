import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { NastavniMaterijal } from '../../model/entity/nastavni_materijal.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { NastavniMaterijalFactory } from '../../model/patterns/creational/factory_method/nastavni_materijal.factory';
import { Observable } from 'rxjs';
import { NastavniMaterijalDTO } from '../../model/dto/nastavni_materijal.dto';
import { map, catchError } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class NastavniMaterijalService extends CrudService<
  NastavniMaterijal,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      NastavniMaterijalFactory.getInstance(),
      MICROSERVICE.NASTAVA,
      PATH.NASTAVNI_MATERIJAL
    );
    this._findAll();
  }

  public findAllByIshodId = (id: number): Observable<NastavniMaterijal[]> => {
    // console.log(this._url + '/ishod/' + id);
    return this._httpClient
      .get<NastavniMaterijalDTO[]>(this._url + '/ishod/' + id)
      .pipe(
        map((entiteti) =>
          entiteti.map((e) => <NastavniMaterijal>this._factory.build(e))
        ),
        catchError(this._onHandleError)
      );
  };
}
