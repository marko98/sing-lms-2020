import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { GodinaStudijaObavestenje } from '../../model/entity/godina_studija_obavestenje.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { GodinaStudijaObavestenjeFactory } from '../../model/patterns/creational/factory_method/godina_studija_obavestenje.factory';

@Injectable({ providedIn: 'root' })
export class GodinaStudijaObavestenjeService extends CrudService<
  GodinaStudijaObavestenje,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      GodinaStudijaObavestenjeFactory.getInstance(),
      MICROSERVICE.OBAVESTENJE,
      PATH.GODINA_STUDIJA_OBAVESTENJE
    );
    this._findAll();
  }
}
