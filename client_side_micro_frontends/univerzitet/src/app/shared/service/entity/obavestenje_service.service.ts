import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { Obavestenje } from '../../model/entity/obavestenje.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { ObavestenjeFactory } from '../../model/patterns/creational/factory_method/obavestenje.factory';

@Injectable({ providedIn: 'root' })
export class ObavestenjeService extends CrudService<Obavestenje, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      ObavestenjeFactory.getInstance(),
      MICROSERVICE.OBAVESTENJE,
      PATH.OBAVESTENJE
    );
    this._findAll();
  }
}
