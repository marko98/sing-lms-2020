import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { NastavnikEvaluacijaZnanja } from '../../model/entity/nastavnik_evaluacija_znanja.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { NastavnikEvaluacijaZnanjaFactory } from '../../model/patterns/creational/factory_method/nastavnik_evaluacija_znanja.factory';

@Injectable({ providedIn: 'root' })
export class NastavnikEvaluacijaZnanjaService extends CrudService<
  NastavnikEvaluacijaZnanja,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      NastavnikEvaluacijaZnanjaFactory.getInstance(),
      MICROSERVICE.NASTAVNIK,
      PATH.NASTAVNIK_EVALUACIJA_ZNANJA
    );
    this._findAll();
  }
}
