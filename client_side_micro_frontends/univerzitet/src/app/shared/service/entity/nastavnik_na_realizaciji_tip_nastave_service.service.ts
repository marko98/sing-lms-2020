import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { NastavnikNaRealizacijiTipNastave } from '../../model/entity/nastavnik_na_realizaciji_tip_nastave.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { NastavnikNaRealizacijiTipNastaveFactory } from '../../model/patterns/creational/factory_method/nastavnik_na_realizaciji_tip_nastave.factory';

@Injectable({ providedIn: 'root' })
export class NastavnikNaRealizacijiTipNastaveService extends CrudService<
  NastavnikNaRealizacijiTipNastave,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      NastavnikNaRealizacijiTipNastaveFactory.getInstance(),
      MICROSERVICE.NASTAVNIK,
      PATH.NASTAVNIK_NA_REALIZACIJI_TIP_NASTAVE
    );
    this._findAll();
  }
}
