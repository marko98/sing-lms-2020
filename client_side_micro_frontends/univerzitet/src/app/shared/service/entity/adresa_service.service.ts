import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Adresa } from '../../model/entity/adresa.model';
import { CrudService } from '../crud.service';
import { UIService } from '../ui.service';
import { AdresaFactory } from '../../model/patterns/creational/factory_method/adresa.factory';

@Injectable({ providedIn: 'root' })
export class AdresaService extends CrudService<Adresa, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      AdresaFactory.getInstance(),
      MICROSERVICE.ADRESA,
      PATH.ADRESA
    );
    this._findAll();
  }
}
