import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { Univerzitet } from '../../model/entity/univerzitet.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { UniverzitetFactory } from '../../model/patterns/creational/factory_method/univerzitet.factory';

@Injectable({ providedIn: 'root' })
export class UniverzitetService extends CrudService<Univerzitet, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      UniverzitetFactory.getInstance(),
      MICROSERVICE.FAKULTET,
      PATH.UNIVERZITET
    );

    this._findAll();
  }
}
