import { CrudService } from '../crud.service';
import { PATH, MICROSERVICE } from '../../const';
import { Injectable } from '@angular/core';
import { StudentNaStudiji } from '../../model/entity/student_na_studiji.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { StudentNaStudijiFactory } from '../../model/patterns/creational/factory_method/student_na_studiji.factory';

@Injectable({ providedIn: 'root' })
export class StudentNaStudijiService extends CrudService<
  StudentNaStudiji,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      StudentNaStudijiFactory.getInstance(),
      MICROSERVICE.STUDENT,
      PATH.STUDENT_NA_STUDIJI
    );
    this._findAll();
  }
}
