import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MaterialModule } from './material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MyAngularFormModule } from 'my-angular-form';
import { MyAngularTableModule } from 'my-angular-table';
import { RemoveDirective } from './directive/remove.directive';
import { AssetUrlPipe } from './pipe/asset-url-pipe.pipe';
import { UniverzitetiComponent } from '../univerziteti/univerziteti.component';
import { UniverzitetComponent } from '../univerziteti/univerzitet/univerzitet.component';
import { FakultetiComponent } from '../fakulteti/fakulteti.component';
import { FakultetComponent } from '../fakulteti/fakultet/fakultet.component';
import { ModelObrisanDirective } from './directive/model-obrisan.directive';
import { StudijskiProgramiComponent } from '../studijski-programi/studijski-programi.component';
import { StudijskiProgramComponent } from '../studijski-programi/studijski-program/studijski-program.component';
import { PredmetiComponent } from '../predmeti/predmeti.component';
import { PredmetComponent } from '../predmeti/predmet/predmet.component';
import { RealizacijePredmetaComponent } from '../realizacije-predmeta/realizacije-predmeta.component';
import { RealizacijaPredmetaComponent } from '../realizacije-predmeta/realizacija-predmeta/realizacija-predmeta.component';

@NgModule({
  declarations: [
    RemoveDirective,
    AssetUrlPipe,
    UniverzitetiComponent,
    UniverzitetComponent,
    FakultetiComponent,
    FakultetComponent,
    ModelObrisanDirective,
    StudijskiProgramiComponent,
    StudijskiProgramComponent,
    PredmetiComponent,
    PredmetComponent,
    RealizacijePredmetaComponent,
    RealizacijaPredmetaComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MaterialModule,
    FlexLayoutModule,
    DragDropModule,
    MyAngularFormModule,
    MyAngularTableModule,
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MaterialModule,
    FlexLayoutModule,
    DragDropModule,
    MyAngularFormModule,
    MyAngularTableModule,

    RemoveDirective,
    ModelObrisanDirective,
    AssetUrlPipe,

    UniverzitetiComponent,
    UniverzitetComponent,
    FakultetiComponent,
    FakultetComponent,
    StudijskiProgramiComponent,
    StudijskiProgramComponent,
    PredmetiComponent,
    PredmetComponent,
    RealizacijePredmetaComponent,
    RealizacijaPredmetaComponent,
  ],
})
export class SharedModule {}
