/*
    observers nisu nista drugo do same funkcije koja treba da se pozove kada nad
    objektom tipa Observable na koju smo subscribe-ovani dodje do promena

    primer observer-a:

export class AppComponent implements OnInit, OnDestroy {

    articleObserver = (observable: ArticleRepository): void => {
        console.log(observable);
        this.articles = observable.getArticles();
    }

    // interfaces
    ngOnInit() {
        this.articleRepository.attach(this.articleObserver);

        this.articles = this.articleRepository.getArticles();
        console.log("AppComponent init");
    }

    ngOnDestroy() {
        if (this.articleRepository)
            this.articleRepository.dettach(this.articleObserver);
        console.log("AppComponent destroyed");
    }
}
*/

import { Observer } from './observer.interface';

export abstract class Observable {
  protected observers: Set<Observer | Function> = new Set();

  attach = (observer: Observer | Function): void => {
    if (!this.observers.has(observer)) this.observers.add(observer);
  };

  dettach = (observer: Observer | Function): void => {
    if (this.observers.has(observer)) this.observers.delete(observer);
  };

  protected notify = (): void => {
    for (let observer of this.observers) {
      // ne znam bas koliko ima smisla
      if (observer) {
        try {
          (<Observer>observer).update();
        } catch (error) {
          (<Function>observer)();
        }
      } else this.dettach(observer);
    }
  };
}
