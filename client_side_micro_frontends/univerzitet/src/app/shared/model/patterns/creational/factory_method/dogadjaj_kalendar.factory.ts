import { DogadjajKalendar } from '../../../entity/dogadjaj_kalendar.model';
import { DogadjajKalendarDTO } from '../../../dto/dogadjaj_kalendar.dto';
import { DogadjajFactory } from './dogadjaj.factory';
import { KalendarFactory } from './kalendar.factory';
import { Factory } from './factory.model';

export class DogadjajKalendarFactory extends Factory {
  public static _instance: DogadjajKalendarFactory;

  public static getInstance = (): DogadjajKalendarFactory => {
    if (!DogadjajKalendarFactory._instance)
      DogadjajKalendarFactory._instance = new DogadjajKalendarFactory();
    return <DogadjajKalendarFactory>DogadjajKalendarFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: DogadjajKalendarDTO): DogadjajKalendar {
    let dogadjajKalendar = new DogadjajKalendar();
    dogadjajKalendar.setId(dto.id);
    dogadjajKalendar.setStanje(dto.stanje);
    dogadjajKalendar.setVersion(dto.version);

    if (dto.dogadjajDTO)
      dogadjajKalendar.setDogadjaj(
        DogadjajFactory.getInstance().build(dto.dogadjajDTO)
      );
    if (dto.kalendarDTO)
      dogadjajKalendar.setKalendar(
        KalendarFactory.getInstance().build(dto.kalendarDTO)
      );

    return dogadjajKalendar;
  }
}
