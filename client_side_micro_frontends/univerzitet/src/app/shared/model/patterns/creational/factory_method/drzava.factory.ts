import { Drzava } from '../../../entity/drzava.model';
import { DrzavaDTO } from '../../../dto/drzava.dto';
import { GradFactory } from './grad.factory';
import { Factory } from './factory.model';

export class DrzavaFactory extends Factory {
  public static _instance: DrzavaFactory;
  public static getInstance = (): DrzavaFactory => {
    if (!DrzavaFactory._instance) DrzavaFactory._instance = new DrzavaFactory();
    return <DrzavaFactory>DrzavaFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: DrzavaDTO): Drzava {
    let drzava = new Drzava();
    drzava.setId(dto.id);
    drzava.setNaziv(dto.naziv);
    drzava.setStanje(dto.stanje);
    drzava.setVersion(dto.version);

    if (dto.gradoviDTO)
      drzava.setGradovi(
        dto.gradoviDTO.map((t) => GradFactory.getInstance().build(t))
      );

    return drzava;
  }
}
