import { EvaluacijaZnanja } from '../../../entity/evaluacija_znanja.model';
import { EvaluacijaZnanjaDTO } from '../../../dto/evaluacija_znanja.dto';
import { IshodFactory } from './ishod.factory';
import { RealizacijaPredmetaFactory } from './realizacija_predmeta.factory';
import { EvaluacijaZnanjaNacinEvaluacijeFactory } from './evaluacija_znanja_nacin_evaluacije.factory';
import { FajlFactory } from './fajl.factory';
import { NastavnikEvaluacijaZnanjaFactory } from './nastavnik_evaluacija_znanja.factory';
import { PolaganjeFactory } from './polaganje.factory';
import { PitanjeFactory } from './pitanje.factory';
import { Factory } from './factory.model';

export class EvaluacijaZnanjaFactory extends Factory {
  public static _instance: EvaluacijaZnanjaFactory;
  public static getInstance = (): EvaluacijaZnanjaFactory => {
    if (!EvaluacijaZnanjaFactory._instance)
      EvaluacijaZnanjaFactory._instance = new EvaluacijaZnanjaFactory();
    return <EvaluacijaZnanjaFactory>EvaluacijaZnanjaFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: EvaluacijaZnanjaDTO): EvaluacijaZnanja {
    let evaluacijaZnanja = new EvaluacijaZnanja();
    evaluacijaZnanja.setId(dto.id);
    evaluacijaZnanja.setIspitniRok(dto.ispitniRok);
    evaluacijaZnanja.setPocetak(new Date(dto.pocetak));
    evaluacijaZnanja.setStanje(dto.stanje);
    evaluacijaZnanja.setTipEvaluacije(dto.tipEvaluacije);
    evaluacijaZnanja.setTrajanje(dto.trajanje);
    evaluacijaZnanja.setVersion(dto.version);

    if (dto.ishodDTO)
      evaluacijaZnanja.setIshod(IshodFactory.getInstance().build(dto.ishodDTO));
    if (dto.realizacijaPredmetaDTO)
      evaluacijaZnanja.setRealizacijaPredmeta(
        RealizacijaPredmetaFactory.getInstance().build(
          dto.realizacijaPredmetaDTO
        )
      );

    if (dto.evaluacijaZnanjaNaciniEvaluacijeDTO)
      evaluacijaZnanja.setEvaluacijaZnanjaNaciniEvaluacije(
        dto.evaluacijaZnanjaNaciniEvaluacijeDTO.map((t) =>
          EvaluacijaZnanjaNacinEvaluacijeFactory.getInstance().build(t)
        )
      );

    if (dto.fajloviDTO)
      evaluacijaZnanja.setFajlovi(
        dto.fajloviDTO.map((t) => FajlFactory.getInstance().build(t))
      );

    if (dto.nastavniciEvaluacijaZnanjaDTO)
      evaluacijaZnanja.setNastavniciEvaluacijaZnanja(
        dto.nastavniciEvaluacijaZnanjaDTO.map((t) =>
          NastavnikEvaluacijaZnanjaFactory.getInstance().build(t)
        )
      );

    if (dto.polaganjaDTO)
      evaluacijaZnanja.setPolaganja(
        dto.polaganjaDTO.map((t) => PolaganjeFactory.getInstance().build(t))
      );

    if (dto.pitanjaDTO)
      evaluacijaZnanja.setPitanja(
        dto.pitanjaDTO.map((t) => PitanjeFactory.getInstance().build(t))
      );

    return evaluacijaZnanja;
  }
}
