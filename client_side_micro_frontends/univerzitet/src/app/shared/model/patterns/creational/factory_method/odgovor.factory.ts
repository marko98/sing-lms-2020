import { Odgovor } from '../../../entity/odgovor.model';
import { OdgovorDTO } from '../../../dto/odgovor.dto';
import { PitanjeFactory } from './pitanje.factory';
import { Factory } from './factory.model';

export class OdgovorFactory extends Factory {
  public static _instance: OdgovorFactory;
  public static getInstance = (): OdgovorFactory => {
    if (!OdgovorFactory._instance)
      OdgovorFactory._instance = new OdgovorFactory();
    return <OdgovorFactory>OdgovorFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: OdgovorDTO): Odgovor {
    let odgovor = new Odgovor();
    odgovor.setId(dto.id);
    odgovor.setSadrzaj(dto.sadrzaj);
    odgovor.setStanje(dto.stanje);
    odgovor.setVersion(dto.version);

    if (dto.pitanjeDTO)
      odgovor.setPitanje(PitanjeFactory.getInstance().build(dto.pitanjeDTO));
    return odgovor;
  }
}
