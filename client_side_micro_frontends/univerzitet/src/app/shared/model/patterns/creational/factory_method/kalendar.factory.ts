import { Kalendar } from '../../../entity/kalendar.model';
import { KalendarDTO } from '../../../dto/kalendar.dto';
import { UniverzitetFactory } from './univerzitet.factory';
import { DogadjajKalendarFactory } from './dogadjaj_kalendar.factory';
import { Factory } from './factory.model';

export class KalendarFactory extends Factory {
  public static _instance: KalendarFactory;
  public static getInstance = (): KalendarFactory => {
    if (!KalendarFactory._instance)
      KalendarFactory._instance = new KalendarFactory();
    return <KalendarFactory>KalendarFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: KalendarDTO): Kalendar {
    let kalendar = new Kalendar();
    kalendar.setId(dto.id);
    kalendar.setKrajnjiDatum(new Date(dto.krajnjiDatum));
    kalendar.setPocetniDatum(new Date(dto.pocetniDatum));
    kalendar.setStanje(dto.stanje);
    kalendar.setTipStudija(dto.tipStudija);
    kalendar.setVersion(dto.version);

    if (dto.univerzitetDTO)
      kalendar.setUniverzitet(
        UniverzitetFactory.getInstance().build(dto.univerzitetDTO)
      );

    if (dto.dogadjajiKalendarDTO)
      kalendar.setDogadjajiKalendar(
        dto.dogadjajiKalendarDTO.map((t) =>
          DogadjajKalendarFactory.getInstance().build(t)
        )
      );

    return kalendar;
  }
}
