import { NastavnikEvaluacijaZnanja } from '../../../entity/nastavnik_evaluacija_znanja.model';
import { NastavnikEvaluacijaZnanjaDTO } from '../../../dto/nastavnik_evaluacija_znanja.dto';
import { EvaluacijaZnanjaFactory } from './evaluacija_znanja.factory';
import { NastavnikFactory } from './nastavnik.factory';
import { Factory } from './factory.model';

export class NastavnikEvaluacijaZnanjaFactory extends Factory {
  public static _instance: NastavnikEvaluacijaZnanjaFactory;
  public static getInstance = (): NastavnikEvaluacijaZnanjaFactory => {
    if (!NastavnikEvaluacijaZnanjaFactory._instance)
      NastavnikEvaluacijaZnanjaFactory._instance = new NastavnikEvaluacijaZnanjaFactory();
    return <NastavnikEvaluacijaZnanjaFactory>(
      NastavnikEvaluacijaZnanjaFactory._instance
    );
  };

  private constructor() {
    super();
  }

  build(dto: NastavnikEvaluacijaZnanjaDTO): NastavnikEvaluacijaZnanja {
    let nastavnikEvaluacijaZnanja = new NastavnikEvaluacijaZnanja();
    nastavnikEvaluacijaZnanja.setId(dto.id);
    nastavnikEvaluacijaZnanja.setDatum(new Date(dto.datum));
    nastavnikEvaluacijaZnanja.setStanje(dto.stanje);
    nastavnikEvaluacijaZnanja.setVersion(dto.version);

    if (dto.evaluacijaZnanjaDTO)
      nastavnikEvaluacijaZnanja.setEvaluacijaZnanja(
        EvaluacijaZnanjaFactory.getInstance().build(dto.evaluacijaZnanjaDTO)
      );
    if (dto.nastavnikDTO)
      nastavnikEvaluacijaZnanja.setNastavnik(
        NastavnikFactory.getInstance().build(dto.nastavnikDTO)
      );

    return nastavnikEvaluacijaZnanja;
  }
}
