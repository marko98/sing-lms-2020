import { Administrator } from '../../../entity/administrator.model';
import { AdministratorDTO } from '../../../dto/administrator.dto';
import { RegistrovaniKorisnikFactory } from './registrovani_korisnik.factory';
import { Factory } from './factory.model';

export class AdministratorFactory extends Factory {
  public static _instance: AdministratorFactory;

  public static getInstance = (): AdministratorFactory => {
    if (!AdministratorFactory._instance)
      AdministratorFactory._instance = new AdministratorFactory();
    return <AdministratorFactory>AdministratorFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: AdministratorDTO): Administrator {
    let administrator = new Administrator();

    administrator.setId(dto.id);
    administrator.setStanje(dto.stanje);
    administrator.setVersion(dto.version);

    if (dto.registrovaniKorisnikDTO)
      administrator.setRegistrovaniKorisnik(
        RegistrovaniKorisnikFactory.getInstance().build(
          dto.registrovaniKorisnikDTO
        )
      );

    return administrator;
  }
}
