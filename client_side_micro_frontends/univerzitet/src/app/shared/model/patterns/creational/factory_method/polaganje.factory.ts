import { Polaganje } from '../../../entity/polaganje.model';
import { PolaganjeDTO } from '../../../dto/polaganje.dto';
import { EvaluacijaZnanjaFactory } from './evaluacija_znanja.factory';
import { PohadjanjePredmetaFactory } from './pohadjanje_predmeta.factory';
import { Factory } from './factory.model';

export class PolaganjeFactory extends Factory {
  public static _instance: PolaganjeFactory;
  public static getInstance = (): PolaganjeFactory => {
    if (!PolaganjeFactory._instance)
      PolaganjeFactory._instance = new PolaganjeFactory();
    return <PolaganjeFactory>PolaganjeFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: PolaganjeDTO): Polaganje {
    let polaganje = new Polaganje();
    polaganje.setId(dto.id);
    polaganje.setBodovi(dto.bodovi);
    polaganje.setNapomena(dto.napomena);
    polaganje.setStanje(dto.stanje);
    polaganje.setVersion(dto.version);

    if (dto.evaluacijaZnanjaDTO)
      polaganje.setEvaluacijaZnanja(
        EvaluacijaZnanjaFactory.getInstance().build(dto.evaluacijaZnanjaDTO)
      );
    if (dto.pohadjanjePredmetaDTO)
      polaganje.setPohadjanjePredmeta(
        PohadjanjePredmetaFactory.getInstance().build(dto.pohadjanjePredmetaDTO)
      );

    return polaganje;
  }
}
