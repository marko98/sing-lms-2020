import { NastavnikNaRealizacijiTipNastave } from '../../../entity/nastavnik_na_realizaciji_tip_nastave.model';
import { NastavnikNaRealizacijiTipNastaveDTO } from '../../../dto/nastavnik_na_realizaciji_tip_nastave.dto';
import { NastavnikNaRealizacijiFactory } from './nastavnik_na_realizaciji.factory';
import { Factory } from './factory.model';

export class NastavnikNaRealizacijiTipNastaveFactory extends Factory {
  public static _instance: NastavnikNaRealizacijiTipNastaveFactory;
  public static getInstance = (): NastavnikNaRealizacijiTipNastaveFactory => {
    if (!NastavnikNaRealizacijiTipNastaveFactory._instance)
      NastavnikNaRealizacijiTipNastaveFactory._instance = new NastavnikNaRealizacijiTipNastaveFactory();
    return <NastavnikNaRealizacijiTipNastaveFactory>(
      NastavnikNaRealizacijiTipNastaveFactory._instance
    );
  };

  private constructor() {
    super();
  }

  build(
    dto: NastavnikNaRealizacijiTipNastaveDTO
  ): NastavnikNaRealizacijiTipNastave {
    let nastavnikNaRealizacijiTipNastave = new NastavnikNaRealizacijiTipNastave();
    nastavnikNaRealizacijiTipNastave.setId(dto.id);
    nastavnikNaRealizacijiTipNastave.setStanje(dto.stanje);
    nastavnikNaRealizacijiTipNastave.setTipNastave(dto.tipNastave);
    nastavnikNaRealizacijiTipNastave.setVersion(dto.version);

    if (dto.nastavnikNaRealizacijiDTO)
      nastavnikNaRealizacijiTipNastave.setNastavnikNaRealizaciji(
        NastavnikNaRealizacijiFactory.getInstance().build(
          dto.nastavnikNaRealizacijiDTO
        )
      );

    return nastavnikNaRealizacijiTipNastave;
  }
}
