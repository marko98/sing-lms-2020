import { StudentskaSluzba } from '../../../entity/studentska_sluzba.model';
import { StudentskaSluzbaDTO } from '../../../dto/studentska_sluzba.dto';
import { OdeljenjeFactory } from './odeljenje.factory';
import { UniverzitetFactory } from './univerzitet.factory';
import { StudentskiSluzbenikFactory } from './studentski_sluzbenik.factory';
import { Factory } from './factory.model';

export class StudentskaSluzbaFactory extends Factory {
  public static _instance: StudentskaSluzbaFactory;
  public static getInstance = (): StudentskaSluzbaFactory => {
    if (!StudentskaSluzbaFactory._instance)
      StudentskaSluzbaFactory._instance = new StudentskaSluzbaFactory();
    return <StudentskaSluzbaFactory>StudentskaSluzbaFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: StudentskaSluzbaDTO): StudentskaSluzba {
    let studentskaSluzba = new StudentskaSluzba();
    studentskaSluzba.setId(dto.id);
    studentskaSluzba.setStanje(dto.stanje);
    studentskaSluzba.setVersion(dto.version);

    if (dto.odeljenjeDTO)
      studentskaSluzba.setOdeljenje(
        OdeljenjeFactory.getInstance().build(dto.odeljenjeDTO)
      );
    if (dto.univerzitetDTO)
      studentskaSluzba.setUniverzitet(
        UniverzitetFactory.getInstance().build(dto.univerzitetDTO)
      );

    if (dto.studentskiSluzbeniciDTO)
      studentskaSluzba.setStudentskiSluzbenici(
        dto.studentskiSluzbeniciDTO.map((t) =>
          StudentskiSluzbenikFactory.getInstance().build(t)
        )
      );

    return studentskaSluzba;
  }
}
