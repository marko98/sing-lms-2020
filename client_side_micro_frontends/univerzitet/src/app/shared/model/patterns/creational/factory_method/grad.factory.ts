import { Grad } from '../../../entity/grad.model';
import { GradDTO } from '../../../dto/grad.dto';
import { DrzavaFactory } from './drzava.factory';
import { AdresaFactory } from './adresa.factory';
import { Factory } from './factory.model';

export class GradFactory extends Factory {
  public static _instance: GradFactory;
  public static getInstance = (): GradFactory => {
    if (!GradFactory._instance) GradFactory._instance = new GradFactory();
    return <GradFactory>GradFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: GradDTO): Grad {
    let grad = new Grad();
    grad.setId(dto.id);
    grad.setNaziv(dto.naziv);
    grad.setStanje(dto.stanje);
    grad.setVersion(dto.version);

    if (dto.drzavaDTO)
      grad.setDrzava(DrzavaFactory.getInstance().build(dto.drzavaDTO));

    if (dto.adreseDTO)
      grad.setAdrese(
        dto.adreseDTO.map((t) => AdresaFactory.getInstance().build(t))
      );

    return grad;
  }
}
