import { IstrazivackiRadStudentNaStudiji } from './istrazivacki_rad_student_na_studiji.model';
import { Nastavnik } from './nastavnik.model';
import { RealizacijaPredmeta } from './realizacija_predmeta.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';
import { Identifikacija } from '../interface/identifikacija.interface';

export class IstrazivackiRad implements Entitet<IstrazivackiRad, number> {
  private id: number;

  private tema: string;

  private stanje: StanjeModela;

  private version: number;

  private istrazivackiRadStudentiNaStudijama: IstrazivackiRadStudentNaStudiji[];

  private nastavnik: Nastavnik;

  private realizacijaPredmeta: RealizacijaPredmeta;

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getTema = (): string => {
    return this.tema;
  };

  public setTema = (tema: string): void => {
    this.tema = tema;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getIstrazivackiRadStudentiNaStudijama = (): IstrazivackiRadStudentNaStudiji[] => {
    return this.istrazivackiRadStudentiNaStudijama;
  };

  public setIstrazivackiRadStudentiNaStudijama = (
    istrazivackiRadStudentiNaStudijama: IstrazivackiRadStudentNaStudiji[]
  ): void => {
    this.istrazivackiRadStudentiNaStudijama = istrazivackiRadStudentiNaStudijama;
  };

  public getNastavnik = (): Nastavnik => {
    return this.nastavnik;
  };

  public setNastavnik = (nastavnik: Nastavnik): void => {
    this.nastavnik = nastavnik;
  };

  public getRealizacijaPredmeta = (): RealizacijaPredmeta => {
    return this.realizacijaPredmeta;
  };

  public setRealizacijaPredmeta = (
    realizacijaPredmeta: RealizacijaPredmeta
  ): void => {
    this.realizacijaPredmeta = realizacijaPredmeta;
  };

  public constructor() {
    this.istrazivackiRadStudentiNaStudijama = [];
  }

  public getIstrazivackiRadStudentNaStudiji(
    identifikacija: Identifikacija<number>
  ): IstrazivackiRadStudentNaStudiji {
    throw new Error('Not Implmented');
  }
}
