import { StudentNaStudiji } from './student_na_studiji.model';
import { RealizacijaPredmeta } from './realizacija_predmeta.model';
import { Polaganje } from './polaganje.model';
import { DatumPohadjanjaPredmeta } from './datum_pohadjanja_predmeta.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';
import { Identifikacija } from '../interface/identifikacija.interface';

export class PohadjanjePredmeta implements Entitet<PohadjanjePredmeta, number> {
  private id: number;

  private konacnaOcena: number;

  private stanje: StanjeModela;

  private version: number;

  private studentNaStudiji: StudentNaStudiji;

  private realizacijaPredmeta: RealizacijaPredmeta;

  private polaganja: Polaganje[];

  private prisustvo: DatumPohadjanjaPredmeta[];

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getKonacnaOcena = (): number => {
    return this.konacnaOcena;
  };

  public setKonacnaOcena = (konacnaOcena: number): void => {
    this.konacnaOcena = konacnaOcena;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getStudentNaStudiji = (): StudentNaStudiji => {
    return this.studentNaStudiji;
  };

  public setStudentNaStudiji = (studentNaStudiji: StudentNaStudiji): void => {
    this.studentNaStudiji = studentNaStudiji;
  };

  public getRealizacijaPredmeta = (): RealizacijaPredmeta => {
    return this.realizacijaPredmeta;
  };

  public setRealizacijaPredmeta = (
    realizacijaPredmeta: RealizacijaPredmeta
  ): void => {
    this.realizacijaPredmeta = realizacijaPredmeta;
  };

  public getPolaganja = (): Polaganje[] => {
    return this.polaganja;
  };

  public setPolaganja = (polaganja: Polaganje[]): void => {
    this.polaganja = polaganja;
  };

  public getPrisustvo = (): DatumPohadjanjaPredmeta[] => {
    return this.prisustvo;
  };

  public setPrisustvo = (prisustvo: DatumPohadjanjaPredmeta[]): void => {
    this.prisustvo = prisustvo;
  };

  public constructor() {
    this.polaganja = [];
    this.prisustvo = [];
  }

  public getPolaganje(identifikacija: Identifikacija<number>): Polaganje {
    throw new Error('Not Implmented');
  }

  public getDatumPohadjanjaPredmeta(
    identifikacija: Identifikacija<number>
  ): DatumPohadjanjaPredmeta {
    throw new Error('Not Implmented');
  }
}
