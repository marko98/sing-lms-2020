import { Univerzitet } from './univerzitet.model';
import { DogadjajKalendar } from './dogadjaj_kalendar.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';
import { TipStudija } from '../enum/tip_studija.enum';
import { Identifikacija } from '../interface/identifikacija.interface';

export class Kalendar implements Entitet<Kalendar, number> {
  private id: number;

  private pocetniDatum: Date;

  private krajnjiDatum: Date;

  private stanje: StanjeModela;

  private version: number;

  private tipStudija: TipStudija;

  private univerzitet: Univerzitet;

  private dogadjajiKalendar: DogadjajKalendar[];

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getPocetniDatum = (): Date => {
    return this.pocetniDatum;
  };

  public setPocetniDatum = (pocetniDatum: Date): void => {
    this.pocetniDatum = pocetniDatum;
  };

  public getKrajnjiDatum = (): Date => {
    return this.krajnjiDatum;
  };

  public setKrajnjiDatum = (krajnjiDatum: Date): void => {
    this.krajnjiDatum = krajnjiDatum;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getTipStudija = (): TipStudija => {
    return this.tipStudija;
  };

  public setTipStudija = (tipStudija: TipStudija): void => {
    this.tipStudija = tipStudija;
  };

  public getUniverzitet = (): Univerzitet => {
    return this.univerzitet;
  };

  public setUniverzitet = (univerzitet: Univerzitet): void => {
    this.univerzitet = univerzitet;
  };

  public getDogadjajiKalendar = (): DogadjajKalendar[] => {
    return this.dogadjajiKalendar;
  };

  public setDogadjajiKalendar = (
    dogadjajiKalendar: DogadjajKalendar[]
  ): void => {
    this.dogadjajiKalendar = dogadjajiKalendar;
  };

  public constructor() {
    this.dogadjajiKalendar = [];
  }

  public getDogadjajKalendar(
    identifikacija: Identifikacija<number>
  ): DogadjajKalendar {
    throw new Error('Not Implmented');
  }
}
