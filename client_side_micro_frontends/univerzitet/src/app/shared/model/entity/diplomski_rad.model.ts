import { Nastavnik } from './nastavnik.model';
import { NastavnikDiplomskiRad } from './nastavnik_diplomski_rad.model';
import { StudentNaStudiji } from './student_na_studiji.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';
import { Identifikacija } from '../interface/identifikacija.interface';

export class DiplomskiRad implements Entitet<DiplomskiRad, number> {
  private id: number;

  private tema: string;

  private ocena: number;

  private stanje: StanjeModela;

  private version: number;

  private mentor: Nastavnik;

  private nastavniciDiplomskiRad: NastavnikDiplomskiRad[];

  private studentNaStudiji: StudentNaStudiji;

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getTema = (): string => {
    return this.tema;
  };

  public setTema = (tema: string): void => {
    this.tema = tema;
  };

  public getOcena = (): number => {
    return this.ocena;
  };

  public setOcena = (ocena: number): void => {
    this.ocena = ocena;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getMentor = (): Nastavnik => {
    return this.mentor;
  };

  public setMentor = (mentor: Nastavnik): void => {
    this.mentor = mentor;
  };

  public getNastavniciDiplomskiRad = (): NastavnikDiplomskiRad[] => {
    return this.nastavniciDiplomskiRad;
  };

  public setNastavniciDiplomskiRad = (
    nastavniciDiplomskiRad: NastavnikDiplomskiRad[]
  ): void => {
    this.nastavniciDiplomskiRad = nastavniciDiplomskiRad;
  };

  public getStudentNaStudiji = (): StudentNaStudiji => {
    return this.studentNaStudiji;
  };

  public setStudentNaStudiji = (studentNaStudiji: StudentNaStudiji): void => {
    this.studentNaStudiji = studentNaStudiji;
  };

  public constructor() {
    this.nastavniciDiplomskiRad = [];
  }

  public getNastavnikDiplomskiRad(
    identifikacija: Identifikacija<number>
  ): NastavnikDiplomskiRad {
    throw new Error('Not Implmented');
  }
}
