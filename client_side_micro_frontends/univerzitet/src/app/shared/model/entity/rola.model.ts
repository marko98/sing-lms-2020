import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';
import { RegistrovaniKorisnikRola } from './registrovani_korisnik_rola.model';

export class Rola implements Entitet<Rola, number> {
  private id: number;

  private stanje: StanjeModela;

  private version: number;

  private naziv: string;

  private registrovaniKorisniciRola: RegistrovaniKorisnikRola[];

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getNaziv = (): string => {
    return this.naziv;
  };

  public setNaziv = (naziv: string): void => {
    this.naziv = naziv;
  };

  public getRegistrovaniKorisniciRola = (): RegistrovaniKorisnikRola[] => {
    return this.registrovaniKorisniciRola;
  };

  public setRegistrovaniKorisniciRola = (
    registrovaniKorisniciRola: RegistrovaniKorisnikRola[]
  ): void => {
    this.registrovaniKorisniciRola = registrovaniKorisniciRola;
  };

  public constructor() {
    this.registrovaniKorisniciRola = [];
  }
}
