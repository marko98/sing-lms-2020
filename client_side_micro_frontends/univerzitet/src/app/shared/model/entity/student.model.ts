import { RegistrovaniKorisnik } from './registrovani_korisnik.model';
import { StudentNaStudiji } from './student_na_studiji.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';
import { Identifikacija } from '../interface/identifikacija.interface';

export class Student implements Entitet<Student, number> {
  private id: number;

  private stanje: StanjeModela;

  private version: number;

  private registrovaniKorisnik: RegistrovaniKorisnik;

  private studije: StudentNaStudiji[];

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getRegistrovaniKorisnik = (): RegistrovaniKorisnik => {
    return this.registrovaniKorisnik;
  };

  public setRegistrovaniKorisnik = (
    registrovaniKorisnik: RegistrovaniKorisnik
  ): void => {
    this.registrovaniKorisnik = registrovaniKorisnik;
  };

  public getStudije = (): StudentNaStudiji[] => {
    return this.studije;
  };

  public setStudije = (studije: StudentNaStudiji[]): void => {
    this.studije = studije;
  };

  public constructor() {
    this.studije = [];
  }

  public getStudija(identifikacija: Identifikacija<number>): StudentNaStudiji {
    throw new Error('Not Implmented');
  }
}
