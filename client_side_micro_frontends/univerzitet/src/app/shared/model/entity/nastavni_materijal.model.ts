import { Ishod } from './ishod.model';
import { Fajl } from './fajl.model';
import { AutorNastavniMaterijal } from './autor_nastavni_materijal.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';
import { Identifikacija } from '../interface/identifikacija.interface';

export class NastavniMaterijal implements Entitet<NastavniMaterijal, number> {
  private id: number;

  private naziv: string;

  private datum: Date;

  private stanje: StanjeModela;

  private version: number;

  private ishod: Ishod;

  private fajlovi: Fajl[];

  private autoriNastavniMaterijal: AutorNastavniMaterijal[];

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getNaziv = (): string => {
    return this.naziv;
  };

  public setNaziv = (naziv: string): void => {
    this.naziv = naziv;
  };

  public getDatum = (): Date => {
    return this.datum;
  };

  public setDatum = (datum: Date): void => {
    this.datum = datum;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getIshod = (): Ishod => {
    return this.ishod;
  };

  public setIshod = (ishod: Ishod): void => {
    this.ishod = ishod;
  };

  public getFajlovi = (): Fajl[] => {
    return this.fajlovi;
  };

  public setFajlovi = (fajlovi: Fajl[]): void => {
    this.fajlovi = fajlovi;
  };

  public getAutoriNastavniMaterijal = (): AutorNastavniMaterijal[] => {
    return this.autoriNastavniMaterijal;
  };

  public setAutoriNastavniMaterijal = (
    autoriNastavniMaterijal: AutorNastavniMaterijal[]
  ): void => {
    this.autoriNastavniMaterijal = autoriNastavniMaterijal;
  };

  public constructor() {
    this.fajlovi = [];
    this.autoriNastavniMaterijal = [];
  }

  public getFajl(identifikacija: Identifikacija<number>): Fajl {
    throw new Error('Not Implmented');
  }

  public getAutorNastavniMaterijal(
    identifikacija: Identifikacija<number>
  ): AutorNastavniMaterijal {
    throw new Error('Not Implmented');
  }
}
