import { Pitanje } from './pitanje.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';

export class Odgovor implements Entitet<Odgovor, number> {
  private id: number;

  private sadrzaj: string;

  private stanje: StanjeModela;

  private version: number;

  private pitanje: Pitanje;

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getSadrzaj = (): string => {
    return this.sadrzaj;
  };

  public setSadrzaj = (sadrzaj: string): void => {
    this.sadrzaj = sadrzaj;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getPitanje = (): Pitanje => {
    return this.pitanje;
  };

  public setPitanje = (pitanje: Pitanje): void => {
    this.pitanje = pitanje;
  };

  public constructor() {}
}
