import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';
import { RegistrovaniKorisnik } from './registrovani_korisnik.model';
import { Rola } from './rola.model';

export class RegistrovaniKorisnikRola
  implements Entitet<RegistrovaniKorisnikRola, number> {
  private id: number;

  private stanje: StanjeModela;

  private version: number;

  private registrovaniKorisnik: RegistrovaniKorisnik;

  private rola: Rola;

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getRegistrovaniKorisnik = (): RegistrovaniKorisnik => {
    return this.registrovaniKorisnik;
  };

  public setRegistrovaniKorisnik = (
    registrovaniKorisnik: RegistrovaniKorisnik
  ): void => {
    this.registrovaniKorisnik = registrovaniKorisnik;
  };

  public getRola = (): Rola => {
    return this.rola;
  };

  public setRola = (rola: Rola): void => {
    this.rola = rola;
  };

  public constructor() {}
}
