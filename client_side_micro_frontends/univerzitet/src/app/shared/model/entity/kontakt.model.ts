import { Univerzitet } from './univerzitet.model';
import { Fakultet } from './fakultet.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';

export class Kontakt implements Entitet<Kontakt, number> {
  private id: number;

  private mobilni: string;

  private fiksni: string;

  private version: number;

  private stanje: StanjeModela;

  private univerzitet: Univerzitet;

  private fakultet: Fakultet;

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getMobilni = (): string => {
    return this.mobilni;
  };

  public setMobilni = (mobilni: string): void => {
    this.mobilni = mobilni;
  };

  public getFiksni = (): string => {
    return this.fiksni;
  };

  public setFiksni = (fiksni: string): void => {
    this.fiksni = fiksni;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getUniverzitet = (): Univerzitet => {
    return this.univerzitet;
  };

  public setUniverzitet = (univerzitet: Univerzitet): void => {
    this.univerzitet = univerzitet;
  };

  public getFakultet = (): Fakultet => {
    return this.fakultet;
  };

  public setFakultet = (fakultet: Fakultet): void => {
    this.fakultet = fakultet;
  };

  public constructor() {}
}
