import { Identifikacija } from './identifikacija.interface';

export declare interface Entitet<T, ID> extends Identifikacija<ID> {}
