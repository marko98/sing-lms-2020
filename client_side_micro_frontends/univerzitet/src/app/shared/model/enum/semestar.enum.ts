export enum Semestar {
  PRVI = 'PRVI',
  DRUGI = 'DRUGI',
  TRECI = 'TRECI',
  CETVRTI = 'CETVRTI',
  PETI = 'PETI',
  SESTI = 'SESTI',
  SEDMI = 'SEDMI',
  OSMI = 'OSMI',
  DEVETI = 'DEVETI',
  DESETI = 'DESETI',
}
