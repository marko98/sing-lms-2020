import { EntitetDTO } from './entitet.dto';
import { FakultetDTO } from './fakultet.dto';
import { NastavnikDTO } from './nastavnik.dto';
import { GodinaStudijaDTO } from './godina_studija.dto';

export declare interface StudijskiProgramDTO extends EntitetDTO {
  naziv: string;
  brojGodinaStudija: number;
  fakultetDTO: FakultetDTO;
  rukovodilacDTO: NastavnikDTO;
  godineStudijaDTO: GodinaStudijaDTO[];
  opis: string;
}
