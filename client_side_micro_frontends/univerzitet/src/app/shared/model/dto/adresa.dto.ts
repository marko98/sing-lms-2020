import { EntitetDTO } from './entitet.dto';
import { TipAdrese } from '../enum/tip_adrese.enum';
import { GradDTO } from './grad.dto';
import { OdeljenjeDTO } from './odeljenje.dto';
import { UniverzitetDTO } from './univerzitet.dto';
import { RegistrovaniKorisnikDTO } from './registrovani_korisnik.dto';

export declare interface AdresaDTO extends EntitetDTO {
  ulica: string;
  broj: string;
  tip: TipAdrese;
  gradDTO: GradDTO;
  odeljenjeDTO: OdeljenjeDTO;
  univerzitetDTO: UniverzitetDTO;
  registrovaniKorisnikDTO: RegistrovaniKorisnikDTO;
}
