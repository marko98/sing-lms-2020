import { EntitetDTO } from './entitet.dto';
import { RegistrovaniKorisnikDTO } from './registrovani_korisnik.dto';
import { StudentskaSluzbaDTO } from './studentska_sluzba.dto';

export declare interface StudentskiSluzbenikDTO extends EntitetDTO {
  registrovaniKorisnikDTO: RegistrovaniKorisnikDTO;
  studentskaSluzbaDTO: StudentskaSluzbaDTO;
}
