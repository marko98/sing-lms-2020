import { EntitetDTO } from './entitet.dto';
import { Zvanje } from '../enum/zvanje.enum';
import { RealizacijaPredmetaDTO } from './realizacija_predmeta.dto';
import { NastavnikNaRealizacijiTipNastaveDTO } from './nastavnik_na_realizaciji_tip_nastave.dto';
import { NastavnikDTO } from './nastavnik.dto';

export declare interface NastavnikNaRealizacijiDTO extends EntitetDTO {
  brojCasova: number;
  zvanje: Zvanje;
  realizacijaPredmetaDTO: RealizacijaPredmetaDTO;
  nastavnikNaRealizacijiTipoviNastaveDTO: NastavnikNaRealizacijiTipNastaveDTO[];
  nastavnikDTO: NastavnikDTO;
}
