import { EntitetDTO } from './entitet.dto';
import { StudentNaStudijiDTO } from './student_na_studiji.dto';
import { PolaganjeDTO } from './polaganje.dto';
import { RealizacijaPredmetaDTO } from './realizacija_predmeta.dto';
import { DatumPohadjanjaPredmetaDTO } from './datum_pohadjanja_predmeta.dto';

export declare interface PohadjanjePredmetaDTO extends EntitetDTO {
  konacnaOcena: number;
  studentNaStudijiDTO: StudentNaStudijiDTO;
  polaganjaDTO: PolaganjeDTO[];
  realizacijaPredmetaDTO: RealizacijaPredmetaDTO;
  prisustvoDTO: DatumPohadjanjaPredmetaDTO[];
}
