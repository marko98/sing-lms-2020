import { EntitetDTO } from './entitet.dto';
import { StudentskiSluzbenikDTO } from './studentski_sluzbenik.dto';
import { OdeljenjeDTO } from './odeljenje.dto';
import { UniverzitetDTO } from './univerzitet.dto';

export declare interface StudentskaSluzbaDTO extends EntitetDTO {
  studentskiSluzbeniciDTO: StudentskiSluzbenikDTO[];
  odeljenjeDTO: OdeljenjeDTO;
  univerzitetDTO: UniverzitetDTO;
}
