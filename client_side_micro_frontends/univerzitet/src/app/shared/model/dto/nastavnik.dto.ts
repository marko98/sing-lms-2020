import { EntitetDTO } from './entitet.dto';
import { TerminNastaveDTO } from './termin_nastave.dto';
import { StudijskiProgramDTO } from './studijski_program.dto';
import { FakultetDTO } from './fakultet.dto';
import { NastavnikFakultetDTO } from './nastavnik_fakultet.dto';
import { UniverzitetDTO } from './univerzitet.dto';
import { TitulaDTO } from './titula.dto';
import { DiplomskiRadDTO } from './diplomski_rad.dto';
import { RegistrovaniKorisnikDTO } from './registrovani_korisnik.dto';
import { IstrazivackiRadDTO } from './istrazivacki_rad.dto';
import { NastavnikEvaluacijaZnanjaDTO } from './nastavnik_evaluacija_znanja.dto';
import { KonsultacijaDTO } from './konsultacija.dto';
import { NastavnikDiplomskiRadDTO } from './nastavnik_diplomski_rad.dto';
import { NastavnikNaRealizacijiDTO } from './nastavnik_na_realizaciji.dto';

export declare interface NastavnikDTO extends EntitetDTO {
  biografija: string;
  terminiNastaveDTO: TerminNastaveDTO[];
  studijskiProgramDTO: StudijskiProgramDTO[];
  fakultetiDTO: FakultetDTO[];
  nastavnikFakultetiDTO: NastavnikFakultetDTO[];
  univerzitetiDTO: UniverzitetDTO[];
  tituleDTO: TitulaDTO[];
  mentorDiplomskiRadoviDTO: DiplomskiRadDTO[];
  registrovaniKorisnikDTO: RegistrovaniKorisnikDTO;
  istrazivackiRadoviDTO: IstrazivackiRadDTO[];
  nastavnikEvaluacijeZnanjaDTO: NastavnikEvaluacijaZnanjaDTO[];
  konsultacijeDTO: KonsultacijaDTO[];
  nastavnikDiplomskiRadoviDTO: NastavnikDiplomskiRadDTO[];
  nastavnikNaRealizacijamaDTO: NastavnikNaRealizacijiDTO[];
}
