import { EntitetDTO } from './entitet.dto';
import { GodinaStudijaObavestenjeDTO } from './godina_studija_obavestenje.dto';
import { FajlDTO } from './fajl.dto';

export declare interface ObavestenjeDTO extends EntitetDTO {
  naslov: string;
  datum: string;
  sadrzaj: string;
  godineStudijaObavestenjeDTO: GodinaStudijaObavestenjeDTO[];
  fajloviDTO: FajlDTO[];
}
