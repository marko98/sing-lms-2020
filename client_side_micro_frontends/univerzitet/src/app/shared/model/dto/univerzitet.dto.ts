import { EntitetDTO } from './entitet.dto';
import { AdresaDTO } from './adresa.dto';
import { FakultetDTO } from './fakultet.dto';
import { KalendarDTO } from './kalendar.dto';
import { NastavnikDTO } from './nastavnik.dto';
import { StudentskaSluzbaDTO } from './studentska_sluzba.dto';
import { KontaktDTO } from './kontakt.dto';

export declare interface UniverzitetDTO extends EntitetDTO {
  naziv: string;
  datumOsnivanja: string;
  adreseDTO: AdresaDTO[];
  fakultetiDTO: FakultetDTO[];
  kalendariDTO: KalendarDTO[];
  rektorDTO: NastavnikDTO;
  studentskaSluzbaDTO: StudentskaSluzbaDTO;
  kontaktiDTO: KontaktDTO[];
  opis: string;
}
