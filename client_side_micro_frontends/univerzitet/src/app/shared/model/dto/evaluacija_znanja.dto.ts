import { EntitetDTO } from './entitet.dto';
import { IspitniRok } from '../enum/ispitni_rok.enum';
import { NastavnikEvaluacijaZnanjaDTO } from './nastavnik_evaluacija_znanja.dto';
import { TipEvaluacije } from '../enum/tip_evaluacije.enum';
import { RealizacijaPredmetaDTO } from './realizacija_predmeta.dto';
import { IshodDTO } from './ishod.dto';
import { FajlDTO } from './fajl.dto';
import { PolaganjeDTO } from './polaganje.dto';
import { PitanjeDTO } from './pitanje.dto';
import { EvaluacijaZnanjaNacinEvaluacijeDTO } from './evaluacija_znanja_nacin_evaluacije.dto';

export declare interface EvaluacijaZnanjaDTO extends EntitetDTO {
  pocetak: string;
  trajanje: number;
  ispitniRok: IspitniRok;
  nastavniciEvaluacijaZnanjaDTO: NastavnikEvaluacijaZnanjaDTO[];
  tipEvaluacije: TipEvaluacije;
  realizacijaPredmetaDTO: RealizacijaPredmetaDTO;
  ishodDTO: IshodDTO;
  fajloviDTO: FajlDTO[];
  polaganjaDTO: PolaganjeDTO[];
  pitanjaDTO: PitanjeDTO[];
  evaluacijaZnanjaNaciniEvaluacijeDTO: EvaluacijaZnanjaNacinEvaluacijeDTO[];
}
