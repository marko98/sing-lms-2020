import { EntitetDTO } from './entitet.dto';
import { TipTitule } from '../enum/tip_titule.enum';
import { NaucnaOblastDTO } from './naucna_oblast.dto';
import { NastavnikDTO } from './nastavnik.dto';

export declare interface TitulaDTO extends EntitetDTO {
  datumIzbora: string;
  datumPrestanka: string;
  tip: TipTitule;
  naucnaOblastDTO: NaucnaOblastDTO;
  nastavnikDTO: NastavnikDTO;
}
