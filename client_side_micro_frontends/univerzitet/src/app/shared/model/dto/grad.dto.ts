import { EntitetDTO } from './entitet.dto';
import { DrzavaDTO } from './drzava.dto';
import { AdresaDTO } from './adresa.dto';

export declare interface GradDTO extends EntitetDTO {
  naziv: string;
  drzavaDTO: DrzavaDTO;
  adreseDTO: AdresaDTO[];
}
