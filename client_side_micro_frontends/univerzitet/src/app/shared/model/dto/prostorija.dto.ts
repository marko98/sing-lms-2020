import { EntitetDTO } from './entitet.dto';
import { TipProstorije } from '../enum/tip_prostorije.enum';
import { OdeljenjeDTO } from './odeljenje.dto';
import { TerminNastaveDTO } from './termin_nastave.dto';

export declare interface ProstorijaDTO extends EntitetDTO {
  naziv: string;
  tip: TipProstorije;
  odeljenjeDTO: OdeljenjeDTO;
  terminiNastaveDTO: TerminNastaveDTO[];
}
