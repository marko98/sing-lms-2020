import { EntitetDTO } from './entitet.dto';
import { PohadjanjePredmetaDTO } from './pohadjanje_predmeta.dto';

export declare interface DatumPohadjanjaPredmetaDTO extends EntitetDTO {
  datum: string;
  pohadjanjePredmetaDTO: PohadjanjePredmetaDTO;
}
