import { EntitetDTO } from './entitet.dto';
import { PredmetDTO } from './predmet.dto';

export declare interface PredmetUslovniPredmetDTO extends EntitetDTO {
  uslovDTO: PredmetDTO;
  predmetDTO: PredmetDTO;
}
