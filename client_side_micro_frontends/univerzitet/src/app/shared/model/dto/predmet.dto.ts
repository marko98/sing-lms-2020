import { EntitetDTO } from './entitet.dto';
import { SilabusDTO } from './silabus.dto';
import { GodinaStudijaDTO } from './godina_studija.dto';
import { PredmetTipDrugogOblikaNastaveDTO } from './predmet_tip_drugog_oblika_nastave.dto';
import { PredmetTipNastaveDTO } from './predmet_tip_nastave.dto';
import { PredmetUslovniPredmetDTO } from './predmet_uslovni_predmet.dto';
import { RealizacijaPredmetaDTO } from './realizacija_predmeta.dto';

export declare interface PredmetDTO extends EntitetDTO {
  naziv: string;
  obavezan: boolean;
  brojPredavanja: number;
  brojVezbi: number;
  brojCasovaZaIstrazivackeRadove: number;
  espb: number;
  trajanjeUSemestrima: number;
  silabusDTO: SilabusDTO;
  godinaStudijaDTO: GodinaStudijaDTO;
  predmetTipoviDrugogOblikaNastaveDTO: PredmetTipDrugogOblikaNastaveDTO[];
  predmetTipoviNastaveDTO: PredmetTipNastaveDTO[];
  predmetiZaKojeSamUslovDTO: PredmetUslovniPredmetDTO[];
  uslovniPredmetiDTO: PredmetUslovniPredmetDTO[];
  realizacijaPredmetaDTO: RealizacijaPredmetaDTO;
}
