import { EntitetDTO } from './entitet.dto';
import { TitulaDTO } from './titula.dto';

export declare interface NaucnaOblastDTO extends EntitetDTO {
  naziv: string;
  titule: TitulaDTO[];
}
