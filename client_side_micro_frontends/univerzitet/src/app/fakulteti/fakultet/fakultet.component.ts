import { Component, OnInit, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { Fakultet } from 'src/app/shared/model/entity/fakultet.model';
import { ActivatedRoute, Router } from '@angular/router';
import { Odeljenje } from 'src/app/shared/model/entity/odeljenje.model';
import { OdeljenjeService } from 'src/app/shared/service/entity/odeljenje_service.service';
import { take } from 'rxjs/operators';
import { NastavnikService } from 'src/app/shared/service/entity/nastavnik_service.service';
import { Nastavnik } from 'src/app/shared/model/entity/nastavnik.model';
import { MICRO_FRONTENDS_PREFIX } from 'src/app/shared/const';

@Component({
  selector: 'app-fakultet',
  templateUrl: './fakultet.component.html',
  styleUrls: ['./fakultet.component.css'],
})
export class FakultetComponent implements OnInit, OnDestroy {
  public fakultet: Fakultet;
  public odeljenja: Odeljenje[];
  public dekan: Nastavnik;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _location: Location,
    private _odeljenjeService: OdeljenjeService,
    private _nastavnikService: NastavnikService,
    private _router: Router
  ) {}

  public onGoBack = (): void => {
    this._location.back();
  };

  public onGoToStudijskiProgram = (id: number): void => {
    this._router.navigate([
      MICRO_FRONTENDS_PREFIX.MF_2_NK,
      'studijski_programi',
      id.toString(),
    ]);
  };

  ngOnInit(): void {
    this.fakultet = this._activatedRoute.snapshot.data.fakultet;

    this._odeljenjeService
      .findAllByFakultetId(this.fakultet.getId())
      .pipe(take(1))
      .subscribe((odeljenja: Odeljenje[]) => {
        this.odeljenja = odeljenja.filter(
          (e) => (<any>e).getStanje().toString() !== 'OBRISAN'
        );
      });

    this._nastavnikService
      .findOne({ id: this.fakultet.getDekan().getId() })
      .pipe(take(1))
      .subscribe((dekan) => {
        this.dekan = <Nastavnik>dekan;
      });

    console.log('FakultetComponent init');
  }

  ngOnDestroy(): void {
    console.log('FakultetComponent destroyed');
  }
}
