import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Entitet } from 'src/app/shared/model/interface/entitet.interface';
import { FakultetService } from 'src/app/shared/service/entity/fakultet_service.service';
import { Fakultet } from 'src/app/shared/model/entity/fakultet.model';

@Injectable({ providedIn: 'root' })
export class FakultetResolver implements Resolve<Entitet<Fakultet, number>> {
  constructor(private _fakultetService: FakultetService) {}

  resolve(
    route: ActivatedRouteSnapshot
  ):
    | Observable<Entitet<Fakultet, number>>
    | Promise<Entitet<Fakultet, number>>
    | Entitet<Fakultet, number> {
    return this._fakultetService.findOne({ id: route.paramMap.get('id') });
  }
}
