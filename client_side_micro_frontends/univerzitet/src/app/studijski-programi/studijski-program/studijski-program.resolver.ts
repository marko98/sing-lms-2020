import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Entitet } from 'src/app/shared/model/interface/entitet.interface';
import { StudijskiProgram } from 'src/app/shared/model/entity/studijski_program.model';
import { StudijskiProgramService } from 'src/app/shared/service/entity/studijski_program_service.service';

@Injectable({ providedIn: 'root' })
export class StudijskiProgramResolver
  implements Resolve<Entitet<StudijskiProgram, number>> {
  constructor(private _studijskiProgramService: StudijskiProgramService) {}

  resolve(
    route: ActivatedRouteSnapshot
  ):
    | Observable<Entitet<StudijskiProgram, number>>
    | Promise<Entitet<StudijskiProgram, number>>
    | Entitet<StudijskiProgram, number> {
    return this._studijskiProgramService.findOne({
      id: route.paramMap.get('id'),
    });
  }
}
