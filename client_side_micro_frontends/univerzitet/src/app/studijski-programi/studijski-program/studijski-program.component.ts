import { Component, OnInit, OnDestroy } from '@angular/core';
import { StudijskiProgram } from 'src/app/shared/model/entity/studijski_program.model';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Nastavnik } from 'src/app/shared/model/entity/nastavnik.model';
import { NastavnikService } from 'src/app/shared/service/entity/nastavnik_service.service';
import { take } from 'rxjs/operators';
import { GodinaStudija } from 'src/app/shared/model/entity/godina_studija.model';
import { GodinaStudijaService } from 'src/app/shared/service/entity/godina_studija_service.service';
import { MICRO_FRONTENDS_PREFIX } from 'src/app/shared/const';

@Component({
  selector: 'app-studijski-program',
  templateUrl: './studijski-program.component.html',
  styleUrls: ['./studijski-program.component.css'],
})
export class StudijskiProgramComponent implements OnInit, OnDestroy {
  public studijskiProgram: StudijskiProgram;
  public rukovodiocSmera: Nastavnik;
  public godineStudija: GodinaStudija[];

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _location: Location,
    private _nastavnikService: NastavnikService,
    private _godineStudijaService: GodinaStudijaService,
    private _router: Router
  ) {}

  public onGoBack = (): void => {
    this._location.back();
  };

  public onGoToPredmet = (id: number): void => {
    this._router.navigate([
      MICRO_FRONTENDS_PREFIX.MF_2_NK,
      'predmeti',
      id.toString(),
    ]);
  };

  ngOnInit(): void {
    this.studijskiProgram = this._activatedRoute.snapshot.data.studijskiProgram;

    this._nastavnikService
      .findOne({ id: this.studijskiProgram.getRukovodilac().getId() })
      .pipe(take(1))
      .subscribe((rukovodiocSmera) => {
        this.rukovodiocSmera = <Nastavnik>rukovodiocSmera;
      });

    this._godineStudijaService
      .findAllByStudijskiProgramId(this.studijskiProgram.getId())
      .pipe(take(1))
      .subscribe((godineStudija: GodinaStudija[]) => {
        this.godineStudija = godineStudija
          .filter((e) => (<any>e).getStanje().toString() !== 'OBRISAN')
          .sort((a, b) => {
            return b.getGodina().getTime() - a.getGodina().getTime();
          });
        // console.log(this.godineStudija);
      });

    console.log('StudijskiProgramComponent init');
  }

  ngOnDestroy(): void {
    console.log('StudijskiProgramComponent destroyed');
  }
}
