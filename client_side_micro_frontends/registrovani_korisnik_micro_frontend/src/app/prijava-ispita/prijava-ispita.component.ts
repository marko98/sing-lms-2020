import { Component, OnInit } from "@angular/core";
import { KorisnikService } from "../korisnik.service";
import { LoginService } from "../login.service";
import { tap } from "rxjs/operators";
import { CrudService } from "../crud.service";

@Component({
  selector: "app-prijava-ispita",
  templateUrl: "./prijava-ispita.component.html",
  styleUrls: ["./prijava-ispita.component.css"],
})
export class PrijavaIspitaComponent implements OnInit {
  public predmeti = [];
  public student = null;
  public pohadjanja = [];
  constructor(
    private korisnikService: KorisnikService,
    private loginService: LoginService,
    private crudService: CrudService
  ) {}

  ngOnInit(): void {
    this.loginService
      .getStudentID()
      .pipe(
        tap((id) => {
          this.getStudentNaStudiji(id);
        })
      )
      .subscribe();
  }

  getEvaluacije() {
    this.crudService
      .getAll(
        "http://localhost:8080/api/student-microservice/student_na_studiji/"
      )
      .subscribe();
  }
  getStudentNaStudiji(studentId) {
    // console.log("Predmeti student id:", this.studentId);
    this.crudService
      .getOne(
        "http://localhost:8080/api/student-microservice/student_na_studiji/" +
          studentId
      )
      .subscribe(
        (korisnik: any) => {
          this.student = korisnik;
          // console.log("Student: ", this.student);
          this.predmetiLista();
        },
        (err) => console.log(err)
      );
  }

  predmetiLista() {
    for (let predmet of this.student.pohadjanjaPredmetaDTO) {
      this.predmeti.push(predmet);
    }
    this.getPohadjanjePredmeta();
    // console.log("Pohadjanje predmeta", this.predmeti);
  }

  getPohadjanjePredmeta() {
    for (let p of this.predmeti) {
      this.crudService
        .getOne(
          "http://localhost:8080/api/student-microservice/pohadjanje_predmeta/" +
            p.id
        )
        .subscribe((p) => {
          this.pohadjanja.push(p);
          this.ispiti();
        });
    }
    // console.log("Pohadjanje:", this.pohadjanja);
  }

  ispiti() {}
}
