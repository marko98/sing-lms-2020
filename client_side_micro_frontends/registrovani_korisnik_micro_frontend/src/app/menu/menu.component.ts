import { Component, OnInit } from "@angular/core";
import { LoginService } from "../login.service";

@Component({
  selector: "app-menu",
  templateUrl: "./menu.component.html",
  styleUrls: ["./menu.component.css"],
})
export class MenuComponent implements OnInit {
  constructor(public loginService: LoginService) {}

  ngOnInit(): void {}

  checkRoles(role: string[]) {
    return this.loginService.validateRoles(role);
  }
}
