import { Injectable } from '@angular/core';
import { CrudService } from './crud.service';
import { LoginService } from './login.service';
import { tap } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { StudentNaStudijiDTO } from './shared/model/dto/student_na_studiji.dto';

@Injectable({
  providedIn: 'root',
})
export class KorisnikService {
  public studentId;
  public pohadjanja = [];
  public predmetiPohadjanje = [];
  public predmeti = [];
  public obavestenja = new BehaviorSubject<any[]>([]);
  public studentNaStudiji: StudentNaStudijiDTO = {
    id: null,
    datumUpisa: null,
    brojIndeksa: '',
    studentDTO: null,
    pohadjanjaPredmetaDTO: null,
    godinaStudijaDTO: null,
    istrazivackiRadoviStudentNaStudijiDTO: null,
    diplomskiRadDTO: null,
    version: 0,
    stanje: null,
  };
  public studentIstorija = new BehaviorSubject<any>(null);
  public student = new BehaviorSubject<any>(null);
  public godineStudija;
  constructor(
    public crudService: CrudService,
    public loginService: LoginService
  ) {
    this.studentId = this.loginService.getStudentID();
  }

  getStudentNaStudiji(id) {
    // console.log("Predmeti student id:", id);
    this.crudService
      .getOne(
        'http://localhost:8080/api/student-microservice/student_na_studiji/' +
          id
      )
      .subscribe(
        (korisnik: any) => {
          this.studentNaStudiji = korisnik;
          // console.log("Student: ", this.studentNaStudiji);
          // for (let predmet of this.studentNaStudiji.pohadjanjaPredmetaDTO) {
          //   if (predmet.stanje == "AKTIVAN") {
          //     // console.log("Predmet: ", predmet);
          //   }
          // }
          this.getObavestenja();
          this.loginService.setStudentID(this.studentNaStudiji.id);
          this.setStudent(korisnik);
        },
        (err) => console.log(err)
      );
  }

  getObavestenja() {
    // console.log("Student id", this.studentNaStudiji);
    this.godineStudija = this.studentNaStudiji.godinaStudijaDTO.id;
    this.crudService
      .getAll(
        'http://localhost:8080/api/obavestenje-microservice/godina_studija_obavestenje'
      )
      .subscribe(
        (obavestenja: any) => {
          const obavestenjaBH = [];
          for (let obavestenje of obavestenja) {
            if (obavestenje.godinaStudijaDTO.id === this.godineStudija) {
              obavestenjaBH.push(obavestenje);
              // console.log(obavestenje.godinaStudijaDTO.id);
              // console.log("Obavestenja: ", obavestenje);
            }
          }
          this.obavestenja.next(obavestenjaBH);
        },
        (err) => console.log(err)
      );
  }

  getPredmet(pohadjanje) {
    // console.log("Pozvani predmeti");
    // for (let predmet of this.pohadjanje) {
    //   console.log(predmet.realizacijaPredmetaDTO);
    this.crudService
      .getOne(
        'http://localhost:8080/api/predmet-microservice/predmet/' +
          pohadjanje.realizacijaPredmetaDTO.id
      )
      .subscribe(
        (predmet: any) => {
          this.predmeti.push(predmet);
          // this.korisnikService.predmetiID.push(predmet.id);
          console.log('Predmet je: ', predmet);
        },
        (err) => console.log(err)
      );
    // }
    // console.log("Predmeti", this.predmeti);
  }

  getPredmeti() {
    return this.predmeti;
  }
  getPredmetiPohadjanje() {
    return this.predmetiPohadjanje;
  }
  getListuObavestenja() {
    return this.obavestenja;
  }
  setStudent(studentNaStudiji) {
    this.student.next(studentNaStudiji);
  }
  getStudent() {
    return this.student;
  }
  studentNaStudijiGet() {
    this.studentIstorija.next(this.studentNaStudiji);
    return this.studentIstorija;
  }
}
