import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login.service';
import { CrudService } from '../crud.service';
import { tap, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { Router } from '@angular/router';
import { KorisnikService } from '../korisnik.service';
import { RegistrovaniKorisnikDTO } from '../shared/model/dto/registrovani_korisnik.dto';
import { StudentNaStudijiDTO } from '../shared/model/dto/student_na_studiji.dto';
import { NastavnikDTO } from '../shared/model/dto/nastavnik.dto';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  public role = '';
  public korisnik: RegistrovaniKorisnikDTO = {
    id: null,
    korisnickoIme: '',
    lozinka: '',
    email: '',
    datumRodjenja: null,
    jmbg: '',
    ime: '',
    prezime: '',
    adreseDTO: null,
    nastavnikDTO: null,
    studentskiSluzbenikDTO: null,
    administratorDTO: null,
    studentDTO: null,
    registrovaniKorisnikRoleDTO: null,
    version: 0,
    stanje: null,
  };
  public korisnikSaUlogom: any;
  public studentNaStudiji: StudentNaStudijiDTO;
  public titula = '';

  public nastavnik: NastavnikDTO = {
    id: null,
    biografija: '',
    terminiNastaveDTO: null,
    studijskiProgramDTO: null,
    fakultetiDTO: null,
    nastavnikFakultetiDTO: null,
    univerzitetiDTO: null,
    tituleDTO: null,
    mentorDiplomskiRadoviDTO: null,
    registrovaniKorisnikDTO: null,
    istrazivackiRadoviDTO: null,
    nastavnikEvaluacijeZnanjaDTO: null,
    konsultacijeDTO: null,
    nastavnikDiplomskiRadoviDTO: null,
    nastavnikNaRealizacijamaDTO: null,
    version: 0,
    stanje: null,
  };
  constructor(
    private loginService: LoginService,
    private crudService: CrudService,
    private router: Router,
    private korisnikService: KorisnikService
  ) {}

  ngOnInit(): void {
    const token = this.loginService.getToken();
    // console.log(token);
    let ime = JSON.parse(atob(token.split('.')[1]))['username'];
    this.getUser(ime);
    // console.log(ime);
    // console.log(korisnik);
  }

  getUser(username: String): void {
    this.crudService
      .getOne(
        'http://localhost:8080/api/korisnik-microservice/registrovani_korisnik/korisnickoIme/' +
          username
      )
      .pipe(
        tap((response) => {
          // console.log("Response je:", response);
          this.korisnik = response;
          // console.log("Korisnik je: ", this.korisnik);
          switch (this.korisnik.registrovaniKorisnikRoleDTO[1].rolaDTO.naziv) {
            case 'ROLE_NASTAVNIK':
              this.role = 'Nastavnik';
              console.log(this.korisnik.id);
              this.getNastavnik(this.korisnik.id);
              break;
            case 'ROLE_STUDENT':
              // console.log("usao");
              this.role = 'Student';
              this.getStudent(this.korisnik.id);
              break;
            default:
              this.role = '';
          }
        }),
        catchError((err) => {
          console.log(err);
          // of pravi observable od bilo cega
          return of(null);
        })
      )
      .subscribe();
  }

  logout(): void {
    this.loginService.logout();
  }

  getStudent(id) {
    // console.log("id studenta:", id);
    this.crudService
      .getOne(
        'http://localhost:8080/api/student-microservice/student/registrovani_korisnik/' +
          id
      )
      .subscribe(
        (korisnik: any) => {
          this.korisnikSaUlogom = korisnik;
          // console.log("Korisnik sa ulogom: ", this.korisnikSaUlogom);
          this.korisnikService.getStudentNaStudiji(this.korisnikSaUlogom.id);
        },
        (err) => console.log(err)
      );
  }

  getStudentNaStudiji(id) {
    console.log('id:', id);
    this.crudService
      .getOne(
        'http://localhost:8080/api/student-microservice/student_na_studiji/' +
          id
      )
      .subscribe(
        (korisnik: any) => {
          this.studentNaStudiji = korisnik;
          console.log('Student: ', this.studentNaStudiji);
          for (let predmet of this.studentNaStudiji.pohadjanjaPredmetaDTO) {
            if (predmet.stanje == 'AKTIVAN') {
              console.log('Predmet: ', predmet);
              // this.korisnikService.predmetiPohadjanje.push(predmet.id);
            }
          }
          this.loginService.setStudentID(this.studentNaStudiji.id);
        },
        (err) => console.log(err)
      );
  }

  getNastavnik(id) {
    // console.log("id nastavnika:", id);
    this.crudService
      .getOne(
        'http://localhost:8080/api/nastavnik-microservice/nastavnik/registrovani_korisnik/' +
          id
      )
      .subscribe(
        (korisnik: any) => {
          this.nastavnik = korisnik;
          this.loginService.setNastavnikID(this.nastavnik.id);
          // console.log('Nastavnik sa ulogom: ', this.nastavnik);
          switch (this.nastavnik.tituleDTO[0].tip) {
            case 'DOKTOR_NAUKA':
              this.titula = 'Doktor nauka';
              break;
            case 'MASTER':
              this.titula = 'Master';
              break;
            case 'DIPLOMIRANI':
              this.titula = 'Diplomirani';
              break;
            default:
              this.titula = 'Doktorant';
          }
        },
        (err) => console.log(err)
      );
  }
}
