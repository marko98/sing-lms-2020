import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IstorijaStudiranjaComponent } from './istorija-studiranja.component';

describe('IstorijaStudiranjaComponent', () => {
  let component: IstorijaStudiranjaComponent;
  let fixture: ComponentFixture<IstorijaStudiranjaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IstorijaStudiranjaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IstorijaStudiranjaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
