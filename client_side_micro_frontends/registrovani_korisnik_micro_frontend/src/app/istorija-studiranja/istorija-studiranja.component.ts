import { Component, OnInit } from '@angular/core';
import { KorisnikService } from '../korisnik.service';
import { BehaviorSubject } from 'rxjs';
import { LoginService } from '../login.service';
import { tap } from 'rxjs/operators';
import { CrudService } from '../crud.service';

@Component({
  selector: 'app-istorija-studiranja',
  templateUrl: './istorija-studiranja.component.html',
  styleUrls: ['./istorija-studiranja.component.css'],
})
export class IstorijaStudiranjaComponent implements OnInit {
  private student;
  public prosek = 0;
  public ukupnoBodova = 0;
  public pohadjaniPredmeti = [];
  constructor(
    private korisnikService: KorisnikService,
    private loginService: LoginService,
    private crudService: CrudService
  ) {}

  ngOnInit(): void {
    this.korisnikService
      .getStudent()
      .pipe(
        tap((s) => {
          this.student = s;
          this.setPohadjaniPredmeti();
        })
      )
      .subscribe();
    // console.log("Student", this.student);
  }

  setPohadjaniPredmeti() {
    for (let predmet of this.student.pohadjanjaPredmetaDTO) {
      if (predmet.konacnaOcena) {
        // this.pohadjaniPredmeti.push(predmet);
        this.crudService
          .getOne(
            'http://localhost:8080/api/student-microservice/pohadjanje_predmeta/' +
              predmet.id
          )
          .subscribe(
            (pohadjanje) => {
              predmet['realizacija_id'] = pohadjanje.realizacijaPredmetaDTO.id;
              this.getPredmet(pohadjanje.realizacijaPredmetaDTO.id, predmet);
            },
            (err) => console.log(err)
          );
      }
    }
  }

  getPredmet(id, predmet) {
    this.crudService
      .getOne(
        'http://localhost:8080/api/predmet-microservice/realizacija_predmeta/' +
          id
      )
      .subscribe(
        (p) => {
          // predmet["realizacija_id"] = pohadjanje.realizacijaPredmetaDTO.id;
          // this.getPredmet(pohadjanje.realizacijaPredmetaDTO.id);
          // console.log("Predmet na real:", p);
          predmet['nazivPredmeta'] = p.predmetDTO.naziv;
          predmet['espb'] = p.predmetDTO.espb;
          this.pohadjaniPredmeti.push(predmet);
          if (this.pohadjaniPredmeti.length > 1) {
            this.prosek = (this.prosek + predmet.konacnaOcena) / 2;
          } else {
            this.prosek += predmet.konacnaOcena;
          }
          this.ukupnoBodova += predmet.espb;
          console.log('Prosek', this.prosek);
          console.log('ESPB', this.ukupnoBodova);
        },
        (err) => console.log(err)
      );
  }
  // getOcene() {
  //   for (let p of this.pohadjaniPredmeti) {
  //     this.prosek += p.konacnaOcena;
  //     this.ukupnoBodova += p.espb;
  //   }
  //   this.ukupanProsek = this.prosek / this.pohadjaniPredmeti.length;
  //   console.log('Bodovi', this.ukupnoBodova);
  //   console.log('Prosek', this.ukupanProsek);
  //   console.log('Bodovi');
  // }
}
