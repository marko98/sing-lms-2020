import { Component, OnInit } from "@angular/core";
import { KorisnikService } from "../korisnik.service";
import { LoginService } from "../login.service";
import { tap } from "rxjs/operators";

@Component({
  selector: "app-obavestenja",
  templateUrl: "./obavestenja.component.html",
  styleUrls: ["./obavestenja.component.css"],
})
export class ObavestenjaComponent implements OnInit {
  public obavestenja = [];
  constructor(
    public korisnikService: KorisnikService,
    private loginService: LoginService
  ) {}

  ngOnInit(): void {
    // console.log("Student id: ", this.loginService.getStudentID());
    this.korisnikService.getStudentNaStudiji(
      this.loginService.getStudentID().value
    );
    this.getObavestenja();
    this.korisnikService.obavestenja
      .pipe(
        tap((o) => {
          this.obavestenja = o;
          // console.log("Obavestenja su: ", this.obavestenja);
        })
      )
      .subscribe();
  }

  getObavestenja() {
    // this.korisnikService.obavestenja.subscribe(
    //   (obavestenje) => {
    //     console.log("Obavestenja su: ", this.obavestenja);
    //     this.obavestenja = obavestenje;
    //   },
    //   (err) => console.log(err)
    // );
    // console.log("Obavestenja su:", this.korisnikService.getListuObavestenja());
  }
}
