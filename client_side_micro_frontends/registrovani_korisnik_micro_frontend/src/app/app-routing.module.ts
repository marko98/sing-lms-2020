import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmptyRouteComponent } from './empty-route/empty-route.component';
import { APP_BASE_HREF } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './auth.guard';
import { HomeComponent } from './home/home.component';
import { ProfilComponent } from './profil/profil.component';
import { PredmetiComponent } from './predmeti/predmeti.component';
import { ObavestenjaComponent } from './obavestenja/obavestenja.component';
import { IstorijaStudiranjaComponent } from './istorija-studiranja/istorija-studiranja.component';
import { PrijavaIspitaComponent } from './prijava-ispita/prijava-ispita.component';
import { PredmetiNastavnikComponent } from './predmeti-nastavnik/predmeti-nastavnik.component';
import { StudentiComponent } from './studenti/studenti.component';
import { UredjivanjeSilabusaComponent } from './predmeti-nastavnik/uredjivanje-silabusa/uredjivanje-silabusa.component';

const routes: Routes = [
  {
    path: 'mf_3',
    children: [
      { path: 'login', component: LoginComponent },
      {
        path: 'home',
        canActivate: [AuthGuard],
        component: HomeComponent,
        children: [
          { path: 'uredjivanjeProfila', component: ProfilComponent },
          { path: 'predmeti', component: PredmetiComponent },
          { path: 'obavestenja', component: ObavestenjaComponent },
          { path: 'istorija', component: IstorijaStudiranjaComponent },
          { path: 'prijavaIspita', component: PrijavaIspitaComponent },
          {
            path: 'predmetiNastavnik',
            component: PredmetiNastavnikComponent,
            children: [
              {
                path: 'uredjivanjeSilabusa/{predmet}',
                component: UredjivanjeSilabusaComponent,
              },
            ],
          },
          { path: 'studenti', component: StudentiComponent },
          { path: '**', redirectTo: 'mf_3/home' },
        ],
      },
      {
        path: '**',
        redirectTo: 'login',
      },
    ],
  },
  { path: '**', component: EmptyRouteComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [{ provide: APP_BASE_HREF, useValue: '' }],
})
export class AppRoutingModule {}
