import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login.service';
import { CrudService } from '../crud.service';
import { KorisnikService } from '../korisnik.service';
import { tap } from 'rxjs/operators';
import { StudentNaStudijiDTO } from '../shared/model/dto/student_na_studiji.dto';

@Component({
  selector: 'app-predmeti',
  templateUrl: './predmeti.component.html',
  styleUrls: ['./predmeti.component.css'],
})
export class PredmetiComponent implements OnInit {
  public studentId;
  public pohadjanje = [];
  public predmeti = [];
  public zavrseno = false;
  public silabus = false;
  public silabusTekst = '';
  public aktivniPredmet = null;
  public studentNaStudiji: StudentNaStudijiDTO = {
    id: null,
    datumUpisa: null,
    brojIndeksa: '',
    studentDTO: null,
    pohadjanjaPredmetaDTO: null,
    godinaStudijaDTO: null,
    istrazivackiRadoviStudentNaStudijiDTO: null,
    diplomskiRadDTO: null,
    version: 0,
    stanje: null,
  };
  constructor(
    public loginService: LoginService,
    private crudService: CrudService,
    private korisnikService: KorisnikService
  ) {}

  ngOnInit(): void {
    // console.log("Student id:", this.loginService.getStudentID());
    this.loginService
      .getStudentID()
      .pipe(
        tap((id) => {
          this.getStudentNaStudiji(id);
        })
      )
      .subscribe();
    // console.log("Predmeti:", this.predmeti);
  }

  getStudentNaStudiji(studentId) {
    // console.log("Predmeti student id:", this.studentId);
    this.crudService
      .getOne(
        'http://localhost:8080/api/student-microservice/student_na_studiji/' +
          studentId
      )
      .subscribe(
        (korisnik: any) => {
          this.studentNaStudiji = korisnik;
          // console.log("Student: ", this.studentNaStudiji);
          this.getPohadjanje();
        },
        (err) => console.log(err)
      );
  }

  getPohadjanje() {
    // console.log("usao");
    for (let pohadjanje of this.studentNaStudiji.pohadjanjaPredmetaDTO) {
      if (pohadjanje.stanje == 'AKTIVAN') {
        // console.log(pohadjanje);
        this.crudService
          .getOne(
            'http://localhost:8080/api/student-microservice/pohadjanje_predmeta/' +
              pohadjanje.id
          )
          .subscribe(
            (pohadjanje: any) => {
              this.pohadjanje.push(pohadjanje);
              // console.log("POohadjanje ", pohadjanje);
              this.getPredmet(pohadjanje);
            },
            (err) => console.log(err)
          );
      }
      // this.getPredmet();
      // console.log("Pohadjanja", this.pohadjanje);
    }
  }

  getPredmet(pohadjanje) {
    // console.log("Pozvani predmeti");
    // for (let predmet of this.pohadjanje) {
    //   console.log(predmet.realizacijaPredmetaDTO);
    this.crudService
      .getOne(
        'http://localhost:8080/api/predmet-microservice/predmet/' +
          pohadjanje.realizacijaPredmetaDTO.id
      )
      .subscribe(
        (predmet: any) => {
          this.predmeti.push(predmet);
          // this.korisnikService.predmetiID.push(predmet.id);
          // console.log("Predmet ", predmet);
        },
        (err) => console.log(err)
      );
    // }
    // console.log("Predmeti", this.predmeti);
  }

  silabusPrikaz(predmet) {
    // console.log("Dobijeni predmet:", predmet);
    this.aktivniPredmet = predmet;
  }
}
