import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginService } from '../login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  hide = true;
  private token = null;
  korisnikLogin = {
    korisnickoIme: '',
    lozinka: '',
  };
  public izmenaLozinke: boolean = false;

  korisnikIzmenaLozinke = {
    korisnickoIme: '',
    email: '',
    lozinka: 1234,
  };
  constructor(
    private http: HttpClient,
    private loginService: LoginService,
    private router: Router
  ) {}

  ngOnInit(): void {
    console.log(this.korisnikLogin);
  }
  login() {
    // console.log("radi");
    if (localStorage.getItem('token')) {
      this.router.navigate(['mf_3/home']);
    }
    console.log(this.korisnikLogin);
    this.loginService.login(this.korisnikLogin).subscribe(
      (r) => {
        // console.log("success");
        this.router.navigate(['mf_3/home']);
      },
      (r) => {
        // console.log("failed");
        window.alert('Neuspesan login!');
        this.router.navigate(['mf_3/login']);
      }
    );
  }

  promenaLozinke(): void {
    // this.http
    //   .post(
    //     "http://localhost:8080/api/korisnik-microservice/registrovani_korisnik/izmenaLozinke" +
    //       this.korisnikIzmenaLozinke.korisnickoIme,
    //     this.korisnikIzmenaLozinke
    //   )
    //   .subscribe((user: any) => {
    if (
      this.korisnikIzmenaLozinke.korisnickoIme == '' ||
      this.korisnikIzmenaLozinke.email == ''
    ) {
      window.alert('Niste uneli ispravne podatke');
    } else {
      window.alert(
        'Vas zahtev je proslednjen i uskoro cete dobiti mail sa novom lozinkom'
      );
      this.izmenaLozinke = !this.izmenaLozinke;
    }
    // }, err => (console.log(err)));
  }
}
