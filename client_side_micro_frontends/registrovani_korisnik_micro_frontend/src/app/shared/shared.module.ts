import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MaterialModule } from './material.module';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MyAngularFormModule } from 'my-angular-form';
import { MyAngularTableModule } from 'my-angular-table';
import { RemoveDirective } from './directive/remove.directive';
import { ModelObrisanDirective } from './directive/model-obrisan.directive';

@NgModule({
  declarations: [RemoveDirective, ModelObrisanDirective],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MaterialModule,
    DragDropModule,
    MyAngularFormModule,
    MyAngularTableModule,
  ],
  exports: [],
})
export class SharedModule {}
