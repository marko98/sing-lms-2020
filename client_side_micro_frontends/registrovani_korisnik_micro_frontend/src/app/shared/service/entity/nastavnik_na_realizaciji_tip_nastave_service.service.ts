import { CrudService } from '../crud.service';
import {
  PATH,
  WS_PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
} from '../../const';
import { Injectable } from '@angular/core';
import { NastavnikNaRealizacijiTipNastave } from '../../model/entity/nastavnik_na_realizaciji_tip_nastave.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { NastavnikNaRealizacijiTipNastaveFactory } from '../../model/patterns/creational/factory_method/nastavnik_na_realizaciji_tip_nastave.factory';
import { CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';

export const NASTAVNIK_NA_REALIZACIJI_TIP_NASTAVE_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: NastavnikNaRealizacijiTipNastaveFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.NASTAVNIK,
  path: PATH.NASTAVNIK_NA_REALIZACIJI_TIP_NASTAVE,
  microservicePort: MICROSERVICE_PORT.NASTAVNIK,
  wsPath: WS_PATH.NASTAVNIK_NA_REALIZACIJI_TIP_NASTAVE,
};
@Injectable({ providedIn: 'root' })
export class NastavnikNaRealizacijiTipNastaveService extends CrudService<
  NastavnikNaRealizacijiTipNastave,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      NastavnikNaRealizacijiTipNastaveFactory.getInstance(),
      MICROSERVICE_NAME.NASTAVNIK,
      PATH.NASTAVNIK_NA_REALIZACIJI_TIP_NASTAVE,
      MICROSERVICE_PORT.NASTAVNIK,
      WS_PATH.NASTAVNIK_NA_REALIZACIJI_TIP_NASTAVE
    );
    this._findAll();
  }
}
