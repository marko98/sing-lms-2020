import { CrudService, CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';
import {
  PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
  WS_PATH,
} from '../../const';
import { Injectable } from '@angular/core';
import { Student } from '../../model/entity/student.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { StudentFactory } from '../../model/patterns/creational/factory_method/student.factory';

export const STUDENT_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: StudentFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.STUDENT,
  path: PATH.STUDENT,
  microservicePort: MICROSERVICE_PORT.STUDENT,
  wsPath: WS_PATH.STUDENT,
};

@Injectable({ providedIn: 'root' })
export class StudentService extends CrudService<Student, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      StudentFactory.getInstance(),
      MICROSERVICE_NAME.STUDENT,
      PATH.STUDENT,
      MICROSERVICE_PORT.STUDENT,
      WS_PATH.STUDENT
    );
    this._findAll();
  }
}
