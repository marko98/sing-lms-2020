import { CrudService } from '../crud.service';
import {
  PATH,
  WS_PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
} from '../../const';
import { Injectable } from '@angular/core';
import { Fajl } from '../../model/entity/fajl.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { FajlFactory } from '../../model/patterns/creational/factory_method/fajl.factory';
import { CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';

export const FAJL_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: FajlFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.OBAVESTENJE,
  path: PATH.FAJL,
  microservicePort: MICROSERVICE_PORT.OBAVESTENJE,
  wsPath: WS_PATH.FAJL,
};
@Injectable({ providedIn: 'root' })
export class FajlService extends CrudService<Fajl, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      FajlFactory.getInstance(),
      MICROSERVICE_NAME.OBAVESTENJE,
      PATH.FAJL,
      MICROSERVICE_PORT.OBAVESTENJE,
      WS_PATH.FAJL
    );
    this._findAll();
  }
}
