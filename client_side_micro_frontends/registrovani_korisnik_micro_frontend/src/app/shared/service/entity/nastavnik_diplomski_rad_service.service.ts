import { CrudService } from '../crud.service';
import {
  PATH,
  WS_PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
} from '../../const';
import { Injectable } from '@angular/core';
import { NastavnikDiplomskiRad } from '../../model/entity/nastavnik_diplomski_rad.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { NastavnikDiplomskiRadFactory } from '../../model/patterns/creational/factory_method/nastavnik_diplomski_rad.factory';
import { CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';

export const NASTAVNIK_DIPLOMSKI_RAD_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: NastavnikDiplomskiRadFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.NASTAVNIK,
  path: PATH.NASTAVNIK_DIPLOMSKI_RAD,
  microservicePort: MICROSERVICE_PORT.NASTAVNIK,
  wsPath: WS_PATH.NASTAVNIK_DIPLOMSKI_RAD,
};
@Injectable({ providedIn: 'root' })
export class NastavnikDiplomskiRadService extends CrudService<
  NastavnikDiplomskiRad,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      NastavnikDiplomskiRadFactory.getInstance(),
      MICROSERVICE_NAME.NASTAVNIK,
      PATH.NASTAVNIK_DIPLOMSKI_RAD,
      MICROSERVICE_PORT.NASTAVNIK,
      WS_PATH.NASTAVNIK_DIPLOMSKI_RAD
    );
    this._findAll();
  }
}
