import { CrudService } from '../crud.service';
import {
  PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
  WS_PATH,
} from '../../const';
import { Injectable } from '@angular/core';
import { Pitanje } from '../../model/entity/pitanje.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { PitanjeFactory } from '../../model/patterns/creational/factory_method/pitanje.factory';
import { CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';

export const PITANJE_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: PitanjeFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.EVALUACIJA_ZNANJA,
  path: PATH.PITANJE,
  microservicePort: MICROSERVICE_PORT.EVALUACIJA_ZNANJA,
  wsPath: WS_PATH.PITANJE,
};
@Injectable({ providedIn: 'root' })
export class PitanjeService extends CrudService<Pitanje, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      PitanjeFactory.getInstance(),
      MICROSERVICE_NAME.EVALUACIJA_ZNANJA,
      PATH.PITANJE,
      MICROSERVICE_PORT.EVALUACIJA_ZNANJA,
      WS_PATH.PITANJE
    );
    this._findAll();
  }
}
