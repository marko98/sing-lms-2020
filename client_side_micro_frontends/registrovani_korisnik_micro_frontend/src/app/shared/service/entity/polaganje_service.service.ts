import { CrudService } from '../crud.service';
import {
  PATH,
  WS_PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
} from '../../const';
import { Injectable } from '@angular/core';
import { Polaganje } from '../../model/entity/polaganje.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { PolaganjeFactory } from '../../model/patterns/creational/factory_method/polaganje.factory';
import { CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';
import { Observable } from 'rxjs';
import { PolaganjeDTO } from '../../model/dto/polaganje.dto';
import { map, catchError } from 'rxjs/operators';

export const POLAGANJE_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: PolaganjeFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.EVALUACIJA_ZNANJA,
  path: PATH.POLAGANJE,
  microservicePort: MICROSERVICE_PORT.EVALUACIJA_ZNANJA,
  wsPath: WS_PATH.POLAGANJE,
};
@Injectable({ providedIn: 'root' })
export class PolaganjeService extends CrudService<Polaganje, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      PolaganjeFactory.getInstance(),
      MICROSERVICE_NAME.EVALUACIJA_ZNANJA,
      PATH.POLAGANJE,
      MICROSERVICE_PORT.EVALUACIJA_ZNANJA,
      WS_PATH.POLAGANJE
    );
    this._findAll();
  }

  public findAllByEvaluacijaZnanjaId = (
    id: number
  ): Observable<Polaganje[]> => {
    // console.log(this._url + '/evaluacija_znanja/' + id);
    return this._httpClient
      .get<PolaganjeDTO[]>(this._url + '/evaluacija_znanja/' + id)
      .pipe(
        map((entiteti) =>
          entiteti.map((e) => <Polaganje>this._factory.build(e))
        ),
        catchError(this._onHandleError)
      );
  };
}
