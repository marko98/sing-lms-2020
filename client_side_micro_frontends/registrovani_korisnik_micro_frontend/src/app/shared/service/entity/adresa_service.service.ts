import {
  PATH,
  WS_PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
} from '../../const';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Adresa } from '../../model/entity/adresa.model';
import { CrudService, CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';
import { UIService } from '../ui.service';
import { AdresaFactory } from '../../model/patterns/creational/factory_method/adresa.factory';

export const ADRESA_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: AdresaFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.ADRESA,
  path: PATH.ADRESA,
  microservicePort: MICROSERVICE_PORT.ADRESA,
  wsPath: WS_PATH.ADRESA,
};

@Injectable({ providedIn: 'root' })
export class AdresaService extends CrudService<Adresa, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      AdresaFactory.getInstance(),
      MICROSERVICE_NAME.ADRESA,
      PATH.ADRESA,
      MICROSERVICE_PORT.ADRESA,
      WS_PATH.ADRESA
    );
    this._findAll();
  }
}
