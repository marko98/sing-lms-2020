import { CrudService } from '../crud.service';
import {
  PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
  WS_PATH,
} from '../../const';
import { Injectable } from '@angular/core';
import { PredmetTipNastave } from '../../model/entity/predmet_tip_nastave.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { PredmetTipNastaveFactory } from '../../model/patterns/creational/factory_method/predmet_tip_nastave.factory';
import { CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';

export const PREDMET_TIP_NASTAVE_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: PredmetTipNastaveFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.PREDMET,
  path: PATH.PREDMET_TIP_NASTAVE,
  microservicePort: MICROSERVICE_PORT.PREDMET,
  wsPath: WS_PATH.PREDMET_TIP_NASTAVE,
};
@Injectable({ providedIn: 'root' })
export class PredmetTipNastaveService extends CrudService<
  PredmetTipNastave,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      PredmetTipNastaveFactory.getInstance(),
      MICROSERVICE_NAME.PREDMET,
      PATH.PREDMET_TIP_NASTAVE,
      MICROSERVICE_PORT.PREDMET,
      WS_PATH.PREDMET_TIP_NASTAVE
    );
    this._findAll();
  }
}
