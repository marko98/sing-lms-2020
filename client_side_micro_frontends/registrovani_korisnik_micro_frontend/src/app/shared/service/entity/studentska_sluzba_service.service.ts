import { CrudService, CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';
import {
  PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
  WS_PATH,
} from '../../const';
import { Injectable } from '@angular/core';
import { StudentskaSluzba } from '../../model/entity/studentska_sluzba.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { StudentskaSluzbaFactory } from '../../model/patterns/creational/factory_method/studentska_sluzba.factory';

export const STUDENTSKA_SLUZBA_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: StudentskaSluzbaFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.ADMINISTRACIJA,
  path: PATH.STUDENTSKA_SLUZBA,
  microservicePort: MICROSERVICE_PORT.ADMINISTRACIJA,
  wsPath: WS_PATH.STUDENTSKA_SLUZBA,
};

@Injectable({ providedIn: 'root' })
export class StudentskaSluzbaService extends CrudService<
  StudentskaSluzba,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      StudentskaSluzbaFactory.getInstance(),
      MICROSERVICE_NAME.ADMINISTRACIJA,
      PATH.STUDENTSKA_SLUZBA,
      MICROSERVICE_PORT.ADMINISTRACIJA,
      WS_PATH.STUDENTSKA_SLUZBA
    );
    this._findAll();
  }
}
