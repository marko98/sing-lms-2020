import { CrudService, CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';
import {
  PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
  WS_PATH,
} from '../../const';
import { Injectable } from '@angular/core';
import { StudentskiSluzbenik } from '../../model/entity/studentski_sluzbenik.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { StudentskiSluzbenikFactory } from '../../model/patterns/creational/factory_method/studentski_sluzbenik.factory';

export const STUDENTSKI_SLUZBENIK_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: StudentskiSluzbenikFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.ADMINISTRACIJA,
  path: PATH.STUDENTSKI_SLUZBENIK,
  microservicePort: MICROSERVICE_PORT.ADMINISTRACIJA,
  wsPath: WS_PATH.STUDENTSKI_SLUZBENIK,
};

@Injectable({ providedIn: 'root' })
export class StudentskiSluzbenikService extends CrudService<
  StudentskiSluzbenik,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      StudentskiSluzbenikFactory.getInstance(),
      MICROSERVICE_NAME.ADMINISTRACIJA,
      PATH.STUDENTSKI_SLUZBENIK,
      MICROSERVICE_PORT.ADMINISTRACIJA,
      WS_PATH.STUDENTSKI_SLUZBENIK
    );
    this._findAll();
  }
}
