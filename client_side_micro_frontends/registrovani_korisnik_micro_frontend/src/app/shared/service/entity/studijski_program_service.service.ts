import { CrudService, CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';
import {
  PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
  WS_PATH,
} from '../../const';
import { Injectable } from '@angular/core';
import { StudijskiProgram } from '../../model/entity/studijski_program.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { StudijskiProgramFactory } from '../../model/patterns/creational/factory_method/studijski_program.factory';

export const STUDIJSKI_PROGRAM_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: StudijskiProgramFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.NASTAVA,
  path: PATH.STUDIJSKI_PROGRAM,
  microservicePort: MICROSERVICE_PORT.NASTAVA,
  wsPath: WS_PATH.STUDIJSKI_PROGRAM,
};

@Injectable({ providedIn: 'root' })
export class StudijskiProgramService extends CrudService<
  StudijskiProgram,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      StudijskiProgramFactory.getInstance(),
      MICROSERVICE_NAME.NASTAVA,
      PATH.STUDIJSKI_PROGRAM,
      MICROSERVICE_PORT.NASTAVA,
      WS_PATH.STUDIJSKI_PROGRAM
    );
    this._findAll();
  }
}
