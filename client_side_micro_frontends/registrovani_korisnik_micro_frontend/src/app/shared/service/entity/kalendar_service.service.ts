import { CrudService } from '../crud.service';
import {
  PATH,
  WS_PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
} from '../../const';
import { Injectable } from '@angular/core';
import { Kalendar } from '../../model/entity/kalendar.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { KalendarFactory } from '../../model/patterns/creational/factory_method/kalendar.factory';
import { CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';

export const KALENDAR_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: KalendarFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.NASTAVA,
  path: PATH.KALENDAR,
  microservicePort: MICROSERVICE_PORT.NASTAVA,
  wsPath: WS_PATH.KALENDAR,
};
@Injectable({ providedIn: 'root' })
export class KalendarService extends CrudService<Kalendar, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      KalendarFactory.getInstance(),
      MICROSERVICE_NAME.NASTAVA,
      PATH.KALENDAR,
      MICROSERVICE_PORT.NASTAVA,
      WS_PATH.KALENDAR
    );
    this._findAll();
  }
}
