import { CrudService } from '../crud.service';
import {
  PATH,
  WS_PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
} from '../../const';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { Rola } from '../../model/entity/rola.model';
import { RolaFactory } from '../../model/patterns/creational/factory_method/rola.factory';
import { CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';

export const ROLA_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: RolaFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.KORISNIK,
  path: PATH.ROLA,
  microservicePort: MICROSERVICE_PORT.KORISNIK,
  wsPath: WS_PATH.ROLA,
};
@Injectable({ providedIn: 'root' })
export class RolaService extends CrudService<Rola, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      RolaFactory.getInstance(),
      MICROSERVICE_NAME.KORISNIK,
      PATH.ROLA,
      MICROSERVICE_PORT.KORISNIK,
      WS_PATH.ROLA
    );
    this._findAll();
  }
}
