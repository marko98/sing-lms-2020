import { CrudService } from '../crud.service';
import {
  PATH,
  WS_PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
} from '../../const';
import { Injectable } from '@angular/core';
import { DogadjajKalendar } from '../../model/entity/dogadjaj_kalendar.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { DogadjajKalendarFactory } from '../../model/patterns/creational/factory_method/dogadjaj_kalendar.factory';
import { CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';

export const DOGADJAJ_KALENDAR_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: DogadjajKalendarFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.NASTAVA,
  path: PATH.DOGADJAJ_KALENDAR,
  microservicePort: MICROSERVICE_PORT.NASTAVA,
  wsPath: WS_PATH.DOGADJAJ_KALENDAR,
};
@Injectable({ providedIn: 'root' })
export class DogadjajKalendarService extends CrudService<
  DogadjajKalendar,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      DogadjajKalendarFactory.getInstance(),
      MICROSERVICE_NAME.NASTAVA,
      PATH.DOGADJAJ_KALENDAR,
      MICROSERVICE_PORT.NASTAVA,
      WS_PATH.DOGADJAJ_KALENDAR
    );
    this._findAll();
  }
}
