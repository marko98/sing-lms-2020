import { CrudService } from '../crud.service';
import {
  PATH,
  WS_PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
} from '../../const';
import { Injectable } from '@angular/core';
import { Predmet } from '../../model/entity/predmet.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { PredmetFactory } from '../../model/patterns/creational/factory_method/predmet.factory';
import { CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';

export const PREDMET_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: PredmetFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.PREDMET,
  path: PATH.PREDMET,
  microservicePort: MICROSERVICE_PORT.PREDMET,
  wsPath: WS_PATH.PREDMET,
};
@Injectable({ providedIn: 'root' })
export class PredmetService extends CrudService<Predmet, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      PredmetFactory.getInstance(),
      MICROSERVICE_NAME.PREDMET,
      PATH.PREDMET,
      MICROSERVICE_PORT.PREDMET,
      WS_PATH.PREDMET
    );
    this._findAll();
  }
}
