import { GodinaStudija } from './godina_studija.model';
import { Silabus } from './silabus.model';
import { PredmetTipDrugogOblikaNastave } from './predmet_tip_drugog_oblika_nastave.model';
import { PredmetTipNastave } from './predmet_tip_nastave.model';
import { RealizacijaPredmeta } from './realizacija_predmeta.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';
import { PredmetUslovniPredmet } from './predmet_uslovni_predmet.model';
import { Identifikacija } from '../interface/identifikacija.interface';
import { CrudServiceForModel } from '../../service/crud.service-for-model';
import { Subject } from 'rxjs';
import {
  Form,
  FormRow,
  ROW_ITEM_TYPE,
  FormFieldInputNumberInterface,
  FORM_FIELD_INPUT_TYPE,
  SelectInterface,
  VALIDATOR_NAMES,
} from 'my-angular-form';
import { Validators } from '@angular/forms';
import { HeaderValue, Table } from 'my-angular-table';

export class Predmet implements Entitet<Predmet, number> {
  private id: number;

  private naziv: string;

  private obavezan: boolean;

  private brojPredavanja: number;

  private brojVezbi: number;

  private brojCasovaZaIstrazivackeRadove: number;

  private espb: number;

  private trajanjeUSemestrima: number;

  private stanje: StanjeModela;

  private version: number;

  private godinaStudija: GodinaStudija;

  private silabus: Silabus;

  private predmetTipoviDrugogOblikaNastave: PredmetTipDrugogOblikaNastave[];

  private predmetTipoviNastave: PredmetTipNastave[];

  private predmetiZaKojeSamUslov: PredmetUslovniPredmet[];

  private uslovniPredmeti: PredmetUslovniPredmet[];

  private realizacijaPredmeta: RealizacijaPredmeta;

  private _formSubject: Subject<Form> = new Subject();

  public getFormSubject = (): Subject<Form> => {
    return this._formSubject;
  };

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getNaziv = (): string => {
    return this.naziv;
  };

  public setNaziv = (naziv: string): void => {
    this.naziv = naziv;
  };

  public getObavezan = (): boolean => {
    return this.obavezan;
  };

  public setObavezan = (obavezan: boolean): void => {
    this.obavezan = obavezan;
  };

  public getBrojPredavanja = (): number => {
    return this.brojPredavanja;
  };

  public setBrojPredavanja = (brojPredavanja: number): void => {
    this.brojPredavanja = brojPredavanja;
  };

  public getBrojVezbi = (): number => {
    return this.brojVezbi;
  };

  public setBrojVezbi = (brojVezbi: number): void => {
    this.brojVezbi = brojVezbi;
  };

  public getBrojCasovaZaIstrazivackeRadove = (): number => {
    return this.brojCasovaZaIstrazivackeRadove;
  };

  public setBrojCasovaZaIstrazivackeRadove = (
    brojCasovaZaIstrazivackeRadove: number
  ): void => {
    this.brojCasovaZaIstrazivackeRadove = brojCasovaZaIstrazivackeRadove;
  };

  public getEspb = (): number => {
    return this.espb;
  };

  public setEspb = (espb: number): void => {
    this.espb = espb;
  };

  public getTrajanjeUSemestrima = (): number => {
    return this.trajanjeUSemestrima;
  };

  public setTrajanjeUSemestrima = (trajanjeUSemestrima: number): void => {
    this.trajanjeUSemestrima = trajanjeUSemestrima;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getGodinaStudija = (): GodinaStudija => {
    return this.godinaStudija;
  };

  public setGodinaStudija = (godinaStudija: GodinaStudija): void => {
    this.godinaStudija = godinaStudija;
  };

  public getSilabus = (): Silabus => {
    return this.silabus;
  };

  public setSilabus = (silabus: Silabus): void => {
    this.silabus = silabus;
  };

  public getPredmetTipoviDrugogOblikaNastave = (): PredmetTipDrugogOblikaNastave[] => {
    return this.predmetTipoviDrugogOblikaNastave;
  };

  public setPredmetTipoviDrugogOblikaNastave = (
    predmetTipoviDrugogOblikaNastave: PredmetTipDrugogOblikaNastave[]
  ): void => {
    this.predmetTipoviDrugogOblikaNastave = predmetTipoviDrugogOblikaNastave;
  };

  public getPredmetTipoviNastave = (): PredmetTipNastave[] => {
    return this.predmetTipoviNastave;
  };

  public setPredmetTipoviNastave = (
    predmetTipoviNastave: PredmetTipNastave[]
  ): void => {
    this.predmetTipoviNastave = predmetTipoviNastave;
  };

  public getPredmetiZaKojeSamUslov = (): PredmetUslovniPredmet[] => {
    return this.predmetiZaKojeSamUslov;
  };

  public setPredmetiZaKojeSamUslov = (
    predmetiZaKojeSamUslov: PredmetUslovniPredmet[]
  ): void => {
    this.predmetiZaKojeSamUslov = predmetiZaKojeSamUslov;
  };

  public getUslovniPredmeti = (): PredmetUslovniPredmet[] => {
    return this.uslovniPredmeti;
  };

  public setUslovniPredmeti = (
    uslovniPredmeti: PredmetUslovniPredmet[]
  ): void => {
    this.uslovniPredmeti = uslovniPredmeti;
  };

  public getRealizacijaPredmeta = (): RealizacijaPredmeta => {
    return this.realizacijaPredmeta;
  };

  public setRealizacijaPredmeta = (
    realizacijaPredmeta: RealizacijaPredmeta
  ): void => {
    this.realizacijaPredmeta = realizacijaPredmeta;
  };

  public constructor() {
    this.predmetTipoviDrugogOblikaNastave = [];
    this.predmetTipoviNastave = [];
    this.predmetiZaKojeSamUslov = [];
    this.uslovniPredmeti = [];
  }

  public getPreduslov(identifikacija: Identifikacija<number>): Predmet {
    throw new Error('Not Implmented');
  }

  public getPredmetTipNastave(
    identifikacija: Identifikacija<number>
  ): PredmetTipNastave {
    throw new Error('Not Implmented');
  }

  public getPredmetTipDrugogOblikaNastave(
    identifikacija: Identifikacija<number>
  ): PredmetTipDrugogOblikaNastave {
    throw new Error('Not Implmented');
  }

  public getPredmetZaKojiSamUslov(
    identifikacija: Identifikacija<number>
  ): PredmetUslovniPredmet {
    throw new Error('Not Implmented');
  }

  public getUslovniPredmet(
    identifikacija: Identifikacija<number>
  ): PredmetUslovniPredmet {
    throw new Error('Not Implmented');
  }

  public getForm = (
    csfm: CrudServiceForModel,
    isFormForCreation: boolean,
    disableAllFields: boolean = false
  ): Subject<Form> => {
    let subject: Subject<Form> = new Subject();

    let form = new Form({
      showSubmitButton: !disableAllFields,
      submitButtonText: 'potvrdi',
      showCancelButton: !disableAllFields,
      cancelButtonText: 'odustani',
      showResetButton: isFormForCreation,
      resetButtonText: 'resetuj',
      submitOnlyIfFormValid: true,
    });

    if (!isFormForCreation) {
      let rowId = new FormRow();
      form.addChild(rowId);
      rowId.addChildInterface(<FormFieldInputNumberInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
        formFieldInputType: FORM_FIELD_INPUT_TYPE.NUMBER,
        controlName: 'id',
        labelName: 'Id',
        defaultValue: this.id,
        disabled: true,
      });

      // console.log(StanjeModela.AKTIVAN);
      // console.log(typeof StanjeModela.AKTIVAN);

      let rowStanjeModela = new FormRow();
      form.addChild(rowStanjeModela);
      rowStanjeModela.addChildInterface(<SelectInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
        controlName: 'stanje',
        labelName: 'Stanje modela',
        defaultValue: this.stanje,
        optionValues: [
          { value: StanjeModela.AKTIVAN, textToShow: 'AKTIVAN' },
          { value: StanjeModela.NEAKTIVAN, textToShow: 'NEAKTIVAN' },
        ],
        disabled: disableAllFields,
        validators: [
          {
            message: 'Stanje modela je obavezno',
            name: VALIDATOR_NAMES.REQUIRED,
            validatorFn: Validators.required,
          },
        ],
      });
    }

    return subject;
  };

  public save = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    // let gradService = csfm.getCrudServiceForModel<Grad, number>(
    //   GRAD_SERVICE_FOR_MODEL_INTERFACE
    // );
    // gradService
    //   .save(entitetDict)
    //   .pipe(take(1))
    //   .subscribe(
    //     (entitet) => {},
    //     (err: HttpErrorResponse) => {
    //       subject.error(err);
    //     },
    //     () => {
    //       subject.complete();
    //     }
    //   );

    return subject;
  };

  public update = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    // let gradService = csfm.getCrudServiceForModel<Grad, number>(
    //   GRAD_SERVICE_FOR_MODEL_INTERFACE
    // );
    // gradService
    //   .update(entitetDict)
    //   .pipe(take(1))
    //   .subscribe(
    //     (entitet) => {},
    //     (err: HttpErrorResponse) => {
    //       subject.error(err);
    //     },
    //     () => {
    //       subject.complete();
    //     }
    //   );

    return subject;
  };

  public getHeaderValue = (): HeaderValue => {
    return {
      headerItems: [
        { context: 'id' },
        { context: 'stanje modela' },
        // { context: 'naziv' },
        // { context: 'drzava' },
        { context: '' },
        { context: '' },
        { context: '' },
      ],
    };
  };

  public addRowToTable = (
    table: Table,
    detaljiFn: Function = () => {},
    izmeniFn: Function = () => {},
    obrisiFn: Function = () => {}
  ): void => {
    try {
      table.addChildValue({
        data: this,
        rowItems: [
          {
            context: this.id,
          },
          {
            context: this.stanje,
          },
          // {
          //   context: this.naziv,
          // },
          // {
          //   context: this.drzava ? this.drzava.getNaziv() : '/',
          // },
          // {
          //   context: 'detalji',
          //   button: { function: detaljiFn },
          // },
          // {
          //   context: 'izmeni',
          //   button: { function: izmeniFn },
          // },
          // {
          //   context: this.stanje !== StanjeModela.OBRISAN ? 'obrisi' : '',
          //   button:
          //     this.stanje !== StanjeModela.OBRISAN
          //       ? { function: obrisiFn }
          //       : undefined,
          // },
        ],
      });
    } catch (error) {
      console.log(error);
    }
  };
}
