import { Univerzitet } from './univerzitet.model';
import { Fakultet } from './fakultet.model';
import { NastavnikFakultet } from './nastavnik_fakultet.model';
import { TerminNastave } from './termin_nastave.model';
import { StudijskiProgram } from './studijski_program.model';
import { Titula } from './titula.model';
import { RegistrovaniKorisnik } from './registrovani_korisnik.model';
import { DiplomskiRad } from './diplomski_rad.model';
import { IstrazivackiRad } from './istrazivacki_rad.model';
import { Konsultacija } from './konsultacija.model';
import { NastavnikEvaluacijaZnanja } from './nastavnik_evaluacija_znanja.model';
import { NastavnikDiplomskiRad } from './nastavnik_diplomski_rad.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';
import { Identifikacija } from '../interface/identifikacija.interface';
import { NastavnikNaRealizaciji } from './nastavnik_na_realizaciji.model';
import { CrudServiceForModel } from '../../service/crud.service-for-model';
import { Subject } from 'rxjs';
import {
  Form,
  FormRow,
  ROW_ITEM_TYPE,
  FormFieldInputNumberInterface,
  FORM_FIELD_INPUT_TYPE,
  SelectInterface,
  VALIDATOR_NAMES,
} from 'my-angular-form';
import { Validators } from '@angular/forms';
import { HeaderValue, Table } from 'my-angular-table';

export class Nastavnik implements Entitet<Nastavnik, number> {
  private id: number;

  private biografija: string;

  private stanje: StanjeModela;

  private version: number;

  private univerziteti: Univerzitet[];

  private fakulteti: Fakultet[];

  private nastavnikFakulteti: NastavnikFakultet[];

  private terminiNastave: TerminNastave[];

  private studijskiProgrami: StudijskiProgram[];

  private titule: Titula[];

  private registrovaniKorisnik: RegistrovaniKorisnik;

  private mentorDiplomskiRadovi: DiplomskiRad[];

  private istrazivackiRadovi: IstrazivackiRad[];

  private konsultacije: Konsultacija[];

  private nastavnikEvaluacijeZnanja: NastavnikEvaluacijaZnanja[];

  private nastavnikDiplomskiRadovi: NastavnikDiplomskiRad[];

  private nastavnikNaRealizacijama: NastavnikNaRealizaciji[];

  private _formSubject: Subject<Form> = new Subject();

  public getFormSubject = (): Subject<Form> => {
    return this._formSubject;
  };

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getBiografija = (): string => {
    return this.biografija;
  };

  public setBiografija = (biografija: string): void => {
    this.biografija = biografija;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getUniverziteti = (): Univerzitet[] => {
    return this.univerziteti;
  };

  public setUniverziteti = (univerziteti: Univerzitet[]): void => {
    this.univerziteti = univerziteti;
  };

  public getFakulteti = (): Fakultet[] => {
    return this.fakulteti;
  };

  public setFakulteti = (fakulteti: Fakultet[]): void => {
    this.fakulteti = fakulteti;
  };

  public getNastavnikFakulteti = (): NastavnikFakultet[] => {
    return this.nastavnikFakulteti;
  };

  public setNastavnikFakulteti = (
    nastavnikFakulteti: NastavnikFakultet[]
  ): void => {
    this.nastavnikFakulteti = nastavnikFakulteti;
  };

  public getTerminiNastave = (): TerminNastave[] => {
    return this.terminiNastave;
  };

  public setTerminiNastave = (terminiNastave: TerminNastave[]): void => {
    this.terminiNastave = terminiNastave;
  };

  public getStudijskiProgrami = (): StudijskiProgram[] => {
    return this.studijskiProgrami;
  };

  public setStudijskiProgrami = (
    studijskiProgrami: StudijskiProgram[]
  ): void => {
    this.studijskiProgrami = studijskiProgrami;
  };

  public getTitule = (): Titula[] => {
    return this.titule;
  };

  public setTitule = (titule: Titula[]): void => {
    this.titule = titule;
  };

  public getRegistrovaniKorisnik = (): RegistrovaniKorisnik => {
    return this.registrovaniKorisnik;
  };

  public setRegistrovaniKorisnik = (
    registrovaniKorisnik: RegistrovaniKorisnik
  ): void => {
    this.registrovaniKorisnik = registrovaniKorisnik;
  };

  public getMentorDiplomskiRadovi = (): DiplomskiRad[] => {
    return this.mentorDiplomskiRadovi;
  };

  public setMentorDiplomskiRadovi = (
    mentorDiplomskiRadovi: DiplomskiRad[]
  ): void => {
    this.mentorDiplomskiRadovi = mentorDiplomskiRadovi;
  };

  public getIstrazivackiRadovi = (): IstrazivackiRad[] => {
    return this.istrazivackiRadovi;
  };

  public setIstrazivackiRadovi = (
    istrazivackiRadovi: IstrazivackiRad[]
  ): void => {
    this.istrazivackiRadovi = istrazivackiRadovi;
  };

  public getKonsultacije = (): Konsultacija[] => {
    return this.konsultacije;
  };

  public setKonsultacije = (konsultacije: Konsultacija[]): void => {
    this.konsultacije = konsultacije;
  };

  public getNastavnikEvaluacijeZnanja = (): NastavnikEvaluacijaZnanja[] => {
    return this.nastavnikEvaluacijeZnanja;
  };

  public setNastavnikEvaluacijeZnanja = (
    nastavnikEvaluacijeZnanja: NastavnikEvaluacijaZnanja[]
  ): void => {
    this.nastavnikEvaluacijeZnanja = nastavnikEvaluacijeZnanja;
  };

  public getNastavnikDiplomskiRadovi = (): NastavnikDiplomskiRad[] => {
    return this.nastavnikDiplomskiRadovi;
  };

  public setNastavnikDiplomskiRadovi = (
    nastavnikDiplomskiRadovi: NastavnikDiplomskiRad[]
  ): void => {
    this.nastavnikDiplomskiRadovi = nastavnikDiplomskiRadovi;
  };

  public getNastavnikNaRealizacijama = (): NastavnikNaRealizaciji[] => {
    return this.nastavnikNaRealizacijama;
  };

  public setNastavnikNaRealizacijama = (
    nastavnikNaRealizacijama: NastavnikNaRealizaciji[]
  ): void => {
    this.nastavnikNaRealizacijama = nastavnikNaRealizacijama;
  };

  public constructor() {
    this.univerziteti = [];
    this.fakulteti = [];
    this.nastavnikFakulteti = [];
    this.terminiNastave = [];
    this.studijskiProgrami = [];
    this.titule = [];
    this.mentorDiplomskiRadovi = [];
    this.istrazivackiRadovi = [];
    this.konsultacije = [];
    this.nastavnikEvaluacijeZnanja = [];
    this.nastavnikDiplomskiRadovi = [];
    this.nastavnikNaRealizacijama = [];
  }

  public getTitula(identifikacija: Identifikacija<number>): Titula {
    throw new Error('Not Implmented');
  }

  public getNastavnikFakultet(
    identifikacija: Identifikacija<number>
  ): NastavnikFakultet {
    throw new Error('Not Implmented');
  }

  public getMentorDiplomskiRad(
    identifikacija: Identifikacija<number>
  ): DiplomskiRad {
    throw new Error('Not Implmented');
  }

  public getIstrazivackiRad(
    identifikacija: Identifikacija<number>
  ): IstrazivackiRad {
    throw new Error('Not Implmented');
  }

  public getKonsultacija(identifikacija: Identifikacija<number>): Konsultacija {
    throw new Error('Not Implmented');
  }

  public getNastavnikEvaluacijaZnanja(
    identifikacija: Identifikacija<number>
  ): NastavnikEvaluacijaZnanja {
    throw new Error('Not Implmented');
  }

  public getTerminNastave(
    identifikacija: Identifikacija<number>
  ): TerminNastave {
    throw new Error('Not Implmented');
  }

  public getNastavnikDiplomskiRad(
    identifikacija: Identifikacija<number>
  ): NastavnikDiplomskiRad {
    throw new Error('Not Implmented');
  }

  public getNastavnikNaRealizaciji(
    identifikacija: Identifikacija<number>
  ): NastavnikNaRealizaciji {
    throw new Error('Not Implmented');
  }

  public getForm = (
    csfm: CrudServiceForModel,
    isFormForCreation: boolean,
    disableAllFields: boolean = false
  ): Subject<Form> => {
    let subject: Subject<Form> = new Subject();

    let form = new Form({
      showSubmitButton: !disableAllFields,
      submitButtonText: 'potvrdi',
      showCancelButton: !disableAllFields,
      cancelButtonText: 'odustani',
      showResetButton: isFormForCreation,
      resetButtonText: 'resetuj',
      submitOnlyIfFormValid: true,
    });

    if (!isFormForCreation) {
      let rowId = new FormRow();
      form.addChild(rowId);
      rowId.addChildInterface(<FormFieldInputNumberInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
        formFieldInputType: FORM_FIELD_INPUT_TYPE.NUMBER,
        controlName: 'id',
        labelName: 'Id',
        defaultValue: this.id,
        disabled: true,
      });

      // console.log(StanjeModela.AKTIVAN);
      // console.log(typeof StanjeModela.AKTIVAN);

      let rowStanjeModela = new FormRow();
      form.addChild(rowStanjeModela);
      rowStanjeModela.addChildInterface(<SelectInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
        controlName: 'stanje',
        labelName: 'Stanje modela',
        defaultValue: this.stanje,
        optionValues: [
          { value: StanjeModela.AKTIVAN, textToShow: 'AKTIVAN' },
          { value: StanjeModela.NEAKTIVAN, textToShow: 'NEAKTIVAN' },
        ],
        disabled: disableAllFields,
        validators: [
          {
            message: 'Stanje modela je obavezno',
            name: VALIDATOR_NAMES.REQUIRED,
            validatorFn: Validators.required,
          },
        ],
      });
    }

    return subject;
  };

  public save = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    // let gradService = csfm.getCrudServiceForModel<Grad, number>(
    //   GRAD_SERVICE_FOR_MODEL_INTERFACE
    // );
    // gradService
    //   .save(entitetDict)
    //   .pipe(take(1))
    //   .subscribe(
    //     (entitet) => {},
    //     (err: HttpErrorResponse) => {
    //       subject.error(err);
    //     },
    //     () => {
    //       subject.complete();
    //     }
    //   );

    return subject;
  };

  public update = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    // let gradService = csfm.getCrudServiceForModel<Grad, number>(
    //   GRAD_SERVICE_FOR_MODEL_INTERFACE
    // );
    // gradService
    //   .update(entitetDict)
    //   .pipe(take(1))
    //   .subscribe(
    //     (entitet) => {},
    //     (err: HttpErrorResponse) => {
    //       subject.error(err);
    //     },
    //     () => {
    //       subject.complete();
    //     }
    //   );

    return subject;
  };

  public getHeaderValue = (): HeaderValue => {
    return {
      headerItems: [
        { context: 'id' },
        { context: 'stanje modela' },
        // { context: 'naziv' },
        // { context: 'drzava' },
        { context: '' },
        { context: '' },
        { context: '' },
      ],
    };
  };

  public addRowToTable = (
    table: Table,
    detaljiFn: Function = () => {},
    izmeniFn: Function = () => {},
    obrisiFn: Function = () => {}
  ): void => {
    try {
      table.addChildValue({
        data: this,
        rowItems: [
          {
            context: this.id,
          },
          {
            context: this.stanje,
          },
          // {
          //   context: this.naziv,
          // },
          // {
          //   context: this.drzava ? this.drzava.getNaziv() : '/',
          // },
          // {
          //   context: 'detalji',
          //   button: { function: detaljiFn },
          // },
          // {
          //   context: 'izmeni',
          //   button: { function: izmeniFn },
          // },
          // {
          //   context: this.stanje !== StanjeModela.OBRISAN ? 'obrisi' : '',
          //   button:
          //     this.stanje !== StanjeModela.OBRISAN
          //       ? { function: obrisiFn }
          //       : undefined,
          // },
        ],
      });
    } catch (error) {
      console.log(error);
    }
  };
}
