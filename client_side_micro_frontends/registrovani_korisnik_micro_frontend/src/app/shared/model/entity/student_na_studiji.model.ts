import { Student } from './student.model';
import { GodinaStudija } from './godina_studija.model';
import { PohadjanjePredmeta } from './pohadjanje_predmeta.model';
import { IstrazivackiRadStudentNaStudiji } from './istrazivacki_rad_student_na_studiji.model';
import { DiplomskiRad } from './diplomski_rad.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';
import { Identifikacija } from '../interface/identifikacija.interface';
import { CrudServiceForModel } from '../../service/crud.service-for-model';
import { Subject } from 'rxjs';
import {
  Form,
  FormRow,
  ROW_ITEM_TYPE,
  FormFieldInputNumberInterface,
  FORM_FIELD_INPUT_TYPE,
  SelectInterface,
  VALIDATOR_NAMES,
} from 'my-angular-form';
import { Validators } from '@angular/forms';
import { HeaderValue, Table } from 'my-angular-table';

export class StudentNaStudiji implements Entitet<StudentNaStudiji, number> {
  private id: number;

  private datumUpisa: Date;

  private brojIndeksa: string;

  private stanje: StanjeModela;

  private version: number;

  private student: Student;

  private godinaStudija: GodinaStudija;

  private pohadjanjaPredmeta: PohadjanjePredmeta[];

  private istrazivackiRadoviStudentNaStudiji: IstrazivackiRadStudentNaStudiji[];

  private diplomskiRad: DiplomskiRad;

  private _formSubject: Subject<Form> = new Subject();

  public getFormSubject = (): Subject<Form> => {
    return this._formSubject;
  };

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getDatumUpisa = (): Date => {
    return this.datumUpisa;
  };

  public setDatumUpisa = (datumUpisa: Date): void => {
    this.datumUpisa = datumUpisa;
  };

  public getBrojIndeksa = (): string => {
    return this.brojIndeksa;
  };

  public setBrojIndeksa = (brojIndeksa: string): void => {
    this.brojIndeksa = brojIndeksa;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getStudent = (): Student => {
    return this.student;
  };

  public setStudent = (student: Student): void => {
    this.student = student;
  };

  public getGodinaStudija = (): GodinaStudija => {
    return this.godinaStudija;
  };

  public setGodinaStudija = (godinaStudija: GodinaStudija): void => {
    this.godinaStudija = godinaStudija;
  };

  public getPohadjanjaPredmeta = (): PohadjanjePredmeta[] => {
    return this.pohadjanjaPredmeta;
  };

  public setPohadjanjaPredmeta = (
    pohadjanjaPredmeta: PohadjanjePredmeta[]
  ): void => {
    this.pohadjanjaPredmeta = pohadjanjaPredmeta;
  };

  public getIstrazivackiRadoviStudentNaStudiji = (): IstrazivackiRadStudentNaStudiji[] => {
    return this.istrazivackiRadoviStudentNaStudiji;
  };

  public setIstrazivackiRadoviStudentNaStudiji = (
    istrazivackiRadoviStudentNaStudiji: IstrazivackiRadStudentNaStudiji[]
  ): void => {
    this.istrazivackiRadoviStudentNaStudiji = istrazivackiRadoviStudentNaStudiji;
  };

  public getDiplomskiRad = (): DiplomskiRad => {
    return this.diplomskiRad;
  };

  public setDiplomskiRad = (diplomskiRad: DiplomskiRad): void => {
    this.diplomskiRad = diplomskiRad;
  };

  public constructor() {
    this.pohadjanjaPredmeta = [];
    this.istrazivackiRadoviStudentNaStudiji = [];
  }

  public getPohadjanjePredmeta(
    identifikacija: Identifikacija<number>
  ): PohadjanjePredmeta {
    throw new Error('Not Implmented');
  }

  public getIstrazivackiRadStudentNaStudiji(
    identifikacija: Identifikacija<number>
  ): IstrazivackiRadStudentNaStudiji {
    throw new Error('Not Implmented');
  }

  public getForm = (
    csfm: CrudServiceForModel,
    isFormForCreation: boolean,
    disableAllFields: boolean = false
  ): Subject<Form> => {
    let subject: Subject<Form> = new Subject();

    let form = new Form({
      showSubmitButton: !disableAllFields,
      submitButtonText: 'potvrdi',
      showCancelButton: !disableAllFields,
      cancelButtonText: 'odustani',
      showResetButton: isFormForCreation,
      resetButtonText: 'resetuj',
      submitOnlyIfFormValid: true,
    });

    if (!isFormForCreation) {
      let rowId = new FormRow();
      form.addChild(rowId);
      rowId.addChildInterface(<FormFieldInputNumberInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
        formFieldInputType: FORM_FIELD_INPUT_TYPE.NUMBER,
        controlName: 'id',
        labelName: 'Id',
        defaultValue: this.id,
        disabled: true,
      });

      // console.log(StanjeModela.AKTIVAN);
      // console.log(typeof StanjeModela.AKTIVAN);

      let rowStanjeModela = new FormRow();
      form.addChild(rowStanjeModela);
      rowStanjeModela.addChildInterface(<SelectInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
        controlName: 'stanje',
        labelName: 'Stanje modela',
        defaultValue: this.stanje,
        optionValues: [
          { value: StanjeModela.AKTIVAN, textToShow: 'AKTIVAN' },
          { value: StanjeModela.NEAKTIVAN, textToShow: 'NEAKTIVAN' },
        ],
        disabled: disableAllFields,
        validators: [
          {
            message: 'Stanje modela je obavezno',
            name: VALIDATOR_NAMES.REQUIRED,
            validatorFn: Validators.required,
          },
        ],
      });
    }

    return subject;
  };

  public save = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    // let gradService = csfm.getCrudServiceForModel<Grad, number>(
    //   GRAD_SERVICE_FOR_MODEL_INTERFACE
    // );
    // gradService
    //   .save(entitetDict)
    //   .pipe(take(1))
    //   .subscribe(
    //     (entitet) => {},
    //     (err: HttpErrorResponse) => {
    //       subject.error(err);
    //     },
    //     () => {
    //       subject.complete();
    //     }
    //   );

    return subject;
  };

  public update = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    // let gradService = csfm.getCrudServiceForModel<Grad, number>(
    //   GRAD_SERVICE_FOR_MODEL_INTERFACE
    // );
    // gradService
    //   .update(entitetDict)
    //   .pipe(take(1))
    //   .subscribe(
    //     (entitet) => {},
    //     (err: HttpErrorResponse) => {
    //       subject.error(err);
    //     },
    //     () => {
    //       subject.complete();
    //     }
    //   );

    return subject;
  };

  public getHeaderValue = (): HeaderValue => {
    return {
      headerItems: [
        { context: 'id' },
        { context: 'stanje modela' },
        // { context: 'naziv' },
        // { context: 'drzava' },
        { context: '' },
        { context: '' },
        { context: '' },
      ],
    };
  };

  public addRowToTable = (
    table: Table,
    detaljiFn: Function = () => {},
    izmeniFn: Function = () => {},
    obrisiFn: Function = () => {}
  ): void => {
    try {
      table.addChildValue({
        data: this,
        rowItems: [
          {
            context: this.id,
          },
          {
            context: this.stanje,
          },
          // {
          //   context: this.naziv,
          // },
          // {
          //   context: this.drzava ? this.drzava.getNaziv() : '/',
          // },
          // {
          //   context: 'detalji',
          //   button: { function: detaljiFn },
          // },
          // {
          //   context: 'izmeni',
          //   button: { function: izmeniFn },
          // },
          // {
          //   context: this.stanje !== StanjeModela.OBRISAN ? 'obrisi' : '',
          //   button:
          //     this.stanje !== StanjeModela.OBRISAN
          //       ? { function: obrisiFn }
          //       : undefined,
          // },
        ],
      });
    } catch (error) {
      console.log(error);
    }
  };
}
