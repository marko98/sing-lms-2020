export enum TipNastave {
  PREDAVANJA = 'PREDAVANJA',
  VEZBE = 'VEZBE',
  DODATNA_NASTAVA = 'DODATNA_NASTAVA',
  RADIONICA = 'RADIONICA',
  TIMSKI_PROJEKAT = 'TIMSKI_PROJEKAT',
  FAKULTETSKI_PROJEKAT = 'FAKULTETSKI_PROJEKAT',
  ISTRAZIVACKI_RAD = 'ISTRAZIVACKI_RAD',
  OSTALI_CASOVI = 'OSTALI_CASOVI',
}
