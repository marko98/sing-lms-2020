export enum TipStudija {
  OSNOVNE = 'OSNOVNE',
  MASTER = 'MASTER',
  DOKTORSKE = 'DOKTORSKE',
}
