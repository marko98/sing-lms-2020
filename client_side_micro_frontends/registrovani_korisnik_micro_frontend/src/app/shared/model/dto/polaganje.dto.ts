import { EntitetDTO } from './entitet.dto';
import { PohadjanjePredmetaDTO } from './pohadjanje_predmeta.dto';
import { EvaluacijaZnanjaDTO } from './evaluacija_znanja.dto';

export declare interface PolaganjeDTO extends EntitetDTO {
  bodovi: number;
  napomena: string;
  pohadjanjePredmetaDTO: PohadjanjePredmetaDTO;
  evaluacijaZnanjaDTO: EvaluacijaZnanjaDTO;
}
