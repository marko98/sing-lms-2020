import { EntitetDTO } from './entitet.dto';
import { AutorDTO } from './autor.dto';
import { NastavniMaterijalDTO } from './nastavni_materijal.dto';

export declare interface AutorNastavniMaterijalDTO extends EntitetDTO {
  autorDTO: AutorDTO;
  nastavniMaterijalDTO: NastavniMaterijalDTO;
}
