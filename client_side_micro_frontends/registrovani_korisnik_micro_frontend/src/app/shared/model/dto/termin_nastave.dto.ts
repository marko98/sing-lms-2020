import { EntitetDTO } from './entitet.dto';
import { TipNastave } from '../enum/tip_nastave.enum';
import { ProstorijaDTO } from './prostorija.dto';
import { NastavnikDTO } from './nastavnik.dto';
import { RealizacijaPredmetaDTO } from './realizacija_predmeta.dto';
import { VremeOdrzavanjaUNedeljiDTO } from './vreme_odrzavanja_u_nedelji.dto';

export declare interface TerminNastaveDTO extends EntitetDTO {
  trajanje: number;
  tipNastave: TipNastave;
  prostorijaDTO: ProstorijaDTO;
  nastavnikDTO: NastavnikDTO;
  realizacijaPredmetaDTO: RealizacijaPredmetaDTO;
  vremeOdrzavanjaUNedeljiDTO: VremeOdrzavanjaUNedeljiDTO;
}
