import { EntitetDTO } from './entitet.dto';
import { StudentNaStudijiDTO } from './student_na_studiji.dto';
import { IstrazivackiRadDTO } from './istrazivacki_rad.dto';

export declare interface IstrazivackiRadStudentNaStudijiDTO extends EntitetDTO {
  datum: string;
  studentNaStudijiDTO: StudentNaStudijiDTO;
  istrazivackiRadDTO: IstrazivackiRadDTO;
}
