import { EntitetDTO } from './entitet.dto';
import { OdeljenjeDTO } from './odeljenje.dto';
import { StudijskiProgramDTO } from './studijski_program.dto';
import { NastavnikFakultetDTO } from './nastavnik_fakultet.dto';
import { UniverzitetDTO } from './univerzitet.dto';
import { NastavnikDTO } from './nastavnik.dto';
import { KontaktDTO } from './kontakt.dto';

export declare interface FakultetDTO extends EntitetDTO {
  naziv: string;
  odeljenjaDTO: OdeljenjeDTO[];
  studijskiProgramiDTO: StudijskiProgramDTO[];
  nastavniciFakultetDTO: NastavnikFakultetDTO[];
  univerzitetDTO: UniverzitetDTO;
  dekanDTO: NastavnikDTO;
  kontaktiDTO: KontaktDTO[];
  opis: string;
}
