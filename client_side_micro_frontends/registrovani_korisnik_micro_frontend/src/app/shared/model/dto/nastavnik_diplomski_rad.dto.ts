import { EntitetDTO } from './entitet.dto';
import { NastavnikDTO } from './nastavnik.dto';
import { DiplomskiRadDTO } from './diplomski_rad.dto';

export declare interface NastavnikDiplomskiRadDTO extends EntitetDTO {
  nastavnikUKomisijiDTO: NastavnikDTO;
  diplomskiRadDTO: DiplomskiRadDTO;
}
