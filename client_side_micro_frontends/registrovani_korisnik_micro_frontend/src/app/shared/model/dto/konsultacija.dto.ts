import { EntitetDTO } from './entitet.dto';
import { GodinaStudijaDTO } from './godina_studija.dto';
import { VremeOdrzavanjaUNedeljiDTO } from './vreme_odrzavanja_u_nedelji.dto';
import { NastavnikDTO } from './nastavnik.dto';

export declare interface KonsultacijaDTO extends EntitetDTO {
  godinaStudijaDTO: GodinaStudijaDTO;
  vremeOdrzavanjaUNedeljiDTO: VremeOdrzavanjaUNedeljiDTO;
  nastavnikDTO: NastavnikDTO;
}
