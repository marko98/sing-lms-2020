import { EntitetDTO } from './entitet.dto';
import { FakultetDTO } from './fakultet.dto';
import { NastavnikDTO } from './nastavnik.dto';

export declare interface NastavnikFakultetDTO extends EntitetDTO {
  datum: string;
  fakultetDTO: FakultetDTO;
  nastavnikDTO: NastavnikDTO;
}
