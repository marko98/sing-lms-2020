import { EntitetDTO } from './entitet.dto';
import { EvaluacijaZnanjaDTO } from './evaluacija_znanja.dto';
import { NastavnikDTO } from './nastavnik.dto';

export declare interface NastavnikEvaluacijaZnanjaDTO extends EntitetDTO {
  datum: string;
  evaluacijaZnanjaDTO: EvaluacijaZnanjaDTO;
  nastavnikDTO: NastavnikDTO;
}
