import { DatumPohadjanjaPredmeta } from '../../../entity/datum_pohadjanja_predmeta.model';
import { DatumPohadjanjaPredmetaDTO } from '../../../dto/datum_pohadjanja_predmeta.dto';
import { PohadjanjePredmetaFactory } from './pohadjanje_predmeta.factory';
import { Factory } from './factory.model';

export class DatumPohadjanjaPredmetaFactory extends Factory {
  public static _instance: DatumPohadjanjaPredmetaFactory;

  public static getInstance = (): DatumPohadjanjaPredmetaFactory => {
    if (!DatumPohadjanjaPredmetaFactory._instance)
      DatumPohadjanjaPredmetaFactory._instance = new DatumPohadjanjaPredmetaFactory();
    return <DatumPohadjanjaPredmetaFactory>(
      DatumPohadjanjaPredmetaFactory._instance
    );
  };

  private constructor() {
    super();
  }

  build(dto: DatumPohadjanjaPredmetaDTO): DatumPohadjanjaPredmeta {
    let datumPohadjanjaPredmeta = new DatumPohadjanjaPredmeta();
    datumPohadjanjaPredmeta.setId(dto.id);
    datumPohadjanjaPredmeta.setDatum(new Date(dto.datum));
    datumPohadjanjaPredmeta.setStanje(dto.stanje);
    datumPohadjanjaPredmeta.setVersion(dto.version);

    if (dto.pohadjanjePredmetaDTO)
      datumPohadjanjaPredmeta.setPohadjanjePredmeta(
        PohadjanjePredmetaFactory.getInstance().build(dto.pohadjanjePredmetaDTO)
      );

    return datumPohadjanjaPredmeta;
  }
}
