import { RegistrovaniKorisnikRolaDTO } from '../../../dto/registrovani_korisnik_rola.dto';
import { RegistrovaniKorisnikRola } from '../../../entity/registrovani_korisnik_rola.model';
import { RegistrovaniKorisnikFactory } from './registrovani_korisnik.factory';
import { RolaFactory } from './rola.factory';
import { Factory } from './factory.model';

export class RegistrovaniKorisnikRolaFactory extends Factory {
  public static _instance: RegistrovaniKorisnikRolaFactory;
  public static getInstance = (): RegistrovaniKorisnikRolaFactory => {
    if (!RegistrovaniKorisnikRolaFactory._instance)
      RegistrovaniKorisnikRolaFactory._instance = new RegistrovaniKorisnikRolaFactory();
    return <RegistrovaniKorisnikRolaFactory>(
      RegistrovaniKorisnikRolaFactory._instance
    );
  };

  private constructor() {
    super();
  }

  build(dto: RegistrovaniKorisnikRolaDTO): RegistrovaniKorisnikRola {
    let registrovaniKorisnikRola = new RegistrovaniKorisnikRola();

    registrovaniKorisnikRola.setId(dto.id);
    registrovaniKorisnikRola.setVersion(dto.version);
    registrovaniKorisnikRola.setStanje(dto.stanje);

    if (dto.registrovaniKorisnikDTO)
      registrovaniKorisnikRola.setRegistrovaniKorisnik(
        RegistrovaniKorisnikFactory.getInstance().build(
          dto.registrovaniKorisnikDTO
        )
      );

    if (dto.rolaDTO)
      registrovaniKorisnikRola.setRola(
        RolaFactory.getInstance().build(dto.rolaDTO)
      );

    return registrovaniKorisnikRola;
  }
}
