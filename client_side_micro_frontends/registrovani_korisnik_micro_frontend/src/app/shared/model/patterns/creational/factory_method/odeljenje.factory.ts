import { Odeljenje } from '../../../entity/odeljenje.model';
import { OdeljenjeDTO } from '../../../dto/odeljenje.dto';
import { AdresaFactory } from './adresa.factory';
import { FakultetFactory } from './fakultet.factory';
import { StudentskaSluzbaFactory } from './studentska_sluzba.factory';
import { ProstorijaFactory } from './prostorija.factory';
import { Factory } from './factory.model';

export class OdeljenjeFactory extends Factory {
  public static _instance: OdeljenjeFactory;
  public static getInstance = (): OdeljenjeFactory => {
    if (!OdeljenjeFactory._instance)
      OdeljenjeFactory._instance = new OdeljenjeFactory();
    return <OdeljenjeFactory>OdeljenjeFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: OdeljenjeDTO): Odeljenje {
    let odeljenje = new Odeljenje();
    odeljenje.setId(dto.id);
    odeljenje.setStanje(dto.stanje);
    odeljenje.setTip(dto.tip);
    odeljenje.setVersion(dto.version);

    if (dto.adresaDTO)
      odeljenje.setAdresa(AdresaFactory.getInstance().build(dto.adresaDTO));
    if (dto.fakultetDTO)
      odeljenje.setFakultet(
        FakultetFactory.getInstance().build(dto.fakultetDTO)
      );
    if (dto.studentskaSluzbaDTO)
      odeljenje.setStudentskaSluzba(
        StudentskaSluzbaFactory.getInstance().build(dto.studentskaSluzbaDTO)
      );

    if (dto.prostorijeDTO)
      odeljenje.setProstorije(
        dto.prostorijeDTO.map((t) => ProstorijaFactory.getInstance().build(t))
      );

    return odeljenje;
  }
}
