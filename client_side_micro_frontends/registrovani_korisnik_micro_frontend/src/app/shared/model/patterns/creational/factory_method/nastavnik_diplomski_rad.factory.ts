import { NastavnikDiplomskiRad } from '../../../entity/nastavnik_diplomski_rad.model';
import { NastavnikDiplomskiRadDTO } from '../../../dto/nastavnik_diplomski_rad.dto';
import { DiplomskiRadFactory } from './diplomski_rad.factory';
import { NastavnikFactory } from './nastavnik.factory';
import { Factory } from './factory.model';

export class NastavnikDiplomskiRadFactory extends Factory {
  public static _instance: NastavnikDiplomskiRadFactory;
  public static getInstance = (): NastavnikDiplomskiRadFactory => {
    if (!NastavnikDiplomskiRadFactory._instance)
      NastavnikDiplomskiRadFactory._instance = new NastavnikDiplomskiRadFactory();
    return <NastavnikDiplomskiRadFactory>NastavnikDiplomskiRadFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: NastavnikDiplomskiRadDTO): NastavnikDiplomskiRad {
    let nastavnikDiplomskiRad = new NastavnikDiplomskiRad();
    nastavnikDiplomskiRad.setId(dto.id);
    nastavnikDiplomskiRad.setStanje(dto.stanje);
    nastavnikDiplomskiRad.setVersion(dto.version);

    if (dto.diplomskiRadDTO)
      nastavnikDiplomskiRad.setDiplomskiRad(
        DiplomskiRadFactory.getInstance().build(dto.diplomskiRadDTO)
      );
    if (dto.nastavnikUKomisijiDTO)
      nastavnikDiplomskiRad.setNastavnikUKomisiji(
        NastavnikFactory.getInstance().build(dto.nastavnikUKomisijiDTO)
      );

    return nastavnikDiplomskiRad;
  }
}
