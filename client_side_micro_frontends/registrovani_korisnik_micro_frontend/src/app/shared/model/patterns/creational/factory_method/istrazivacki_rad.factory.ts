import { IstrazivackiRad } from '../../../entity/istrazivacki_rad.model';
import { IstrazivackiRadDTO } from '../../../dto/istrazivacki_rad.dto';
import { NastavnikFactory } from './nastavnik.factory';
import { RealizacijaPredmetaFactory } from './realizacija_predmeta.factory';
import { IstrazivackiRadStudentNaStudijiFactory } from './istrazivacki_rad_student_na_studiji.factory';
import { Factory } from './factory.model';

export class IstrazivackiRadFactory extends Factory {
  public static _instance: IstrazivackiRadFactory;
  public static getInstance = (): IstrazivackiRadFactory => {
    if (!IstrazivackiRadFactory._instance)
      IstrazivackiRadFactory._instance = new IstrazivackiRadFactory();
    return <IstrazivackiRadFactory>IstrazivackiRadFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: IstrazivackiRadDTO): IstrazivackiRad {
    let istrazivackiRad = new IstrazivackiRad();
    istrazivackiRad.setId(dto.id);
    istrazivackiRad.setStanje(dto.stanje);
    istrazivackiRad.setTema(dto.tema);
    istrazivackiRad.setVersion(dto.version);

    if (dto.nastavnikDTO)
      istrazivackiRad.setNastavnik(
        NastavnikFactory.getInstance().build(dto.nastavnikDTO)
      );
    if (dto.realizacijaPredmetaDTO)
      istrazivackiRad.setRealizacijaPredmeta(
        RealizacijaPredmetaFactory.getInstance().build(
          dto.realizacijaPredmetaDTO
        )
      );

    if (dto.istrazivackiRadStudentiNaStudijamaDTO)
      istrazivackiRad.setIstrazivackiRadStudentiNaStudijama(
        dto.istrazivackiRadStudentiNaStudijamaDTO.map((t) =>
          IstrazivackiRadStudentNaStudijiFactory.getInstance().build(t)
        )
      );

    return istrazivackiRad;
  }
}
