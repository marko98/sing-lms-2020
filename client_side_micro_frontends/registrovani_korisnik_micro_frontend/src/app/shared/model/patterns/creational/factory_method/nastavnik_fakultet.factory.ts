import { NastavnikFakultet } from '../../../entity/nastavnik_fakultet.model';
import { NastavnikFakultetDTO } from '../../../dto/nastavnik_fakultet.dto';
import { FakultetFactory } from './fakultet.factory';
import { NastavnikFactory } from './nastavnik.factory';
import { Factory } from './factory.model';

export class NastavnikFakultetFactory extends Factory {
  public static _instance: NastavnikFakultetFactory;
  public static getInstance = (): NastavnikFakultetFactory => {
    if (!NastavnikFakultetFactory._instance)
      NastavnikFakultetFactory._instance = new NastavnikFakultetFactory();
    return <NastavnikFakultetFactory>NastavnikFakultetFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: NastavnikFakultetDTO): NastavnikFakultet {
    let nastavnikFakultet = new NastavnikFakultet();
    nastavnikFakultet.setId(dto.id);
    nastavnikFakultet.setDatum(new Date(dto.datum));
    nastavnikFakultet.setStanje(dto.stanje);
    nastavnikFakultet.setVersion(dto.version);

    if (dto.fakultetDTO)
      nastavnikFakultet.setFakultet(
        FakultetFactory.getInstance().build(dto.fakultetDTO)
      );
    if (dto.nastavnikDTO)
      nastavnikFakultet.setNastavnik(
        NastavnikFactory.getInstance().build(dto.nastavnikDTO)
      );

    return nastavnikFakultet;
  }
}
