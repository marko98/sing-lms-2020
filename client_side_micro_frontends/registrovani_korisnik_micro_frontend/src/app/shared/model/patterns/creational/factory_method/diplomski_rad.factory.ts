import { DiplomskiRad } from '../../../entity/diplomski_rad.model';
import { DiplomskiRadDTO } from '../../../dto/diplomski_rad.dto';
import { NastavnikFactory } from './nastavnik.factory';
import { StudentNaStudijiFactory } from './student_na_studiji.factory';
import { NastavnikDiplomskiRadFactory } from './nastavnik_diplomski_rad.factory';
import { Factory } from './factory.model';

export class DiplomskiRadFactory extends Factory {
  public static _instance: DiplomskiRadFactory;
  public static getInstance = (): DiplomskiRadFactory => {
    if (!DiplomskiRadFactory._instance)
      DiplomskiRadFactory._instance = new DiplomskiRadFactory();
    return <DiplomskiRadFactory>DiplomskiRadFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: DiplomskiRadDTO): DiplomskiRad {
    let diplomskiRad = new DiplomskiRad();
    diplomskiRad.setId(dto.id);
    diplomskiRad.setOcena(dto.ocena);
    diplomskiRad.setStanje(dto.stanje);
    diplomskiRad.setTema(dto.tema);
    diplomskiRad.setVersion(dto.version);

    if (dto.mentorDTO)
      diplomskiRad.setMentor(
        NastavnikFactory.getInstance().build(dto.mentorDTO)
      );
    if (dto.studentNaStudijiDTO)
      diplomskiRad.setStudentNaStudiji(
        StudentNaStudijiFactory.getInstance().build(dto.studentNaStudijiDTO)
      );

    if (dto.nastavniciDiplomskiRadDTO)
      diplomskiRad.setNastavniciDiplomskiRad(
        dto.nastavniciDiplomskiRadDTO.map((t) =>
          NastavnikDiplomskiRadFactory.getInstance().build(t)
        )
      );
    return diplomskiRad;
  }
}
