import { TerminNastave } from '../../../entity/termin_nastave.model';
import { TerminNastaveDTO } from '../../../dto/termin_nastave.dto';
import { NastavnikFactory } from './nastavnik.factory';
import { RealizacijaPredmetaFactory } from './realizacija_predmeta.factory';
import { ProstorijaFactory } from './prostorija.factory';
import { VremeOdrzavanjaUNedeljiFactory } from './vreme_odrzavanja_u_nedelji.factory';
import { Factory } from './factory.model';

export class TerminNastaveFactory extends Factory {
  public static _instance: TerminNastaveFactory;
  public static getInstance = (): TerminNastaveFactory => {
    if (!TerminNastaveFactory._instance)
      TerminNastaveFactory._instance = new TerminNastaveFactory();
    return <TerminNastaveFactory>TerminNastaveFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: TerminNastaveDTO): TerminNastave {
    let terminNastave = new TerminNastave();
    terminNastave.setId(dto.id);
    terminNastave.setStanje(dto.stanje);
    terminNastave.setTipNastave(dto.tipNastave);
    terminNastave.setTrajanje(dto.trajanje);
    terminNastave.setVersion(dto.version);

    if (dto.nastavnikDTO)
      terminNastave.setNastavnik(
        NastavnikFactory.getInstance().build(dto.nastavnikDTO)
      );
    if (dto.prostorijaDTO)
      terminNastave.setProstorija(
        ProstorijaFactory.getInstance().build(dto.prostorijaDTO)
      );
    if (dto.realizacijaPredmetaDTO)
      terminNastave.setRealizacijaPredmeta(
        RealizacijaPredmetaFactory.getInstance().build(
          dto.realizacijaPredmetaDTO
        )
      );
    if (dto.vremeOdrzavanjaUNedeljiDTO)
      terminNastave.setVremeOdrzavanjaUNedelji(
        VremeOdrzavanjaUNedeljiFactory.getInstance().build(
          dto.vremeOdrzavanjaUNedeljiDTO
        )
      );

    return terminNastave;
  }
}
