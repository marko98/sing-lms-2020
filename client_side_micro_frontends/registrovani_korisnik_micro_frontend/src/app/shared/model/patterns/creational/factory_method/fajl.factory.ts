import { Fajl } from '../../../entity/fajl.model';
import { FajlDTO } from '../../../dto/fajl.dto';
import { EvaluacijaZnanjaFactory } from './evaluacija_znanja.factory';
import { NastavniMaterijalFactory } from './nastavni_materijal.factory';
import { ObavestenjeFactory } from './obavestenje.factory';
import { Factory } from './factory.model';

export class FajlFactory extends Factory {
  public static _instance: FajlFactory;
  public static getInstance = (): FajlFactory => {
    if (!FajlFactory._instance) FajlFactory._instance = new FajlFactory();
    return <FajlFactory>FajlFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: FajlDTO): Fajl {
    let fajl = new Fajl();
    fajl.setId(dto.id);
    fajl.setOpis(dto.opis);
    fajl.setStanje(dto.stanje);
    fajl.setUrl(dto.url);
    fajl.setVersion(dto.version);
    fajl.setNaziv(dto.naziv);
    fajl.setTip(dto.tip);

    if (dto.evaluacijaZnanjaDTO)
      fajl.setEvaluacijaZnanja(
        EvaluacijaZnanjaFactory.getInstance().build(dto.evaluacijaZnanjaDTO)
      );
    if (dto.nastavniMaterijalDTO)
      fajl.setNastavniMaterijal(
        NastavniMaterijalFactory.getInstance().build(dto.nastavniMaterijalDTO)
      );
    if (dto.obavestenjeDTO)
      fajl.setObavestenje(
        ObavestenjeFactory.getInstance().build(dto.obavestenjeDTO)
      );

    return fajl;
  }
}
