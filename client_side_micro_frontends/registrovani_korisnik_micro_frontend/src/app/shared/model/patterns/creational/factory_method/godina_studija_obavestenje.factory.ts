import { GodinaStudijaObavestenje } from '../../../entity/godina_studija_obavestenje.model';
import { GodinaStudijaObavestenjeDTO } from '../../../dto/godina_studija_obavestenje.dto';
import { GodinaStudijaFactory } from './godina_studija.factory';
import { ObavestenjeFactory } from './obavestenje.factory';
import { Factory } from './factory.model';

export class GodinaStudijaObavestenjeFactory extends Factory {
  public static _instance: GodinaStudijaObavestenjeFactory;
  public static getInstance = (): GodinaStudijaObavestenjeFactory => {
    if (!GodinaStudijaObavestenjeFactory._instance)
      GodinaStudijaObavestenjeFactory._instance = new GodinaStudijaObavestenjeFactory();
    return <GodinaStudijaObavestenjeFactory>(
      GodinaStudijaObavestenjeFactory._instance
    );
  };

  private constructor() {
    super();
  }

  build(dto: GodinaStudijaObavestenjeDTO): GodinaStudijaObavestenje {
    let godinaStudijaObavestenje = new GodinaStudijaObavestenje();
    godinaStudijaObavestenje.setId(dto.id);
    godinaStudijaObavestenje.setStanje(dto.stanje);
    godinaStudijaObavestenje.setVersion(dto.version);

    if (dto.godinaStudijaDTO)
      godinaStudijaObavestenje.setGodinaStudija(
        GodinaStudijaFactory.getInstance().build(dto.godinaStudijaDTO)
      );
    if (dto.obavestenjeDTO)
      godinaStudijaObavestenje.setObavestenje(
        ObavestenjeFactory.getInstance().build(dto.obavestenjeDTO)
      );

    return godinaStudijaObavestenje;
  }
}
