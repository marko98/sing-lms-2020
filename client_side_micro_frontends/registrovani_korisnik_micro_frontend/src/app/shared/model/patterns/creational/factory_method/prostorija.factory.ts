import { Prostorija } from '../../../entity/prostorija.model';
import { ProstorijaDTO } from '../../../dto/prostorija.dto';
import { OdeljenjeFactory } from './odeljenje.factory';
import { TerminNastaveFactory } from './termin_nastave.factory';
import { Factory } from './factory.model';

export class ProstorijaFactory extends Factory {
  public static _instance: ProstorijaFactory;
  public static getInstance = (): ProstorijaFactory => {
    if (!ProstorijaFactory._instance)
      ProstorijaFactory._instance = new ProstorijaFactory();
    return <ProstorijaFactory>ProstorijaFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: ProstorijaDTO): Prostorija {
    let prostorija = new Prostorija();
    prostorija.setId(dto.id);
    prostorija.setNaziv(dto.naziv);
    prostorija.setStanje(dto.stanje);
    prostorija.setTip(dto.tip);
    prostorija.setVersion(dto.version);

    if (dto.odeljenjeDTO)
      prostorija.setOdeljenje(
        OdeljenjeFactory.getInstance().build(dto.odeljenjeDTO)
      );

    if (dto.terminiNastaveDTO)
      prostorija.setTerminiNastave(
        dto.terminiNastaveDTO.map((t) =>
          TerminNastaveFactory.getInstance().build(t)
        )
      );

    return prostorija;
  }
}
