import { Identifikacija } from './identifikacija.interface';
import { Formable } from './formable.interface';
import { Tableable } from './tableable.interface';

export declare interface Entitet<T, ID> extends Identifikacija<ID> {}
