import { StanjeModela } from '../enum/stanje_modela.enum';
import { CrudServiceForModel } from '../../service/crud.service-for-model';
import { Subject } from 'rxjs';
import {
  Form,
  FormRow,
  ROW_ITEM_TYPE,
  FormFieldInputNumberInterface,
  FORM_FIELD_INPUT_TYPE,
  SelectInterface,
  VALIDATOR_NAMES,
  FormFieldInputTextInterface,
  FormFieldInputPasswordInterface,
  AT_LEAST_ONE_LOWER_CASE_CHARACTER_ONE_UPPER_CASE_CHARACTER_ONE_DIGIT_ONE_SPECIAL_CHARACTER_REGEX,
  FormFieldInputInterface,
  NUMBERS_ONLY_REGEX,
  DatepickerInterface,
  MAT_COLOR,
} from 'my-angular-form';
import { Validators } from '@angular/forms';
import { HeaderValue, Table } from 'my-angular-table';
import { Formable } from '../interface/formable.interface';
import { Tableable } from '../interface/tableable.interface';
import { RegistrovaniKorisnik } from '../entity/registrovani_korisnik.model';
import { REGISTROVANI_KORISNIK_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/registrovani_korisnik_service.service';
import { take } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';

export class RegistrovaniKorisnikFormableTableable
  implements Formable, Tableable {
  private _formSubject: Subject<Form> = new Subject();

  private _korisnickoImeUlogovanogKorisnika: string;

  public getFormSubject = (): Subject<Form> => {
    return this._formSubject;
  };

  constructor(private _entitet: RegistrovaniKorisnik) {
    this._korisnickoImeUlogovanogKorisnika = JSON.parse(
      localStorage.getItem('ulogovaniKorisnik')
    )['korisnickoIme'];
  }

  public getForm = (
    csfm: CrudServiceForModel,
    isFormForCreation: boolean,
    isFormForDetails: boolean = false,
    isFormForUpdate: boolean = false,
    disableAllFields: boolean = false
  ): void => {
    let form = new Form({
      showSubmitButton: !disableAllFields,
      submitButtonText: 'potvrdi',
      showCancelButton: !disableAllFields,
      cancelButtonText: 'odustani',
      showResetButton: isFormForCreation,
      resetButtonText: 'resetuj',
      submitOnlyIfFormValid: true,
    });

    // private id: number;+

    // private korisnickoIme: string;+

    // private lozinka: string;+

    // private email: string;+

    // private datumRodjenja: Date;+

    // private jmbg: string;+

    // private ime: string;+

    // private prezime: string;+

    // private stanje: StanjeModela;+

    // private version: number;+

    // private adrese: Adresa[];+

    // private nastavnik: Nastavnik;-

    // private administrator: Administrator;-

    // private studentskiSluzbenik: StudentskiSluzbenik;-

    // private student: Student;-

    // private registrovaniKorisnikRole: RegistrovaniKorisnikRola[];+

    if (!isFormForCreation) {
      let rowId = new FormRow();
      form.addChild(rowId);
      rowId.addChildInterface(<FormFieldInputNumberInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
        formFieldInputType: FORM_FIELD_INPUT_TYPE.NUMBER,
        controlName: 'id',
        labelName: 'Id',
        defaultValue: this._entitet.getId(),
        disabled: true,
      });

      // console.log(StanjeModela.AKTIVAN);
      // console.log(typeof StanjeModela.AKTIVAN);

      let rowStanjeModela = new FormRow();
      form.addChild(rowStanjeModela);
      rowStanjeModela.addChildInterface(<SelectInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
        controlName: 'stanje',
        labelName: 'Stanje modela',
        defaultValue: this._entitet.getStanje(),
        optionValues: [
          { value: StanjeModela.AKTIVAN, textToShow: 'AKTIVAN' },
          { value: StanjeModela.NEAKTIVAN, textToShow: 'NEAKTIVAN' },
        ],
        disabled: disableAllFields,
        validators: [
          {
            message: 'Stanje modela je obavezno',
            name: VALIDATOR_NAMES.REQUIRED,
            validatorFn: Validators.required,
          },
        ],
      });
    }

    let formRowImePrezime = new FormRow();
    form.addChild(formRowImePrezime);
    formRowImePrezime.addChildInterface(<FormFieldInputTextInterface>{
      type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
      formFieldInputType: FORM_FIELD_INPUT_TYPE.TEXT,
      controlName: 'ime',
      labelName: 'Ime:',
      appearance: 'outline',
      placeholder: 'unesite ime',
      defaultValue: this._entitet.getIme(),
      disabled: disableAllFields,
      validators: [
        {
          message: 'ime je obavezno',
          name: VALIDATOR_NAMES.REQUIRED,
          validatorFn: Validators.required,
        },
      ],
    });
    formRowImePrezime.addChildInterface(<FormFieldInputTextInterface>{
      type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
      formFieldInputType: FORM_FIELD_INPUT_TYPE.TEXT,
      controlName: 'prezime',
      labelName: 'Prezime:',
      appearance: 'outline',
      placeholder: 'unesite prezime',
      defaultValue: this._entitet.getPrezime(),
      disabled: disableAllFields,
      validators: [
        {
          message: 'prezime je obavezno',
          name: VALIDATOR_NAMES.REQUIRED,
          validatorFn: Validators.required,
        },
      ],
    });

    let formRowKorisnickoImeLozinka = new FormRow();
    form.addChild(formRowKorisnickoImeLozinka);
    formRowKorisnickoImeLozinka.addChildInterface(<FormFieldInputTextInterface>{
      type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
      formFieldInputType: FORM_FIELD_INPUT_TYPE.TEXT,
      controlName: 'korisnickoIme',
      labelName: 'Korisnicko ime:',
      appearance: 'outline',
      placeholder: 'unesite korisnicko ime',
      defaultValue: this._entitet.getKorisnickoIme(),
      disabled: disableAllFields,
      validators: [
        {
          message: 'korisnicko ime je obavezno',
          name: VALIDATOR_NAMES.REQUIRED,
          validatorFn: Validators.required,
        },
        {
          message: 'korisnicko ime mora biti najmanje 5 karaktera dugacko',
          name: VALIDATOR_NAMES.MIN_LENGTH,
          validatorFn: Validators.minLength(5),
          length: 5,
        },
      ],
      showHintAboutMinMaxLength: true,
    });

    if (isFormForCreation)
      formRowKorisnickoImeLozinka.addChildInterface(<
        FormFieldInputPasswordInterface
      >{
        type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
        formFieldInputType: FORM_FIELD_INPUT_TYPE.PASSWORD,
        controlName: 'lozinka',
        labelName: 'Lozinka',
        appearance: 'outline',
        placeholder: 'unesite lozinku',
        disabled: disableAllFields,
        validators: [
          {
            name: VALIDATOR_NAMES.REQUIRED,
            message: 'lozinka je obazvezna',
            validatorFn: Validators.required,
          },
          {
            message: 'lozinka mora biti najmanje 6 karaktera dugacka',
            name: VALIDATOR_NAMES.MIN_LENGTH,
            validatorFn: Validators.minLength(6),
            length: 6,
          },
          {
            message:
              'lozinka mora imati bar jedno malo slovo, jedno veliko slovo, jedan broj i specijalan karakter',
            name: VALIDATOR_NAMES.PATTERN,
            validatorFn: Validators.pattern(
              AT_LEAST_ONE_LOWER_CASE_CHARACTER_ONE_UPPER_CASE_CHARACTER_ONE_DIGIT_ONE_SPECIAL_CHARACTER_REGEX
            ),
          },
        ],
        matImages: 'menu',
        showPasswordInMs: 1500,
        showHintAboutMinMaxLength: true,
      });

    let formRowDatumRodjenjaJmbg = new FormRow();
    form.addChild(formRowDatumRodjenjaJmbg);
    formRowDatumRodjenjaJmbg.addChildInterface(<DatepickerInterface>{
      type: ROW_ITEM_TYPE.DATEPICKER,
      labelName: 'Datum rodjenja',
      controlName: 'datumRodjenja',
      color: MAT_COLOR.ACCENT,
      matIcon: 'date_range',
      disabled: disableAllFields,
      min: new Date(1900, 0, 1),
      max: new Date(new Date().getFullYear() - 18, 0, 1),
      startAt: new Date(new Date().getFullYear() - 19, 0, 1),
      validators: [
        {
          message: 'datum rodjenja je obavezan',
          name: VALIDATOR_NAMES.REQUIRED,
          validatorFn: Validators.required,
        },
      ],
      defaultValue: this._entitet.getDatumRodjenja(),
      appearance: 'outline',
      // disabledDays: [DAY.MONDAY, DAY.SATURDAY],
      timepicker: {
        buttonAlign: 'left',
        /**
         * min i max pisi u formatu:
         * min: 10:00 AM
         * max: 03:00 PM
         */
        // min: '10:00 AM',
        // max: '03:00 PM',
        /**
         * Set a default value and time for a timepicker. The format of the time is in 24 hours notation 23:00.
         * A Date string won't work.
         */
        defaultValue: this._entitet.getDatumRodjenja()
          ? this._entitet.getDatumRodjenja().getHours() +
            ':' +
            this._entitet.getDatumRodjenja().getMinutes()
          : undefined,
        labelName: 'vreme rodjenja',
        disabled: disableAllFields,
      },
    });

    formRowDatumRodjenjaJmbg.addChildInterface(<FormFieldInputTextInterface>{
      type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
      formFieldInputType: FORM_FIELD_INPUT_TYPE.TEXT,
      controlName: 'jmbg',
      labelName: 'Jmbg:',
      appearance: 'outline',
      placeholder: 'unesite jmbg',
      defaultValue: this._entitet.getJmbg(),
      disabled: disableAllFields,
      validators: [
        {
          message: 'jmbg je obazvezan',
          name: VALIDATOR_NAMES.REQUIRED,
          validatorFn: Validators.required,
        },
        {
          message: 'jmbg mora biti 13 karaktera dugacak',
          name: VALIDATOR_NAMES.MIN_LENGTH,
          validatorFn: Validators.minLength(13),
          length: 13,
        },
        {
          message: 'jmbg mora biti 13 karaktera dugacak',
          name: VALIDATOR_NAMES.MAX_LENGTH,
          validatorFn: Validators.maxLength(13),
          length: 13,
        },
        {
          message: 'jmbg nije ispravan',
          name: VALIDATOR_NAMES.PATTERN,
          validatorFn: Validators.pattern(NUMBERS_ONLY_REGEX),
        },
      ],
      showHintAboutMinMaxLength: true,
    });

    let formRowEmail = new FormRow();
    form.addChild(formRowEmail);
    formRowEmail.addChildInterface(<FormFieldInputInterface>{
      type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
      formFieldInputType: FORM_FIELD_INPUT_TYPE.EMAIL,
      controlName: 'email',
      labelName: 'Email',
      appearance: 'outline',
      defaultValue: this._entitet.getEmail(),
      disabled: disableAllFields,
      validators: [
        {
          message: 'email nije ispravan',
          name: VALIDATOR_NAMES.EMAIL,
          validatorFn: Validators.email,
        },
        {
          message: 'email je obavezan',
          name: VALIDATOR_NAMES.REQUIRED,
          validatorFn: Validators.required,
        },
      ],
    });

    this._formSubject.next(form);
    this._formSubject.complete();
  };

  public save = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    let regitrovaniKorisnikService = csfm.getCrudServiceForModel<
      RegistrovaniKorisnik,
      number
    >(REGISTROVANI_KORISNIK_SERVICE_FOR_MODEL_INTERFACE);
    entitetDict.datumRodjenja = new Date(
      entitetDict.datumRodjenja.toUTCString()
    );
    regitrovaniKorisnikService
      .save(entitetDict)
      .pipe(take(1))
      .subscribe(
        (entitet) => {},
        (err: HttpErrorResponse) => {
          subject.error(err);
        },
        () => {
          subject.complete();
        }
      );

    return subject;
  };

  public update = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    let regitrovaniKorisnikService = csfm.getCrudServiceForModel<
      RegistrovaniKorisnik,
      number
    >(REGISTROVANI_KORISNIK_SERVICE_FOR_MODEL_INTERFACE);
    entitetDict.datumRodjenja = new Date(
      entitetDict.datumRodjenja.toUTCString()
    );
    regitrovaniKorisnikService
      .update(entitetDict)
      .pipe(take(1))
      .subscribe(
        (entitet) => {},
        (err: HttpErrorResponse) => {
          subject.error(err);
        },
        () => {
          subject.complete();
        }
      );

    return subject;
  };

  public getHeaderValue = (): HeaderValue => {
    return {
      headerItems: [
        { context: 'id' },
        { context: 'stanje modela' },
        { context: 'ime' },
        { context: 'prezime' },
        { context: 'jmbg' },
        { context: 'datum rodjenja' },
        { context: 'korisnicko ime' },
        { context: 'email' },
        { context: '' },
        { context: '' },
        { context: '' },
      ],
    };
  };

  public addRowToTable = (
    table: Table,
    detaljiFn: Function = () => {},
    izmeniFn: Function = () => {},
    obrisiFn: Function = () => {}
  ): void => {
    if (
      this._korisnickoImeUlogovanogKorisnika !==
      this._entitet.getKorisnickoIme()
    ) {
      try {
        table.addChildValue({
          data: this._entitet,
          rowItems: [
            {
              context: this._entitet.getId(),
            },
            {
              context: this._entitet.getStanje(),
            },
            {
              context: this._entitet.getIme(),
            },
            {
              context: this._entitet.getPrezime(),
            },
            {
              context: this._entitet.getJmbg(),
            },
            {
              context: this._entitet.getDatumRodjenja().toLocaleDateString(),
            },
            {
              context: this._entitet.getKorisnickoIme(),
            },
            {
              context: this._entitet.getEmail(),
            },
            {
              context: 'detalji',
              button: { function: detaljiFn },
            },
            {
              context: 'izmeni',
              button: { function: izmeniFn },
            },
            {
              context:
                this._entitet.getStanje() !== StanjeModela.OBRISAN
                  ? 'obrisi'
                  : '',
              button:
                this._entitet.getStanje() !== StanjeModela.OBRISAN
                  ? { function: obrisiFn }
                  : undefined,
            },
          ],
        });
      } catch (error) {
        console.log(error);
      }
    }
  };
}
