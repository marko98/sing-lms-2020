import { StanjeModela } from '../enum/stanje_modela.enum';
import { CrudServiceForModel } from '../../service/crud.service-for-model';
import { Subject } from 'rxjs';
import {
  Form,
  FormRow,
  ROW_ITEM_TYPE,
  FormFieldInputNumberInterface,
  FORM_FIELD_INPUT_TYPE,
  SelectInterface,
  VALIDATOR_NAMES,
} from 'my-angular-form';
import { Validators } from '@angular/forms';
import { HeaderValue, Table } from 'my-angular-table';
import { Formable } from '../interface/formable.interface';
import { Tableable } from '../interface/tableable.interface';
import { TerminNastave } from '../entity/termin_nastave.model';
import { TERMIN_NASTAVE_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/termin_nastave_service.service';
import { take } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { Prostorija } from '../entity/prostorija.model';
import { PROSTORIJA_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/prostorija_service.service';
import { Nastavnik } from '../entity/nastavnik.model';
import { NASTAVNIK_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/nastavnik_service.service';
import { TipNastave } from '../enum/tip_nastave.enum';
import { RealizacijaPredmeta } from '../entity/realizacija_predmeta.model';
import { REALIZACIJA_PREDMETA_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/realizacija_predmeta_service.service';
import { VremeOdrzavanjaUNedelji } from '../entity/vreme_odrzavanja_u_nedelji.model';
import { VREME_ODRZAVANJA_U_NEDELJI_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/vreme_odrzavanja_u_nedelji_service.service';
import { VremeOdrzavanjaUNedeljiDTO } from '../dto/vreme_odrzavanja_u_nedelji.dto';

export class TerminNastaveFormableTableable implements Formable, Tableable {
  private _formSubject: Subject<Form> = new Subject();

  public getFormSubject = (): Subject<Form> => {
    return this._formSubject;
  };

  constructor(private _entitet: TerminNastave) {}

  public getForm = (
    csfm: CrudServiceForModel,
    isFormForCreation: boolean,
    isFormForDetails: boolean = false,
    isFormForUpdate: boolean = false,
    disableAllFields: boolean = false
  ): void => {
    let form = new Form({
      showSubmitButton: !disableAllFields,
      submitButtonText: 'potvrdi',
      showCancelButton: !disableAllFields,
      cancelButtonText: 'odustani',
      showResetButton: isFormForCreation,
      resetButtonText: 'resetuj',
      submitOnlyIfFormValid: true,
    });

    // private id: number;+

    // private trajanje: number;+

    // private stanje: StanjeModela;+

    // private version: number;+

    // private prostorija: Prostorija;+

    // private nastavnik: Nastavnik;+

    // private realizacijaPredmeta: RealizacijaPredmeta;+

    // private tipNastave: TipNastave;+

    // private vremeOdrzavanjaUNedelji: VremeOdrzavanjaUNedelji;

    if (!isFormForCreation) {
      let rowId = new FormRow();
      form.addChild(rowId);
      rowId.addChildInterface(<FormFieldInputNumberInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
        formFieldInputType: FORM_FIELD_INPUT_TYPE.NUMBER,
        controlName: 'id',
        labelName: 'Id',
        defaultValue: this._entitet.getId(),
        disabled: true,
      });

      // console.log(StanjeModela.AKTIVAN);
      // console.log(typeof StanjeModela.AKTIVAN);

      let rowStanjeModela = new FormRow();
      form.addChild(rowStanjeModela);
      rowStanjeModela.addChildInterface(<SelectInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
        controlName: 'stanje',
        labelName: 'Stanje modela',
        defaultValue: this._entitet.getStanje(),
        optionValues: [
          { value: StanjeModela.AKTIVAN, textToShow: 'AKTIVAN' },
          { value: StanjeModela.NEAKTIVAN, textToShow: 'NEAKTIVAN' },
        ],
        disabled: disableAllFields,
        validators: [
          {
            message: 'Stanje modela je obavezno',
            name: VALIDATOR_NAMES.REQUIRED,
            validatorFn: Validators.required,
          },
        ],
      });
    }

    let rowTrajanje = new FormRow();
    form.addChild(rowTrajanje);
    rowTrajanje.addChildInterface(<FormFieldInputNumberInterface>{
      type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
      formFieldInputType: FORM_FIELD_INPUT_TYPE.NUMBER,
      controlName: 'trajanje',
      labelName: 'Trajanje u minutima',
      step: 1,
      textSuffix: 'min.',
      defaultValue: this._entitet.getTrajanje(),
      disabled: disableAllFields,
      validators: [
        {
          message: 'nastava mora da traje najmanje 15 minuta',
          name: VALIDATOR_NAMES.MIN,
          validatorFn: Validators.min(15),
        },
        {
          message:
            'nastava moze da traje najvise 315 minuta (7 casova po 45 minuta)',
          name: VALIDATOR_NAMES.MAX,
          validatorFn: Validators.max(315),
        },
      ],
    });

    let rowTipNastave = new FormRow();
    form.addChild(rowTipNastave);
    rowTipNastave.addChildInterface(<SelectInterface>{
      type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
      controlName: 'tipNastave',
      labelName: 'Tip nastave',
      defaultValue: this._entitet.getTipNastave(),
      optionValues: [
        {
          value: TipNastave.DODATNA_NASTAVA,
          textToShow: TipNastave.DODATNA_NASTAVA,
        },
        {
          value: TipNastave.FAKULTETSKI_PROJEKAT,
          textToShow: TipNastave.FAKULTETSKI_PROJEKAT,
        },
        {
          value: TipNastave.ISTRAZIVACKI_RAD,
          textToShow: TipNastave.ISTRAZIVACKI_RAD,
        },
        {
          value: TipNastave.OSTALI_CASOVI,
          textToShow: TipNastave.OSTALI_CASOVI,
        },
        { value: TipNastave.PREDAVANJA, textToShow: TipNastave.PREDAVANJA },
        { value: TipNastave.RADIONICA, textToShow: TipNastave.RADIONICA },
        {
          value: TipNastave.TIMSKI_PROJEKAT,
          textToShow: TipNastave.TIMSKI_PROJEKAT,
        },
        { value: TipNastave.VEZBE, textToShow: TipNastave.VEZBE },
      ],
      disabled: disableAllFields,
      validators: [
        {
          message: 'Tip nastave je obavezan',
          name: VALIDATOR_NAMES.REQUIRED,
          validatorFn: Validators.required,
        },
      ],
    });

    let prostorijaService = csfm.getCrudServiceForModel<Prostorija, number>(
      PROSTORIJA_SERVICE_FOR_MODEL_INTERFACE
    );

    prostorijaService
      .findAll()
      .pipe(take(1))
      .subscribe(
        (entiteti: Prostorija[]) => {
          for (let index = 0; index < entiteti.length; index++) {
            if (
              entiteti[index].getId().toString() ===
              this._entitet.getProstorija()?.getId().toString()
            ) {
              this._entitet.setProstorija(entiteti[index]);
              break;
            }
          }

          if (isFormForCreation) {
            // entiteti = entiteti.filter((r) => !r.getUniverzitet());
          } else if (isFormForUpdate) {
            // entiteti = entiteti.filter((r) => !r.getUniverzitet());
            // entiteti.push(this._entitet.getProstorija());
          } else if (isFormForDetails) {
            if (
              this._entitet.getProstorija() &&
              !entiteti.find(
                (e) =>
                  e.getId().toString() ===
                  this._entitet.getProstorija().getId().toString()
              )
            )
              entiteti.push(this._entitet.getProstorija());
          }

          // if (!(entiteti.length > 0)) {
          //   this._formSubject.error(
          //     new Error(
          //       'Sve studentske sluzbe vec pripadaju nekim univerzitetima'
          //     )
          //   );
          //   return;
          // } else {
          let rowEntitet = new FormRow();
          form.addChild(rowEntitet);
          rowEntitet.addChildInterface(<SelectInterface>{
            type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
            controlName: 'prostorija',
            labelName: 'Prostorija',
            defaultValue: this._entitet.getProstorija(),
            optionValues: entiteti.map((d) => {
              return {
                value: d,
                textToShow: 'id: ' + d.getId().toString() + ', ' + d.getNaziv(),
              };
            }),
            disabled: disableAllFields,
          });
          // }

          this._formSubject.next(form);
          // --------------------------------------
          let nastavnikService = csfm.getCrudServiceForModel<Nastavnik, number>(
            NASTAVNIK_SERVICE_FOR_MODEL_INTERFACE
          );

          nastavnikService
            .findAll()
            .pipe(take(1))
            .subscribe(
              (entiteti: Nastavnik[]) => {
                for (let index = 0; index < entiteti.length; index++) {
                  if (
                    entiteti[index].getId().toString() ===
                    this._entitet.getNastavnik()?.getId().toString()
                  ) {
                    this._entitet.setNastavnik(entiteti[index]);
                    break;
                  }
                }

                if (isFormForCreation) {
                  // entiteti = entiteti.filter((r) => !r.getUniverzitet());
                } else if (isFormForUpdate) {
                  // entiteti = entiteti.filter((r) => !r.getUniverzitet());
                  // entiteti.push(this._entitet.getNastavnik());
                } else if (isFormForDetails) {
                  if (
                    this._entitet.getNastavnik() &&
                    !entiteti.find(
                      (e) =>
                        e.getId().toString() ===
                        this._entitet.getNastavnik().getId().toString()
                    )
                  )
                    entiteti.push(this._entitet.getNastavnik());
                }

                // if (!(entiteti.length > 0)) {
                //   this._formSubject.error(
                //     new Error(
                //       'Sve studentske sluzbe vec pripadaju nekim univerzitetima'
                //     )
                //   );
                //   return;
                // } else {
                let rowEntitet = new FormRow();
                form.addChild(rowEntitet);
                rowEntitet.addChildInterface(<SelectInterface>{
                  type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
                  controlName: 'nastavnik',
                  labelName: 'Nastavnik',
                  defaultValue: this._entitet.getNastavnik(),
                  optionValues: entiteti.map((d) => {
                    return {
                      value: d,
                      textToShow: d.getRegistrovaniKorisnik()
                        ? 'id: ' +
                          d.getId().toString() +
                          ', ' +
                          d.getRegistrovaniKorisnik().getIme() +
                          ' ' +
                          d.getRegistrovaniKorisnik().getPrezime()
                        : 'id: ' + d.getId().toString(),
                    };
                  }),
                  disabled: disableAllFields,
                });
                // }

                this._formSubject.next(form);
                // --------------------------------------
                let realizacijapredmetaService = csfm.getCrudServiceForModel<
                  RealizacijaPredmeta,
                  number
                >(REALIZACIJA_PREDMETA_SERVICE_FOR_MODEL_INTERFACE);

                realizacijapredmetaService
                  .findAll()
                  .pipe(take(1))
                  .subscribe(
                    (entiteti: RealizacijaPredmeta[]) => {
                      for (let index = 0; index < entiteti.length; index++) {
                        if (
                          entiteti[index].getId().toString() ===
                          this._entitet
                            .getRealizacijaPredmeta()
                            ?.getId()
                            .toString()
                        ) {
                          this._entitet.setRealizacijaPredmeta(entiteti[index]);
                          break;
                        }
                      }

                      if (isFormForCreation) {
                        // entiteti = entiteti.filter((r) => !r.getUniverzitet());
                      } else if (isFormForUpdate) {
                        // entiteti = entiteti.filter((r) => !r.getUniverzitet());
                        // entiteti.push(this._entitet.getRealizacijaPredmeta());
                      } else if (isFormForDetails) {
                        if (
                          this._entitet.getRealizacijaPredmeta() &&
                          !entiteti.find(
                            (e) =>
                              e.getId().toString() ===
                              this._entitet
                                .getRealizacijaPredmeta()
                                .getId()
                                .toString()
                          )
                        )
                          entiteti.push(this._entitet.getRealizacijaPredmeta());
                      }

                      // if (!(entiteti.length > 0)) {
                      //   this._formSubject.error(
                      //     new Error(
                      //       'Sve studentske sluzbe vec pripadaju nekim univerzitetima'
                      //     )
                      //   );
                      //   return;
                      // } else {
                      let rowEntitet = new FormRow();
                      form.addChild(rowEntitet);
                      rowEntitet.addChildInterface(<SelectInterface>{
                        type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
                        controlName: 'realizacijaPredmeta',
                        labelName: 'Realizacija predmeta',
                        defaultValue: this._entitet.getRealizacijaPredmeta(),
                        optionValues: entiteti.map((d) => {
                          return {
                            value: d,
                            textToShow: d.getPredmet()
                              ? 'id: ' +
                                d.getId().toString() +
                                ', ' +
                                d.getPredmet().getNaziv()
                              : 'id: ' + d.getId().toString(),
                          };
                        }),
                        disabled: disableAllFields,
                      });
                      // }

                      this._formSubject.next(form);
                      // --------------------------------------
                      let vremeOdrzavanjaUNedeljiService = csfm.getCrudServiceForModel<
                        VremeOdrzavanjaUNedelji,
                        number
                      >(VREME_ODRZAVANJA_U_NEDELJI_SERVICE_FOR_MODEL_INTERFACE);

                      vremeOdrzavanjaUNedeljiService
                        .findAll()
                        .pipe(take(1))
                        .subscribe(
                          (entiteti: VremeOdrzavanjaUNedelji[]) => {
                            for (
                              let index = 0;
                              index < entiteti.length;
                              index++
                            ) {
                              if (
                                entiteti[index].getId().toString() ===
                                this._entitet
                                  .getVremeOdrzavanjaUNedelji()
                                  ?.getId()
                                  .toString()
                              ) {
                                this._entitet.setVremeOdrzavanjaUNedelji(
                                  entiteti[index]
                                );
                                break;
                              }
                            }

                            if (isFormForCreation) {
                              entiteti = entiteti.filter(
                                (r) => !r.getTerminNastave()
                              );
                            } else if (isFormForUpdate) {
                              entiteti = entiteti.filter(
                                (r) => !r.getTerminNastave()
                              );
                              entiteti.push(
                                this._entitet.getVremeOdrzavanjaUNedelji()
                              );
                            } else if (isFormForDetails) {
                              if (
                                this._entitet.getVremeOdrzavanjaUNedelji() &&
                                !entiteti.find(
                                  (e) =>
                                    e.getId().toString() ===
                                    this._entitet
                                      .getVremeOdrzavanjaUNedelji()
                                      .getId()
                                      .toString()
                                )
                              )
                                entiteti.push(
                                  this._entitet.getVremeOdrzavanjaUNedelji()
                                );
                            }

                            if (!(entiteti.length > 0)) {
                              this._formSubject.error(
                                new Error(
                                  'Sva vremena odrzavanja u nedelji vec pripadaju nekim terminima nastave'
                                )
                              );
                              return;
                            } else {
                              let rowEntitet = new FormRow();
                              form.addChild(rowEntitet);
                              rowEntitet.addChildInterface(<SelectInterface>{
                                type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
                                controlName: 'vremeOdrzavanjaUNedelji',
                                labelName: 'Vreme odrzavanja u nedelji',
                                defaultValue: this._entitet.getVremeOdrzavanjaUNedelji(),
                                optionValues: entiteti.map((d) => {
                                  return {
                                    value: d,
                                    textToShow:
                                      'id: ' +
                                      d.getId() +
                                      ', ' +
                                      d.getDan() +
                                      ' - ' +
                                      d.getVreme().hours +
                                      ':' +
                                      d.getVreme().minutes,
                                  };
                                }),
                                disabled: disableAllFields,
                              });
                            }

                            this._formSubject.next(form);
                            this._formSubject.complete();
                          },
                          (err: HttpErrorResponse) => {
                            this._formSubject.error(err);
                          }
                        );
                      // ------------------------------------
                    },
                    (err: HttpErrorResponse) => {
                      this._formSubject.error(err);
                    }
                  );
                // ------------------------------------
              },
              (err: HttpErrorResponse) => {
                this._formSubject.error(err);
              }
            );
          // ------------------------------------
        },
        (err: HttpErrorResponse) => {
          this._formSubject.error(err);
        }
      );
  };

  public save = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    let datum = new Date();
    datum.setHours(
      (<VremeOdrzavanjaUNedelji>entitetDict.vremeOdrzavanjaUNedelji).getVreme()
        .hours
    );
    datum.setMinutes(
      (<VremeOdrzavanjaUNedelji>entitetDict.vremeOdrzavanjaUNedelji).getVreme()
        .minutes
    );
    datum.setSeconds(0);
    entitetDict.vremeOdrzavanjaUNedelji.vreme = datum
      .toUTCString()
      .split(' ')[4];
    // console.log(entitetDict);
    let terminNastaveService = csfm.getCrudServiceForModel<
      TerminNastave,
      number
    >(TERMIN_NASTAVE_SERVICE_FOR_MODEL_INTERFACE);
    terminNastaveService
      .save(entitetDict)
      .pipe(take(1))
      .subscribe(
        (entitet) => {},
        (err: HttpErrorResponse) => {
          subject.error(err);
        },
        () => {
          subject.complete();
        }
      );

    return subject;
  };

  public update = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    let datum = new Date();
    datum.setHours(
      (<VremeOdrzavanjaUNedelji>entitetDict.vremeOdrzavanjaUNedelji).getVreme()
        .hours
    );
    datum.setMinutes(
      (<VremeOdrzavanjaUNedelji>entitetDict.vremeOdrzavanjaUNedelji).getVreme()
        .minutes
    );
    datum.setSeconds(0);
    entitetDict.vremeOdrzavanjaUNedelji.vreme = datum
      .toUTCString()
      .split(' ')[4];
    // console.log(entitetDict);
    let terminNastaveService = csfm.getCrudServiceForModel<
      TerminNastave,
      number
    >(TERMIN_NASTAVE_SERVICE_FOR_MODEL_INTERFACE);
    terminNastaveService
      .update(entitetDict)
      .pipe(take(1))
      .subscribe(
        (entitet) => {},
        (err: HttpErrorResponse) => {
          subject.error(err);
        },
        () => {
          subject.complete();
        }
      );

    return subject;
  };

  public getHeaderValue = (): HeaderValue => {
    return {
      headerItems: [
        { context: 'id' },
        { context: 'stanje modela' },
        { context: 'trajanje u min' },
        { context: 'prostorija' },
        { context: 'nastavnik' },
        { context: 'realizacija predmeta' },
        { context: 'tip nastave' },
        { context: 'vreme odrzavanja u nedelji' },
        { context: '' },
        { context: '' },
        { context: '' },
      ],
    };
  };

  public addRowToTable = (
    table: Table,
    detaljiFn: Function = () => {},
    izmeniFn: Function = () => {},
    obrisiFn: Function = () => {}
  ): void => {
    try {
      table.addChildValue({
        data: this._entitet,
        rowItems: [
          {
            context: this._entitet.getId(),
          },
          {
            context: this._entitet.getStanje(),
          },
          {
            context: this._entitet.getTrajanje().toString(),
          },
          {
            context: this._entitet.getProstorija()
              ? 'id: ' +
                this._entitet.getProstorija().getId() +
                ', ' +
                this._entitet.getProstorija().getNaziv()
              : '/',
          },
          {
            context: this._entitet.getNastavnik()
              ? 'id: ' + this._entitet.getNastavnik().getId()
              : '/',
          },
          {
            context: this._entitet.getRealizacijaPredmeta()
              ? 'id: ' + this._entitet.getRealizacijaPredmeta().getId()
              : '/',
          },
          {
            context: this._entitet
              .getTipNastave()
              .replace('_', ' ')
              .toLowerCase(),
          },
          {
            context: this._entitet.getVremeOdrzavanjaUNedelji()
              ? 'id: ' +
                this._entitet.getVremeOdrzavanjaUNedelji().getId() +
                ', ' +
                this._entitet.getVremeOdrzavanjaUNedelji().getDan() +
                ' - ' +
                this._entitet.getVremeOdrzavanjaUNedelji().getVreme().hours +
                ':' +
                this._entitet.getVremeOdrzavanjaUNedelji().getVreme().minutes
              : '/',
          },
          {
            context: 'detalji',
            button: { function: detaljiFn },
          },
          {
            context: 'izmeni',
            button: { function: izmeniFn },
          },
          {
            context:
              this._entitet.getStanje() !== StanjeModela.OBRISAN
                ? 'obrisi'
                : '',
            button:
              this._entitet.getStanje() !== StanjeModela.OBRISAN
                ? { function: obrisiFn }
                : undefined,
          },
        ],
      });
    } catch (error) {
      console.log(error);
    }
  };
}
