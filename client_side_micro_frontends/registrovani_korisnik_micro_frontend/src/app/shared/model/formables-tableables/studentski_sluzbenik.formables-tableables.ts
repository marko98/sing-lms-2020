import { StanjeModela } from '../enum/stanje_modela.enum';
import { CrudServiceForModel } from '../../service/crud.service-for-model';
import { Subject } from 'rxjs';
import {
  Form,
  FormRow,
  ROW_ITEM_TYPE,
  FormFieldInputNumberInterface,
  FORM_FIELD_INPUT_TYPE,
  SelectInterface,
  VALIDATOR_NAMES,
} from 'my-angular-form';
import { Validators } from '@angular/forms';
import { HeaderValue, Table } from 'my-angular-table';
import { Formable } from '../interface/formable.interface';
import { Tableable } from '../interface/tableable.interface';
import { StudentskiSluzbenik } from '../entity/studentski_sluzbenik.model';
import { StudentskaSluzba } from '../entity/studentska_sluzba.model';
import { STUDENTSKI_SLUZBENIK_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/studentski_sluzbenik_service.service';
import { take } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { RegistrovaniKorisnik } from '../entity/registrovani_korisnik.model';
import { REGISTROVANI_KORISNIK_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/registrovani_korisnik_service.service';
import { STUDENTSKA_SLUZBA_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/studentska_sluzba_service.service';

export class StudentskiSluzbenikFormableTableable
  implements Formable, Tableable {
  private _formSubject: Subject<Form> = new Subject();

  public getFormSubject = (): Subject<Form> => {
    return this._formSubject;
  };

  constructor(private _entitet: StudentskiSluzbenik) {}

  public getForm = (
    csfm: CrudServiceForModel,
    isFormForCreation: boolean,
    isFormForDetails: boolean = false,
    isFormForUpdate: boolean = false,
    disableAllFields: boolean = false
  ): void => {
    // private id: number;

    // private stanje: StanjeModela;

    // private version: number;

    // private registrovaniKorisnik: RegistrovaniKorisnik;

    // private studentskaSluzba: StudentskaSluzba;

    let form = new Form({
      showSubmitButton: !disableAllFields,
      submitButtonText: 'potvrdi',
      showCancelButton: !disableAllFields,
      cancelButtonText: 'odustani',
      showResetButton: isFormForCreation,
      resetButtonText: 'resetuj',
      submitOnlyIfFormValid: true,
    });

    if (!isFormForCreation) {
      let rowId = new FormRow();
      form.addChild(rowId);
      rowId.addChildInterface(<FormFieldInputNumberInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
        formFieldInputType: FORM_FIELD_INPUT_TYPE.NUMBER,
        controlName: 'id',
        labelName: 'Id',
        defaultValue: this._entitet.getId(),
        disabled: true,
      });

      // console.log(StanjeModela.AKTIVAN);
      // console.log(typeof StanjeModela.AKTIVAN);

      let rowStanjeModela = new FormRow();
      form.addChild(rowStanjeModela);
      rowStanjeModela.addChildInterface(<SelectInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
        controlName: 'stanje',
        labelName: 'Stanje modela',
        defaultValue: this._entitet.getStanje(),
        optionValues: [
          { value: StanjeModela.AKTIVAN, textToShow: 'AKTIVAN' },
          { value: StanjeModela.NEAKTIVAN, textToShow: 'NEAKTIVAN' },
        ],
        disabled: disableAllFields,
        validators: [
          {
            message: 'Stanje modela je obavezno',
            name: VALIDATOR_NAMES.REQUIRED,
            validatorFn: Validators.required,
          },
        ],
      });
    }

    let registrovaniKorisnikService = csfm.getCrudServiceForModel<
      RegistrovaniKorisnik,
      number
    >(REGISTROVANI_KORISNIK_SERVICE_FOR_MODEL_INTERFACE);

    registrovaniKorisnikService
      .findAll()
      .pipe(take(1))
      .subscribe(
        (rks: RegistrovaniKorisnik[]) => {
          for (let index = 0; index < rks.length; index++) {
            if (
              rks[index].getId().toString() ===
              this._entitet.getRegistrovaniKorisnik()?.getId().toString()
            ) {
              this._entitet.setRegistrovaniKorisnik(rks[index]);
              break;
            }
          }

          if (isFormForCreation) {
            rks = rks.filter((r) => !r.getStudentskiSluzbenik());
          } else if (isFormForUpdate) {
            rks = rks.filter((r) => !r.getStudentskiSluzbenik());
            rks.push(this._entitet.getRegistrovaniKorisnik());
          } else if (isFormForDetails) {
            if (
              this._entitet.getRegistrovaniKorisnik() &&
              !rks.find(
                (e) =>
                  e.getId().toString() ===
                  this._entitet.getRegistrovaniKorisnik().getId().toString()
              )
            )
              rks.push(this._entitet.getRegistrovaniKorisnik());
          }

          if (!(rks.length > 0)) {
            this._formSubject.error(
              new Error(
                'Svi registrovani korisnici su vec i studentski sluzbenici'
              )
            );
            return;
          }

          let rowEntitet = new FormRow();
          form.addChild(rowEntitet);
          rowEntitet.addChildInterface(<SelectInterface>{
            type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
            controlName: 'registrovaniKorisnik',
            labelName: 'Registrovani korisnik',
            defaultValue: this._entitet.getRegistrovaniKorisnik(),
            optionValues: rks.map((d) => {
              return {
                value: d,
                textToShow:
                  d.getId().toString() +
                  ' ' +
                  d.getIme() +
                  ' ' +
                  d.getPrezime(),
              };
            }),
            disabled: disableAllFields,
          });
          this._formSubject.next(form);
          // --------------------------------------
          let studentskaSluzbaService = csfm.getCrudServiceForModel<
            StudentskaSluzba,
            number
          >(STUDENTSKA_SLUZBA_SERVICE_FOR_MODEL_INTERFACE);

          studentskaSluzbaService
            .findAll()
            .pipe(take(1))
            .subscribe(
              (entiteti: StudentskaSluzba[]) => {
                for (let index = 0; index < entiteti.length; index++) {
                  if (
                    entiteti[index].getId().toString() ===
                    this._entitet.getStudentskaSluzba()?.getId().toString()
                  ) {
                    this._entitet.setStudentskaSluzba(entiteti[index]);
                    break;
                  }
                }

                if (isFormForCreation) {
                  // entiteti = entiteti.filter((r) => !r.getStudentskiSluzbenik());
                } else if (isFormForUpdate) {
                  // entiteti = entiteti.filter((r) => !r.getStudentskiSluzbenik());
                  // entiteti.push(this._entitet.getStudentskaSluzba());
                } else if (isFormForDetails) {
                  entiteti = [];
                  entiteti.push(this._entitet.getStudentskaSluzba());
                }

                // if (!(entiteti.length > 0)) {
                //   this._formSubject.error(
                //     new Error(
                //       'Svi registrovani korisnici su vec i studentski sluzbenici'
                //     )
                //   );
                //   return;
                // }

                let rowEntitet = new FormRow();
                form.addChild(rowEntitet);
                rowEntitet.addChildInterface(<SelectInterface>{
                  type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
                  controlName: 'studentskaSluzba',
                  labelName: 'Studentska sluzba',
                  defaultValue: this._entitet.getStudentskaSluzba(),
                  optionValues: entiteti.map((d) => {
                    return {
                      value: d,
                      textToShow: d.getId().toString(),
                    };
                  }),
                  disabled: disableAllFields,
                });
                this._formSubject.next(form);
                this._formSubject.complete();
              },
              (err: HttpErrorResponse) => {
                this._formSubject.error(err);
              }
            );
          // -------------------------------------
        },
        (err: HttpErrorResponse) => {
          this._formSubject.error(err);
        }
      );
  };

  public save = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    let studentskiSluzbenikService = csfm.getCrudServiceForModel<
      StudentskiSluzbenik,
      number
    >(STUDENTSKI_SLUZBENIK_SERVICE_FOR_MODEL_INTERFACE);
    studentskiSluzbenikService
      .save(entitetDict)
      .pipe(take(1))
      .subscribe(
        (entitet) => {},
        (err: HttpErrorResponse) => {
          subject.error(err);
        },
        () => {
          subject.complete();
        }
      );

    return subject;
  };

  public update = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    let studentskiSluzbenikService = csfm.getCrudServiceForModel<
      StudentskiSluzbenik,
      number
    >(STUDENTSKI_SLUZBENIK_SERVICE_FOR_MODEL_INTERFACE);
    studentskiSluzbenikService
      .update(entitetDict)
      .pipe(take(1))
      .subscribe(
        (entitet) => {},
        (err: HttpErrorResponse) => {
          subject.error(err);
        },
        () => {
          subject.complete();
        }
      );

    return subject;
  };

  public getHeaderValue = (): HeaderValue => {
    return {
      headerItems: [
        { context: 'id' },
        { context: 'stanje modela' },
        { context: 'registrovani korisnik' },
        { context: 'studentska sluzba' },
        { context: '' },
        { context: '' },
        { context: '' },
      ],
    };
  };

  public addRowToTable = (
    table: Table,
    detaljiFn: Function = () => {},
    izmeniFn: Function = () => {},
    obrisiFn: Function = () => {}
  ): void => {
    try {
      table.addChildValue({
        data: this._entitet,
        rowItems: [
          {
            context: this._entitet.getId(),
          },
          {
            context: this._entitet.getStanje(),
          },
          {
            context: this._entitet.getRegistrovaniKorisnik()
              ? this._entitet.getRegistrovaniKorisnik().getId() +
                ' ' +
                this._entitet.getRegistrovaniKorisnik().getIme() +
                ' ' +
                this._entitet.getRegistrovaniKorisnik().getPrezime()
              : '/',
          },
          {
            context: this._entitet.getStudentskaSluzba()
              ? this._entitet.getStudentskaSluzba().getId()
              : '/',
          },
          {
            context: 'detalji',
            button: { function: detaljiFn },
          },
          {
            context: 'izmeni',
            button: { function: izmeniFn },
          },
          {
            context:
              this._entitet.getStanje() !== StanjeModela.OBRISAN
                ? 'obrisi'
                : '',
            button:
              this._entitet.getStanje() !== StanjeModela.OBRISAN
                ? { function: obrisiFn }
                : undefined,
          },
        ],
      });
    } catch (error) {
      console.log(error);
    }
  };
}
