import { StanjeModela } from '../enum/stanje_modela.enum';
import { CrudServiceForModel } from '../../service/crud.service-for-model';
import { Subject } from 'rxjs';
import {
  Form,
  FormRow,
  ROW_ITEM_TYPE,
  FormFieldInputNumberInterface,
  FORM_FIELD_INPUT_TYPE,
  SelectInterface,
  VALIDATOR_NAMES,
  FormFieldInputTextInterface,
} from 'my-angular-form';
import { Validators } from '@angular/forms';
import { HeaderValue, Table } from 'my-angular-table';
import { Tableable } from '../interface/tableable.interface';
import { Formable } from '../interface/formable.interface';
import { Adresa } from '../entity/adresa.model';
import { ADRESA_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/adresa_service.service';
import { take } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { TipAdrese } from '../enum/tip_adrese.enum';
import { Grad } from '../entity/grad.model';
import { GRAD_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/grad_service.service';
import { Odeljenje } from '../entity/odeljenje.model';
import { ODELJENJE_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/odeljenje_service.service';
import { Univerzitet } from '../entity/univerzitet.model';
import { UNIVERZITET_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/univerzitet_service.service';
import { RegistrovaniKorisnik } from '../entity/registrovani_korisnik.model';
import { REGISTROVANI_KORISNIK_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/registrovani_korisnik_service.service';

export class AdresaFormableTableable implements Formable, Tableable {
  private _formSubject: Subject<Form> = new Subject();

  public getFormSubject = (): Subject<Form> => {
    return this._formSubject;
  };

  constructor(private _entitet: Adresa) {}

  public getForm = (
    csfm: CrudServiceForModel,
    isFormForCreation: boolean,
    isFormForDetails: boolean = false,
    isFormForUpdate: boolean = false,
    disableAllFields: boolean = false
  ): void => {
    // private id: number;+

    // private ulica: string;+

    // private broj: string;+

    // private stanje: StanjeModela;+

    // private version: number;+

    // private grad: Grad;+

    // private tip: TipAdrese;+

    // private odeljenje: Odeljenje;+

    // private univerzitet: Univerzitet;+

    // private registrovaniKorisnik: RegistrovaniKorisnik;+

    let form = new Form({
      showSubmitButton: !disableAllFields,
      submitButtonText: 'potvrdi',
      showCancelButton: !disableAllFields,
      cancelButtonText: 'odustani',
      showResetButton: isFormForCreation,
      resetButtonText: 'resetuj',
      submitOnlyIfFormValid: true,
    });

    if (!isFormForCreation) {
      let rowId = new FormRow();
      form.addChild(rowId);
      rowId.addChildInterface(<FormFieldInputNumberInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
        formFieldInputType: FORM_FIELD_INPUT_TYPE.NUMBER,
        controlName: 'id',
        labelName: 'Id',
        defaultValue: this._entitet.getId(),
        disabled: true,
      });

      // console.log(StanjeModela.AKTIVAN);
      // console.log(typeof StanjeModela.AKTIVAN);

      let rowStanjeModela = new FormRow();
      form.addChild(rowStanjeModela);
      rowStanjeModela.addChildInterface(<SelectInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
        controlName: 'stanje',
        labelName: 'Stanje modela',
        defaultValue: this._entitet.getStanje(),
        optionValues: [
          { value: StanjeModela.AKTIVAN, textToShow: 'AKTIVAN' },
          { value: StanjeModela.NEAKTIVAN, textToShow: 'NEAKTIVAN' },
        ],
        disabled: disableAllFields,
        validators: [
          {
            message: 'Stanje modela je obavezno',
            name: VALIDATOR_NAMES.REQUIRED,
            validatorFn: Validators.required,
          },
        ],
      });
    }

    let rowUlica = new FormRow();
    form.addChild(rowUlica);
    rowUlica.addChildInterface(<FormFieldInputTextInterface>{
      type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
      formFieldInputType: FORM_FIELD_INPUT_TYPE.TEXT,
      controlName: 'ulica',
      labelName: 'Ulica',
      defaultValue: this._entitet.getUlica(),
      appearance: 'outline',
      placeholder: 'unesite naziv ulice',
      disabled: disableAllFields,
      validators: [
        {
          message: 'naziv ulice je obavezan',
          name: VALIDATOR_NAMES.REQUIRED,
          validatorFn: Validators.required,
        },
      ],
    });

    let rowBroj = new FormRow();
    form.addChild(rowBroj);
    rowBroj.addChildInterface(<FormFieldInputTextInterface>{
      type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
      formFieldInputType: FORM_FIELD_INPUT_TYPE.TEXT,
      controlName: 'broj',
      labelName: 'Broj ulice',
      defaultValue: this._entitet.getBroj(),
      appearance: 'outline',
      placeholder: 'unesite broj ulice',
      disabled: disableAllFields,
      validators: [
        {
          message: 'broj ulice je obavezan',
          name: VALIDATOR_NAMES.REQUIRED,
          validatorFn: Validators.required,
        },
      ],
    });

    let rowTipAdrese = new FormRow();
    form.addChild(rowTipAdrese);
    rowTipAdrese.addChildInterface(<SelectInterface>{
      type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
      controlName: 'tip',
      labelName: 'Tip adrese',
      defaultValue: this._entitet.getTip(),
      optionValues: [
        { value: TipAdrese.PRIMARNA, textToShow: TipAdrese.PRIMARNA },
        {
          value: TipAdrese.SEKUNDARNA,
          textToShow: TipAdrese.SEKUNDARNA,
        },
        {
          value: TipAdrese.OSTALO,
          textToShow: TipAdrese.OSTALO,
        },
      ],
      disabled: disableAllFields,
      validators: [
        {
          message: 'Tip adrese je obavezan',
          name: VALIDATOR_NAMES.REQUIRED,
          validatorFn: Validators.required,
        },
      ],
    });
    let gradService = csfm.getCrudServiceForModel<Grad, number>(
      GRAD_SERVICE_FOR_MODEL_INTERFACE
    );

    gradService
      .findAll()
      .pipe(take(1))
      .subscribe(
        (entiteti: Grad[]) => {
          for (let index = 0; index < entiteti.length; index++) {
            if (
              entiteti[index].getId().toString() ===
              this._entitet.getGrad()?.getId().toString()
            ) {
              this._entitet.setGrad(entiteti[index]);
              break;
            }
          }

          if (isFormForCreation) {
            // entiteti = entiteti.filter((r) => !r.getUniverzitet());
          } else if (isFormForUpdate) {
            // entiteti = entiteti.filter((r) => !r.getUniverzitet());
            // entiteti.push(this._entitet.getGrad());
          } else if (isFormForDetails) {
            if (
              this._entitet.getGrad() &&
              !entiteti.find(
                (e) =>
                  e.getId().toString() ===
                  this._entitet.getGrad().getId().toString()
              )
            )
              entiteti.push(this._entitet.getGrad());
          }

          // if (!(entiteti.length > 0)) {
          //   this._formSubject.error(
          //     new Error(
          //       'Sve studentske sluzbe vec pripadaju nekim univerzitetima'
          //     )
          //   );
          //   return;
          // }

          let rowEntitet = new FormRow();
          form.addChild(rowEntitet);
          rowEntitet.addChildInterface(<SelectInterface>{
            type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
            controlName: 'grad',
            labelName: 'Grad',
            defaultValue: this._entitet.getGrad(),
            optionValues: entiteti.map((d) => {
              return {
                value: d,
                textToShow: 'id: ' + d.getId().toString() + ', ' + d.getNaziv(),
              };
            }),
            disabled: disableAllFields,
          });
          this._formSubject.next(form);
          // --------------------------------------
          let odeljenjeService = csfm.getCrudServiceForModel<Odeljenje, number>(
            ODELJENJE_SERVICE_FOR_MODEL_INTERFACE
          );

          odeljenjeService
            .findAll()
            .pipe(take(1))
            .subscribe(
              (entiteti: Odeljenje[]) => {
                for (let index = 0; index < entiteti.length; index++) {
                  if (
                    entiteti[index].getId().toString() ===
                    this._entitet.getOdeljenje()?.getId().toString()
                  ) {
                    this._entitet.setOdeljenje(entiteti[index]);
                    break;
                  }
                }

                if (isFormForCreation) {
                  entiteti = entiteti.filter((r) => !r.getAdresa());
                } else if (isFormForUpdate) {
                  entiteti = entiteti.filter((r) => !r.getAdresa());
                  entiteti.push(this._entitet.getOdeljenje());
                } else if (isFormForDetails) {
                  if (
                    this._entitet.getOdeljenje() &&
                    !entiteti.find(
                      (e) =>
                        e.getId().toString() ===
                        this._entitet.getOdeljenje().getId().toString()
                    )
                  )
                    entiteti.push(this._entitet.getOdeljenje());
                }

                if (!(entiteti.length > 0)) {
                  // this._formSubject.error(
                  //   new Error('Sve odeljenja vec imaju neke adrese')
                  // );
                  // return;
                } else {
                  let rowEntitet = new FormRow();
                  form.addChild(rowEntitet);
                  rowEntitet.addChildInterface(<SelectInterface>{
                    type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
                    controlName: 'odeljenje',
                    labelName: 'Odeljenje',
                    defaultValue: this._entitet.getOdeljenje(),
                    optionValues: entiteti.map((d) => {
                      return {
                        value: d,
                        textToShow: d.getId().toString(),
                      };
                    }),
                    disabled: disableAllFields,
                  });
                }

                // --------------------------------------
                let univerzitetService = csfm.getCrudServiceForModel<
                  Univerzitet,
                  number
                >(UNIVERZITET_SERVICE_FOR_MODEL_INTERFACE);

                univerzitetService
                  .findAll()
                  .pipe(take(1))
                  .subscribe(
                    (entiteti: Univerzitet[]) => {
                      for (let index = 0; index < entiteti.length; index++) {
                        if (
                          entiteti[index].getId().toString() ===
                          this._entitet.getUniverzitet()?.getId().toString()
                        ) {
                          this._entitet.setUniverzitet(entiteti[index]);
                          break;
                        }
                      }

                      if (isFormForCreation) {
                        // entiteti = entiteti.filter((r) => !r.getUniverzitet());
                      } else if (isFormForUpdate) {
                        // entiteti = entiteti.filter((r) => !r.getUniverzitet());
                        // entiteti.push(this._entitet.getUniverzitet());
                      } else if (isFormForDetails) {
                        if (
                          this._entitet.getUniverzitet() &&
                          !entiteti.find(
                            (e) =>
                              e.getId().toString() ===
                              this._entitet.getUniverzitet().getId().toString()
                          )
                        )
                          entiteti.push(this._entitet.getUniverzitet());
                      }

                      // if (!(entiteti.length > 0)) {
                      //   this._formSubject.error(
                      //     new Error(
                      //       'Sve studentske sluzbe vec pripadaju nekim univerzitetima'
                      //     )
                      //   );
                      //   return;
                      // } else {
                      let rowEntitet = new FormRow();
                      form.addChild(rowEntitet);
                      rowEntitet.addChildInterface(<SelectInterface>{
                        type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
                        controlName: 'univerzitet',
                        labelName: 'Univerzitet',
                        defaultValue: this._entitet.getUniverzitet(),
                        optionValues: entiteti.map((d) => {
                          return {
                            value: d,
                            textToShow:
                              'id: ' +
                              d.getId().toString() +
                              ', ' +
                              d.getNaziv(),
                          };
                        }),
                        disabled: disableAllFields,
                      });
                      // }

                      // --------------------------------------
                      let registrovaniKorisnikService = csfm.getCrudServiceForModel<
                        RegistrovaniKorisnik,
                        number
                      >(REGISTROVANI_KORISNIK_SERVICE_FOR_MODEL_INTERFACE);

                      registrovaniKorisnikService
                        .findAll()
                        .pipe(take(1))
                        .subscribe(
                          (entiteti: RegistrovaniKorisnik[]) => {
                            for (
                              let index = 0;
                              index < entiteti.length;
                              index++
                            ) {
                              if (
                                entiteti[index].getId().toString() ===
                                this._entitet
                                  .getRegistrovaniKorisnik()
                                  ?.getId()
                                  .toString()
                              ) {
                                this._entitet.setRegistrovaniKorisnik(
                                  entiteti[index]
                                );
                                break;
                              }
                            }

                            if (isFormForCreation) {
                              // entiteti = entiteti.filter(
                              //   (r) => !r.getUniverzitet()
                              // );
                            } else if (isFormForUpdate) {
                              // entiteti = entiteti.filter(
                              //   (r) => !r.getUniverzitet()
                              // );
                              // entiteti.push(
                              //   this._entitet.getRegistrovaniKorisnik()
                              // );
                            } else if (isFormForDetails) {
                              if (
                                this._entitet.getRegistrovaniKorisnik() &&
                                !entiteti.find(
                                  (e) =>
                                    e.getId().toString() ===
                                    this._entitet
                                      .getRegistrovaniKorisnik()
                                      .getId()
                                      .toString()
                                )
                              )
                                entiteti.push(
                                  this._entitet.getRegistrovaniKorisnik()
                                );
                            }

                            // if (!(entiteti.length > 0)) {
                            //   this._formSubject.error(
                            //     new Error(
                            //       'Sve studentske sluzbe vec pripadaju nekim univerzitetima'
                            //     )
                            //   );
                            //   return;
                            // } else {
                            let rowEntitet = new FormRow();
                            form.addChild(rowEntitet);
                            rowEntitet.addChildInterface(<SelectInterface>{
                              type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
                              controlName: 'registrovaniKorisnik',
                              labelName: 'Registrovani korisnik',
                              defaultValue: this._entitet.getRegistrovaniKorisnik(),
                              optionValues: entiteti.map((d) => {
                                return {
                                  value: d,
                                  textToShow: d.getId().toString(),
                                };
                              }),
                              disabled: disableAllFields,
                            });
                            // }

                            this._formSubject.next(form);
                            this._formSubject.complete();
                          },
                          (err: HttpErrorResponse) => {
                            this._formSubject.error(err);
                          }
                        );
                      this._formSubject.next(form);
                      this._formSubject.complete();
                      // ------------------------------------
                    },
                    (err: HttpErrorResponse) => {
                      this._formSubject.error(err);
                    }
                  );
                // ------------------------------------
              },
              (err: HttpErrorResponse) => {
                this._formSubject.error(err);
              }
            );
          // ------------------------------------
        },
        (err: HttpErrorResponse) => {
          this._formSubject.error(err);
        }
      );
  };

  public save = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    let adresaService = csfm.getCrudServiceForModel<Adresa, number>(
      ADRESA_SERVICE_FOR_MODEL_INTERFACE
    );
    adresaService
      .save(entitetDict)
      .pipe(take(1))
      .subscribe(
        (entitet) => {},
        (err: HttpErrorResponse) => {
          subject.error(err);
        },
        () => {
          subject.complete();
        }
      );

    return subject;
  };

  public update = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    let adresaService = csfm.getCrudServiceForModel<Adresa, number>(
      ADRESA_SERVICE_FOR_MODEL_INTERFACE
    );
    adresaService
      .update(entitetDict)
      .pipe(take(1))
      .subscribe(
        (entitet) => {},
        (err: HttpErrorResponse) => {
          subject.error(err);
        },
        () => {
          subject.complete();
        }
      );

    return subject;
  };

  public getHeaderValue = (): HeaderValue => {
    return {
      headerItems: [
        { context: 'id' },
        { context: 'stanje modela' },
        { context: 'ulica' },
        { context: 'broj' },
        { context: 'grad' },
        { context: 'tip adrese' },
        { context: 'odeljenje' },
        { context: 'univerzitet' },
        { context: 'registrovani korisnik' },
        { context: '' },
        { context: '' },
        { context: '' },
      ],
    };
  };

  public addRowToTable = (
    table: Table,
    detaljiFn: Function = () => {},
    izmeniFn: Function = () => {},
    obrisiFn: Function = () => {}
  ): void => {
    try {
      table.addChildValue({
        data: this._entitet,
        rowItems: [
          {
            context: this._entitet.getId(),
          },
          {
            context: this._entitet.getStanje(),
          },
          {
            context: this._entitet.getUlica(),
          },
          {
            context: this._entitet.getBroj(),
          },
          {
            context: this._entitet.getGrad()
              ? this._entitet.getGrad().getNaziv()
              : '/',
          },
          {
            context: this._entitet.getTip(),
          },
          {
            context: this._entitet.getOdeljenje()
              ? this._entitet.getOdeljenje().getId().toString()
              : '/',
          },
          {
            context: this._entitet.getUniverzitet()
              ? this._entitet.getUniverzitet().getNaziv()
              : '/',
          },
          {
            context: this._entitet.getRegistrovaniKorisnik()
              ? 'id: ' +
                this._entitet.getRegistrovaniKorisnik().getId() +
                ', ' +
                this._entitet.getRegistrovaniKorisnik().getIme() +
                ' ' +
                this._entitet.getRegistrovaniKorisnik().getPrezime()
              : '/',
          },
          {
            context: 'detalji',
            button: { function: detaljiFn },
          },
          {
            context: 'izmeni',
            button: { function: izmeniFn },
          },
          {
            context:
              this._entitet.getStanje() !== StanjeModela.OBRISAN
                ? 'obrisi'
                : '',
            button:
              this._entitet.getStanje() !== StanjeModela.OBRISAN
                ? { function: obrisiFn }
                : undefined,
          },
        ],
      });
    } catch (error) {
      console.log(error);
    }
  };
}
