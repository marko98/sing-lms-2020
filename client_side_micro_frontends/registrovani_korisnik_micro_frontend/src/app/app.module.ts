import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AssetUrlPipe } from './asset-url-pipe.pipe';
import { AuthInterceptor } from './auth.interceptor';
import { HomeComponent } from './home/home.component';
import { IstorijaStudiranjaComponent } from './istorija-studiranja/istorija-studiranja.component';
import { LoginComponent } from './login/login.component';
import { MenuComponent } from './menu/menu.component';
import { ObavestenjaComponent } from './obavestenja/obavestenja.component';
import { PredmetiNastavnikComponent } from './predmeti-nastavnik/predmeti-nastavnik.component';
import { PredmetiComponent } from './predmeti/predmeti.component';
import { PrijavaIspitaComponent } from './prijava-ispita/prijava-ispita.component';
import { ProfilComponent } from './profil/profil.component';
import { StudentiComponent } from './studenti/studenti.component';
import { UredjivanjeSilabusaComponent } from './predmeti-nastavnik/uredjivanje-silabusa/uredjivanje-silabusa.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    MenuComponent,
    ProfilComponent,
    PredmetiComponent,
    ObavestenjaComponent,
    IstorijaStudiranjaComponent,
    PrijavaIspitaComponent,
    AssetUrlPipe,
    PredmetiNastavnikComponent,
    StudentiComponent,
    UredjivanjeSilabusaComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatIconModule,
    FormsModule,
    HttpClientModule,
    MatListModule,
    FlexLayoutModule,
    MatToolbarModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
