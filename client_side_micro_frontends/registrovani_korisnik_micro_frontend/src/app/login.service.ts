import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, of } from 'rxjs';
import { catchError, filter, switchMap, take, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  private token = null;
  private roles = new BehaviorSubject<any>(null);
  public studentID = new BehaviorSubject<any>(null);
  public nastavnikID = new BehaviorSubject<any>(null);
  public korisnik = '';

  constructor(private http: HttpClient, private router: Router) {
    if (localStorage && localStorage.getItem('token')) {
      this.token = localStorage.getItem('token');
      const role = JSON.parse(atob(this.token.split('.')[1]))['role'];
      this.roles.next(role.map((r) => r.authority));
      // console.log("Roles ", role);
    }
  }

  login(user) {
    // console.log("LoginService#login - entered");
    return this.http
      .post(
        'http://localhost:8080/api/korisnik-microservice/registrovani_korisnik/login',
        user
      )
      .pipe(
        take(1),
        tap((response) => {
          // console.log("Response: ", response);
          // console.log("response is ", JSON.stringify(response));
          this.token = response['token'];
          const userToken = JSON.parse(atob(response['token'].split('.')[1]));
          const role = JSON.parse(atob(this.token.split('.')[1]))['role'];

          this.roles.next(role.map((r) => r.authority));
          // console.log(this.user.value);
          // console.log(this.token);
          localStorage.setItem('token', response['token']);
          this.token = response['token'];
        }),
        catchError((error) => {
          console.log('Error response');
          console.log(error);
          throw `Failed to login: ${error}`;
        })
      );
  }

  validateRoles(roles) {
    return this.roles.pipe(
      filter((u) => !!u),
      switchMap((u) => {
        if (this.roles.value) {
          let matchedRoles = roles.filter((r) => {
            // console.log("R ", r);
            return this.roles.value.find((userRole) => userRole === r);
          });
          // console.log("UserRoles: ", userRoles);
          // console.log("MatchedRoles: ", matchedRoles);
          if (matchedRoles.length > 0) {
            return of(true);
          }
        }
        return of(false);
      })
    );
  }

  hasSession() {
    return !!this.token;
  }

  getToken() {
    if (!this.token) {
      return '';
    }
    return this.token;
  }

  logout() {
    localStorage.clear();
    this.roles.next(null);
    this.studentID = new BehaviorSubject<any>(null);
    this.nastavnikID = new BehaviorSubject<any>(null);
    this.router.navigate(['mf_3/login']);
  }

  setStudentID(id) {
    // console.log("in");
    this.studentID.next(id);
    // console.log("ID studenta: ", this.studentID);
  }
  getStudentID() {
    // console.log(this.studentID.value);
    return this.studentID;
  }
  setNastavnikID(id) {
    this.nastavnikID.next(id);
    // console.log("Nastavnik id: ", this.nastavnikID);
  }
  getNastavnikID() {
    return this.nastavnikID;
  }
}
