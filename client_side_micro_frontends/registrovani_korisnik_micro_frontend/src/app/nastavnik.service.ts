import { Injectable } from '@angular/core';
import { CrudService } from './crud.service';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class NastavnikService {
  private studenti = new BehaviorSubject<any[]>(null);
  constructor(private crudService: CrudService) {}

  // getAllStudenti() {
  //   this.crudService
  //     .getAll('http://localhost:8080/api/student-microservice/student')
  //     .subscribe(
  //       (studenti) => {
  //         this.setStudenti(studenti);
  //       },
  //       (err) => console.log(err)
  //     );
  // }
  // setStudenti(studenti) {
  //   this.studenti.next(studenti);
  // }
  // getStudenti() {
  //   return this.studenti;
  // }
}
