import { Component, OnInit } from '@angular/core';
import { KorisnikService } from '../korisnik.service';
import { LoginService } from '../login.service';
import { CrudService } from '../crud.service';
import { NastavnikNaRealizaciji } from '../shared/model/entity/nastavnik_na_realizaciji.model';
import { NastavnikDTO } from '../shared/model/dto/nastavnik.dto';
import { tap } from 'rxjs/operators';
import { StudentNaStudijiDTO } from '../shared/model/dto/student_na_studiji.dto';
import { BehaviorSubject } from 'rxjs';
import { ObavestenjeDTO } from '../shared/model/dto/obavestenje.dto';
import { getLocaleDateTimeFormat } from '@angular/common';

@Component({
  selector: 'app-predmeti-nastavnik',
  templateUrl: './predmeti-nastavnik.component.html',
  styleUrls: ['./predmeti-nastavnik.component.css'],
})
export class PredmetiNastavnikComponent implements OnInit {
  private nastavnikID;
  private studenti;
  private realizacija = [];
  private nastavnik: NastavnikDTO;
  public aktivniPredmet = null;
  public aktivniPredmetObavestenje = null;
  public predmeti = [];
  public obavestenje: ObavestenjeDTO = {
    id: null,
    naslov: '',
    datum: '',
    sadrzaj: '',
    godineStudijaObavestenjeDTO: null,
    fajloviDTO: null,
    version: 0,
    stanje: null,
  };
  constructor(
    private loginService: LoginService,
    private crudService: CrudService
  ) {}

  ngOnInit(): void {
    this.loginService
      .getNastavnikID()
      .pipe(
        tap((n) => {
          this.nastavnikID = n;
          this.getNastavnik(this.nastavnikID);
        })
      )
      .subscribe();
    // console.log('Nastavnik id:', this.nastavnikID);
  }
  getNastavnik(id) {
    this.crudService
      .getOne(
        'http://localhost:8080/api/nastavnik-microservice/nastavnik/' + id
      )
      .subscribe(
        (nastavnik) => {
          this.nastavnik = nastavnik;
          // console.log(this.nastavnik);
          this.getRealizacija();
          // console.log('Predmeti', this.predmeti);
        },
        (err) => console.log(err)
      );
  }

  getRealizacija() {
    for (let predmet of this.nastavnik.nastavnikNaRealizacijamaDTO) {
      if (predmet.stanje == 'AKTIVAN') {
        this.crudService
          .getOne(
            'http://localhost:8080/api/nastavnik-microservice/nastavnik_na_realizaciji/' +
              predmet.id
          )
          .subscribe(
            (predmet) => {
              this.realizacija.push(predmet);
              this.getPredmet(predmet);
              // console.log('Realizacija: ', this.realizacija);
            },
            (err) => console.log(err)
          );
      }
    }
  }
  getPredmet(predmet) {
    this.crudService
      .getOne(
        'http://localhost:8080/api/predmet-microservice/predmet/' +
          predmet.realizacijaPredmetaDTO.id
      )
      .subscribe(
        (predmet) => {
          this.predmeti.push(predmet);
          // console.log('Predmet je: ', this.predmeti);
        },
        (err) => console.log(err)
      );
  }
  urediSilabus(predmet) {
    this.aktivniPredmet = predmet;
  }

  sacuvajSilabus() {
    // console.log('Predmet izmena', this.aktivniPredmet.silabusDTO);
    this.crudService
      .updateOne(
        'http://localhost:8080/api/predmet-microservice/silabus/',
        this.aktivniPredmet.silabusDTO
      )
      .subscribe(
        (silabus) => {
          window.alert('Uspesno izmenjeni podaci!');
          this.aktivniPredmet = null;
        },
        (err) => console.log(err)
      );
  }

  dodajObavestenje(predmet) {
    this.aktivniPredmetObavestenje = predmet;
    this.obavestenje.naslov = '';
    this.obavestenje.sadrzaj = '';
    this.obavestenje.datum = '';
    // console.log(this.aktivniPredmetObavestenje);
  }

  sacuvajObavestenje() {
    this.obavestenje.datum = new Date().toISOString().slice(0, 19);
    this.crudService
      .createOne(
        'http://localhost:8080/api/obavestenje-microservice/obavestenje',
        this.obavestenje
      )
      .subscribe(
        (o) => {
          window.alert('Uspesno ste dodali novo obavestenje');
          this.aktivniPredmetObavestenje = null;
          this.obavestenje.naslov = '';
          this.obavestenje.sadrzaj = '';
          this.obavestenje.datum = '';
        },
        (err) => console.log(err)
      );
  }
}
