import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UredjivanjeSilabusaComponent } from './uredjivanje-silabusa.component';

describe('UredjivanjeSilabusaComponent', () => {
  let component: UredjivanjeSilabusaComponent;
  let fixture: ComponentFixture<UredjivanjeSilabusaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UredjivanjeSilabusaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UredjivanjeSilabusaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
