import { Component, OnInit } from '@angular/core';
import { CrudService } from '../crud.service';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-studenti',
  templateUrl: './studenti.component.html',
  styleUrls: ['./studenti.component.css'],
})
export class StudentiComponent implements OnInit {
  public studenti;
  public studentiPrikaz = [];
  public studentiDobavljanje = [];
  public studentiSliced = [];
  private temparray;
  public name: string;
  public position: number;
  public weight: number;
  public symbol: string;

  constructor(public crudService: CrudService) {}

  ngOnInit(): void {
    this.getAllStudenti();
    // this.dataSource = new MatTableDataSource(this.studenti);
    // console.log(this.studentiSliced);
  }
  getAllStudenti() {
    this.crudService
      .getAll('http://localhost:8080/api/student-microservice/student')
      .subscribe(
        (studenti) => {
          // this.studenti = studenti;
          for (let s of studenti) {
            if (s.studijeDTO) {
              this.studentiDobavljanje.push(s);
            }
          }
          this.studenti = this.studentiDobavljanje;
          this.studentiPrikaz = this.studenti;
          // console.log(studenti);
          // console.log('Prikaz ', this.studentiPrikaz);
        },
        (err) => console.log(err)
      );
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.studenti.filter = filterValue.trim().toLowerCase();
    // // console.log((this.studenti.filter = filterValue.trim().toLowerCase()));
    const pretraga = filterValue.trim().toLowerCase();
    // return this.studenti.filter((it) => {
    //   it.name.toLowerCase().includes(pretraga);
    //   return it.name.toLowerCase().includes(pretraga);
    // });
    this.studentiPrikaz = [];
    for (let s of this.studenti) {
      if (s.studijeDTO[0].brojIndeksa.includes(pretraga)) {
        this.studentiPrikaz.push(s);
      }
    }
  }
  applyFilter2(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.studenti.filter = filterValue.trim().toLowerCase();
    // // console.log((this.studenti.filter = filterValue.trim().toLowerCase()));
    const pretraga = filterValue.trim().toLowerCase();
    // return this.studenti.filter((it) => {
    //   it.name.toLowerCase().includes(pretraga);
    //   return it.name.toLowerCase().includes(pretraga);
    // });
    console.log(pretraga);
    this.studentiPrikaz = [];
    for (let s of this.studenti) {
      // console.log();
      if (
        s.registrovaniKorisnikDTO.ime.toLowerCase().includes(pretraga) ||
        s.registrovaniKorisnikDTO.prezime.toLowerCase().includes(pretraga)
      ) {
        this.studentiPrikaz.push(s);
      }
    }
  }
}
