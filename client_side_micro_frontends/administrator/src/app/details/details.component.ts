import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { Formable } from '../shared/model/interface/formable.interface';
import { Form } from 'my-angular-form';
import { CrudServiceForModel } from '../shared/service/crud.service-for-model';
import { UIService } from '../shared/service/ui.service';
import { take } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { Location } from '@angular/common';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css'],
})
export class DetailsComponent implements OnInit, OnDestroy, OnChanges {
  @Input() entitet: Formable;
  @Output() onDetailsFinished: EventEmitter<void> = new EventEmitter();

  public form: Form;
  public loading: boolean = true;

  private _formSubscription: Subscription;

  constructor(
    private _csfm: CrudServiceForModel,
    private _uiService: UIService
  ) {}

  public onCancel = (): void => {
    this.onDetailsFinished.next();
  };

  private _onInitForm = (): void => {
    // console.log(this.entitet);
    this._formSubscription = this.entitet.getFormSubject().subscribe(
      (form) => {
        this.form = form;
      },
      (err: HttpErrorResponse) => {
        this._uiService.showSnackbar(err.message, null, 1000);
        this.onCancel();
      },
      () => {
        this.loading = false;
      }
    );
    this.entitet.getForm(this._csfm, false, true, false, true);
  };

  ngOnInit(): void {
    console.log('DetailsComponent init');
  }

  ngOnChanges(changes: SimpleChanges): void {
    // console.log(changes);
    this.loading = true;
    this.form = undefined;
    try {
      this._onInitForm();
    } catch (error) {
      this._uiService.showSnackbar(
        'bice implementirano u verziji: 2.0',
        null,
        1000
      );
      this.onCancel();
    }
  }

  ngOnDestroy(): void {
    if (this._formSubscription) this._formSubscription.unsubscribe();
    console.log('DetailsComponent destroyed');
  }
}
