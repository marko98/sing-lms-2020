import {
  Component,
  OnInit,
  OnDestroy,
  Output,
  EventEmitter,
} from '@angular/core';
import { Student } from 'src/app/shared/model/entity/student.model';
import { BehaviorSubject, Subscription } from 'rxjs';
import { Table } from 'my-angular-table';
import { StudentService } from 'src/app/shared/service/entity/student_service.service';
import { RegistrovaniKorisnik } from 'src/app/shared/model/entity/registrovani_korisnik.model';
import { RegistrovaniKorisnikService } from 'src/app/shared/service/entity/registrovani_korisnik_service.service';
import { take } from 'rxjs/operators';
import { StudentNaStudiji } from 'src/app/shared/model/entity/student_na_studiji.model';
import { StudentNaStudijiService } from 'src/app/shared/service/entity/student_na_studiji_service.service';
import { StudijskiProgramService } from 'src/app/shared/service/entity/studijski_program_service.service';
import { GodinaStudijaService } from 'src/app/shared/service/entity/godina_studija_service.service';
import { GodinaStudija } from 'src/app/shared/model/entity/godina_studija.model';
import { StudijskiProgram } from 'src/app/shared/model/entity/studijski_program.model';
import { PohadjanjePredmetaService } from 'src/app/shared/service/entity/pohadjanje_predmeta_service.service';
import { PohadjanjePredmeta } from 'src/app/shared/model/entity/pohadjanje_predmeta.model';
import { RealizacijaPredmetaService } from 'src/app/shared/service/entity/realizacija_predmeta_service.service';
import { RealizacijaPredmeta } from 'src/app/shared/model/entity/realizacija_predmeta.model';
import { PdfService } from 'src/app/shared/service/pdf.service';
import { XmlService } from 'src/app/shared/service/xml.service';

@Component({
  selector: 'app-studenti',
  templateUrl: './studenti.component.html',
  styleUrls: ['./studenti.component.css'],
})
export class StudentiComponent implements OnInit, OnDestroy {
  @Output() onDone: EventEmitter<void> = new EventEmitter();

  public studenti: Student[];
  public table: Table;
  public showTable: boolean = false;
  public pdfXmlFileName: string = 'studenti';

  private _waitingRequests: BehaviorSubject<number> = new BehaviorSubject(0);
  private _subscription: Subscription;
  private _studentSubscription: Subscription;

  constructor(
    private _studentService: StudentService,
    private _studentNaStudijiService: StudentNaStudijiService,
    private _godinaStudijaService: GodinaStudijaService,
    private _studijskiProgramService: StudijskiProgramService,
    private _pohadjanjePredmetaService: PohadjanjePredmetaService,
    private _realizacijaPredmetaService: RealizacijaPredmetaService,
    private _pdfService: PdfService,
    private _xmlService: XmlService
  ) {}

  public onExportToXML = (): void => {
    this._xmlService.onExportXml(
      this.pdfXmlFileName,
      this.studenti,
      'studenti'
    );
  };

  public onExportToXMLContextMenu = (studenti: Student[]): void => {
    this._xmlService.onExportXml(this.pdfXmlFileName, studenti, 'studenti');
  };

  public onExportToPDF = (): void => {
    this._pdfService.exportToPDF(
      this.pdfXmlFileName,
      document.querySelector('#toPDFStudenti')
    );
  };

  private _onInitTable = (waitingRequests: number): void => {
    if (!(waitingRequests > 0) && this.studenti) {
      this.table = new Table({
        header: {
          headerItems: [
            { context: 'id' },
            { context: 'ime' },
            { context: 'prezime' },
            { context: 'fakultet' },
            { context: 'studijski program' },
            { context: 'semestar' },
            { context: 'predmet' },
            { context: 'ocena' },
          ],
        },
        contextMenu: {
          contextMenuItems: [
            {
              context: 'eksport u xml',
              function: this.onExportToXMLContextMenu,
              imgSrc: 'https://img.icons8.com/dusk/2x/xml-file.png',
            },
          ],
        },
      });

      this._fillTable();
    } else {
      this.showTable = false;
      this.table = undefined;
    }
  };

  private _fillTable = (): void => {
    if (this.table.getChildren().length > 0) this.table.removeChildren();

    this.studenti.forEach((student) => {
      student.getStudije().forEach((sns) => {
        sns.getPohadjanjaPredmeta().forEach((pp) => {
          this.table.addChildValue({
            data: student,
            rowItems: [
              { context: student.getId().toString() },
              { context: student.getRegistrovaniKorisnik().getIme() },
              { context: student.getRegistrovaniKorisnik().getPrezime() },
              {
                context: sns
                  .getGodinaStudija()
                  ?.getStudijskiProgram()
                  ?.getFakultet()
                  ?.getNaziv()
                  ? sns
                      .getGodinaStudija()
                      .getStudijskiProgram()
                      .getFakultet()
                      .getNaziv()
                  : '/',
              },
              {
                context: sns
                  .getGodinaStudija()
                  ?.getStudijskiProgram()
                  ?.getNaziv()
                  ? sns.getGodinaStudija().getStudijskiProgram().getNaziv()
                  : '/',
              },
              {
                context: sns.getGodinaStudija().getSemestar(),
              },
              {
                context: pp.getRealizacijaPredmeta()?.getPredmet()?.getNaziv()
                  ? pp.getRealizacijaPredmeta()?.getPredmet()?.getNaziv()
                  : '/',
              },
              {
                context: pp.getKonacnaOcena() ? pp.getKonacnaOcena() : '/',
              },
            ],
          });
        });
      });
    });

    this.showTable = true;
  };

  public onExportDone = (): void => {
    this.onDone.next();
  };

  ngOnInit(): void {
    this._subscription = this._waitingRequests.subscribe(this._onInitTable);

    this._waitingRequests.next(this._waitingRequests.getValue() + 1);
    this._studentSubscription = this._studentService.getEntiteti().subscribe(
      (studenti: Student[]) => {
        this.studenti = studenti;

        this.studenti.forEach((student) => {
          for (
            let godinaStudijaIndex = 0;
            godinaStudijaIndex < student.getStudije().length;
            godinaStudijaIndex++
          ) {
            this._waitingRequests.next(this._waitingRequests.getValue() + 1);
            this._studentNaStudijiService
              .findOne({ id: student.getStudije()[godinaStudijaIndex].getId() })
              .pipe(take(1))
              .subscribe(
                (sns: StudentNaStudiji) => {
                  student.getStudije()[godinaStudijaIndex] = sns;

                  for (
                    let pohadjanjePredmetaIndex = 0;
                    pohadjanjePredmetaIndex <
                    student
                      .getStudije()
                      [godinaStudijaIndex].getPohadjanjaPredmeta().length;
                    pohadjanjePredmetaIndex++
                  ) {
                    this._waitingRequests.next(
                      this._waitingRequests.getValue() + 1
                    );

                    this._pohadjanjePredmetaService
                      .findOne({
                        id: student
                          .getStudije()
                          [godinaStudijaIndex].getPohadjanjaPredmeta()
                          [pohadjanjePredmetaIndex].getId(),
                      })
                      .pipe(take(1))
                      .subscribe(
                        (pp: PohadjanjePredmeta) => {
                          student
                            .getStudije()
                            [godinaStudijaIndex].getPohadjanjaPredmeta()[
                            pohadjanjePredmetaIndex
                          ] = pp;

                          this._waitingRequests.next(
                            this._waitingRequests.getValue() + 1
                          );
                          this._realizacijaPredmetaService
                            .findOne({
                              id: pp.getRealizacijaPredmeta().getId(),
                            })
                            .pipe(take(1))
                            .subscribe(
                              (rp: RealizacijaPredmeta) => {
                                student
                                  .getStudije()
                                  [godinaStudijaIndex].getPohadjanjaPredmeta()
                                  [
                                    pohadjanjePredmetaIndex
                                  ].setRealizacijaPredmeta(rp);
                                this._waitingRequests.next(
                                  this._waitingRequests.getValue() - 1
                                );
                              },
                              (err) => {
                                this._waitingRequests.next(
                                  this._waitingRequests.getValue() - 1
                                );
                              }
                            );

                          this._waitingRequests.next(
                            this._waitingRequests.getValue() - 1
                          );
                        },
                        (err) => {
                          this._waitingRequests.next(
                            this._waitingRequests.getValue() - 1
                          );
                        }
                      );
                  }

                  this._waitingRequests.next(
                    this._waitingRequests.getValue() + 1
                  );
                  this._godinaStudijaService
                    .findOne({
                      id: student
                        .getStudije()
                        [godinaStudijaIndex].getGodinaStudija()
                        .getId(),
                    })
                    .pipe(take(1))
                    .subscribe(
                      (gs: GodinaStudija) => {
                        student
                          .getStudije()
                          [godinaStudijaIndex].setGodinaStudija(gs);

                        this._waitingRequests.next(
                          this._waitingRequests.getValue() + 1
                        );
                        this._studijskiProgramService
                          .findOne({
                            id: student
                              .getStudije()
                              [godinaStudijaIndex].getGodinaStudija()
                              .getStudijskiProgram()
                              .getId(),
                          })
                          .pipe(take(1))
                          .subscribe(
                            (sp: StudijskiProgram) => {
                              student
                                .getStudije()
                                [godinaStudijaIndex].getGodinaStudija()
                                .setStudijskiProgram(sp);
                              this._waitingRequests.next(
                                this._waitingRequests.getValue() - 1
                              );
                            },
                            (err) => {
                              this._waitingRequests.next(
                                this._waitingRequests.getValue() - 1
                              );
                            }
                          );

                        this._waitingRequests.next(
                          this._waitingRequests.getValue() - 1
                        );
                      },
                      (err) => {
                        this._waitingRequests.next(
                          this._waitingRequests.getValue() - 1
                        );
                      }
                    );

                  this._waitingRequests.next(
                    this._waitingRequests.getValue() - 1
                  );
                },
                (err) => {
                  this._waitingRequests.next(
                    this._waitingRequests.getValue() - 1
                  );
                }
              );
          }
        });

        this._waitingRequests.next(this._waitingRequests.getValue() - 1);
      },
      (err) => {
        this._waitingRequests.next(this._waitingRequests.getValue() - 1);
      }
    );

    console.log('StudentiComponent view init');
  }

  ngOnDestroy(): void {
    if (this._subscription) this._subscription.unsubscribe();
    if (this._studentSubscription) this._studentSubscription.unsubscribe();
    console.log('StudentiComponent destroyed');
  }
}
