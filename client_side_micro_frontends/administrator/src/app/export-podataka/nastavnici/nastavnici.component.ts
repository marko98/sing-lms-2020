import {
  Component,
  OnInit,
  OnDestroy,
  Output,
  EventEmitter,
} from '@angular/core';
import { Nastavnik } from 'src/app/shared/model/entity/nastavnik.model';
import { Table } from 'my-angular-table';
import { NastavnikService } from 'src/app/shared/service/entity/nastavnik_service.service';
import { BehaviorSubject, Subscription } from 'rxjs';
import { TitulaService } from 'src/app/shared/service/entity/titula_service.service';
import { take } from 'rxjs/operators';
import { Titula } from 'src/app/shared/model/entity/titula.model';
import { PdfService } from 'src/app/shared/service/pdf.service';
import { XmlService } from 'src/app/shared/service/xml.service';

@Component({
  selector: 'app-nastavnici',
  templateUrl: './nastavnici.component.html',
  styleUrls: ['./nastavnici.component.css'],
})
export class NastavniciComponent implements OnInit, OnDestroy {
  @Output() onDone: EventEmitter<void> = new EventEmitter();

  public nastavnici: Nastavnik[];
  public table: Table;
  public showTable: boolean = false;
  public pdfXmlFileName: string = 'nastavnici';

  private _waitingRequests: BehaviorSubject<number> = new BehaviorSubject(0);
  private _subscription: Subscription;
  private _nastavniciSubscription: Subscription;

  constructor(
    private _nastavnikService: NastavnikService,
    private _titulaService: TitulaService,
    private _pdfService: PdfService,
    private _xmlService: XmlService
  ) {}

  public onExportToXML = (): void => {
    this._xmlService.onExportXml(
      this.pdfXmlFileName,
      this.nastavnici,
      'nastavnici'
    );
  };

  public onExportToXMLContextMenu = (nastavnici: Nastavnik[]): void => {
    this._xmlService.onExportXml(this.pdfXmlFileName, nastavnici, 'nastavnici');
  };

  public onExportToPDF = (): void => {
    this._pdfService.exportToPDF(
      this.pdfXmlFileName,
      document.querySelector('#toPDFNastavnici')
    );
  };

  private _onInitTable = (waitingRequests: number): void => {
    if (!(waitingRequests > 0) && this.nastavnici) {
      this.table = new Table({
        header: {
          headerItems: [
            {
              context: 'id',
            },
            {
              context: 'ime',
            },
            {
              context: 'prezime',
            },
            { context: 'naucna oblast' },
            { context: 'titula' },
          ],
        },
        contextMenu: {
          contextMenuItems: [
            {
              context: 'eksport u xml',
              function: this.onExportToXMLContextMenu,
              imgSrc: 'https://img.icons8.com/dusk/2x/xml-file.png',
            },
          ],
        },
      });
      this._fillTable();
    } else {
      this.showTable = false;
    }
  };

  private _fillTable = (): void => {
    if (this.table.getChildren().length > 0) this.table.removeChildren();

    this.nastavnici.forEach((nastavnik) => {
      nastavnik.getTitule().forEach((titula) => {
        this.table.addChildValue({
          data: nastavnik,
          rowItems: [
            {
              context: nastavnik.getId(),
            },
            {
              context: nastavnik.getRegistrovaniKorisnik()
                ? nastavnik.getRegistrovaniKorisnik().getIme()
                : '',
            },
            {
              context: nastavnik.getRegistrovaniKorisnik()?.getPrezime()
                ? nastavnik.getRegistrovaniKorisnik().getPrezime()
                : '',
            },
            {
              context: titula.getNaucnaOblast()?.getNaziv()
                ? titula.getNaucnaOblast().getNaziv()
                : '',
            },
            {
              context: titula.getTip(),
            },
          ],
        });
      });
    });
    this.showTable = true;
  };

  public onExportDone = (): void => {
    this.onDone.next();
  };

  ngOnInit(): void {
    this._subscription = this._waitingRequests.subscribe(this._onInitTable);

    this._waitingRequests.next(this._waitingRequests.getValue() + 1);
    this._nastavniciSubscription = this._nastavnikService
      .getEntiteti()
      .subscribe(
        (nastavnici: Nastavnik[]) => {
          this.nastavnici = nastavnici;

          this.nastavnici.forEach((nastavnik) => {
            for (
              let titulaIndex = 0;
              titulaIndex < nastavnik.getTitule().length;
              titulaIndex++
            ) {
              this._waitingRequests.next(this._waitingRequests.getValue() + 1);
              this._titulaService
                .findOne({
                  id: nastavnik.getTitule()[titulaIndex].getId(),
                })
                .pipe(take(1))
                .subscribe(
                  (titula: Titula) => {
                    nastavnik.getTitule()[titulaIndex] = titula;
                    this._waitingRequests.next(
                      this._waitingRequests.getValue() - 1
                    );
                  },
                  (err) => {
                    this._waitingRequests.next(
                      this._waitingRequests.getValue() - 1
                    );
                  }
                );
            }
          });

          this._waitingRequests.next(this._waitingRequests.getValue() - 1);
        },
        (err) => {
          this._waitingRequests.next(this._waitingRequests.getValue() - 1);
        }
      );

    console.log('NastavniciComponent view init');
  }

  ngOnDestroy(): void {
    if (this._subscription) this._subscription.unsubscribe();
    if (this._nastavniciSubscription)
      this._nastavniciSubscription.unsubscribe();
    console.log('NastavniciComponent destroyed');
  }
}
