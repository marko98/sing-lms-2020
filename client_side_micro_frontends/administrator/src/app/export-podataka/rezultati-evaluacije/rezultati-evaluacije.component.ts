import {
  Component,
  OnInit,
  OnDestroy,
  Output,
  EventEmitter,
} from '@angular/core';
import { EvaluacijaZnanjaService } from 'src/app/shared/service/entity/evaluacija_znanja_service.service';
import { EvaluacijaZnanja } from 'src/app/shared/model/entity/evaluacija_znanja.model';
import { take } from 'rxjs/operators';
import { Subscription, BehaviorSubject } from 'rxjs';
import { PolaganjeService } from 'src/app/shared/service/entity/polaganje_service.service';
import { StudentNaStudijiService } from 'src/app/shared/service/entity/student_na_studiji_service.service';
import { PohadjanjePredmetaService } from 'src/app/shared/service/entity/pohadjanje_predmeta_service.service';
import { PohadjanjePredmeta } from 'src/app/shared/model/entity/pohadjanje_predmeta.model';
import { Table } from 'my-angular-table';
import { Observable } from 'src/app/shared/model/patterns/behavioural/observer/observable.model';
import { RealizacijaPredmetaService } from 'src/app/shared/service/entity/realizacija_predmeta_service.service';
import { RealizacijaPredmeta } from 'src/app/shared/model/entity/realizacija_predmeta.model';
import { PdfService } from 'src/app/shared/service/pdf.service';
import { XmlService } from 'src/app/shared/service/xml.service';

@Component({
  selector: 'app-rezultati-evaluacije',
  templateUrl: './rezultati-evaluacije.component.html',
  styleUrls: ['./rezultati-evaluacije.component.css'],
})
export class RezultatiEvaluacijeComponent implements OnInit, OnDestroy {
  @Output() onDone: EventEmitter<void> = new EventEmitter();

  public evaluacijeZnanja: EvaluacijaZnanja[];
  public table: Table;
  public showTable: boolean = false;
  public pdfXmlFileName: string = 'rezultati_evaluacije';
  public waitingOnRequests: BehaviorSubject<number> = new BehaviorSubject(0);

  private _subscription: Subscription;

  constructor(
    private _evaluacijaZnanjaService: EvaluacijaZnanjaService,
    private _polaganjeService: PolaganjeService,
    private _pohadjanjePredmetaService: PohadjanjePredmetaService,
    private _realizacijaPredmetaService: RealizacijaPredmetaService,
    private _pdfService: PdfService,
    private _xmlService: XmlService
  ) {}

  public onDoneExport = (): void => {
    this.onDone.next();
  };

  public onExportToXML = (): void => {
    this._xmlService.onExportXml(
      this.pdfXmlFileName,
      this.evaluacijeZnanja,
      'evaluacijeZnanja'
    );
  };

  public onExportToXMLContextMenu = (
    evaluacijeZnanja: EvaluacijaZnanja[]
  ): void => {
    this._xmlService.onExportXml(
      this.pdfXmlFileName,
      evaluacijeZnanja,
      'evaluacijeZnanja'
    );
  };

  public onExportToPDF = (): void => {
    this._pdfService.exportToPDF(
      this.pdfXmlFileName,
      document.querySelector('#toPDFRezultatiEvaluacije')
    );
  };

  private _initTable = (waitingOnRequests: number): void => {
    if (!(waitingOnRequests > 0) && this.evaluacijeZnanja) {
      this.table = new Table({
        header: {
          headerItems: [
            { context: 'id' },
            { context: 'datum' },
            { context: 'naziv predmeta' },
            { context: 'index' },
            { context: 'bodovi' },
          ],
        },
        contextMenu: {
          contextMenuItems: [
            {
              context: 'eksport u xml',
              function: this.onExportToXMLContextMenu,
              imgSrc: 'https://img.icons8.com/dusk/2x/xml-file.png',
            },
          ],
        },
      });

      this._fillTable();
    } else {
      this.showTable = false;
    }
  };

  private _fillTable = (): void => {
    if (this.table.getChildren().length > 0) this.table.removeChildren();

    this.evaluacijeZnanja.forEach((ez) => {
      ez.getPolaganja().forEach((polaganje) => {
        this.table.addChildValue({
          data: ez,
          rowItems: [
            {
              context: ez.getId(),
            },
            {
              context: ez.getPocetak().toLocaleDateString(),
            },
            {
              context: ez.getRealizacijaPredmeta()?.getPredmet()?.getNaziv()
                ? ez.getRealizacijaPredmeta()?.getPredmet()?.getNaziv()
                : '/',
            },
            {
              context: polaganje
                .getPohadjanjePredmeta()
                ?.getStudentNaStudiji()
                ?.getBrojIndeksa()
                ? polaganje
                    .getPohadjanjePredmeta()
                    ?.getStudentNaStudiji()
                    ?.getBrojIndeksa()
                : '/',
            },
            {
              context: polaganje.getBodovi(),
            },
          ],
        });
      });
    });

    this.showTable = true;
  };

  ngOnInit(): void {
    // this._countRequests.attach(this._initTable);
    this.waitingOnRequests.subscribe(this._initTable);

    // this._countRequests.increase();
    this.waitingOnRequests.next(this.waitingOnRequests.getValue() + 1);
    this._subscription = this._evaluacijaZnanjaService.getEntiteti().subscribe(
      (evaluacijeZnanja: EvaluacijaZnanja[]) => {
        this.waitingOnRequests.next(this.waitingOnRequests.getValue() - 1);
        this.evaluacijeZnanja = evaluacijeZnanja;
        this.evaluacijeZnanja.forEach((e) => {
          this.waitingOnRequests.next(this.waitingOnRequests.getValue() + 1);
          this._realizacijaPredmetaService
            .findOne({ id: e.getRealizacijaPredmeta().getId() })
            .pipe(take(1))
            .subscribe(
              (rp) => {
                this.waitingOnRequests.next(
                  this.waitingOnRequests.getValue() - 1
                );
                e.setRealizacijaPredmeta(<RealizacijaPredmeta>rp);
              },
              (err) => {
                this.waitingOnRequests.next(
                  this.waitingOnRequests.getValue() - 1
                );
              }
            );
        });
        this.evaluacijeZnanja.forEach((e) => {
          this.waitingOnRequests.next(this.waitingOnRequests.getValue() + 1);
          this._polaganjeService
            .findAllByEvaluacijaZnanjaId(e.getId())
            .pipe(take(1))
            .subscribe(
              (polaganja) => {
                this.waitingOnRequests.next(
                  this.waitingOnRequests.getValue() - 1
                );
                e.setPolaganja(polaganja);
                polaganja.forEach((polaganje) => {
                  this.waitingOnRequests.next(
                    this.waitingOnRequests.getValue() + 1
                  );
                  this._pohadjanjePredmetaService
                    .findOne({ id: polaganje.getPohadjanjePredmeta().getId() })
                    .pipe(take(1))
                    .subscribe(
                      (pohadjanje) => {
                        this.waitingOnRequests.next(
                          this.waitingOnRequests.getValue() - 1
                        );
                        polaganje.setPohadjanjePredmeta(
                          <PohadjanjePredmeta>pohadjanje
                        );
                      },
                      (err) => {
                        this.waitingOnRequests.next(
                          this.waitingOnRequests.getValue() - 1
                        );
                      }
                    );
                });
              },
              (err) => {
                this.waitingOnRequests.next(
                  this.waitingOnRequests.getValue() - 1
                );
              }
            );
        });
      },
      (err) => {
        this.waitingOnRequests.next(this.waitingOnRequests.getValue() - 1);
      }
    );

    console.log('RezultatiEvaluacijeComponent init');
  }

  ngOnDestroy(): void {
    if (this._subscription) this._subscription.unsubscribe();
    // if (this._countRequests) this._countRequests.dettach(this._initTable);
    console.log('RezultatiEvaluacijeComponent destroyed');
  }
}
