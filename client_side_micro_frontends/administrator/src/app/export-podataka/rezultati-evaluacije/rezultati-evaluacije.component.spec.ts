import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RezultatiEvaluacijeComponent } from './rezultati-evaluacije.component';

describe('RezultatiEvaluacijeComponent', () => {
  let component: RezultatiEvaluacijeComponent;
  let fixture: ComponentFixture<RezultatiEvaluacijeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RezultatiEvaluacijeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RezultatiEvaluacijeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
