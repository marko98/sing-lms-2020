import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-export-podataka',
  templateUrl: './export-podataka.component.html',
  styleUrls: ['./export-podataka.component.css'],
})
export class ExportPodatakaComponent implements OnInit, OnDestroy {
  public showRezultatiEvaluacije: boolean = false;
  public showStudenti: boolean = false;
  public showNastavnici: boolean = false;

  constructor() {}

  ngOnInit(): void {
    console.log('ExportPodatakaComponent init');
  }

  ngOnDestroy(): void {
    console.log('ExportPodatakaComponent destroyed');
  }
}
