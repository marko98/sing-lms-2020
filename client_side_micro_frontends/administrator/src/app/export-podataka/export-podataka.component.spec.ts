import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExportPodatakaComponent } from './export-podataka.component';

describe('ExportPodatakaComponent', () => {
  let component: ExportPodatakaComponent;
  let fixture: ComponentFixture<ExportPodatakaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExportPodatakaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExportPodatakaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
