import {
  Component,
  OnInit,
  OnDestroy,
  AfterViewInit,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { Entitet } from '../shared/model/interface/entitet.interface';
import { CrudService } from '../shared/service/crud.service';
import { Subscription } from 'rxjs';
import { Tableable } from '../shared/model/interface/tableable.interface';
import { Table } from 'my-angular-table';
import { UIService } from '../shared/service/ui.service';
import { HttpErrorResponse } from '@angular/common/http';
import { CrudServiceForModel } from '../shared/service/crud.service-for-model';
import { DRZAVA_SERVICE_FOR_MODEL_INTERFACE } from '../shared/service/entity/drzava_service.service';
import { Drzava } from '../shared/model/entity/drzava.model';
import { take, map } from 'rxjs/operators';
import { CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../shared/service/crud.service';
import { Formable } from '../shared/model/interface/formable.interface';
import {
  AuthUserModelInterface,
  AuthService,
} from '../shared/service/auth.service';

@Component({
  selector: 'app-administracija',
  templateUrl: './administracija.component.html',
  styleUrls: ['./administracija.component.css'],
})
export class AdministracijaComponent
  implements OnInit, OnDestroy, AfterViewInit, OnChanges {
  @Input() data: {
    formableClass: any;
    tableableClass: any;
    entitetClass: any;
    crudServiceForModelInterface: CRUD_SERVICE_FOR_MODEL_INTERFACE;
    crudRoles: {
      create: string[];
      read: string[];
      update: string[];
      delete: string[];
    };
  };

  public entitetZaKreiranje: Formable;
  public entitetZaIzmenu: Formable;
  public entitetZaDetalje: Formable;

  public table: Table;
  public crudService: CrudService<any, any>;

  public loading: boolean = false;

  private _subscription: Subscription;

  private _user: AuthUserModelInterface;

  constructor(
    private _authService: AuthService,
    private _uiService: UIService,
    private _csfm: CrudServiceForModel
  ) {}

  public getClassName = (): string => {
    return this.data.entitetClass.name
      .toString()
      .replace(/([A-Z])/g, ' $1')
      .trim();
  };

  public onFormSubmit = (): void => {
    this.loading = true;
  };

  public onCreationFinished = (): void => {
    this.entitetZaKreiranje = undefined;
  };

  public onUpdateFinished = (): void => {
    this.entitetZaIzmenu = undefined;
  };

  public onDetailsFinished = (): void => {
    this.entitetZaDetalje = undefined;
  };

  public onKreirajNoviEntitet = (): void => {
    if (
      this.intersection(new Set(this.data.crudRoles.create), this._user.role)
        .size > 0
    ) {
      this.entitetZaKreiranje = new this.data.formableClass(
        new this.data.entitetClass()
      );
      this.entitetZaDetalje = undefined;
      this.entitetZaIzmenu = undefined;
    } else {
      this._uiService.showSnackbar(
        'Nemate pravo na kreiranje entiteta',
        null,
        1000
      );
    }
  };

  ngOnInit(): void {
    this._authService
      .getUser()
      .pipe(take(1))
      .subscribe((user: AuthUserModelInterface) => {
        this._user = user;
      });

    console.log('AdministratorComponent init');
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.data) {
      // console.log(this.crudService);

      this.loading = true;

      if (this._subscription) this._subscription.unsubscribe();

      let crudService = this._csfm.getCrudServiceForModel<
        Entitet<any, any>,
        number
      >(this.data.crudServiceForModelInterface);
      crudService
        .findAll()
        .pipe(take(1))
        .subscribe(
          (entiteti) => {
            this.crudService = crudService;

            // console.log(entiteti);

            if (this.crudService) {
              // this.loading = true;
              this._subscription = this.crudService.getEntiteti().subscribe(
                (entiteti) => {
                  if (entiteti.length > 0) {
                    this.table = new Table({
                      header: new this.data.tableableClass(
                        entiteti[0]
                      ).getHeaderValue(),
                      contextMenu: {
                        contextMenuItems: [
                          {
                            context: 'obrisi',
                            function:
                              this.intersection(
                                new Set(this.data.crudRoles.delete),
                                this._user.role
                              ).size > 0
                                ? (entiteti: Entitet<any, any>[]) => {
                                    this.entitetZaDetalje = undefined;
                                    this.entitetZaIzmenu = undefined;
                                    this.entitetZaKreiranje = undefined;
                                    // console.log(entiteti);
                                    entiteti.forEach((e) => {
                                      if (!this.loading) this.loading = true;
                                      this.crudService.delete(e);
                                    });
                                  }
                                : () => {
                                    this._uiService.showSnackbar(
                                      'Nemate pravo na brisanje entiteta',
                                      null,
                                      1000
                                    );
                                  },
                            imgSrc:
                              'https://img.icons8.com/cotton/2x/delete-sign--v2.png',
                          },
                        ],
                      },
                    });
                    entiteti.forEach((e) => {
                      new this.data.tableableClass(e).addRowToTable(
                        this.table,
                        (entiteti: Entitet<any, any>[]) => {
                          this.entitetZaDetalje = new this.data.formableClass(
                            entiteti[0]
                          );
                          this.entitetZaIzmenu = undefined;
                          this.entitetZaKreiranje = undefined;
                        },
                        (entiteti: Entitet<any, any>[]) => {
                          if (
                            this.intersection(
                              new Set(this.data.crudRoles.update),
                              this._user.role
                            ).size > 0
                          ) {
                            this.entitetZaDetalje = undefined;
                            this.entitetZaIzmenu = new this.data.formableClass(
                              entiteti[0]
                            );
                            this.entitetZaKreiranje = undefined;
                          } else {
                            this._uiService.showSnackbar(
                              'Nemate pravo na izmenu entiteta',
                              null,
                              1000
                            );
                          }
                        },
                        (entiteti: Entitet<any, any>[]) => {
                          if (
                            this.intersection(
                              new Set(this.data.crudRoles.delete),
                              this._user.role
                            ).size > 0
                          ) {
                            this.entitetZaDetalje = undefined;
                            this.entitetZaIzmenu = undefined;
                            this.entitetZaKreiranje = undefined;
                            // console.log(entiteti);
                            entiteti.forEach((e) => {
                              if (!this.loading) this.loading = true;
                              this.crudService.delete(e);
                            });
                          } else {
                            this._uiService.showSnackbar(
                              'Nemate pravo na brisanje entiteta',
                              null,
                              1000
                            );
                          }
                        }
                      );
                    });
                  } else {
                    this.table = undefined;
                  }
                  this.loading = false;
                },
                (err: HttpErrorResponse) => {
                  this.loading = false;
                  this._uiService.showSnackbar(err.message, null, 1000);
                },
                () => {
                  this.loading = false;
                }
              );
            }
          },
          (err) => {
            this.loading = false;
            this._uiService.showSnackbar(err, null, 1000);
          }
        );
    }
  }

  private intersection = (a: Set<any>, b: Set<any>): Set<any> => {
    return new Set([...a].filter((x) => b.has(x)));
  };

  ngAfterViewInit(): void {
    console.log('AdministratorComponent view init');
  }

  ngOnDestroy(): void {
    if (this._subscription) this._subscription.unsubscribe();
    console.log('AdministratorComponent destroyed');
  }
}
