import {
  Component,
  OnInit,
  OnDestroy,
  Input,
  Output,
  EventEmitter,
  SimpleChanges,
  OnChanges,
} from '@angular/core';
import { Formable } from '../shared/model/interface/formable.interface';
import { NgForm } from '@angular/forms';
import { Form } from 'my-angular-form';
import { CrudServiceForModel } from '../shared/service/crud.service-for-model';
import { UIService } from '../shared/service/ui.service';
import { HttpErrorResponse } from '@angular/common/http';
import { take } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
})
export class CreateComponent implements OnInit, OnDestroy, OnChanges {
  @Input() entitet: Formable;
  @Output() onCreationFinished: EventEmitter<void> = new EventEmitter();
  @Output() onCreationSubmit: EventEmitter<void> = new EventEmitter();

  public form: Form;
  public loading: boolean = true;

  private _formSubscription: Subscription;

  constructor(
    private _csfm: CrudServiceForModel,
    private _uiService: UIService
  ) {}

  public onSubmit = (ngForm: NgForm): void => {
    // console.log(ngForm);
    // console.log(ngForm.value);
    this.loading = true;
    this.onCreationSubmit.next();

    this.entitet
      .save(this._csfm, ngForm.value)
      .pipe(take(1))
      .subscribe(
        () => {},
        (err: HttpErrorResponse) => {
          this._uiService.showSnackbar(err.message, null, 1000);
        },
        () => {
          this._uiService.showSnackbar('Entitet uspesno kreiran', null, 1000);
          this.onCreationFinished.next();
        }
      );
  };

  public onCancel = (): void => {
    this.onCreationFinished.next();
  };

  private _onInitForm = (): void => {
    this._formSubscription = this.entitet.getFormSubject().subscribe(
      (form) => {
        this.form = form;
      },
      (err: HttpErrorResponse) => {
        this._uiService.showSnackbar(err.message, null, 1000);
        this.onCancel();
      },
      () => {
        this.loading = false;
      }
    );

    this.entitet.getForm(this._csfm, true);
  };

  ngOnInit(): void {
    console.log('CreateComponent init');
  }

  ngOnChanges(changes: SimpleChanges): void {
    // console.log(changes);
    this.loading = true;
    this.form = undefined;
    try {
      this._onInitForm();
    } catch (error) {
      this._uiService.showSnackbar(
        'bice implementirano u verziji: 2.0',
        null,
        1000
      );
      this.onCancel();
    }
  }

  ngOnDestroy(): void {
    if (this._formSubscription) this._formSubscription.unsubscribe();
    console.log('CreateComponent destroyed');
  }
}
