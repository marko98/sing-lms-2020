import { Component, OnInit, OnDestroy } from '@angular/core';
import { CrudService } from '../shared/service/crud.service';
import { Grad } from '../shared/model/entity/grad.model';
import { Drzava } from '../shared/model/entity/drzava.model';
import {
  GradService,
  GRAD_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/grad_service.service';
import {
  DrzavaService,
  DRZAVA_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/drzava_service.service';
import {
  AdresaService,
  ADRESA_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/adresa_service.service';
import {
  AdministratorService,
  ADMINISTRATOR_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/administrator_service.service';
import {
  AutorNastavniMaterijalService,
  AUTOR_NASTAVNI_MATERIJAL_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/autor_nastavni_materijal_service.service';
import {
  AutorService,
  AUTOR_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/autor_service.service';
import {
  DatumPohadjanjaPredmetaService,
  DATUM_POHADJANJA_PREDMETA_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/datum_pohadjanja_predmeta_service.service';
import {
  DiplomskiRadService,
  DIPLOMSKI_RAD_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/diplomski_rad_service.service';
import {
  DogadjajKalendarService,
  DOGADJAJ_KALENDAR_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/dogadjaj_kalendar_service.service';
import {
  DogadjajService,
  DOGADJAJ_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/dogadjaj_service.service';
import {
  DrugiOblikNastaveService,
  DRUGI_OBLIK_NASTAVE_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/drugi_oblik_nastave_service.service';
import {
  EvaluacijaZnanjaNacinEvaluacijeService,
  EVALUACIJA_ZNANJA_NACIN_EVALUACIJE_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/evaluacija_znanja_nacin_evaluacije_service.service';
import {
  EvaluacijaZnanjaService,
  EVALUACIJA_ZNANJA_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/evaluacija_znanja_service.service';
import {
  FajlService,
  FAJL_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/fajl_service.service';
import {
  FakultetService,
  FAKULTET_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/fakultet_service.service';
import {
  GodinaStudijaObavestenjeService,
  GODINA_STUDIJA_OBAVESTENJE_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/godina_studija_obavestenje_service.service';
import {
  GodinaStudijaService,
  GODINA_STUDIJA_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/godina_studija_service.service';
import {
  IshodService,
  ISHOD_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/ishod_service.service';
import {
  IstrazivackiRadService,
  ISTRAZIVACKI_RAD_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/istrazivacki_rad_service.service';
import {
  IstrazivackiRadStudentNaStudijiService,
  ISTRAZIVACKI_RAD_STUDENT_NA_STUDIJI_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/istrazivacki_rad_student_na_studiji_service.service';
import {
  KalendarService,
  KALENDAR_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/kalendar_service.service';
import {
  KonsultacijaService,
  KONSULTACIJA_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/konsultacija_service.service';
import {
  NastavniMaterijalService,
  NASTAVNI_MATERIJAL_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/nastavni_materijal_service.service';
import {
  NastavnikDiplomskiRadService,
  NASTAVNIK_DIPLOMSKI_RAD_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/nastavnik_diplomski_rad_service.service';
import {
  NastavnikEvaluacijaZnanjaService,
  NASTAVNIK_EVALUACIJA_ZNANJA_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/nastavnik_evaluacija_znanja_service.service';
import {
  NastavnikFakultetService,
  NASTAVNIK_FAKULTET_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/nastavnik_fakultet_service.service';
import {
  NastavnikNaRealizacijiService,
  NASTAVNIK_NA_REALIZACIJI_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/nastavnik_na_realizaciji_service.service';
import {
  NastavnikNaRealizacijiTipNastaveService,
  NASTAVNIK_NA_REALIZACIJI_TIP_NASTAVE_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/nastavnik_na_realizaciji_tip_nastave_service.service';
import {
  NastavnikService,
  NASTAVNIK_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/nastavnik_service.service';
import {
  NaucnaOblastService,
  NAUCNA_OBLAST_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/naucna_oblast_service.service';
import {
  ObavestenjeService,
  OBAVESTENJE_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/obavestenje_service.service';
import {
  ObrazovniCiljService,
  OBRAZOVNI_CILJ_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/obrazovni_cilj_service.service';
import {
  OdeljenjeService,
  ODELJENJE_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/odeljenje_service.service';
import {
  OdgovorService,
  ODGOVOR_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/odgovor_service.service';
import {
  PitanjeService,
  PITANJE_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/pitanje_service.service';
import {
  PohadjanjePredmetaService,
  POHADJANJE_PREDMETA_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/pohadjanje_predmeta_service.service';
import {
  PolaganjeService,
  POLAGANJE_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/polaganje_service.service';
import {
  PredmetService,
  PREDMET_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/predmet_service.service';
import {
  PredmetTipDrugogOblikaNastaveService,
  PREDMET_TIP_DRUGOG_OBLIKA_NASTAVE_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/predmet_tip_drugog_oblika_nastave_service.service';
import {
  PredmetTipNastaveService,
  PREDMET_TIP_NASTAVE_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/predmet_tip_nastave_service.service';
import {
  PredmetUslovniPredmetService,
  PREDMET_USLOVNI_PREDMET_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/predmet_uslovni_predmet_service.service';
import {
  ProstorijaService,
  PROSTORIJA_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/prostorija_service.service';
import {
  RealizacijaPredmetaService,
  REALIZACIJA_PREDMETA_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/realizacija_predmeta_service.service';
import {
  RolaService,
  ROLA_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/rola_service.service';
import {
  RegistrovaniKorisnikRolaService,
  REGISTROVANI_KORISNIK_ROLA_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/registrovani_korisnik_rola_service.service';
import { RegistrovaniKorisnik } from '../shared/model/entity/registrovani_korisnik.model';
import {
  RegistrovaniKorisnikService,
  REGISTROVANI_KORISNIK_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/registrovani_korisnik_service.service';
import {
  SilabusService,
  SILABUS_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/silabus_service.service';
import {
  StudentNaStudijiService,
  STUDENT_NA_STUDIJI_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/student_na_studiji_service.service';
import {
  StudentService,
  STUDENT_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/student_service.service';
import {
  StudentskaSluzbaService,
  STUDENTSKA_SLUZBA_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/studentska_sluzba_service.service';
import {
  StudentskiSluzbenikService,
  STUDENTSKI_SLUZBENIK_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/studentski_sluzbenik_service.service';
import {
  StudijskiProgramService,
  STUDIJSKI_PROGRAM_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/studijski_program_service.service';
import {
  TerminNastaveService,
  TERMIN_NASTAVE_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/termin_nastave_service.service';
import {
  TitulaService,
  TITULA_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/titula_service.service';
import {
  UniverzitetService,
  UNIVERZITET_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/univerzitet_service.service';
import {
  VremeOdrzavanjaUNedeljiService,
  VREME_ODRZAVANJA_U_NEDELJI_SERVICE_FOR_MODEL_INTERFACE,
} from '../shared/service/entity/vreme_odrzavanja_u_nedelji_service.service';
import { ROLES } from '../shared/const';
import {
  AuthService,
  AuthUserModelInterface,
} from '../shared/service/auth.service';
import { Adresa } from '../shared/model/entity/adresa.model';
import { Administrator } from '../shared/model/entity/administrator.model';
import { AutorNastavniMaterijal } from '../shared/model/entity/autor_nastavni_materijal.model';
import { Autor } from '../shared/model/entity/autor.model';
import { DatumPohadjanjaPredmeta } from '../shared/model/entity/datum_pohadjanja_predmeta.model';
import { DiplomskiRad } from '../shared/model/entity/diplomski_rad.model';
import { DogadjajKalendar } from '../shared/model/entity/dogadjaj_kalendar.model';
import { Dogadjaj } from '../shared/model/entity/dogadjaj.model';
import { DrugiOblikNastave } from '../shared/model/entity/drugi_oblik_nastave.model';
import { EvaluacijaZnanjaNacinEvaluacije } from '../shared/model/entity/evaluacija_znanja_nacin_evaluacije.model';
import { EvaluacijaZnanja } from '../shared/model/entity/evaluacija_znanja.model';
import { Fajl } from '../shared/model/entity/fajl.model';
import { Fakultet } from '../shared/model/entity/fakultet.model';
import { GodinaStudijaObavestenje } from '../shared/model/entity/godina_studija_obavestenje.model';
import { GodinaStudija } from '../shared/model/entity/godina_studija.model';
import { Ishod } from '../shared/model/entity/ishod.model';
import { IstrazivackiRad } from '../shared/model/entity/istrazivacki_rad.model';
import { IstrazivackiRadStudentNaStudiji } from '../shared/model/entity/istrazivacki_rad_student_na_studiji.model';
import { Kalendar } from '../shared/model/entity/kalendar.model';
import { Konsultacija } from '../shared/model/entity/konsultacija.model';
import { NastavniMaterijal } from '../shared/model/entity/nastavni_materijal.model';
import { NastavnikDiplomskiRad } from '../shared/model/entity/nastavnik_diplomski_rad.model';
import { NastavnikEvaluacijaZnanja } from '../shared/model/entity/nastavnik_evaluacija_znanja.model';
import { NastavnikFakultet } from '../shared/model/entity/nastavnik_fakultet.model';
import { NastavnikNaRealizaciji } from '../shared/model/entity/nastavnik_na_realizaciji.model';
import { NastavnikNaRealizacijiTipNastave } from '../shared/model/entity/nastavnik_na_realizaciji_tip_nastave.model';
import { Nastavnik } from '../shared/model/entity/nastavnik.model';
import { NaucnaOblast } from '../shared/model/entity/naucna_oblast.model';
import { Obavestenje } from '../shared/model/entity/obavestenje.model';
import { ObrazovniCilj } from '../shared/model/entity/obrazovni_cilj.model';
import { Odeljenje } from '../shared/model/entity/odeljenje.model';
import { Odgovor } from '../shared/model/entity/odgovor.model';
import { Pitanje } from '../shared/model/entity/pitanje.model';
import { PohadjanjePredmeta } from '../shared/model/entity/pohadjanje_predmeta.model';
import { Polaganje } from '../shared/model/entity/polaganje.model';
import { Predmet } from '../shared/model/entity/predmet.model';
import { PredmetTipDrugogOblikaNastave } from '../shared/model/entity/predmet_tip_drugog_oblika_nastave.model';
import { PredmetTipNastave } from '../shared/model/entity/predmet_tip_nastave.model';
import { PredmetUslovniPredmet } from '../shared/model/entity/predmet_uslovni_predmet.model';
import { Prostorija } from '../shared/model/entity/prostorija.model';
import { RealizacijaPredmeta } from '../shared/model/entity/realizacija_predmeta.model';
import { Rola } from '../shared/model/entity/rola.model';
import { RegistrovaniKorisnikRola } from '../shared/model/entity/registrovani_korisnik_rola.model';
import { Silabus } from '../shared/model/entity/silabus.model';
import { StudentNaStudiji } from '../shared/model/entity/student_na_studiji.model';
import { Student } from '../shared/model/entity/student.model';
import { StudentskaSluzba } from '../shared/model/entity/studentska_sluzba.model';
import { StudentskiSluzbenik } from '../shared/model/entity/studentski_sluzbenik.model';
import { StudijskiProgram } from '../shared/model/entity/studijski_program.model';
import { TerminNastave } from '../shared/model/entity/termin_nastave.model';
import { Titula } from '../shared/model/entity/titula.model';
import { Univerzitet } from '../shared/model/entity/univerzitet.model';
import { VremeOdrzavanjaUNedelji } from '../shared/model/entity/vreme_odrzavanja_u_nedelji.model';
import { CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../shared/service/crud.service';
import { AdministratorFormableTableable } from '../shared/model/formables-tableables/adminstrator.formables-tableables';
import { AdresaFormableTableable } from '../shared/model/formables-tableables/adresa.formables-tableables';
import { AutorNastavniMaterijalFormableTableable } from '../shared/model/formables-tableables/autor_nastavni_materijal.formables-tableables';
import { AutorFormableTableable } from '../shared/model/formables-tableables/autor.formables-tableables';
import { DrzavaFormableTableable } from '../shared/model/formables-tableables/drzava.formables-tableables';
import { DatumPohadjanjaPredmetaFormableTableable } from '../shared/model/formables-tableables/datum_pohadjanja_predmeta.formables-tableables';
import { DiplomskiRadFormableTableable } from '../shared/model/formables-tableables/diplomski_rad.formables-tableables';
import { DogadjajKalendarFormableTableable } from '../shared/model/formables-tableables/dogadjaj_kalendar.formables-tableables';
import { DogadjajFormableTableable } from '../shared/model/formables-tableables/dogadjaj.formables-tableables';
import { DrugiOblikNastaveFormableTableable } from '../shared/model/formables-tableables/drugi_oblik_nastave.formables-tableables';
import { EvaluacijaZnanjaNacinEvaluacijeFormableTableable } from '../shared/model/formables-tableables/evaluacija_znanja_nacin_evaluacije.formables-tableables';
import { EvaluacijaZnanjaFormableTableable } from '../shared/model/formables-tableables/evaluacija_znanja.formables-tableables';
import { FajlFormableTableable } from '../shared/model/formables-tableables/fajl.formables-tableables';
import { FakultetFormableTableable } from '../shared/model/formables-tableables/fakultet.formables-tableables';
import { GradFormableTableable } from '../shared/model/formables-tableables/grad.formables-tableables';
import { GodinaStudijaObavestenjeFormableTableable } from '../shared/model/formables-tableables/godina_studija_obavestenje.formables-tableables';
import { GodinaStudijaFormableTableable } from '../shared/model/formables-tableables/godina_studija.formables-tableables';
import { IshodFormableTableable } from '../shared/model/formables-tableables/ishod.formables-tableables';
import { IstrazivackiRadFormableTableable } from '../shared/model/formables-tableables/istrazivacki_rad.formables-tableables';
import { VremeOdrzavanjaUNedeljiFormableTableable } from '../shared/model/formables-tableables/vreme_odrzavanja_u_nedelji.formables-tableables';
import { UniverzitetFormableTableable } from '../shared/model/formables-tableables/univerzitet.formables-tableables';
import { TitulaFormableTableable } from '../shared/model/formables-tableables/titula.formables-tableables';
import { TerminNastaveFormableTableable } from '../shared/model/formables-tableables/termin_nastave.formables-tableables';
import { StudijskiProgramFormableTableable } from '../shared/model/formables-tableables/studijski_program.formables-tableables';
import { StudentskiSluzbenikFormableTableable } from '../shared/model/formables-tableables/studentski_sluzbenik.formables-tableables';
import { StudentskaSluzbaFormableTableable } from '../shared/model/formables-tableables/studentska_sluzba.formables-tableables';
import { StudentFormableTableable } from '../shared/model/formables-tableables/student.formables-tableables';
import { StudentNaStudijiFormableTableable } from '../shared/model/formables-tableables/student_na_studiji.formables-tableables';
import { SilabusFormableTableable } from '../shared/model/formables-tableables/silabus.formables-tableables';
import { RegistrovaniKorisnikFormableTableable } from '../shared/model/formables-tableables/registrovani_korisnik.formables-tableables';
import { RegistrovaniKorisnikRolaFormableTableable } from '../shared/model/formables-tableables/registrovani_korisnik_rola.formables-tableables';
import { RolaFormableTableable } from '../shared/model/formables-tableables/rola.formables-tableables';
import { RealizacijaPredmetaFormableTableable } from '../shared/model/formables-tableables/realizacija_predmeta.formables-tableables';
import { ProstorijaFormableTableable } from '../shared/model/formables-tableables/prostorija.formables-tableables';
import { PredmetUslovniPredmetFormableTableable } from '../shared/model/formables-tableables/predmet_uslovni_predmet.formables-tableables';
import { PredmetTipNastaveFormableTableable } from '../shared/model/formables-tableables/predmet_tip_nastave.formables-tableables';
import { PredmetTipDrugogOblikaNastaveFormableTableable } from '../shared/model/formables-tableables/predmet_tip_drugog_oblika_nastave.formables-tableables';
import { PredmetFormableTableable } from '../shared/model/formables-tableables/predmet.formables-tableables';
import { PolaganjeFormableTableable } from '../shared/model/formables-tableables/polaganje.formables-tableables';
import { PohadjanjePredmetaFormableTableable } from '../shared/model/formables-tableables/pohadjanje_predmeta.formables-tableables';
import { PitanjeFormableTableable } from '../shared/model/formables-tableables/pitanje.formables-tableables';
import { OdgovorFormableTableable } from '../shared/model/formables-tableables/odgovor.formables-tableables';
import { OdeljenjeFormableTableable } from '../shared/model/formables-tableables/odeljenje.formables-tableables';
import { ObrazovniCiljFormableTableable } from '../shared/model/formables-tableables/obrazovni_cilj.formables-tableables';
import { NastavnikFormableTableable } from '../shared/model/formables-tableables/nastavnik.formables-tableables';
import { NaucnaOblastFormableTableable } from '../shared/model/formables-tableables/naucna_oblast.formables-tableables';
import { ObavestenjeFormableTableable } from '../shared/model/formables-tableables/obavestenje.formables-tableables';
import { NastavnikNaRealizacijiTipNastaveFormableTableable } from '../shared/model/formables-tableables/nastavnik_na_realizaciji_tip_nastave.formables-tableables';
import { NastavnikNaRealizacijiFormableTableable } from '../shared/model/formables-tableables/nastavnik_na_realizaciji.formables-tableables';
import { NastavnikFakultetFormableTableable } from '../shared/model/formables-tableables/nastavnik_fakultet.formables-tableables';
import { NastavniMaterijalFormableTableable } from '../shared/model/formables-tableables/nastavni_materijal.formables-tableables';
import { NastavnikDiplomskiRadFormableTableable } from '../shared/model/formables-tableables/nastavnik_diplomski_rad.formables-tableables';
import { NastavnikEvaluacijaZnanjaFormableTableable } from '../shared/model/formables-tableables/nastavnik_evaluacija_znanja.formables-tableables';
import { IstrazivackiRadStudentNaStudijiFormableTableable } from '../shared/model/formables-tableables/istrazivacki_rad_student_na_studiji.formables-tableables';
import { KalendarFormableTableable } from '../shared/model/formables-tableables/kalendar.formables-tableables';
import { KonsultacijaFormableTableable } from '../shared/model/formables-tableables/konsultacija.formables-tableables';
import { Kontakt } from '../shared/model/entity/kontakt.model';
import { KONTAKT_SERVICE_FOR_MODEL_INTERFACE } from '../shared/service/entity/kontakt.service';
import { KontaktFormableTableable } from '../shared/model/formables-tableables/kontakt.formables-tableables';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit, OnDestroy {
  public ulogovaniRegistrovaniKorisnik: RegistrovaniKorisnik;

  public showExportPodataka: boolean = false;

  public data: {
    formableClass: any;
    tableableClass: any;
    entitetClass: any;
    crudRoles: {
      create: string[];
      read: string[];
      update: string[];
      delete: string[];
    };
    crudServiceForModelInterface: CRUD_SERVICE_FOR_MODEL_INTERFACE;
  };
  public sidenavList: {
    text: string;
    formableClass: any;
    tableableClass: any;
    entitetClass: any;
    reqRoles: string[];
    crudRoles: {
      create: string[];
      read: string[];
      update: string[];
      delete: string[];
    };
    crudServiceForModelInterface: CRUD_SERVICE_FOR_MODEL_INTERFACE;
  }[] = [
    {
      text: 'adresa',
      formableClass: AdresaFormableTableable,
      tableableClass: AdresaFormableTableable,
      entitetClass: Adresa,
      crudServiceForModelInterface: ADRESA_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'administrator',
      formableClass: AdministratorFormableTableable,
      tableableClass: AdministratorFormableTableable,
      entitetClass: Administrator,
      crudServiceForModelInterface: ADMINISTRATOR_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'autor nastavni materijal',
      formableClass: AutorNastavniMaterijalFormableTableable,
      tableableClass: AutorNastavniMaterijalFormableTableable,
      entitetClass: AutorNastavniMaterijal,
      crudServiceForModelInterface: AUTOR_NASTAVNI_MATERIJAL_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'autor',
      formableClass: AutorFormableTableable,
      tableableClass: AutorFormableTableable,
      entitetClass: Autor,
      crudServiceForModelInterface: AUTOR_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'drzava',
      formableClass: DrzavaFormableTableable,
      tableableClass: DrzavaFormableTableable,
      entitetClass: Drzava,
      crudServiceForModelInterface: DRZAVA_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'datum_pohadjanja_predmeta',
      formableClass: DatumPohadjanjaPredmetaFormableTableable,
      tableableClass: DatumPohadjanjaPredmetaFormableTableable,
      entitetClass: DatumPohadjanjaPredmeta,
      crudServiceForModelInterface: DATUM_POHADJANJA_PREDMETA_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'diplomski_rad',
      formableClass: DiplomskiRadFormableTableable,
      tableableClass: DiplomskiRadFormableTableable,
      entitetClass: DiplomskiRad,
      crudServiceForModelInterface: DIPLOMSKI_RAD_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'dogadjaj_kalendar',
      formableClass: DogadjajKalendarFormableTableable,
      tableableClass: DogadjajKalendarFormableTableable,
      entitetClass: DogadjajKalendar,
      crudServiceForModelInterface: DOGADJAJ_KALENDAR_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
        read: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
        update: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'dogadjaj',
      formableClass: DogadjajFormableTableable,
      tableableClass: DogadjajFormableTableable,
      entitetClass: Dogadjaj,
      crudServiceForModelInterface: DOGADJAJ_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
        read: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
        update: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'drugi_oblik_nastave',
      formableClass: DrugiOblikNastaveFormableTableable,
      tableableClass: DrugiOblikNastaveFormableTableable,
      entitetClass: DrugiOblikNastave,
      crudServiceForModelInterface: DRUGI_OBLIK_NASTAVE_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'evaluacija_znanja_nacin_evaluacije',
      formableClass: EvaluacijaZnanjaNacinEvaluacijeFormableTableable,
      tableableClass: EvaluacijaZnanjaNacinEvaluacijeFormableTableable,
      entitetClass: EvaluacijaZnanjaNacinEvaluacije,
      crudServiceForModelInterface: EVALUACIJA_ZNANJA_NACIN_EVALUACIJE_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'evaluacija_znanja',
      formableClass: EvaluacijaZnanjaFormableTableable,
      tableableClass: EvaluacijaZnanjaFormableTableable,
      entitetClass: EvaluacijaZnanja,
      crudServiceForModelInterface: EVALUACIJA_ZNANJA_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
        read: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
        update: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'fajl',
      formableClass: FajlFormableTableable,
      tableableClass: FajlFormableTableable,
      entitetClass: Fajl,
      crudServiceForModelInterface: FAJL_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'fakultet',
      formableClass: FakultetFormableTableable,
      tableableClass: FakultetFormableTableable,
      entitetClass: Fakultet,
      crudServiceForModelInterface: FAKULTET_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'grad',
      formableClass: GradFormableTableable,
      tableableClass: GradFormableTableable,
      entitetClass: Grad,
      crudServiceForModelInterface: GRAD_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'godina_studija_obavestenje',
      formableClass: GodinaStudijaObavestenjeFormableTableable,
      tableableClass: GodinaStudijaObavestenjeFormableTableable,
      entitetClass: GodinaStudijaObavestenje,
      crudServiceForModelInterface: GODINA_STUDIJA_OBAVESTENJE_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
        read: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
        update: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'godina_studija',
      formableClass: GodinaStudijaFormableTableable,
      tableableClass: GodinaStudijaFormableTableable,
      entitetClass: GodinaStudija,
      crudServiceForModelInterface: GODINA_STUDIJA_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'ishod',
      formableClass: IshodFormableTableable,
      tableableClass: IshodFormableTableable,
      entitetClass: Ishod,
      crudServiceForModelInterface: ISHOD_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'istrazivacki_rad',
      formableClass: IstrazivackiRadFormableTableable,
      tableableClass: IstrazivackiRadFormableTableable,
      entitetClass: IstrazivackiRad,
      crudServiceForModelInterface: ISTRAZIVACKI_RAD_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'istrazivacki_rad_student_na_studiji',
      formableClass: IstrazivackiRadStudentNaStudijiFormableTableable,
      tableableClass: IstrazivackiRadStudentNaStudijiFormableTableable,
      entitetClass: IstrazivackiRadStudentNaStudiji,
      crudServiceForModelInterface: ISTRAZIVACKI_RAD_STUDENT_NA_STUDIJI_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'kalendar',
      formableClass: KalendarFormableTableable,
      tableableClass: KalendarFormableTableable,
      entitetClass: Kalendar,
      crudServiceForModelInterface: KALENDAR_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
        read: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
        update: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'konsultacija',
      formableClass: KonsultacijaFormableTableable,
      tableableClass: KonsultacijaFormableTableable,
      entitetClass: Konsultacija,
      crudServiceForModelInterface: KONSULTACIJA_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'kontakt',
      formableClass: KontaktFormableTableable,
      tableableClass: KontaktFormableTableable,
      entitetClass: Kontakt,
      crudServiceForModelInterface: KONTAKT_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'nastavni_materijal',
      formableClass: NastavniMaterijalFormableTableable,
      tableableClass: NastavniMaterijalFormableTableable,
      entitetClass: NastavniMaterijal,
      crudServiceForModelInterface: NASTAVNI_MATERIJAL_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'nastavnik_diplomski_rad',
      formableClass: NastavnikDiplomskiRadFormableTableable,
      tableableClass: NastavnikDiplomskiRadFormableTableable,
      entitetClass: NastavnikDiplomskiRad,
      crudServiceForModelInterface: NASTAVNIK_DIPLOMSKI_RAD_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'nastavnik_evaluacija_znanja',
      formableClass: NastavnikEvaluacijaZnanjaFormableTableable,
      tableableClass: NastavnikEvaluacijaZnanjaFormableTableable,
      entitetClass: NastavnikEvaluacijaZnanja,
      crudServiceForModelInterface: NASTAVNIK_EVALUACIJA_ZNANJA_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'nastavnik_fakultet',
      formableClass: NastavnikFakultetFormableTableable,
      tableableClass: NastavnikFakultetFormableTableable,
      entitetClass: NastavnikFakultet,
      crudServiceForModelInterface: NASTAVNIK_FAKULTET_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'nastavnik_na_realizaciji',
      formableClass: NastavnikNaRealizacijiFormableTableable,
      tableableClass: NastavnikNaRealizacijiFormableTableable,
      entitetClass: NastavnikNaRealizaciji,
      crudServiceForModelInterface: NASTAVNIK_NA_REALIZACIJI_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'nastavnik_na_realizaciji_tip_nastave',
      formableClass: NastavnikNaRealizacijiTipNastaveFormableTableable,
      tableableClass: NastavnikNaRealizacijiTipNastaveFormableTableable,
      entitetClass: NastavnikNaRealizacijiTipNastave,
      crudServiceForModelInterface: NASTAVNIK_NA_REALIZACIJI_TIP_NASTAVE_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'nastavnik',
      formableClass: NastavnikFormableTableable,
      tableableClass: NastavnikFormableTableable,
      entitetClass: Nastavnik,
      crudServiceForModelInterface: NASTAVNIK_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'naucna_oblast',
      formableClass: NaucnaOblastFormableTableable,
      tableableClass: NaucnaOblastFormableTableable,
      entitetClass: NaucnaOblast,
      crudServiceForModelInterface: NAUCNA_OBLAST_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'obavestenje',
      formableClass: ObavestenjeFormableTableable,
      tableableClass: ObavestenjeFormableTableable,
      entitetClass: Obavestenje,
      crudServiceForModelInterface: OBAVESTENJE_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
        read: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
        update: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'obrazovni_cilj',
      formableClass: ObrazovniCiljFormableTableable,
      tableableClass: ObrazovniCiljFormableTableable,
      entitetClass: ObrazovniCilj,
      crudServiceForModelInterface: OBRAZOVNI_CILJ_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'odeljenje',
      formableClass: OdeljenjeFormableTableable,
      tableableClass: OdeljenjeFormableTableable,
      entitetClass: Odeljenje,
      crudServiceForModelInterface: ODELJENJE_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'odgovor',
      formableClass: OdgovorFormableTableable,
      tableableClass: OdgovorFormableTableable,
      entitetClass: Odgovor,
      crudServiceForModelInterface: ODGOVOR_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'pitanje',
      formableClass: PitanjeFormableTableable,
      tableableClass: PitanjeFormableTableable,
      entitetClass: Pitanje,
      crudServiceForModelInterface: PITANJE_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'pohadjanje_predmeta',
      formableClass: PohadjanjePredmetaFormableTableable,
      tableableClass: PohadjanjePredmetaFormableTableable,
      entitetClass: PohadjanjePredmeta,
      crudServiceForModelInterface: POHADJANJE_PREDMETA_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'polaganje',
      formableClass: PolaganjeFormableTableable,
      tableableClass: PolaganjeFormableTableable,
      entitetClass: Polaganje,
      crudServiceForModelInterface: POLAGANJE_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'predmet',
      formableClass: PredmetFormableTableable,
      tableableClass: PredmetFormableTableable,
      entitetClass: Predmet,
      crudServiceForModelInterface: PREDMET_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'predmet_tip_drugog_oblika_nastave',
      formableClass: PredmetTipDrugogOblikaNastaveFormableTableable,
      tableableClass: PredmetTipDrugogOblikaNastaveFormableTableable,
      entitetClass: PredmetTipDrugogOblikaNastave,
      crudServiceForModelInterface: PREDMET_TIP_DRUGOG_OBLIKA_NASTAVE_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'predmet_tip_nastave',
      formableClass: PredmetTipNastaveFormableTableable,
      tableableClass: PredmetTipNastaveFormableTableable,
      entitetClass: PredmetTipNastave,
      crudServiceForModelInterface: PREDMET_TIP_NASTAVE_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'predmet_uslovni_predmet',
      formableClass: PredmetUslovniPredmetFormableTableable,
      tableableClass: PredmetUslovniPredmetFormableTableable,
      entitetClass: PredmetUslovniPredmet,
      crudServiceForModelInterface: PREDMET_USLOVNI_PREDMET_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'prostorija',
      formableClass: ProstorijaFormableTableable,
      tableableClass: ProstorijaFormableTableable,
      entitetClass: Prostorija,
      crudServiceForModelInterface: PROSTORIJA_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'realizacija_predmeta',
      formableClass: RealizacijaPredmetaFormableTableable,
      tableableClass: RealizacijaPredmetaFormableTableable,
      entitetClass: RealizacijaPredmeta,
      crudServiceForModelInterface: REALIZACIJA_PREDMETA_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'rola',
      formableClass: RolaFormableTableable,
      tableableClass: RolaFormableTableable,
      entitetClass: Rola,
      crudServiceForModelInterface: ROLA_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'registrovani_korisnik_rola',
      formableClass: RegistrovaniKorisnikRolaFormableTableable,
      tableableClass: RegistrovaniKorisnikRolaFormableTableable,
      entitetClass: RegistrovaniKorisnikRola,
      crudServiceForModelInterface: REGISTROVANI_KORISNIK_ROLA_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'registrovani_korisnik',
      formableClass: RegistrovaniKorisnikFormableTableable,
      tableableClass: RegistrovaniKorisnikFormableTableable,
      entitetClass: RegistrovaniKorisnik,
      crudServiceForModelInterface: REGISTROVANI_KORISNIK_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'silabus',
      formableClass: SilabusFormableTableable,
      tableableClass: SilabusFormableTableable,
      entitetClass: Silabus,
      crudServiceForModelInterface: SILABUS_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'student_na_studiji',
      formableClass: StudentNaStudijiFormableTableable,
      tableableClass: StudentNaStudijiFormableTableable,
      entitetClass: StudentNaStudiji,
      crudServiceForModelInterface: STUDENT_NA_STUDIJI_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'student',
      formableClass: StudentFormableTableable,
      tableableClass: StudentFormableTableable,
      entitetClass: Student,
      crudServiceForModelInterface: STUDENT_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
        read: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
        update: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'studentska_sluzba',
      formableClass: StudentskaSluzbaFormableTableable,
      tableableClass: StudentskaSluzbaFormableTableable,
      entitetClass: StudentskaSluzba,
      crudServiceForModelInterface: STUDENTSKA_SLUZBA_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'studentski_sluzbenik',
      formableClass: StudentskiSluzbenikFormableTableable,
      tableableClass: StudentskiSluzbenikFormableTableable,
      entitetClass: StudentskiSluzbenik,
      crudServiceForModelInterface: STUDENTSKI_SLUZBENIK_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'studijski_program',
      formableClass: StudijskiProgramFormableTableable,
      tableableClass: StudijskiProgramFormableTableable,
      entitetClass: StudijskiProgram,
      crudServiceForModelInterface: STUDIJSKI_PROGRAM_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'termin_nastave',
      formableClass: TerminNastaveFormableTableable,
      tableableClass: TerminNastaveFormableTableable,
      entitetClass: TerminNastave,
      crudServiceForModelInterface: TERMIN_NASTAVE_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
        read: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
        update: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'titula',
      formableClass: TitulaFormableTableable,
      tableableClass: TitulaFormableTableable,
      entitetClass: Titula,
      crudServiceForModelInterface: TITULA_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'univerzitet',
      formableClass: UniverzitetFormableTableable,
      tableableClass: UniverzitetFormableTableable,
      entitetClass: Univerzitet,
      crudServiceForModelInterface: UNIVERZITET_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR],
        read: [ROLES.ROLE_ADMINISTRATOR],
        update: [ROLES.ROLE_ADMINISTRATOR],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
    {
      text: 'vreme_odrzavanja_u_nedelji',
      formableClass: VremeOdrzavanjaUNedeljiFormableTableable,
      tableableClass: VremeOdrzavanjaUNedeljiFormableTableable,
      entitetClass: VremeOdrzavanjaUNedelji,
      crudServiceForModelInterface: VREME_ODRZAVANJA_U_NEDELJI_SERVICE_FOR_MODEL_INTERFACE,
      reqRoles: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
      crudRoles: {
        create: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
        read: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
        update: [ROLES.ROLE_ADMINISTRATOR, ROLES.ROLE_STUDENTSKI_SLUZBENIK],
        delete: [ROLES.ROLE_ADMINISTRATOR],
      },
    },
  ];

  constructor(
    private _authService: AuthService,
    private _registrovaniKorisnikService: RegistrovaniKorisnikService
  ) {}

  public sidenavListItemSelected = (snli: {
    text: string;
    formableClass: any;
    tableableClass: any;
    entitetClass: any;
    crudRoles: {
      create: string[];
      read: string[];
      update: string[];
      delete: string[];
    };
    crudServiceForModelInterface: CRUD_SERVICE_FOR_MODEL_INTERFACE;
  }): void => {
    this.data = {
      formableClass: snli.formableClass,
      tableableClass: snli.tableableClass,
      entitetClass: snli.entitetClass,
      crudRoles: snli.crudRoles,
      crudServiceForModelInterface: snli.crudServiceForModelInterface,
    };
  };

  public onLogout = (): void => {
    this._authService.onLogout();
  };

  ngOnInit(): void {
    this._authService
      .getUser()
      .pipe(take(1))
      .subscribe((user: AuthUserModelInterface) => {
        this._registrovaniKorisnikService
          .findOneByKorisnickoIme(user.korisnickoIme)
          .pipe(take(1))
          .subscribe((regKor) => {
            this.ulogovaniRegistrovaniKorisnik = regKor;
          });
      });

    console.log('HomeComponent init');
  }

  ngOnDestroy(): void {
    console.log('HomeComponent destroyed');
  }
}
