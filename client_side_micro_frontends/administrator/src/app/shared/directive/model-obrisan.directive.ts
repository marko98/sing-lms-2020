import { Directive, Input, OnInit, ElementRef } from '@angular/core';
import { StanjeModela } from '../model/enum/stanje_modela.enum';

@Directive({
  selector: '[appModelObrisan]',
})
export class ModelObrisanDirective implements OnInit {
  @Input() stanjeModela: StanjeModela;

  constructor(private _elRef: ElementRef) {}

  ngOnInit(): void {
    if (this.stanjeModela.toString() === 'OBRISAN')
      (<HTMLElement>this._elRef.nativeElement).parentNode.removeChild(
        this._elRef.nativeElement
      );
  }
}
