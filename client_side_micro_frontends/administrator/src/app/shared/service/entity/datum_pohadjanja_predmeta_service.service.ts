import { CrudService } from '../crud.service';
import {
  PATH,
  WS_PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
} from '../../const';
import { Injectable } from '@angular/core';
import { DatumPohadjanjaPredmeta } from '../../model/entity/datum_pohadjanja_predmeta.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { DatumPohadjanjaPredmetaFactory } from '../../model/patterns/creational/factory_method/datum_pohadjanja_predmeta.factory';
import { CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';

export const DATUM_POHADJANJA_PREDMETA_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: DatumPohadjanjaPredmetaFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.STUDENT,
  path: PATH.DATUM_POHADJANJA_PREDMETA,
  microservicePort: MICROSERVICE_PORT.STUDENT,
  wsPath: WS_PATH.DATUM_POHADJANJA_PREDMETA,
};
@Injectable({ providedIn: 'root' })
export class DatumPohadjanjaPredmetaService extends CrudService<
  DatumPohadjanjaPredmeta,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      DatumPohadjanjaPredmetaFactory.getInstance(),
      MICROSERVICE_NAME.STUDENT,
      PATH.DATUM_POHADJANJA_PREDMETA,
      MICROSERVICE_PORT.STUDENT,
      WS_PATH.DATUM_POHADJANJA_PREDMETA
    );
    this._findAll();
  }
}
