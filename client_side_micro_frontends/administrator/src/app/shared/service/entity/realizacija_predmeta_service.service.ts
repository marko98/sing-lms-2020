import { CrudService, CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';
import {
  PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
  WS_PATH,
} from '../../const';
import { Injectable } from '@angular/core';
import { RealizacijaPredmeta } from '../../model/entity/realizacija_predmeta.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { RealizacijaPredmetaFactory } from '../../model/patterns/creational/factory_method/realizacija_predmeta.factory';

export const REALIZACIJA_PREDMETA_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: RealizacijaPredmetaFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.PREDMET,
  path: PATH.REALIZACIJA_PREDMETA,
  microservicePort: MICROSERVICE_PORT.PREDMET,
  wsPath: WS_PATH.REALIZACIJA_PREDMETA,
};

@Injectable({ providedIn: 'root' })
export class RealizacijaPredmetaService extends CrudService<
  RealizacijaPredmeta,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      RealizacijaPredmetaFactory.getInstance(),
      MICROSERVICE_NAME.PREDMET,
      PATH.REALIZACIJA_PREDMETA,
      MICROSERVICE_PORT.PREDMET,
      WS_PATH.REALIZACIJA_PREDMETA
    );
    this._findAll();
  }
}
