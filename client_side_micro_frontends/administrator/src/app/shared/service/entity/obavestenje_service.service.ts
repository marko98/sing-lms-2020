import { CrudService } from '../crud.service';
import {
  PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
  WS_PATH,
} from '../../const';
import { Injectable } from '@angular/core';
import { Obavestenje } from '../../model/entity/obavestenje.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { ObavestenjeFactory } from '../../model/patterns/creational/factory_method/obavestenje.factory';
import { CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';

export const OBAVESTENJE_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: ObavestenjeFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.OBAVESTENJE,
  path: PATH.OBAVESTENJE,
  microservicePort: MICROSERVICE_PORT.OBAVESTENJE,
  wsPath: WS_PATH.OBAVESTENJE,
};
@Injectable({ providedIn: 'root' })
export class ObavestenjeService extends CrudService<Obavestenje, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      ObavestenjeFactory.getInstance(),
      MICROSERVICE_NAME.OBAVESTENJE,
      PATH.OBAVESTENJE,
      MICROSERVICE_PORT.OBAVESTENJE,
      WS_PATH.OBAVESTENJE
    );
    this._findAll();
  }
}
