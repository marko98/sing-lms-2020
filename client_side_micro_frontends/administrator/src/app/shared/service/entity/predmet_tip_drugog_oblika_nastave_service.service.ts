import { CrudService } from '../crud.service';
import {
  PATH,
  WS_PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
} from '../../const';
import { Injectable } from '@angular/core';
import { PredmetTipDrugogOblikaNastave } from '../../model/entity/predmet_tip_drugog_oblika_nastave.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { PredmetTipDrugogOblikaNastaveFactory } from '../../model/patterns/creational/factory_method/predmet_tip_drugog_oblika_nastave.factory';
import { CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';

export const PREDMET_TIP_DRUGOG_OBLIKA_NASTAVE_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: PredmetTipDrugogOblikaNastaveFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.PREDMET,
  path: PATH.PREDMET_TIP_DRUGOG_OBLIKA_NASTAVE,
  microservicePort: MICROSERVICE_PORT.PREDMET,
  wsPath: WS_PATH.PREDMET_TIP_DRUGOG_OBLIKA_NASTAVE,
};
@Injectable({ providedIn: 'root' })
export class PredmetTipDrugogOblikaNastaveService extends CrudService<
  PredmetTipDrugogOblikaNastave,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      PredmetTipDrugogOblikaNastaveFactory.getInstance(),
      MICROSERVICE_NAME.PREDMET,
      PATH.PREDMET_TIP_DRUGOG_OBLIKA_NASTAVE,
      MICROSERVICE_PORT.PREDMET,
      WS_PATH.PREDMET_TIP_DRUGOG_OBLIKA_NASTAVE
    );
    this._findAll();
  }
}
