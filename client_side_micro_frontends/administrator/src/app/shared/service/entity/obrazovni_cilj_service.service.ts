import { CrudService } from '../crud.service';
import {
  PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
  WS_PATH,
} from '../../const';
import { Injectable } from '@angular/core';
import { ObrazovniCilj } from '../../model/entity/obrazovni_cilj.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { ObrazovniCiljFactory } from '../../model/patterns/creational/factory_method/obrazovni_cilj.factory';
import { CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';

export const OBRAZOVNI_CILJ_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: ObrazovniCiljFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.PREDMET,
  path: PATH.OBRAZOVNI_CILJ,
  microservicePort: MICROSERVICE_PORT.PREDMET,
  wsPath: WS_PATH.OBRAZOVNI_CILJ,
};
@Injectable({ providedIn: 'root' })
export class ObrazovniCiljService extends CrudService<ObrazovniCilj, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      ObrazovniCiljFactory.getInstance(),
      MICROSERVICE_NAME.PREDMET,
      PATH.OBRAZOVNI_CILJ,
      MICROSERVICE_PORT.PREDMET,
      WS_PATH.OBRAZOVNI_CILJ
    );
    this._findAll();
  }
}
