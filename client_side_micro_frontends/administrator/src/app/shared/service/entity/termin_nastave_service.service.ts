import { CrudService, CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';
import {
  PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
  WS_PATH,
} from '../../const';
import { Injectable } from '@angular/core';
import { TerminNastave } from '../../model/entity/termin_nastave.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { TerminNastaveFactory } from '../../model/patterns/creational/factory_method/termin_nastave.factory';

export const TERMIN_NASTAVE_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: TerminNastaveFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.NASTAVA,
  path: PATH.TERMIN_NASTAVE,
  microservicePort: MICROSERVICE_PORT.NASTAVA,
  wsPath: WS_PATH.TERMIN_NASTAVE,
};

@Injectable({ providedIn: 'root' })
export class TerminNastaveService extends CrudService<TerminNastave, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      TerminNastaveFactory.getInstance(),
      MICROSERVICE_NAME.NASTAVA,
      PATH.TERMIN_NASTAVE,
      MICROSERVICE_PORT.NASTAVA,
      WS_PATH.TERMIN_NASTAVE
    );
    this._findAll();
  }
}
