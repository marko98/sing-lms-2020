import { CrudService } from '../crud.service';
import {
  PATH,
  WS_PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
} from '../../const';
import { Injectable } from '@angular/core';
import { GodinaStudija } from '../../model/entity/godina_studija.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { GodinaStudijaFactory } from '../../model/patterns/creational/factory_method/godina_studija.factory';
import { Observable } from 'rxjs';
import { GodinaStudijaDTO } from '../../model/dto/godina_studija.dto';
import { map, catchError } from 'rxjs/operators';
import { CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';

export const GODINA_STUDIJA_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: GodinaStudijaFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.NASTAVA,
  path: PATH.GODINA_STUDIJA,
  microservicePort: MICROSERVICE_PORT.NASTAVA,
  wsPath: WS_PATH.GODINA_STUDIJA,
};
@Injectable({ providedIn: 'root' })
export class GodinaStudijaService extends CrudService<GodinaStudija, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      GodinaStudijaFactory.getInstance(),
      MICROSERVICE_NAME.NASTAVA,
      PATH.GODINA_STUDIJA,
      MICROSERVICE_PORT.NASTAVA,
      WS_PATH.GODINA_STUDIJA
    );
    this._findAll();
  }

  public findAllByStudijskiProgramId = (
    id: number
  ): Observable<GodinaStudija[]> => {
    // console.log(this._url + '/studijski_program/' + id);
    return this._httpClient
      .get<GodinaStudijaDTO[]>(this._url + '/studijski_program/' + id)
      .pipe(
        map((entiteti) =>
          entiteti.map((e) => <GodinaStudija>this._factory.build(e))
        ),
        catchError(this._onHandleError)
      );
  };
}
