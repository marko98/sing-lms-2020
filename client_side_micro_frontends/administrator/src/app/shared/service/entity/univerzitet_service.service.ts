import { CrudService, CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';
import {
  PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
  WS_PATH,
} from '../../const';
import { Injectable } from '@angular/core';
import { Univerzitet } from '../../model/entity/univerzitet.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { UniverzitetFactory } from '../../model/patterns/creational/factory_method/univerzitet.factory';

export const UNIVERZITET_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: UniverzitetFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.FAKULTET,
  path: PATH.UNIVERZITET,
  microservicePort: MICROSERVICE_PORT.FAKULTET,
  wsPath: WS_PATH.UNIVERZITET,
};

@Injectable({ providedIn: 'root' })
export class UniverzitetService extends CrudService<Univerzitet, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      UniverzitetFactory.getInstance(),
      MICROSERVICE_NAME.FAKULTET,
      PATH.UNIVERZITET,
      MICROSERVICE_PORT.FAKULTET,
      WS_PATH.UNIVERZITET
    );

    this._findAll();
  }
}
