import { CrudService } from '../crud.service';
import {
  PATH,
  WS_PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
} from '../../const';
import { Injectable } from '@angular/core';
import { DrugiOblikNastave } from '../../model/entity/drugi_oblik_nastave.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { DrugiOblikNastaveFactory } from '../../model/patterns/creational/factory_method/drugi_oblik_nastave.factory';
import { CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';

export const DRUGI_OBLIK_NASTAVE_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: DrugiOblikNastaveFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.NASTAVA,
  path: PATH.DRUGI_OBLIK_NASTAVE,
  microservicePort: MICROSERVICE_PORT.NASTAVA,
  wsPath: WS_PATH.DRUGI_OBLIK_NASTAVE,
};
@Injectable({ providedIn: 'root' })
export class DrugiOblikNastaveService extends CrudService<
  DrugiOblikNastave,
  number
> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      DrugiOblikNastaveFactory.getInstance(),
      MICROSERVICE_NAME.NASTAVA,
      PATH.DRUGI_OBLIK_NASTAVE,
      MICROSERVICE_PORT.NASTAVA,
      WS_PATH.DRUGI_OBLIK_NASTAVE
    );
    this._findAll();
  }
}
