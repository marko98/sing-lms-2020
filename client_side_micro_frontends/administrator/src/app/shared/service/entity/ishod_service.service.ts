import { CrudService, CRUD_SERVICE_FOR_MODEL_INTERFACE } from '../crud.service';
import {
  PATH,
  WS_PATH,
  MICROSERVICE_NAME,
  MICROSERVICE_PORT,
} from '../../const';
import { Injectable } from '@angular/core';
import { Ishod } from '../../model/entity/ishod.model';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../ui.service';
import { IshodFactory } from '../../model/patterns/creational/factory_method/ishod.factory';
import { Observable } from 'rxjs';
import { IshodDTO } from '../../model/dto/ishod.dto';
import { map, catchError } from 'rxjs/operators';

export const ISHOD_SERVICE_FOR_MODEL_INTERFACE: CRUD_SERVICE_FOR_MODEL_INTERFACE = {
  factory: IshodFactory.getInstance(),
  microserviceName: MICROSERVICE_NAME.OBAVESTENJE,
  path: PATH.ISHOD,
  microservicePort: MICROSERVICE_PORT.OBAVESTENJE,
  wsPath: WS_PATH.ISHOD,
};

@Injectable({ providedIn: 'root' })
export class IshodService extends CrudService<Ishod, number> {
  constructor(
    protected _httpClient: HttpClient,
    protected _uiService: UIService
  ) {
    super(
      _httpClient,
      _uiService,
      IshodFactory.getInstance(),
      MICROSERVICE_NAME.OBAVESTENJE,
      PATH.ISHOD,
      MICROSERVICE_PORT.OBAVESTENJE,
      WS_PATH.ISHOD
    );
    this._findAll();
  }

  public findAllByRealizacijaPredmetaId = (id: number): Observable<Ishod[]> => {
    // console.log(this._url + '/realizacija_predmeta/' + id);
    return this._httpClient
      .get<IshodDTO[]>(this._url + '/realizacija_predmeta/' + id)
      .pipe(
        map((entiteti) => entiteti.map((e) => <Ishod>this._factory.build(e))),
        catchError(this._onHandleError)
      );
  };
}
