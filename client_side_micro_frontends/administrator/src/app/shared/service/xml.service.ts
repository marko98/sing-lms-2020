import { Injectable } from '@angular/core';
import { FileSaverService } from './file-saver.service';
import * as jsonxml from 'jsontoxml';

/**
 * npm i jsontoxml
 */

export declare interface XmlExportable {
  convertToJSON(
    shouldHaveOwnKey: boolean,
    rootKeyFromOutsideJSON?: string
  ): object;
}

@Injectable({ providedIn: 'root' })
export class XmlService {
  constructor(private _fileSaverService: FileSaverService) {}

  public onExportXml = (
    fileName: string,
    objects: XmlExportable[],
    rootTagName: string = 'root'
  ): void => {
    let data = {};
    data[rootTagName] = objects.map((o) => o.convertToJSON(false));
    let xml = jsonxml(data, { xmlHeader: true, prettyPrint: true });

    this._fileSaverService.onSaveFile(xml, 'text/xml', fileName);
  };
}
