import { Titula } from './titula.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';
import { CrudServiceForModel } from '../../service/crud.service-for-model';
import { Subject } from 'rxjs';
import {
  Form,
  FormRow,
  ROW_ITEM_TYPE,
  FormFieldInputNumberInterface,
  FORM_FIELD_INPUT_TYPE,
  SelectInterface,
  VALIDATOR_NAMES,
} from 'my-angular-form';
import { Validators } from '@angular/forms';
import { HeaderValue, Table } from 'my-angular-table';
import { XmlExportable } from '../../service/xml.service';

export class NaucnaOblast
  implements Entitet<NaucnaOblast, number>, XmlExportable {
  private id: number;

  private naziv: string;

  private stanje: StanjeModela;

  private version: number;

  private titule: Titula[];

  private _formSubject: Subject<Form> = new Subject();

  public getFormSubject = (): Subject<Form> => {
    return this._formSubject;
  };

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getNaziv = (): string => {
    return this.naziv;
  };

  public setNaziv = (naziv: string): void => {
    this.naziv = naziv;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getTitule = (): Titula[] => {
    return this.titule;
  };

  public setTitule = (titule: Titula[]): void => {
    this.titule = titule;
  };

  public constructor() {
    this.titule = [];
  }

  public convertToJSON = (
    shouldHaveOwnKey: boolean = true,
    rootKeyFromOutsideJSON?: string
  ): object => {
    // private id: number;+

    // private naziv: string;+

    // private stanje: StanjeModela;+

    // private version: number;-

    // private titule: Titula[];+

    if (shouldHaveOwnKey) {
      return {
        id: this.id,
        stanje: this.stanje,
        naziv: this.naziv,
        titule: this.titule.map((t) =>
          rootKeyFromOutsideJSON && rootKeyFromOutsideJSON === 'titula'
            ? ''
            : t
            ? t.convertToJSON(false, 'naucnaOblast')
            : ''
        ),
      };
    } else {
      return {
        naucnaOblast: {
          id: this.id,
          stanje: this.stanje,
          naziv: this.naziv,
          titule: this.titule.map((t) =>
            rootKeyFromOutsideJSON && rootKeyFromOutsideJSON === 'titula'
              ? ''
              : t
              ? t.convertToJSON(false, 'naucnaOblast')
              : ''
          ),
        },
      };
    }
  };
}
