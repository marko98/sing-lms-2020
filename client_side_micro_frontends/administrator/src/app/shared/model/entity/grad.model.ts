import { Drzava } from './drzava.model';
import { Adresa } from './adresa.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';

export class Grad implements Entitet<Grad, number> {
  private id: number;

  private naziv: string;

  private stanje: StanjeModela;

  private version: number;

  private drzava: Drzava;

  private adrese: Adresa[];

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getNaziv = (): string => {
    return this.naziv;
  };

  public setNaziv = (naziv: string): void => {
    this.naziv = naziv;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getDrzava = (): Drzava => {
    return this.drzava;
  };

  public setDrzava = (drzava: Drzava): void => {
    this.drzava = drzava;
  };

  public getAdrese = (): Adresa[] => {
    return this.adrese;
  };

  public setAdrese = (adrese: Adresa[]): void => {
    this.adrese = adrese;
  };

  public constructor() {
    this.adrese = [];
  }
}
