import { NaucnaOblast } from './naucna_oblast.model';
import { Nastavnik } from './nastavnik.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';
import { TipTitule } from '../enum/tip_titule.enum';
import { CrudServiceForModel } from '../../service/crud.service-for-model';
import { Subject } from 'rxjs';
import {
  Form,
  FormRow,
  ROW_ITEM_TYPE,
  FormFieldInputNumberInterface,
  FORM_FIELD_INPUT_TYPE,
  SelectInterface,
  VALIDATOR_NAMES,
} from 'my-angular-form';
import { Validators } from '@angular/forms';
import { HeaderValue, Table } from 'my-angular-table';
import { XmlExportable } from '../../service/xml.service';

export class Titula implements Entitet<Titula, number>, XmlExportable {
  private id: number;

  private datumIzbora: Date;

  private datumPrestanka: Date;

  private stanje: StanjeModela;

  private version: number;

  private tip: TipTitule;

  private naucnaOblast: NaucnaOblast;

  private nastavnik: Nastavnik;

  private _formSubject: Subject<Form> = new Subject();

  public getFormSubject = (): Subject<Form> => {
    return this._formSubject;
  };

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getDatumIzbora = (): Date => {
    return this.datumIzbora;
  };

  public setDatumIzbora = (datumIzbora: Date): void => {
    this.datumIzbora = datumIzbora;
  };

  public getDatumPrestanka = (): Date => {
    return this.datumPrestanka;
  };

  public setDatumPrestanka = (datumPrestanka: Date): void => {
    this.datumPrestanka = datumPrestanka;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getTip = (): TipTitule => {
    return this.tip;
  };

  public setTip = (tip: TipTitule): void => {
    this.tip = tip;
  };

  public getNaucnaOblast = (): NaucnaOblast => {
    return this.naucnaOblast;
  };

  public setNaucnaOblast = (naucnaOblast: NaucnaOblast): void => {
    this.naucnaOblast = naucnaOblast;
  };

  public getNastavnik = (): Nastavnik => {
    return this.nastavnik;
  };

  public setNastavnik = (nastavnik: Nastavnik): void => {
    this.nastavnik = nastavnik;
  };

  public constructor() {}

  public convertToJSON = (
    shouldHaveOwnKey: boolean = true,
    rootKeyFromOutsideJSON?: string
  ): object => {
    // private id: number;+

    // private datumIzbora: Date;+

    // private datumPrestanka: Date;+

    // private stanje: StanjeModela;+

    // private version: number;-

    // private tip: TipTitule;+

    // private naucnaOblast: NaucnaOblast;+

    // private nastavnik: Nastavnik;+

    if (shouldHaveOwnKey) {
      return {
        id: this.id,
        stanje: this.stanje,
        datumIzbora: this.datumIzbora.toLocaleDateString(),
        datumPrestanka: this.datumPrestanka.toLocaleDateString(),
        tip: this.tip,
        nastavnik:
          rootKeyFromOutsideJSON && rootKeyFromOutsideJSON === 'nastavnik'
            ? ''
            : this.nastavnik
            ? this.nastavnik.convertToJSON(true, 'titula')
            : '',
        naucnaOblast:
          rootKeyFromOutsideJSON && rootKeyFromOutsideJSON === 'naucnaOblast'
            ? ''
            : this.naucnaOblast
            ? this.naucnaOblast.convertToJSON(true, 'titula')
            : '',
      };
    } else {
      return {
        titula: {
          id: this.id,
          stanje: this.stanje,
          datumIzbora: this.datumIzbora.toLocaleDateString(),
          datumPrestanka: this.datumPrestanka.toLocaleDateString(),
          tip: this.tip,
          nastavnik:
            rootKeyFromOutsideJSON && rootKeyFromOutsideJSON === 'nastavnik'
              ? ''
              : this.nastavnik
              ? this.nastavnik.convertToJSON(true, 'titula')
              : '',
          naucnaOblast:
            rootKeyFromOutsideJSON && rootKeyFromOutsideJSON === 'naucnaOblast'
              ? ''
              : this.naucnaOblast
              ? this.naucnaOblast.convertToJSON(true, 'titula')
              : '',
        },
      };
    }
  };
}
