import { DrugiOblikNastave } from './drugi_oblik_nastave.model';
import { NastavnikNaRealizaciji } from './nastavnik_na_realizaciji.model';
import { PohadjanjePredmeta } from './pohadjanje_predmeta.model';
import { EvaluacijaZnanja } from './evaluacija_znanja.model';
import { ObrazovniCilj } from './obrazovni_cilj.model';
import { TerminNastave } from './termin_nastave.model';
import { Ishod } from './ishod.model';
import { IstrazivackiRad } from './istrazivacki_rad.model';
import { Predmet } from './predmet.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';
import { Identifikacija } from '../interface/identifikacija.interface';
import { CrudServiceForModel } from '../../service/crud.service-for-model';
import { Subject } from 'rxjs';
import {
  Form,
  FormRow,
  ROW_ITEM_TYPE,
  FormFieldInputNumberInterface,
  FORM_FIELD_INPUT_TYPE,
  SelectInterface,
  VALIDATOR_NAMES,
} from 'my-angular-form';
import { Validators } from '@angular/forms';
import { HeaderValue, Table } from 'my-angular-table';
import { XmlExportable } from '../../service/xml.service';

export class RealizacijaPredmeta
  implements Entitet<RealizacijaPredmeta, number>, XmlExportable {
  private id: number;

  private stanje: StanjeModela;

  private version: number;

  private drugiObliciNastave: DrugiOblikNastave[];

  private nastavniciNaRealizaciji: NastavnikNaRealizaciji[];

  private pohadjanjaPredmeta: PohadjanjePredmeta[];

  private evaluacijeZnanja: EvaluacijaZnanja[];

  private obrazovniCiljevi: ObrazovniCilj[];

  private terminiNastave: TerminNastave[];

  private ishodi: Ishod[];

  private istrazivackiRadovi: IstrazivackiRad[];

  private predmet: Predmet;

  private _formSubject: Subject<Form> = new Subject();

  public getFormSubject = (): Subject<Form> => {
    return this._formSubject;
  };

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getDrugiObliciNastave = (): DrugiOblikNastave[] => {
    return this.drugiObliciNastave;
  };

  public setDrugiObliciNastave = (
    drugiObliciNastave: DrugiOblikNastave[]
  ): void => {
    this.drugiObliciNastave = drugiObliciNastave;
  };

  public getNastavniciNaRealizaciji = (): NastavnikNaRealizaciji[] => {
    return this.nastavniciNaRealizaciji;
  };

  public setNastavniciNaRealizaciji = (
    nastavniciNaRealizaciji: NastavnikNaRealizaciji[]
  ): void => {
    this.nastavniciNaRealizaciji = nastavniciNaRealizaciji;
  };

  public getPohadjanjaPredmeta = (): PohadjanjePredmeta[] => {
    return this.pohadjanjaPredmeta;
  };

  public setPohadjanjaPredmeta = (
    pohadjanjaPredmeta: PohadjanjePredmeta[]
  ): void => {
    this.pohadjanjaPredmeta = pohadjanjaPredmeta;
  };

  public getEvaluacijeZnanja = (): EvaluacijaZnanja[] => {
    return this.evaluacijeZnanja;
  };

  public setEvaluacijeZnanja = (evaluacijeZnanja: EvaluacijaZnanja[]): void => {
    this.evaluacijeZnanja = evaluacijeZnanja;
  };

  public getObrazovniCiljevi = (): ObrazovniCilj[] => {
    return this.obrazovniCiljevi;
  };

  public setObrazovniCiljevi = (obrazovniCiljevi: ObrazovniCilj[]): void => {
    this.obrazovniCiljevi = obrazovniCiljevi;
  };

  public getTerminiNastave = (): TerminNastave[] => {
    return this.terminiNastave;
  };

  public setTerminiNastave = (terminiNastave: TerminNastave[]): void => {
    this.terminiNastave = terminiNastave;
  };

  public getIshodi = (): Ishod[] => {
    return this.ishodi;
  };

  public setIshodi = (ishodi: Ishod[]): void => {
    this.ishodi = ishodi;
  };

  public getIstrazivackiRadovi = (): IstrazivackiRad[] => {
    return this.istrazivackiRadovi;
  };

  public setIstrazivackiRadovi = (
    istrazivackiRadovi: IstrazivackiRad[]
  ): void => {
    this.istrazivackiRadovi = istrazivackiRadovi;
  };

  public getPredmet = (): Predmet => {
    return this.predmet;
  };

  public setPredmet = (predmet: Predmet): void => {
    this.predmet = predmet;
  };

  public constructor() {
    this.drugiObliciNastave = [];
    this.nastavniciNaRealizaciji = [];
    this.pohadjanjaPredmeta = [];
    this.evaluacijeZnanja = [];
    this.obrazovniCiljevi = [];
    this.terminiNastave = [];
    this.ishodi = [];
    this.istrazivackiRadovi = [];
  }

  public getDrugiOblikNastave(
    identifikacija: Identifikacija<number>
  ): DrugiOblikNastave {
    throw new Error('Not Implmented');
  }

  public getNastavnikNaRealizaciji(
    identifikacija: Identifikacija<number>
  ): NastavnikNaRealizaciji {
    throw new Error('Not Implmented');
  }

  public getPohadjanjePredmeta(
    identifikacija: Identifikacija<number>
  ): PohadjanjePredmeta {
    throw new Error('Not Implmented');
  }

  public getEvaluacijaZnanja(
    identifikacija: Identifikacija<number>
  ): EvaluacijaZnanja {
    throw new Error('Not Implmented');
  }

  public getObrazovniCilj(
    identifikacija: Identifikacija<number>
  ): ObrazovniCilj {
    throw new Error('Not Implmented');
  }

  public getTerminNastave(
    identifikacija: Identifikacija<number>
  ): TerminNastave {
    throw new Error('Not Implmented');
  }

  public getIshod(identifikacija: Identifikacija<number>): Ishod {
    throw new Error('Not Implmented');
  }

  public getIstrazivackiRad(
    identifikacija: Identifikacija<number>
  ): IstrazivackiRad {
    throw new Error('Not Implmented');
  }

  public convertToJSON = (
    shouldHaveOwnKey: boolean = true,
    rootKeyFromOutsideJSON?: string
  ): object => {
    // private id: number;+

    // private stanje: StanjeModela;+

    // private version: number;-

    // private drugiObliciNastave: DrugiOblikNastave[];-

    // private nastavniciNaRealizaciji: NastavnikNaRealizaciji[];-

    // private pohadjanjaPredmeta: PohadjanjePredmeta[];-

    // private evaluacijeZnanja: EvaluacijaZnanja[];-

    // private obrazovniCiljevi: ObrazovniCilj[];-

    // private terminiNastave: TerminNastave[];-

    // private ishodi: Ishod[];-

    // private istrazivackiRadovi: IstrazivackiRad[];-

    // private predmet: Predmet;

    if (shouldHaveOwnKey) {
      return {
        id: this.id,
        stanje: this.stanje,
        predmet:
          rootKeyFromOutsideJSON && rootKeyFromOutsideJSON === 'predmet'
            ? ''
            : this.predmet
            ? this.predmet.convertToJSON(true, 'realizacijaPredmeta')
            : '',
      };
    } else {
      return {
        realizacijaPredmeta: {
          id: this.id,
          stanje: this.stanje,
          predmet:
            rootKeyFromOutsideJSON && rootKeyFromOutsideJSON === 'predmet'
              ? ''
              : this.predmet
              ? this.predmet.convertToJSON(true, 'realizacijaPredmeta')
              : '',
        },
      };
    }
  };
}
