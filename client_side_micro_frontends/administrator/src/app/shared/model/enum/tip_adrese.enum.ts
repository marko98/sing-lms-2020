export enum TipAdrese {
  PRIMARNA = 'PRIMARNA',
  SEKUNDARNA = 'SEKUNDARNA',
  OSTALO = 'OSTALO',
}
