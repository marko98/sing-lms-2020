import { DrugiOblikNastave } from '../../../entity/drugi_oblik_nastave.model';
import { DrugiOblikNastaveDTO } from '../../../dto/drugi_oblik_nastave.dto';
import { RealizacijaPredmetaFactory } from './realizacija_predmeta.factory';
import { Factory } from './factory.model';

export class DrugiOblikNastaveFactory extends Factory {
  public static _instance: DrugiOblikNastaveFactory;
  public static getInstance = (): DrugiOblikNastaveFactory => {
    if (!DrugiOblikNastaveFactory._instance)
      DrugiOblikNastaveFactory._instance = new DrugiOblikNastaveFactory();
    return <DrugiOblikNastaveFactory>DrugiOblikNastaveFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: DrugiOblikNastaveDTO): DrugiOblikNastave {
    let drugiOblikNastave = new DrugiOblikNastave();
    drugiOblikNastave.setId(dto.id);
    drugiOblikNastave.setDatum(new Date(dto.datum));
    drugiOblikNastave.setStanje(dto.stanje);
    drugiOblikNastave.setTipDrugogOblikaNastave(dto.tipDrugogOblikaNastave);
    drugiOblikNastave.setVersion(dto.version);

    if (dto.realizacijaPredmetaDTO)
      drugiOblikNastave.setRealizacijaPredmeta(
        RealizacijaPredmetaFactory.getInstance().build(
          dto.realizacijaPredmetaDTO
        )
      );

    return drugiOblikNastave;
  }
}
