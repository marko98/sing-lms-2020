import { Autor } from '../../../entity/autor.model';
import { AutorDTO } from '../../../dto/autor.dto';
import { AutorNastavniMaterijalFactory } from './autor_nastavni_materijal.factory';
import { Factory } from './factory.model';

export class AutorFactory extends Factory {
  public static _instance: AutorFactory;

  public static getInstance = (): AutorFactory => {
    if (!AutorFactory._instance) AutorFactory._instance = new AutorFactory();
    return <AutorFactory>AutorFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: AutorDTO): Autor {
    let autor = new Autor();
    autor.setId(dto.id);
    autor.setIme(dto.ime);
    autor.setPrezime(dto.prezime);
    autor.setStanje(dto.stanje);
    autor.setVersion(dto.version);

    if (dto.autorNastavniMaterijaliDTO)
      autor.setAutorNastavniMaterijali(
        dto.autorNastavniMaterijaliDTO.map((t) =>
          AutorNastavniMaterijalFactory.getInstance().build(t)
        )
      );

    return autor;
  }
}
