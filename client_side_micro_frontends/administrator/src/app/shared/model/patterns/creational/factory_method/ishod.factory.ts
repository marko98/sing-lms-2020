import { Ishod } from '../../../entity/ishod.model';
import { IshodDTO } from '../../../dto/ishod.dto';
import { EvaluacijaZnanjaFactory } from './evaluacija_znanja.factory';
import { RealizacijaPredmetaFactory } from './realizacija_predmeta.factory';
import { NastavniMaterijalFactory } from './nastavni_materijal.factory';
import { ObrazovniCiljFactory } from './obrazovni_cilj.factory';
import { Factory } from './factory.model';

export class IshodFactory extends Factory {
  public static _instance: IshodFactory;
  public static getInstance = (): IshodFactory => {
    if (!IshodFactory._instance) IshodFactory._instance = new IshodFactory();
    return <IshodFactory>IshodFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: IshodDTO): Ishod {
    let ishod = new Ishod();
    ishod.setId(dto.id);
    ishod.setNaslov(dto.naslov);
    ishod.setOpis(dto.opis);
    ishod.setPutanjaZaSliku(dto.putanjaZaSliku);
    ishod.setStanje(dto.stanje);
    ishod.setVersion(dto.version);

    if (dto.evaluacijaZnanjaDTO)
      ishod.setEvaluacijaZnanja(
        EvaluacijaZnanjaFactory.getInstance().build(dto.evaluacijaZnanjaDTO)
      );
    if (dto.realizacijaPredmetaDTO)
      ishod.setRealizacijaPredmeta(
        RealizacijaPredmetaFactory.getInstance().build(
          dto.realizacijaPredmetaDTO
        )
      );

    if (dto.nastavniMaterijaliDTO)
      ishod.setNastavniMaterijali(
        dto.nastavniMaterijaliDTO.map((t) =>
          NastavniMaterijalFactory.getInstance().build(t)
        )
      );

    if (dto.obrazovniCiljeviDTO)
      ishod.setObrazovniCiljevi(
        dto.obrazovniCiljeviDTO.map((t) =>
          ObrazovniCiljFactory.getInstance().build(t)
        )
      );

    return ishod;
  }
}
