import { Titula } from '../../../entity/titula.model';
import { TitulaDTO } from '../../../dto/titula.dto';
import { NastavnikFactory } from './nastavnik.factory';
import { NaucnaOblastFactory } from './naucna_oblast.factory';
import { Factory } from './factory.model';

export class TitulaFactory extends Factory {
  public static _instance: TitulaFactory;
  public static getInstance = (): TitulaFactory => {
    if (!TitulaFactory._instance) TitulaFactory._instance = new TitulaFactory();
    return <TitulaFactory>TitulaFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: TitulaDTO): Titula {
    let titula = new Titula();
    titula.setId(dto.id);
    titula.setDatumIzbora(new Date(dto.datumIzbora));
    titula.setDatumPrestanka(new Date(dto.datumPrestanka));
    titula.setStanje(dto.stanje);
    titula.setTip(dto.tip);
    titula.setVersion(dto.version);

    if (dto.nastavnikDTO)
      titula.setNastavnik(
        NastavnikFactory.getInstance().build(dto.nastavnikDTO)
      );
    if (dto.naucnaOblastDTO)
      titula.setNaucnaOblast(
        NaucnaOblastFactory.getInstance().build(dto.naucnaOblastDTO)
      );

    return titula;
  }
}
