import { IstrazivackiRadStudentNaStudiji } from '../../../entity/istrazivacki_rad_student_na_studiji.model';
import { IstrazivackiRadStudentNaStudijiDTO } from '../../../dto/istrazivacki_rad_student_na_studiji.dto';
import { IstrazivackiRadFactory } from './istrazivacki_rad.factory';
import { StudentNaStudijiFactory } from './student_na_studiji.factory';
import { Factory } from './factory.model';

export class IstrazivackiRadStudentNaStudijiFactory extends Factory {
  public static _instance: IstrazivackiRadStudentNaStudijiFactory;
  public static getInstance = (): IstrazivackiRadStudentNaStudijiFactory => {
    if (!IstrazivackiRadStudentNaStudijiFactory._instance)
      IstrazivackiRadStudentNaStudijiFactory._instance = new IstrazivackiRadStudentNaStudijiFactory();
    return <IstrazivackiRadStudentNaStudijiFactory>(
      IstrazivackiRadStudentNaStudijiFactory._instance
    );
  };

  private constructor() {
    super();
  }

  build(
    dto: IstrazivackiRadStudentNaStudijiDTO
  ): IstrazivackiRadStudentNaStudiji {
    let istrazivackiRadStudentNaStudiji = new IstrazivackiRadStudentNaStudiji();
    istrazivackiRadStudentNaStudiji.setId(dto.id);
    istrazivackiRadStudentNaStudiji.setDatum(new Date(dto.datum));
    istrazivackiRadStudentNaStudiji.setStanje(dto.stanje);
    istrazivackiRadStudentNaStudiji.setVersion(dto.version);

    if (dto.istrazivackiRadDTO)
      istrazivackiRadStudentNaStudiji.setIstrazivackiRad(
        IstrazivackiRadFactory.getInstance().build(dto.istrazivackiRadDTO)
      );
    if (dto.studentNaStudijiDTO)
      istrazivackiRadStudentNaStudiji.setStudentNaStudiji(
        StudentNaStudijiFactory.getInstance().build(dto.studentNaStudijiDTO)
      );

    return istrazivackiRadStudentNaStudiji;
  }
}
