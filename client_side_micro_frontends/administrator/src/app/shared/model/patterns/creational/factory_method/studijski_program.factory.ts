import { StudijskiProgram } from '../../../entity/studijski_program.model';
import { StudijskiProgramDTO } from '../../../dto/studijski_program.dto';
import { FakultetFactory } from './fakultet.factory';
import { NastavnikFactory } from './nastavnik.factory';
import { GodinaStudijaFactory } from './godina_studija.factory';
import { Factory } from './factory.model';

export class StudijskiProgramFactory extends Factory {
  public static _instance: StudijskiProgramFactory;
  public static getInstance = (): StudijskiProgramFactory => {
    if (!StudijskiProgramFactory._instance)
      StudijskiProgramFactory._instance = new StudijskiProgramFactory();
    return <StudijskiProgramFactory>StudijskiProgramFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: StudijskiProgramDTO): StudijskiProgram {
    let studijskiProgram = new StudijskiProgram();
    studijskiProgram.setId(dto.id);
    studijskiProgram.setBrojGodinaStudija(dto.brojGodinaStudija);
    studijskiProgram.setNaziv(dto.naziv);
    studijskiProgram.setStanje(dto.stanje);
    studijskiProgram.setVersion(dto.version);
    studijskiProgram.setOpis(dto.opis);

    if (dto.fakultetDTO)
      studijskiProgram.setFakultet(
        FakultetFactory.getInstance().build(dto.fakultetDTO)
      );
    if (dto.rukovodilacDTO)
      studijskiProgram.setRukovodilac(
        NastavnikFactory.getInstance().build(dto.rukovodilacDTO)
      );

    if (dto.godineStudijaDTO)
      studijskiProgram.setGodineStudija(
        dto.godineStudijaDTO.map((t) =>
          GodinaStudijaFactory.getInstance().build(t)
        )
      );

    return studijskiProgram;
  }
}
