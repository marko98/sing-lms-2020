import { EvaluacijaZnanjaNacinEvaluacije } from '../../../entity/evaluacija_znanja_nacin_evaluacije.model';
import { EvaluacijaZnanjaNacinEvaluacijeDTO } from '../../../dto/evaluacija_znanja_nacin_evaluacije.dto';
import { EvaluacijaZnanjaFactory } from './evaluacija_znanja.factory';
import { Factory } from './factory.model';

export class EvaluacijaZnanjaNacinEvaluacijeFactory extends Factory {
  public static _instance: EvaluacijaZnanjaNacinEvaluacijeFactory;
  public static getInstance = (): EvaluacijaZnanjaNacinEvaluacijeFactory => {
    if (!EvaluacijaZnanjaNacinEvaluacijeFactory._instance)
      EvaluacijaZnanjaNacinEvaluacijeFactory._instance = new EvaluacijaZnanjaNacinEvaluacijeFactory();
    return <EvaluacijaZnanjaNacinEvaluacijeFactory>(
      EvaluacijaZnanjaNacinEvaluacijeFactory._instance
    );
  };

  private constructor() {
    super();
  }

  build(
    dto: EvaluacijaZnanjaNacinEvaluacijeDTO
  ): EvaluacijaZnanjaNacinEvaluacije {
    let evaluacijaZnanjaNacinEvaluacije = new EvaluacijaZnanjaNacinEvaluacije();
    evaluacijaZnanjaNacinEvaluacije.setId(dto.id);
    evaluacijaZnanjaNacinEvaluacije.setNacinEvaluacije(dto.nacinEvaluacije);
    evaluacijaZnanjaNacinEvaluacije.setStanje(dto.stanje);
    evaluacijaZnanjaNacinEvaluacije.setVersion(dto.version);

    if (dto.evaluacijaZnanjaDTO)
      evaluacijaZnanjaNacinEvaluacije.setEvaluacijaZnanja(
        EvaluacijaZnanjaFactory.getInstance().build(dto.evaluacijaZnanjaDTO)
      );

    return evaluacijaZnanjaNacinEvaluacije;
  }
}
