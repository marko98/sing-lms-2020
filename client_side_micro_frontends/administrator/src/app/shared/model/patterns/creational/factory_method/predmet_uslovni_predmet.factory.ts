import { PredmetUslovniPredmet } from '../../../entity/predmet_uslovni_predmet.model';
import { PredmetUslovniPredmetDTO } from '../../../dto/predmet_uslovni_predmet.dto';
import { PredmetFactory } from './predmet.factory';
import { Factory } from './factory.model';

export class PredmetUslovniPredmetFactory extends Factory {
  public static _instance: PredmetUslovniPredmetFactory;
  public static getInstance = (): PredmetUslovniPredmetFactory => {
    if (!PredmetUslovniPredmetFactory._instance)
      PredmetUslovniPredmetFactory._instance = new PredmetUslovniPredmetFactory();
    return <PredmetUslovniPredmetFactory>PredmetUslovniPredmetFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: PredmetUslovniPredmetDTO): PredmetUslovniPredmet {
    let predmetUslovniPredmet = new PredmetUslovniPredmet();

    predmetUslovniPredmet.setId(dto.id);
    predmetUslovniPredmet.setStanje(dto.stanje);
    predmetUslovniPredmet.setVersion(dto.version);

    if (dto.predmetDTO)
      predmetUslovniPredmet.setPredmet(
        PredmetFactory.getInstance().build(dto.predmetDTO)
      );
    if (dto.uslovDTO)
      predmetUslovniPredmet.setUslov(
        PredmetFactory.getInstance().build(dto.uslovDTO)
      );

    return predmetUslovniPredmet;
  }
}
