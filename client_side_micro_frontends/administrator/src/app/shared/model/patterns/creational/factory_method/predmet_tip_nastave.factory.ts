import { PredmetTipNastave } from '../../../entity/predmet_tip_nastave.model';
import { PredmetFactory } from './predmet.factory';
import { PredmetTipNastaveDTO } from '../../../dto/predmet_tip_nastave.dto';
import { Factory } from './factory.model';

export class PredmetTipNastaveFactory extends Factory {
  public static _instance: PredmetTipNastaveFactory;
  public static getInstance = (): PredmetTipNastaveFactory => {
    if (!PredmetTipNastaveFactory._instance)
      PredmetTipNastaveFactory._instance = new PredmetTipNastaveFactory();
    return <PredmetTipNastaveFactory>PredmetTipNastaveFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: PredmetTipNastaveDTO): PredmetTipNastave {
    let predmetTipNastave = new PredmetTipNastave();

    predmetTipNastave.setId(dto.id);
    predmetTipNastave.setStanje(dto.stanje);
    predmetTipNastave.setTipNastave(dto.tipNastave);
    predmetTipNastave.setVersion(dto.version);

    if (dto.predmetDTO)
      predmetTipNastave.setPredmet(
        PredmetFactory.getInstance().build(dto.predmetDTO)
      );

    return predmetTipNastave;
  }
}
