import { Obavestenje } from '../../../entity/obavestenje.model';
import { ObavestenjeDTO } from '../../../dto/obavestenje.dto';
import { FajlFactory } from './fajl.factory';
import { GodinaStudijaObavestenjeFactory } from './godina_studija_obavestenje.factory';
import { Factory } from './factory.model';

export class ObavestenjeFactory extends Factory {
  public static _instance: ObavestenjeFactory;
  public static getInstance = (): ObavestenjeFactory => {
    if (!ObavestenjeFactory._instance)
      ObavestenjeFactory._instance = new ObavestenjeFactory();
    return <ObavestenjeFactory>ObavestenjeFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: ObavestenjeDTO): Obavestenje {
    let obavestenje = new Obavestenje();
    obavestenje.setId(dto.id);
    obavestenje.setDatum(new Date(dto.datum));
    obavestenje.setNaslov(dto.naslov);
    obavestenje.setSadrzaj(dto.sadrzaj);
    obavestenje.setStanje(dto.stanje);
    obavestenje.setVersion(dto.version);

    if (dto.fajloviDTO)
      obavestenje.setFajlovi(
        dto.fajloviDTO.map((t) => FajlFactory.getInstance().build(t))
      );

    if (dto.godineStudijaObavestenjeDTO)
      obavestenje.setGodineStudijaObavestenje(
        dto.godineStudijaObavestenjeDTO.map((t) =>
          GodinaStudijaObavestenjeFactory.getInstance().build(t)
        )
      );

    return obavestenje;
  }
}
