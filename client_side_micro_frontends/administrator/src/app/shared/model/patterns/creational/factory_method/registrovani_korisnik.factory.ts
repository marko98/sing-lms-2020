import { RegistrovaniKorisnikDTO } from '../../../dto/registrovani_korisnik.dto';
import { StudentskiSluzbenikFactory } from './studentski_sluzbenik.factory';
import { StudentFactory } from './student.factory';
import { AdresaFactory } from './adresa.factory';
import { RegistrovaniKorisnikRolaFactory } from './registrovani_korisnik_rola.factory';
import { RegistrovaniKorisnik } from '../../../entity/registrovani_korisnik.model';
import { Factory } from './factory.model';
import { NastavnikFactory } from './nastavnik.factory';
import { AdministratorFactory } from './administrator.factory';

export class RegistrovaniKorisnikFactory extends Factory {
  public static _instance: RegistrovaniKorisnikFactory;
  public static getInstance = (): RegistrovaniKorisnikFactory => {
    if (!RegistrovaniKorisnikFactory._instance)
      RegistrovaniKorisnikFactory._instance = new RegistrovaniKorisnikFactory();
    return <RegistrovaniKorisnikFactory>RegistrovaniKorisnikFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: RegistrovaniKorisnikDTO): RegistrovaniKorisnik {
    let registrovaniKorisnik = new RegistrovaniKorisnik();
    registrovaniKorisnik.setId(dto.id);
    registrovaniKorisnik.setDatumRodjenja(new Date(dto.datumRodjenja));
    registrovaniKorisnik.setEmail(dto.email);
    registrovaniKorisnik.setIme(dto.ime);
    registrovaniKorisnik.setJmbg(dto.jmbg);
    registrovaniKorisnik.setKorisnickoIme(dto.korisnickoIme);
    registrovaniKorisnik.setLozinka(dto.lozinka);
    registrovaniKorisnik.setPrezime(dto.prezime);
    registrovaniKorisnik.setStanje(dto.stanje);
    registrovaniKorisnik.setVersion(dto.version);

    if (dto.administratorDTO)
      registrovaniKorisnik.setAdministrator(
        AdministratorFactory.getInstance().build(dto.administratorDTO)
      );
    if (dto.nastavnikDTO)
      registrovaniKorisnik.setNastavnik(
        NastavnikFactory.getInstance().build(dto.nastavnikDTO)
      );
    if (dto.studentskiSluzbenikDTO)
      registrovaniKorisnik.setStudentskiSluzbenik(
        StudentskiSluzbenikFactory.getInstance().build(
          dto.studentskiSluzbenikDTO
        )
      );
    if (dto.studentDTO)
      registrovaniKorisnik.setStudent(
        StudentFactory.getInstance().build(dto.studentDTO)
      );

    if (dto.adreseDTO)
      registrovaniKorisnik.setAdrese(
        dto.adreseDTO.map((t) => AdresaFactory.getInstance().build(t))
      );

    if (dto.registrovaniKorisnikRoleDTO)
      registrovaniKorisnik.setRegistrovaniKorisnikRole(
        dto.registrovaniKorisnikRoleDTO.map((t) =>
          RegistrovaniKorisnikRolaFactory.getInstance().build(t)
        )
      );

    return registrovaniKorisnik;
  }
}
