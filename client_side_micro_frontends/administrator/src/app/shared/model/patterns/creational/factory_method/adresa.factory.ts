import { Adresa } from '../../../entity/adresa.model';
import { AdresaDTO } from '../../../dto/adresa.dto';
import { GradFactory } from './grad.factory';
import { OdeljenjeFactory } from './odeljenje.factory';
import { RegistrovaniKorisnikFactory } from './registrovani_korisnik.factory';
import { UniverzitetFactory } from './univerzitet.factory';
import { Factory } from './factory.model';

export class AdresaFactory extends Factory {
  public static _instance: AdresaFactory;

  public static getInstance = (): AdresaFactory => {
    if (!AdresaFactory._instance) AdresaFactory._instance = new AdresaFactory();
    return <AdresaFactory>AdresaFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: AdresaDTO): Adresa {
    let adresa = new Adresa();
    adresa.setId(dto.id);
    adresa.setBroj(dto.broj);
    adresa.setStanje(dto.stanje);
    adresa.setTip(dto.tip);
    adresa.setUlica(dto.ulica);
    adresa.setVersion(dto.version);

    if (dto.gradDTO)
      adresa.setGrad(GradFactory.getInstance().build(dto.gradDTO));
    if (dto.odeljenjeDTO)
      adresa.setOdeljenje(
        OdeljenjeFactory.getInstance().build(dto.odeljenjeDTO)
      );
    if (dto.registrovaniKorisnikDTO)
      adresa.setRegistrovaniKorisnik(
        RegistrovaniKorisnikFactory.getInstance().build(
          dto.registrovaniKorisnikDTO
        )
      );
    if (dto.univerzitetDTO)
      adresa.setUniverzitet(
        UniverzitetFactory.getInstance().build(dto.univerzitetDTO)
      );

    return adresa;
  }
}
