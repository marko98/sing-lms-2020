import { GodinaStudija } from '../../../entity/godina_studija.model';
import { GodinaStudijaDTO } from '../../../dto/godina_studija.dto';
import { StudijskiProgramFactory } from './studijski_program.factory';
import { GodinaStudijaObavestenjeFactory } from './godina_studija_obavestenje.factory';
import { KonsultacijaFactory } from './konsultacija.factory';
import { PredmetFactory } from './predmet.factory';
import { StudentNaStudijiFactory } from './student_na_studiji.factory';
import { Factory } from './factory.model';

export class GodinaStudijaFactory extends Factory {
  public static _instance: GodinaStudijaFactory;
  public static getInstance = (): GodinaStudijaFactory => {
    if (!GodinaStudijaFactory._instance)
      GodinaStudijaFactory._instance = new GodinaStudijaFactory();
    return <GodinaStudijaFactory>GodinaStudijaFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: GodinaStudijaDTO): GodinaStudija {
    let godinaStudija = new GodinaStudija();
    godinaStudija.setId(dto.id);
    godinaStudija.setGodina(new Date(dto.godina));
    godinaStudija.setSemestar(dto.semestar);
    godinaStudija.setStanje(dto.stanje);
    godinaStudija.setVersion(dto.version);

    if (dto.studijskiProgramDTO)
      godinaStudija.setStudijskiProgram(
        StudijskiProgramFactory.getInstance().build(dto.studijskiProgramDTO)
      );

    if (dto.godinaStudijaObavestenjaDTO)
      godinaStudija.setGodinaStudijaObavestenja(
        dto.godinaStudijaObavestenjaDTO.map((t) =>
          GodinaStudijaObavestenjeFactory.getInstance().build(t)
        )
      );

    if (dto.konsultacijeDTO)
      godinaStudija.setKonsultacije(
        dto.konsultacijeDTO.map((t) =>
          KonsultacijaFactory.getInstance().build(t)
        )
      );

    if (dto.predmetiDTO)
      godinaStudija.setPredmeti(
        dto.predmetiDTO.map((t) => PredmetFactory.getInstance().build(t))
      );

    if (dto.studentiNaStudijiDTO)
      godinaStudija.setStudentiNaStudiji(
        dto.studentiNaStudijiDTO.map((t) =>
          StudentNaStudijiFactory.getInstance().build(t)
        )
      );

    return godinaStudija;
  }
}
