import { EntitetDTO } from '../../../dto/entitet.dto';
import { Entitet } from '../../../interface/entitet.interface';

export abstract class Factory {
  protected constructor() {}
  public static getInstance = (): Factory => {
    throw Error('getInstance not implmented');
  };
  abstract build(dto: EntitetDTO): Entitet<any, any>;
}
