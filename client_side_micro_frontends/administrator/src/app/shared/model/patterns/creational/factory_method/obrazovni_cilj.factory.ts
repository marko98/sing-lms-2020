import { ObrazovniCilj } from '../../../entity/obrazovni_cilj.model';
import { ObrazovniCiljDTO } from '../../../dto/obrazovni_cilj.dto';
import { IshodFactory } from './ishod.factory';
import { RealizacijaPredmetaFactory } from './realizacija_predmeta.factory';
import { Factory } from './factory.model';

export class ObrazovniCiljFactory extends Factory {
  public static _instance: ObrazovniCiljFactory;
  public static getInstance = (): ObrazovniCiljFactory => {
    if (!ObrazovniCiljFactory._instance)
      ObrazovniCiljFactory._instance = new ObrazovniCiljFactory();
    return <ObrazovniCiljFactory>ObrazovniCiljFactory._instance;
  };

  private constructor() {
    super();
  }

  build(dto: ObrazovniCiljDTO): ObrazovniCilj {
    let obrazovniCilj = new ObrazovniCilj();
    obrazovniCilj.setId(dto.id);
    obrazovniCilj.setOpis(dto.opis);
    obrazovniCilj.setStanje(dto.stanje);
    obrazovniCilj.setVersion(dto.version);

    if (dto.ishodDTO)
      obrazovniCilj.setIshod(IshodFactory.getInstance().build(dto.ishodDTO));
    if (dto.realizacijaPredmetaDTO)
      obrazovniCilj.setRealizacijaPredmeta(
        RealizacijaPredmetaFactory.getInstance().build(
          dto.realizacijaPredmetaDTO
        )
      );

    return obrazovniCilj;
  }
}
