import { EntitetDTO } from './entitet.dto';
import { GradDTO } from './grad.dto';

export declare interface DrzavaDTO extends EntitetDTO {
  naziv: string;
  gradoviDTO: GradDTO[];
}
