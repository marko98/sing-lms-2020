import { EntitetDTO } from './entitet.dto';
import { Semestar } from '../enum/semestar.enum';
import { StudijskiProgramDTO } from './studijski_program.dto';
import { StudentNaStudijiDTO } from './student_na_studiji.dto';
import { PredmetDTO } from './predmet.dto';
import { GodinaStudijaObavestenjeDTO } from './godina_studija_obavestenje.dto';
import { KonsultacijaDTO } from './konsultacija.dto';

export declare interface GodinaStudijaDTO extends EntitetDTO {
  godina: string;
  semestar: Semestar;
  studijskiProgramDTO: StudijskiProgramDTO;
  studentiNaStudijiDTO: StudentNaStudijiDTO[];
  predmetiDTO: PredmetDTO[];
  godinaStudijaObavestenjaDTO: GodinaStudijaObavestenjeDTO[];
  konsultacijeDTO: KonsultacijaDTO[];
}
