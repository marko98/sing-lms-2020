import { EntitetDTO } from './entitet.dto';
import { RegistrovaniKorisnikDTO } from './registrovani_korisnik.dto';
import { StudentNaStudijiDTO } from './student_na_studiji.dto';

export declare interface StudentDTO extends EntitetDTO {
  registrovaniKorisnikDTO: RegistrovaniKorisnikDTO;
  studijeDTO: StudentNaStudijiDTO[];
}
