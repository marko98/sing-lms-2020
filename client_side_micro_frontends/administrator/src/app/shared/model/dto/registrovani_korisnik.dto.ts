import { EntitetDTO } from './entitet.dto';
import { AdresaDTO } from './adresa.dto';
import { NastavnikDTO } from './nastavnik.dto';
import { StudentskiSluzbenikDTO } from './studentski_sluzbenik.dto';
import { AdministratorDTO } from './administrator.dto';
import { StudentDTO } from './student.dto';
import { RegistrovaniKorisnikRolaDTO } from './registrovani_korisnik_rola.dto';

export declare interface RegistrovaniKorisnikDTO extends EntitetDTO {
  korisnickoIme: string;
  lozinka: string;
  email: string;
  datumRodjenja: string;
  jmbg: string;
  ime: string;
  prezime: string;
  adreseDTO: AdresaDTO[];
  nastavnikDTO: NastavnikDTO;
  studentskiSluzbenikDTO: StudentskiSluzbenikDTO;
  administratorDTO: AdministratorDTO;
  studentDTO: StudentDTO;
  registrovaniKorisnikRoleDTO: RegistrovaniKorisnikRolaDTO[];
}
