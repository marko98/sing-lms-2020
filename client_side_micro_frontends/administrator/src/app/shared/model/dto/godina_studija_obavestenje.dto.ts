import { EntitetDTO } from './entitet.dto';
import { GodinaStudijaDTO } from './godina_studija.dto';
import { ObavestenjeDTO } from './obavestenje.dto';

export declare interface GodinaStudijaObavestenjeDTO extends EntitetDTO {
  godinaStudijaDTO: GodinaStudijaDTO;
  obavestenjeDTO: ObavestenjeDTO;
}
