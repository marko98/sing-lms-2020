import { EntitetDTO } from './entitet.dto';
import { DogadjajKalendarDTO } from './dogadjaj_kalendar.dto';

export declare interface DogadjajDTO extends EntitetDTO {
  pocetniDatum: string;
  krajnjiDatum: string;
  opis: string;
  dogadjajKalendariDTO: DogadjajKalendarDTO[];
}
