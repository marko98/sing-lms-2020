import { EntitetDTO } from './entitet.dto';
import { IshodDTO } from './ishod.dto';
import { FajlDTO } from './fajl.dto';
import { AutorNastavniMaterijalDTO } from './autor_nastavni_materijal.dto';

export declare interface NastavniMaterijalDTO extends EntitetDTO {
  naziv: string;
  datum: string;
  ishodDTO: IshodDTO;
  fajloviDTO: FajlDTO[];
  autoriNastavniMaterijalDTO: AutorNastavniMaterijalDTO[];
}
