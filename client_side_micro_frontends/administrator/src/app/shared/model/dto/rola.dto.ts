import { EntitetDTO } from './entitet.dto';
import { RegistrovaniKorisnikRolaDTO } from './registrovani_korisnik_rola.dto';

export declare interface RolaDTO extends EntitetDTO {
  naziv: string;
  registrovaniKorisniciRolaDTO: RegistrovaniKorisnikRolaDTO[];
}
