import { EntitetDTO } from './entitet.dto';
import { NastavnikDTO } from './nastavnik.dto';
import { RealizacijaPredmetaDTO } from './realizacija_predmeta.dto';
import { IstrazivackiRadStudentNaStudijiDTO } from './istrazivacki_rad_student_na_studiji.dto';

export declare interface IstrazivackiRadDTO extends EntitetDTO {
  tema: string;
  nastavnikDTO: NastavnikDTO;
  realizacijaPredmetaDTO: RealizacijaPredmetaDTO;
  istrazivackiRadStudentiNaStudijamaDTO: IstrazivackiRadStudentNaStudijiDTO[];
}
