import { EntitetDTO } from './entitet.dto';
import { TipDrugogOblikaNastave } from '../enum/tip_drugog_oblika_nastave.enum';
import { RealizacijaPredmetaDTO } from './realizacija_predmeta.dto';

export declare interface DrugiOblikNastaveDTO extends EntitetDTO {
  datum: string;
  tipDrugogOblikaNastave: TipDrugogOblikaNastave;
  realizacijaPredmetaDTO: RealizacijaPredmetaDTO;
}
