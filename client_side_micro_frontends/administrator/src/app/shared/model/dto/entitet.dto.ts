import { IdentifikacijaDTO } from './identifikacija.dto';
import { StanjeModela } from '../enum/stanje_modela.enum';

export declare interface EntitetDTO extends IdentifikacijaDTO {
  version: number;
  stanje: StanjeModela;
}
