import { StanjeModela } from '../enum/stanje_modela.enum';
import { CrudServiceForModel } from '../../service/crud.service-for-model';
import { Subject } from 'rxjs';
import {
  Form,
  FormRow,
  ROW_ITEM_TYPE,
  FormFieldInputNumberInterface,
  FORM_FIELD_INPUT_TYPE,
  SelectInterface,
  VALIDATOR_NAMES,
} from 'my-angular-form';
import { Validators } from '@angular/forms';
import { HeaderValue, Table } from 'my-angular-table';
import { Obavestenje } from '../entity/obavestenje.model';
import { Formable } from '../interface/formable.interface';
import { Tableable } from '../interface/tableable.interface';

export class ObavestenjeFormableTableable implements Formable, Tableable {
  private _formSubject: Subject<Form> = new Subject();

  public getFormSubject = (): Subject<Form> => {
    return this._formSubject;
  };

  constructor(private _entitet: Obavestenje) {}

  public getForm = (
    csfm: CrudServiceForModel,
    isFormForCreation: boolean,
    disableAllFields: boolean = false
  ): Subject<Form> => {
    let subject: Subject<Form> = new Subject();

    let form = new Form({
      showSubmitButton: !disableAllFields,
      submitButtonText: 'potvrdi',
      showCancelButton: !disableAllFields,
      cancelButtonText: 'odustani',
      showResetButton: isFormForCreation,
      resetButtonText: 'resetuj',
      submitOnlyIfFormValid: true,
    });

    if (!isFormForCreation) {
      let rowId = new FormRow();
      form.addChild(rowId);
      rowId.addChildInterface(<FormFieldInputNumberInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
        formFieldInputType: FORM_FIELD_INPUT_TYPE.NUMBER,
        controlName: 'id',
        labelName: 'Id',
        defaultValue: this._entitet.getId(),
        disabled: true,
      });

      // console.log(StanjeModela.AKTIVAN);
      // console.log(typeof StanjeModela.AKTIVAN);

      let rowStanjeModela = new FormRow();
      form.addChild(rowStanjeModela);
      rowStanjeModela.addChildInterface(<SelectInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
        controlName: 'stanje',
        labelName: 'Stanje modela',
        defaultValue: this._entitet.getStanje(),
        optionValues: [
          { value: StanjeModela.AKTIVAN, textToShow: 'AKTIVAN' },
          { value: StanjeModela.NEAKTIVAN, textToShow: 'NEAKTIVAN' },
        ],
        disabled: disableAllFields,
        validators: [
          {
            message: 'Stanje modela je obavezno',
            name: VALIDATOR_NAMES.REQUIRED,
            validatorFn: Validators.required,
          },
        ],
      });
    }

    // let registrovaniKorisnikService = csfm.getCrudServiceForModel<
    //   RegistrovaniKorisnik,
    //   number
    // >(REGISTROVANI_KORISNIK_SERVICE_FOR_MODEL_INTERFACE);

    // registrovaniKorisnikService
    //   .findAll()
    //   .pipe(take(1))
    //   .subscribe(
    //     (rks: RegistrovaniKorisnik[]) => {
    //       for (let index = 0; index < rks.length; index++) {
    //         if (
    //           rks[index].getId().toString() ===
    //           this._entitet.getRegistrovaniKorisnik()?.getId().toString()
    //         ) {
    //           this._entitet.setRegistrovaniKorisnik(rks[index]);
    //           break;
    //         }
    //       }

    //       // console.log(rks);
    //       let rowDrzava = new FormRow();
    //       form.addChild(rowDrzava);
    //       rowDrzava.addChildInterface(<SelectInterface>{
    //         type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
    //         controlName: 'registrovaniKorisnik',
    //         labelName: 'Registrovani Korisnik',
    //         defaultValue: this._entitet.getRegistrovaniKorisnik(),
    //         optionValues: rks.map((d) => {
    //           return {
    //             value: d,
    //             textToShow: d.getId() + ' ' + d.getIme() + ' ' + d.getPrezime(),
    //           };
    //         }),
    //         disabled: disableAllFields,
    //       });
    //       this._formSubject.next(form);
    //       this._formSubject.complete();
    //     },
    //     (err: HttpErrorResponse) => {
    //       this._formSubject.error(err);
    //     }
    //   );

    return subject;
  };

  public save = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    // let adminstratorService = csfm.getCrudServiceForModel<
    //   Administrator,
    //   number
    // >(ADMINISTRATOR_SERVICE_FOR_MODEL_INTERFACE);
    // adminstratorService
    //   .save(entitetDict)
    //   .pipe(take(1))
    //   .subscribe(
    //     (entitet) => {},
    //     (err: HttpErrorResponse) => {
    //       subject.error(err);
    //     },
    //     () => {
    //       subject.complete();
    //     }
    //   );

    return subject;
  };

  public update = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    // let adminstratorService = csfm.getCrudServiceForModel<
    //   Administrator,
    //   number
    // >(ADMINISTRATOR_SERVICE_FOR_MODEL_INTERFACE);
    // adminstratorService
    //   .update(entitetDict)
    //   .pipe(take(1))
    //   .subscribe(
    //     (entitet) => {},
    //     (err: HttpErrorResponse) => {
    //       subject.error(err);
    //     },
    //     () => {
    //       subject.complete();
    //     }
    //   );

    return subject;
  };

  public getHeaderValue = (): HeaderValue => {
    return {
      headerItems: [
        { context: 'id' },
        { context: 'stanje modela' },
        // { context: 'registrovani korisnik' },
        { context: '' },
        { context: '' },
        { context: '' },
      ],
    };
  };

  public addRowToTable = (
    table: Table,
    detaljiFn: Function = () => {},
    izmeniFn: Function = () => {},
    obrisiFn: Function = () => {}
  ): void => {
    try {
      table.addChildValue({
        data: this,
        rowItems: [
          {
            context: this._entitet.getId(),
          },
          {
            context: this._entitet.getStanje(),
          },
          // {
          //   context: this._entitet.getRegistrovaniKorisnik()
          //     ? this._entitet.getRegistrovaniKorisnik().getId() +
          //       ' ' +
          //       this._entitet.getRegistrovaniKorisnik().getIme() +
          //       ' ' +
          //       this._entitet.getRegistrovaniKorisnik().getPrezime()
          //     : '/',
          // },
          {
            context: 'detalji',
            button: { function: detaljiFn },
          },
          {
            context: 'izmeni',
            button: { function: izmeniFn },
          },
          {
            context:
              this._entitet.getStanje() !== StanjeModela.OBRISAN
                ? 'obrisi'
                : '',
            button:
              this._entitet.getStanje() !== StanjeModela.OBRISAN
                ? { function: obrisiFn }
                : undefined,
          },
        ],
      });
    } catch (error) {
      console.log(error);
    }
  };
}
