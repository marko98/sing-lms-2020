import { StanjeModela } from '../enum/stanje_modela.enum';
import { CrudServiceForModel } from '../../service/crud.service-for-model';
import { Subject } from 'rxjs';
import {
  Form,
  FormRow,
  ROW_ITEM_TYPE,
  FormFieldInputNumberInterface,
  FORM_FIELD_INPUT_TYPE,
  SelectInterface,
  VALIDATOR_NAMES,
  FormFieldInputTextInterface,
  DatepickerInterface,
  MAT_COLOR,
  TextareaInterface,
} from 'my-angular-form';
import { Validators } from '@angular/forms';
import { HeaderValue, Table } from 'my-angular-table';
import { Univerzitet } from '../entity/univerzitet.model';
import { Formable } from '../interface/formable.interface';
import { Tableable } from '../interface/tableable.interface';
import { UNIVERZITET_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/univerzitet_service.service';
import { take } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { Nastavnik } from '../entity/nastavnik.model';
import { NASTAVNIK_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/nastavnik_service.service';
import { StudentskaSluzba } from '../entity/studentska_sluzba.model';
import { STUDENTSKA_SLUZBA_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/studentska_sluzba_service.service';

export class UniverzitetFormableTableable implements Formable, Tableable {
  private _formSubject: Subject<Form> = new Subject();

  public getFormSubject = (): Subject<Form> => {
    return this._formSubject;
  };

  constructor(private _entitet: Univerzitet) {}

  public getForm = (
    csfm: CrudServiceForModel,
    isFormForCreation: boolean,
    isFormForDetails: boolean = false,
    isFormForUpdate: boolean = false,
    disableAllFields: boolean = false
  ): void => {
    let form = new Form({
      showSubmitButton: !disableAllFields,
      submitButtonText: 'potvrdi',
      showCancelButton: !disableAllFields,
      cancelButtonText: 'odustani',
      showResetButton: isFormForCreation,
      resetButtonText: 'resetuj',
      submitOnlyIfFormValid: true,
    });

    // private id: number;+

    // private naziv: string;+

    // private datumOsnivanja: Date;+

    // private opis: string;+

    // private stanje: StanjeModela;+

    // private version: number;+

    // private adrese: Adresa[];+

    // private rektor: Nastavnik;+

    // private fakulteti: Fakultet[];+

    // private kalendari: Kalendar[];+

    // private studentskaSluzba: StudentskaSluzba;+

    // private kontakti: Kontakt[];+

    if (!isFormForCreation) {
      let rowId = new FormRow();
      form.addChild(rowId);
      rowId.addChildInterface(<FormFieldInputNumberInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
        formFieldInputType: FORM_FIELD_INPUT_TYPE.NUMBER,
        controlName: 'id',
        labelName: 'Id',
        defaultValue: this._entitet.getId(),
        disabled: true,
      });

      // console.log(StanjeModela.AKTIVAN);
      // console.log(typeof StanjeModela.AKTIVAN);

      let rowStanjeModela = new FormRow();
      form.addChild(rowStanjeModela);
      rowStanjeModela.addChildInterface(<SelectInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
        controlName: 'stanje',
        labelName: 'Stanje modela',
        defaultValue: this._entitet.getStanje(),
        optionValues: [
          { value: StanjeModela.AKTIVAN, textToShow: 'AKTIVAN' },
          { value: StanjeModela.NEAKTIVAN, textToShow: 'NEAKTIVAN' },
        ],
        disabled: disableAllFields,
        validators: [
          {
            message: 'Stanje modela je obavezno',
            name: VALIDATOR_NAMES.REQUIRED,
            validatorFn: Validators.required,
          },
        ],
      });
    }

    let rowNaziv = new FormRow();
    form.addChild(rowNaziv);
    rowNaziv.addChildInterface(<FormFieldInputTextInterface>{
      type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
      formFieldInputType: FORM_FIELD_INPUT_TYPE.TEXT,
      controlName: 'naziv',
      labelName: 'Naziv univerziteta',
      defaultValue: this._entitet.getNaziv(),
      appearance: 'outline',
      placeholder: 'unesite naziv univerziteta',
      disabled: disableAllFields,
      validators: [
        {
          message: 'naziv univerziteta je obavezan',
          name: VALIDATOR_NAMES.REQUIRED,
          validatorFn: Validators.required,
        },
      ],
    });

    let rowDatumOsnivanja = new FormRow();
    form.addChild(rowDatumOsnivanja);
    rowDatumOsnivanja.addChildInterface(<DatepickerInterface>{
      type: ROW_ITEM_TYPE.DATEPICKER,
      labelName: 'Datum osnivanja',
      controlName: 'datumOsnivanja',
      color: MAT_COLOR.ACCENT,
      matIcon: 'date_range',
      disabled: disableAllFields,
      min: new Date(1950, 0, 1),
      startAt: new Date(2005, 0, 1),
      validators: [
        {
          message: 'datum osnivanja je obavezan',
          name: VALIDATOR_NAMES.REQUIRED,
          validatorFn: Validators.required,
        },
      ],
      defaultValue: this._entitet.getDatumOsnivanja(),
      appearance: 'outline',
      // disabledDays: [DAY.MONDAY, DAY.SATURDAY],
      timepicker: {
        buttonAlign: 'left',
        /**
         * min i max pisi u formatu:
         * min: 10:00 AM
         * max: 03:00 PM
         */
        // min: '10:00 AM',
        // max: '03:00 PM',
        /**
         * Set a default value and time for a timepicker. The format of the time is in 24 hours notation 23:00.
         * A Date string won't work.
         */
        defaultValue: this._entitet.getDatumOsnivanja()
          ? this._entitet.getDatumOsnivanja().getHours() +
            ':' +
            this._entitet.getDatumOsnivanja().getMinutes()
          : undefined,
        labelName: 'vreme osnivanja',
        disabled: disableAllFields,
      },
    });

    let rowOpis = new FormRow();
    form.addChild(rowOpis);
    rowOpis.addChildInterface(<TextareaInterface>{
      type: ROW_ITEM_TYPE.FORM_FIELD_TEXTAREA,
      controlName: 'opis',
      labelName: 'Opis univerziteta',
      defaultValue: this._entitet.getOpis(),
      validators: [
        {
          message: 'opis mora imati vise od 9 karaktera',
          name: VALIDATOR_NAMES.MIN_LENGTH,
          validatorFn: Validators.minLength(10),
          length: 10,
        },
      ],
      showHintAboutMinMaxLength: true,
      requiredOption: {
        required: true,
        hideRequiredMarker: true,
      },
      disabled: disableAllFields,
    });

    let nastavnikService = csfm.getCrudServiceForModel<Nastavnik, number>(
      NASTAVNIK_SERVICE_FOR_MODEL_INTERFACE
    );

    nastavnikService
      .findAll()
      .pipe(take(1))
      .subscribe(
        (entiteti: Nastavnik[]) => {
          for (let index = 0; index < entiteti.length; index++) {
            if (
              entiteti[index].getId().toString() ===
              this._entitet.getRektor()?.getId().toString()
            ) {
              this._entitet.setRektor(entiteti[index]);
              break;
            }
          }

          if (isFormForCreation) {
            // entiteti = entiteti.filter((r) => !r.getStudentskiSluzbenik());
          } else if (isFormForUpdate) {
            // entiteti = entiteti.filter((r) => !r.getStudentskiSluzbenik());
            // entiteti.push(this._entitet.getRektor());
          } else if (isFormForDetails) {
            if (
              this._entitet.getRektor() &&
              !entiteti.find(
                (e) =>
                  e.getId().toString() ===
                  this._entitet.getRektor().getId().toString()
              )
            )
              entiteti.push(this._entitet.getRektor());
          }

          // if (!(entiteti.length > 0)) {
          //   this._formSubject.error(
          //     new Error(
          //       'neki error text'
          //     )
          //   );
          //   return;
          // }

          let rowEntitet = new FormRow();
          form.addChild(rowEntitet);
          rowEntitet.addChildInterface(<SelectInterface>{
            type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
            controlName: 'rektor',
            labelName: 'Rektor univerziteta',
            defaultValue: this._entitet.getRektor(),
            optionValues: entiteti.map((d) => {
              return {
                value: d,
                textToShow: d.getId().toString(),
              };
            }),
            disabled: disableAllFields,
          });
          this._formSubject.next(form);
          // --------------------------------------
          let studentskaSluzbaService = csfm.getCrudServiceForModel<
            StudentskaSluzba,
            number
          >(STUDENTSKA_SLUZBA_SERVICE_FOR_MODEL_INTERFACE);

          studentskaSluzbaService
            .findAll()
            .pipe(take(1))
            .subscribe(
              (entiteti: StudentskaSluzba[]) => {
                for (let index = 0; index < entiteti.length; index++) {
                  if (
                    entiteti[index].getId().toString() ===
                    this._entitet.getStudentskaSluzba()?.getId().toString()
                  ) {
                    this._entitet.setStudentskaSluzba(entiteti[index]);
                    break;
                  }
                }

                if (isFormForCreation) {
                  entiteti = entiteti.filter((r) => !r.getUniverzitet());
                } else if (isFormForUpdate) {
                  entiteti = entiteti.filter((r) => !r.getUniverzitet());
                  entiteti.push(this._entitet.getStudentskaSluzba());
                } else if (isFormForDetails) {
                  if (
                    this._entitet.getStudentskaSluzba() &&
                    !entiteti.find(
                      (e) =>
                        e.getId().toString() ===
                        this._entitet.getStudentskaSluzba().getId().toString()
                    )
                  )
                    entiteti.push(this._entitet.getStudentskaSluzba());
                }

                if (!(entiteti.length > 0)) {
                  this._formSubject.error(
                    new Error(
                      'Sve studentske sluzbe vec pripadaju nekim univerzitetima'
                    )
                  );
                  return;
                } else {
                  let rowEntitet = new FormRow();
                  form.addChild(rowEntitet);
                  rowEntitet.addChildInterface(<SelectInterface>{
                    type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
                    controlName: 'studentskaSluzba',
                    labelName: 'Studentska sluzba univerziteta',
                    defaultValue: this._entitet.getStudentskaSluzba(),
                    optionValues: entiteti.map((d) => {
                      return {
                        value: d,
                        textToShow: d.getId().toString(),
                      };
                    }),
                    disabled: disableAllFields,
                  });
                }

                this._formSubject.next(form);
                this._formSubject.complete();
              },
              (err: HttpErrorResponse) => {
                this._formSubject.error(err);
              }
            );
          // ------------------------------------
        },
        (err: HttpErrorResponse) => {
          this._formSubject.error(err);
        }
      );
  };

  public save = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    let univerzitetService = csfm.getCrudServiceForModel<Univerzitet, number>(
      UNIVERZITET_SERVICE_FOR_MODEL_INTERFACE
    );
    entitetDict.datumOsnivanja = new Date(
      entitetDict.datumOsnivanja.toUTCString()
    );
    univerzitetService
      .save(entitetDict)
      .pipe(take(1))
      .subscribe(
        (entitet) => {},
        (err: HttpErrorResponse) => {
          subject.error(err);
        },
        () => {
          subject.complete();
        }
      );

    return subject;
  };

  public update = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    let univerzitetService = csfm.getCrudServiceForModel<Univerzitet, number>(
      UNIVERZITET_SERVICE_FOR_MODEL_INTERFACE
    );
    entitetDict.datumOsnivanja = new Date(
      entitetDict.datumOsnivanja.toUTCString()
    );
    univerzitetService
      .update(entitetDict)
      .pipe(take(1))
      .subscribe(
        (entitet) => {},
        (err: HttpErrorResponse) => {
          subject.error(err);
        },
        () => {
          subject.complete();
        }
      );

    return subject;
  };

  public getHeaderValue = (): HeaderValue => {
    return {
      headerItems: [
        { context: 'id' },
        { context: 'stanje modela' },
        { context: 'naziv' },
        { context: 'datum osnivanja' },
        { context: 'opis' },
        { context: 'rektor' },
        { context: 'studentska sluzba' },
        { context: '' },
        { context: '' },
        { context: '' },
      ],
    };
  };

  public addRowToTable = (
    table: Table,
    detaljiFn: Function = () => {},
    izmeniFn: Function = () => {},
    obrisiFn: Function = () => {}
  ): void => {
    try {
      table.addChildValue({
        data: this._entitet,
        rowItems: [
          {
            context: this._entitet.getId(),
          },
          {
            context: this._entitet.getStanje(),
          },
          {
            context: this._entitet.getNaziv(),
          },
          {
            context: this._entitet.getDatumOsnivanja().toLocaleDateString(),
          },
          {
            context: this._entitet.getOpis(),
          },
          {
            context: this._entitet.getRektor()
              ? this._entitet.getRektor().getId().toString()
              : '/',
          },
          {
            context: this._entitet.getStudentskaSluzba()
              ? this._entitet.getStudentskaSluzba().getId().toString()
              : '/',
          },
          {
            context: 'detalji',
            button: { function: detaljiFn },
          },
          {
            context: 'izmeni',
            button: { function: izmeniFn },
          },
          {
            context:
              this._entitet.getStanje() !== StanjeModela.OBRISAN
                ? 'obrisi'
                : '',
            button:
              this._entitet.getStanje() !== StanjeModela.OBRISAN
                ? { function: obrisiFn }
                : undefined,
          },
        ],
      });
    } catch (error) {
      console.log(error);
    }
  };
}
