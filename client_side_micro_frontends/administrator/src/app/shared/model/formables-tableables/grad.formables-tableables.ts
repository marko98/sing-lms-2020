import { StanjeModela } from '../enum/stanje_modela.enum';
import {
  Form,
  FormRow,
  FormFieldInputNumberInterface,
  ROW_ITEM_TYPE,
  FORM_FIELD_INPUT_TYPE,
  SelectInterface,
  VALIDATOR_NAMES,
  FormFieldInputTextInterface,
} from 'my-angular-form';
import { Validators } from '@angular/forms';
import { CrudServiceForModel } from '../../service/crud.service-for-model';
import { take } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Table, HeaderValue } from 'my-angular-table';
import { Tableable } from '../interface/tableable.interface';
import { Formable } from '../interface/formable.interface';
import { Grad } from '../entity/grad.model';
import { DRZAVA_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/drzava_service.service';
import { Drzava } from '../entity/drzava.model';
import { HttpErrorResponse } from '@angular/common/http';
import { GRAD_SERVICE_FOR_MODEL_INTERFACE } from '../../service/entity/grad_service.service';

export class GradFormableTableable implements Formable, Tableable {
  private _formSubject: Subject<Form> = new Subject();

  public getFormSubject = (): Subject<Form> => {
    return this._formSubject;
  };

  constructor(private _entitet: Grad) {}

  public getForm = (
    csfm: CrudServiceForModel,
    isFormForCreation: boolean,
    disableAllFields: boolean = false
  ): void => {
    let form = new Form({
      showSubmitButton: !disableAllFields,
      submitButtonText: 'potvrdi',
      showCancelButton: !disableAllFields,
      cancelButtonText: 'odustani',
      showResetButton: isFormForCreation,
      resetButtonText: 'resetuj',
      submitOnlyIfFormValid: true,
    });

    // private id: number;+

    // private naziv: string;+

    // private stanje: StanjeModela;+

    // private version: number;+

    // private drzava: Drzava;+

    // private adrese: Adresa[];+

    if (!isFormForCreation) {
      let rowId = new FormRow();
      form.addChild(rowId);
      rowId.addChildInterface(<FormFieldInputNumberInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
        formFieldInputType: FORM_FIELD_INPUT_TYPE.NUMBER,
        controlName: 'id',
        labelName: 'Id',
        defaultValue: this._entitet.getId(),
        disabled: true,
      });

      // console.log(StanjeModela.AKTIVAN);
      // console.log(typeof StanjeModela.AKTIVAN);

      let rowStanjeModela = new FormRow();
      form.addChild(rowStanjeModela);
      rowStanjeModela.addChildInterface(<SelectInterface>{
        type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
        controlName: 'stanje',
        labelName: 'Stanje modela',
        defaultValue: this._entitet.getStanje(),
        optionValues: [
          { value: StanjeModela.AKTIVAN, textToShow: 'AKTIVAN' },
          { value: StanjeModela.NEAKTIVAN, textToShow: 'NEAKTIVAN' },
        ],
        disabled: disableAllFields,
        validators: [
          {
            message: 'Stanje modela je obavezno',
            name: VALIDATOR_NAMES.REQUIRED,
            validatorFn: Validators.required,
          },
        ],
      });
    }

    let rowNaziv = new FormRow();
    form.addChild(rowNaziv);
    rowNaziv.addChildInterface(<FormFieldInputTextInterface>{
      type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
      formFieldInputType: FORM_FIELD_INPUT_TYPE.TEXT,
      controlName: 'naziv',
      labelName: 'Naziv grada',
      defaultValue: this._entitet.getNaziv(),
      appearance: 'outline',
      placeholder: 'unesite naziv grada',
      validators: [
        {
          message: 'Naziv grada je obavezan',
          name: VALIDATOR_NAMES.REQUIRED,
          validatorFn: Validators.required,
        },
      ],
      disabled: disableAllFields,
    });

    let drzavaService = csfm.getCrudServiceForModel<Drzava, number>(
      DRZAVA_SERVICE_FOR_MODEL_INTERFACE
    );

    drzavaService
      .findAll()
      .pipe(take(1))
      .subscribe(
        (drzave: Drzava[]) => {
          for (let index = 0; index < drzave.length; index++) {
            if (
              drzave[index].getId().toString() ===
              this._entitet.getDrzava()?.getId().toString()
            ) {
              this._entitet.setDrzava(drzave[index]);
              break;
            }
          }

          // console.log(drzave);
          let rowDrzava = new FormRow();
          form.addChild(rowDrzava);
          rowDrzava.addChildInterface(<SelectInterface>{
            type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
            controlName: 'drzava',
            labelName: 'Drzava',
            defaultValue: this._entitet.getDrzava(),
            optionValues: drzave.map((d) => {
              return { value: d, textToShow: d.getNaziv() };
            }),
            disabled: disableAllFields,
          });
          this._formSubject.next(form);
          this._formSubject.complete();
        },
        (err: HttpErrorResponse) => {
          this._formSubject.error(err);
        }
      );
  };

  public save = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    let gradService = csfm.getCrudServiceForModel<Grad, number>(
      GRAD_SERVICE_FOR_MODEL_INTERFACE
    );
    gradService
      .save(entitetDict)
      .pipe(take(1))
      .subscribe(
        (entitet) => {},
        (err: HttpErrorResponse) => {
          subject.error(err);
        },
        () => {
          subject.complete();
        }
      );

    return subject;
  };

  public update = (
    csfm: CrudServiceForModel,
    entitetDict: any
  ): Subject<void> => {
    let subject: Subject<void> = new Subject();
    // console.log(entitetDict);

    let gradService = csfm.getCrudServiceForModel<Grad, number>(
      GRAD_SERVICE_FOR_MODEL_INTERFACE
    );
    gradService
      .update(entitetDict)
      .pipe(take(1))
      .subscribe(
        (entitet) => {},
        (err: HttpErrorResponse) => {
          subject.error(err);
        },
        () => {
          subject.complete();
        }
      );

    return subject;
  };

  public getHeaderValue = (): HeaderValue => {
    return {
      headerItems: [
        { context: 'id' },
        { context: 'stanje modela' },
        { context: 'naziv' },
        { context: 'drzava' },
        { context: '' },
        { context: '' },
        { context: '' },
      ],
    };
  };

  public addRowToTable = (
    table: Table,
    detaljiFn: Function = () => {},
    izmeniFn: Function = () => {},
    obrisiFn: Function = () => {}
  ): void => {
    try {
      table.addChildValue({
        data: this,
        rowItems: [
          {
            context: this._entitet.getId(),
          },
          {
            context: this._entitet.getStanje(),
          },
          {
            context: this._entitet.getNaziv(),
          },
          {
            context: this._entitet.getDrzava()
              ? this._entitet.getDrzava().getNaziv()
              : '/',
          },
          {
            context: 'detalji',
            button: { function: detaljiFn },
          },
          {
            context: 'izmeni',
            button: { function: izmeniFn },
          },
          {
            context:
              this._entitet.getStanje() !== StanjeModela.OBRISAN
                ? 'obrisi'
                : '',
            button:
              this._entitet.getStanje() !== StanjeModela.OBRISAN
                ? { function: obrisiFn }
                : undefined,
          },
        ],
      });
    } catch (error) {
      console.log(error);
    }
  };
}
