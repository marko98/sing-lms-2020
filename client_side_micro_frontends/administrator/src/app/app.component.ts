import { Component, OnInit, OnDestroy } from '@angular/core';
import { CrudService } from './shared/service/crud.service';
import { GradService } from './shared/service/entity/grad_service.service';
import { DrzavaService } from './shared/service/entity/drzava_service.service';
import { Grad } from './shared/model/entity/grad.model';
import { Drzava } from './shared/model/entity/drzava.model';
import { AuthService } from './shared/service/auth.service';
import { MICRO_FRONTENDS_PREFIX } from './shared/const';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'kva-k2';

  constructor(private _authService: AuthService) {
    this._authService.setAfterLoginRoute(
      MICRO_FRONTENDS_PREFIX.MF_4 + '/administracija'
    );
    this._authService.setAfterLogoutRoute(
      MICRO_FRONTENDS_PREFIX.MF_4 + '/login'
    );
    this._authService.onAutoLogin();
  }

  ngOnInit(): void {
    console.log('AppComponent init');
  }

  ngOnDestroy(): void {
    console.log('AppComponent destroyed');
  }
}
