import { Component, OnInit, OnDestroy } from '@angular/core';
import {
  Form,
  FormRow,
  FormFieldInputTextInterface,
  ROW_ITEM_TYPE,
  FORM_FIELD_INPUT_TYPE,
  VALIDATOR_NAMES,
  FormFieldInputPasswordInterface,
  AT_LEAST_ONE_LOWER_CASE_CHARACTER_ONE_UPPER_CASE_CHARACTER_ONE_DIGIT_ONE_SPECIAL_CHARACTER_REGEX,
  SelectInterface,
} from 'my-angular-form';
import { Validators, NgForm } from '@angular/forms';
import { Location } from '@angular/common';
import { AuthService } from '../shared/service/auth.service';
import {
  MICROSERVICE_NAME,
  PATH,
  MICRO_FRONTENDS_PREFIX,
  ROLES,
} from '../shared/const';
import { take } from 'rxjs/operators';
import { Router } from '@angular/router';
import { UIService } from '../shared/service/ui.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit, OnDestroy {
  public form: Form;

  constructor(
    private _location: Location,
    private _authService: AuthService,
    private _router: Router,
    private _uiSerice: UIService
  ) {}

  public onSubmit = (ngForm: NgForm): void => {
    if (ngForm.valid) {
      // console.log(ngForm.value);
      if (ngForm.value.prijavaKao === ROLES.ROLE_STUDENT) {
        // window.alert('nemamo home page za studenta za sada');
        this._onLogin(
          ngForm.value.korisnickoIme,
          ngForm.value.lozinka,
          MICROSERVICE_NAME.STUDENT,
          PATH.STUDENT + '/login',
          MICRO_FRONTENDS_PREFIX.MF_6 + '/prijemno_sanduce'
        );
      } else if (ngForm.value.prijavaKao === ROLES.ROLE_NASTAVNIK) {
        window.alert('nemamo home page za nastavnika za sada');
        // this._onLogin(
        //   ngForm.value.korisnickoIme,
        //   ngForm.value.lozinka,
        //   MICROSERVICE_NAME.NASTAVNIK,
        //   PATH.NASTAVNIK + '/login',
        //   'nemamo home page za nastavnika za sada'
        // );
      } else if (ngForm.value.prijavaKao === ROLES.ROLE_ADMINISTRATOR) {
        this._onLogin(
          ngForm.value.korisnickoIme,
          ngForm.value.lozinka,
          MICROSERVICE_NAME.ADMINISTRACIJA,
          PATH.ADMINISTRATOR + '/login',
          MICRO_FRONTENDS_PREFIX.MF_4 + '/administracija'
        );
      } else if (ngForm.value.prijavaKao === ROLES.ROLE_STUDENTSKI_SLUZBENIK) {
        this._onLogin(
          ngForm.value.korisnickoIme,
          ngForm.value.lozinka,
          MICROSERVICE_NAME.ADMINISTRACIJA,
          PATH.STUDENTSKI_SLUZBENIK + '/login',
          MICRO_FRONTENDS_PREFIX.MF_4 + '/administracija'
        );
      }
    }
  };

  public onGoToUniverziteti = (): void => {
    this._router.navigate([MICRO_FRONTENDS_PREFIX.MF_2_NK, 'univerziteti']);
  };

  private _onLogin = (
    korisnickoIme: string,
    lozinka: string,
    microserviceName: string,
    path: string,
    navigationPath: string
  ): void => {
    this._authService
      .onLogin(korisnickoIme, lozinka, microserviceName, path)
      .pipe(take(1))
      .subscribe(
        () => {
          this._router.navigate([navigationPath]);
        },
        (err) => {
          this._uiSerice.showSnackbar(
            'Došlo je do greške, ukoliko ste sigurni da su uneti podaci ispravni, obratite se nekom od adminstratora ili studentskih službenika',
            null,
            4000
          );
        }
      );
  };

  public onCancel = (): void => {
    this._location.back();
  };

  private _onFormInit = (): void => {
    this.form = new Form({
      showCancelButton: true,
      showResetButton: true,
      showSubmitButton: true,
      cancelButtonText: 'odustani',
      resetButtonText: 'resetuj',
      submitButtonText: 'login',
      submitOnlyIfFormValid: true,
    });

    let formRowKorisnickoImeLozinka = new FormRow();
    this.form.addChild(formRowKorisnickoImeLozinka);
    formRowKorisnickoImeLozinka.addChildInterface(<FormFieldInputTextInterface>{
      type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
      formFieldInputType: FORM_FIELD_INPUT_TYPE.TEXT,
      controlName: 'korisnickoIme',
      labelName: 'Vase korisnicko ime:',
      appearance: 'outline',
      placeholder: 'unesite Vase korisnicko ime',
      validators: [
        {
          message: 'korisnicko ime je obavezno',
          name: VALIDATOR_NAMES.REQUIRED,
          validatorFn: Validators.required,
        },
        {
          message: 'korisnicko ime mora biti najmanje 5 karaktera dugacko',
          name: VALIDATOR_NAMES.MIN_LENGTH,
          validatorFn: Validators.minLength(5),
          length: 5,
        },
      ],
      showHintAboutMinMaxLength: true,
    });

    formRowKorisnickoImeLozinka.addChildInterface(<
      FormFieldInputPasswordInterface
    >{
      type: ROW_ITEM_TYPE.FORM_FIELD_INPUT,
      formFieldInputType: FORM_FIELD_INPUT_TYPE.PASSWORD,
      controlName: 'lozinka',
      labelName: 'Vasa lozinka',
      appearance: 'outline',
      placeholder: 'unesite Vasu lozinku',
      validators: [
        {
          name: VALIDATOR_NAMES.REQUIRED,
          message: 'lozinka je obazvezna',
          validatorFn: Validators.required,
        },
        {
          message: 'lozinka mora biti najmanje 6 karaktera dugacka',
          name: VALIDATOR_NAMES.MIN_LENGTH,
          validatorFn: Validators.minLength(6),
          length: 6,
        },
        {
          message:
            'lozinka mora imati bar jedno malo slovo, jedno veliko slovo, jedan broj i specijalan karakter',
          name: VALIDATOR_NAMES.PATTERN,
          validatorFn: Validators.pattern(
            AT_LEAST_ONE_LOWER_CASE_CHARACTER_ONE_UPPER_CASE_CHARACTER_ONE_DIGIT_ONE_SPECIAL_CHARACTER_REGEX
          ),
        },
      ],
      matImages: 'menu',
      showPasswordInMs: 1500,
      showHintAboutMinMaxLength: true,
    });

    let rowUloga = new FormRow();
    this.form.addChild(rowUloga);
    rowUloga.addChildInterface(<SelectInterface>{
      type: ROW_ITEM_TYPE.FORM_FIELD_SELECT,
      controlName: 'prijavaKao',
      labelName: 'Prijavite se kao',
      defaultValue: ROLES.ROLE_STUDENT,
      optionValues: [
        {
          value: ROLES.ROLE_STUDENT,
          textToShow: ROLES.ROLE_STUDENT.replace('ROLE_', '')
            .replace('_', ' ')
            .toLowerCase(),
        },
        {
          value: ROLES.ROLE_NASTAVNIK,
          textToShow: ROLES.ROLE_NASTAVNIK.replace('ROLE_', '')
            .replace('_', ' ')
            .toLowerCase(),
        },
        {
          value: ROLES.ROLE_ADMINISTRATOR,
          textToShow: ROLES.ROLE_ADMINISTRATOR.replace('ROLE_', '')
            .replace('_', ' ')
            .toLowerCase(),
        },
        {
          value: ROLES.ROLE_STUDENTSKI_SLUZBENIK,
          textToShow: ROLES.ROLE_STUDENTSKI_SLUZBENIK.replace('ROLE_', '')
            .replace('_', ' ')
            .toLowerCase(),
        },
      ],
      // disabled: true,
      requiredOption: {
        required: true,
        hideRequiredMarker: true,
      },
    });
  };

  ngOnInit(): void {
    this._onFormInit();
    console.log('LoginComponent init');
  }

  ngOnDestroy(): void {
    console.log('LoginComponent destroyed');
  }
}
