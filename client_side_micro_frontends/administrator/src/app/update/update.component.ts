import {
  Component,
  OnInit,
  OnDestroy,
  Input,
  Output,
  EventEmitter,
  SimpleChanges,
  OnChanges,
} from '@angular/core';
import { Formable } from '../shared/model/interface/formable.interface';
import { Form } from 'my-angular-form';
import { CrudServiceForModel } from '../shared/service/crud.service-for-model';
import { UIService } from '../shared/service/ui.service';
import { NgForm } from '@angular/forms';
import { take } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css'],
})
export class UpdateComponent implements OnInit, OnDestroy, OnChanges {
  @Input() entitet: Formable;
  @Output() onUpdateFinished: EventEmitter<void> = new EventEmitter();
  @Output() onUpdateSubmit: EventEmitter<void> = new EventEmitter();

  private _formSubscription: Subscription;

  public form: Form;
  public loading: boolean = true;

  constructor(
    private _csfm: CrudServiceForModel,
    private _uiService: UIService
  ) {}

  public onSubmit = (ngForm: NgForm): void => {
    // console.log(ngForm);
    // console.log(ngForm.value);
    this.loading = true;
    this.onUpdateSubmit.next();

    this.entitet
      .update(this._csfm, ngForm.value)
      .pipe(take(1))
      .subscribe(
        () => {},
        (err: HttpErrorResponse) => {
          this._uiService.showSnackbar(err.message, null, 1000);
        },
        () => {
          this._uiService.showSnackbar('Entitet uspesno izmenjen', null, 1000);
          this.onUpdateFinished.next();
        }
      );
  };

  public onCancel = (): void => {
    this.onUpdateFinished.next();
  };

  private _onInitForm = (): void => {
    // console.log(this.entitet);

    this._formSubscription = this.entitet.getFormSubject().subscribe(
      (form) => {
        this.form = form;
      },
      (err: HttpErrorResponse) => {
        this._uiService.showSnackbar(err.message, null, 1000);
        this.onCancel();
      },
      () => {
        this.loading = false;
      }
    );

    this.entitet.getForm(this._csfm, false, false, true);
  };

  ngOnInit(): void {
    console.log('UpdateComponent init');
  }

  ngOnChanges(changes: SimpleChanges): void {
    // console.log(changes);
    this.loading = true;
    this.form = undefined;
    try {
      this._onInitForm();
    } catch (error) {
      this._uiService.showSnackbar(
        'bice implementirano u verziji: 2.0',
        null,
        1000
      );
      this.onCancel();
    }
  }

  ngOnDestroy(): void {
    if (this._formSubscription) this._formSubscription.unsubscribe();
    console.log('UpdateComponent destroyed');
  }
}
