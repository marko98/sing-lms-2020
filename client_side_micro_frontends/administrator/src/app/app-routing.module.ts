import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmptyRouteComponent } from './empty-route/empty-route.component';
import { APP_BASE_HREF } from '@angular/common';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { ROLES } from './shared/const';
import { AuthGuard } from './shared/guard/auth.guard';

const routes: Routes = [
  {
    path: 'mf_4',
    data: {
      requiredRoles: [],
    },
    children: [
      {
        path: 'administracija',
        component: HomeComponent,
        data: {
          requiredRoles: [
            ROLES.ROLE_ADMINISTRATOR,
            ROLES.ROLE_STUDENTSKI_SLUZBENIK,
          ],
        },
        canActivate: [AuthGuard],
      },
      {
        path: 'login',
        component: LoginComponent,
      },
      {
        path: '',
        pathMatch: 'full',
        redirectTo: '/mf_4/administracija',
        canActivate: [AuthGuard],
      },
      { path: '**', redirectTo: '/mf_4/administracija' },
    ],
  },
  { path: '**', component: EmptyRouteComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [{ provide: APP_BASE_HREF, useValue: '' }],
})
export class AppRoutingModule {}
