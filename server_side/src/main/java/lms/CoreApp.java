package lms;

import lms.host.HostApp;
import lms.plugin.administracija.PluginAdministracijaApp;
import lms.plugin.adresa.PluginAdresaApp;
import lms.plugin.evaluacija_znanja.PluginEvaluacijaZnanjaApp;
import lms.plugin.fakultet.PluginFakultetApp;
import lms.plugin.korisnik.PluginKorisnikApp;
import lms.plugin.nastava.PluginNastavaApp;
import lms.plugin.nastavnik.PluginNastavnikApp;
import lms.plugin.obavestenje.PluginObavestenjeApp;
import lms.plugin.predmet.PluginPredmetApp;
import lms.plugin.student.PluginStudentApp;

public class CoreApp {

    public static void main(String args[]) {
//	host
	HostApp.main(args);

//	plugins
	PluginAdresaApp.main(args);
	PluginAdministracijaApp.main(args);
	PluginEvaluacijaZnanjaApp.main(args);
	PluginFakultetApp.main(args);
	PluginKorisnikApp.main(args);
	PluginNastavaApp.main(args);
	PluginNastavnikApp.main(args);
	PluginObavestenjeApp.main(args);
	PluginPredmetApp.main(args);
	PluginStudentApp.main(args);
    }

}