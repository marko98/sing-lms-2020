package lms.host.controller.evaluacija_znanja;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.EvaluacijaZnanja;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/evaluacija_znanja")
@CrossOrigin(origins = "*")
public class EvaluacijaZnanjaController extends HostCrudController<EvaluacijaZnanja, Long> {

    public EvaluacijaZnanjaController() {
	this.category = "evaluacija_znanja";
    }

}