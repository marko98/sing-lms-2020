package lms.host.controller.evaluacija_znanja;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.Pitanje;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/pitanje")
@CrossOrigin(origins = "*")
public class PitanjeController extends HostCrudController<Pitanje, Long> {

    public PitanjeController() {
	this.category = "pitanje";
    }

}