package lms.host.controller.evaluacija_znanja;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.EvaluacijaZnanjaNacinEvaluacije;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/evaluacija_znanja_nacin_evaluacije")
@CrossOrigin(origins = "*")
public class EvaluacijaZnanjaNacinEvaluacijeController
	extends HostCrudController<EvaluacijaZnanjaNacinEvaluacije, Long> {

    public EvaluacijaZnanjaNacinEvaluacijeController() {
	this.category = "evaluacija_znanja_nacin_evaluacije";
    }

}