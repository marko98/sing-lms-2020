package lms.host.controller.evaluacija_znanja;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.Odgovor;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/odgovor")
@CrossOrigin(origins = "*")
public class OdgovorController extends HostCrudController<Odgovor, Long> {

    public OdgovorController() {
	this.category = "odgovor";
    }

}