package lms.host.controller.evaluacija_znanja;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.Polaganje;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/polaganje")
@CrossOrigin(origins = "*")
public class PolaganjeController extends HostCrudController<Polaganje, Long> {

    public PolaganjeController() {
	this.category = "polaganje";
    }

}