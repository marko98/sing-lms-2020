package lms.host.controller.student;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.Student;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/student")
@CrossOrigin(origins = "*")

public class StudentController extends HostCrudController<Student, Long> {

    public StudentController() {
	this.category = "student";
    }

}