package lms.host.controller.student;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.StudentNaStudiji;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/student_na_studiji")
@CrossOrigin(origins = "*")

public class StudentNaStudijiController extends HostCrudController<StudentNaStudiji, Long> {

    public StudentNaStudijiController() {
	this.category = "student_na_studiji";
    }

}