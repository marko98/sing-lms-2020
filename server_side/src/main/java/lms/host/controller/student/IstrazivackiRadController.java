package lms.host.controller.student;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.IstrazivackiRad;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/istrazivacki_rad")
@CrossOrigin(origins = "*")

public class IstrazivackiRadController extends HostCrudController<IstrazivackiRad, Long> {

    public IstrazivackiRadController() {
	this.category = "istrazivacki_rad";
    }

}