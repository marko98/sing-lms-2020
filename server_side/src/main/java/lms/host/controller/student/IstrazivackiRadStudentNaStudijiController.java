package lms.host.controller.student;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.IstrazivackiRadStudentNaStudiji;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/istrazivacki_rad_student_na_studiji")
@CrossOrigin(origins = "*")

public class IstrazivackiRadStudentNaStudijiController
	extends HostCrudController<IstrazivackiRadStudentNaStudiji, Long> {

    public IstrazivackiRadStudentNaStudijiController() {
	this.category = "istrazivacki_rad_student_na_studiji";
    }

}