package lms.host.controller.student;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.DatumPohadjanjaPredmeta;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/datum_pohadjanja_predmeta")
@CrossOrigin(origins = "*")

public class DatumPohadjanjaPredmetaController extends HostCrudController<DatumPohadjanjaPredmeta, Long> {

    public DatumPohadjanjaPredmetaController() {
	this.category = "datum_pohadjanja_predmeta";
    }

}