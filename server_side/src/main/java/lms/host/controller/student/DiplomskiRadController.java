package lms.host.controller.student;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.DiplomskiRad;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/diplomski_rad")
@CrossOrigin(origins = "*")

public class DiplomskiRadController extends HostCrudController<DiplomskiRad, Long> {

    public DiplomskiRadController() {
	this.category = "diplomski_rad";
    }

}