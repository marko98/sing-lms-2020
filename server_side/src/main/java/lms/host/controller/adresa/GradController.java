package lms.host.controller.adresa;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.Grad;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/grad")
@CrossOrigin(origins = "*")
public class GradController extends HostCrudController<Grad, Long> {

    public GradController() {
	this.category = "grad";
    }

}