package lms.host.controller.adresa;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.Drzava;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/drzava")
@CrossOrigin(origins = "*")
public class DrzavaController extends HostCrudController<Drzava, Long> {

    public DrzavaController() {
	this.category = "drzava";
    }

}