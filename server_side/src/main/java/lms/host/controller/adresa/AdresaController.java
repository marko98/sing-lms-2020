package lms.host.controller.adresa;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.Adresa;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/adresa")
@CrossOrigin(origins = "*")
public class AdresaController extends HostCrudController<Adresa, Long> {

    public AdresaController() {
	this.category = "adresa";
    }

}