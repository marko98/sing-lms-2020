package lms.host.controller.obavestenje;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.Obavestenje;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/obavestenje")
@CrossOrigin(origins = "*")

public class ObavestenjeController extends HostCrudController<Obavestenje, Long> {

    public ObavestenjeController() {
	this.category = "obavestenje";
    }

}