package lms.host.controller.obavestenje;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.GodinaStudijaObavestenje;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/godina_studija_obavestenje")
@CrossOrigin(origins = "*")

public class GodinaStudijaObavestenjeController extends HostCrudController<GodinaStudijaObavestenje, Long> {

    public GodinaStudijaObavestenjeController() {
	this.category = "godina_studija_obavestenje";
    }

}