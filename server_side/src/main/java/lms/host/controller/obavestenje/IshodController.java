package lms.host.controller.obavestenje;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.Ishod;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/ishod")
@CrossOrigin(origins = "*")

public class IshodController extends HostCrudController<Ishod, Long> {

    public IshodController() {
	this.category = "ishod";
    }

}