package lms.host.controller.obavestenje;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.Fajl;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/fajl")
@CrossOrigin(origins = "*")

public class FajlController extends HostCrudController<Fajl, Long> {

    public FajlController() {
	this.category = "fajl";
    }

}