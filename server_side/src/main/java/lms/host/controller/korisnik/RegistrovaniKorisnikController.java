package lms.host.controller.korisnik;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.RegistrovaniKorisnik;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/registrovani_korisnik")
@CrossOrigin(origins = "*")
public class RegistrovaniKorisnikController extends HostCrudController<RegistrovaniKorisnik, Long> {

    public RegistrovaniKorisnikController() {
	this.category = "registrovani_korisnik";
    }

}