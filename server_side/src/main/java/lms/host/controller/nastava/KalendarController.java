package lms.host.controller.nastava;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.Kalendar;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/kalendar")
@CrossOrigin(origins = "*")

public class KalendarController extends HostCrudController<Kalendar, Long> {

    public KalendarController() {
	this.category = "kalendar";
    }

}