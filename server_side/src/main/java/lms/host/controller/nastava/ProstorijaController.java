package lms.host.controller.nastava;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.Prostorija;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/prostorija")
@CrossOrigin(origins = "*")

public class ProstorijaController extends HostCrudController<Prostorija, Long> {

    public ProstorijaController() {
	this.category = "prostorija";
    }

}