package lms.host.controller.nastava;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.AutorNastavniMaterijal;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/autor_nastavni_materijal")
@CrossOrigin(origins = "*")

public class AutorNastavniMaterijalController extends HostCrudController<AutorNastavniMaterijal, Long> {

    public AutorNastavniMaterijalController() {
	this.category = "autor_nastavni_materijal";
    }

}