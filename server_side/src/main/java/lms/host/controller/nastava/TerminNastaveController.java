package lms.host.controller.nastava;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.TerminNastave;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/termin_nastave")
@CrossOrigin(origins = "*")

public class TerminNastaveController extends HostCrudController<TerminNastave, Long> {

    public TerminNastaveController() {
	this.category = "termin_nastave";
    }

}