package lms.host.controller.nastava;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.Konsultacija;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/konsultacija")
@CrossOrigin(origins = "*")

public class KonsultacijaController extends HostCrudController<Konsultacija, Long> {

    public KonsultacijaController() {
	this.category = "konsultacija";
    }

}