package lms.host.controller.nastava;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.Dogadjaj;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/dogadjaj")
@CrossOrigin(origins = "*")

public class DogadjajController extends HostCrudController<Dogadjaj, Long> {

    public DogadjajController() {
	this.category = "dogadjaj";
    }

}