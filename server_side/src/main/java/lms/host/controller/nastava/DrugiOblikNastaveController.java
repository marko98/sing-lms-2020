package lms.host.controller.nastava;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.DrugiOblikNastave;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/drugi_oblik_nastave")
@CrossOrigin(origins = "*")

public class DrugiOblikNastaveController extends HostCrudController<DrugiOblikNastave, Long> {

    public DrugiOblikNastaveController() {
	this.category = "drugi_oblik_nastave";
    }

}