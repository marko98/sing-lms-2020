package lms.host.controller.nastava;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.DogadjajKalendar;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/dogadjaj_kalendar")
@CrossOrigin(origins = "*")

public class DogadjajKalendarController extends HostCrudController<DogadjajKalendar, Long> {

    public DogadjajKalendarController() {
	this.category = "dogadjaj_kalendar";
    }

}