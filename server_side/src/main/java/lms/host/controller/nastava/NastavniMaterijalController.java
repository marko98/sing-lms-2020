package lms.host.controller.nastava;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.NastavniMaterijal;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/nastavni_materijal")
@CrossOrigin(origins = "*")

public class NastavniMaterijalController extends HostCrudController<NastavniMaterijal, Long> {

    public NastavniMaterijalController() {
	this.category = "nastavni_materijal";
    }

}