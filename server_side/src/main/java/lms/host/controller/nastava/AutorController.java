package lms.host.controller.nastava;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.Autor;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/autor")
@CrossOrigin(origins = "*")

public class AutorController extends HostCrudController<Autor, Long> {

    public AutorController() {
	this.category = "autor";
    }

}