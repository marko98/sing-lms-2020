package lms.host.controller.nastava;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.StudijskiProgram;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/studijski_program")
@CrossOrigin(origins = "*")

public class StudijskiProgramController extends HostCrudController<StudijskiProgram, Long> {

    public StudijskiProgramController() {
	this.category = "studijski_program";
    }

}