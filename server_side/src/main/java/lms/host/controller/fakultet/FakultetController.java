package lms.host.controller.fakultet;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.Fakultet;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/fakultet")
@CrossOrigin(origins = "*")
public class FakultetController extends HostCrudController<Fakultet, Long> {

    public FakultetController() {
	this.category = "fakultet";
    }

}