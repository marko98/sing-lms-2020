package lms.host.controller.fakultet;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.Univerzitet;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/univerzitet")
@CrossOrigin(origins = "*")
public class UniverzitetController extends HostCrudController<Univerzitet, Long> {

    public UniverzitetController() {
	this.category = "univerzitet";
    }

}