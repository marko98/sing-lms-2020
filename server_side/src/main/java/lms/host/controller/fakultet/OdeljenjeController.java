package lms.host.controller.fakultet;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.Odeljenje;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/odeljenje")
@CrossOrigin(origins = "*")
public class OdeljenjeController extends HostCrudController<Odeljenje, Long> {

    public OdeljenjeController() {
	this.category = "odeljenje";
    }

}