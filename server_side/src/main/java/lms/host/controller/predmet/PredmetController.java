package lms.host.controller.predmet;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.Predmet;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/predmet")
@CrossOrigin(origins = "*")

public class PredmetController extends HostCrudController<Predmet, Long> {

    public PredmetController() {
	this.category = "predmet";
    }

}