package lms.host.controller.predmet;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.Silabus;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/silabus")
@CrossOrigin(origins = "*")

public class SilabusController extends HostCrudController<Silabus, Long> {

    public SilabusController() {
	this.category = "silabus";
    }

}