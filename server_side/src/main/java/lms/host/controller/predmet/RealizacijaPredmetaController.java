package lms.host.controller.predmet;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.RealizacijaPredmeta;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/realizacija_predmeta")
@CrossOrigin(origins = "*")

public class RealizacijaPredmetaController extends HostCrudController<RealizacijaPredmeta, Long> {

    public RealizacijaPredmetaController() {
	this.category = "realizacija_predmeta";
    }

}