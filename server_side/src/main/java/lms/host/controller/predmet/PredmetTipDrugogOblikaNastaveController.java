package lms.host.controller.predmet;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.PredmetTipDrugogOblikaNastave;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/predmet_tip_drugog_oblika_nastave")
@CrossOrigin(origins = "*")

public class PredmetTipDrugogOblikaNastaveController extends HostCrudController<PredmetTipDrugogOblikaNastave, Long> {

    public PredmetTipDrugogOblikaNastaveController() {
	this.category = "predmet_tip_drugog_oblika_nastave";
    }

}