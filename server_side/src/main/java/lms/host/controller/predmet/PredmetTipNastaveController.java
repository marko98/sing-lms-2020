package lms.host.controller.predmet;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.PredmetTipNastave;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/predmet_tip_nastave")
@CrossOrigin(origins = "*")

public class PredmetTipNastaveController extends HostCrudController<PredmetTipNastave, Long> {

    public PredmetTipNastaveController() {
	this.category = "predmet_tip_nastave";
    }

}