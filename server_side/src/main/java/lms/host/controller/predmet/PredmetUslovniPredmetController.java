package lms.host.controller.predmet;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.PredmetUslovniPredmet;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/predmet_uslovni_predmet")
@CrossOrigin(origins = "*")

public class PredmetUslovniPredmetController extends HostCrudController<PredmetUslovniPredmet, Long> {

    public PredmetUslovniPredmetController() {
	this.category = "predmet_uslovni_predmet";
    }

}