package lms.host.controller.predmet;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.ObrazovniCilj;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/obrazovni_cilj")
@CrossOrigin(origins = "*")

public class ObrazovniCiljController extends HostCrudController<ObrazovniCilj, Long> {

    public ObrazovniCiljController() {
	this.category = "obrazovni_cilj";
    }

}