package lms.host.controller.nastavnik;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.NaucnaOblast;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/naucna_oblast")
@CrossOrigin(origins = "*")

public class NaucnaOblastController extends HostCrudController<NaucnaOblast, Long> {

    public NaucnaOblastController() {
	this.category = "naucna_oblast";
    }

}