package lms.host.controller.nastavnik;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.NastavnikNaRealizaciji;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/nastavnik_na_realizaciji")
@CrossOrigin(origins = "*")

public class NastavnikNaRealizacijiController extends HostCrudController<NastavnikNaRealizaciji, Long> {

    public NastavnikNaRealizacijiController() {
	this.category = "nastavnik_na_realizaciji";
    }

}