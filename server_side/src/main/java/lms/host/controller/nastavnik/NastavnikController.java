package lms.host.controller.nastavnik;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.Nastavnik;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/nastavnik")
@CrossOrigin(origins = "*")

public class NastavnikController extends HostCrudController<Nastavnik, Long> {

    public NastavnikController() {
	this.category = "nastavnik";
    }

}