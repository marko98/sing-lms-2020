package lms.host.controller.nastavnik;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.NastavnikFakultet;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/nastavnik_fakultet")
@CrossOrigin(origins = "*")

public class NastavnikFakultetController extends HostCrudController<NastavnikFakultet, Long> {

    public NastavnikFakultetController() {
	this.category = "nastavnik_fakultet";
    }

}