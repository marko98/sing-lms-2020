package lms.host.controller.nastavnik;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.Titula;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/titula")
@CrossOrigin(origins = "*")

public class TitulaController extends HostCrudController<Titula, Long> {

    public TitulaController() {
	this.category = "titula";
    }

}