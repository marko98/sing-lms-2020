package lms.host.controller.administracija;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.Administrator;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/administrator")
@CrossOrigin(origins = "*")
public class AdministratorController extends HostCrudController<Administrator, Long> {

    public AdministratorController() {
	this.category = "administrator";
    }

}