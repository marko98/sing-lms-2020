package lms.host.controller.administracija;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.StudentskiSluzbenik;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/studentski_sluzbenik")
@CrossOrigin(origins = "*")
public class StudentskiSluzbenikController extends HostCrudController<StudentskiSluzbenik, Long> {

    public StudentskiSluzbenikController() {
	this.category = "studentski_sluzbenik";
    }

}