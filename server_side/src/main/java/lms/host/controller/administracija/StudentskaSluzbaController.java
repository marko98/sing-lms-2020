package lms.host.controller.administracija;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.host.controller.HostCrudController;
import lms.model.StudentskaSluzba;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/studentska_sluzba")
@CrossOrigin(origins = "*")
public class StudentskaSluzbaController extends HostCrudController<StudentskaSluzba, Long> {

    public StudentskaSluzbaController() {
	this.category = "studentska_sluzba";
    }

}