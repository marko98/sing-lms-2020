package lms.model;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.FajlDTO;
import lms.enums.StanjeModela;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE fajl SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "fajl")
@Transactional
public class Fajl implements Entitet<Fajl, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -893495636539656799L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "fajl_id")
    private Long id;

    private String opis;

    private String url;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "obavestenje_id")
    private Obavestenje obavestenje;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "evaluacija_znanja_id")
    private EvaluacijaZnanja evaluacijaZnanja;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "nastavni_materijal_id")
    private NastavniMaterijal nastavniMaterijal;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getOpis() {
	return this.opis;
    }

    public void setOpis(String opis) {
	this.opis = opis;
    }

    public String getUrl() {
	return this.url;
    }

    public void setUrl(String url) {
	this.url = url;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public Obavestenje getObavestenje() {
	return this.obavestenje;
    }

    public void setObavestenje(Obavestenje obavestenje) {
	this.obavestenje = obavestenje;
    }

    public EvaluacijaZnanja getEvaluacijaZnanja() {
	return this.evaluacijaZnanja;
    }

    public void setEvaluacijaZnanja(EvaluacijaZnanja evaluacijaZnanja) {
	this.evaluacijaZnanja = evaluacijaZnanja;
    }

    public NastavniMaterijal getNastavniMaterijal() {
	return this.nastavniMaterijal;
    }

    public void setNastavniMaterijal(NastavniMaterijal nastavniMaterijal) {
	this.nastavniMaterijal = nastavniMaterijal;
    }

    public Fajl() {
	super();
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((evaluacijaZnanja == null) ? 0 : evaluacijaZnanja.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((nastavniMaterijal == null) ? 0 : nastavniMaterijal.hashCode());
	result = prime * result + ((obavestenje == null) ? 0 : obavestenje.hashCode());
	result = prime * result + ((opis == null) ? 0 : opis.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((url == null) ? 0 : url.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Fajl other = (Fajl) obj;
	if (evaluacijaZnanja == null) {
	    if (other.evaluacijaZnanja != null)
		return false;
	} else if (!evaluacijaZnanja.equals(other.evaluacijaZnanja))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (nastavniMaterijal == null) {
	    if (other.nastavniMaterijal != null)
		return false;
	} else if (!nastavniMaterijal.equals(other.nastavniMaterijal))
	    return false;
	if (obavestenje == null) {
	    if (other.obavestenje != null)
		return false;
	} else if (!obavestenje.equals(other.obavestenje))
	    return false;
	if (opis == null) {
	    if (other.opis != null)
		return false;
	} else if (!opis.equals(other.opis))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (url == null) {
	    if (other.url != null)
		return false;
	} else if (!url.equals(other.url))
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public FajlDTO getDTO() {
	FajlDTO fajlDTO = new FajlDTO();
	fajlDTO.setId(id);
	fajlDTO.setOpis(opis);
	fajlDTO.setStanje(stanje);
	fajlDTO.setUrl(url);
	fajlDTO.setVersion(version);

	if (this.evaluacijaZnanja != null)
	    fajlDTO.setEvaluacijaZnanjaDTO(this.evaluacijaZnanja.getDTOinsideDTO());
	if (this.nastavniMaterijal != null)
	    fajlDTO.setNastavniMaterijalDTO(this.nastavniMaterijal.getDTOinsideDTO());
	if (this.obavestenje != null)
	    fajlDTO.setObavestenjeDTO(this.obavestenje.getDTOinsideDTO());

	return fajlDTO;
    }

    @Override
    public FajlDTO getDTOinsideDTO() {
	FajlDTO fajlDTO = new FajlDTO();
	fajlDTO.setId(id);
	fajlDTO.setOpis(opis);
	fajlDTO.setStanje(stanje);
	fajlDTO.setUrl(url);
	fajlDTO.setVersion(version);
	return fajlDTO;
    }

    @Override
    public void update(Fajl entitet) {
	this.setOpis(entitet.getOpis());
	this.setStanje(entitet.getStanje());
	this.setUrl(entitet.getUrl());
	this.setVersion(entitet.getVersion());

	this.setEvaluacijaZnanja(entitet.getEvaluacijaZnanja());
	this.setNastavniMaterijal(entitet.getNastavniMaterijal());
	this.setObavestenje(entitet.getObavestenje());

    }

}