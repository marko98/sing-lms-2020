package lms.model;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.StudentskaSluzbaDTO;
import lms.dto.StudentskiSluzbenikDTO;
import lms.enums.StanjeModela;
import lms.interfaces.Identifikacija;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE studentska_sluzba SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "studentska_sluzba")
@Transactional
public class StudentskaSluzba implements Entitet<StudentskaSluzba, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -7753433415275476020L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "studentska_sluzba_id")
    private Long id;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.OneToMany(mappedBy = "studentskaSluzba")
    private java.util.List<StudentskiSluzbenik> studentskiSluzbenici;

    @javax.persistence.OneToOne(mappedBy = "studentskaSluzba")
    private Univerzitet univerzitet;

    @javax.persistence.OneToOne(mappedBy = "studentskaSluzba")
    private Odeljenje odeljenje;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public java.util.List<StudentskiSluzbenik> getStudentskiSluzbenici() {
	return this.studentskiSluzbenici;
    }

    public void setStudentskiSluzbenici(java.util.List<StudentskiSluzbenik> studentskiSluzbenici) {
	this.studentskiSluzbenici = studentskiSluzbenici;
    }

    public Univerzitet getUniverzitet() {
	return this.univerzitet;
    }

    public void setUniverzitet(Univerzitet univerzitet) {
	this.univerzitet = univerzitet;
    }

    public Odeljenje getOdeljenje() {
	return this.odeljenje;
    }

    public void setOdeljenje(Odeljenje odeljenje) {
	this.odeljenje = odeljenje;
    }

    public StudentskaSluzba() {
	super();
	this.studentskiSluzbenici = new java.util.ArrayList<>();
    }

    public StudentskiSluzbenik getStudentskiSluzbenik(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addStudentskiSluzbenik(StudentskiSluzbenik studentskiSluzbenik) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeStudentskiSluzbenik(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((odeljenje == null) ? 0 : odeljenje.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((studentskiSluzbenici == null) ? 0 : studentskiSluzbenici.hashCode());
	result = prime * result + ((univerzitet == null) ? 0 : univerzitet.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	StudentskaSluzba other = (StudentskaSluzba) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (odeljenje == null) {
	    if (other.odeljenje != null)
		return false;
	} else if (!odeljenje.equals(other.odeljenje))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (studentskiSluzbenici == null) {
	    if (other.studentskiSluzbenici != null)
		return false;
	} else if (!studentskiSluzbenici.equals(other.studentskiSluzbenici))
	    return false;
	if (univerzitet == null) {
	    if (other.univerzitet != null)
		return false;
	} else if (!univerzitet.equals(other.univerzitet))
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public StudentskaSluzbaDTO getDTO() {
	StudentskaSluzbaDTO studentskaSluzbaDTO = new StudentskaSluzbaDTO();
	studentskaSluzbaDTO.setId(id);
	studentskaSluzbaDTO.setStanje(stanje);
	studentskaSluzbaDTO.setVersion(version);

	if (this.odeljenje != null)
	    studentskaSluzbaDTO.setOdeljenjeDTO(this.odeljenje.getDTOinsideDTO());
	if (this.univerzitet != null)
	    studentskaSluzbaDTO.setUniverzitetDTO(this.univerzitet.getDTOinsideDTO());

	ArrayList<StudentskiSluzbenikDTO> studentskiSluzbeniciDTOs = new ArrayList<StudentskiSluzbenikDTO>();
	for (StudentskiSluzbenik studentskiSluzbenik : this.studentskiSluzbenici) {
	    studentskiSluzbeniciDTOs.add(studentskiSluzbenik.getDTOinsideDTO());
	}
	studentskaSluzbaDTO.setStudentskiSluzbeniciDTO(studentskiSluzbeniciDTOs);

	return studentskaSluzbaDTO;
    }

    @Override
    public StudentskaSluzbaDTO getDTOinsideDTO() {
	StudentskaSluzbaDTO studentskaSluzbaDTO = new StudentskaSluzbaDTO();
	studentskaSluzbaDTO.setId(id);
	studentskaSluzbaDTO.setStanje(stanje);
	studentskaSluzbaDTO.setVersion(version);
	return studentskaSluzbaDTO;
    }

    @Override
    public void update(StudentskaSluzba entitet) {
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());

	this.setOdeljenje(entitet.getOdeljenje());
	this.setUniverzitet(entitet.getUniverzitet());

    }

}