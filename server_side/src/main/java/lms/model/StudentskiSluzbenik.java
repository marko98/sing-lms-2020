package lms.model;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.StudentskiSluzbenikDTO;
import lms.enums.StanjeModela;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE studentski_sluzbenik SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "studentski_sluzbenik")
@Transactional
public class StudentskiSluzbenik implements Entitet<StudentskiSluzbenik, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 8410207627446604364L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "studentski_sluzbenik_id")
    private Long id;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.OneToOne
    private RegistrovaniKorisnik registrovaniKorisnik;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "studentska_sluzba_id")
    private StudentskaSluzba studentskaSluzba;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public RegistrovaniKorisnik getRegistrovaniKorisnik() {
	return this.registrovaniKorisnik;
    }

    public void setRegistrovaniKorisnik(RegistrovaniKorisnik registrovaniKorisnik) {
	this.registrovaniKorisnik = registrovaniKorisnik;
    }

    public StudentskaSluzba getStudentskaSluzba() {
	return this.studentskaSluzba;
    }

    public void setStudentskaSluzba(StudentskaSluzba studentskaSluzba) {
	this.studentskaSluzba = studentskaSluzba;
    }

    public StudentskiSluzbenik() {
	super();
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.NEAKTIVAN; // aktivira ga administrator
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((registrovaniKorisnik == null) ? 0 : registrovaniKorisnik.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((studentskaSluzba == null) ? 0 : studentskaSluzba.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	StudentskiSluzbenik other = (StudentskiSluzbenik) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (registrovaniKorisnik == null) {
	    if (other.registrovaniKorisnik != null)
		return false;
	} else if (!registrovaniKorisnik.equals(other.registrovaniKorisnik))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (studentskaSluzba == null) {
	    if (other.studentskaSluzba != null)
		return false;
	} else if (!studentskaSluzba.equals(other.studentskaSluzba))
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public StudentskiSluzbenikDTO getDTO() {
	StudentskiSluzbenikDTO studentskiSluzbenikDTO = new StudentskiSluzbenikDTO();
	studentskiSluzbenikDTO.setId(id);
	studentskiSluzbenikDTO.setStanje(stanje);
	studentskiSluzbenikDTO.setVersion(version);

	if (this.registrovaniKorisnik != null)
	    studentskiSluzbenikDTO.setRegistrovaniKorisnikDTO(this.registrovaniKorisnik.getDTOinsideDTO());
	if (this.studentskaSluzba != null)
	    studentskiSluzbenikDTO.setStudentskaSluzbaDTO(this.studentskaSluzba.getDTOinsideDTO());

	return studentskiSluzbenikDTO;
    }

    @Override
    public StudentskiSluzbenikDTO getDTOinsideDTO() {
	StudentskiSluzbenikDTO studentskiSluzbenikDTO = new StudentskiSluzbenikDTO();
	studentskiSluzbenikDTO.setId(id);
	studentskiSluzbenikDTO.setStanje(stanje);
	studentskiSluzbenikDTO.setVersion(version);
	return studentskiSluzbenikDTO;
    }

    @Override
    public void update(StudentskiSluzbenik entitet) {
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());

	this.setRegistrovaniKorisnik(entitet.getRegistrovaniKorisnik());
	this.setStudentskaSluzba(entitet.getStudentskaSluzba());

    }

}