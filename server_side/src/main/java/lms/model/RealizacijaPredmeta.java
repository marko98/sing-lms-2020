package lms.model;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.DrugiOblikNastaveDTO;
import lms.dto.EvaluacijaZnanjaDTO;
import lms.dto.IshodDTO;
import lms.dto.IstrazivackiRadDTO;
import lms.dto.NastavnikNaRealizacijiDTO;
import lms.dto.ObrazovniCiljDTO;
import lms.dto.PohadjanjePredmetaDTO;
import lms.dto.RealizacijaPredmetaDTO;
import lms.dto.TerminNastaveDTO;
import lms.enums.StanjeModela;
import lms.interfaces.Identifikacija;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE realizacija_predmeta SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "realizacija_predmeta")
@Transactional
public class RealizacijaPredmeta implements Entitet<RealizacijaPredmeta, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 2519724602243186591L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "realizacija_predmeta_id")
    private Long id;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.OneToMany(mappedBy = "realizacijaPredmeta")
    private java.util.List<DrugiOblikNastave> drugiObliciNastave;

    @javax.persistence.OneToMany(mappedBy = "realizacijaPredmeta")
    private java.util.List<NastavnikNaRealizaciji> nastavniciNaRealizaciji;

    @javax.persistence.OneToMany(mappedBy = "realizacijaPredmeta")
    private java.util.List<PohadjanjePredmeta> pohadjanjaPredmeta;

    @javax.persistence.OneToMany(mappedBy = "realizacijaPredmeta")
    private java.util.List<EvaluacijaZnanja> evaluacijeZnanja;

    @javax.persistence.OneToMany(mappedBy = "realizacijaPredmeta")
    private java.util.List<ObrazovniCilj> obrazovniCiljevi;

    @javax.persistence.OneToMany(mappedBy = "realizacijaPredmeta")
    private java.util.List<TerminNastave> terminiNastave;

    @javax.persistence.OneToMany(mappedBy = "realizacijaPredmeta")
    private java.util.List<Ishod> ishodi;

    @javax.persistence.OneToMany(mappedBy = "realizacijaPredmeta")
    private java.util.List<IstrazivackiRad> istrazivackiRadovi;

    @javax.persistence.OneToOne
    private Predmet predmet;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public java.util.List<DrugiOblikNastave> getDrugiObliciNastave() {
	return this.drugiObliciNastave;
    }

    public void setDrugiObliciNastave(java.util.List<DrugiOblikNastave> drugiObliciNastave) {
	this.drugiObliciNastave = drugiObliciNastave;
    }

    public java.util.List<NastavnikNaRealizaciji> getNastavniciNaRealizaciji() {
	return this.nastavniciNaRealizaciji;
    }

    public void setNastavniciNaRealizaciji(java.util.List<NastavnikNaRealizaciji> nastavniciNaRealizaciji) {
	this.nastavniciNaRealizaciji = nastavniciNaRealizaciji;
    }

    public java.util.List<PohadjanjePredmeta> getPohadjanjaPredmeta() {
	return this.pohadjanjaPredmeta;
    }

    public void setPohadjanjaPredmeta(java.util.List<PohadjanjePredmeta> pohadjanjaPredmeta) {
	this.pohadjanjaPredmeta = pohadjanjaPredmeta;
    }

    public java.util.List<EvaluacijaZnanja> getEvaluacijeZnanja() {
	return this.evaluacijeZnanja;
    }

    public void setEvaluacijeZnanja(java.util.List<EvaluacijaZnanja> evaluacijeZnanja) {
	this.evaluacijeZnanja = evaluacijeZnanja;
    }

    public java.util.List<ObrazovniCilj> getObrazovniCiljevi() {
	return this.obrazovniCiljevi;
    }

    public void setObrazovniCiljevi(java.util.List<ObrazovniCilj> obrazovniCiljevi) {
	this.obrazovniCiljevi = obrazovniCiljevi;
    }

    public java.util.List<TerminNastave> getTerminiNastave() {
	return this.terminiNastave;
    }

    public void setTerminiNastave(java.util.List<TerminNastave> terminiNastave) {
	this.terminiNastave = terminiNastave;
    }

    public java.util.List<Ishod> getIshodi() {
	return this.ishodi;
    }

    public void setIshodi(java.util.List<Ishod> ishodi) {
	this.ishodi = ishodi;
    }

    public java.util.List<IstrazivackiRad> getIstrazivackiRadovi() {
	return this.istrazivackiRadovi;
    }

    public void setIstrazivackiRadovi(java.util.List<IstrazivackiRad> istrazivackiRadovi) {
	this.istrazivackiRadovi = istrazivackiRadovi;
    }

    public Predmet getPredmet() {
	return this.predmet;
    }

    public void setPredmet(Predmet predmet) {
	this.predmet = predmet;
    }

    public RealizacijaPredmeta() {
	super();
	this.drugiObliciNastave = new java.util.ArrayList<>();
	this.nastavniciNaRealizaciji = new java.util.ArrayList<>();
	this.pohadjanjaPredmeta = new java.util.ArrayList<>();
	this.evaluacijeZnanja = new java.util.ArrayList<>();
	this.obrazovniCiljevi = new java.util.ArrayList<>();
	this.terminiNastave = new java.util.ArrayList<>();
	this.ishodi = new java.util.ArrayList<>();
	this.istrazivackiRadovi = new java.util.ArrayList<>();
    }

    public DrugiOblikNastave getDrugiOblikNastave(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addDrugiOblikNastave(DrugiOblikNastave drugiOblikNastave) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeDrugiOblikNastave(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public NastavnikNaRealizaciji getNastavnikNaRealizaciji(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addNastavnikNaRealizaciji(NastavnikNaRealizaciji nastavnikNaRealizaciji) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeNastavnikNaRealizaciji(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public PohadjanjePredmeta getPohadjanjePredmeta(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addPohadjanjePredmeta(PohadjanjePredmeta pohadjanjePredmeta) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removePohadjanjePredmeta(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public EvaluacijaZnanja getEvaluacijaZnanja(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addEvaluacijaZnanja(EvaluacijaZnanja evaluacijaZnanja) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeEvaluacijaZnanja(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public ObrazovniCilj getObrazovniCilj(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addObrazovniCilj(ObrazovniCilj obrazovniCilj) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeObrazovniCilj(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public TerminNastave getTerminNastave(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addTerminNastave(TerminNastave terminNastave) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeTerminNastave(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public Ishod getIshod(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addIshod(Ishod ishod) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeIshod(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public IstrazivackiRad getIstrazivackiRad(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addIstrazivackiRad(IstrazivackiRad istrazivackiRad) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeIstrazivackiRad(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((drugiObliciNastave == null) ? 0 : drugiObliciNastave.hashCode());
	result = prime * result + ((evaluacijeZnanja == null) ? 0 : evaluacijeZnanja.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((ishodi == null) ? 0 : ishodi.hashCode());
	result = prime * result + ((istrazivackiRadovi == null) ? 0 : istrazivackiRadovi.hashCode());
	result = prime * result + ((nastavniciNaRealizaciji == null) ? 0 : nastavniciNaRealizaciji.hashCode());
	result = prime * result + ((obrazovniCiljevi == null) ? 0 : obrazovniCiljevi.hashCode());
	result = prime * result + ((pohadjanjaPredmeta == null) ? 0 : pohadjanjaPredmeta.hashCode());
	result = prime * result + ((predmet == null) ? 0 : predmet.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((terminiNastave == null) ? 0 : terminiNastave.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	RealizacijaPredmeta other = (RealizacijaPredmeta) obj;
	if (drugiObliciNastave == null) {
	    if (other.drugiObliciNastave != null)
		return false;
	} else if (!drugiObliciNastave.equals(other.drugiObliciNastave))
	    return false;
	if (evaluacijeZnanja == null) {
	    if (other.evaluacijeZnanja != null)
		return false;
	} else if (!evaluacijeZnanja.equals(other.evaluacijeZnanja))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (ishodi == null) {
	    if (other.ishodi != null)
		return false;
	} else if (!ishodi.equals(other.ishodi))
	    return false;
	if (istrazivackiRadovi == null) {
	    if (other.istrazivackiRadovi != null)
		return false;
	} else if (!istrazivackiRadovi.equals(other.istrazivackiRadovi))
	    return false;
	if (nastavniciNaRealizaciji == null) {
	    if (other.nastavniciNaRealizaciji != null)
		return false;
	} else if (!nastavniciNaRealizaciji.equals(other.nastavniciNaRealizaciji))
	    return false;
	if (obrazovniCiljevi == null) {
	    if (other.obrazovniCiljevi != null)
		return false;
	} else if (!obrazovniCiljevi.equals(other.obrazovniCiljevi))
	    return false;
	if (pohadjanjaPredmeta == null) {
	    if (other.pohadjanjaPredmeta != null)
		return false;
	} else if (!pohadjanjaPredmeta.equals(other.pohadjanjaPredmeta))
	    return false;
	if (predmet == null) {
	    if (other.predmet != null)
		return false;
	} else if (!predmet.equals(other.predmet))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (terminiNastave == null) {
	    if (other.terminiNastave != null)
		return false;
	} else if (!terminiNastave.equals(other.terminiNastave))
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public RealizacijaPredmetaDTO getDTO() {
	RealizacijaPredmetaDTO realizacijaPredmetaDTO = new RealizacijaPredmetaDTO();
	realizacijaPredmetaDTO.setId(id);
	realizacijaPredmetaDTO.setStanje(stanje);
	realizacijaPredmetaDTO.setVersion(version);

	if (this.predmet != null)
	    realizacijaPredmetaDTO.setPredmetDTO(this.predmet.getDTOinsideDTO());

	ArrayList<DrugiOblikNastaveDTO> drugiObliciNastaveDTOs = new ArrayList<DrugiOblikNastaveDTO>();
	for (DrugiOblikNastave drugiOblikNastave : this.drugiObliciNastave) {
	    drugiObliciNastaveDTOs.add(drugiOblikNastave.getDTOinsideDTO());
	}
	realizacijaPredmetaDTO.setDrugiObliciNastaveDTO(drugiObliciNastaveDTOs);

	ArrayList<EvaluacijaZnanjaDTO> evaluacijeZnanjaDTOs = new ArrayList<EvaluacijaZnanjaDTO>();
	for (EvaluacijaZnanja evaluacijaZnanja : this.evaluacijeZnanja) {
	    evaluacijeZnanjaDTOs.add(evaluacijaZnanja.getDTOinsideDTO());
	}
	realizacijaPredmetaDTO.setEvaluacijeZnanjaDTO(evaluacijeZnanjaDTOs);

	ArrayList<IshodDTO> ishodiDTOs = new ArrayList<IshodDTO>();
	for (Ishod ishod : this.ishodi) {
	    ishodiDTOs.add(ishod.getDTOinsideDTO());
	}
	realizacijaPredmetaDTO.setIshodiDTO(ishodiDTOs);

	ArrayList<IstrazivackiRadDTO> istrazivackiRadoviDTOs = new ArrayList<IstrazivackiRadDTO>();
	for (IstrazivackiRad istrazivackiRad : this.istrazivackiRadovi) {
	    istrazivackiRadoviDTOs.add(istrazivackiRad.getDTOinsideDTO());
	}
	realizacijaPredmetaDTO.setIstrazivackiRadoviDTO(istrazivackiRadoviDTOs);

	ArrayList<NastavnikNaRealizacijiDTO> nastavniciNaRealizacijiDTOs = new ArrayList<NastavnikNaRealizacijiDTO>();
	for (NastavnikNaRealizaciji nastavnikNaRealizaciji : this.nastavniciNaRealizaciji) {
	    nastavniciNaRealizacijiDTOs.add(nastavnikNaRealizaciji.getDTOinsideDTO());
	}
	realizacijaPredmetaDTO.setNastavniciNaRealizacijiDTO(nastavniciNaRealizacijiDTOs);

	ArrayList<ObrazovniCiljDTO> obrazovniCiljeviDTOs = new ArrayList<ObrazovniCiljDTO>();
	for (ObrazovniCilj obrazovniCilj : this.obrazovniCiljevi) {
	    obrazovniCiljeviDTOs.add(obrazovniCilj.getDTOinsideDTO());
	}
	realizacijaPredmetaDTO.setObrazovniCiljeviDTO(obrazovniCiljeviDTOs);

	ArrayList<PohadjanjePredmetaDTO> pohadjanjaPredmetaDTOs = new ArrayList<PohadjanjePredmetaDTO>();
	for (PohadjanjePredmeta pohadjanjePredmeta : this.pohadjanjaPredmeta) {
	    pohadjanjaPredmetaDTOs.add(pohadjanjePredmeta.getDTOinsideDTO());
	}
	realizacijaPredmetaDTO.setPohadjanjaPredmetaDTO(pohadjanjaPredmetaDTOs);

	ArrayList<TerminNastaveDTO> terminiNastaveDTOs = new ArrayList<TerminNastaveDTO>();
	for (TerminNastave terminNastave : this.terminiNastave) {
	    terminiNastaveDTOs.add(terminNastave.getDTOinsideDTO());
	}
	realizacijaPredmetaDTO.setTerminiNastaveDTO(terminiNastaveDTOs);

	return realizacijaPredmetaDTO;
    }

    @Override
    public RealizacijaPredmetaDTO getDTOinsideDTO() {
	RealizacijaPredmetaDTO realizacijaPredmetaDTO = new RealizacijaPredmetaDTO();
	realizacijaPredmetaDTO.setId(id);
	realizacijaPredmetaDTO.setStanje(stanje);
	realizacijaPredmetaDTO.setVersion(version);
	return realizacijaPredmetaDTO;
    }

    @Override
    public void update(RealizacijaPredmeta entitet) {
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());

	this.setPredmet(entitet.getPredmet());

    }

}