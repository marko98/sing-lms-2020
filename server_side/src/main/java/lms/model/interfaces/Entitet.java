package lms.model.interfaces;

import java.io.Serializable;

import lms.dto.interfaces.DTO;
import lms.interfaces.Identifikacija;

public interface Entitet<T, ID extends Serializable> extends Identifikacija<ID> {

    public DTO<ID> getDTO();

    public DTO<ID> getDTOinsideDTO();

    public void update(T entitet);

}