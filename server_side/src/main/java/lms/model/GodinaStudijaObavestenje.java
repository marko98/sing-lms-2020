package lms.model;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.GodinaStudijaObavestenjeDTO;
import lms.enums.StanjeModela;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE godina_studija_obavestenje SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "godina_studija_obavestenje")
@Transactional
public class GodinaStudijaObavestenje implements Entitet<GodinaStudijaObavestenje, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -2033530115571874L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "godina_studija_obavestenje_id")
    private Long id;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "godina_studija_id")
    private GodinaStudija godinaStudija;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "obavestenje_id")
    private Obavestenje obavestenje;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public GodinaStudija getGodinaStudija() {
	return this.godinaStudija;
    }

    public void setGodinaStudija(GodinaStudija godinaStudija) {
	this.godinaStudija = godinaStudija;
    }

    public Obavestenje getObavestenje() {
	return this.obavestenje;
    }

    public void setObavestenje(Obavestenje obavestenje) {
	this.obavestenje = obavestenje;
    }

    public GodinaStudijaObavestenje() {
	super();
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((godinaStudija == null) ? 0 : godinaStudija.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((obavestenje == null) ? 0 : obavestenje.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	GodinaStudijaObavestenje other = (GodinaStudijaObavestenje) obj;
	if (godinaStudija == null) {
	    if (other.godinaStudija != null)
		return false;
	} else if (!godinaStudija.equals(other.godinaStudija))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (obavestenje == null) {
	    if (other.obavestenje != null)
		return false;
	} else if (!obavestenje.equals(other.obavestenje))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public GodinaStudijaObavestenjeDTO getDTO() {
	GodinaStudijaObavestenjeDTO godinaStudijaObavestenjeDTO = new GodinaStudijaObavestenjeDTO();
	godinaStudijaObavestenjeDTO.setId(id);
	godinaStudijaObavestenjeDTO.setStanje(stanje);
	godinaStudijaObavestenjeDTO.setVersion(version);

	if (this.godinaStudija != null)
	    godinaStudijaObavestenjeDTO.setGodinaStudijaDTO(this.godinaStudija.getDTOinsideDTO());
	if (this.obavestenje != null)
	    godinaStudijaObavestenjeDTO.setObavestenjeDTO(this.obavestenje.getDTOinsideDTO());

	return godinaStudijaObavestenjeDTO;
    }

    @Override
    public GodinaStudijaObavestenjeDTO getDTOinsideDTO() {
	GodinaStudijaObavestenjeDTO godinaStudijaObavestenjeDTO = new GodinaStudijaObavestenjeDTO();
	godinaStudijaObavestenjeDTO.setId(id);
	godinaStudijaObavestenjeDTO.setStanje(stanje);
	godinaStudijaObavestenjeDTO.setVersion(version);
	return godinaStudijaObavestenjeDTO;
    }

    @Override
    public void update(GodinaStudijaObavestenje entitet) {
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());

	this.setGodinaStudija(entitet.getGodinaStudija());
	this.setObavestenje(entitet.getObavestenje());

    }

}