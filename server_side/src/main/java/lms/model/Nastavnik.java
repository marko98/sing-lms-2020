package lms.model;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.DiplomskiRadDTO;
import lms.dto.FakultetDTO;
import lms.dto.IstrazivackiRadDTO;
import lms.dto.KonsultacijaDTO;
import lms.dto.NastavnikDTO;
import lms.dto.NastavnikDiplomskiRadDTO;
import lms.dto.NastavnikEvaluacijaZnanjaDTO;
import lms.dto.NastavnikFakultetDTO;
import lms.dto.StudijskiProgramDTO;
import lms.dto.TerminNastaveDTO;
import lms.dto.TitulaDTO;
import lms.dto.UniverzitetDTO;
import lms.enums.StanjeModela;
import lms.interfaces.Identifikacija;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE nastavnik SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "nastavnik")
@Transactional
public class Nastavnik implements Entitet<Nastavnik, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -3211898054225183655L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "nastavnik_id")
    private Long id;

    private String biografija;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.OneToMany(mappedBy = "rektor")
    private java.util.List<Univerzitet> univerziteti;

    @javax.persistence.OneToMany(mappedBy = "dekan")
    private java.util.List<Fakultet> fakulteti;

    @javax.persistence.OneToMany(mappedBy = "nastavnik")
    private java.util.List<NastavnikFakultet> nastavnikFakulteti;

    @javax.persistence.OneToMany(mappedBy = "nastavnik")
    private java.util.List<TerminNastave> terminiNastave;

    @javax.persistence.OneToMany(mappedBy = "rukovodilac")
    private java.util.List<StudijskiProgram> studijskiProgrami;

    @javax.persistence.OneToMany(mappedBy = "nastavnik")
    private java.util.List<Titula> titule;

    @javax.persistence.OneToOne
    private RegistrovaniKorisnik registrovaniKorisnik;

    @javax.persistence.OneToMany(mappedBy = "mentor")
    private java.util.List<DiplomskiRad> mentorDiplomskiRadovi;

    @javax.persistence.OneToMany(mappedBy = "nastavnik")
    private java.util.List<IstrazivackiRad> istrazivackiRadovi;

    @javax.persistence.OneToMany(mappedBy = "nastavnik")
    private java.util.List<Konsultacija> konsultacije;

    @javax.persistence.OneToMany(mappedBy = "nastavnik")
    private java.util.List<NastavnikEvaluacijaZnanja> nastavnikEvaluacijeZnanja;

    @javax.persistence.OneToMany(mappedBy = "nastavnikUKomisiji")
    private java.util.List<NastavnikDiplomskiRad> nastavnikDiplomskiRadovi;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getBiografija() {
	return this.biografija;
    }

    public void setBiografija(String biografija) {
	this.biografija = biografija;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public java.util.List<Univerzitet> getUniverzitet() {
	return this.univerziteti;
    }

    public void setUniverzitet(java.util.List<Univerzitet> univerziteti) {
	this.univerziteti = univerziteti;
    }

    public java.util.List<Fakultet> getFakultet() {
	return this.fakulteti;
    }

    public void setFakultet(java.util.List<Fakultet> fakulteti) {
	this.fakulteti = fakulteti;
    }

    public java.util.List<NastavnikFakultet> getNastavnikFakulteti() {
	return this.nastavnikFakulteti;
    }

    public void setNastavnikFakulteti(java.util.List<NastavnikFakultet> nastavnikFakulteti) {
	this.nastavnikFakulteti = nastavnikFakulteti;
    }

    public java.util.List<TerminNastave> getTerminiNastave() {
	return this.terminiNastave;
    }

    public void setTerminiNastave(java.util.List<TerminNastave> terminiNastave) {
	this.terminiNastave = terminiNastave;
    }

    public java.util.List<StudijskiProgram> getStudijskiProgrami() {
	return this.studijskiProgrami;
    }

    public void setStudijskiProgrami(java.util.List<StudijskiProgram> studijskiProgrami) {
	this.studijskiProgrami = studijskiProgrami;
    }

    public java.util.List<Titula> getTitule() {
	return this.titule;
    }

    public void setTitule(java.util.List<Titula> titule) {
	this.titule = titule;
    }

    public RegistrovaniKorisnik getRegistrovaniKorisnik() {
	return this.registrovaniKorisnik;
    }

    public void setRegistrovaniKorisnik(RegistrovaniKorisnik registrovaniKorisnik) {
	this.registrovaniKorisnik = registrovaniKorisnik;
    }

    public java.util.List<DiplomskiRad> getMentorDiplomskiRadovi() {
	return this.mentorDiplomskiRadovi;
    }

    public void setMentorDiplomskiRadovi(java.util.List<DiplomskiRad> mentorDiplomskiRadovi) {
	this.mentorDiplomskiRadovi = mentorDiplomskiRadovi;
    }

    public java.util.List<IstrazivackiRad> getIstrazivackiRadovi() {
	return this.istrazivackiRadovi;
    }

    public void setIstrazivackiRadovi(java.util.List<IstrazivackiRad> istrazivackiRadovi) {
	this.istrazivackiRadovi = istrazivackiRadovi;
    }

    public java.util.List<Konsultacija> getKonsultacije() {
	return this.konsultacije;
    }

    public void setKonsultacije(java.util.List<Konsultacija> konsultacije) {
	this.konsultacije = konsultacije;
    }

    public java.util.List<NastavnikEvaluacijaZnanja> getNastavnikEvaluacijeZnanja() {
	return this.nastavnikEvaluacijeZnanja;
    }

    public void setNastavnikEvaluacijeZnanja(java.util.List<NastavnikEvaluacijaZnanja> nastavnikEvaluacijeZnanja) {
	this.nastavnikEvaluacijeZnanja = nastavnikEvaluacijeZnanja;
    }

    public java.util.List<NastavnikDiplomskiRad> getNastavnikDiplomskiRadovi() {
	return this.nastavnikDiplomskiRadovi;
    }

    public void setNastavnikDiplomskiRadovi(java.util.List<NastavnikDiplomskiRad> nastavnikDiplomskiRadovi) {
	this.nastavnikDiplomskiRadovi = nastavnikDiplomskiRadovi;
    }

    public Nastavnik() {
	super();
	this.univerziteti = new java.util.ArrayList<>();
	this.fakulteti = new java.util.ArrayList<>();
	this.nastavnikFakulteti = new java.util.ArrayList<>();
	this.terminiNastave = new java.util.ArrayList<>();
	this.studijskiProgrami = new java.util.ArrayList<>();
	this.titule = new java.util.ArrayList<>();
	this.mentorDiplomskiRadovi = new java.util.ArrayList<>();
	this.istrazivackiRadovi = new java.util.ArrayList<>();
	this.konsultacije = new java.util.ArrayList<>();
	this.nastavnikEvaluacijeZnanja = new java.util.ArrayList<>();
	this.nastavnikDiplomskiRadovi = new java.util.ArrayList<>();
    }

    public Titula getTitula(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addTitula(Titula titula) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeTitula(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public NastavnikFakultet getNastavnikFakultet(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addNastavnikFakultet(NastavnikFakultet nastavnikFakultet) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeNastavnikFakultet(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public DiplomskiRad getMentorDiplomskiRad(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addMentorDiplomskiRad(DiplomskiRad mentorDiplomskiRad) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeMentorDiplomskiRad(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public IstrazivackiRad getIstrazivackiRad(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addIstrazivackiRad(IstrazivackiRad istrazivackiRad) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeIstrazivackiRad(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public Konsultacija getKonsultacija(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addKonsultacija(Konsultacija konsultacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeKonsultacija(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public NastavnikEvaluacijaZnanja getNastavnikEvaluacijaZnanja(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addNastavnikEvaluacijaZnanja(NastavnikEvaluacijaZnanja nastavnikEvaluacijaZnanja) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeNastavnikEvaluacijaZnanja(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public TerminNastave getTerminNastave(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addTerminNastave(TerminNastave terminNastave) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeTerminNastave(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public NastavnikDiplomskiRad getNastavnikDiplomskiRad(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addNastavnikDiplomskiRad(NastavnikDiplomskiRad nastavnikDiplomskiRad) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeNastavnikDiplomskiRad(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.NEAKTIVAN; // aktivira ga administrator
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((biografija == null) ? 0 : biografija.hashCode());
	result = prime * result + ((fakulteti == null) ? 0 : fakulteti.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((istrazivackiRadovi == null) ? 0 : istrazivackiRadovi.hashCode());
	result = prime * result + ((konsultacije == null) ? 0 : konsultacije.hashCode());
	result = prime * result + ((mentorDiplomskiRadovi == null) ? 0 : mentorDiplomskiRadovi.hashCode());
	result = prime * result + ((nastavnikDiplomskiRadovi == null) ? 0 : nastavnikDiplomskiRadovi.hashCode());
	result = prime * result + ((nastavnikEvaluacijeZnanja == null) ? 0 : nastavnikEvaluacijeZnanja.hashCode());
	result = prime * result + ((nastavnikFakulteti == null) ? 0 : nastavnikFakulteti.hashCode());
	result = prime * result + ((registrovaniKorisnik == null) ? 0 : registrovaniKorisnik.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((studijskiProgrami == null) ? 0 : studijskiProgrami.hashCode());
	result = prime * result + ((terminiNastave == null) ? 0 : terminiNastave.hashCode());
	result = prime * result + ((titule == null) ? 0 : titule.hashCode());
	result = prime * result + ((univerziteti == null) ? 0 : univerziteti.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Nastavnik other = (Nastavnik) obj;
	if (biografija == null) {
	    if (other.biografija != null)
		return false;
	} else if (!biografija.equals(other.biografija))
	    return false;
	if (fakulteti == null) {
	    if (other.fakulteti != null)
		return false;
	} else if (!fakulteti.equals(other.fakulteti))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (istrazivackiRadovi == null) {
	    if (other.istrazivackiRadovi != null)
		return false;
	} else if (!istrazivackiRadovi.equals(other.istrazivackiRadovi))
	    return false;
	if (konsultacije == null) {
	    if (other.konsultacije != null)
		return false;
	} else if (!konsultacije.equals(other.konsultacije))
	    return false;
	if (mentorDiplomskiRadovi == null) {
	    if (other.mentorDiplomskiRadovi != null)
		return false;
	} else if (!mentorDiplomskiRadovi.equals(other.mentorDiplomskiRadovi))
	    return false;
	if (nastavnikDiplomskiRadovi == null) {
	    if (other.nastavnikDiplomskiRadovi != null)
		return false;
	} else if (!nastavnikDiplomskiRadovi.equals(other.nastavnikDiplomskiRadovi))
	    return false;
	if (nastavnikEvaluacijeZnanja == null) {
	    if (other.nastavnikEvaluacijeZnanja != null)
		return false;
	} else if (!nastavnikEvaluacijeZnanja.equals(other.nastavnikEvaluacijeZnanja))
	    return false;
	if (nastavnikFakulteti == null) {
	    if (other.nastavnikFakulteti != null)
		return false;
	} else if (!nastavnikFakulteti.equals(other.nastavnikFakulteti))
	    return false;
	if (registrovaniKorisnik == null) {
	    if (other.registrovaniKorisnik != null)
		return false;
	} else if (!registrovaniKorisnik.equals(other.registrovaniKorisnik))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (studijskiProgrami == null) {
	    if (other.studijskiProgrami != null)
		return false;
	} else if (!studijskiProgrami.equals(other.studijskiProgrami))
	    return false;
	if (terminiNastave == null) {
	    if (other.terminiNastave != null)
		return false;
	} else if (!terminiNastave.equals(other.terminiNastave))
	    return false;
	if (titule == null) {
	    if (other.titule != null)
		return false;
	} else if (!titule.equals(other.titule))
	    return false;
	if (univerziteti == null) {
	    if (other.univerziteti != null)
		return false;
	} else if (!univerziteti.equals(other.univerziteti))
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public NastavnikDTO getDTO() {
	NastavnikDTO nastavnikDTO = new NastavnikDTO();
	nastavnikDTO.setId(id);
	nastavnikDTO.setBiografija(biografija);
	nastavnikDTO.setStanje(stanje);
	nastavnikDTO.setVersion(version);

	if (this.registrovaniKorisnik != null)
	    nastavnikDTO.setRegistrovaniKorisnikDTO(this.registrovaniKorisnik.getDTOinsideDTO());

	ArrayList<DiplomskiRadDTO> mentorDiplomskiRadoviDTOs = new ArrayList<DiplomskiRadDTO>();
	for (DiplomskiRad diplomskiRad : this.mentorDiplomskiRadovi) {
	    mentorDiplomskiRadoviDTOs.add(diplomskiRad.getDTOinsideDTO());
	}
	nastavnikDTO.setMentorDiplomskiRadoviDTO(mentorDiplomskiRadoviDTOs);

	ArrayList<FakultetDTO> fakultetiDTOs = new ArrayList<FakultetDTO>();
	for (Fakultet fakultet : this.fakulteti) {
	    fakultetiDTOs.add(fakultet.getDTOinsideDTO());
	}
	nastavnikDTO.setFakultetDTO(fakultetiDTOs);

	ArrayList<IstrazivackiRadDTO> istrazivackiRadoviDTOs = new ArrayList<IstrazivackiRadDTO>();
	for (IstrazivackiRad istrazivackiRad : this.istrazivackiRadovi) {
	    istrazivackiRadoviDTOs.add(istrazivackiRad.getDTOinsideDTO());
	}
	nastavnikDTO.setIstrazivackiRadoviDTO(istrazivackiRadoviDTOs);

	ArrayList<KonsultacijaDTO> konsultacijeDTOs = new ArrayList<KonsultacijaDTO>();
	for (Konsultacija konsultacija : this.konsultacije) {
	    konsultacijeDTOs.add(konsultacija.getDTOinsideDTO());
	}
	nastavnikDTO.setKonsultacijeDTO(konsultacijeDTOs);

	ArrayList<NastavnikDiplomskiRadDTO> nastavnikDiplomskiRadoviDTOs = new ArrayList<NastavnikDiplomskiRadDTO>();
	for (NastavnikDiplomskiRad nastavnikDiplomskiRad : this.nastavnikDiplomskiRadovi) {
	    nastavnikDiplomskiRadoviDTOs.add(nastavnikDiplomskiRad.getDTOinsideDTO());
	}
	nastavnikDTO.setNastavnikDiplomskiRadoviDTO(nastavnikDiplomskiRadoviDTOs);

	ArrayList<NastavnikEvaluacijaZnanjaDTO> nastavnikEvaluacijeZnanjaDTOs = new ArrayList<NastavnikEvaluacijaZnanjaDTO>();
	for (NastavnikEvaluacijaZnanja nastavnikEvaluacijaZnanja : this.nastavnikEvaluacijeZnanja) {
	    nastavnikEvaluacijeZnanjaDTOs.add(nastavnikEvaluacijaZnanja.getDTOinsideDTO());
	}
	nastavnikDTO.setNastavnikEvaluacijeZnanjaDTO(nastavnikEvaluacijeZnanjaDTOs);

	ArrayList<NastavnikFakultetDTO> nastavnikFakultetiDTOs = new ArrayList<NastavnikFakultetDTO>();
	for (NastavnikFakultet nastavnikFakultet : this.nastavnikFakulteti) {
	    nastavnikFakultetiDTOs.add(nastavnikFakultet.getDTOinsideDTO());
	}
	nastavnikDTO.setNastavnikFakultetiDTO(nastavnikFakultetiDTOs);

	ArrayList<StudijskiProgramDTO> studijskiProgramiDTOs = new ArrayList<StudijskiProgramDTO>();
	for (StudijskiProgram studijskiProgram : this.studijskiProgrami) {
	    studijskiProgramiDTOs.add(studijskiProgram.getDTOinsideDTO());
	}
	nastavnikDTO.setStudijskiProgramDTO(studijskiProgramiDTOs);

	ArrayList<TerminNastaveDTO> terminiNastaveDTOs = new ArrayList<TerminNastaveDTO>();
	for (TerminNastave terminNastave : this.terminiNastave) {
	    terminiNastaveDTOs.add(terminNastave.getDTOinsideDTO());
	}
	nastavnikDTO.setTerminiNastaveDTO(terminiNastaveDTOs);

	ArrayList<TitulaDTO> tituleDTOs = new ArrayList<TitulaDTO>();
	for (Titula titula : this.titule) {
	    tituleDTOs.add(titula.getDTOinsideDTO());
	}
	nastavnikDTO.setTituleDTO(tituleDTOs);

	ArrayList<UniverzitetDTO> univerzitetiDTOs = new ArrayList<UniverzitetDTO>();
	for (Univerzitet univerzitet : this.univerziteti) {
	    univerzitetiDTOs.add(univerzitet.getDTOinsideDTO());
	}
	nastavnikDTO.setUniverzitetDTO(univerzitetiDTOs);

	return nastavnikDTO;
    }

    @Override
    public NastavnikDTO getDTOinsideDTO() {
	NastavnikDTO nastavnikDTO = new NastavnikDTO();
	nastavnikDTO.setId(id);
	nastavnikDTO.setBiografija(biografija);
	nastavnikDTO.setStanje(stanje);
	nastavnikDTO.setVersion(version);
	return nastavnikDTO;
    }

    @Override
    public void update(Nastavnik entitet) {
	this.setBiografija(entitet.getBiografija());
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());

	this.setRegistrovaniKorisnik(entitet.getRegistrovaniKorisnik());
    }

}