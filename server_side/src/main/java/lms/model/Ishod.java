package lms.model;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.IshodDTO;
import lms.dto.NastavniMaterijalDTO;
import lms.dto.ObrazovniCiljDTO;
import lms.enums.StanjeModela;
import lms.interfaces.Identifikacija;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE ishod SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "ishod")
@Transactional
public class Ishod implements Entitet<Ishod, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -8444285409333986850L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "ishod_id")
    private Long id;

    private String opis;

    private String naslov;

    private String putanjaZaSliku;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "realizacija_predmeta_id")
    private RealizacijaPredmeta realizacijaPredmeta;

    @javax.persistence.OneToOne(mappedBy = "ishod")
    private EvaluacijaZnanja evaluacijaZnanja;

    @javax.persistence.OneToMany(mappedBy = "ishod")
    private java.util.List<ObrazovniCilj> obrazovniCiljevi;

    @javax.persistence.OneToMany(mappedBy = "ishod")
    private java.util.List<NastavniMaterijal> nastavniMaterijali;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getOpis() {
	return this.opis;
    }

    public void setOpis(String opis) {
	this.opis = opis;
    }

    public String getNaslov() {
	return this.naslov;
    }

    public void setNaslov(String naslov) {
	this.naslov = naslov;
    }

    public String getPutanjaZaSliku() {
	return this.putanjaZaSliku;
    }

    public void setPutanjaZaSliku(String putanjaZaSliku) {
	this.putanjaZaSliku = putanjaZaSliku;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public RealizacijaPredmeta getRealizacijaPredmeta() {
	return this.realizacijaPredmeta;
    }

    public void setRealizacijaPredmeta(RealizacijaPredmeta realizacijaPredmeta) {
	this.realizacijaPredmeta = realizacijaPredmeta;
    }

    public EvaluacijaZnanja getEvaluacijaZnanja() {
	return this.evaluacijaZnanja;
    }

    public void setEvaluacijaZnanja(EvaluacijaZnanja evaluacijaZnanja) {
	this.evaluacijaZnanja = evaluacijaZnanja;
    }

    public java.util.List<ObrazovniCilj> getObrazovniCiljevi() {
	return this.obrazovniCiljevi;
    }

    public void setObrazovniCiljevi(java.util.List<ObrazovniCilj> obrazovniCiljevi) {
	this.obrazovniCiljevi = obrazovniCiljevi;
    }

    public java.util.List<NastavniMaterijal> getNastavniMaterijali() {
	return this.nastavniMaterijali;
    }

    public void setNastavniMaterijali(java.util.List<NastavniMaterijal> nastavniMaterijali) {
	this.nastavniMaterijali = nastavniMaterijali;
    }

    public Ishod() {
	super();
	this.obrazovniCiljevi = new java.util.ArrayList<>();
	this.nastavniMaterijali = new java.util.ArrayList<>();
    }

    public ObrazovniCilj getObrazovniCilj(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addObrazovniCilj(ObrazovniCilj obrazovniCilj) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeObrazovniCilj(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public NastavniMaterijal getNastavniMaterijal(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addNastavniMaterijal(NastavniMaterijal nastavniMaterijal) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeNastavniMaterijal(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((evaluacijaZnanja == null) ? 0 : evaluacijaZnanja.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((naslov == null) ? 0 : naslov.hashCode());
	result = prime * result + ((nastavniMaterijali == null) ? 0 : nastavniMaterijali.hashCode());
	result = prime * result + ((obrazovniCiljevi == null) ? 0 : obrazovniCiljevi.hashCode());
	result = prime * result + ((opis == null) ? 0 : opis.hashCode());
	result = prime * result + ((putanjaZaSliku == null) ? 0 : putanjaZaSliku.hashCode());
	result = prime * result + ((realizacijaPredmeta == null) ? 0 : realizacijaPredmeta.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Ishod other = (Ishod) obj;
	if (evaluacijaZnanja == null) {
	    if (other.evaluacijaZnanja != null)
		return false;
	} else if (!evaluacijaZnanja.equals(other.evaluacijaZnanja))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (naslov == null) {
	    if (other.naslov != null)
		return false;
	} else if (!naslov.equals(other.naslov))
	    return false;
	if (nastavniMaterijali == null) {
	    if (other.nastavniMaterijali != null)
		return false;
	} else if (!nastavniMaterijali.equals(other.nastavniMaterijali))
	    return false;
	if (obrazovniCiljevi == null) {
	    if (other.obrazovniCiljevi != null)
		return false;
	} else if (!obrazovniCiljevi.equals(other.obrazovniCiljevi))
	    return false;
	if (opis == null) {
	    if (other.opis != null)
		return false;
	} else if (!opis.equals(other.opis))
	    return false;
	if (putanjaZaSliku == null) {
	    if (other.putanjaZaSliku != null)
		return false;
	} else if (!putanjaZaSliku.equals(other.putanjaZaSliku))
	    return false;
	if (realizacijaPredmeta == null) {
	    if (other.realizacijaPredmeta != null)
		return false;
	} else if (!realizacijaPredmeta.equals(other.realizacijaPredmeta))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public IshodDTO getDTO() {
	IshodDTO ishodDTO = new IshodDTO();
	ishodDTO.setId(id);
	ishodDTO.setNaslov(naslov);
	ishodDTO.setOpis(opis);
	ishodDTO.setPutanjaZaSliku(putanjaZaSliku);
	ishodDTO.setStanje(stanje);
	ishodDTO.setVersion(version);

	if (this.evaluacijaZnanja != null)
	    ishodDTO.setEvaluacijaZnanjaDTO(this.evaluacijaZnanja.getDTOinsideDTO());
	if (this.realizacijaPredmeta != null)
	    ishodDTO.setRealizacijaPredmetaDTO(this.realizacijaPredmeta.getDTOinsideDTO());

	ArrayList<NastavniMaterijalDTO> nastavniMaterijaliDTOs = new ArrayList<NastavniMaterijalDTO>();
	for (NastavniMaterijal nastavniMaterijal : this.nastavniMaterijali) {
	    nastavniMaterijaliDTOs.add(nastavniMaterijal.getDTOinsideDTO());
	}
	ishodDTO.setNastavniMaterijaliDTO(nastavniMaterijaliDTOs);

	ArrayList<ObrazovniCiljDTO> obrazovniCiljeviDTOs = new ArrayList<ObrazovniCiljDTO>();
	for (ObrazovniCilj obrazovniCilj : this.obrazovniCiljevi) {
	    obrazovniCiljeviDTOs.add(obrazovniCilj.getDTOinsideDTO());
	}
	ishodDTO.setObrazovniCiljeviDTO(obrazovniCiljeviDTOs);

	return ishodDTO;
    }

    @Override
    public IshodDTO getDTOinsideDTO() {
	IshodDTO ishodDTO = new IshodDTO();
	ishodDTO.setId(id);
	ishodDTO.setNaslov(naslov);
	ishodDTO.setOpis(opis);
	ishodDTO.setPutanjaZaSliku(putanjaZaSliku);
	ishodDTO.setStanje(stanje);
	ishodDTO.setVersion(version);
	return ishodDTO;
    }

    @Override
    public void update(Ishod entitet) {
	this.setNaslov(entitet.getNaslov());
	this.setOpis(entitet.getOpis());
	this.setPutanjaZaSliku(entitet.getPutanjaZaSliku());
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());

	this.setEvaluacijaZnanja(entitet.getEvaluacijaZnanja());
	this.setRealizacijaPredmeta(entitet.getRealizacijaPredmeta());

    }
}