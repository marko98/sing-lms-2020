package lms.model;

import java.time.LocalDateTime;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.AutorNastavniMaterijalDTO;
import lms.dto.FajlDTO;
import lms.dto.NastavniMaterijalDTO;
import lms.enums.StanjeModela;
import lms.interfaces.Identifikacija;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE nastavni_materijal SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "nastavni_materijal")
@Transactional
public class NastavniMaterijal implements Entitet<NastavniMaterijal, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -6647661259395230714L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "nastavni_materijal_id")
    private Long id;

    private String naziv;

    private LocalDateTime datum;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "ishod_id")
    private Ishod ishod;

    @javax.persistence.OneToMany(mappedBy = "nastavniMaterijal")
    private java.util.List<Fajl> fajlovi;

    @javax.persistence.OneToMany(mappedBy = "nastavniMaterijal")
    private java.util.List<AutorNastavniMaterijal> autoriNastavniMaterijal;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getNaziv() {
	return this.naziv;
    }

    public void setNaziv(String naziv) {
	this.naziv = naziv;
    }

    public LocalDateTime getDatum() {
	return this.datum;
    }

    public void setDatum(LocalDateTime datum) {
	this.datum = datum;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public Ishod getIshod() {
	return this.ishod;
    }

    public void setIshod(Ishod ishod) {
	this.ishod = ishod;
    }

    public java.util.List<Fajl> getFajlovi() {
	return this.fajlovi;
    }

    public void setFajlovi(java.util.List<Fajl> fajlovi) {
	this.fajlovi = fajlovi;
    }

    public java.util.List<AutorNastavniMaterijal> getAutoriNastavniMaterijal() {
	return this.autoriNastavniMaterijal;
    }

    public void setAutoriNastavniMaterijal(java.util.List<AutorNastavniMaterijal> autoriNastavniMaterijal) {
	this.autoriNastavniMaterijal = autoriNastavniMaterijal;
    }

    public NastavniMaterijal() {
	super();
	this.fajlovi = new java.util.ArrayList<>();
	this.autoriNastavniMaterijal = new java.util.ArrayList<>();
    }

    public Fajl getFajl(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addFajl(Fajl fajl) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeFajl(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public AutorNastavniMaterijal getAutorNastavniMaterijal(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addAutorNastavniMaterijal(AutorNastavniMaterijal autorNastavniMaterijal) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeAutorNastavniMaterijal(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((autoriNastavniMaterijal == null) ? 0 : autoriNastavniMaterijal.hashCode());
	result = prime * result + ((fajlovi == null) ? 0 : fajlovi.hashCode());
	result = prime * result + ((datum == null) ? 0 : datum.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((ishod == null) ? 0 : ishod.hashCode());
	result = prime * result + ((naziv == null) ? 0 : naziv.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	NastavniMaterijal other = (NastavniMaterijal) obj;
	if (autoriNastavniMaterijal == null) {
	    if (other.autoriNastavniMaterijal != null)
		return false;
	} else if (!autoriNastavniMaterijal.equals(other.autoriNastavniMaterijal))
	    return false;
	if (fajlovi == null) {
	    if (other.fajlovi != null)
		return false;
	} else if (!fajlovi.equals(other.fajlovi))
	    return false;
	if (datum == null) {
	    if (other.datum != null)
		return false;
	} else if (!datum.equals(other.datum))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (ishod == null) {
	    if (other.ishod != null)
		return false;
	} else if (!ishod.equals(other.ishod))
	    return false;
	if (naziv == null) {
	    if (other.naziv != null)
		return false;
	} else if (!naziv.equals(other.naziv))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public NastavniMaterijalDTO getDTO() {
	NastavniMaterijalDTO nastavniMaterijalDTO = new NastavniMaterijalDTO();
	nastavniMaterijalDTO.setId(id);
	nastavniMaterijalDTO.setDatum(datum);
	nastavniMaterijalDTO.setNaziv(naziv);
	nastavniMaterijalDTO.setStanje(stanje);
	nastavniMaterijalDTO.setVersion(version);

	if (this.ishod != null)
	    nastavniMaterijalDTO.setIshodDTO(this.ishod.getDTOinsideDTO());

	ArrayList<AutorNastavniMaterijalDTO> autoriNastavniMaterijalDTOs = new ArrayList<AutorNastavniMaterijalDTO>();
	for (AutorNastavniMaterijal autorNastavniMaterijal : this.autoriNastavniMaterijal) {
	    autoriNastavniMaterijalDTOs.add(autorNastavniMaterijal.getDTOinsideDTO());
	}
	nastavniMaterijalDTO.setAutoriNastavniMaterijalDTO(autoriNastavniMaterijalDTOs);

	ArrayList<FajlDTO> fajloviDTOs = new ArrayList<FajlDTO>();
	for (Fajl fajl : this.fajlovi) {
	    fajloviDTOs.add(fajl.getDTOinsideDTO());
	}
	nastavniMaterijalDTO.setFajloviDTO(fajloviDTOs);

	return nastavniMaterijalDTO;
    }

    @Override
    public NastavniMaterijalDTO getDTOinsideDTO() {
	NastavniMaterijalDTO nastavniMaterijalDTO = new NastavniMaterijalDTO();
	nastavniMaterijalDTO.setId(id);
	nastavniMaterijalDTO.setDatum(datum);
	nastavniMaterijalDTO.setNaziv(naziv);
	nastavniMaterijalDTO.setStanje(stanje);
	nastavniMaterijalDTO.setVersion(version);
	return nastavniMaterijalDTO;
    }

    @Override
    public void update(NastavniMaterijal entitet) {
	this.setDatum(entitet.getDatum());
	this.setNaziv(entitet.getNaziv());
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());

	this.setIshod(entitet.getIshod());
    }

}