package lms.model;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.DrzavaDTO;
import lms.dto.GradDTO;
import lms.enums.StanjeModela;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE drzava SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "drzava")
public class Drzava implements Entitet<Drzava, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 4647743630737861418L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "drzava_id")
    private Long id;

    private String naziv;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.OneToMany(mappedBy = "drzava")
    private java.util.List<Grad> gradovi;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getNaziv() {
	return this.naziv;
    }

    public void setNaziv(String naziv) {
	this.naziv = naziv;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public java.util.List<Grad> getGradovi() {
	return this.gradovi;
    }

    public void setGradovi(java.util.List<Grad> gradovi) {
	this.gradovi = gradovi;
    }

    public Drzava() {
	super();
	this.gradovi = new java.util.ArrayList<>();
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((gradovi == null) ? 0 : gradovi.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((naziv == null) ? 0 : naziv.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Drzava other = (Drzava) obj;
	if (gradovi == null) {
	    if (other.gradovi != null)
		return false;
	} else if (!gradovi.equals(other.gradovi))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (naziv == null) {
	    if (other.naziv != null)
		return false;
	} else if (!naziv.equals(other.naziv))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public DrzavaDTO getDTO() {
	DrzavaDTO drzavaDTO = new DrzavaDTO();
	drzavaDTO.setId(id);
	drzavaDTO.setNaziv(naziv);
	drzavaDTO.setStanje(stanje);
	drzavaDTO.setVersion(version);

	ArrayList<GradDTO> gradoviDTOs = new ArrayList<GradDTO>();
	for (Grad grad : this.gradovi) {
	    gradoviDTOs.add(grad.getDTOinsideDTO());
	}
	drzavaDTO.setGradoviDTO(gradoviDTOs);

	return drzavaDTO;
    }

    @Override
    public DrzavaDTO getDTOinsideDTO() {
	DrzavaDTO drzavaDTO = new DrzavaDTO();
	drzavaDTO.setId(id);
	drzavaDTO.setNaziv(naziv);
	drzavaDTO.setStanje(stanje);
	drzavaDTO.setVersion(version);
	return drzavaDTO;
    }

    @Override
    public void update(Drzava entitet) {
	this.setNaziv(entitet.getNaziv());
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());

    }

}