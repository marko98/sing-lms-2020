package lms.model;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.AdministratorDTO;
import lms.enums.StanjeModela;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE administrator SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "administrator")
@Transactional
public class Administrator implements Entitet<Administrator, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 6519253006393408576L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "administrator_id")
    private Long id;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.OneToOne
    private RegistrovaniKorisnik registrovaniKorisnik;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public RegistrovaniKorisnik getRegistrovaniKorisnik() {
	return this.registrovaniKorisnik;
    }

    public void setRegistrovaniKorisnik(RegistrovaniKorisnik registrovaniKorisnik) {
	this.registrovaniKorisnik = registrovaniKorisnik;
    }

    public Administrator() {
	super();
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.NEAKTIVAN; // aktivira ga administrator
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((registrovaniKorisnik == null) ? 0 : registrovaniKorisnik.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Administrator other = (Administrator) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (registrovaniKorisnik == null) {
	    if (other.registrovaniKorisnik != null)
		return false;
	} else if (!registrovaniKorisnik.equals(other.registrovaniKorisnik))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public AdministratorDTO getDTO() {
	AdministratorDTO administratorDTO = new AdministratorDTO();
	administratorDTO.setId(id);
	administratorDTO.setStanje(stanje);
	administratorDTO.setVersion(version);

	if (this.registrovaniKorisnik != null)
	    administratorDTO.setRegistrovaniKorisnikDTO(this.registrovaniKorisnik.getDTOinsideDTO());

	return administratorDTO;
    }

    @Override
    public AdministratorDTO getDTOinsideDTO() {
	AdministratorDTO administratorDTO = new AdministratorDTO();
	administratorDTO.setId(id);
	administratorDTO.setStanje(stanje);
	administratorDTO.setVersion(version);

	return administratorDTO;
    }

    @Override
    public void update(Administrator entitet) {
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());
	this.setRegistrovaniKorisnik(entitet.getRegistrovaniKorisnik());
    }

}