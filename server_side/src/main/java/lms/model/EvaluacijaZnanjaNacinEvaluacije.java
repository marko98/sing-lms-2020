package lms.model;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.EvaluacijaZnanjaNacinEvaluacijeDTO;
import lms.enums.NacinEvaluacije;
import lms.enums.StanjeModela;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE evaluacija_znanja_nacin_evaluacije SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "evaluacija_znanja_nacin_evaluacije")
@Transactional
public class EvaluacijaZnanjaNacinEvaluacije implements Entitet<EvaluacijaZnanjaNacinEvaluacije, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 1425910201658337866L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "evaluacija_znanja_nacin_evaluacije_id")
    private Long id;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "evaluacija_znanja_id")
    private EvaluacijaZnanja evaluacijaZnanja;

    @Enumerated(EnumType.STRING)
    private NacinEvaluacije nacinEvaluacije;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public EvaluacijaZnanja getEvaluacijaZnanja() {
	return this.evaluacijaZnanja;
    }

    public void setEvaluacijaZnanja(EvaluacijaZnanja evaluacijaZnanja) {
	this.evaluacijaZnanja = evaluacijaZnanja;
    }

    public NacinEvaluacije getNacinEvaluacije() {
	return this.nacinEvaluacije;
    }

    public void setNacinEvaluacije(NacinEvaluacije nacinEvaluacije) {
	this.nacinEvaluacije = nacinEvaluacije;
    }

    public EvaluacijaZnanjaNacinEvaluacije() {
	super();
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((evaluacijaZnanja == null) ? 0 : evaluacijaZnanja.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((nacinEvaluacije == null) ? 0 : nacinEvaluacije.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	EvaluacijaZnanjaNacinEvaluacije other = (EvaluacijaZnanjaNacinEvaluacije) obj;
	if (evaluacijaZnanja == null) {
	    if (other.evaluacijaZnanja != null)
		return false;
	} else if (!evaluacijaZnanja.equals(other.evaluacijaZnanja))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (nacinEvaluacije != other.nacinEvaluacije)
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public EvaluacijaZnanjaNacinEvaluacijeDTO getDTO() {
	EvaluacijaZnanjaNacinEvaluacijeDTO evaluacijaZnanjaNacinEvaluacijeDTO = new EvaluacijaZnanjaNacinEvaluacijeDTO();
	evaluacijaZnanjaNacinEvaluacijeDTO.setId(id);
	evaluacijaZnanjaNacinEvaluacijeDTO.setNacinEvaluacije(nacinEvaluacije);
	evaluacijaZnanjaNacinEvaluacijeDTO.setStanje(stanje);
	evaluacijaZnanjaNacinEvaluacijeDTO.setVersion(version);

	if (this.evaluacijaZnanja != null)
	    evaluacijaZnanjaNacinEvaluacijeDTO.setEvaluacijaZnanjaDTO(this.evaluacijaZnanja.getDTOinsideDTO());

	return evaluacijaZnanjaNacinEvaluacijeDTO;
    }

    @Override
    public EvaluacijaZnanjaNacinEvaluacijeDTO getDTOinsideDTO() {
	EvaluacijaZnanjaNacinEvaluacijeDTO evaluacijaZnanjaNacinEvaluacijeDTO = new EvaluacijaZnanjaNacinEvaluacijeDTO();
	evaluacijaZnanjaNacinEvaluacijeDTO.setId(id);
	evaluacijaZnanjaNacinEvaluacijeDTO.setNacinEvaluacije(nacinEvaluacije);
	evaluacijaZnanjaNacinEvaluacijeDTO.setStanje(stanje);
	evaluacijaZnanjaNacinEvaluacijeDTO.setVersion(version);
	return evaluacijaZnanjaNacinEvaluacijeDTO;
    }

    @Override
    public void update(EvaluacijaZnanjaNacinEvaluacije entitet) {
	this.setNacinEvaluacije(entitet.getNacinEvaluacije());
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());

	this.setEvaluacijaZnanja(entitet.getEvaluacijaZnanja());

    }

}