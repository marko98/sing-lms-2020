package lms.model;

import java.time.LocalDateTime;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.GodinaStudijaDTO;
import lms.dto.GodinaStudijaObavestenjeDTO;
import lms.dto.KonsultacijaDTO;
import lms.dto.PredmetDTO;
import lms.dto.StudentNaStudijiDTO;
import lms.enums.Semestar;
import lms.enums.StanjeModela;
import lms.interfaces.Identifikacija;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE godina_studija SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "godina_studija")
@Transactional
public class GodinaStudija implements Entitet<GodinaStudija, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 2474510020413586820L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "godina_studija_id")
    private Long id;

    private LocalDateTime godina;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.OneToMany(mappedBy = "godinaStudija")
    private java.util.List<StudentNaStudiji> studentiNaStudiji;

    @Enumerated(EnumType.STRING)
    private Semestar semestar;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "studijski_program_id")
    private StudijskiProgram studijskiProgram;

    @javax.persistence.OneToMany(mappedBy = "godinaStudija")
    private java.util.List<Konsultacija> konsultacije;

    @javax.persistence.OneToMany(mappedBy = "godinaStudija")
    private java.util.List<GodinaStudijaObavestenje> godinaStudijaObavestenja;

    @javax.persistence.OneToMany(mappedBy = "godinaStudija")
    private java.util.List<Predmet> predmeti;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public LocalDateTime getGodina() {
	return this.godina;
    }

    public void setGodina(LocalDateTime godina) {
	this.godina = godina;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public java.util.List<StudentNaStudiji> getStudentiNaStudiji() {
	return this.studentiNaStudiji;
    }

    public void setStudentiNaStudiji(java.util.List<StudentNaStudiji> studentiNaStudiji) {
	this.studentiNaStudiji = studentiNaStudiji;
    }

    public Semestar getSemestar() {
	return this.semestar;
    }

    public void setSemestar(Semestar semestar) {
	this.semestar = semestar;
    }

    public StudijskiProgram getStudijskiProgram() {
	return this.studijskiProgram;
    }

    public void setStudijskiProgram(StudijskiProgram studijskiProgram) {
	this.studijskiProgram = studijskiProgram;
    }

    public java.util.List<Konsultacija> getKonsultacije() {
	return this.konsultacije;
    }

    public void setKonsultacije(java.util.List<Konsultacija> konsultacije) {
	this.konsultacije = konsultacije;
    }

    public java.util.List<GodinaStudijaObavestenje> getGodinaStudijaObavestenja() {
	return this.godinaStudijaObavestenja;
    }

    public void setGodinaStudijaObavestenja(java.util.List<GodinaStudijaObavestenje> godinaStudijaObavestenja) {
	this.godinaStudijaObavestenja = godinaStudijaObavestenja;
    }

    public java.util.List<Predmet> getPredmeti() {
	return this.predmeti;
    }

    public void setPredmeti(java.util.List<Predmet> predmeti) {
	this.predmeti = predmeti;
    }

    public GodinaStudija() {
	super();
	this.studentiNaStudiji = new java.util.ArrayList<>();
	this.konsultacije = new java.util.ArrayList<>();
	this.godinaStudijaObavestenja = new java.util.ArrayList<>();
	this.predmeti = new java.util.ArrayList<>();
    }

    public StudentNaStudiji getStudentNaStudiji(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addStudentNaStudiji(StudentNaStudiji studentNaStudiji) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeStudentNaStudiji(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public Konsultacija getKonsultacija(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addKonsultacija(Konsultacija konsultacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeKonsultacija(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public GodinaStudijaObavestenje getGodinaStudijaObavestenje(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addGodinaStudijaObavestenje(GodinaStudijaObavestenje godinaStudijaObavestenje) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeGodinaStudijaObavestenje(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public Predmet getPredmet(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addPredmet(Predmet predmet) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removePredmet(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((godina == null) ? 0 : godina.hashCode());
	result = prime * result + ((godinaStudijaObavestenja == null) ? 0 : godinaStudijaObavestenja.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((konsultacije == null) ? 0 : konsultacije.hashCode());
	result = prime * result + ((predmeti == null) ? 0 : predmeti.hashCode());
	result = prime * result + ((semestar == null) ? 0 : semestar.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((studentiNaStudiji == null) ? 0 : studentiNaStudiji.hashCode());
	result = prime * result + ((studijskiProgram == null) ? 0 : studijskiProgram.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	GodinaStudija other = (GodinaStudija) obj;
	if (godina == null) {
	    if (other.godina != null)
		return false;
	} else if (!godina.equals(other.godina))
	    return false;
	if (godinaStudijaObavestenja == null) {
	    if (other.godinaStudijaObavestenja != null)
		return false;
	} else if (!godinaStudijaObavestenja.equals(other.godinaStudijaObavestenja))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (konsultacije == null) {
	    if (other.konsultacije != null)
		return false;
	} else if (!konsultacije.equals(other.konsultacije))
	    return false;
	if (predmeti == null) {
	    if (other.predmeti != null)
		return false;
	} else if (!predmeti.equals(other.predmeti))
	    return false;
	if (semestar != other.semestar)
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (studentiNaStudiji == null) {
	    if (other.studentiNaStudiji != null)
		return false;
	} else if (!studentiNaStudiji.equals(other.studentiNaStudiji))
	    return false;
	if (studijskiProgram == null) {
	    if (other.studijskiProgram != null)
		return false;
	} else if (!studijskiProgram.equals(other.studijskiProgram))
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public GodinaStudijaDTO getDTO() {
	GodinaStudijaDTO godinaStudijaDTO = new GodinaStudijaDTO();
	godinaStudijaDTO.setId(id);
	godinaStudijaDTO.setGodina(godina);
	godinaStudijaDTO.setSemestar(semestar);
	godinaStudijaDTO.setStanje(stanje);
	godinaStudijaDTO.setVersion(version);

	if (this.studijskiProgram != null)
	    godinaStudijaDTO.setStudijskiProgramDTO(this.studijskiProgram.getDTOinsideDTO());

	ArrayList<GodinaStudijaObavestenjeDTO> godinaStudijaObavestenjaDTOs = new ArrayList<GodinaStudijaObavestenjeDTO>();
	for (GodinaStudijaObavestenje godinaStudijaObavestenje : this.godinaStudijaObavestenja) {
	    godinaStudijaObavestenjaDTOs.add(godinaStudijaObavestenje.getDTOinsideDTO());
	}
	godinaStudijaDTO.setGodinaStudijaObavestenjaDTO(godinaStudijaObavestenjaDTOs);

	ArrayList<KonsultacijaDTO> konsultacijeDTOs = new ArrayList<KonsultacijaDTO>();
	for (Konsultacija konsultacija : this.konsultacije) {
	    konsultacijeDTOs.add(konsultacija.getDTOinsideDTO());
	}
	godinaStudijaDTO.setKonsultacijeDTO(konsultacijeDTOs);

	ArrayList<PredmetDTO> predmetiDTOs = new ArrayList<PredmetDTO>();
	for (Predmet predmet : this.predmeti) {
	    predmetiDTOs.add(predmet.getDTOinsideDTO());
	}
	godinaStudijaDTO.setPredmetiDTO(predmetiDTOs);

	ArrayList<StudentNaStudijiDTO> studentiNaStudijiDTOs = new ArrayList<StudentNaStudijiDTO>();
	for (StudentNaStudiji studentNaStudiji : this.studentiNaStudiji) {
	    studentiNaStudijiDTOs.add(studentNaStudiji.getDTOinsideDTO());
	}
	godinaStudijaDTO.setStudentiNaStudijiDTO(studentiNaStudijiDTOs);

	return godinaStudijaDTO;
    }

    @Override
    public GodinaStudijaDTO getDTOinsideDTO() {
	GodinaStudijaDTO godinaStudijaDTO = new GodinaStudijaDTO();
	godinaStudijaDTO.setId(id);
	godinaStudijaDTO.setGodina(godina);
	godinaStudijaDTO.setSemestar(semestar);
	godinaStudijaDTO.setStanje(stanje);
	godinaStudijaDTO.setVersion(version);
	return godinaStudijaDTO;
    }

    @Override
    public void update(GodinaStudija entitet) {
	this.setGodina(entitet.getGodina());
	this.setSemestar(entitet.getSemestar());
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());

	this.setStudijskiProgram(entitet.getStudijskiProgram());

    }

}