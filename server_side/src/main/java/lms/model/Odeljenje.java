package lms.model;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.OdeljenjeDTO;
import lms.dto.ProstorijaDTO;
import lms.enums.StanjeModela;
import lms.enums.TipOdeljenja;
import lms.interfaces.Identifikacija;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE odeljenje SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "odeljenje")
@Transactional
public class Odeljenje implements Entitet<Odeljenje, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -3142281686054211030L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "odeljenje_id")
    private Long id;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @Enumerated(EnumType.STRING)
    private TipOdeljenja tip;

    @javax.persistence.OneToMany(mappedBy = "odeljenje")
    private java.util.List<Prostorija> prostorije;

    @javax.persistence.OneToOne
    private Adresa adresa;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "fakultet_id")
    private Fakultet fakultet;

    @javax.persistence.OneToOne
    private StudentskaSluzba studentskaSluzba;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public TipOdeljenja getTip() {
	return this.tip;
    }

    public void setTip(TipOdeljenja tip) {
	this.tip = tip;
    }

    public java.util.List<Prostorija> getProstorije() {
	return this.prostorije;
    }

    public void setProstorije(java.util.List<Prostorija> prostorije) {
	this.prostorije = prostorije;
    }

    public Adresa getAdresa() {
	return this.adresa;
    }

    public void setAdresa(Adresa adresa) {
	this.adresa = adresa;
    }

    public Fakultet getFakultet() {
	return this.fakultet;
    }

    public void setFakultet(Fakultet fakultet) {
	this.fakultet = fakultet;
    }

    public StudentskaSluzba getStudentskaSluzba() {
	return this.studentskaSluzba;
    }

    public void setStudentskaSluzba(StudentskaSluzba studentskaSluzba) {
	this.studentskaSluzba = studentskaSluzba;
    }

    public Odeljenje() {
	super();
	this.prostorije = new java.util.ArrayList<>();
    }

    public Prostorija getProstorija(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addProstorija(Prostorija prostorija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeProstorija(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((adresa == null) ? 0 : adresa.hashCode());
	result = prime * result + ((fakultet == null) ? 0 : fakultet.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((prostorije == null) ? 0 : prostorije.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((studentskaSluzba == null) ? 0 : studentskaSluzba.hashCode());
	result = prime * result + ((tip == null) ? 0 : tip.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Odeljenje other = (Odeljenje) obj;
	if (adresa == null) {
	    if (other.adresa != null)
		return false;
	} else if (!adresa.equals(other.adresa))
	    return false;
	if (fakultet == null) {
	    if (other.fakultet != null)
		return false;
	} else if (!fakultet.equals(other.fakultet))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (prostorije == null) {
	    if (other.prostorije != null)
		return false;
	} else if (!prostorije.equals(other.prostorije))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (studentskaSluzba == null) {
	    if (other.studentskaSluzba != null)
		return false;
	} else if (!studentskaSluzba.equals(other.studentskaSluzba))
	    return false;
	if (tip != other.tip)
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public OdeljenjeDTO getDTO() {
	OdeljenjeDTO odeljenjeDTO = new OdeljenjeDTO();
	odeljenjeDTO.setId(id);
	odeljenjeDTO.setStanje(stanje);
	odeljenjeDTO.setTip(tip);
	odeljenjeDTO.setVersion(version);

	if (this.adresa != null)
	    odeljenjeDTO.setAdresaDTO(this.adresa.getDTOinsideDTO());
	if (this.fakultet != null)
	    odeljenjeDTO.setFakultetDTO(this.fakultet.getDTOinsideDTO());
	if (this.studentskaSluzba != null)
	    odeljenjeDTO.setStudentskaSluzbaDTO(this.studentskaSluzba.getDTOinsideDTO());

	ArrayList<ProstorijaDTO> prostorijeDTOs = new ArrayList<ProstorijaDTO>();
	for (Prostorija prostorija : this.prostorije) {
	    prostorijeDTOs.add(prostorija.getDTOinsideDTO());
	}
	odeljenjeDTO.setProstorijeDTO(prostorijeDTOs);

	return odeljenjeDTO;
    }

    @Override
    public OdeljenjeDTO getDTOinsideDTO() {
	OdeljenjeDTO odeljenjeDTO = new OdeljenjeDTO();
	odeljenjeDTO.setId(id);
	odeljenjeDTO.setStanje(stanje);
	odeljenjeDTO.setTip(tip);
	odeljenjeDTO.setVersion(version);
	return odeljenjeDTO;
    }

    @Override
    public void update(Odeljenje entitet) {
	this.setStanje(entitet.getStanje());
	this.setTip(entitet.getTip());
	this.setVersion(entitet.getVersion());

	this.setAdresa(entitet.getAdresa());
	this.setFakultet(entitet.getFakultet());
	this.setStudentskaSluzba(entitet.getStudentskaSluzba());

    }

}