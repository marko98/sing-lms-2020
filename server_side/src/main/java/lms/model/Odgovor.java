package lms.model;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.OdgovorDTO;
import lms.enums.StanjeModela;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE odgovor SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "odgovor")
@Transactional
public class Odgovor implements Entitet<Odgovor, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 92891903404974795L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "odgovor_id")
    private Long id;

    private String sadrzaj;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "pitanje_id")
    private Pitanje pitanje;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getSadrzaj() {
	return this.sadrzaj;
    }

    public void setSadrzaj(String sadrzaj) {
	this.sadrzaj = sadrzaj;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public Pitanje getPitanje() {
	return this.pitanje;
    }

    public void setPitanje(Pitanje pitanje) {
	this.pitanje = pitanje;
    }

    public Odgovor() {
	super();
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((pitanje == null) ? 0 : pitanje.hashCode());
	result = prime * result + ((sadrzaj == null) ? 0 : sadrzaj.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Odgovor other = (Odgovor) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (pitanje == null) {
	    if (other.pitanje != null)
		return false;
	} else if (!pitanje.equals(other.pitanje))
	    return false;
	if (sadrzaj == null) {
	    if (other.sadrzaj != null)
		return false;
	} else if (!sadrzaj.equals(other.sadrzaj))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public OdgovorDTO getDTO() {
	OdgovorDTO odgovorDTO = new OdgovorDTO();
	odgovorDTO.setId(id);
	odgovorDTO.setSadrzaj(sadrzaj);
	odgovorDTO.setStanje(stanje);
	odgovorDTO.setVersion(version);

	if (this.pitanje != null)
	    odgovorDTO.setPitanjeDTO(this.pitanje.getDTOinsideDTO());

	return odgovorDTO;
    }

    @Override
    public OdgovorDTO getDTOinsideDTO() {
	OdgovorDTO odgovorDTO = new OdgovorDTO();
	odgovorDTO.setId(id);
	odgovorDTO.setSadrzaj(sadrzaj);
	odgovorDTO.setStanje(stanje);
	odgovorDTO.setVersion(version);
	return odgovorDTO;
    }

    @Override
    public void update(Odgovor entitet) {
	this.setSadrzaj(entitet.getSadrzaj());
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());

	this.setPitanje(entitet.getPitanje());

    }

}