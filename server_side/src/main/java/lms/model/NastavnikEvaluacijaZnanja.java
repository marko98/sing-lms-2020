package lms.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.NastavnikEvaluacijaZnanjaDTO;
import lms.enums.StanjeModela;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE nastavnik_evaluacija_znanja SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "nastavnik_evaluacija_znanja")
@Transactional
public class NastavnikEvaluacijaZnanja implements Entitet<NastavnikEvaluacijaZnanja, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -2970021093889654536L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "nastavnik_evaluacija_znanja_id")
    private Long id;

    private LocalDateTime datum;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "evaluacija_znanja_id")
    private EvaluacijaZnanja evaluacijaZnanja;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "nastavnik_id")
    private Nastavnik nastavnik;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public LocalDateTime getDatum() {
	return this.datum;
    }

    public void setDatum(LocalDateTime datum) {
	this.datum = datum;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public EvaluacijaZnanja getEvaluacijaZnanja() {
	return this.evaluacijaZnanja;
    }

    public void setEvaluacijaZnanja(EvaluacijaZnanja evaluacijaZnanja) {
	this.evaluacijaZnanja = evaluacijaZnanja;
    }

    public Nastavnik getNastavnik() {
	return this.nastavnik;
    }

    public void setNastavnik(Nastavnik nastavnik) {
	this.nastavnik = nastavnik;
    }

    public NastavnikEvaluacijaZnanja() {
	super();
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((datum == null) ? 0 : datum.hashCode());
	result = prime * result + ((evaluacijaZnanja == null) ? 0 : evaluacijaZnanja.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((nastavnik == null) ? 0 : nastavnik.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	NastavnikEvaluacijaZnanja other = (NastavnikEvaluacijaZnanja) obj;
	if (datum == null) {
	    if (other.datum != null)
		return false;
	} else if (!datum.equals(other.datum))
	    return false;
	if (evaluacijaZnanja == null) {
	    if (other.evaluacijaZnanja != null)
		return false;
	} else if (!evaluacijaZnanja.equals(other.evaluacijaZnanja))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (nastavnik == null) {
	    if (other.nastavnik != null)
		return false;
	} else if (!nastavnik.equals(other.nastavnik))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public NastavnikEvaluacijaZnanjaDTO getDTO() {
	NastavnikEvaluacijaZnanjaDTO nastavnikEvaluacijaZnanjaDTO = new NastavnikEvaluacijaZnanjaDTO();
	nastavnikEvaluacijaZnanjaDTO.setId(id);
	nastavnikEvaluacijaZnanjaDTO.setDatum(datum);
	nastavnikEvaluacijaZnanjaDTO.setStanje(stanje);
	nastavnikEvaluacijaZnanjaDTO.setVersion(version);

	if (this.evaluacijaZnanja != null)
	    nastavnikEvaluacijaZnanjaDTO.setEvaluacijaZnanjaDTO(this.evaluacijaZnanja.getDTOinsideDTO());
	if (this.nastavnik != null)
	    nastavnikEvaluacijaZnanjaDTO.setNastavnikDTO(this.nastavnik.getDTOinsideDTO());

	return nastavnikEvaluacijaZnanjaDTO;
    }

    @Override
    public NastavnikEvaluacijaZnanjaDTO getDTOinsideDTO() {
	NastavnikEvaluacijaZnanjaDTO nastavnikEvaluacijaZnanjaDTO = new NastavnikEvaluacijaZnanjaDTO();
	nastavnikEvaluacijaZnanjaDTO.setId(id);
	nastavnikEvaluacijaZnanjaDTO.setDatum(datum);
	nastavnikEvaluacijaZnanjaDTO.setStanje(stanje);
	nastavnikEvaluacijaZnanjaDTO.setVersion(version);
	return nastavnikEvaluacijaZnanjaDTO;
    }

    @Override
    public void update(NastavnikEvaluacijaZnanja entitet) {
	this.setDatum(entitet.getDatum());
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());

	this.setEvaluacijaZnanja(entitet.getEvaluacijaZnanja());
	this.setNastavnik(entitet.getNastavnik());

    }

}