package lms.model;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.NastavnikNaRealizacijiTipNastaveDTO;
import lms.enums.StanjeModela;
import lms.enums.TipNastave;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE nastavnik_na_realizaciji_tip_nastave SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "nastavnik_na_realizaciji_tip_nastave")
@Transactional
public class NastavnikNaRealizacijiTipNastave implements Entitet<NastavnikNaRealizacijiTipNastave, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -6019199787609012495L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "nastavnik_na_realizaciji_tip_nastave_id")
    private Long id;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "nastavnik_na_realizaciji_id")
    private NastavnikNaRealizaciji nastavnikNaRealizaciji;

    @Enumerated(EnumType.STRING)
    private TipNastave tipNastave;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public NastavnikNaRealizaciji getNastavnikNaRealizaciji() {
	return this.nastavnikNaRealizaciji;
    }

    public void setNastavnikNaRealizaciji(NastavnikNaRealizaciji nastavnikNaRealizaciji) {
	this.nastavnikNaRealizaciji = nastavnikNaRealizaciji;
    }

    public TipNastave getTipNastave() {
	return this.tipNastave;
    }

    public void setTipNastave(TipNastave tipNastave) {
	this.tipNastave = tipNastave;
    }

    public NastavnikNaRealizacijiTipNastave() {
	super();
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((nastavnikNaRealizaciji == null) ? 0 : nastavnikNaRealizaciji.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((tipNastave == null) ? 0 : tipNastave.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	NastavnikNaRealizacijiTipNastave other = (NastavnikNaRealizacijiTipNastave) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (nastavnikNaRealizaciji == null) {
	    if (other.nastavnikNaRealizaciji != null)
		return false;
	} else if (!nastavnikNaRealizaciji.equals(other.nastavnikNaRealizaciji))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (tipNastave != other.tipNastave)
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public NastavnikNaRealizacijiTipNastaveDTO getDTO() {
	NastavnikNaRealizacijiTipNastaveDTO nastavnikNaRealizacijiTipNastaveDTO = new NastavnikNaRealizacijiTipNastaveDTO();
	nastavnikNaRealizacijiTipNastaveDTO.setId(id);
	nastavnikNaRealizacijiTipNastaveDTO.setStanje(stanje);
	nastavnikNaRealizacijiTipNastaveDTO.setTipNastave(tipNastave);
	nastavnikNaRealizacijiTipNastaveDTO.setVersion(version);

	if (this.nastavnikNaRealizaciji != null)
	    nastavnikNaRealizacijiTipNastaveDTO
		    .setNastavnikNaRealizacijiDTO(this.nastavnikNaRealizaciji.getDTOinsideDTO());

	return nastavnikNaRealizacijiTipNastaveDTO;
    }

    @Override
    public NastavnikNaRealizacijiTipNastaveDTO getDTOinsideDTO() {
	NastavnikNaRealizacijiTipNastaveDTO nastavnikNaRealizacijiTipNastaveDTO = new NastavnikNaRealizacijiTipNastaveDTO();
	nastavnikNaRealizacijiTipNastaveDTO.setId(id);
	nastavnikNaRealizacijiTipNastaveDTO.setStanje(stanje);
	nastavnikNaRealizacijiTipNastaveDTO.setTipNastave(tipNastave);
	nastavnikNaRealizacijiTipNastaveDTO.setVersion(version);
	return nastavnikNaRealizacijiTipNastaveDTO;
    }

    @Override
    public void update(NastavnikNaRealizacijiTipNastave entitet) {
	this.setStanje(entitet.getStanje());
	this.setTipNastave(entitet.getTipNastave());
	this.setVersion(entitet.getVersion());

	this.setNastavnikNaRealizaciji(entitet.getNastavnikNaRealizaciji());
    }

}