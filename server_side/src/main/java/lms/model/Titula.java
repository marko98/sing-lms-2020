package lms.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.TitulaDTO;
import lms.enums.StanjeModela;
import lms.enums.TipTitule;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE titula SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "titula")
@Transactional
public class Titula implements Entitet<Titula, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -3864825822939570373L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "titula_id")
    private Long id;

    private LocalDateTime datumIzbora;

    private LocalDateTime datumPrestanka;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @Enumerated(EnumType.STRING)
    private TipTitule tip;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "naucna_oblast_id")
    private NaucnaOblast naucnaOblast;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "nastavnik_id")
    private Nastavnik nastavnik;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public LocalDateTime getDatumIzbora() {
	return this.datumIzbora;
    }

    public void setDatumIzbora(LocalDateTime datumIzbora) {
	this.datumIzbora = datumIzbora;
    }

    public LocalDateTime getDatumPrestanka() {
	return this.datumPrestanka;
    }

    public void setDatumPrestanka(LocalDateTime datumPrestanka) {
	this.datumPrestanka = datumPrestanka;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public TipTitule getTip() {
	return this.tip;
    }

    public void setTip(TipTitule tip) {
	this.tip = tip;
    }

    public NaucnaOblast getNaucnaOblast() {
	return this.naucnaOblast;
    }

    public void setNaucnaOblast(NaucnaOblast naucnaOblast) {
	this.naucnaOblast = naucnaOblast;
    }

    public Nastavnik getNastavnik() {
	return this.nastavnik;
    }

    public void setNastavnik(Nastavnik nastavnik) {
	this.nastavnik = nastavnik;
    }

    public Titula() {
	super();
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((datumIzbora == null) ? 0 : datumIzbora.hashCode());
	result = prime * result + ((datumPrestanka == null) ? 0 : datumPrestanka.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((nastavnik == null) ? 0 : nastavnik.hashCode());
	result = prime * result + ((naucnaOblast == null) ? 0 : naucnaOblast.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((tip == null) ? 0 : tip.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Titula other = (Titula) obj;
	if (datumIzbora == null) {
	    if (other.datumIzbora != null)
		return false;
	} else if (!datumIzbora.equals(other.datumIzbora))
	    return false;
	if (datumPrestanka == null) {
	    if (other.datumPrestanka != null)
		return false;
	} else if (!datumPrestanka.equals(other.datumPrestanka))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (nastavnik == null) {
	    if (other.nastavnik != null)
		return false;
	} else if (!nastavnik.equals(other.nastavnik))
	    return false;
	if (naucnaOblast == null) {
	    if (other.naucnaOblast != null)
		return false;
	} else if (!naucnaOblast.equals(other.naucnaOblast))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (tip != other.tip)
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public TitulaDTO getDTO() {
	TitulaDTO titulaDTO = new TitulaDTO();
	titulaDTO.setId(id);
	titulaDTO.setDatumIzbora(datumIzbora);
	titulaDTO.setDatumPrestanka(datumPrestanka);
	titulaDTO.setStanje(stanje);
	titulaDTO.setTip(tip);
	titulaDTO.setVersion(version);

	if (this.nastavnik != null)
	    titulaDTO.setNastavnikDTO(this.nastavnik.getDTOinsideDTO());
	if (this.naucnaOblast != null)
	    titulaDTO.setNaucnaOblastDTO(this.naucnaOblast.getDTOinsideDTO());

	return titulaDTO;
    }

    @Override
    public TitulaDTO getDTOinsideDTO() {
	TitulaDTO titulaDTO = new TitulaDTO();
	titulaDTO.setId(id);
	titulaDTO.setDatumIzbora(datumIzbora);
	titulaDTO.setDatumPrestanka(datumPrestanka);
	titulaDTO.setStanje(stanje);
	titulaDTO.setTip(tip);
	titulaDTO.setVersion(version);
	return titulaDTO;
    }

    @Override
    public void update(Titula entitet) {
	this.setDatumIzbora(entitet.getDatumIzbora());
	this.setDatumPrestanka(entitet.getDatumPrestanka());
	this.setStanje(entitet.getStanje());
	this.setTip(entitet.getTip());
	this.setVersion(entitet.getVersion());

	this.setNastavnik(entitet.getNastavnik());
	this.setNaucnaOblast(entitet.getNaucnaOblast());

    }

}