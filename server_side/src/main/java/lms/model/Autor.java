package lms.model;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.AutorDTO;
import lms.dto.AutorNastavniMaterijalDTO;
import lms.enums.StanjeModela;
import lms.interfaces.Identifikacija;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE autor SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "autor")
@Transactional
public class Autor implements Entitet<Autor, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 7433944523478526674L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "autor_id")
    private Long id;

    private String ime;

    private String prezime;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.OneToMany(mappedBy = "autor")
    private java.util.List<AutorNastavniMaterijal> autorNastavniMaterijali;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getIme() {
	return this.ime;
    }

    public void setIme(String ime) {
	this.ime = ime;
    }

    public String getPrezime() {
	return this.prezime;
    }

    public void setPrezime(String prezime) {
	this.prezime = prezime;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public java.util.List<AutorNastavniMaterijal> getAutorNastavniMaterijali() {
	return this.autorNastavniMaterijali;
    }

    public void setAutorNastavniMaterijali(java.util.List<AutorNastavniMaterijal> autorNastavniMaterijali) {
	this.autorNastavniMaterijali = autorNastavniMaterijali;
    }

    public Autor() {
	super();
	this.autorNastavniMaterijali = new java.util.ArrayList<>();
    }

    public AutorNastavniMaterijal getAutorNastavniMaterijal(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addAutorNastavniMaterijal(AutorNastavniMaterijal autorNastavniMaterijal) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeAutorNastavniMaterijal(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((autorNastavniMaterijali == null) ? 0 : autorNastavniMaterijali.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((ime == null) ? 0 : ime.hashCode());
	result = prime * result + ((prezime == null) ? 0 : prezime.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Autor other = (Autor) obj;
	if (autorNastavniMaterijali == null) {
	    if (other.autorNastavniMaterijali != null)
		return false;
	} else if (!autorNastavniMaterijali.equals(other.autorNastavniMaterijali))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (ime == null) {
	    if (other.ime != null)
		return false;
	} else if (!ime.equals(other.ime))
	    return false;
	if (prezime == null) {
	    if (other.prezime != null)
		return false;
	} else if (!prezime.equals(other.prezime))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public AutorDTO getDTO() {
	AutorDTO autorDTO = new AutorDTO();
	autorDTO.setId(id);
	autorDTO.setIme(ime);
	autorDTO.setPrezime(prezime);
	autorDTO.setStanje(stanje);
	autorDTO.setVersion(version);
	
	ArrayList<AutorNastavniMaterijalDTO> autorNastavniMaterijalDTOs = new ArrayList<AutorNastavniMaterijalDTO>();
	for (AutorNastavniMaterijal autorNastavniMaterijal : autorNastavniMaterijali) {
	    autorNastavniMaterijalDTOs.add(autorNastavniMaterijal.getDTOinsideDTO());
	}
	autorDTO.setAutorNastavniMaterijaliDTO(autorNastavniMaterijalDTOs);
	
	return autorDTO;
    }

    @Override
    public AutorDTO getDTOinsideDTO() {
	AutorDTO autorDTO = new AutorDTO();
	autorDTO.setId(id);
	autorDTO.setIme(ime);
	autorDTO.setPrezime(prezime);
	autorDTO.setStanje(stanje);
	autorDTO.setVersion(version);
	
	return autorDTO;
    }

    @Override
    public void update(Autor entitet) {
	this.setIme(entitet.getIme());
	this.setPrezime(entitet.getPrezime());
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());
    }

}