package lms.model;

import java.time.LocalDateTime;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.AdresaDTO;
import lms.dto.RegistrovaniKorisnikDTO;
import lms.enums.StanjeModela;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE registrovani_korisnik SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "registrovani_korisnik")
@Transactional
public class RegistrovaniKorisnik implements Entitet<RegistrovaniKorisnik, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -2781947607403256614L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "registrovani_korisnik_id")
    private Long id;

    private String korisnickoIme;

    private String lozinka;

    private String email;

    private LocalDateTime datumRodjenja;

    private String jmbg;

    private String ime;

    private String prezime;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.OneToMany(mappedBy = "registrovaniKorisnik")
    private java.util.List<Adresa> adrese;

    @javax.persistence.OneToOne(mappedBy = "registrovaniKorisnik")
    private Nastavnik nastavnik;

    @javax.persistence.OneToOne(mappedBy = "registrovaniKorisnik")
    private Administrator administrator;

    @javax.persistence.OneToOne(mappedBy = "registrovaniKorisnik")
    private StudentskiSluzbenik studentskiSluzbenik;

    @javax.persistence.OneToOne(mappedBy = "registrovaniKorisnik")
    private Student student;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getKorisnickoIme() {
	return this.korisnickoIme;
    }

    public void setKorisnickoIme(String korisnickoIme) {
	this.korisnickoIme = korisnickoIme;
    }

    public String getLozinka() {
	return this.lozinka;
    }

    public void setLozinka(String lozinka) {
	this.lozinka = lozinka;
    }

    public String getEmail() {
	return this.email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public LocalDateTime getDatumRodjenja() {
	return this.datumRodjenja;
    }

    public void setDatumRodjenja(LocalDateTime datumRodjenja) {
	this.datumRodjenja = datumRodjenja;
    }

    public String getJmbg() {
	return this.jmbg;
    }

    public void setJmbg(String jmbg) {
	this.jmbg = jmbg;
    }

    public String getIme() {
	return this.ime;
    }

    public void setIme(String ime) {
	this.ime = ime;
    }

    public String getPrezime() {
	return this.prezime;
    }

    public void setPrezime(String prezime) {
	this.prezime = prezime;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public java.util.List<Adresa> getAdrese() {
	return this.adrese;
    }

    public void setAdrese(java.util.List<Adresa> adrese) {
	this.adrese = adrese;
    }

    public Nastavnik getNastavnik() {
	return this.nastavnik;
    }

    public void setNastavnik(Nastavnik nastavnik) {
	this.nastavnik = nastavnik;
    }

    public Administrator getAdministrator() {
	return this.administrator;
    }

    public void setAdministrator(Administrator administrator) {
	this.administrator = administrator;
    }

    public StudentskiSluzbenik getStudentskiSluzbenik() {
	return this.studentskiSluzbenik;
    }

    public void setStudentskiSluzbenik(StudentskiSluzbenik studentskiSluzbenik) {
	this.studentskiSluzbenik = studentskiSluzbenik;
    }

    public Student getStudent() {
	return this.student;
    }

    public void setStudent(Student student) {
	this.student = student;
    }

    public RegistrovaniKorisnik() {
	super();
	this.adrese = new java.util.ArrayList<>();
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((administrator == null) ? 0 : administrator.hashCode());
	result = prime * result + ((adrese == null) ? 0 : adrese.hashCode());
	result = prime * result + ((datumRodjenja == null) ? 0 : datumRodjenja.hashCode());
	result = prime * result + ((email == null) ? 0 : email.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((ime == null) ? 0 : ime.hashCode());
	result = prime * result + ((jmbg == null) ? 0 : jmbg.hashCode());
	result = prime * result + ((korisnickoIme == null) ? 0 : korisnickoIme.hashCode());
	result = prime * result + ((lozinka == null) ? 0 : lozinka.hashCode());
	result = prime * result + ((nastavnik == null) ? 0 : nastavnik.hashCode());
	result = prime * result + ((prezime == null) ? 0 : prezime.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((student == null) ? 0 : student.hashCode());
	result = prime * result + ((studentskiSluzbenik == null) ? 0 : studentskiSluzbenik.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	RegistrovaniKorisnik other = (RegistrovaniKorisnik) obj;
	if (administrator == null) {
	    if (other.administrator != null)
		return false;
	} else if (!administrator.equals(other.administrator))
	    return false;
	if (adrese == null) {
	    if (other.adrese != null)
		return false;
	} else if (!adrese.equals(other.adrese))
	    return false;
	if (datumRodjenja == null) {
	    if (other.datumRodjenja != null)
		return false;
	} else if (!datumRodjenja.equals(other.datumRodjenja))
	    return false;
	if (email == null) {
	    if (other.email != null)
		return false;
	} else if (!email.equals(other.email))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (ime == null) {
	    if (other.ime != null)
		return false;
	} else if (!ime.equals(other.ime))
	    return false;
	if (jmbg == null) {
	    if (other.jmbg != null)
		return false;
	} else if (!jmbg.equals(other.jmbg))
	    return false;
	if (korisnickoIme == null) {
	    if (other.korisnickoIme != null)
		return false;
	} else if (!korisnickoIme.equals(other.korisnickoIme))
	    return false;
	if (lozinka == null) {
	    if (other.lozinka != null)
		return false;
	} else if (!lozinka.equals(other.lozinka))
	    return false;
	if (nastavnik == null) {
	    if (other.nastavnik != null)
		return false;
	} else if (!nastavnik.equals(other.nastavnik))
	    return false;
	if (prezime == null) {
	    if (other.prezime != null)
		return false;
	} else if (!prezime.equals(other.prezime))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (student == null) {
	    if (other.student != null)
		return false;
	} else if (!student.equals(other.student))
	    return false;
	if (studentskiSluzbenik == null) {
	    if (other.studentskiSluzbenik != null)
		return false;
	} else if (!studentskiSluzbenik.equals(other.studentskiSluzbenik))
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public RegistrovaniKorisnikDTO getDTO() {
	RegistrovaniKorisnikDTO registrovaniKorisnikDTO = new RegistrovaniKorisnikDTO();
	registrovaniKorisnikDTO.setId(id);
	registrovaniKorisnikDTO.setDatumRodjenja(datumRodjenja);
	registrovaniKorisnikDTO.setEmail(email);
	registrovaniKorisnikDTO.setIme(ime);
	registrovaniKorisnikDTO.setJmbg(jmbg);
	registrovaniKorisnikDTO.setKorisnickoIme(korisnickoIme);
	registrovaniKorisnikDTO.setLozinka(lozinka);
	registrovaniKorisnikDTO.setPrezime(prezime);
	registrovaniKorisnikDTO.setStanje(stanje);
	registrovaniKorisnikDTO.setVersion(version);

	if (this.administrator != null)
	    registrovaniKorisnikDTO.setAdministratorDTO(this.administrator.getDTOinsideDTO());
	if (this.nastavnik != null)
	    registrovaniKorisnikDTO.setNastavnikDTO(this.nastavnik.getDTOinsideDTO());
	if (this.studentskiSluzbenik != null)
	    registrovaniKorisnikDTO.setStudentskiSluzbenikDTO(this.studentskiSluzbenik.getDTOinsideDTO());
	if (this.student != null)
	    registrovaniKorisnikDTO.setStudentDTO(this.student.getDTOinsideDTO());

	ArrayList<AdresaDTO> adreseDTOs = new ArrayList<AdresaDTO>();
	for (Adresa adresa : this.adrese) {
	    adreseDTOs.add(adresa.getDTOinsideDTO());
	}
	registrovaniKorisnikDTO.setAdreseDTO(adreseDTOs);

	return registrovaniKorisnikDTO;
    }

    @Override
    public RegistrovaniKorisnikDTO getDTOinsideDTO() {
	RegistrovaniKorisnikDTO registrovaniKorisnikDTO = new RegistrovaniKorisnikDTO();
	registrovaniKorisnikDTO.setId(id);
	registrovaniKorisnikDTO.setDatumRodjenja(datumRodjenja);
	registrovaniKorisnikDTO.setEmail(email);
	registrovaniKorisnikDTO.setIme(ime);
	registrovaniKorisnikDTO.setJmbg(jmbg);
	registrovaniKorisnikDTO.setKorisnickoIme(korisnickoIme);
	registrovaniKorisnikDTO.setLozinka(lozinka);
	registrovaniKorisnikDTO.setPrezime(prezime);
	registrovaniKorisnikDTO.setStanje(stanje);
	registrovaniKorisnikDTO.setVersion(version);
	return registrovaniKorisnikDTO;
    }

    @Override
    public void update(RegistrovaniKorisnik entitet) {
	this.setDatumRodjenja(entitet.getDatumRodjenja());
	this.setEmail(entitet.getEmail());
	this.setIme(entitet.getIme());
	this.setJmbg(entitet.getJmbg());
	this.setKorisnickoIme(entitet.getKorisnickoIme());
	this.setLozinka(entitet.getLozinka());
	this.setPrezime(entitet.getPrezime());
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());

	this.setAdministrator(entitet.getAdministrator());
	this.setNastavnik(entitet.getNastavnik());
	this.setStudentskiSluzbenik(entitet.getStudentskiSluzbenik());
	this.setStudent(entitet.getStudent());

    }

}