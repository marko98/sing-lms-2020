package lms.model;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.ProstorijaDTO;
import lms.dto.TerminNastaveDTO;
import lms.enums.StanjeModela;
import lms.enums.TipProstorije;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE prostorija SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "prostorija")
@Transactional
public class Prostorija implements Entitet<Prostorija, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -1295340225504252784L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "prostorija_id")
    private Long id;

    private String naziv;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @Enumerated(EnumType.STRING)
    private TipProstorije tip;

    @javax.persistence.OneToMany(mappedBy = "prostorija")
    private java.util.List<TerminNastave> terminiNastave;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "odeljenje_id")
    private Odeljenje odeljenje;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getNaziv() {
	return this.naziv;
    }

    public void setNaziv(String naziv) {
	this.naziv = naziv;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public TipProstorije getTip() {
	return this.tip;
    }

    public void setTip(TipProstorije tip) {
	this.tip = tip;
    }

    public java.util.List<TerminNastave> getTerminiNastave() {
	return this.terminiNastave;
    }

    public void setTerminiNastave(java.util.List<TerminNastave> terminiNastave) {
	this.terminiNastave = terminiNastave;
    }

    public Odeljenje getOdeljenje() {
	return this.odeljenje;
    }

    public void setOdeljenje(Odeljenje odeljenje) {
	this.odeljenje = odeljenje;
    }

    public Prostorija() {
	super();
	this.terminiNastave = new java.util.ArrayList<>();
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((naziv == null) ? 0 : naziv.hashCode());
	result = prime * result + ((odeljenje == null) ? 0 : odeljenje.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((terminiNastave == null) ? 0 : terminiNastave.hashCode());
	result = prime * result + ((tip == null) ? 0 : tip.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Prostorija other = (Prostorija) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (naziv == null) {
	    if (other.naziv != null)
		return false;
	} else if (!naziv.equals(other.naziv))
	    return false;
	if (odeljenje == null) {
	    if (other.odeljenje != null)
		return false;
	} else if (!odeljenje.equals(other.odeljenje))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (terminiNastave == null) {
	    if (other.terminiNastave != null)
		return false;
	} else if (!terminiNastave.equals(other.terminiNastave))
	    return false;
	if (tip != other.tip)
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public ProstorijaDTO getDTO() {
	ProstorijaDTO prostorijaDTO = new ProstorijaDTO();
	prostorijaDTO.setId(id);
	prostorijaDTO.setNaziv(naziv);
	prostorijaDTO.setStanje(stanje);
	prostorijaDTO.setTip(tip);
	prostorijaDTO.setVersion(version);

	if (this.odeljenje != null)
	    prostorijaDTO.setOdeljenjeDTO(this.odeljenje.getDTOinsideDTO());

	ArrayList<TerminNastaveDTO> terminiNastaveDTOs = new ArrayList<TerminNastaveDTO>();
	for (TerminNastave terminNastave : this.terminiNastave) {
	    terminiNastaveDTOs.add(terminNastave.getDTOinsideDTO());
	}
	prostorijaDTO.setTerminiNastaveDTO(terminiNastaveDTOs);

	return prostorijaDTO;
    }

    @Override
    public ProstorijaDTO getDTOinsideDTO() {
	ProstorijaDTO prostorijaDTO = new ProstorijaDTO();
	prostorijaDTO.setId(id);
	prostorijaDTO.setNaziv(naziv);
	prostorijaDTO.setStanje(stanje);
	prostorijaDTO.setTip(tip);
	prostorijaDTO.setVersion(version);
	return prostorijaDTO;
    }

    @Override
    public void update(Prostorija entitet) {
	this.setNaziv(entitet.getNaziv());
	this.setStanje(entitet.getStanje());
	this.setTip(entitet.getTip());
	this.setVersion(entitet.getVersion());

	this.setOdeljenje(entitet.getOdeljenje());
    }

}