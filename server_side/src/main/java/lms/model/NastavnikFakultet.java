package lms.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.NastavnikFakultetDTO;
import lms.enums.StanjeModela;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE nastavnik_fakultet SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "nastavnik_fakultet")
@Transactional
public class NastavnikFakultet implements Entitet<NastavnikFakultet, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 500306613920490900L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "nastavnik_fakultet_id")
    private Long id;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    private LocalDateTime datum;

    @Version
    private Long version;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "fakultet_id")
    private Fakultet fakultet;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "nastavnik_id")
    private Nastavnik nastavnik;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public LocalDateTime getDatum() {
	return this.datum;
    }

    public void setDatum(LocalDateTime datum) {
	this.datum = datum;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public Fakultet getFakultet() {
	return this.fakultet;
    }

    public void setFakultet(Fakultet fakultet) {
	this.fakultet = fakultet;
    }

    public Nastavnik getNastavnik() {
	return this.nastavnik;
    }

    public void setNastavnik(Nastavnik nastavnik) {
	this.nastavnik = nastavnik;
    }

    public NastavnikFakultet() {
	super();
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((datum == null) ? 0 : datum.hashCode());
	result = prime * result + ((fakultet == null) ? 0 : fakultet.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((nastavnik == null) ? 0 : nastavnik.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	NastavnikFakultet other = (NastavnikFakultet) obj;
	if (datum == null) {
	    if (other.datum != null)
		return false;
	} else if (!datum.equals(other.datum))
	    return false;
	if (fakultet == null) {
	    if (other.fakultet != null)
		return false;
	} else if (!fakultet.equals(other.fakultet))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (nastavnik == null) {
	    if (other.nastavnik != null)
		return false;
	} else if (!nastavnik.equals(other.nastavnik))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public NastavnikFakultetDTO getDTO() {
	NastavnikFakultetDTO nastavnikFakultetDTO = new NastavnikFakultetDTO();
	nastavnikFakultetDTO.setId(id);
	nastavnikFakultetDTO.setDatum(datum);
	nastavnikFakultetDTO.setStanje(stanje);
	nastavnikFakultetDTO.setVersion(version);

	if (this.fakultet != null)
	    nastavnikFakultetDTO.setFakultetDTO(this.fakultet.getDTOinsideDTO());
	if (this.nastavnik != null)
	    nastavnikFakultetDTO.setNastavnikDTO(this.nastavnik.getDTOinsideDTO());

	return nastavnikFakultetDTO;
    }

    @Override
    public NastavnikFakultetDTO getDTOinsideDTO() {
	NastavnikFakultetDTO nastavnikFakultetDTO = new NastavnikFakultetDTO();
	nastavnikFakultetDTO.setId(id);
	nastavnikFakultetDTO.setDatum(datum);
	nastavnikFakultetDTO.setStanje(stanje);
	nastavnikFakultetDTO.setVersion(version);
	return nastavnikFakultetDTO;
    }

    @Override
    public void update(NastavnikFakultet entitet) {
	this.setDatum(entitet.getDatum());
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());

	this.setFakultet(entitet.getFakultet());
	this.setNastavnik(entitet.getNastavnik());

    }

}