package lms.model;

import java.time.LocalDateTime;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.EvaluacijaZnanjaDTO;
import lms.dto.EvaluacijaZnanjaNacinEvaluacijeDTO;
import lms.dto.FajlDTO;
import lms.dto.NastavnikEvaluacijaZnanjaDTO;
import lms.dto.PitanjeDTO;
import lms.dto.PolaganjeDTO;
import lms.enums.IspitniRok;
import lms.enums.StanjeModela;
import lms.enums.TipEvaluacije;
import lms.interfaces.Identifikacija;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE evaluacija_znanja SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "evaluacija_znanja")
@Transactional
public class EvaluacijaZnanja implements Entitet<EvaluacijaZnanja, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -8843960160527234173L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "evaluacija_znanja_id")
    private Long id;

    private LocalDateTime pocetak;

    private Integer trajanje;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.OneToMany(mappedBy = "evaluacijaZnanja")
    private java.util.List<NastavnikEvaluacijaZnanja> nastavniciEvaluacijaZnanja;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "realizacija_predmeta_id")
    private RealizacijaPredmeta realizacijaPredmeta;

    @javax.persistence.OneToMany(mappedBy = "evaluacijaZnanja")
    private java.util.List<Polaganje> polaganja;

    @javax.persistence.OneToOne
    private Ishod ishod;

    @javax.persistence.OneToMany(mappedBy = "evaluacijaZnanja")
    private java.util.List<Fajl> fajlovi;

    @Enumerated(EnumType.STRING)
    private TipEvaluacije tipEvaluacije;

    @javax.persistence.OneToMany(mappedBy = "evaluacijaZnanja")
    private java.util.List<Pitanje> pitanja;

    @Enumerated(EnumType.STRING)
    private IspitniRok ispitniRok;

    @javax.persistence.OneToMany(mappedBy = "evaluacijaZnanja")
    private java.util.List<EvaluacijaZnanjaNacinEvaluacije> evaluacijaZnanjaNaciniEvaluacije;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public LocalDateTime getPocetak() {
	return this.pocetak;
    }

    public void setPocetak(LocalDateTime pocetak) {
	this.pocetak = pocetak;
    }

    public Integer getTrajanje() {
	return this.trajanje;
    }

    public void setTrajanje(Integer trajanje) {
	this.trajanje = trajanje;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public java.util.List<NastavnikEvaluacijaZnanja> getNastavniciEvaluacijaZnanja() {
	return this.nastavniciEvaluacijaZnanja;
    }

    public void setNastavniciEvaluacijaZnanja(java.util.List<NastavnikEvaluacijaZnanja> nastavniciEvaluacijaZnanja) {
	this.nastavniciEvaluacijaZnanja = nastavniciEvaluacijaZnanja;
    }

    public RealizacijaPredmeta getRealizacijaPredmeta() {
	return this.realizacijaPredmeta;
    }

    public void setRealizacijaPredmeta(RealizacijaPredmeta realizacijaPredmeta) {
	this.realizacijaPredmeta = realizacijaPredmeta;
    }

    public java.util.List<Polaganje> getPolaganja() {
	return this.polaganja;
    }

    public void setPolaganja(java.util.List<Polaganje> polaganja) {
	this.polaganja = polaganja;
    }

    public Ishod getIshod() {
	return this.ishod;
    }

    public void setIshod(Ishod ishod) {
	this.ishod = ishod;
    }

    public java.util.List<Fajl> getFajlovi() {
	return this.fajlovi;
    }

    public void setFajlovi(java.util.List<Fajl> fajlovi) {
	this.fajlovi = fajlovi;
    }

    public TipEvaluacije getTipEvaluacije() {
	return this.tipEvaluacije;
    }

    public void setTipEvaluacije(TipEvaluacije tipEvaluacije) {
	this.tipEvaluacije = tipEvaluacije;
    }

    public java.util.List<Pitanje> getPitanja() {
	return this.pitanja;
    }

    public void setPitanja(java.util.List<Pitanje> pitanja) {
	this.pitanja = pitanja;
    }

    public IspitniRok getIspitniRok() {
	return this.ispitniRok;
    }

    public void setIspitniRok(IspitniRok ispitniRok) {
	this.ispitniRok = ispitniRok;
    }

    public java.util.List<EvaluacijaZnanjaNacinEvaluacije> getEvaluacijaZnanjaNaciniEvaluacije() {
	return this.evaluacijaZnanjaNaciniEvaluacije;
    }

    public void setEvaluacijaZnanjaNaciniEvaluacije(
	    java.util.List<EvaluacijaZnanjaNacinEvaluacije> evaluacijaZnanjaNaciniEvaluacije) {
	this.evaluacijaZnanjaNaciniEvaluacije = evaluacijaZnanjaNaciniEvaluacije;
    }

    public EvaluacijaZnanja() {
	super();
	this.nastavniciEvaluacijaZnanja = new java.util.ArrayList<>();
	this.polaganja = new java.util.ArrayList<>();
	this.fajlovi = new java.util.ArrayList<>();
	this.pitanja = new java.util.ArrayList<>();
	this.evaluacijaZnanjaNaciniEvaluacije = new java.util.ArrayList<>();
    }

    public NastavnikEvaluacijaZnanja getNastavnikEvaluacijaZnanja(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addNastavnikEvaluacijaZnanja(NastavnikEvaluacijaZnanja nastavnikEvaluacijaZnanja) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeNastavnikEvaluacijaZnanja(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public Polaganje getPolaganje(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addPolaganje(Polaganje polaganje) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removePolaganje(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public EvaluacijaZnanjaNacinEvaluacije getEvaluacijaZnanjaNacinEvaluacije(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addEvaluacijaZnanjaNacinEvaluacije(EvaluacijaZnanjaNacinEvaluacije evaluacijaZnanjaNacinEvaluacije) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeEvaluacijaZnanjaNacinEvaluacije(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public Fajl getFajl(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addFajl(Fajl fajl) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeFajl(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public Pitanje getPitanje(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addPitanje(Pitanje pitanje) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removePitanje(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result
		+ ((evaluacijaZnanjaNaciniEvaluacije == null) ? 0 : evaluacijaZnanjaNaciniEvaluacije.hashCode());
	result = prime * result + ((fajlovi == null) ? 0 : fajlovi.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((ishod == null) ? 0 : ishod.hashCode());
	result = prime * result + ((ispitniRok == null) ? 0 : ispitniRok.hashCode());
	result = prime * result + ((nastavniciEvaluacijaZnanja == null) ? 0 : nastavniciEvaluacijaZnanja.hashCode());
	result = prime * result + ((pitanja == null) ? 0 : pitanja.hashCode());
	result = prime * result + ((pocetak == null) ? 0 : pocetak.hashCode());
	result = prime * result + ((polaganja == null) ? 0 : polaganja.hashCode());
	result = prime * result + ((realizacijaPredmeta == null) ? 0 : realizacijaPredmeta.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((tipEvaluacije == null) ? 0 : tipEvaluacije.hashCode());
	result = prime * result + ((trajanje == null) ? 0 : trajanje.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	EvaluacijaZnanja other = (EvaluacijaZnanja) obj;
	if (evaluacijaZnanjaNaciniEvaluacije == null) {
	    if (other.evaluacijaZnanjaNaciniEvaluacije != null)
		return false;
	} else if (!evaluacijaZnanjaNaciniEvaluacije.equals(other.evaluacijaZnanjaNaciniEvaluacije))
	    return false;
	if (fajlovi == null) {
	    if (other.fajlovi != null)
		return false;
	} else if (!fajlovi.equals(other.fajlovi))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (ishod == null) {
	    if (other.ishod != null)
		return false;
	} else if (!ishod.equals(other.ishod))
	    return false;
	if (ispitniRok != other.ispitniRok)
	    return false;
	if (nastavniciEvaluacijaZnanja == null) {
	    if (other.nastavniciEvaluacijaZnanja != null)
		return false;
	} else if (!nastavniciEvaluacijaZnanja.equals(other.nastavniciEvaluacijaZnanja))
	    return false;
	if (pitanja == null) {
	    if (other.pitanja != null)
		return false;
	} else if (!pitanja.equals(other.pitanja))
	    return false;
	if (pocetak == null) {
	    if (other.pocetak != null)
		return false;
	} else if (!pocetak.equals(other.pocetak))
	    return false;
	if (polaganja == null) {
	    if (other.polaganja != null)
		return false;
	} else if (!polaganja.equals(other.polaganja))
	    return false;
	if (realizacijaPredmeta == null) {
	    if (other.realizacijaPredmeta != null)
		return false;
	} else if (!realizacijaPredmeta.equals(other.realizacijaPredmeta))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (tipEvaluacije != other.tipEvaluacije)
	    return false;
	if (trajanje == null) {
	    if (other.trajanje != null)
		return false;
	} else if (!trajanje.equals(other.trajanje))
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public EvaluacijaZnanjaDTO getDTO() {
	EvaluacijaZnanjaDTO evaluacijaZnanjaDTO = new EvaluacijaZnanjaDTO();
	evaluacijaZnanjaDTO.setId(id);
	evaluacijaZnanjaDTO.setIspitniRok(ispitniRok);
	evaluacijaZnanjaDTO.setPocetak(pocetak);
	evaluacijaZnanjaDTO.setStanje(stanje);
	evaluacijaZnanjaDTO.setTipEvaluacije(tipEvaluacije);
	evaluacijaZnanjaDTO.setTrajanje(trajanje);
	evaluacijaZnanjaDTO.setVersion(version);

	if (this.ishod != null)
	    evaluacijaZnanjaDTO.setIshodDTO(this.ishod.getDTOinsideDTO());
	if (this.realizacijaPredmeta != null)
	    evaluacijaZnanjaDTO.setRealizacijaPredmetaDTO(this.realizacijaPredmeta.getDTOinsideDTO());

	ArrayList<EvaluacijaZnanjaNacinEvaluacijeDTO> evaluacijaZnanjaNaciniEvaluacijeDTOs = new ArrayList<EvaluacijaZnanjaNacinEvaluacijeDTO>();
	for (EvaluacijaZnanjaNacinEvaluacije evaluacijaZnanjaNacinEvaluacije : this.evaluacijaZnanjaNaciniEvaluacije) {
	    evaluacijaZnanjaNaciniEvaluacijeDTOs.add(evaluacijaZnanjaNacinEvaluacije.getDTOinsideDTO());
	}
	evaluacijaZnanjaDTO.setEvaluacijaZnanjaNaciniEvaluacijeDTO(evaluacijaZnanjaNaciniEvaluacijeDTOs);

	ArrayList<FajlDTO> fajloviDTOs = new ArrayList<FajlDTO>();
	for (Fajl fajl : this.fajlovi) {
	    fajloviDTOs.add(fajl.getDTOinsideDTO());
	}
	evaluacijaZnanjaDTO.setFajloviDTO(fajloviDTOs);

	ArrayList<NastavnikEvaluacijaZnanjaDTO> nastavniciEvaluacijaZnanjaDTOs = new ArrayList<NastavnikEvaluacijaZnanjaDTO>();
	for (NastavnikEvaluacijaZnanja nastavnikEvaluacijaZnanja : this.nastavniciEvaluacijaZnanja) {
	    nastavniciEvaluacijaZnanjaDTOs.add(nastavnikEvaluacijaZnanja.getDTOinsideDTO());
	}
	evaluacijaZnanjaDTO.setNastavniciEvaluacijaZnanjaDTO(nastavniciEvaluacijaZnanjaDTOs);

	ArrayList<PitanjeDTO> pitanjaDTOs = new ArrayList<PitanjeDTO>();
	for (Pitanje pitanje : this.pitanja) {
	    pitanjaDTOs.add(pitanje.getDTOinsideDTO());
	}
	evaluacijaZnanjaDTO.setPitanjaDTO(pitanjaDTOs);

	ArrayList<PolaganjeDTO> polaganjaDTOs = new ArrayList<PolaganjeDTO>();
	for (Polaganje polaganje : this.polaganja) {
	    polaganjaDTOs.add(polaganje.getDTOinsideDTO());
	}
	evaluacijaZnanjaDTO.setPolaganjaDTO(polaganjaDTOs);

	return evaluacijaZnanjaDTO;
    }

    @Override
    public EvaluacijaZnanjaDTO getDTOinsideDTO() {
	EvaluacijaZnanjaDTO evaluacijaZnanjaDTO = new EvaluacijaZnanjaDTO();
	evaluacijaZnanjaDTO.setId(id);
	evaluacijaZnanjaDTO.setIspitniRok(ispitniRok);
	evaluacijaZnanjaDTO.setPocetak(pocetak);
	evaluacijaZnanjaDTO.setStanje(stanje);
	evaluacijaZnanjaDTO.setTipEvaluacije(tipEvaluacije);
	evaluacijaZnanjaDTO.setTrajanje(trajanje);
	evaluacijaZnanjaDTO.setVersion(version);
	return evaluacijaZnanjaDTO;
    }

    @Override
    public void update(EvaluacijaZnanja entitet) {
	this.setIspitniRok(entitet.getIspitniRok());
	this.setPocetak(entitet.getPocetak());
	this.setStanje(entitet.getStanje());
	this.setTipEvaluacije(entitet.getTipEvaluacije());
	this.setTrajanje(entitet.getTrajanje());
	this.setVersion(entitet.getVersion());

	this.setIshod(entitet.getIshod());
	this.setRealizacijaPredmeta(entitet.getRealizacijaPredmeta());

    }

}