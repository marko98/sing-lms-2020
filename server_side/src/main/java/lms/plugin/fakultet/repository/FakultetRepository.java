package lms.plugin.fakultet.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.Fakultet;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class FakultetRepository extends CrudRepository<Fakultet, Long> {

}