package lms.plugin.fakultet.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.Fakultet;

@Repository
@Scope("singleton")
public interface FakultetPASRepository extends PagingAndSortingRepository<Fakultet, Long> {

}