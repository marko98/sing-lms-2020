package lms.plugin.fakultet.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.Odeljenje;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class OdeljenjeService extends CrudService<Odeljenje, Long> {

}