package lms.plugin.fakultet.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.Fakultet;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/fakultet")
@CrossOrigin(origins = "*")
public class FakultetController extends CrudController<Fakultet, Long> {

}