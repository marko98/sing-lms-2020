package lms.plugin.fakultet.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.Odeljenje;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/odeljenje")
@CrossOrigin(origins = "*")
public class OdeljenjeController extends CrudController<Odeljenje, Long> {

}