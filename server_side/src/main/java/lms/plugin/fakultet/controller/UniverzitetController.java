package lms.plugin.fakultet.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.Univerzitet;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/univerzitet")
@CrossOrigin(origins = "*")
public class UniverzitetController extends CrudController<Univerzitet, Long> {

}