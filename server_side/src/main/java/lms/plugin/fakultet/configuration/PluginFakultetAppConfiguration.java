package lms.plugin.fakultet.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;

@Configuration
public class PluginFakultetAppConfiguration {

    @Bean
    public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
	PropertySourcesPlaceholderConfigurer properties = new PropertySourcesPlaceholderConfigurer();
	properties.setLocation(
		new ClassPathResource("/lms/plugin/fakultet/configuration/plugin.fakultet.application.properties"));
	properties.setIgnoreResourceNotFound(false);
	return properties;
    }

}