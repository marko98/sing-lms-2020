package lms.plugin.nastavnik.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.NastavnikEvaluacijaZnanja;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/nastavnik_evaluacija_znanja")
@CrossOrigin(origins = "*")
public class NastavnikEvaluacijaZnanjaController extends CrudController<NastavnikEvaluacijaZnanja, Long> {

}