package lms.plugin.nastavnik.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.NaucnaOblast;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/naucna_oblast")
@CrossOrigin(origins = "*")
public class NaucnaOblastController extends CrudController<NaucnaOblast, Long> {

}