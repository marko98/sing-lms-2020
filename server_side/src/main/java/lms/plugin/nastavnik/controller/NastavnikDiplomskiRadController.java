package lms.plugin.nastavnik.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.NastavnikDiplomskiRad;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/nastavnik_diplomski_rad")
@CrossOrigin(origins = "*")
public class NastavnikDiplomskiRadController extends CrudController<NastavnikDiplomskiRad, Long> {

}