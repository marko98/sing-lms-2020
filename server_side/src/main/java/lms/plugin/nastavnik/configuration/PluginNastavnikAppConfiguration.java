package lms.plugin.nastavnik.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;

@Configuration
public class PluginNastavnikAppConfiguration {

    @Bean
    public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
	PropertySourcesPlaceholderConfigurer properties = new PropertySourcesPlaceholderConfigurer();
	properties.setLocation(
		new ClassPathResource("/lms/plugin/nastavnik/configuration/plugin.nastavnik.application.properties"));
	properties.setIgnoreResourceNotFound(false);
	return properties;
    }

}