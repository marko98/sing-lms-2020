package lms.plugin.nastavnik.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.NastavnikNaRealizacijiTipNastave;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class NastavnikNaRealizacijiTipNastaveRepository extends CrudRepository<NastavnikNaRealizacijiTipNastave, Long> {

}