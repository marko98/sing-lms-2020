package lms.plugin.nastavnik.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.Titula;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class TitulaRepository extends CrudRepository<Titula, Long> {

}