package lms.plugin.nastavnik.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.NastavnikNaRealizacijiTipNastave;

@Repository
@Scope("singleton")
public interface NastavnikNaRealizacijiTipNastavePASRepository
	extends PagingAndSortingRepository<NastavnikNaRealizacijiTipNastave, Long> {

}