package lms.plugin.nastavnik.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.NastavnikFakultet;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class NastavnikFakultetRepository extends CrudRepository<NastavnikFakultet, Long> {

}