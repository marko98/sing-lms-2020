package lms.plugin.nastavnik.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.Nastavnik;

@Repository
@Scope("singleton")
public interface NastavnikPASRepository extends PagingAndSortingRepository<Nastavnik, Long> {

}