package lms.plugin.nastavnik.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.NastavnikDiplomskiRad;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class NastavnikDiplomskiRadService extends CrudService<NastavnikDiplomskiRad, Long> {

}