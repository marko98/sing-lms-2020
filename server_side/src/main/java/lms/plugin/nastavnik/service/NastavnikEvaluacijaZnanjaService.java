package lms.plugin.nastavnik.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.NastavnikEvaluacijaZnanja;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class NastavnikEvaluacijaZnanjaService extends CrudService<NastavnikEvaluacijaZnanja, Long> {

}