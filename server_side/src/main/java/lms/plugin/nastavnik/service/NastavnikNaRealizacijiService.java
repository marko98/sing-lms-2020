package lms.plugin.nastavnik.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.NastavnikNaRealizaciji;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class NastavnikNaRealizacijiService extends CrudService<NastavnikNaRealizaciji, Long> {

}