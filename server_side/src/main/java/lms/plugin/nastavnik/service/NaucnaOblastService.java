package lms.plugin.nastavnik.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.NaucnaOblast;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class NaucnaOblastService extends CrudService<NaucnaOblast, Long> {

}