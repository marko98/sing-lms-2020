package lms.plugin.nastavnik.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.Nastavnik;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class NastavnikService extends CrudService<Nastavnik, Long> {

}