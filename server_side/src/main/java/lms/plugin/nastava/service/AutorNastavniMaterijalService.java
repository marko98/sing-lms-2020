package lms.plugin.nastava.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.AutorNastavniMaterijal;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class AutorNastavniMaterijalService extends CrudService<AutorNastavniMaterijal, Long> {

}