package lms.plugin.nastava.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.Dogadjaj;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class DogadjajService extends CrudService<Dogadjaj, Long> {

}