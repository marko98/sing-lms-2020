package lms.plugin.nastava.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.Prostorija;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class ProstorijaService extends CrudService<Prostorija, Long> {

}