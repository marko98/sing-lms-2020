package lms.plugin.nastava.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.Autor;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class AutorService extends CrudService<Autor, Long> {

}