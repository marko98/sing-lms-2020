package lms.plugin.nastava.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.DrugiOblikNastave;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class DrugiOblikNastaveService extends CrudService<DrugiOblikNastave, Long> {

}