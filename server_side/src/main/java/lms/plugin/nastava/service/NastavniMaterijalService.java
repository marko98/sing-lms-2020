package lms.plugin.nastava.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.NastavniMaterijal;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class NastavniMaterijalService extends CrudService<NastavniMaterijal, Long> {

}