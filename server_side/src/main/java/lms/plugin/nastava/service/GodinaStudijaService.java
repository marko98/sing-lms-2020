package lms.plugin.nastava.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.GodinaStudija;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class GodinaStudijaService extends CrudService<GodinaStudija, Long> {

}