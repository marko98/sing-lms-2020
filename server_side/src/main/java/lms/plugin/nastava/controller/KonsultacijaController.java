package lms.plugin.nastava.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.Konsultacija;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/konsultacija")
@CrossOrigin(origins = "*")
public class KonsultacijaController extends CrudController<Konsultacija, Long> {

}