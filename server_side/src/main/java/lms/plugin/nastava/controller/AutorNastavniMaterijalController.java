package lms.plugin.nastava.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.AutorNastavniMaterijal;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/autor_nastavni_materijal")
@CrossOrigin(origins = "*")
public class AutorNastavniMaterijalController extends CrudController<AutorNastavniMaterijal, Long> {

}