package lms.plugin.nastava.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.VremeOdrzavanjaUNedelji;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/vreme_odrzavanja_u_nedelji")
@CrossOrigin(origins = "*")
public class VremeOdrzavanjaUNedeljiController extends CrudController<VremeOdrzavanjaUNedelji, Long> {

}