package lms.plugin.nastava.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.Prostorija;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/prostorija")
@CrossOrigin(origins = "*")
public class ProstorijaController extends CrudController<Prostorija, Long> {

}