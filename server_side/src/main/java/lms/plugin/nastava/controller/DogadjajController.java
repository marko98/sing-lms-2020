package lms.plugin.nastava.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.Dogadjaj;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/dogadjaj")
@CrossOrigin(origins = "*")
public class DogadjajController extends CrudController<Dogadjaj, Long> {

}