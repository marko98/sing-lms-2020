package lms.plugin.nastava.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.StudijskiProgram;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/studijski_program")
@CrossOrigin(origins = "*")
public class StudijskiProgramController extends CrudController<StudijskiProgram, Long> {

}