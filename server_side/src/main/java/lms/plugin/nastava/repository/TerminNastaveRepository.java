package lms.plugin.nastava.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.TerminNastave;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class TerminNastaveRepository extends CrudRepository<TerminNastave, Long> {

}