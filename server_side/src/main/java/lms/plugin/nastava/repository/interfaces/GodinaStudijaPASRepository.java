package lms.plugin.nastava.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.GodinaStudija;

@Repository
@Scope("singleton")
public interface GodinaStudijaPASRepository extends PagingAndSortingRepository<GodinaStudija, Long> {

}