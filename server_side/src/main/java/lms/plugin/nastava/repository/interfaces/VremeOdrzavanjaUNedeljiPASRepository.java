package lms.plugin.nastava.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.VremeOdrzavanjaUNedelji;

@Repository
@Scope("singleton")
public interface VremeOdrzavanjaUNedeljiPASRepository
	extends PagingAndSortingRepository<VremeOdrzavanjaUNedelji, Long> {

}