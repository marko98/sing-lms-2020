package lms.plugin.nastava.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.Autor;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class AutorRepository extends CrudRepository<Autor, Long> {

}