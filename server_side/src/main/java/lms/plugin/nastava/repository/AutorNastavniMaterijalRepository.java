package lms.plugin.nastava.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.AutorNastavniMaterijal;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class AutorNastavniMaterijalRepository extends CrudRepository<AutorNastavniMaterijal, Long> {

}