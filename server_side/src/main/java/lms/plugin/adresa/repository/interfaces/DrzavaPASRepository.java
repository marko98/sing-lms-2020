package lms.plugin.adresa.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.Drzava;

@Repository
@Scope("singleton")
public interface DrzavaPASRepository extends PagingAndSortingRepository<Drzava, Long> {

}