package lms.plugin.adresa.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.Grad;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class GradService extends CrudService<Grad, Long> {

}