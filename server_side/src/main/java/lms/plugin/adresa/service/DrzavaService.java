package lms.plugin.adresa.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.Drzava;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class DrzavaService extends CrudService<Drzava, Long> {

}