package lms.plugin.adresa.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.Adresa;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/adresa")
@CrossOrigin(origins = "*")
public class AdresaController extends CrudController<Adresa, Long> {

}