package lms.plugin.student.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.IstrazivackiRad;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/istrazivacki_rad")
@CrossOrigin(origins = "*")
public class IstrazivackiRadController extends CrudController<IstrazivackiRad, Long> {

}