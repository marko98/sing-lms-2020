package lms.plugin.student.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.DatumPohadjanjaPredmeta;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/datum_pohadjanja_predmeta")
@CrossOrigin(origins = "*")
public class DatumPohadjanjaPredmetaController extends CrudController<DatumPohadjanjaPredmeta, Long> {

}