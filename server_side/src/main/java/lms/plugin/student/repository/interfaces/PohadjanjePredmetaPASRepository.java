package lms.plugin.student.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.PohadjanjePredmeta;

@Repository
@Scope("singleton")
public interface PohadjanjePredmetaPASRepository extends PagingAndSortingRepository<PohadjanjePredmeta, Long> {

}