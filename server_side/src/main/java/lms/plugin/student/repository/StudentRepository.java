package lms.plugin.student.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.Student;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class StudentRepository extends CrudRepository<Student, Long> {

}