package lms.plugin.student.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.PohadjanjePredmeta;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class PohadjanjePredmetaRepository extends CrudRepository<PohadjanjePredmeta, Long> {

}