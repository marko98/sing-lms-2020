package lms.plugin.student.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.DatumPohadjanjaPredmeta;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class DatumPohadjanjaPredmetaRepository extends CrudRepository<DatumPohadjanjaPredmeta, Long> {

}