package lms.plugin.student.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.PohadjanjePredmeta;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class PohadjanjePredmetaService extends CrudService<PohadjanjePredmeta, Long> {

}