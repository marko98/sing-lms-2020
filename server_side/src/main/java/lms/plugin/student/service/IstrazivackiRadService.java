package lms.plugin.student.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.IstrazivackiRad;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class IstrazivackiRadService extends CrudService<IstrazivackiRad, Long> {

}