package lms.plugin.student.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.Student;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class StudentService extends CrudService<Student, Long> {

}