package lms.plugin.student.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.IstrazivackiRadStudentNaStudiji;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class IstrazivackiRadStudentNaStudijiService extends CrudService<IstrazivackiRadStudentNaStudiji, Long> {

}