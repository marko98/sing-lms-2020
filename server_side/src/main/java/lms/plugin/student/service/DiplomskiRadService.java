package lms.plugin.student.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.DiplomskiRad;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class DiplomskiRadService extends CrudService<DiplomskiRad, Long> {

}