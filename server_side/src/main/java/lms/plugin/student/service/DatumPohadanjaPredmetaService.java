package lms.plugin.student.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.DatumPohadjanjaPredmeta;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class DatumPohadanjaPredmetaService extends CrudService<DatumPohadjanjaPredmeta, Long> {

}