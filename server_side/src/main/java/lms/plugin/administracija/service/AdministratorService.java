package lms.plugin.administracija.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.Administrator;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class AdministratorService extends CrudService<Administrator, Long> {

}