package lms.plugin.administracija.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.StudentskaSluzba;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class StudentskaSluzbaService extends CrudService<StudentskaSluzba, Long> {

}