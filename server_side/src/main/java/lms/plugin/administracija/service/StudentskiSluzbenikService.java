package lms.plugin.administracija.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.StudentskiSluzbenik;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class StudentskiSluzbenikService extends CrudService<StudentskiSluzbenik, Long> {

}