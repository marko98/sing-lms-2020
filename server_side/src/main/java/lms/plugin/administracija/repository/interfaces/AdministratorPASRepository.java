package lms.plugin.administracija.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.Administrator;

@Repository
@Scope("singleton")
public interface AdministratorPASRepository extends PagingAndSortingRepository<Administrator, Long> {

}