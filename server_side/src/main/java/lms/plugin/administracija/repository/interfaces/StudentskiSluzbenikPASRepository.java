package lms.plugin.administracija.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.StudentskiSluzbenik;

@Repository
@Scope("singleton")
public interface StudentskiSluzbenikPASRepository extends PagingAndSortingRepository<StudentskiSluzbenik, Long> {

}