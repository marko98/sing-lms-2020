package lms.plugin.administracija.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.Administrator;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/administrator")
@CrossOrigin(origins = "*")
public class AdministratorController extends CrudController<Administrator, Long> {

}