package lms.plugin.predmet.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.PredmetTipNastave;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/predmet_tip_nastave")
@CrossOrigin(origins = "*")
public class PredmetTipNastaveController extends CrudController<PredmetTipNastave, Long> {

}