package lms.plugin.predmet.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.RealizacijaPredmeta;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/realizacija_predmeta")
@CrossOrigin(origins = "*")
public class RealizacijaPredmetaController extends CrudController<RealizacijaPredmeta, Long> {

}