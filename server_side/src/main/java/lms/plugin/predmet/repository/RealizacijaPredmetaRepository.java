package lms.plugin.predmet.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.RealizacijaPredmeta;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class RealizacijaPredmetaRepository extends CrudRepository<RealizacijaPredmeta, Long> {

}