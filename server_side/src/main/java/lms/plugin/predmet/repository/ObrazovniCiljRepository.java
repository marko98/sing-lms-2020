package lms.plugin.predmet.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.ObrazovniCilj;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class ObrazovniCiljRepository extends CrudRepository<ObrazovniCilj, Long> {

}