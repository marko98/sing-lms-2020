package lms.plugin.predmet.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.Silabus;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class SilabusRepository extends CrudRepository<Silabus, Long> {

}