package lms.plugin.predmet.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.PredmetTipDrugogOblikaNastave;

@Repository
@Scope("singleton")
public interface PredmetTipDrugogOblikaNastavePASRepository
	extends PagingAndSortingRepository<PredmetTipDrugogOblikaNastave, Long> {

}