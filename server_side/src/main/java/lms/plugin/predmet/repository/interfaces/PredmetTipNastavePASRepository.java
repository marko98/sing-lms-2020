package lms.plugin.predmet.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.PredmetTipNastave;

@Repository
@Scope("singleton")
public interface PredmetTipNastavePASRepository extends PagingAndSortingRepository<PredmetTipNastave, Long> {

}