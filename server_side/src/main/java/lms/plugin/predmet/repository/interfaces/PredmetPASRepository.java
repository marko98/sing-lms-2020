package lms.plugin.predmet.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.Predmet;

@Repository
@Scope("singleton")
public interface PredmetPASRepository extends PagingAndSortingRepository<Predmet, Long> {

}