package lms.plugin.predmet.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.Silabus;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class SilabusService extends CrudService<Silabus, Long> {

}