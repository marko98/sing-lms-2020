package lms.plugin.predmet.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.Predmet;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class PredmetService extends CrudService<Predmet, Long> {

}