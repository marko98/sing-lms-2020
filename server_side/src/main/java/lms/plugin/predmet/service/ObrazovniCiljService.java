package lms.plugin.predmet.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.ObrazovniCilj;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class ObrazovniCiljService extends CrudService<ObrazovniCilj, Long> {

}