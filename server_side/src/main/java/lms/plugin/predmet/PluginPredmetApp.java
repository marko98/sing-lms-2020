package lms.plugin.predmet;

import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpMethod;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.web.client.RestTemplate;

import plugin.model.PluginDescription;

@SpringBootApplication
@ComponentScan(basePackages = { "lms.configuration", "lms.repository", "lms.service", "lms.controller", "lms.hbsf", "lms.plugin.predmet" })
@EntityScan(basePackages = { "lms.model" })
@EnableJpaRepositories("lms.plugin.predmet.repository")
@EnableJms
public class PluginPredmetApp extends SpringBootServletInitializer {

    public static void main(String args[]) {
	RestTemplate rt = new RestTemplate();

//	kreiramo opis plugin-a
	PluginDescription pluginDescription = new PluginDescription("Predmet", "Plugin za logicku celinu predmet",
		"http", "localhost:8109", new HashMap<String, HashMap<HttpMethod, HashMap<String, String>>>());

//	dodajemo kategorije koje plugin pruza
	ArrayList<String> categories = new ArrayList<String>();
	categories.add("predmet_tip_drugog_oblika_nastave");
	categories.add("predmet_tip_nastave");
	categories.add("predmet");
	categories.add("predmet_uslovni_predmet");
	categories.add("silabus");
	categories.add("realizacija_predmeta");
	categories.add("obrazovni_cilj");
	pluginDescription.setCategories(categories);

	for (String category : categories) {
//		dodajemo spisak endpoint-a u opis plugin-a
	    pluginDescription.getEndpoints().put(category, new HashMap<HttpMethod, HashMap<String, String>>());
	    pluginDescription.getEndpoints().get(category).put(HttpMethod.GET, new HashMap<String, String>());
	    pluginDescription.getEndpoints().get(category).get(HttpMethod.GET).putIfAbsent("findAll",
		    "/api/" + category);
	    pluginDescription.getEndpoints().get(category).get(HttpMethod.GET).putIfAbsent("findOne",
		    "/api/" + category + "/");

	    pluginDescription.getEndpoints().get(category).put(HttpMethod.POST, new HashMap<String, String>());
	    pluginDescription.getEndpoints().get(category).get(HttpMethod.POST).putIfAbsent("create",
		    "/api/" + category);

	    pluginDescription.getEndpoints().get(category).put(HttpMethod.PUT, new HashMap<String, String>());
	    pluginDescription.getEndpoints().get(category).get(HttpMethod.PUT).putIfAbsent("update",
		    "/api/" + category);

	    pluginDescription.getEndpoints().get(category).put(HttpMethod.DELETE, new HashMap<String, String>());
	    pluginDescription.getEndpoints().get(category).get(HttpMethod.DELETE).putIfAbsent("delete",
		    "/api/" + category + "/");
	}

//	registracija plugin-a na host-u
	rt.postForLocation("http://localhost:8080/api/plugins", pluginDescription);

//	pokretanje plugin-a
	SpringApplication.run(PluginPredmetApp.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
	return builder.sources(PluginPredmetApp.class);
    }

}