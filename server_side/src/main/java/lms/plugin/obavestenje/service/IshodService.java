package lms.plugin.obavestenje.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.Ishod;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class IshodService extends CrudService<Ishod, Long> {

}