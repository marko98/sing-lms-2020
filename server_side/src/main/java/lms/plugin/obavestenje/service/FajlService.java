package lms.plugin.obavestenje.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.Fajl;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class FajlService extends CrudService<Fajl, Long> {

}