package lms.plugin.obavestenje.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.Fajl;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/fajl")
@CrossOrigin(origins = "*")
public class FajlController extends CrudController<Fajl, Long> {

}