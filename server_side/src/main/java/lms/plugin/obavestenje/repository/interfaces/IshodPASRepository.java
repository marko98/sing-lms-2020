package lms.plugin.obavestenje.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.Ishod;

@Repository
@Scope("singleton")
public interface IshodPASRepository extends PagingAndSortingRepository<Ishod, Long> {

}