package lms.plugin.obavestenje.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.Obavestenje;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class ObavestenjeRepository extends CrudRepository<Obavestenje, Long> {

}