package lms.plugin.korisnik.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.RegistrovaniKorisnik;

@Repository
@Scope("singleton")
public interface RegistrovaniKorisnikPASRepository extends PagingAndSortingRepository<RegistrovaniKorisnik, Long> {

}