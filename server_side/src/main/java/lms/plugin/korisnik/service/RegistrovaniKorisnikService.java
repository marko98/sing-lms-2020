package lms.plugin.korisnik.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.RegistrovaniKorisnik;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class RegistrovaniKorisnikService extends CrudService<RegistrovaniKorisnik, Long> {

}