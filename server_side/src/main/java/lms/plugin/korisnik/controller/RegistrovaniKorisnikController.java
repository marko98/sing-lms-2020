package lms.plugin.korisnik.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.RegistrovaniKorisnik;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/registrovani_korisnik")
@CrossOrigin(origins = "*")
public class RegistrovaniKorisnikController extends CrudController<RegistrovaniKorisnik, Long> {

}