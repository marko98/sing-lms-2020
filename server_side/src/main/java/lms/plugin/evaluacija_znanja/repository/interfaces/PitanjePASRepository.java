package lms.plugin.evaluacija_znanja.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.Pitanje;

@Repository
@Scope("singleton")
public interface PitanjePASRepository extends PagingAndSortingRepository<Pitanje, Long> {

}