package lms.plugin.evaluacija_znanja.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.Pitanje;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class PitanjeRepository extends CrudRepository<Pitanje, Long> {

}