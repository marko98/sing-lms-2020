package lms.plugin.evaluacija_znanja.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.Odgovor;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class OdgovorRepository extends CrudRepository<Odgovor, Long> {

}