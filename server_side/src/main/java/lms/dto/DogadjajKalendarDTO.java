package lms.dto;

import lms.dto.interfaces.DTO;
import lms.enums.StanjeModela;

public class DogadjajKalendarDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 8766440888162786196L;

    private Long id;

    private StanjeModela stanje;

    private Long version;

    private KalendarDTO kalendarDTO;

    private DogadjajDTO dogadjajDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public KalendarDTO getKalendarDTO() {
	return this.kalendarDTO;
    }

    public void setKalendarDTO(KalendarDTO kalendarDTO) {
	this.kalendarDTO = kalendarDTO;
    }

    public DogadjajDTO getDogadjajDTO() {
	return this.dogadjajDTO;
    }

    public void setDogadjajDTO(DogadjajDTO dogadjajDTO) {
	this.dogadjajDTO = dogadjajDTO;
    }

    public DogadjajKalendarDTO() {
	super();
    }

}