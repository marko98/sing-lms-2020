package lms.dto;

import java.time.LocalDateTime;

import lms.dto.interfaces.DTO;
import lms.enums.IspitniRok;
import lms.enums.StanjeModela;
import lms.enums.TipEvaluacije;
import lms.interfaces.Identifikacija;

public class EvaluacijaZnanjaDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 7032819319650514628L;

    private Long id;

    private LocalDateTime pocetak;

    private Integer trajanje;

    private StanjeModela stanje;

    private Long version;

    private IspitniRok ispitniRok;

    private java.util.List<NastavnikEvaluacijaZnanjaDTO> nastavniciEvaluacijaZnanjaDTO;

    private TipEvaluacije tipEvaluacije;

    private RealizacijaPredmetaDTO realizacijaPredmetaDTO;

    private IshodDTO ishodDTO;

    private java.util.List<FajlDTO> fajloviDTO;

    private java.util.List<PolaganjeDTO> polaganjaDTO;

    private java.util.List<PitanjeDTO> pitanjaDTO;

    private java.util.List<EvaluacijaZnanjaNacinEvaluacijeDTO> evaluacijaZnanjaNaciniEvaluacijeDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public LocalDateTime getPocetak() {
	return this.pocetak;
    }

    public void setPocetak(LocalDateTime pocetak) {
	this.pocetak = pocetak;
    }

    public Integer getTrajanje() {
	return this.trajanje;
    }

    public void setTrajanje(Integer trajanje) {
	this.trajanje = trajanje;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public IspitniRok getIspitniRok() {
	return this.ispitniRok;
    }

    public void setIspitniRok(IspitniRok ispitniRok) {
	this.ispitniRok = ispitniRok;
    }

    public java.util.List<NastavnikEvaluacijaZnanjaDTO> getNastavniciEvaluacijaZnanjaDTO() {
	return this.nastavniciEvaluacijaZnanjaDTO;
    }

    public void setNastavniciEvaluacijaZnanjaDTO(
	    java.util.List<NastavnikEvaluacijaZnanjaDTO> nastavniciEvaluacijaZnanjaDTO) {
	this.nastavniciEvaluacijaZnanjaDTO = nastavniciEvaluacijaZnanjaDTO;
    }

    public TipEvaluacije getTipEvaluacije() {
	return this.tipEvaluacije;
    }

    public void setTipEvaluacije(TipEvaluacije tipEvaluacije) {
	this.tipEvaluacije = tipEvaluacije;
    }

    public RealizacijaPredmetaDTO getRealizacijaPredmetaDTO() {
	return this.realizacijaPredmetaDTO;
    }

    public void setRealizacijaPredmetaDTO(RealizacijaPredmetaDTO realizacijaPredmetaDTO) {
	this.realizacijaPredmetaDTO = realizacijaPredmetaDTO;
    }

    public IshodDTO getIshodDTO() {
	return this.ishodDTO;
    }

    public void setIshodDTO(IshodDTO ishodDTO) {
	this.ishodDTO = ishodDTO;
    }

    public java.util.List<FajlDTO> getFajloviDTO() {
	return this.fajloviDTO;
    }

    public void setFajloviDTO(java.util.List<FajlDTO> fajloviDTO) {
	this.fajloviDTO = fajloviDTO;
    }

    public java.util.List<PolaganjeDTO> getPolaganjaDTO() {
	return this.polaganjaDTO;
    }

    public void setPolaganjaDTO(java.util.List<PolaganjeDTO> polaganjaDTO) {
	this.polaganjaDTO = polaganjaDTO;
    }

    public java.util.List<PitanjeDTO> getPitanjaDTO() {
	return this.pitanjaDTO;
    }

    public void setPitanjaDTO(java.util.List<PitanjeDTO> pitanjaDTO) {
	this.pitanjaDTO = pitanjaDTO;
    }

    public java.util.List<EvaluacijaZnanjaNacinEvaluacijeDTO> getEvaluacijaZnanjaNaciniEvaluacijeDTO() {
	return this.evaluacijaZnanjaNaciniEvaluacijeDTO;
    }

    public void setEvaluacijaZnanjaNaciniEvaluacijeDTO(
	    java.util.List<EvaluacijaZnanjaNacinEvaluacijeDTO> evaluacijaZnanjaNaciniEvaluacijeDTO) {
	this.evaluacijaZnanjaNaciniEvaluacijeDTO = evaluacijaZnanjaNaciniEvaluacijeDTO;
    }

    public EvaluacijaZnanjaDTO() {
	super();
	this.nastavniciEvaluacijaZnanjaDTO = new java.util.ArrayList<>();
	this.fajloviDTO = new java.util.ArrayList<>();
	this.polaganjaDTO = new java.util.ArrayList<>();
	this.pitanjaDTO = new java.util.ArrayList<>();
	this.evaluacijaZnanjaNaciniEvaluacijeDTO = new java.util.ArrayList<>();
    }

    public NastavnikEvaluacijaZnanjaDTO getNastavnikEvaluacijaZnanjaDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addNastavnikEvaluacijaZnanjaDTO(NastavnikEvaluacijaZnanjaDTO nastavnikEvaluacijaZnanjaDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeNastavnikEvaluacijaZnanjaDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public PolaganjeDTO getPolaganjeDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addPolaganjeDTO(PolaganjeDTO polaganjeDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removePolaganjeDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public EvaluacijaZnanjaNacinEvaluacijeDTO getEvaluacijaZnanjaNacinEvaluacijeDTO(
	    Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addEvaluacijaZnanjaNacinEvaluacijeDTO(
	    EvaluacijaZnanjaNacinEvaluacijeDTO evaluacijaZnanjaNacinEvaluacijeDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeEvaluacijaZnanjaNacinEvaluacijeDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public FajlDTO getFajlDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addFajlDTO(FajlDTO fajlDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeFajlDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public PitanjeDTO getPitanjeDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addPitanjeDTO(PitanjeDTO pitanjeDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removePitanjeDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

}