package lms.dto;

import java.time.LocalDateTime;

import lms.dto.interfaces.DTO;
import lms.enums.StanjeModela;

public class DatumPohadjanjaPredmetaDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 2209967706022570433L;

    private Long id;

    private LocalDateTime datum;

    private StanjeModela stanje;

    private Long version;

    private PohadjanjePredmetaDTO pohadjanjePredmetaDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public LocalDateTime getDatum() {
	return this.datum;
    }

    public void setDatum(LocalDateTime datum) {
	this.datum = datum;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public PohadjanjePredmetaDTO getPohadjanjePredmetaDTO() {
	return this.pohadjanjePredmetaDTO;
    }

    public void setPohadjanjePredmetaDTO(PohadjanjePredmetaDTO pohadjanjePredmetaDTO) {
	this.pohadjanjePredmetaDTO = pohadjanjePredmetaDTO;
    }

    public DatumPohadjanjaPredmetaDTO() {
	super();
    }

}