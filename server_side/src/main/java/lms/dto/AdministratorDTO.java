package lms.dto;

import lms.dto.interfaces.DTO;
import lms.enums.StanjeModela;

public class AdministratorDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -8501989259788501734L;

    private Long id;

    private StanjeModela stanje;

    private Long version;

    private RegistrovaniKorisnikDTO registrovaniKorisnikDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public RegistrovaniKorisnikDTO getRegistrovaniKorisnikDTO() {
	return this.registrovaniKorisnikDTO;
    }

    public void setRegistrovaniKorisnikDTO(RegistrovaniKorisnikDTO registrovaniKorisnikDTO) {
	this.registrovaniKorisnikDTO = registrovaniKorisnikDTO;
    }

    public AdministratorDTO() {
	super();
    }

}