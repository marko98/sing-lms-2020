package lms.dto;

import java.time.LocalDateTime;

import lms.dto.interfaces.DTO;
import lms.enums.StanjeModela;
import lms.interfaces.Identifikacija;

public class DogadjajDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -5668038554652471325L;

    private Long id;

    private LocalDateTime pocetniDatum;

    private LocalDateTime krajnjiDatum;

    private String opis;

    private StanjeModela stanje;

    private Long version;

    private java.util.List<DogadjajKalendarDTO> dogadjajKalendariDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public LocalDateTime getPocetniDatum() {
	return this.pocetniDatum;
    }

    public void setPocetniDatum(LocalDateTime pocetniDatum) {
	this.pocetniDatum = pocetniDatum;
    }

    public LocalDateTime getKrajnjiDatum() {
	return this.krajnjiDatum;
    }

    public void setKrajnjiDatum(LocalDateTime krajnjiDatum) {
	this.krajnjiDatum = krajnjiDatum;
    }

    public String getOpis() {
	return this.opis;
    }

    public void setOpis(String opis) {
	this.opis = opis;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public java.util.List<DogadjajKalendarDTO> getDogadjajKalendariDTO() {
	return this.dogadjajKalendariDTO;
    }

    public void setDogadjajKalendariDTO(java.util.List<DogadjajKalendarDTO> dogadjajKalendariDTO) {
	this.dogadjajKalendariDTO = dogadjajKalendariDTO;
    }

    public DogadjajDTO() {
	super();
	this.dogadjajKalendariDTO = new java.util.ArrayList<>();
    }

    public DogadjajKalendarDTO getDogadjajKalendarDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addDogadjajKalendarDTO(DogadjajKalendarDTO dogadjajKalendarDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeDogadjajKalendarDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

}