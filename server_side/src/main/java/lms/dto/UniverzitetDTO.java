package lms.dto;

import java.time.LocalDateTime;

import lms.dto.interfaces.DTO;
import lms.enums.StanjeModela;
import lms.interfaces.Identifikacija;

public class UniverzitetDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -4550428000264478425L;

    private Long id;

    private String naziv;

    private LocalDateTime datumOsnivanja;

    private StanjeModela stanje;

    private Long version;

    private java.util.List<AdresaDTO> adreseDTO;

    private java.util.List<FakultetDTO> fakultetiDTO;

    private java.util.List<KalendarDTO> kalendariDTO;

    private NastavnikDTO rektorDTO;

    private StudentskaSluzbaDTO studentskaSluzbaDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getNaziv() {
	return this.naziv;
    }

    public void setNaziv(String naziv) {
	this.naziv = naziv;
    }

    public LocalDateTime getDatumOsnivanja() {
	return this.datumOsnivanja;
    }

    public void setDatumOsnivanja(LocalDateTime datumOsnivanja) {
	this.datumOsnivanja = datumOsnivanja;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public java.util.List<AdresaDTO> getAdreseDTO() {
	return this.adreseDTO;
    }

    public void setAdreseDTO(java.util.List<AdresaDTO> adreseDTO) {
	this.adreseDTO = adreseDTO;
    }

    public java.util.List<FakultetDTO> getFakultetiDTO() {
	return this.fakultetiDTO;
    }

    public void setFakultetiDTO(java.util.List<FakultetDTO> fakultetiDTO) {
	this.fakultetiDTO = fakultetiDTO;
    }

    public java.util.List<KalendarDTO> getKalendariDTO() {
	return this.kalendariDTO;
    }

    public void setKalendariDTO(java.util.List<KalendarDTO> kalendariDTO) {
	this.kalendariDTO = kalendariDTO;
    }

    public NastavnikDTO getRektorDTO() {
	return this.rektorDTO;
    }

    public void setRektorDTO(NastavnikDTO rektorDTO) {
	this.rektorDTO = rektorDTO;
    }

    public StudentskaSluzbaDTO getStudentskaSluzbaDTO() {
	return this.studentskaSluzbaDTO;
    }

    public void setStudentskaSluzbaDTO(StudentskaSluzbaDTO studentskaSluzbaDTO) {
	this.studentskaSluzbaDTO = studentskaSluzbaDTO;
    }

    public UniverzitetDTO() {
	super();
	this.adreseDTO = new java.util.ArrayList<>();
	this.fakultetiDTO = new java.util.ArrayList<>();
	this.kalendariDTO = new java.util.ArrayList<>();
    }

    public AdresaDTO getAdresaDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addAdresaDTO(AdresaDTO adresaDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeAdresaDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public FakultetDTO getFakultetDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addFakultetDTO(FakultetDTO fakultetDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeFakultetDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public KalendarDTO getKalendarDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addKalendarDTO(KalendarDTO kalendarDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeKalendarDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

}