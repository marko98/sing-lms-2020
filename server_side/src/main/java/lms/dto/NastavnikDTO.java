package lms.dto;

import lms.dto.interfaces.DTO;
import lms.enums.StanjeModela;
import lms.interfaces.Identifikacija;

public class NastavnikDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -5752236934394858322L;

    private Long id;

    private String biografija;

    private StanjeModela stanje;

    private Long version;

    private java.util.List<TerminNastaveDTO> terminiNastaveDTO;

    private java.util.List<StudijskiProgramDTO> studijskiProgramDTO;

    private java.util.List<FakultetDTO> fakultetiDTO;

    private java.util.List<NastavnikFakultetDTO> nastavnikFakultetiDTO;

    private java.util.List<UniverzitetDTO> univerzitetiDTO;

    private java.util.List<TitulaDTO> tituleDTO;

    private java.util.List<DiplomskiRadDTO> mentorDiplomskiRadoviDTO;

    private RegistrovaniKorisnikDTO registrovaniKorisnikDTO;

    private java.util.List<IstrazivackiRadDTO> istrazivackiRadoviDTO;

    private java.util.List<NastavnikEvaluacijaZnanjaDTO> nastavnikEvaluacijeZnanjaDTO;

    private java.util.List<KonsultacijaDTO> konsultacijeDTO;

    private java.util.List<NastavnikDiplomskiRadDTO> nastavnikDiplomskiRadoviDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getBiografija() {
	return this.biografija;
    }

    public void setBiografija(String biografija) {
	this.biografija = biografija;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public java.util.List<TerminNastaveDTO> getTerminiNastaveDTO() {
	return this.terminiNastaveDTO;
    }

    public void setTerminiNastaveDTO(java.util.List<TerminNastaveDTO> terminiNastaveDTO) {
	this.terminiNastaveDTO = terminiNastaveDTO;
    }

    public java.util.List<StudijskiProgramDTO> getStudijskiProgramDTO() {
	return this.studijskiProgramDTO;
    }

    public void setStudijskiProgramDTO(java.util.List<StudijskiProgramDTO> studijskiProgramDTO) {
	this.studijskiProgramDTO = studijskiProgramDTO;
    }

    public java.util.List<FakultetDTO> getFakultetDTO() {
	return this.fakultetiDTO;
    }

    public void setFakultetDTO(java.util.List<FakultetDTO> fakultetiDTO) {
	this.fakultetiDTO = fakultetiDTO;
    }

    public java.util.List<NastavnikFakultetDTO> getNastavnikFakultetiDTO() {
	return this.nastavnikFakultetiDTO;
    }

    public void setNastavnikFakultetiDTO(java.util.List<NastavnikFakultetDTO> nastavnikFakultetiDTO) {
	this.nastavnikFakultetiDTO = nastavnikFakultetiDTO;
    }

    public java.util.List<UniverzitetDTO> getUniverzitetDTO() {
	return this.univerzitetiDTO;
    }

    public void setUniverzitetDTO(java.util.List<UniverzitetDTO> univerzitetiDTO) {
	this.univerzitetiDTO = univerzitetiDTO;
    }

    public java.util.List<TitulaDTO> getTituleDTO() {
	return this.tituleDTO;
    }

    public void setTituleDTO(java.util.List<TitulaDTO> tituleDTO) {
	this.tituleDTO = tituleDTO;
    }

    public java.util.List<DiplomskiRadDTO> getMentorDiplomskiRadoviDTO() {
	return this.mentorDiplomskiRadoviDTO;
    }

    public void setMentorDiplomskiRadoviDTO(java.util.List<DiplomskiRadDTO> mentorDiplomskiRadoviDTO) {
	this.mentorDiplomskiRadoviDTO = mentorDiplomskiRadoviDTO;
    }

    public RegistrovaniKorisnikDTO getRegistrovaniKorisnikDTO() {
	return this.registrovaniKorisnikDTO;
    }

    public void setRegistrovaniKorisnikDTO(RegistrovaniKorisnikDTO registrovaniKorisnikDTO) {
	this.registrovaniKorisnikDTO = registrovaniKorisnikDTO;
    }

    public java.util.List<IstrazivackiRadDTO> getIstrazivackiRadoviDTO() {
	return this.istrazivackiRadoviDTO;
    }

    public void setIstrazivackiRadoviDTO(java.util.List<IstrazivackiRadDTO> istrazivackiRadoviDTO) {
	this.istrazivackiRadoviDTO = istrazivackiRadoviDTO;
    }

    public java.util.List<NastavnikEvaluacijaZnanjaDTO> getNastavnikEvaluacijeZnanjaDTO() {
	return this.nastavnikEvaluacijeZnanjaDTO;
    }

    public void setNastavnikEvaluacijeZnanjaDTO(
	    java.util.List<NastavnikEvaluacijaZnanjaDTO> nastavnikEvaluacijeZnanjaDTO) {
	this.nastavnikEvaluacijeZnanjaDTO = nastavnikEvaluacijeZnanjaDTO;
    }

    public java.util.List<KonsultacijaDTO> getKonsultacijeDTO() {
	return this.konsultacijeDTO;
    }

    public void setKonsultacijeDTO(java.util.List<KonsultacijaDTO> konsultacijeDTO) {
	this.konsultacijeDTO = konsultacijeDTO;
    }

    public java.util.List<NastavnikDiplomskiRadDTO> getNastavnikDiplomskiRadoviDTO() {
	return this.nastavnikDiplomskiRadoviDTO;
    }

    public void setNastavnikDiplomskiRadoviDTO(java.util.List<NastavnikDiplomskiRadDTO> nastavnikDiplomskiRadoviDTO) {
	this.nastavnikDiplomskiRadoviDTO = nastavnikDiplomskiRadoviDTO;
    }

    public NastavnikDTO() {
	super();
	this.terminiNastaveDTO = new java.util.ArrayList<>();
	this.studijskiProgramDTO = new java.util.ArrayList<>();
	this.fakultetiDTO = new java.util.ArrayList<>();
	this.nastavnikFakultetiDTO = new java.util.ArrayList<>();
	this.univerzitetiDTO = new java.util.ArrayList<>();
	this.tituleDTO = new java.util.ArrayList<>();
	this.mentorDiplomskiRadoviDTO = new java.util.ArrayList<>();
	this.istrazivackiRadoviDTO = new java.util.ArrayList<>();
	this.nastavnikEvaluacijeZnanjaDTO = new java.util.ArrayList<>();
	this.konsultacijeDTO = new java.util.ArrayList<>();
	this.nastavnikDiplomskiRadoviDTO = new java.util.ArrayList<>();
    }

    public TitulaDTO getTitulaDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addTitulaDTO(TitulaDTO titulaDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeTitulaDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public NastavnikFakultetDTO getNastavnikFakultetDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addNastavnikFakultetDTO(NastavnikFakultetDTO nastavnikFakultetDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeNastavnikFakultetDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public DiplomskiRadDTO getMentorDiplomskiRadDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addMentorDiplomskiRadDTO(DiplomskiRadDTO MentorDiplomskiRadDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeMentorDiplomskiRadDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public IstrazivackiRadDTO getIstrazivackiRadDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addIstrazivackiRadDTO(IstrazivackiRadDTO istrazivackiRadDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeIstrazivackiRadDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public KonsultacijaDTO getKonsultacijaDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addKonsultacijaDTO(KonsultacijaDTO konsultacijaDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeKonsultacijaDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public NastavnikEvaluacijaZnanjaDTO getNastavnikEvaluacijaZnanjaDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addNastavnikEvaluacijaZnanjaDTO(NastavnikEvaluacijaZnanjaDTO nastavnikEvaluacijaZnanjaDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeNastavnikEvaluacijaZnanjaDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public TerminNastaveDTO getTerminNastaveDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addTerminNastaveDTO(TerminNastaveDTO terminNastaveDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeTerminNastaveDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public NastavnikDiplomskiRadDTO getNastavnikDiplomskiRadDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addNastavnikDiplomskiRadDTO(NastavnikDiplomskiRadDTO nastavnikDiplomskiRadDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeNastavnikDiplomskiRadDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

}