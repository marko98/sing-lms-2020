package lms.dto;

import lms.dto.interfaces.DTO;
import lms.enums.StanjeModela;
import lms.interfaces.Identifikacija;

public class StudijskiProgramDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -3690682342730030271L;

    private Long id;

    private String naziv;

    private Integer brojGodinaStudija;

    private StanjeModela stanje;

    private Long version;

    private FakultetDTO fakultetDTO;

    private NastavnikDTO rukovodilacDTO;

    private java.util.List<GodinaStudijaDTO> godineStudijaDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getNaziv() {
	return this.naziv;
    }

    public void setNaziv(String naziv) {
	this.naziv = naziv;
    }

    public Integer getBrojGodinaStudija() {
	return this.brojGodinaStudija;
    }

    public void setBrojGodinaStudija(Integer brojGodinaStudija) {
	this.brojGodinaStudija = brojGodinaStudija;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public FakultetDTO getFakultetDTO() {
	return this.fakultetDTO;
    }

    public void setFakultetDTO(FakultetDTO fakultetDTO) {
	this.fakultetDTO = fakultetDTO;
    }

    public NastavnikDTO getRukovodilacDTO() {
	return this.rukovodilacDTO;
    }

    public void setRukovodilacDTO(NastavnikDTO rukovodilacDTO) {
	this.rukovodilacDTO = rukovodilacDTO;
    }

    public java.util.List<GodinaStudijaDTO> getGodineStudijaDTO() {
	return this.godineStudijaDTO;
    }

    public void setGodineStudijaDTO(java.util.List<GodinaStudijaDTO> godineStudijaDTO) {
	this.godineStudijaDTO = godineStudijaDTO;
    }

    public StudijskiProgramDTO() {
	super();
	this.godineStudijaDTO = new java.util.ArrayList<>();
    }

    public GodinaStudijaDTO getGodinaStudijaDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addGodinaStudijaDTO(GodinaStudijaDTO godinaStudijaDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeGodinaStudijaDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

}