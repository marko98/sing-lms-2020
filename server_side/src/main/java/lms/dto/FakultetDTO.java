package lms.dto;

import lms.dto.interfaces.DTO;
import lms.enums.StanjeModela;
import lms.interfaces.Identifikacija;

public class FakultetDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -5801291506426201533L;

    private Long id;

    private String naziv;

    private StanjeModela stanje;

    private Long version;

    private java.util.List<OdeljenjeDTO> odeljenjaDTO;

    private java.util.List<StudijskiProgramDTO> studijskiProgramiDTO;

    private java.util.List<NastavnikFakultetDTO> nastavniciFakultetDTO;

    private UniverzitetDTO univerzitetDTO;

    private NastavnikDTO dekanDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getNaziv() {
	return this.naziv;
    }

    public void setNaziv(String naziv) {
	this.naziv = naziv;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public java.util.List<OdeljenjeDTO> getOdeljenjaDTO() {
	return this.odeljenjaDTO;
    }

    public void setOdeljenjaDTO(java.util.List<OdeljenjeDTO> odeljenjaDTO) {
	this.odeljenjaDTO = odeljenjaDTO;
    }

    public java.util.List<StudijskiProgramDTO> getStudijskiProgramiDTO() {
	return this.studijskiProgramiDTO;
    }

    public void setStudijskiProgramiDTO(java.util.List<StudijskiProgramDTO> studijskiProgramiDTO) {
	this.studijskiProgramiDTO = studijskiProgramiDTO;
    }

    public java.util.List<NastavnikFakultetDTO> getNastavniciFakultetDTO() {
	return this.nastavniciFakultetDTO;
    }

    public void setNastavniciFakultetDTO(java.util.List<NastavnikFakultetDTO> nastavniciFakultetDTO) {
	this.nastavniciFakultetDTO = nastavniciFakultetDTO;
    }

    public UniverzitetDTO getUniverzitetDTO() {
	return this.univerzitetDTO;
    }

    public void setUniverzitetDTO(UniverzitetDTO univerzitetDTO) {
	this.univerzitetDTO = univerzitetDTO;
    }

    public NastavnikDTO getDekanDTO() {
	return this.dekanDTO;
    }

    public void setDekanDTO(NastavnikDTO dekanDTO) {
	this.dekanDTO = dekanDTO;
    }

    public FakultetDTO() {
	super();
	this.odeljenjaDTO = new java.util.ArrayList<>();
	this.studijskiProgramiDTO = new java.util.ArrayList<>();
	this.nastavniciFakultetDTO = new java.util.ArrayList<>();
    }

    public OdeljenjeDTO getOdeljenjeDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addOdeljenjeDTO(OdeljenjeDTO odeljenjeDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeOdeljenjeDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public StudijskiProgramDTO getStudijskiProgramDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addStudijskiProgramDTO(StudijskiProgramDTO studijskiProgramDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeStudijskiProgramDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public NastavnikFakultetDTO getNastavnikFakultetDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addNastavnikFakultetDTO(NastavnikFakultetDTO nastavnikFakultetDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeNastavnikFakultetDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

}