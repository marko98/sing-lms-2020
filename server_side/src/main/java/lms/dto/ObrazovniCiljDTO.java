package lms.dto;

import lms.dto.interfaces.DTO;
import lms.enums.StanjeModela;

public class ObrazovniCiljDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 1572288153112651039L;

    private Long id;

    private String opis;

    private StanjeModela stanje;

    private Long version;

    private RealizacijaPredmetaDTO realizacijaPredmetaDTO;

    private IshodDTO ishodDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getOpis() {
	return this.opis;
    }

    public void setOpis(String opis) {
	this.opis = opis;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public RealizacijaPredmetaDTO getRealizacijaPredmetaDTO() {
	return this.realizacijaPredmetaDTO;
    }

    public void setRealizacijaPredmetaDTO(RealizacijaPredmetaDTO realizacijaPredmetaDTO) {
	this.realizacijaPredmetaDTO = realizacijaPredmetaDTO;
    }

    public IshodDTO getIshodDTO() {
	return this.ishodDTO;
    }

    public void setIshodDTO(IshodDTO ishodDTO) {
	this.ishodDTO = ishodDTO;
    }

    public ObrazovniCiljDTO() {
	super();
    }

}