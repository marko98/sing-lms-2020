import { Component } from '@angular/core';
import { AutorService } from './shared/service/entity_service/autor_service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'lms-client';

  constructor(private a: AutorService) {}
}
