import { Prostorija } from './prostorija.model';
import { Adresa } from './adresa.model';
import { Fakultet } from './fakultet.model';
import { StudentskaSluzba } from './studentska_sluzba.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';
import { TipOdeljenja } from '../enum/tip_odeljenja.enum';
import { Identifikacija } from '../interface/identifikacija.interface';

export class Odeljenje implements Entitet<Odeljenje, number> {
  private id: number;

  private stanje: StanjeModela;

  private version: number;

  private tip: TipOdeljenja;

  private prostorije: Prostorija[];

  private adresa: Adresa;

  private fakultet: Fakultet;

  private studentskaSluzba: StudentskaSluzba;

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getTip = (): TipOdeljenja => {
    return this.tip;
  };

  public setTip = (tip: TipOdeljenja): void => {
    this.tip = tip;
  };

  public getProstorije = (): Prostorija[] => {
    return this.prostorije;
  };

  public setProstorije = (prostorije: Prostorija[]): void => {
    this.prostorije = prostorije;
  };

  public getAdresa = (): Adresa => {
    return this.adresa;
  };

  public setAdresa = (adresa: Adresa): void => {
    this.adresa = adresa;
  };

  public getFakultet = (): Fakultet => {
    return this.fakultet;
  };

  public setFakultet = (fakultet: Fakultet): void => {
    this.fakultet = fakultet;
  };

  public getStudentskaSluzba = (): StudentskaSluzba => {
    return this.studentskaSluzba;
  };

  public setStudentskaSluzba = (studentskaSluzba: StudentskaSluzba): void => {
    this.studentskaSluzba = studentskaSluzba;
  };

  public constructor() {
    this.prostorije = [];
  }

  public getProstorija(identifikacija: Identifikacija<number>): Prostorija {
    throw new Error('Not Implmented');
  }
}
