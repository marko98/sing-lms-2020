import { Grad } from './grad.model';
import { Odeljenje } from './odeljenje.model';
import { Univerzitet } from './univerzitet.model';
import { RegistrovaniKorisnik } from './registrovani_korisnik.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';
import { TipAdrese } from '../enum/tip_adrese.enum';

export class Adresa implements Entitet<Adresa, number> {
  private id: number;

  private ulica: string;

  private broj: string;

  private stanje: StanjeModela;

  private version: number;

  private grad: Grad;

  private tip: TipAdrese;

  private odeljenje: Odeljenje;

  private univerzitet: Univerzitet;

  private registrovaniKorisnik: RegistrovaniKorisnik;

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getUlica = (): string => {
    return this.ulica;
  };

  public setUlica = (ulica: string): void => {
    this.ulica = ulica;
  };

  public getBroj = (): string => {
    return this.broj;
  };

  public setBroj = (broj: string): void => {
    this.broj = broj;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getGrad = (): Grad => {
    return this.grad;
  };

  public setGrad = (grad: Grad): void => {
    this.grad = grad;
  };

  public getTip = (): TipAdrese => {
    return this.tip;
  };

  public setTip = (tip: TipAdrese): void => {
    this.tip = tip;
  };

  public getOdeljenje = (): Odeljenje => {
    return this.odeljenje;
  };

  public setOdeljenje = (odeljenje: Odeljenje): void => {
    this.odeljenje = odeljenje;
  };

  public getUniverzitet = (): Univerzitet => {
    return this.univerzitet;
  };

  public setUniverzitet = (univerzitet: Univerzitet): void => {
    this.univerzitet = univerzitet;
  };

  public getRegistrovaniKorisnik = (): RegistrovaniKorisnik => {
    return this.registrovaniKorisnik;
  };

  public setRegistrovaniKorisnik = (
    registrovaniKorisnik: RegistrovaniKorisnik
  ): void => {
    this.registrovaniKorisnik = registrovaniKorisnik;
  };

  public constructor() {}
}
