import { RealizacijaPredmeta } from './realizacija_predmeta.model';
import { EvaluacijaZnanja } from './evaluacija_znanja.model';
import { ObrazovniCilj } from './obrazovni_cilj.model';
import { NastavniMaterijal } from './nastavni_materijal.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';
import { Identifikacija } from '../interface/identifikacija.interface';

export class Ishod implements Entitet<Ishod, number> {
  private id: number;

  private opis: string;

  private naslov: string;

  private putanjaZaSliku: string;

  private stanje: StanjeModela;

  private version: number;

  private realizacijaPredmeta: RealizacijaPredmeta;

  private evaluacijaZnanja: EvaluacijaZnanja;

  private obrazovniCiljevi: ObrazovniCilj[];

  private nastavniMaterijali: NastavniMaterijal[];

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getOpis = (): string => {
    return this.opis;
  };

  public setOpis = (opis: string): void => {
    this.opis = opis;
  };

  public getNaslov = (): string => {
    return this.naslov;
  };

  public setNaslov = (naslov: string): void => {
    this.naslov = naslov;
  };

  public getPutanjaZaSliku = (): string => {
    return this.putanjaZaSliku;
  };

  public setPutanjaZaSliku = (putanjaZaSliku: string): void => {
    this.putanjaZaSliku = putanjaZaSliku;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getRealizacijaPredmeta = (): RealizacijaPredmeta => {
    return this.realizacijaPredmeta;
  };

  public setRealizacijaPredmeta = (
    realizacijaPredmeta: RealizacijaPredmeta
  ): void => {
    this.realizacijaPredmeta = realizacijaPredmeta;
  };

  public getEvaluacijaZnanja = (): EvaluacijaZnanja => {
    return this.evaluacijaZnanja;
  };

  public setEvaluacijaZnanja = (evaluacijaZnanja: EvaluacijaZnanja): void => {
    this.evaluacijaZnanja = evaluacijaZnanja;
  };

  public getObrazovniCiljevi = (): ObrazovniCilj[] => {
    return this.obrazovniCiljevi;
  };

  public setObrazovniCiljevi = (obrazovniCiljevi: ObrazovniCilj[]): void => {
    this.obrazovniCiljevi = obrazovniCiljevi;
  };

  public getNastavniMaterijali = (): NastavniMaterijal[] => {
    return this.nastavniMaterijali;
  };

  public setNastavniMaterijali = (
    nastavniMaterijali: NastavniMaterijal[]
  ): void => {
    this.nastavniMaterijali = nastavniMaterijali;
  };

  public constructor() {
    this.obrazovniCiljevi = [];
    this.nastavniMaterijali = [];
  }

  public getObrazovniCilj(
    identifikacija: Identifikacija<number>
  ): ObrazovniCilj {
    throw new Error('Not Implmented');
  }

  public getNastavniMaterijal(
    identifikacija: Identifikacija<number>
  ): NastavniMaterijal {
    throw new Error('Not Implmented');
  }
}
