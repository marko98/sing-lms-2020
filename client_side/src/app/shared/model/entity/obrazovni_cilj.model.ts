import { RealizacijaPredmeta } from './realizacija_predmeta.model';
import { Ishod } from './ishod.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';

export class ObrazovniCilj implements Entitet<ObrazovniCilj, number> {
  private id: number;

  private opis: string;

  private stanje: StanjeModela;

  private version: number;

  private realizacijaPredmeta: RealizacijaPredmeta;

  private ishod: Ishod;

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getOpis = (): string => {
    return this.opis;
  };

  public setOpis = (opis: string): void => {
    this.opis = opis;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getRealizacijaPredmeta = (): RealizacijaPredmeta => {
    return this.realizacijaPredmeta;
  };

  public setRealizacijaPredmeta = (
    realizacijaPredmeta: RealizacijaPredmeta
  ): void => {
    this.realizacijaPredmeta = realizacijaPredmeta;
  };

  public getIshod = (): Ishod => {
    return this.ishod;
  };

  public setIshod = (ishod: Ishod): void => {
    this.ishod = ishod;
  };

  public constructor() {}
}
