import { Obavestenje } from './obavestenje.model';
import { EvaluacijaZnanja } from './evaluacija_znanja.model';
import { NastavniMaterijal } from './nastavni_materijal.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';

export class Fajl implements Entitet<Fajl, number> {
  private id: number;

  private opis: string;

  private url: string;

  private stanje: StanjeModela;

  private version: number;

  private obavestenje: Obavestenje;

  private evaluacijaZnanja: EvaluacijaZnanja;

  private nastavniMaterijal: NastavniMaterijal;

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getOpis = (): string => {
    return this.opis;
  };

  public setOpis = (opis: string): void => {
    this.opis = opis;
  };

  public getUrl = (): string => {
    return this.url;
  };

  public setUrl = (url: string): void => {
    this.url = url;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getObavestenje = (): Obavestenje => {
    return this.obavestenje;
  };

  public setObavestenje = (obavestenje: Obavestenje): void => {
    this.obavestenje = obavestenje;
  };

  public getEvaluacijaZnanja = (): EvaluacijaZnanja => {
    return this.evaluacijaZnanja;
  };

  public setEvaluacijaZnanja = (evaluacijaZnanja: EvaluacijaZnanja): void => {
    this.evaluacijaZnanja = evaluacijaZnanja;
  };

  public getNastavniMaterijal = (): NastavniMaterijal => {
    return this.nastavniMaterijal;
  };

  public setNastavniMaterijal = (
    nastavniMaterijal: NastavniMaterijal
  ): void => {
    this.nastavniMaterijal = nastavniMaterijal;
  };

  public constructor() {}
}
