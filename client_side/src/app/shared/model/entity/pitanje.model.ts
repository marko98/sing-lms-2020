import { EvaluacijaZnanja } from './evaluacija_znanja.model';
import { Odgovor } from './odgovor.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';
import { Identifikacija } from '../interface/identifikacija.interface';

export class Pitanje implements Entitet<Pitanje, number> {
  private id: number;

  private oblast: string;

  private pitanje: string;

  private putanjaZaSliku: string;

  private stanje: StanjeModela;

  private version: number;

  private evaluacijaZnanja: EvaluacijaZnanja;

  private odgovori: Odgovor[];

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getOblast = (): string => {
    return this.oblast;
  };

  public setOblast = (oblast: string): void => {
    this.oblast = oblast;
  };

  public getPitanje = (): string => {
    return this.pitanje;
  };

  public setPitanje = (pitanje: string): void => {
    this.pitanje = pitanje;
  };

  public getPutanjaZaSliku = (): string => {
    return this.putanjaZaSliku;
  };

  public setPutanjaZaSliku = (putanjaZaSliku: string): void => {
    this.putanjaZaSliku = putanjaZaSliku;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getEvaluacijaZnanja = (): EvaluacijaZnanja => {
    return this.evaluacijaZnanja;
  };

  public setEvaluacijaZnanja = (evaluacijaZnanja: EvaluacijaZnanja): void => {
    this.evaluacijaZnanja = evaluacijaZnanja;
  };

  public getOdgovori = (): Odgovor[] => {
    return this.odgovori;
  };

  public setOdgovori = (odgovori: Odgovor[]): void => {
    this.odgovori = odgovori;
  };

  public constructor() {
    this.odgovori = [];
  }

  public getOdgovor(identifikacija: Identifikacija<number>): Odgovor {
    throw new Error('Not Implmented');
  }
}
