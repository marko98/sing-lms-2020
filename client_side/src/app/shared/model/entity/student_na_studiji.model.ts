import { Student } from './student.model';
import { GodinaStudija } from './godina_studija.model';
import { PohadjanjePredmeta } from './pohadjanje_predmeta.model';
import { IstrazivackiRadStudentNaStudiji } from './istrazivacki_rad_student_na_studiji.model';
import { DiplomskiRad } from './diplomski_rad.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';
import { Identifikacija } from '../interface/identifikacija.interface';

export class StudentNaStudiji implements Entitet<StudentNaStudiji, number> {
  private id: number;

  private datumUpisa: Date;

  private brojIndeksa: string;

  private stanje: StanjeModela;

  private version: number;

  private student: Student;

  private godinaStudija: GodinaStudija;

  private pohadjanjaPredmeta: PohadjanjePredmeta[];

  private istrazivackiRadoviStudentNaStudiji: IstrazivackiRadStudentNaStudiji[];

  private diplomskiRad: DiplomskiRad;

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getDatumUpisa = (): Date => {
    return this.datumUpisa;
  };

  public setDatumUpisa = (datumUpisa: Date): void => {
    this.datumUpisa = datumUpisa;
  };

  public getBrojIndeksa = (): string => {
    return this.brojIndeksa;
  };

  public setBrojIndeksa = (brojIndeksa: string): void => {
    this.brojIndeksa = brojIndeksa;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getStudent = (): Student => {
    return this.student;
  };

  public setStudent = (student: Student): void => {
    this.student = student;
  };

  public getGodinaStudija = (): GodinaStudija => {
    return this.godinaStudija;
  };

  public setGodinaStudija = (godinaStudija: GodinaStudija): void => {
    this.godinaStudija = godinaStudija;
  };

  public getPohadjanjaPredmeta = (): PohadjanjePredmeta[] => {
    return this.pohadjanjaPredmeta;
  };

  public setPohadjanjaPredmeta = (
    pohadjanjaPredmeta: PohadjanjePredmeta[]
  ): void => {
    this.pohadjanjaPredmeta = pohadjanjaPredmeta;
  };

  public getIstrazivackiRadoviStudentNaStudiji = (): IstrazivackiRadStudentNaStudiji[] => {
    return this.istrazivackiRadoviStudentNaStudiji;
  };

  public setIstrazivackiRadoviStudentNaStudiji = (
    istrazivackiRadoviStudentNaStudiji: IstrazivackiRadStudentNaStudiji[]
  ): void => {
    this.istrazivackiRadoviStudentNaStudiji = istrazivackiRadoviStudentNaStudiji;
  };

  public getDiplomskiRad = (): DiplomskiRad => {
    return this.diplomskiRad;
  };

  public setDiplomskiRad = (diplomskiRad: DiplomskiRad): void => {
    this.diplomskiRad = diplomskiRad;
  };

  public constructor() {
    this.pohadjanjaPredmeta = [];
    this.istrazivackiRadoviStudentNaStudiji = [];
  }

  public getPohadjanjePredmeta(
    identifikacija: Identifikacija<number>
  ): PohadjanjePredmeta {
    throw new Error('Not Implmented');
  }

  public getIstrazivackiRadStudentNaStudiji(
    identifikacija: Identifikacija<number>
  ): IstrazivackiRadStudentNaStudiji {
    throw new Error('Not Implmented');
  }
}
