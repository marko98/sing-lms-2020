import { RealizacijaPredmeta } from './realizacija_predmeta.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';
import { TipDrugogOblikaNastave } from '../enum/tip_drugog_oblika_nastave.enum';

export class DrugiOblikNastave implements Entitet<DrugiOblikNastave, number> {
  private id: number;

  private datum: Date;

  private stanje: StanjeModela;

  private version: number;

  private tipDrugogOblikaNastave: TipDrugogOblikaNastave;

  private realizacijaPredmeta: RealizacijaPredmeta;

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getDatum = (): Date => {
    return this.datum;
  };

  public setDatum = (datum: Date): void => {
    this.datum = datum;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getTipDrugogOblikaNastave = (): TipDrugogOblikaNastave => {
    return this.tipDrugogOblikaNastave;
  };

  public setTipDrugogOblikaNastave = (
    tipDrugogOblikaNastave: TipDrugogOblikaNastave
  ): void => {
    this.tipDrugogOblikaNastave = tipDrugogOblikaNastave;
  };

  public getRealizacijaPredmeta = (): RealizacijaPredmeta => {
    return this.realizacijaPredmeta;
  };

  public setRealizacijaPredmeta = (
    realizacijaPredmeta: RealizacijaPredmeta
  ): void => {
    this.realizacijaPredmeta = realizacijaPredmeta;
  };

  public constructor() {}
}
