import { Titula } from './titula.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';

export class NaucnaOblast implements Entitet<NaucnaOblast, number> {
  private id: number;

  private naziv: string;

  private stanje: StanjeModela;

  private version: number;

  private titule: Titula[];

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getNaziv = (): string => {
    return this.naziv;
  };

  public setNaziv = (naziv: string): void => {
    this.naziv = naziv;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getTitule = (): Titula[] => {
    return this.titule;
  };

  public setTitule = (titule: Titula[]): void => {
    this.titule = titule;
  };

  public constructor() {
    this.titule = [];
  }
}
