import { Adresa } from './adresa.model';
import { Nastavnik } from './nastavnik.model';
import { Fakultet } from './fakultet.model';
import { Kalendar } from './kalendar.model';
import { StudentskaSluzba } from './studentska_sluzba.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';
import { Identifikacija } from '../interface/identifikacija.interface';

export class Univerzitet implements Entitet<Univerzitet, number> {
  private id: number;

  private naziv: string;

  private datumOsnivanja: Date;

  private stanje: StanjeModela;

  private version: number;

  private adrese: Adresa[];

  private rektor: Nastavnik;

  private fakulteti: Fakultet[];

  private kalendari: Kalendar[];

  private studentskaSluzba: StudentskaSluzba;

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getNaziv = (): string => {
    return this.naziv;
  };

  public setNaziv = (naziv: string): void => {
    this.naziv = naziv;
  };

  public getDatumOsnivanja = (): Date => {
    return this.datumOsnivanja;
  };

  public setDatumOsnivanja = (datumOsnivanja: Date): void => {
    this.datumOsnivanja = datumOsnivanja;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getAdrese = (): Adresa[] => {
    return this.adrese;
  };

  public setAdrese = (adrese: Adresa[]): void => {
    this.adrese = adrese;
  };

  public getRektor = (): Nastavnik => {
    return this.rektor;
  };

  public setRektor = (rektor: Nastavnik): void => {
    this.rektor = rektor;
  };

  public getFakulteti = (): Fakultet[] => {
    return this.fakulteti;
  };

  public setFakulteti = (fakulteti: Fakultet[]): void => {
    this.fakulteti = fakulteti;
  };

  public getKalendari = (): Kalendar[] => {
    return this.kalendari;
  };

  public setKalendari = (kalendari: Kalendar[]): void => {
    this.kalendari = kalendari;
  };

  public getStudentskaSluzba = (): StudentskaSluzba => {
    return this.studentskaSluzba;
  };

  public setStudentskaSluzba = (studentskaSluzba: StudentskaSluzba): void => {
    this.studentskaSluzba = studentskaSluzba;
  };

  public constructor() {
    this.adrese = [];
    this.fakulteti = [];
    this.kalendari = [];
  }

  public getAdresa(identifikacija: Identifikacija<number>): Adresa {
    throw new Error('Not Implmented');
  }

  public getFakultet(identifikacija: Identifikacija<number>): Fakultet {
    throw new Error('Not Implmented');
  }

  public getKalendar(identifikacija: Identifikacija<number>): Kalendar {
    throw new Error('Not Implmented');
  }
}
