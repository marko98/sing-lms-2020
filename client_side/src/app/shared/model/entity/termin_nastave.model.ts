import { Prostorija } from './prostorija.model';
import { Nastavnik } from './nastavnik.model';
import { RealizacijaPredmeta } from './realizacija_predmeta.model';
import { VremeOdrzavanjaUNedelji } from './vreme_odrzavanja_u_nedelji.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';
import { TipNastave } from '../enum/tip_nastave.enum';

export class TerminNastave implements Entitet<TerminNastave, number> {
  private id: number;

  private trajanje: number;

  private stanje: StanjeModela;

  private version: number;

  private prostorija: Prostorija;

  private nastavnik: Nastavnik;

  private realizacijaPredmeta: RealizacijaPredmeta;

  private tipNastave: TipNastave;

  private vremeOdrzavanjaUNedelji: VremeOdrzavanjaUNedelji;

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getTrajanje = (): number => {
    return this.trajanje;
  };

  public setTrajanje = (trajanje: number): void => {
    this.trajanje = trajanje;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getProstorija = (): Prostorija => {
    return this.prostorija;
  };

  public setProstorija = (prostorija: Prostorija): void => {
    this.prostorija = prostorija;
  };

  public getNastavnik = (): Nastavnik => {
    return this.nastavnik;
  };

  public setNastavnik = (nastavnik: Nastavnik): void => {
    this.nastavnik = nastavnik;
  };

  public getRealizacijaPredmeta = (): RealizacijaPredmeta => {
    return this.realizacijaPredmeta;
  };

  public setRealizacijaPredmeta = (
    realizacijaPredmeta: RealizacijaPredmeta
  ): void => {
    this.realizacijaPredmeta = realizacijaPredmeta;
  };

  public getTipNastave = (): TipNastave => {
    return this.tipNastave;
  };

  public setTipNastave = (tipNastave: TipNastave): void => {
    this.tipNastave = tipNastave;
  };

  public getVremeOdrzavanjaUNedelji = (): VremeOdrzavanjaUNedelji => {
    return this.vremeOdrzavanjaUNedelji;
  };

  public setVremeOdrzavanjaUNedelji = (
    vremeOdrzavanjaUNedelji: VremeOdrzavanjaUNedelji
  ): void => {
    this.vremeOdrzavanjaUNedelji = vremeOdrzavanjaUNedelji;
  };

  public constructor() {}
}
