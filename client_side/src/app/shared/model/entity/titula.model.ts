import { NaucnaOblast } from './naucna_oblast.model';
import { Nastavnik } from './nastavnik.model';
import { Entitet } from '../interface/entitet.interface';
import { StanjeModela } from '../enum/stanje_modela.enum';
import { TipTitule } from '../enum/tip_titule.enum';

export class Titula implements Entitet<Titula, number> {
  private id: number;

  private datumIzbora: Date;

  private datumPrestanka: Date;

  private stanje: StanjeModela;

  private version: number;

  private tip: TipTitule;

  private naucnaOblast: NaucnaOblast;

  private nastavnik: Nastavnik;

  public getId = (): number => {
    return this.id;
  };

  public setId = (id: number): void => {
    this.id = id;
  };

  public getDatumIzbora = (): Date => {
    return this.datumIzbora;
  };

  public setDatumIzbora = (datumIzbora: Date): void => {
    this.datumIzbora = datumIzbora;
  };

  public getDatumPrestanka = (): Date => {
    return this.datumPrestanka;
  };

  public setDatumPrestanka = (datumPrestanka: Date): void => {
    this.datumPrestanka = datumPrestanka;
  };

  public getStanje = (): StanjeModela => {
    return this.stanje;
  };

  public setStanje = (stanje: StanjeModela): void => {
    this.stanje = stanje;
  };

  public getVersion = (): number => {
    return this.version;
  };

  public setVersion = (version: number): void => {
    this.version = version;
  };

  public getTip = (): TipTitule => {
    return this.tip;
  };

  public setTip = (tip: TipTitule): void => {
    this.tip = tip;
  };

  public getNaucnaOblast = (): NaucnaOblast => {
    return this.naucnaOblast;
  };

  public setNaucnaOblast = (naucnaOblast: NaucnaOblast): void => {
    this.naucnaOblast = naucnaOblast;
  };

  public getNastavnik = (): Nastavnik => {
    return this.nastavnik;
  };

  public setNastavnik = (nastavnik: Nastavnik): void => {
    this.nastavnik = nastavnik;
  };

  public constructor() {}
}
