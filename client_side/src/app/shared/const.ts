export const SCHEMA = 'http';
export const HOST = 'localhost';
export const PORT = 8080;

export const PATHS = {
  ADMINISTRATOR: 'api/administrator',
  STUDENTSKA_SLUZBA: 'api/studentska_sluzba',
  STUDENTSKI_SLUZBENIK: 'api/studentski_sluzbenik',
  ADRESA: 'api/adresa',
  DRZAVA: 'api/drzava',
  GRAD: 'api/grad',
  EVALUACIJA_ZNANJA: 'api/evaluacija_znanja',
  EVALUACIJA_ZNANJA_NACIN_EVALUACIJE: 'api/evaluacija_znanja_nacin_evaluacije',
  ODGOVOR: 'api/odgovor',
  PITANJE: 'api/pitanje',
  POLAGANJE: 'api/polaganje',
  FAKULTET: 'api/fakultet',
  ODELJENJE: 'api/odeljenje',
  UNIVERZITET: 'api/univerzitet',
  REGISTROVANI_KORISNIK: 'api/registrovani_korisnik',
  AUTOR: 'api/autor',
  AUTOR_NASTAVNI_MATERIJAL: 'api/autor_nastavni_materijal',
  DOGADJAJ: 'api/dogadjaj',
  DOGADJAJ_KALENDAR: 'api/dogadjaj_kalendar',
  DRUGI_OBLIK_NASTAVE: 'api/drugi_oblik_nastave',
  GODINA_STUDIJA: 'api/godina_studija',
  KALENDAR: 'api/kalendar',
  KONSULTACIJA: 'api/konsultacija',
  NASTAVNI_MATERIJAL: 'api/nastavni_materijal',
  PROSTORIJA: 'api/prostorija',
  STUDIJSKI_PROGRAM: 'api/studijski_program',
  TERMIN_NASTAVE: 'api/termin_nastave',
  VREME_ODRZAVANJA_U_NEDELJI: 'api/vreme_odrzavanja_u_nedelji',
  NASTAVNIK: 'api/nastavnik',
  NASTAVNIK_DIPLOMSKI_RAD: 'api/nastavnik_diplomski_rad',
  NASTAVNIK_EVALUACIJA_ZNANJA: 'api/nastavnik_evaluacija_znanja',
  NASTAVNIK_FAKULTET: 'api/nastavnik_fakultet',
  NASTAVNIK_NA_REALIZACIJI: 'api/nastavnik_na_realizaciji',
  NASTAVNIK_NA_REALIZACIJI_TIP_NASTAVE:
    'api/nastavnik_na_realizaciji_tip_nastave',
  NAUCNA_OBLAST: 'api/naucna_oblast',
  TITULA: 'api/titula',
  FAJL: 'api/fajl',
  GODINA_STUDIJA_OBAVESTENJE: 'api/godina_studija_obavestenje',
  ISHOD: 'api/ishod',
  OBAVESTENJE: 'api/obavestenje',
  OBRAZOVNI_CILJ: 'api/obrazovni_cilj',
  PREDMET: 'api/predmet',
  PREDMET_TIP_DRUGOG_OBLIKA_NASTAVE: 'api/predmet_tip_drugog_oblika_nastave',
  PREDMET_TIP_NASTAVE: 'api/predmet_tip_nastave',
  PREDMET_USLOVNI_PREDMET: 'api/predmet_uslovni_predmet',
  REALIZACIJA_PREDMETA: 'api/realizacija_predmeta',
  SILABUS: 'api/silabus',
  DATUM_POHADJANJA_PREDMETA: 'api/datum_pohadjanja_predmeta',
  DIPLOMSKI_RAD: 'api/diplomski_rad',
  ISTRAZIVACKI_RAD: 'api/istrazivacki_rad',
  ISTRAZIVACKI_RAD_STUDENT_NA_STUDIJI:
    'api/istrazivacki_rad_student_na_studiji',
  POHADJANJE_PREDMETA: 'api/pohadjanje_predmeta',
  STUDENT: 'api/student',
  STUDENT_NA_STUDIJI: 'api/student_na_studiji',
};
