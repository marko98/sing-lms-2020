// const
import { SCHEMA, HOST, PORT } from '../const';

// model
import { Observable } from '../model/patterns/behavioural/observer/observable.model';
import { HttpClient } from '@angular/common/http';
import { Identifikacija } from '../model/interface/identifikacija.interface';

// interface
import { Entitet } from '../model/interface/entitet.interface';

// service
import { UIService } from './ui.service';

export class CRUDService<T extends Entitet<T, ID>, ID> extends Observable {
  protected url: string = SCHEMA + '://' + HOST + ':' + PORT + '/';
  protected entiteti: T[] = [];

  constructor(
    path: string,
    protected httpClient?: HttpClient,
    private uiService?: UIService
  ) {
    super();
    this.url += path;
  }

  public getAll = (): T[] => {
    // vracamo kopiju
    return this.entiteti.slice();
  };

  public getOne = (identifikacija: Identifikacija<ID>): T => {
    return this.entiteti.find(
      (entitet: Identifikacija<ID>) =>
        entitet.getId() === identifikacija.getId()
    );
  };

  //   read all
  protected findAll = (): void => {
    this.httpClient.get<T[]>(this.url).subscribe(
      (entiteti) => {
        this.entiteti = entiteti;
        this.notify();
      },
      (err) => {
        this.showError(err);
      }
    );
  };

  //   read one
  protected findOne = (identifikacija: Identifikacija<ID>): T => {
    this.httpClient
      .get<T>(this.url + '/' + identifikacija.getId().toString())
      .subscribe(
        (entitet) => {
          return entitet;
        },
        (err) => this.showError(err)
      );
    return undefined;
  };

  // create and update
  protected save = (entitet: T): void => {
    let m = this.findOne(entitet);

    if (!m) {
      this.httpClient.post(this.url, JSON.stringify(entitet)).subscribe(
        (entitet: T) => {
          this.entiteti.push(entitet);
          this.notify();
        },
        (err) => {
          this.showError(err);
        }
      );
    } else {
      this.httpClient.put(this.url, JSON.stringify(entitet)).subscribe(
        (updatedEntitet: T) => {
          this.entiteti.filter((entitet) => {
            if (entitet.getId() === updatedEntitet.getId())
              entitet = updatedEntitet;
            return true;
          });
          this.notify();
        },
        (err) => {
          this.showError(err);
        }
      );
    }
  };

  // delete by id
  protected deleteByIdentificator = (
    identifikacija: Identifikacija<ID>
  ): void => {
    this.httpClient
      .delete(this.url + '/' + identifikacija.getId().toString())
      .subscribe(
        (result) => {
          // console.log(result);
          this.entiteti = this.entiteti.filter(
            (entitet: T) => entitet.getId() !== identifikacija.getId()
          );
          this.notify();
        },
        (err) => {
          this.showError(err);
        }
      );
  };

  //   delete by model
  protected delete = (identifikacija: Identifikacija<ID>): void => {
    this.deleteByIdentificator(identifikacija);
  };

  private showError = (err: any): void => {
    console.log(err);
    this.uiService.showSnackbar(err.error, null, 1500);
  };

  // override-ujemo
  protected notify = () => {
    this.observers.forEach((observer) => {
      observer(this);
    });
  };
}
