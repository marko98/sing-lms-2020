import { Injectable } from '@angular/core';

// model
import { CRUDService } from './CRUD.service';
import { RegistrovaniKorisnik } from '../model/entity/registrovani_korisnik.model';

@Injectable({ providedIn: 'root' })
export class UserService extends CRUDService<RegistrovaniKorisnik, number> {
  // DOVRSI
}
