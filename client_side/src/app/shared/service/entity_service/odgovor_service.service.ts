import { CRUDService } from '../CRUD.service';
import { Odgovor } from '../../model/entity/odgovor.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class OdgovorService extends CRUDService<Odgovor, number> {
  constructor() {
    super(PATHS.ODGOVOR);
  }
}
