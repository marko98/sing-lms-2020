import { CRUDService } from '../CRUD.service';
import { DogadjajKalendar } from '../../model/entity/dogadjaj_kalendar.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class DogadjajKalendarService extends CRUDService<
  DogadjajKalendar,
  number
> {
  constructor() {
    super(PATHS.DOGADJAJ_KALENDAR);
  }
}
