import { CRUDService } from '../CRUD.service';
import { DatumPohadjanjaPredmeta } from '../../model/entity/datum_pohadjanja_predmeta.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class DatumPohadjanjaPredmetaService extends CRUDService<
  DatumPohadjanjaPredmeta,
  number
> {
  constructor() {
    super(PATHS.DATUM_POHADJANJA_PREDMETA);
  }
}
