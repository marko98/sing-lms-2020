import { CRUDService } from '../CRUD.service';
import { Odeljenje } from '../../model/entity/odeljenje.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class OdeljenjeService extends CRUDService<Odeljenje, number> {
  constructor() {
    super(PATHS.ODELJENJE);
  }
}
