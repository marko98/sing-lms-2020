import { CRUDService } from '../CRUD.service';
import { Univerzitet } from '../../model/entity/univerzitet.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class UniverzitetService extends CRUDService<Univerzitet, number> {
  constructor() {
    super(PATHS.UNIVERZITET);
  }
}
