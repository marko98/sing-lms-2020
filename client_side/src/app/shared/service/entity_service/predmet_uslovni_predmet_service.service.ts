import { CRUDService } from '../CRUD.service';
import { PredmetUslovniPredmet } from '../../model/entity/predmet_uslovni_predmet.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class PredmetUslovniPredmetService extends CRUDService<
  PredmetUslovniPredmet,
  number
> {
  constructor() {
    super(PATHS.PREDMET_USLOVNI_PREDMET);
  }
}
