import { CRUDService } from '../CRUD.service';
import { VremeOdrzavanjaUNedelji } from '../../model/entity/vreme_odrzavanja_u_nedelji.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class VremeOdrzavanjaUNedeljiService extends CRUDService<
  VremeOdrzavanjaUNedelji,
  number
> {
  constructor() {
    super(PATHS.VREME_ODRZAVANJA_U_NEDELJI);
  }
}
