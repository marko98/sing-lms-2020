import { CRUDService } from '../CRUD.service';
import { Drzava } from '../../model/entity/drzava.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class DrzavaService extends CRUDService<Drzava, number> {
  constructor() {
    super(PATHS.DRZAVA);
  }
}
