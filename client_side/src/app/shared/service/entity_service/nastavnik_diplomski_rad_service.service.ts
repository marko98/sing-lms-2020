import { CRUDService } from '../CRUD.service';
import { NastavnikDiplomskiRad } from '../../model/entity/nastavnik_diplomski_rad.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class NastavnikDiplomskiRadService extends CRUDService<
  NastavnikDiplomskiRad,
  number
> {
  constructor() {
    super(PATHS.NASTAVNIK_DIPLOMSKI_RAD);
  }
}
