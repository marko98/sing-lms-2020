import { CRUDService } from '../CRUD.service';
import { NaucnaOblast } from '../../model/entity/naucna_oblast.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class NaucnaOblastService extends CRUDService<NaucnaOblast, number> {
  constructor() {
    super(PATHS.NAUCNA_OBLAST);
  }
}
