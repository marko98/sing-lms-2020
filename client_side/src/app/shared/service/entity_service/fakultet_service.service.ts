import { CRUDService } from '../CRUD.service';
import { Fakultet } from '../../model/entity/fakultet.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class FakultetService extends CRUDService<Fakultet, number> {
  constructor() {
    super(PATHS.FAKULTET);
  }
}
