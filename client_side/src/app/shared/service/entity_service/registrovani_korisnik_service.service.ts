import { CRUDService } from '../CRUD.service';
import { RegistrovaniKorisnik } from '../../model/entity/registrovani_korisnik.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class RegistrovaniKorisnikService extends CRUDService<
  RegistrovaniKorisnik,
  number
> {
  constructor() {
    super(PATHS.REGISTROVANI_KORISNIK);
  }
}
