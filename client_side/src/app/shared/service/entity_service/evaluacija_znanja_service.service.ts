import { CRUDService } from '../CRUD.service';
import { EvaluacijaZnanja } from '../../model/entity/evaluacija_znanja.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class EvaluacijaZnanjaService extends CRUDService<
  EvaluacijaZnanja,
  number
> {
  constructor() {
    super(PATHS.EVALUACIJA_ZNANJA);
  }
}
