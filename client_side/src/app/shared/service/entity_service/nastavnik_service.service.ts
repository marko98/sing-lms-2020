import { CRUDService } from '../CRUD.service';
import { Nastavnik } from '../../model/entity/nastavnik.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class NastavnikService extends CRUDService<Nastavnik, number> {
  constructor() {
    super(PATHS.NASTAVNIK);
  }
}
