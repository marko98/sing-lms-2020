import { CRUDService } from '../CRUD.service';
import { NastavnikEvaluacijaZnanja } from '../../model/entity/nastavnik_evaluacija_znanja.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class NastavnikEvaluacijaZnanjaService extends CRUDService<
  NastavnikEvaluacijaZnanja,
  number
> {
  constructor() {
    super(PATHS.NASTAVNIK_EVALUACIJA_ZNANJA);
  }
}
