import { CRUDService } from '../CRUD.service';
import { NastavnikNaRealizaciji } from '../../model/entity/nastavnik_na_realizaciji.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class NastavnikNaRealizacijiService extends CRUDService<
  NastavnikNaRealizaciji,
  number
> {
  constructor() {
    super(PATHS.NASTAVNIK_NA_REALIZACIJI);
  }
}
