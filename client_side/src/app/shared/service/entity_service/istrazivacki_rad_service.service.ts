import { CRUDService } from '../CRUD.service';
import { IstrazivackiRad } from '../../model/entity/istrazivacki_rad.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class IstrazivackiRadService extends CRUDService<
  IstrazivackiRad,
  number
> {
  constructor() {
    super(PATHS.ISTRAZIVACKI_RAD);
  }
}
