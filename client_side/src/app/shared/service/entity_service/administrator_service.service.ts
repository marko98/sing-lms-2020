import { CRUDService } from '../CRUD.service';
import { Administrator } from '../../model/entity/administrator.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class AdministratorService extends CRUDService<Administrator, number> {
  constructor() {
    super(PATHS.ADMINISTRATOR);
  }
}
