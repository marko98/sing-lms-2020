import { CRUDService } from '../CRUD.service';
import { PredmetTipNastave } from '../../model/entity/predmet_tip_nastave.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class PredmetTipNastaveService extends CRUDService<
  PredmetTipNastave,
  number
> {
  constructor() {
    super(PATHS.PREDMET_TIP_NASTAVE);
  }
}
