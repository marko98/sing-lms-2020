import { CRUDService } from '../CRUD.service';
import { Ishod } from '../../model/entity/ishod.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class IshodService extends CRUDService<Ishod, number> {
  constructor() {
    super(PATHS.ISHOD);
  }
}
