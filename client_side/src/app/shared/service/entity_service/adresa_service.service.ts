import { CRUDService } from '../CRUD.service';
import { Adresa } from '../../model/entity/adresa.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class AdresaService extends CRUDService<Adresa, number> {
  constructor() {
    super(PATHS.ADRESA);
  }
}
