import { CRUDService } from '../CRUD.service';
import { GodinaStudija } from '../../model/entity/godina_studija.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class GodinaStudijaService extends CRUDService<GodinaStudija, number> {
  constructor() {
    super(PATHS.GODINA_STUDIJA);
  }
}
