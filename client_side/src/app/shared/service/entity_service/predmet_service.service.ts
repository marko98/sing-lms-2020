import { CRUDService } from '../CRUD.service';
import { Predmet } from '../../model/entity/predmet.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class PredmetService extends CRUDService<Predmet, number> {
  constructor() {
    super(PATHS.PREDMET);
  }
}
