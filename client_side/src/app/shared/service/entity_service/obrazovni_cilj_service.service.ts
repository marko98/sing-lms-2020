import { CRUDService } from '../CRUD.service';
import { ObrazovniCilj } from '../../model/entity/obrazovni_cilj.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class ObrazovniCiljService extends CRUDService<ObrazovniCilj, number> {
  constructor() {
    super(PATHS.OBRAZOVNI_CILJ);
  }
}
