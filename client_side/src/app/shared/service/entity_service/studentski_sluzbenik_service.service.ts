import { CRUDService } from '../CRUD.service';
import { StudentskiSluzbenik } from '../../model/entity/studentski_sluzbenik.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class StudentskiSluzbenikService extends CRUDService<
  StudentskiSluzbenik,
  number
> {
  constructor() {
    super(PATHS.STUDENTSKI_SLUZBENIK);
  }
}
