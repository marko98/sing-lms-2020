import { CRUDService } from '../CRUD.service';
import { AutorNastavniMaterijal } from '../../model/entity/autor_nastavni_materijal.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class AutorNastavniMaterijalService extends CRUDService<
  AutorNastavniMaterijal,
  number
> {
  constructor() {
    super(PATHS.AUTOR_NASTAVNI_MATERIJAL);
  }
}
