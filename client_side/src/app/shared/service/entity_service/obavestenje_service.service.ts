import { CRUDService } from '../CRUD.service';
import { Obavestenje } from '../../model/entity/obavestenje.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class ObavestenjeService extends CRUDService<Obavestenje, number> {
  constructor() {
    super(PATHS.OBAVESTENJE);
  }
}
