import { CRUDService } from '../CRUD.service';
import { Kalendar } from '../../model/entity/kalendar.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class KalendarService extends CRUDService<Kalendar, number> {
  constructor() {
    super(PATHS.KALENDAR);
  }
}
