import { CRUDService } from '../CRUD.service';
import { StudentNaStudiji } from '../../model/entity/student_na_studiji.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class StudentNaStudijiService extends CRUDService<
  StudentNaStudiji,
  number
> {
  constructor() {
    super(PATHS.STUDENT_NA_STUDIJI);
  }
}
