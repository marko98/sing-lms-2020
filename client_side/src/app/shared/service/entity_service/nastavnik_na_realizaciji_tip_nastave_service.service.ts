import { CRUDService } from '../CRUD.service';
import { NastavnikNaRealizacijiTipNastave } from '../../model/entity/nastavnik_na_realizaciji_tip_nastave.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class NastavnikNaRealizacijiTipNastaveService extends CRUDService<
  NastavnikNaRealizacijiTipNastave,
  number
> {
  constructor() {
    super(PATHS.NASTAVNIK_NA_REALIZACIJI_TIP_NASTAVE);
  }
}
