import { CRUDService } from '../CRUD.service';
import { NastavnikFakultet } from '../../model/entity/nastavnik_fakultet.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class NastavnikFakultetService extends CRUDService<
  NastavnikFakultet,
  number
> {
  constructor() {
    super(PATHS.NASTAVNIK_FAKULTET);
  }
}
