import { CRUDService } from '../CRUD.service';
import { Autor } from '../../model/entity/autor.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class AutorService extends CRUDService<Autor, number> {
  constructor() {
    super(PATHS.AUTOR);
  }
}
