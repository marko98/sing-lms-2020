import { CRUDService } from '../CRUD.service';
import { PredmetTipDrugogOblikaNastave } from '../../model/entity/predmet_tip_drugog_oblika_nastave.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class PredmetTipDrugogOblikaNastaveService extends CRUDService<
  PredmetTipDrugogOblikaNastave,
  number
> {
  constructor() {
    super(PATHS.PREDMET_TIP_DRUGOG_OBLIKA_NASTAVE);
  }
}
