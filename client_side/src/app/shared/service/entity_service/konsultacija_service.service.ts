import { CRUDService } from '../CRUD.service';
import { Konsultacija } from '../../model/entity/konsultacija.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class KonsultacijaService extends CRUDService<Konsultacija, number> {
  constructor() {
    super(PATHS.KONSULTACIJA);
  }
}
