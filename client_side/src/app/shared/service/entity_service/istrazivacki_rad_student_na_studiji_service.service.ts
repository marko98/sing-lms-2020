import { CRUDService } from '../CRUD.service';
import { IstrazivackiRadStudentNaStudiji } from '../../model/entity/istrazivacki_rad_student_na_studiji.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class IstrazivackiRadStudentNaStudijiService extends CRUDService<
  IstrazivackiRadStudentNaStudiji,
  number
> {
  constructor() {
    super(PATHS.ISTRAZIVACKI_RAD_STUDENT_NA_STUDIJI);
  }
}
