import { CRUDService } from '../CRUD.service';
import { Fajl } from '../../model/entity/fajl.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class FajlService extends CRUDService<Fajl, number> {
  constructor() {
    super(PATHS.FAJL);
  }
}
