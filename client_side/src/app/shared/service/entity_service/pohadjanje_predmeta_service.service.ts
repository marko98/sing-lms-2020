import { CRUDService } from '../CRUD.service';
import { PohadjanjePredmeta } from '../../model/entity/pohadjanje_predmeta.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class PohadjanjePredmetaService extends CRUDService<
  PohadjanjePredmeta,
  number
> {
  constructor() {
    super(PATHS.POHADJANJE_PREDMETA);
  }
}
