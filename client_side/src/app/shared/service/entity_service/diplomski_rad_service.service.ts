import { CRUDService } from '../CRUD.service';
import { DiplomskiRad } from '../../model/entity/diplomski_rad.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class DiplomskiRadService extends CRUDService<DiplomskiRad, number> {
  constructor() {
    super(PATHS.DIPLOMSKI_RAD);
  }
}
