import { CRUDService } from '../CRUD.service';
import { TerminNastave } from '../../model/entity/termin_nastave.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class TerminNastaveService extends CRUDService<TerminNastave, number> {
  constructor() {
    super(PATHS.TERMIN_NASTAVE);
  }
}
