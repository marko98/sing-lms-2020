import { CRUDService } from '../CRUD.service';
import { Titula } from '../../model/entity/titula.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class TitulaService extends CRUDService<Titula, number> {
  constructor() {
    super(PATHS.TITULA);
  }
}
