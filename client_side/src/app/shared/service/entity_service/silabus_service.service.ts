import { CRUDService } from '../CRUD.service';
import { Silabus } from '../../model/entity/silabus.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class SilabusService extends CRUDService<Silabus, number> {
  constructor() {
    super(PATHS.SILABUS);
  }
}
