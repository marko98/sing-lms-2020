import { CRUDService } from '../CRUD.service';
import { GodinaStudijaObavestenje } from '../../model/entity/godina_studija_obavestenje.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class GodinaStudijaObavestenjeService extends CRUDService<
  GodinaStudijaObavestenje,
  number
> {
  constructor() {
    super(PATHS.GODINA_STUDIJA_OBAVESTENJE);
  }
}
