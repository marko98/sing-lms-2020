import { CRUDService } from '../CRUD.service';
import { StudijskiProgram } from '../../model/entity/studijski_program.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class StudijskiProgramService extends CRUDService<
  StudijskiProgram,
  number
> {
  constructor() {
    super(PATHS.STUDIJSKI_PROGRAM);
  }
}
