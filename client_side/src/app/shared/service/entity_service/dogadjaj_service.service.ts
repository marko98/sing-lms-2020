import { CRUDService } from '../CRUD.service';
import { Dogadjaj } from '../../model/entity/dogadjaj.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class DogadjajService extends CRUDService<Dogadjaj, number> {
  constructor() {
    super(PATHS.DOGADJAJ);
  }
}
