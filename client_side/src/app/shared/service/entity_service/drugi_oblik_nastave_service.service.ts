import { CRUDService } from '../CRUD.service';
import { DrugiOblikNastave } from '../../model/entity/drugi_oblik_nastave.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class DrugiOblikNastaveService extends CRUDService<
  DrugiOblikNastave,
  number
> {
  constructor() {
    super(PATHS.DRUGI_OBLIK_NASTAVE);
  }
}
