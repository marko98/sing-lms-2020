import { Pitanje } from '../../model/entity/pitanje.model';
import { CRUDService } from '../CRUD.service';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class PitanjeService extends CRUDService<Pitanje, number> {
  constructor() {
    super(PATHS.PITANJE);
  }
}
