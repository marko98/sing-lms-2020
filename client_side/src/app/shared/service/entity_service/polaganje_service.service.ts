import { CRUDService } from '../CRUD.service';
import { Polaganje } from '../../model/entity/polaganje.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class PolaganjeService extends CRUDService<Polaganje, number> {
  constructor() {
    super(PATHS.POLAGANJE);
  }
}
