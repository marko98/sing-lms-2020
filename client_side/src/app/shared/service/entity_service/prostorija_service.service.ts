import { CRUDService } from '../CRUD.service';
import { Prostorija } from '../../model/entity/prostorija.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class ProstorijaService extends CRUDService<Prostorija, number> {
  constructor() {
    super(PATHS.PROSTORIJA);
  }
}
