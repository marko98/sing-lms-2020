import { CRUDService } from '../CRUD.service';
import { StudentskaSluzba } from '../../model/entity/studentska_sluzba.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class StudentskaSluzbaService extends CRUDService<
  StudentskaSluzba,
  number
> {
  constructor() {
    super(PATHS.STUDENTSKA_SLUZBA);
  }
}
