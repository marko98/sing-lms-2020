import { CRUDService } from '../CRUD.service';
import { EvaluacijaZnanjaNacinEvaluacije } from '../../model/entity/evaluacija_znanja_nacin_evaluacije.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class EvaluacijaZnanjaNacinEvaluacijeService extends CRUDService<
  EvaluacijaZnanjaNacinEvaluacije,
  number
> {
  constructor() {
    super(PATHS.EVALUACIJA_ZNANJA_NACIN_EVALUACIJE);
  }
}
