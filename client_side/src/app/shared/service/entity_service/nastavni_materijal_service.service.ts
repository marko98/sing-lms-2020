import { CRUDService } from '../CRUD.service';
import { NastavniMaterijal } from '../../model/entity/nastavni_materijal.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class NastavniMaterijalService extends CRUDService<
  NastavniMaterijal,
  number
> {
  constructor() {
    super(PATHS.NASTAVNI_MATERIJAL);
  }
}
