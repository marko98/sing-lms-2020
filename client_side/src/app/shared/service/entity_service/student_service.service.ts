import { Student } from '../../model/entity/student.model';
import { CRUDService } from '../CRUD.service';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class StudentService extends CRUDService<Student, number> {
  constructor() {
    super(PATHS.STUDENT);
  }
}
