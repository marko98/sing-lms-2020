import { CRUDService } from '../CRUD.service';
import { RealizacijaPredmeta } from '../../model/entity/realizacija_predmeta.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class RealizacijaPredmetaService extends CRUDService<
  RealizacijaPredmeta,
  number
> {
  constructor() {
    super(PATHS.REALIZACIJA_PREDMETA);
  }
}
