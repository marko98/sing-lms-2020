import { CRUDService } from '../CRUD.service';
import { Grad } from '../../model/entity/grad.model';
import { PATHS } from '../../const';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class GradService extends CRUDService<Grad, number> {
  constructor() {
    super(PATHS.GRAD);
  }
}
