Cloud-Native rešenja
Primena metoda etičkog hakovanja za procenu bezbednosti Olympus Box Web Servera
Funkcionalna i bezbednosna analiza DHCP protokola
Primena Katalanovih brojeva i nekih kombinatornih problema u kriptografiji
Aplikacija za evidenciju robe u magacinu
Neuromarketing
Falsifikovanje unakrsnih zahteva
Aplikacija za agenciju za izdavanje putničkih vozila
Primena teorije informacija u procesu generisanja kriptoloških ključeva
Analiza i primena kriptoanalitičkih metoda za potrebe penetracionog testiranja
Analiza finansijskih izveštaja Voda Vrnjci ad Vrnjačka Banja
Analiza finansijskih izveštaja Minaqua Doo
Restoran Tabor
Hotel Hyatt
Zaštita podataka na mobilnim uređajima sa studijom slučaja, Android OS
Sinteza sopstvenog izvora slučajnosti za generisanje kriptoloških ključeva
Sinteza sopstvenog izvora slučajnosti TRNG
Simulacija rada Flight Management Computer-a aviona tipa Boeing 737NG
Razvoj kripto modula za razmenu ključeva na Android platformi
Primjena kriptografije u automobilskoj industriji
Praktična analiza bezbednosti bežičnih računarskih mreža
Kriptologija u automobilskoj industriji
Finansijski analiza na primeru preduzeća Alfa-plam a.d. iz Vranja
Autentifikacija i autorizacija klijenta u sistemima za rad sa više servisa
Analiza finansijskih izveštaja, Imlek AD, Mlekara Subotica AD i Somboled d.o.o.
Analiza finansijskih izveštaja kompanije Sunoko d.o.o i kompanije Te-to a.d. 2013-2010
Analiza finansijskih izveštaja kompanija DM - drogerie markt d.o.o. i Lilly drogerie d.o.o. 2010-2013
Razvoj sistema autentifikacije na osnovu biometrije glasa
Primena digitalnih vodenih pečata za zaštitu dokumenata i skrivanja informacija
Poređenje performansi investicionih portfolia
Mogućnosti implementacije elektronskog servisa za glasanje preko Interneta u Srbiji
Identifikacija klijenata u veb aplikacijama
Evaluacija zaštite privatnosti na Internetu
Evaluacija bezbednosti veb servisa Cloud-a sa studijom slučaja Dropbox
Elektronski kanali bankarskog poslovanja
Curenje informacija preko bočnih kanala i njihov uticaj na bezbednost kriptografskih mehanizama
Biometrija privatnosti
Analiza finansijskih izvještaja, Dahlia d.o.o. i Henkel d.o.o.
Analiza finansijskih izveštaja za preduzeća Gebi d.o.o. i Sto posto d.o.o.
Analiza finansijskih izveštaja kompanija za kablovske telekomunikacije SBB i IKOM
Razvoj sopstvenog rešenja za kriptografsku zaštitu sa implementiranim modulom za generisanje simetričnog kriptološkog ključa
Razvoj servisa autentifikacije zasnovanog na kombinovanju biometrije otiska prsta i lozinki
Kriptografska zaštita baza podataka na deljenom Veb hosting-u
Konstruisanje pseudoslučajnog generatora PRNG na osnovu slučajnih početnih stanja, dobijenih preko fizičkog izvora informacija TRNG
Implementacija bezbednosnog rešenja u okviru školskog centra kompanije CPU d.o.o.
Digitalno potpisivanje dokumenata u lokalnoj mreži sa sopstvenim CA
Berzansko tržište stranih valuta , Foreks
Bankarstvo i kamatni mehanizam
Analiza finansijskih izveštaja kompanija za proizvodnju neelektričnih aparata za domaćinstva
Psihološki ugovor između zaposlenih i menadžmenta