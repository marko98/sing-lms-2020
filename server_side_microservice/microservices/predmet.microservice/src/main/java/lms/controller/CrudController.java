package lms.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.core.JsonProcessingException;

import lms.dto.interfaces.DTO;
import lms.enums.StanjeModela;
import lms.model.interfaces.Entitet;
import lms.service.CrudService;
import lms.websocket.SocketHandler;
import lms.websocket.WSMessageKeys;

public abstract class CrudController<T extends Entitet<T, ID>, ID extends Serializable> {

    @Autowired
    protected CrudService<T, ID> service;

    @Autowired
    protected SocketHandler<T, ID> socketHandler;

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<List<DTO<ID>>> findAll() {

	List<T> list = StreamSupport.stream(this.service.findAll().spliterator(), false).collect(Collectors.toList());

	return new ResponseEntity<List<DTO<ID>>>(list.stream().map(t -> t.getDTO()).collect(Collectors.toList()),
		HttpStatus.OK);
    }

//    http://localhost:8080/api/adresa?page=0&size=2&sort=tip,desc

    @RequestMapping(path = "/", method = RequestMethod.GET)
    public ResponseEntity<Page<DTO<ID>>> findAll(Pageable pageable) {
	System.out.println("Pageable: " + pageable); // Page request [number: 0, size 20, sort: UNSORTED]

//	get Page<T>
	Page<T> page = this.service.findAll(pageable);

//	map T to DTO and get Page<DTO>
	Page<DTO<ID>> pageDTO = page.map(t -> t.getDTO());

	return new ResponseEntity<Page<DTO<ID>>>(pageDTO, HttpStatus.OK);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> findOne(@PathVariable("id") ID id) {
	Optional<T> t = this.service.findOne(id);

	if (t.isPresent())
	    return new ResponseEntity<DTO<ID>>(t.get().getDTO(), HttpStatus.OK);
	else
	    return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<?> create(@RequestBody T model) {
//	check for id
	if (model.getId() != null) {
//		id exists, lets try to find model with given id
	    Optional<T> t = this.service.findOne(model.getId());

	    if (t.isEmpty()) {
//			we didn't find model, lets create one and return DTO
		model = this.service.save(model);

		this.socketHandler.sendObjectAsMessage(WSMessageKeys.ON_ENTITY_CREATED, model.getDTO());
		return new ResponseEntity<DTO<ID>>(model.getDTO(), HttpStatus.CREATED);
	    } else {
//			model is found, lets return HttpStatus.CONFLICT
		return new ResponseEntity<Void>(HttpStatus.CONFLICT);
	    }
	} else {
//		id doesn't exist, so model doesn't exist lets create one and return DTO
	    model = this.service.save(model);
	    this.socketHandler.sendObjectAsMessage(WSMessageKeys.ON_ENTITY_CREATED, model.getDTO());
	    return new ResponseEntity<DTO<ID>>(model.getDTO(), HttpStatus.CREATED);
	}
    }

    @RequestMapping(path = "/aktiviraj", method = RequestMethod.POST)
    public ResponseEntity<?> activate(@RequestBody T model) throws JsonProcessingException, IOException {
//	check for id
	if (model.getId() != null) {
//		id exists, lets try to find model with given id
	    Optional<T> t = this.service.findOne(model.getId());

	    if (t.isEmpty()) {
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	    } else {
		T tModel = t.get();
		tModel.setStanje(StanjeModela.AKTIVAN);
		tModel = this.service.save(tModel);
		this.socketHandler.sendObjectAsMessage(WSMessageKeys.ON_ENTITY_UPDATED, tModel.getDTO());
		return new ResponseEntity<DTO<ID>>(tModel.getDTO(), HttpStatus.OK);
	    }
	} else {
	    return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
    }

    @RequestMapping(path = "/deaktiviraj", method = RequestMethod.POST)
    public ResponseEntity<?> deactivate(@RequestBody T model) throws JsonProcessingException, IOException {
//	check for id
	if (model.getId() != null) {
//		id exists, lets try to find model with given id
	    Optional<T> t = this.service.findOne(model.getId());

	    if (t.isEmpty()) {
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	    } else {
		T tModel = t.get();
		tModel.setStanje(StanjeModela.NEAKTIVAN);
		tModel = this.service.save(tModel);
		this.socketHandler.sendObjectAsMessage(WSMessageKeys.ON_ENTITY_UPDATED, tModel.getDTO());
		return new ResponseEntity<DTO<ID>>(tModel.getDTO(), HttpStatus.OK);
	    }
	} else {
	    return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
    }

    @RequestMapping(path = "", method = RequestMethod.PUT)
    public ResponseEntity<?> update(@RequestBody T model) throws JsonProcessingException, IOException {
//	check for id
	if (model.getId() != null) {
//		id exists, lets try to find model
	    Optional<T> t = this.service.findOne(model.getId());

	    if (t.isPresent()) {
//			model is found, lets update it and return DTO
		T tModel = t.get();
		tModel.update(model);
		tModel = this.service.save(tModel);
		this.socketHandler.sendObjectAsMessage(WSMessageKeys.ON_ENTITY_UPDATED, tModel.getDTO());
		return new ResponseEntity<DTO<ID>>(tModel.getDTO(), HttpStatus.OK);
	    } else {
//			we didn't find model, lets return HttpStatus.NOT_FOUND
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	    }
	} else {
//		id doesn't exists, let return HttpStatus.NOT_FOUND
	    return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> delete(@PathVariable("id") ID id) throws JsonProcessingException, IOException {
//	try to find model
	Optional<T> t = this.service.findOne(id);

	if (t.isPresent()) {
//		model is found, lets delete it and return HttpStatus.OK
	    Entitet<T, ID> tModel = t.get();
	    tModel.setStanje(StanjeModela.OBRISAN);
	    DTO<ID> dto = tModel.getDTO();
	    this.service.delete(t.get());
	    this.socketHandler.sendObjectAsMessage(WSMessageKeys.ON_ENTITY_DELETED, dto);
	    return new ResponseEntity<Void>(HttpStatus.OK);
	} else {
//		we didn't find model, lets return HttpStatus.NOT_FOUND
	    return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
    }

    @RequestMapping(path = "", method = RequestMethod.DELETE)
    public ResponseEntity<?> delete(@RequestBody T model) throws JsonProcessingException, IOException {
//	check for id
	if (model.getId() != null) {
//		id exists, lets try to find model
	    Optional<T> t = this.service.findOne(model.getId());

	    if (t.isPresent()) {
//			model is found, lets delete it and return HttpStatus.OK
		Entitet<T, ID> tModel = t.get();
		tModel.setStanje(StanjeModela.OBRISAN);
		DTO<ID> dto = tModel.getDTO();
		this.service.delete(t.get());
		this.socketHandler.sendObjectAsMessage(WSMessageKeys.ON_ENTITY_DELETED, dto);
		return new ResponseEntity<Void>(HttpStatus.OK);
	    } else {
//			we didn't find model, lets return HttpStatus.NOT_FOUND
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	    }
	} else {
//		id doesn't exists, let return HttpStatus.NOT_FOUND
	    return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
    }

}