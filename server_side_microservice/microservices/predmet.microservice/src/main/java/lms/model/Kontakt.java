package lms.model;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.KontaktDTO;
import lms.enums.StanjeModela;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE kontakt SET stanje = 'OBRISAN' WHERE kontakt_id = ? AND version = ? ", check = ResultCheckStyle.COUNT)
@Table(name = "kontakt")
public class Kontakt implements Entitet<Kontakt, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 7246601556631425606L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "kontakt_id")
    private Long id;

    private String mobilni;

    private String fiksni;

    @Version
    private Long version;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "univerzitet_id")
    private Univerzitet univerzitet;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "fakultet_id")
    private Fakultet fakultet;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getMobilni() {
	return this.mobilni;
    }

    public void setMobilni(String mobilni) {
	this.mobilni = mobilni;
    }

    public String getFiksni() {
	return this.fiksni;
    }

    public void setFiksni(String fiksni) {
	this.fiksni = fiksni;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Univerzitet getUniverzitet() {
	return this.univerzitet;
    }

    public void setUniverzitet(Univerzitet univerzitet) {
	this.univerzitet = univerzitet;
    }

    public Fakultet getFakultet() {
	return this.fakultet;
    }

    public void setFakultet(Fakultet fakultet) {
	this.fakultet = fakultet;
    }

    public Kontakt() {
	super();
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public KontaktDTO getDTO() {
	KontaktDTO kontaktDTO = new KontaktDTO();
	kontaktDTO.setId(id);
	kontaktDTO.setFiksni(fiksni);
	kontaktDTO.setMobilni(mobilni);
	kontaktDTO.setStanje(stanje);
	kontaktDTO.setVersion(version);

	if (this.fakultet != null)
	    kontaktDTO.setFakultetDTO(this.fakultet.getDTOinsideDTO());

	if (this.univerzitet != null)
	    kontaktDTO.setUniverzitetDTO(this.univerzitet.getDTOinsideDTO());

	return kontaktDTO;
    }

    @Override
    public KontaktDTO getDTOinsideDTO() {
	KontaktDTO kontaktDTO = new KontaktDTO();
	kontaktDTO.setId(id);
	kontaktDTO.setFiksni(fiksni);
	kontaktDTO.setMobilni(mobilni);
	kontaktDTO.setStanje(stanje);
	kontaktDTO.setVersion(version);
	return kontaktDTO;
    }

    @Override
    public void update(Kontakt entitet) {
	this.setFiksni(entitet.getFiksni());
	this.setMobilni(entitet.getMobilni());
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());

	this.setFakultet(entitet.getFakultet());
	this.setUniverzitet(entitet.getUniverzitet());

    }

}