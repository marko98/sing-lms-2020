package lms.model;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.OdgovorDTO;
import lms.dto.PitanjeDTO;
import lms.enums.StanjeModela;
import lms.interfaces.Identifikacija;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE pitanje SET stanje = 'OBRISAN' WHERE pitanje_id = ? AND version = ? ", check = ResultCheckStyle.COUNT)
@Table(name = "pitanje")
@Transactional
public class Pitanje implements Entitet<Pitanje, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -5814779900413676006L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "pitanje_id")
    private Long id;

    private String oblast;

    private String pitanje;

    private String putanjaZaSliku;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "evaluacija_znanja_id")
    private EvaluacijaZnanja evaluacijaZnanja;

    @javax.persistence.OneToMany(mappedBy = "pitanje")
    private java.util.List<Odgovor> odgovori;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getOblast() {
	return this.oblast;
    }

    public void setOblast(String oblast) {
	this.oblast = oblast;
    }

    public String getPitanje() {
	return this.pitanje;
    }

    public void setPitanje(String pitanje) {
	this.pitanje = pitanje;
    }

    public String getPutanjaZaSliku() {
	return this.putanjaZaSliku;
    }

    public void setPutanjaZaSliku(String putanjaZaSliku) {
	this.putanjaZaSliku = putanjaZaSliku;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public EvaluacijaZnanja getEvaluacijaZnanja() {
	return this.evaluacijaZnanja;
    }

    public void setEvaluacijaZnanja(EvaluacijaZnanja evaluacijaZnanja) {
	this.evaluacijaZnanja = evaluacijaZnanja;
    }

    public java.util.List<Odgovor> getOdgovori() {
	return this.odgovori;
    }

    public void setOdgovori(java.util.List<Odgovor> odgovori) {
	this.odgovori = odgovori;
    }

    public Pitanje() {
	super();
	this.odgovori = new java.util.ArrayList<>();
    }

    public Odgovor getOdgovor(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addOdgovor(Odgovor odgovor) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeOdgovor(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((evaluacijaZnanja == null) ? 0 : evaluacijaZnanja.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((oblast == null) ? 0 : oblast.hashCode());
	result = prime * result + ((odgovori == null) ? 0 : odgovori.hashCode());
	result = prime * result + ((pitanje == null) ? 0 : pitanje.hashCode());
	result = prime * result + ((putanjaZaSliku == null) ? 0 : putanjaZaSliku.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Pitanje other = (Pitanje) obj;
	if (evaluacijaZnanja == null) {
	    if (other.evaluacijaZnanja != null)
		return false;
	} else if (!evaluacijaZnanja.equals(other.evaluacijaZnanja))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (oblast == null) {
	    if (other.oblast != null)
		return false;
	} else if (!oblast.equals(other.oblast))
	    return false;
	if (odgovori == null) {
	    if (other.odgovori != null)
		return false;
	} else if (!odgovori.equals(other.odgovori))
	    return false;
	if (pitanje == null) {
	    if (other.pitanje != null)
		return false;
	} else if (!pitanje.equals(other.pitanje))
	    return false;
	if (putanjaZaSliku == null) {
	    if (other.putanjaZaSliku != null)
		return false;
	} else if (!putanjaZaSliku.equals(other.putanjaZaSliku))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public PitanjeDTO getDTO() {
	PitanjeDTO pitanjeDTO = new PitanjeDTO();
	pitanjeDTO.setId(id);
	pitanjeDTO.setOblast(oblast);
	pitanjeDTO.setPitanje(pitanje);
	pitanjeDTO.setPutanjaZaSliku(putanjaZaSliku);
	pitanjeDTO.setStanje(stanje);
	pitanjeDTO.setVersion(version);

	if (this.evaluacijaZnanja != null)
	    pitanjeDTO.setEvaluacijaZnanjaDTO(this.evaluacijaZnanja.getDTOinsideDTO());

	ArrayList<OdgovorDTO> odgovoriDTOs = new ArrayList<OdgovorDTO>();
	for (Odgovor odgovor : this.odgovori) {
	    odgovoriDTOs.add(odgovor.getDTOinsideDTO());
	}
	pitanjeDTO.setOdgovoriDTO(odgovoriDTOs);

	return pitanjeDTO;
    }

    @Override
    public PitanjeDTO getDTOinsideDTO() {
	PitanjeDTO pitanjeDTO = new PitanjeDTO();
	pitanjeDTO.setId(id);
	pitanjeDTO.setOblast(oblast);
	pitanjeDTO.setPitanje(pitanje);
	pitanjeDTO.setPutanjaZaSliku(putanjaZaSliku);
	pitanjeDTO.setStanje(stanje);
	pitanjeDTO.setVersion(version);
	return pitanjeDTO;
    }

    @Override
    public void update(Pitanje entitet) {
	this.setOblast(entitet.getOblast());
	this.setPitanje(entitet.getPitanje());
	this.setPutanjaZaSliku(entitet.getPutanjaZaSliku());
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());

	this.setEvaluacijaZnanja(entitet.getEvaluacijaZnanja());

    }

}