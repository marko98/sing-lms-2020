package lms.model;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.DogadjajKalendarDTO;
import lms.enums.StanjeModela;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE dogadjaj_kalendar SET stanje = 'OBRISAN' WHERE dogadjaj_kalendar_id = ? AND version = ? ", check = ResultCheckStyle.COUNT)
@Table(name = "dogadjaj_kalendar")
@Transactional
public class DogadjajKalendar implements Entitet<DogadjajKalendar, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 5341811358852604833L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "dogadjaj_kalendar_id")
    private Long id;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "kalendar_id")
    private Kalendar kalendar;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "dogadjaj_id")
    private Dogadjaj dogadjaj;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public Kalendar getKalendar() {
	return this.kalendar;
    }

    public void setKalendar(Kalendar kalendar) {
	this.kalendar = kalendar;
    }

    public Dogadjaj getDogadjaj() {
	return this.dogadjaj;
    }

    public void setDogadjaj(Dogadjaj dogadjaj) {
	this.dogadjaj = dogadjaj;
    }

    public DogadjajKalendar() {
	super();
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((dogadjaj == null) ? 0 : dogadjaj.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((kalendar == null) ? 0 : kalendar.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	DogadjajKalendar other = (DogadjajKalendar) obj;
	if (dogadjaj == null) {
	    if (other.dogadjaj != null)
		return false;
	} else if (!dogadjaj.equals(other.dogadjaj))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (kalendar == null) {
	    if (other.kalendar != null)
		return false;
	} else if (!kalendar.equals(other.kalendar))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public DogadjajKalendarDTO getDTO() {
	DogadjajKalendarDTO dogadjajKalendarDTO = new DogadjajKalendarDTO();
	dogadjajKalendarDTO.setId(id);
	dogadjajKalendarDTO.setStanje(stanje);
	dogadjajKalendarDTO.setVersion(version);

	if (this.dogadjaj != null)
	    dogadjajKalendarDTO.setDogadjajDTO(this.dogadjaj.getDTOinsideDTO());
	if (this.kalendar != null)
	    dogadjajKalendarDTO.setKalendarDTO(this.kalendar.getDTOinsideDTO());

	return dogadjajKalendarDTO;
    }

    @Override
    public DogadjajKalendarDTO getDTOinsideDTO() {
	DogadjajKalendarDTO dogadjajKalendarDTO = new DogadjajKalendarDTO();
	dogadjajKalendarDTO.setId(id);
	dogadjajKalendarDTO.setStanje(stanje);
	dogadjajKalendarDTO.setVersion(version);

	return dogadjajKalendarDTO;
    }

    @Override
    public void update(DogadjajKalendar entitet) {
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());

	this.setDogadjaj(entitet.getDogadjaj());
	this.setKalendar(entitet.getKalendar());

    }

}