package lms.model;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.PredmetTipNastaveDTO;
import lms.enums.StanjeModela;
import lms.enums.TipNastave;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE predmet_tip_nastave SET stanje = 'OBRISAN' WHERE predmet_tip_nastave_id = ? AND version = ? ", check = ResultCheckStyle.COUNT)
@Table(name = "predmet_tip_nastave")
@Transactional
public class PredmetTipNastave implements Entitet<PredmetTipNastave, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 7968576498308863037L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "predmet_tip_nastave_id")
    private Long id;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "predmet_id")
    private Predmet predmet;

    @Enumerated(EnumType.STRING)
    private TipNastave tipNastave;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public Predmet getPredmet() {
	return this.predmet;
    }

    public void setPredmet(Predmet predmet) {
	this.predmet = predmet;
    }

    public TipNastave getTipNastave() {
	return this.tipNastave;
    }

    public void setTipNastave(TipNastave tipNastave) {
	this.tipNastave = tipNastave;
    }

    public PredmetTipNastave() {
	super();
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((predmet == null) ? 0 : predmet.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((tipNastave == null) ? 0 : tipNastave.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	PredmetTipNastave other = (PredmetTipNastave) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (predmet == null) {
	    if (other.predmet != null)
		return false;
	} else if (!predmet.equals(other.predmet))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (tipNastave != other.tipNastave)
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public PredmetTipNastaveDTO getDTO() {
	PredmetTipNastaveDTO predmetTipNastaveDTO = new PredmetTipNastaveDTO();
	predmetTipNastaveDTO.setId(id);
	predmetTipNastaveDTO.setStanje(stanje);
	predmetTipNastaveDTO.setTipNastave(tipNastave);
	predmetTipNastaveDTO.setVersion(version);

	if (this.predmet != null)
	    predmetTipNastaveDTO.setPredmetDTO(this.predmet.getDTOinsideDTO());

	return predmetTipNastaveDTO;
    }

    @Override
    public PredmetTipNastaveDTO getDTOinsideDTO() {
	PredmetTipNastaveDTO predmetTipNastaveDTO = new PredmetTipNastaveDTO();
	predmetTipNastaveDTO.setId(id);
	predmetTipNastaveDTO.setStanje(stanje);
	predmetTipNastaveDTO.setTipNastave(tipNastave);
	predmetTipNastaveDTO.setVersion(version);
	return predmetTipNastaveDTO;
    }

    @Override
    public void update(PredmetTipNastave entitet) {
	this.setStanje(entitet.getStanje());
	this.setTipNastave(entitet.getTipNastave());
	this.setVersion(entitet.getVersion());

	this.setPredmet(entitet.getPredmet());

    }

}