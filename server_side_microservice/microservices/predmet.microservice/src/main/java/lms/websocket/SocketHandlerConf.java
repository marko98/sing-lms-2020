package lms.websocket;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lms.model.ObrazovniCilj;
import lms.model.Predmet;
import lms.model.PredmetTipDrugogOblikaNastave;
import lms.model.PredmetTipNastave;
import lms.model.PredmetUslovniPredmet;
import lms.model.RealizacijaPredmeta;
import lms.model.Silabus;

@Configuration
public class SocketHandlerConf {
    @Bean
    public SocketHandler<ObrazovniCilj, Long> getObrazovniCiljSocketHandler() {
	return new SocketHandler<ObrazovniCilj, Long>("/obrazovni_cilj_ws");
    }

    @Bean
    public SocketHandler<Predmet, Long> getPredmetSocketHandler() {
	return new SocketHandler<Predmet, Long>("/predmet_ws");
    }

    @Bean
    public SocketHandler<PredmetTipDrugogOblikaNastave, Long> getPredmetTipDrugogOblikaNastaveSocketHandler() {
	return new SocketHandler<PredmetTipDrugogOblikaNastave, Long>("/predmet_tip_drugog_oblika_nastave_ws");
    }
    
    @Bean
    public SocketHandler<PredmetTipNastave, Long> getPredmetTipNastaveSocketHandler() {
	return new SocketHandler<PredmetTipNastave, Long>("/predmet_tip_nastave_ws");
    }
    
    @Bean
    public SocketHandler<PredmetUslovniPredmet, Long> getPredmetUslovniPredmetSocketHandler() {
	return new SocketHandler<PredmetUslovniPredmet, Long>("/predmet_uslovni_predmet_ws");
    }
    
    @Bean
    public SocketHandler<RealizacijaPredmeta, Long> getRealizacijaPredmetaSocketHandler() {
	return new SocketHandler<RealizacijaPredmeta, Long>("/realizacija_predmeta_ws");
    }
    
    @Bean
    public SocketHandler<Silabus, Long> getSilabusSocketHandler() {
	return new SocketHandler<Silabus, Long>("/silabus_ws");
    }
}