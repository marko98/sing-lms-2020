package lms.websocket;

public enum WSMessageKeys {
    ON_CONNECTED,
    ON_ENTITY_DELETED,
    ON_ENTITY_UPDATED,
    ON_ENTITY_CREATED,
}
