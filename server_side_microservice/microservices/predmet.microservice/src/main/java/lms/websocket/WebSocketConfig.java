package lms.websocket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

import lms.model.ObrazovniCilj;
import lms.model.Predmet;
import lms.model.PredmetTipDrugogOblikaNastave;
import lms.model.PredmetTipNastave;
import lms.model.PredmetUslovniPredmet;
import lms.model.RealizacijaPredmeta;
import lms.model.Silabus;

@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

    @Autowired
    private SocketHandler<ObrazovniCilj, Long> obrazovniCilj;

    @Autowired
    private SocketHandler<Predmet, Long> predmet;

    @Autowired
    private SocketHandler<PredmetTipDrugogOblikaNastave, Long> predmetTipDrugogOblikaNastave;

    @Autowired
    private SocketHandler<PredmetTipNastave, Long> predmetTipNastave;

    @Autowired
    private SocketHandler<PredmetUslovniPredmet, Long> predmetUslovniPredmet;

    @Autowired
    private SocketHandler<RealizacijaPredmeta, Long> realizacijaPredmeta;

    @Autowired
    private SocketHandler<Silabus, Long> silabus;

    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {

	registry.addHandler(obrazovniCilj, "/obrazovni_cilj_ws").setAllowedOrigins("*");
	registry.addHandler(predmet, "/predmet_ws").setAllowedOrigins("*");
	registry.addHandler(predmetTipDrugogOblikaNastave, "/predmet_tip_drugog_oblika_nastave_ws")
		.setAllowedOrigins("*");
	registry.addHandler(predmetTipNastave, "/predmet_tip_nastave_ws").setAllowedOrigins("*");
	registry.addHandler(predmetUslovniPredmet, "/predmet_uslovni_predmet_ws").setAllowedOrigins("*");
	registry.addHandler(realizacijaPredmeta, "/realizacija_predmeta_ws").setAllowedOrigins("*");
	registry.addHandler(silabus, "/silabus_ws").setAllowedOrigins("*");

    }

}
