package lms.dto;

import java.sql.Time;

import lms.dto.interfaces.DTO;
import lms.enums.Dan;
import lms.enums.StanjeModela;

public class VremeOdrzavanjaUNedeljiDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -1609762099494341949L;

    private Long id;

    private Time vreme;

    private StanjeModela stanje;

    private Long version;

    private KonsultacijaDTO konsultacijaDTO;

    private Dan dan;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Time getVreme() {
	return this.vreme;
    }

    public void setVreme(Time vreme) {
	this.vreme = vreme;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public KonsultacijaDTO getKonsultacijaDTO() {
	return this.konsultacijaDTO;
    }

    public void setKonsultacijaDTO(KonsultacijaDTO konsultacijaDTO) {
	this.konsultacijaDTO = konsultacijaDTO;
    }

    public Dan getDan() {
	return this.dan;
    }

    public void setDan(Dan dan) {
	this.dan = dan;
    }

    public VremeOdrzavanjaUNedeljiDTO() {
	super();
    }

}