package lms.predmet.microservice.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.PredmetTipNastave;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class PredmetTipNastaveRepository extends CrudRepository<PredmetTipNastave, Long> {

}