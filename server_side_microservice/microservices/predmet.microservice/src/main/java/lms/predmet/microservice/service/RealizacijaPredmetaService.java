package lms.predmet.microservice.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.RealizacijaPredmeta;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class RealizacijaPredmetaService extends CrudService<RealizacijaPredmeta, Long> {

}