package lms.predmet.microservice.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.PredmetTipDrugogOblikaNastave;

@Controller
@Scope("singleton")
@RequestMapping(path = "predmet_tip_drugog_oblika_nastave")
@CrossOrigin(origins = "*")
public class PredmetTipDrugogOblikaNastaveController extends CrudController<PredmetTipDrugogOblikaNastave, Long> {

}