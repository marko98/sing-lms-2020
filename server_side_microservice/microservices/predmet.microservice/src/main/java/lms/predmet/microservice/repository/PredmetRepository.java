package lms.predmet.microservice.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.Predmet;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class PredmetRepository extends CrudRepository<Predmet, Long> {

}