package lms.predmet.microservice.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.Silabus;

@Controller
@Scope("singleton")
@RequestMapping(path = "silabus")
@CrossOrigin(origins = "*")
public class SilabusController extends CrudController<Silabus, Long> {

}