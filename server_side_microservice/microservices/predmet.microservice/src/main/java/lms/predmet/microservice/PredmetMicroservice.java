package lms.predmet.microservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jms.annotation.EnableJms;

@SpringBootApplication
@ComponentScan(basePackages = { "lms.*" })
@EntityScan(basePackages = { "lms.model" })
@EnableJpaRepositories({ "lms.predmet.microservice.repository", "lms.security.repository" })
@EnableJms
@EnableEurekaClient
public class PredmetMicroservice {

    public static void main(String[] args) {
	SpringApplication.run(PredmetMicroservice.class, args);

    }

}
