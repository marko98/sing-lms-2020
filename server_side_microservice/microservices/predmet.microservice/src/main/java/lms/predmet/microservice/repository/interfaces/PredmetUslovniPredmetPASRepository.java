package lms.predmet.microservice.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.PredmetUslovniPredmet;

@Repository
@Scope("singleton")
public interface PredmetUslovniPredmetPASRepository extends PagingAndSortingRepository<PredmetUslovniPredmet, Long> {

}