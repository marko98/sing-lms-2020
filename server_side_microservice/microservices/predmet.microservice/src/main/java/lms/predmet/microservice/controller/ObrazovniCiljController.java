package lms.predmet.microservice.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.ObrazovniCilj;

@Controller
@Scope("singleton")
@RequestMapping(path = "obrazovni_cilj")
@CrossOrigin(origins = "*")
public class ObrazovniCiljController extends CrudController<ObrazovniCilj, Long> {

}