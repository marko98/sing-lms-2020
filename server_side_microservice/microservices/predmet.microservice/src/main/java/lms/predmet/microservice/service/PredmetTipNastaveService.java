package lms.predmet.microservice.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.PredmetTipNastave;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class PredmetTipNastaveService extends CrudService<PredmetTipNastave, Long> {

}