package lms.predmet.microservice.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.Silabus;

@Repository
@Scope("singleton")
public interface SilabusPASRepository extends PagingAndSortingRepository<Silabus, Long> {

}