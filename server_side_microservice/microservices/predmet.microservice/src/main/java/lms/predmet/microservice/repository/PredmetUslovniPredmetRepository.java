package lms.predmet.microservice.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.PredmetUslovniPredmet;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class PredmetUslovniPredmetRepository extends CrudRepository<PredmetUslovniPredmet, Long> {

}