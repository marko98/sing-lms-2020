package lms.predmet.microservice.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.Predmet;

@Controller
@Scope("singleton")
@RequestMapping(path = "predmet")
@CrossOrigin(origins = "*")
public class PredmetController extends CrudController<Predmet, Long> {

}