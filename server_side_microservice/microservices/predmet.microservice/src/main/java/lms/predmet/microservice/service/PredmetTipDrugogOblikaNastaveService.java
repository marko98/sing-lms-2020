package lms.predmet.microservice.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.PredmetTipDrugogOblikaNastave;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class PredmetTipDrugogOblikaNastaveService extends CrudService<PredmetTipDrugogOblikaNastave, Long> {

}