package lms.security.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.RegistrovaniKorisnik;

@Repository
@Scope("singleton")
public interface SecurityPASRepository extends PagingAndSortingRepository<RegistrovaniKorisnik, Long> {

}