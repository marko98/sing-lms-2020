package lms.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.RegistrovaniKorisnikRolaDTO;
import lms.enums.StanjeModela;
import lms.model.interfaces.Entitet;

@SQLDelete(sql = "UPDATE registrovani_korisnik_rola SET stanje = 'OBRISAN' WHERE id = ? AND version = ? ", check = ResultCheckStyle.COUNT)
@Table(name = "registrovani_korisnik_rola")
@Transactional
@Entity
public class RegistrovaniKorisnikRola implements Entitet<RegistrovaniKorisnikRola, Long> {
    /**
     * 
     */
    private static final long serialVersionUID = -6215959623661380984L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @ManyToOne
    private RegistrovaniKorisnik registrovaniKorisnik;

    @ManyToOne
    private Rola rola;

    public RegistrovaniKorisnikRola(RegistrovaniKorisnik registrovaniKorisnik, Rola rola) {
	super();
	this.registrovaniKorisnik = registrovaniKorisnik;
	this.rola = rola;
    }

    public RegistrovaniKorisnikRola() {
	super();
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public RegistrovaniKorisnik getRegistrovaniKorisnik() {
	return registrovaniKorisnik;
    }

    public void setRegistrovaniKorisnik(RegistrovaniKorisnik registrovaniKorisnik) {
	this.registrovaniKorisnik = registrovaniKorisnik;
    }

    public Rola getRola() {
	return rola;
    }

    public void setRola(Rola rola) {
	this.rola = rola;
    }

    public StanjeModela getStanje() {
	return stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((registrovaniKorisnik == null) ? 0 : registrovaniKorisnik.hashCode());
	result = prime * result + ((rola == null) ? 0 : rola.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	RegistrovaniKorisnikRola other = (RegistrovaniKorisnikRola) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (registrovaniKorisnik == null) {
	    if (other.registrovaniKorisnik != null)
		return false;
	} else if (!registrovaniKorisnik.equals(other.registrovaniKorisnik))
	    return false;
	if (rola == null) {
	    if (other.rola != null)
		return false;
	} else if (!rola.equals(other.rola))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public RegistrovaniKorisnikRolaDTO getDTO() {
	RegistrovaniKorisnikRolaDTO registrovaniKorisnikRolaDTO = new RegistrovaniKorisnikRolaDTO();
	registrovaniKorisnikRolaDTO.setId(id);
	registrovaniKorisnikRolaDTO.setVersion(version);
	registrovaniKorisnikRolaDTO.setStanje(stanje);

	if (this.registrovaniKorisnik != null)
	    registrovaniKorisnikRolaDTO.setRegistrovaniKorisnikDTO(this.registrovaniKorisnik.getDTOinsideDTO());

	if (this.rola != null)
	    registrovaniKorisnikRolaDTO.setRolaDTO(this.rola.getDTOinsideDTO());

	return registrovaniKorisnikRolaDTO;
    }

    @Override
    public RegistrovaniKorisnikRolaDTO getDTOinsideDTO() {
	RegistrovaniKorisnikRolaDTO registrovaniKorisnikRolaDTO = new RegistrovaniKorisnikRolaDTO();
	registrovaniKorisnikRolaDTO.setId(id);
	registrovaniKorisnikRolaDTO.setVersion(version);
	registrovaniKorisnikRolaDTO.setStanje(stanje);
	return registrovaniKorisnikRolaDTO;
    }

    @Override
    public void update(RegistrovaniKorisnikRola entitet) {
	this.setVersion(entitet.getVersion());
	this.setStanje(entitet.getStanje());

	this.setRegistrovaniKorisnik(entitet.getRegistrovaniKorisnik());
	this.setRola(entitet.getRola());

    }

}
