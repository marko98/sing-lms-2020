package lms.model;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.AutorNastavniMaterijalDTO;
import lms.enums.StanjeModela;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE autor_nastavni_materijal SET stanje = 'OBRISAN' WHERE autor_nastavni_materijal_id = ? AND version = ? ", check = ResultCheckStyle.COUNT)
@Table(name = "autor_nastavni_materijal")
@Transactional
public class AutorNastavniMaterijal implements Entitet<AutorNastavniMaterijal, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 1866803529560529261L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "autor_nastavni_materijal_id")
    private Long id;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "nastavni_materijal_id")
    private NastavniMaterijal nastavniMaterijal;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "autor_id")
    private Autor autor;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public NastavniMaterijal getNastavniMaterijal() {
	return this.nastavniMaterijal;
    }

    public void setNastavniMaterijal(NastavniMaterijal nastavniMaterijal) {
	this.nastavniMaterijal = nastavniMaterijal;
    }

    public Autor getAutor() {
	return this.autor;
    }

    public void setAutor(Autor autor) {
	this.autor = autor;
    }

    public AutorNastavniMaterijal() {
	super();
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((autor == null) ? 0 : autor.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((nastavniMaterijal == null) ? 0 : nastavniMaterijal.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	AutorNastavniMaterijal other = (AutorNastavniMaterijal) obj;
	if (autor == null) {
	    if (other.autor != null)
		return false;
	} else if (!autor.equals(other.autor))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (nastavniMaterijal == null) {
	    if (other.nastavniMaterijal != null)
		return false;
	} else if (!nastavniMaterijal.equals(other.nastavniMaterijal))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public AutorNastavniMaterijalDTO getDTO() {
	AutorNastavniMaterijalDTO autorNastavniMaterijalDTO = new AutorNastavniMaterijalDTO();
	autorNastavniMaterijalDTO.setId(id);
	autorNastavniMaterijalDTO.setStanje(stanje);
	autorNastavniMaterijalDTO.setVersion(version);

	if (this.autor != null)
	    autorNastavniMaterijalDTO.setAutorDTO(this.autor.getDTOinsideDTO());
	if (this.nastavniMaterijal != null)
	    autorNastavniMaterijalDTO.setNastavniMaterijalDTO(this.nastavniMaterijal.getDTOinsideDTO());

	return autorNastavniMaterijalDTO;
    }

    @Override
    public AutorNastavniMaterijalDTO getDTOinsideDTO() {
	AutorNastavniMaterijalDTO autorNastavniMaterijalDTO = new AutorNastavniMaterijalDTO();
	autorNastavniMaterijalDTO.setId(id);
	autorNastavniMaterijalDTO.setStanje(stanje);
	autorNastavniMaterijalDTO.setVersion(version);

	return autorNastavniMaterijalDTO;
    }

    @Override
    public void update(AutorNastavniMaterijal entitet) {
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());

	this.setAutor(entitet.getAutor());
	this.setNastavniMaterijal(entitet.getNastavniMaterijal());

    }

}