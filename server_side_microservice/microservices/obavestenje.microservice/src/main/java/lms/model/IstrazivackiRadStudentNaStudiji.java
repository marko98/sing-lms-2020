package lms.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.IstrazivackiRadStudentNaStudijiDTO;
import lms.enums.StanjeModela;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE istrazivacki_rad_student_na_studiji SET stanje = 'OBRISAN' WHERE istrazivacki_rad_student_na_studiji_id = ? AND version = ? ", check = ResultCheckStyle.COUNT)
@Table(name = "istrazivacki_rad_student_na_studiji")
@Transactional
public class IstrazivackiRadStudentNaStudiji implements Entitet<IstrazivackiRadStudentNaStudiji, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -4572971126881650746L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "istrazivacki_rad_student_na_studiji_id")
    private Long id;

    private LocalDateTime datum;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "istrazivacki_rad_id")
    private IstrazivackiRad istrazivackiRad;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "student_na_studiji_id")
    private StudentNaStudiji studentNaStudiji;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public LocalDateTime getDatum() {
	return this.datum;
    }

    public void setDatum(LocalDateTime datum) {
	this.datum = datum;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public IstrazivackiRad getIstrazivackiRad() {
	return this.istrazivackiRad;
    }

    public void setIstrazivackiRad(IstrazivackiRad istrazivackiRad) {
	this.istrazivackiRad = istrazivackiRad;
    }

    public StudentNaStudiji getStudentNaStudiji() {
	return this.studentNaStudiji;
    }

    public void setStudentNaStudiji(StudentNaStudiji studentNaStudiji) {
	this.studentNaStudiji = studentNaStudiji;
    }

    public IstrazivackiRadStudentNaStudiji() {
	super();
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((datum == null) ? 0 : datum.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((istrazivackiRad == null) ? 0 : istrazivackiRad.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((studentNaStudiji == null) ? 0 : studentNaStudiji.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	IstrazivackiRadStudentNaStudiji other = (IstrazivackiRadStudentNaStudiji) obj;
	if (datum == null) {
	    if (other.datum != null)
		return false;
	} else if (!datum.equals(other.datum))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (istrazivackiRad == null) {
	    if (other.istrazivackiRad != null)
		return false;
	} else if (!istrazivackiRad.equals(other.istrazivackiRad))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (studentNaStudiji == null) {
	    if (other.studentNaStudiji != null)
		return false;
	} else if (!studentNaStudiji.equals(other.studentNaStudiji))
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public IstrazivackiRadStudentNaStudijiDTO getDTO() {
	IstrazivackiRadStudentNaStudijiDTO istrazivackiRadStudentNaStudijiDTO = new IstrazivackiRadStudentNaStudijiDTO();
	istrazivackiRadStudentNaStudijiDTO.setId(id);
	istrazivackiRadStudentNaStudijiDTO.setDatum(datum);
	istrazivackiRadStudentNaStudijiDTO.setStanje(stanje);
	istrazivackiRadStudentNaStudijiDTO.setVersion(version);

	if (this.istrazivackiRad != null)
	    istrazivackiRadStudentNaStudijiDTO.setIstrazivackiRadDTO(this.istrazivackiRad.getDTOinsideDTO());
	if (this.studentNaStudiji != null)
	    istrazivackiRadStudentNaStudijiDTO.setStudentNaStudijiDTO(this.studentNaStudiji.getDTOinsideDTO());

	return istrazivackiRadStudentNaStudijiDTO;
    }

    @Override
    public IstrazivackiRadStudentNaStudijiDTO getDTOinsideDTO() {
	IstrazivackiRadStudentNaStudijiDTO istrazivackiRadStudentNaStudijiDTO = new IstrazivackiRadStudentNaStudijiDTO();
	istrazivackiRadStudentNaStudijiDTO.setId(id);
	istrazivackiRadStudentNaStudijiDTO.setDatum(datum);
	istrazivackiRadStudentNaStudijiDTO.setStanje(stanje);
	istrazivackiRadStudentNaStudijiDTO.setVersion(version);
	return istrazivackiRadStudentNaStudijiDTO;
    }

    @Override
    public void update(IstrazivackiRadStudentNaStudiji entitet) {
	this.setDatum(entitet.getDatum());
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());

	this.setIstrazivackiRad(entitet.getIstrazivackiRad());
	this.setStudentNaStudiji(entitet.getStudentNaStudiji());

    }

}