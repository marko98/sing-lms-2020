package lms.model;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.NaucnaOblastDTO;
import lms.dto.TitulaDTO;
import lms.enums.StanjeModela;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE naucna_oblast SET stanje = 'OBRISAN' WHERE naucna_oblast_id = ? AND version = ? ", check = ResultCheckStyle.COUNT)
@Table(name = "naucna_oblast")
@Transactional
public class NaucnaOblast implements Entitet<NaucnaOblast, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 8984880985275524400L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "naucna_oblast_id")
    private Long id;

    private String naziv;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.OneToMany(mappedBy = "naucnaOblast")
    private java.util.List<Titula> titule;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getNaziv() {
	return this.naziv;
    }

    public void setNaziv(String naziv) {
	this.naziv = naziv;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public java.util.List<Titula> getTitule() {
	return this.titule;
    }

    public void setTitule(java.util.List<Titula> titule) {
	this.titule = titule;
    }

    public NaucnaOblast() {
	super();
	this.titule = new java.util.ArrayList<>();
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((naziv == null) ? 0 : naziv.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((titule == null) ? 0 : titule.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	NaucnaOblast other = (NaucnaOblast) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (naziv == null) {
	    if (other.naziv != null)
		return false;
	} else if (!naziv.equals(other.naziv))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (titule == null) {
	    if (other.titule != null)
		return false;
	} else if (!titule.equals(other.titule))
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public NaucnaOblastDTO getDTO() {
	NaucnaOblastDTO naucnaOblastDTO = new NaucnaOblastDTO();
	naucnaOblastDTO.setId(id);
	naucnaOblastDTO.setNaziv(naziv);
	naucnaOblastDTO.setStanje(stanje);
	naucnaOblastDTO.setVersion(version);

	ArrayList<TitulaDTO> tituleDTOs = new ArrayList<TitulaDTO>();
	for (Titula titula : this.titule) {
	    tituleDTOs.add(titula.getDTOinsideDTO());
	}
	naucnaOblastDTO.setTitule(tituleDTOs);

	return naucnaOblastDTO;
    }

    @Override
    public NaucnaOblastDTO getDTOinsideDTO() {
	NaucnaOblastDTO naucnaOblastDTO = new NaucnaOblastDTO();
	naucnaOblastDTO.setId(id);
	naucnaOblastDTO.setNaziv(naziv);
	naucnaOblastDTO.setStanje(stanje);
	naucnaOblastDTO.setVersion(version);
	return naucnaOblastDTO;
    }

    @Override
    public void update(NaucnaOblast entitet) {
	this.setNaziv(entitet.getNaziv());
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());

    }
}