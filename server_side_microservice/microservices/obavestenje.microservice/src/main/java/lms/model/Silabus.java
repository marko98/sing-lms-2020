package lms.model;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.SilabusDTO;
import lms.enums.StanjeModela;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE silabus SET stanje = 'OBRISAN' WHERE silabus_id = ? AND version = ? ", check = ResultCheckStyle.COUNT)
@Table(name = "silabus")
@Transactional
public class Silabus implements Entitet<Silabus, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -3672899722202591122L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "silabus_id")
    private Long id;

    private String naslov;

    private String opis;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.OneToOne(mappedBy = "silabus")
    private Predmet predmet;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getNaslov() {
	return this.naslov;
    }

    public void setNaslov(String naslov) {
	this.naslov = naslov;
    }

    public String getOpis() {
	return this.opis;
    }

    public void setOpis(String opis) {
	this.opis = opis;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public Predmet getPredmet() {
	return this.predmet;
    }

    public void setPredmet(Predmet predmet) {
	this.predmet = predmet;
    }

    public Silabus() {
	super();
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((naslov == null) ? 0 : naslov.hashCode());
	result = prime * result + ((opis == null) ? 0 : opis.hashCode());
	result = prime * result + ((predmet == null) ? 0 : predmet.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Silabus other = (Silabus) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (naslov == null) {
	    if (other.naslov != null)
		return false;
	} else if (!naslov.equals(other.naslov))
	    return false;
	if (opis == null) {
	    if (other.opis != null)
		return false;
	} else if (!opis.equals(other.opis))
	    return false;
	if (predmet == null) {
	    if (other.predmet != null)
		return false;
	} else if (!predmet.equals(other.predmet))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public SilabusDTO getDTO() {
	SilabusDTO silabusDTO = new SilabusDTO();
	silabusDTO.setId(id);
	silabusDTO.setNaslov(naslov);
	silabusDTO.setOpis(opis);
	silabusDTO.setStanje(stanje);
	silabusDTO.setVersion(version);

	if (this.predmet != null)
	    silabusDTO.setPredmetDTO(this.predmet.getDTOinsideDTO());

	return silabusDTO;
    }

    @Override
    public SilabusDTO getDTOinsideDTO() {
	SilabusDTO silabusDTO = new SilabusDTO();
	silabusDTO.setId(id);
	silabusDTO.setNaslov(naslov);
	silabusDTO.setOpis(opis);
	silabusDTO.setStanje(stanje);
	silabusDTO.setVersion(version);
	return silabusDTO;
    }

    @Override
    public void update(Silabus entitet) {
	this.setNaslov(entitet.getNaslov());
	this.setOpis(entitet.getOpis());
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());

	this.setPredmet(entitet.getPredmet());

    }

}