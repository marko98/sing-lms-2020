package lms.obavestenje.microservice.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.Fajl;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class FajlRepository extends CrudRepository<Fajl, Long> {

}