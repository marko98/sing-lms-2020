package lms.obavestenje.microservice.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.Fajl;

@Repository
@Scope("singleton")
public interface FajlPASRepository extends PagingAndSortingRepository<Fajl, Long> {

}