package lms.obavestenje.microservice.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.GodinaStudijaObavestenje;

@Controller
@Scope("singleton")
@RequestMapping(path = "godina_studija_obavestenje")
@CrossOrigin(origins = "*")
public class GodinaStudijaObavestenjeController extends CrudController<GodinaStudijaObavestenje, Long> {

}