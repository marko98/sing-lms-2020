package lms.obavestenje.microservice.controller;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import lms.controller.CrudController;
import lms.dto.interfaces.DTO;
import lms.model.Ishod;
import lms.obavestenje.microservice.service.IshodService;

@Controller
@Scope("singleton")
@RequestMapping(path = "ishod")
@CrossOrigin(origins = "*")
public class IshodController extends CrudController<Ishod, Long> {

    @RequestMapping(path = "/realizacija_predmeta/{id}", method = RequestMethod.GET)
    public ResponseEntity<List<DTO<Long>>> findAllByRealizacijaPredmetaId(@PathVariable("id") Long id) {

	List<Ishod> list = StreamSupport
		.stream(((IshodService) this.service).findAllByRealizacijaPredmetaId(id).spliterator(), false)
		.collect(Collectors.toList());

	return new ResponseEntity<List<DTO<Long>>>(list.stream().map(t -> t.getDTO()).collect(Collectors.toList()),
		HttpStatus.OK);
    }

}