package lms.obavestenje.microservice.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.GodinaStudijaObavestenje;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class GodinaStudijaObavestenjeRepository extends CrudRepository<GodinaStudijaObavestenje, Long> {

}