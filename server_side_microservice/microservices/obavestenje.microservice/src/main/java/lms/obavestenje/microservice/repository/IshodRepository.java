package lms.obavestenje.microservice.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.Ishod;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class IshodRepository extends CrudRepository<Ishod, Long> {

}