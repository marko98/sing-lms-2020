package lms.obavestenje.microservice.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.Ishod;
import lms.obavestenje.microservice.repository.interfaces.IshodPASRepository;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class IshodService extends CrudService<Ishod, Long> {

    public Iterable<Ishod> findAllByRealizacijaPredmetaId(Long id) {
	return ((IshodPASRepository) this.repository.getRepository()).findAllByRealizacijaPredmetaId(id);
    }

}