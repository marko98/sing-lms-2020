package lms.obavestenje.microservice.repository.interfaces;

import java.util.Collection;

import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.Ishod;

@Repository
@Scope("singleton")
public interface IshodPASRepository extends PagingAndSortingRepository<Ishod, Long> {
    
    @Query("select i from Ishod i where i.realizacijaPredmeta.id = ?1")
    public Collection<Ishod> findAllByRealizacijaPredmetaId (Long id);

}