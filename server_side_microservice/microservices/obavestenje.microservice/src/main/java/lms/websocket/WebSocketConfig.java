package lms.websocket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

import lms.model.Fajl;
import lms.model.GodinaStudijaObavestenje;
import lms.model.Ishod;
import lms.model.Obavestenje;

@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

    @Autowired
    private SocketHandler<Fajl, Long> fajl;

    @Autowired
    private SocketHandler<GodinaStudijaObavestenje, Long> godinaStudijaObavestenje;

    @Autowired
    private SocketHandler<Ishod, Long> ishod;

    @Autowired
    private SocketHandler<Obavestenje, Long> obavestenje;

    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {

	registry.addHandler(fajl, "/fajl_ws").setAllowedOrigins("*");

	registry.addHandler(godinaStudijaObavestenje, "/godina_studija_obavestenje_ws").setAllowedOrigins("*");

	registry.addHandler(ishod, "/ishod_ws").setAllowedOrigins("*");

	registry.addHandler(obavestenje, "/obavestenje_ws").setAllowedOrigins("*");

    }

}
