package lms.websocket;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lms.model.Fajl;
import lms.model.GodinaStudijaObavestenje;
import lms.model.Ishod;
import lms.model.Obavestenje;

@Configuration
public class SocketHandlerConf {
    @Bean
    public SocketHandler<Fajl, Long> getFajlSocketHandler() {
	return new SocketHandler<Fajl, Long>("/fajl_ws");
    }

    @Bean
    public SocketHandler<GodinaStudijaObavestenje, Long> getGodinaStudijaObavestenjeSocketHandler() {
	return new SocketHandler<GodinaStudijaObavestenje, Long>("/godina_studija_obavestenje_ws");
    }

    @Bean
    public SocketHandler<Ishod, Long> getIshodSocketHandler() {
	return new SocketHandler<Ishod, Long>("/ishod_ws");
    }
    
    @Bean
    public SocketHandler<Obavestenje, Long> getObavestenjeSocketHandler() {
	return new SocketHandler<Obavestenje, Long>("/obavestenje_ws");
    }
}