package lms.websocket;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lms.model.interfaces.Entitet;

@Component
public class SocketHandler<T extends Entitet<T, ID>, ID extends Serializable> extends TextWebSocketHandler {
    @Autowired
    private ObjectMapper objectMapper;
    
    private String path;
    private List<WebSocketSession> sessions = new CopyOnWriteArrayList<WebSocketSession>();

    public SocketHandler() {
    }

    public SocketHandler(String path) {
	this.path = path;
    }

    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message)
	    throws InterruptedException, IOException {

	for (WebSocketSession webSocketSession : sessions) {
	    if (webSocketSession.isOpen())
		webSocketSession.sendMessage(new TextMessage(message.getPayload()));
	}
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
//	System.out.println(session.getRemoteAddress().toString());
//	System.out.println("konekcija");
	sessions.add(session);

	if (session.isOpen()) {
	    this.sendObjectAsMessage(WSMessageKeys.ON_CONNECTED, "hello from " + this.path);
	    System.out.println("broj sesija: " + this.sessions.size());
	}

    }

    public List<WebSocketSession> getSessions() {
	return sessions;
    }

    public String getPath() {
	return path;
    }

    public void sendObjectAsMessage(WSMessageKeys key, Object obj) {
	WSMessage wSMessage = new WSMessage();
	wSMessage.getMessage().putIfAbsent(key, obj);
//	ObjectMapper objectMapper = new ObjectMapper();
	this.sessions.forEach(session -> {
	    try {
		if (session.isOpen())
		    session.sendMessage(new TextMessage(this.objectMapper.writeValueAsString(wSMessage)));
	    } catch (JsonProcessingException e) {
		e.printStackTrace();
	    } catch (IOException e) {
		e.printStackTrace();
	    }
	});
    }
}
