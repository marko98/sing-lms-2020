package lms.dto;

import lms.dto.interfaces.DTO;
import lms.enums.StanjeModela;

public class PolaganjeDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -5987488713994289294L;

    private Long id;

    private Integer bodovi;

    private String napomena;

    private StanjeModela stanje;

    private Long version;

    private PohadjanjePredmetaDTO pohadjanjePredmetaDTO;

    private EvaluacijaZnanjaDTO evaluacijaZnanjaDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Integer getBodovi() {
	return this.bodovi;
    }

    public void setBodovi(Integer bodovi) {
	this.bodovi = bodovi;
    }

    public String getNapomena() {
	return this.napomena;
    }

    public void setNapomena(String napomena) {
	this.napomena = napomena;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public PohadjanjePredmetaDTO getPohadjanjePredmetaDTO() {
	return this.pohadjanjePredmetaDTO;
    }

    public void setPohadjanjePredmetaDTO(PohadjanjePredmetaDTO pohadjanjePredmetaDTO) {
	this.pohadjanjePredmetaDTO = pohadjanjePredmetaDTO;
    }

    public EvaluacijaZnanjaDTO getEvaluacijaZnanjaDTO() {
	return this.evaluacijaZnanjaDTO;
    }

    public void setEvaluacijaZnanjaDTO(EvaluacijaZnanjaDTO evaluacijaZnanjaDTO) {
	this.evaluacijaZnanjaDTO = evaluacijaZnanjaDTO;
    }

    public PolaganjeDTO() {
	super();
    }

}