package lms.dto;

import lms.dto.interfaces.DTO;
import lms.enums.StanjeModela;
import lms.interfaces.Identifikacija;

public class StudentskaSluzbaDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 8823344812857506562L;

    private Long id;

    private StanjeModela stanje;

    private Long version;

    private java.util.List<StudentskiSluzbenikDTO> studentskiSluzbeniciDTO;

    private OdeljenjeDTO odeljenjeDTO;

    private UniverzitetDTO univerzitetDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public java.util.List<StudentskiSluzbenikDTO> getStudentskiSluzbeniciDTO() {
	return this.studentskiSluzbeniciDTO;
    }

    public void setStudentskiSluzbeniciDTO(java.util.List<StudentskiSluzbenikDTO> studentskiSluzbeniciDTO) {
	this.studentskiSluzbeniciDTO = studentskiSluzbeniciDTO;
    }

    public OdeljenjeDTO getOdeljenjeDTO() {
	return this.odeljenjeDTO;
    }

    public void setOdeljenjeDTO(OdeljenjeDTO odeljenjeDTO) {
	this.odeljenjeDTO = odeljenjeDTO;
    }

    public UniverzitetDTO getUniverzitetDTO() {
	return this.univerzitetDTO;
    }

    public void setUniverzitetDTO(UniverzitetDTO univerzitetDTO) {
	this.univerzitetDTO = univerzitetDTO;
    }

    public StudentskaSluzbaDTO() {
	super();
	this.studentskiSluzbeniciDTO = new java.util.ArrayList<>();
    }

    public StudentskiSluzbenikDTO getStudentskiSluzbenikDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addStudentskiSluzbenikDTO(StudentskiSluzbenikDTO studentskiSluzbenikDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeStudentskiSluzbenikDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

}