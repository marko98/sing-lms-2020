package lms.dto;

import lms.dto.interfaces.DTO;
import lms.enums.NacinEvaluacije;
import lms.enums.StanjeModela;

public class EvaluacijaZnanjaNacinEvaluacijeDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -5174907388761687197L;

    private Long id;

    private StanjeModela stanje;

    private Long version;

    private NacinEvaluacije nacinEvaluacije;

    private EvaluacijaZnanjaDTO evaluacijaZnanjaDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public NacinEvaluacije getNacinEvaluacije() {
	return this.nacinEvaluacije;
    }

    public void setNacinEvaluacije(NacinEvaluacije nacinEvaluacije) {
	this.nacinEvaluacije = nacinEvaluacije;
    }

    public EvaluacijaZnanjaDTO getEvaluacijaZnanjaDTO() {
	return this.evaluacijaZnanjaDTO;
    }

    public void setEvaluacijaZnanjaDTO(EvaluacijaZnanjaDTO evaluacijaZnanjaDTO) {
	this.evaluacijaZnanjaDTO = evaluacijaZnanjaDTO;
    }

    public EvaluacijaZnanjaNacinEvaluacijeDTO() {
	super();
    }

}