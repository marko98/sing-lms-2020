package lms.security.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.RegistrovaniKorisnik;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class SecurityRepository extends CrudRepository<RegistrovaniKorisnik, Long> {

}