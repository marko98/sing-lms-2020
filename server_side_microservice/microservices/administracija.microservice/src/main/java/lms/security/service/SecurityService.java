package lms.security.service;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import lms.hbsf.HibernateSessionFactory;
import lms.model.RegistrovaniKorisnik;
import lms.model.RegistrovaniKorisnikRola;
import lms.service.CrudService;

@Service(value = "securityService")
@Scope("singleton")
public class SecurityService extends CrudService<RegistrovaniKorisnik, Long> implements UserDetailsService {
    @Autowired
    private HibernateSessionFactory hbsf;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String korisnickoIme) throws UsernameNotFoundException {

	Session session = this.hbsf.getNewSession();
	session.getTransaction().begin();

	Query<RegistrovaniKorisnik> query = session.createQuery(
		"select r from RegistrovaniKorisnik r where r.korisnickoIme = ?1", RegistrovaniKorisnik.class);
	query.setParameter(1, korisnickoIme);
	RegistrovaniKorisnik registrovaniKorisnik = query.getSingleResult();

	if (registrovaniKorisnik != null) {
	    /**
	     * buduci da u UserDetails interfejsu imamo username, password i listu
	     * GrantedAuthority-a(interfejs koji ima metodu getAuthority koja vraca String),
	     */
	    ArrayList<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
	    for (RegistrovaniKorisnikRola registrovaniKorinsikRola : registrovaniKorisnik
		    .getRegistrovaniKorisnikRole()) {
		/**
		 * kreiramo instancu klase SimpleGrantedAuthority koja implemetira
		 * GrantedAuthority interfejs i kao argument prima role tipa String
		 */
		grantedAuthorities.add(new SimpleGrantedAuthority(registrovaniKorinsikRola.getRola().getNaziv()));
	    }

	    /**
	     * kreiramo instancu klase User, koja implmentira interfejs UserDetails i kao
	     * argumente prima username, password i listu GrantedAuthority-a, koju smo
	     * prethodno kreirali
	     */

	    User user = new User(registrovaniKorisnik.getKorisnickoIme(), registrovaniKorisnik.getLozinka(),
		    grantedAuthorities);

	    session.getTransaction().commit();
	    session.close();
	    return user;
	}
	session.getTransaction().commit();
	session.close();

	return null;
    }

}
