package lms.administracija.microservice.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.Administrator;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class AdministratorRepository extends CrudRepository<Administrator, Long> {

}