package lms.administracija.microservice.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.StudentskaSluzba;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class StudentskaSluzbaRepository extends CrudRepository<StudentskaSluzba, Long> {

}