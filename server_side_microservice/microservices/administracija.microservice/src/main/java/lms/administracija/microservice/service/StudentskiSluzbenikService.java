package lms.administracija.microservice.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.administracija.microservice.repository.interfaces.StudentskiSluzbenikPASRepository;
import lms.model.StudentskiSluzbenik;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class StudentskiSluzbenikService extends CrudService<StudentskiSluzbenik, Long> {
    public StudentskiSluzbenik findOneByRegistrovaniKorisnikId(Long id) {
	return ((StudentskiSluzbenikPASRepository) this.repository.getRepository()).findOneByRegistrovaniKorisnikId(id);
    }
}