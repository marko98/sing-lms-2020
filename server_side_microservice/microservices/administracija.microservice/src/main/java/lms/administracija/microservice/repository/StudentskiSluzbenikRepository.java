package lms.administracija.microservice.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.StudentskiSluzbenik;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class StudentskiSluzbenikRepository extends CrudRepository<StudentskiSluzbenik, Long> {

}