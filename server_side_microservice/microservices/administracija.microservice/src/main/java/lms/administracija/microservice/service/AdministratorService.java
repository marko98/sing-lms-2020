package lms.administracija.microservice.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.administracija.microservice.repository.interfaces.AdministratorPASRepository;
import lms.model.Administrator;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class AdministratorService extends CrudService<Administrator, Long> {
    public Administrator findOneByRegistrovaniKorisnikId(Long id) {
	return ((AdministratorPASRepository) this.repository.getRepository()).findOneByRegistrovaniKorisnikId(id);
    }
}