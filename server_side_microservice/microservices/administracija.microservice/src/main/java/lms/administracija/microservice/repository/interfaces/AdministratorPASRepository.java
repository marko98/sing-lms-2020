package lms.administracija.microservice.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.Administrator;

@Repository
@Scope("singleton")
public interface AdministratorPASRepository extends PagingAndSortingRepository<Administrator, Long> {
    @Query("select a from Administrator a where a.registrovaniKorisnik.id = ?1")
    public Administrator findOneByRegistrovaniKorisnikId(Long id);
}