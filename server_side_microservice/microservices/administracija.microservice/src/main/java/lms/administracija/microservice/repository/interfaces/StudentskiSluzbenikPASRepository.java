package lms.administracija.microservice.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.StudentskiSluzbenik;

@Repository
@Scope("singleton")
public interface StudentskiSluzbenikPASRepository extends PagingAndSortingRepository<StudentskiSluzbenik, Long> {
    @Query("select s from StudentskiSluzbenik s where s.registrovaniKorisnik.id = ?1")
    public StudentskiSluzbenik findOneByRegistrovaniKorisnikId(Long id);
}