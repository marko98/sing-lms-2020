package lms.model;

import java.time.LocalDateTime;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.FajlDTO;
import lms.dto.GodinaStudijaObavestenjeDTO;
import lms.dto.ObavestenjeDTO;
import lms.enums.StanjeModela;
import lms.interfaces.Identifikacija;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE obavestenje SET stanje = 'OBRISAN' WHERE obavestenje_id = ? AND version = ? ", check = ResultCheckStyle.COUNT)
@Table(name = "obavestenje")
@Transactional
public class Obavestenje implements Entitet<Obavestenje, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 7520244338585777246L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "obavestenje_id")
    private Long id;

    private String naslov;

    private LocalDateTime datum;

    private String sadrzaj;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.OneToMany(mappedBy = "obavestenje")
    private java.util.List<GodinaStudijaObavestenje> godineStudijaObavestenje;

    @javax.persistence.OneToMany(mappedBy = "obavestenje")
    private java.util.List<Fajl> fajlovi;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getNaslov() {
	return this.naslov;
    }

    public void setNaslov(String naslov) {
	this.naslov = naslov;
    }

    public LocalDateTime getDatum() {
	return this.datum;
    }

    public void setDatum(LocalDateTime datum) {
	this.datum = datum;
    }

    public String getSadrzaj() {
	return this.sadrzaj;
    }

    public void setSadrzaj(String sadrzaj) {
	this.sadrzaj = sadrzaj;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public java.util.List<GodinaStudijaObavestenje> getGodineStudijaObavestenje() {
	return this.godineStudijaObavestenje;
    }

    public void setGodineStudijaObavestenje(java.util.List<GodinaStudijaObavestenje> godineStudijaObavestenje) {
	this.godineStudijaObavestenje = godineStudijaObavestenje;
    }

    public java.util.List<Fajl> getFajlovi() {
	return this.fajlovi;
    }

    public void setFajlovi(java.util.List<Fajl> fajlovi) {
	this.fajlovi = fajlovi;
    }

    public Obavestenje() {
	super();
	this.godineStudijaObavestenje = new java.util.ArrayList<>();
	this.fajlovi = new java.util.ArrayList<>();
    }

    public GodinaStudijaObavestenje getGodinaStudijaObavestenje(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addGodinaStudijaObavestenje(GodinaStudijaObavestenje godinaStudijaObavestenje) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeGodinaStudijaObavestenje(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public Fajl getFajl(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addFajl(Fajl fajl) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeFajl(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((datum == null) ? 0 : datum.hashCode());
	result = prime * result + ((fajlovi == null) ? 0 : fajlovi.hashCode());
	result = prime * result + ((godineStudijaObavestenje == null) ? 0 : godineStudijaObavestenje.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((naslov == null) ? 0 : naslov.hashCode());
	result = prime * result + ((sadrzaj == null) ? 0 : sadrzaj.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Obavestenje other = (Obavestenje) obj;
	if (datum == null) {
	    if (other.datum != null)
		return false;
	} else if (!datum.equals(other.datum))
	    return false;
	if (fajlovi == null) {
	    if (other.fajlovi != null)
		return false;
	} else if (!fajlovi.equals(other.fajlovi))
	    return false;
	if (godineStudijaObavestenje == null) {
	    if (other.godineStudijaObavestenje != null)
		return false;
	} else if (!godineStudijaObavestenje.equals(other.godineStudijaObavestenje))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (naslov == null) {
	    if (other.naslov != null)
		return false;
	} else if (!naslov.equals(other.naslov))
	    return false;
	if (sadrzaj == null) {
	    if (other.sadrzaj != null)
		return false;
	} else if (!sadrzaj.equals(other.sadrzaj))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public ObavestenjeDTO getDTO() {
	ObavestenjeDTO obavestenjeDTO = new ObavestenjeDTO();
	obavestenjeDTO.setId(id);
	obavestenjeDTO.setDatum(datum);
	obavestenjeDTO.setNaslov(naslov);
	obavestenjeDTO.setSadrzaj(sadrzaj);
	obavestenjeDTO.setStanje(stanje);
	obavestenjeDTO.setVersion(version);

	ArrayList<FajlDTO> fajloviDTOs = new ArrayList<FajlDTO>();
	for (Fajl fajl : this.fajlovi) {
	    fajloviDTOs.add(fajl.getDTOinsideDTO());
	}
	obavestenjeDTO.setFajloviDTO(fajloviDTOs);

	ArrayList<GodinaStudijaObavestenjeDTO> godineStudijaObavestenjeDTOs = new ArrayList<GodinaStudijaObavestenjeDTO>();
	for (GodinaStudijaObavestenje godinaStudijaObavestenje : this.godineStudijaObavestenje) {
	    godineStudijaObavestenjeDTOs.add(godinaStudijaObavestenje.getDTOinsideDTO());
	}
	obavestenjeDTO.setGodineStudijaObavestenjeDTO(godineStudijaObavestenjeDTOs);

	return obavestenjeDTO;
    }

    @Override
    public ObavestenjeDTO getDTOinsideDTO() {
	ObavestenjeDTO obavestenjeDTO = new ObavestenjeDTO();
	obavestenjeDTO.setId(id);
	obavestenjeDTO.setDatum(datum);
	obavestenjeDTO.setNaslov(naslov);
	obavestenjeDTO.setSadrzaj(sadrzaj);
	obavestenjeDTO.setStanje(stanje);
	obavestenjeDTO.setVersion(version);
	return obavestenjeDTO;
    }

    @Override
    public void update(Obavestenje entitet) {
	this.setDatum(entitet.getDatum());
	this.setNaslov(entitet.getNaslov());
	this.setSadrzaj(entitet.getSadrzaj());
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());
    }

}