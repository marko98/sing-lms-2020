package lms.model;

import java.time.LocalDateTime;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.DogadjajDTO;
import lms.dto.DogadjajKalendarDTO;
import lms.enums.StanjeModela;
import lms.interfaces.Identifikacija;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE dogadjaj SET stanje = 'OBRISAN' WHERE dogadjaj_id = ? AND version = ? ", check = ResultCheckStyle.COUNT)
@Table(name = "dogadjaj")
@Transactional
public class Dogadjaj implements Entitet<Dogadjaj, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -2046146290427096838L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "dogadjaj_id")
    private Long id;

    private LocalDateTime pocetniDatum;

    private LocalDateTime krajnjiDatum;

    private String opis;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.OneToMany(mappedBy = "dogadjaj")
    private java.util.List<DogadjajKalendar> dogadjajKalendari;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public LocalDateTime getPocetniDatum() {
	return this.pocetniDatum;
    }

    public void setPocetniDatum(LocalDateTime pocetniDatum) {
	this.pocetniDatum = pocetniDatum;
    }

    public LocalDateTime getKrajnjiDatum() {
	return this.krajnjiDatum;
    }

    public void setKrajnjiDatum(LocalDateTime krajnjiDatum) {
	this.krajnjiDatum = krajnjiDatum;
    }

    public String getOpis() {
	return this.opis;
    }

    public void setOpis(String opis) {
	this.opis = opis;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public java.util.List<DogadjajKalendar> getDogadjajKalendari() {
	return this.dogadjajKalendari;
    }

    public void setDogadjajKalendari(java.util.List<DogadjajKalendar> dogadjajKalendari) {
	this.dogadjajKalendari = dogadjajKalendari;
    }

    public Dogadjaj() {
	super();
	this.dogadjajKalendari = new java.util.ArrayList<>();
    }

    public DogadjajKalendar getDogadjajKalendar(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addDogadjajKalendar(DogadjajKalendar dogadjajKalendar) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeDogadjajKalendar(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((dogadjajKalendari == null) ? 0 : dogadjajKalendari.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((krajnjiDatum == null) ? 0 : krajnjiDatum.hashCode());
	result = prime * result + ((opis == null) ? 0 : opis.hashCode());
	result = prime * result + ((pocetniDatum == null) ? 0 : pocetniDatum.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Dogadjaj other = (Dogadjaj) obj;
	if (dogadjajKalendari == null) {
	    if (other.dogadjajKalendari != null)
		return false;
	} else if (!dogadjajKalendari.equals(other.dogadjajKalendari))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (krajnjiDatum == null) {
	    if (other.krajnjiDatum != null)
		return false;
	} else if (!krajnjiDatum.equals(other.krajnjiDatum))
	    return false;
	if (opis == null) {
	    if (other.opis != null)
		return false;
	} else if (!opis.equals(other.opis))
	    return false;
	if (pocetniDatum == null) {
	    if (other.pocetniDatum != null)
		return false;
	} else if (!pocetniDatum.equals(other.pocetniDatum))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public DogadjajDTO getDTO() {
	DogadjajDTO dogadjajDTO = new DogadjajDTO();
	dogadjajDTO.setId(id);
	dogadjajDTO.setKrajnjiDatum(krajnjiDatum);
	dogadjajDTO.setOpis(opis);
	dogadjajDTO.setPocetniDatum(pocetniDatum);
	dogadjajDTO.setStanje(stanje);
	dogadjajDTO.setVersion(version);

	ArrayList<DogadjajKalendarDTO> dogadjajKalendarDTOs = new ArrayList<DogadjajKalendarDTO>();
	for (DogadjajKalendar dogadjajKalendar : this.dogadjajKalendari) {
	    dogadjajKalendarDTOs.add(dogadjajKalendar.getDTOinsideDTO());
	}
	dogadjajDTO.setDogadjajKalendariDTO(dogadjajKalendarDTOs);

	return dogadjajDTO;
    }

    @Override
    public DogadjajDTO getDTOinsideDTO() {
	DogadjajDTO dogadjajDTO = new DogadjajDTO();
	dogadjajDTO.setId(id);
	dogadjajDTO.setKrajnjiDatum(krajnjiDatum);
	dogadjajDTO.setOpis(opis);
	dogadjajDTO.setPocetniDatum(pocetniDatum);
	dogadjajDTO.setStanje(stanje);
	dogadjajDTO.setVersion(version);

	return dogadjajDTO;
    }

    @Override
    public void update(Dogadjaj entitet) {
	this.setKrajnjiDatum(entitet.getKrajnjiDatum());
	this.setOpis(entitet.getOpis());
	this.setPocetniDatum(entitet.getPocetniDatum());
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());
    }

}