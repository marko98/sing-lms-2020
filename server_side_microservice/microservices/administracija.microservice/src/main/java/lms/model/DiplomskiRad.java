package lms.model;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.DiplomskiRadDTO;
import lms.dto.NastavnikDiplomskiRadDTO;
import lms.enums.StanjeModela;
import lms.interfaces.Identifikacija;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE diplomski_rad SET stanje = 'OBRISAN' WHERE diplomski_rad_id = ? AND version = ? ", check = ResultCheckStyle.COUNT)
@Table(name = "diplomski_rad")
@Transactional
public class DiplomskiRad implements Entitet<DiplomskiRad, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -4808668336309010043L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "diplomski_rad_id")
    private Long id;

    private String tema;

    private Integer ocena;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "nastavnik_id")
    private Nastavnik mentor;

    @javax.persistence.OneToMany(mappedBy = "diplomskiRad")
    private java.util.List<NastavnikDiplomskiRad> nastavniciDiplomskiRad;

    @javax.persistence.OneToOne(mappedBy = "diplomskiRad")
    private StudentNaStudiji studentNaStudiji;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getTema() {
	return this.tema;
    }

    public void setTema(String tema) {
	this.tema = tema;
    }

    public Integer getOcena() {
	return this.ocena;
    }

    public void setOcena(Integer ocena) {
	this.ocena = ocena;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public Nastavnik getMentor() {
	return this.mentor;
    }

    public void setMentor(Nastavnik mentor) {
	this.mentor = mentor;
    }

    public java.util.List<NastavnikDiplomskiRad> getNastavniciDiplomskiRad() {
	return this.nastavniciDiplomskiRad;
    }

    public void setNastavniciDiplomskiRad(java.util.List<NastavnikDiplomskiRad> nastavniciDiplomskiRad) {
	this.nastavniciDiplomskiRad = nastavniciDiplomskiRad;
    }

    public StudentNaStudiji getStudentNaStudiji() {
	return this.studentNaStudiji;
    }

    public void setStudentNaStudiji(StudentNaStudiji studentNaStudiji) {
	this.studentNaStudiji = studentNaStudiji;
    }

    public DiplomskiRad() {
	super();
	this.nastavniciDiplomskiRad = new java.util.ArrayList<>();
    }

    public NastavnikDiplomskiRad getNastavnikDiplomskiRad(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addNastavnikDiplomskiRad(NastavnikDiplomskiRad nastavnikDiplomskiRad) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeNastavnikDiplomskiRad(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((mentor == null) ? 0 : mentor.hashCode());
	result = prime * result + ((nastavniciDiplomskiRad == null) ? 0 : nastavniciDiplomskiRad.hashCode());
	result = prime * result + ((ocena == null) ? 0 : ocena.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((studentNaStudiji == null) ? 0 : studentNaStudiji.hashCode());
	result = prime * result + ((tema == null) ? 0 : tema.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	DiplomskiRad other = (DiplomskiRad) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (mentor == null) {
	    if (other.mentor != null)
		return false;
	} else if (!mentor.equals(other.mentor))
	    return false;
	if (nastavniciDiplomskiRad == null) {
	    if (other.nastavniciDiplomskiRad != null)
		return false;
	} else if (!nastavniciDiplomskiRad.equals(other.nastavniciDiplomskiRad))
	    return false;
	if (ocena == null) {
	    if (other.ocena != null)
		return false;
	} else if (!ocena.equals(other.ocena))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (studentNaStudiji == null) {
	    if (other.studentNaStudiji != null)
		return false;
	} else if (!studentNaStudiji.equals(other.studentNaStudiji))
	    return false;
	if (tema == null) {
	    if (other.tema != null)
		return false;
	} else if (!tema.equals(other.tema))
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public DiplomskiRadDTO getDTO() {
	DiplomskiRadDTO diplomskiRadDTO = new DiplomskiRadDTO();
	diplomskiRadDTO.setId(id);
	diplomskiRadDTO.setOcena(ocena);
	diplomskiRadDTO.setStanje(stanje);
	diplomskiRadDTO.setTema(tema);
	diplomskiRadDTO.setVersion(version);

	if (this.mentor != null)
	    diplomskiRadDTO.setMentorDTO(this.mentor.getDTOinsideDTO());
	if (this.studentNaStudiji != null)
	    diplomskiRadDTO.setStudentNaStudijiDTO(this.studentNaStudiji.getDTOinsideDTO());

	ArrayList<NastavnikDiplomskiRadDTO> nastavnikDiplomskiRadDTOs = new ArrayList<NastavnikDiplomskiRadDTO>();
	for (NastavnikDiplomskiRad nastavnikDiplomskiRad : this.nastavniciDiplomskiRad) {
	    nastavnikDiplomskiRadDTOs.add(nastavnikDiplomskiRad.getDTOinsideDTO());
	}
	diplomskiRadDTO.setNastavniciDiplomskiRadDTO(nastavnikDiplomskiRadDTOs);

	return diplomskiRadDTO;
    }

    @Override
    public DiplomskiRadDTO getDTOinsideDTO() {
	DiplomskiRadDTO diplomskiRadDTO = new DiplomskiRadDTO();
	diplomskiRadDTO.setId(id);
	diplomskiRadDTO.setOcena(ocena);
	diplomskiRadDTO.setStanje(stanje);
	diplomskiRadDTO.setTema(tema);
	diplomskiRadDTO.setVersion(version);

	return diplomskiRadDTO;
    }

    @Override
    public void update(DiplomskiRad entitet) {
	this.setOcena(entitet.getOcena());
	this.setStanje(entitet.getStanje());
	this.setTema(entitet.getTema());
	this.setVersion(entitet.getVersion());

	this.setMentor(entitet.getMentor());
	this.setStudentNaStudiji(entitet.getStudentNaStudiji());

    }

}