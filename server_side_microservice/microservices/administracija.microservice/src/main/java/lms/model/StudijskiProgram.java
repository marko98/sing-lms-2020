package lms.model;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.GodinaStudijaDTO;
import lms.dto.StudijskiProgramDTO;
import lms.enums.StanjeModela;
import lms.interfaces.Identifikacija;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE studijski_program SET stanje = 'OBRISAN' WHERE studijski_program_id = ? AND version = ? ", check = ResultCheckStyle.COUNT)
@Table(name = "studijski_program")
@Transactional
public class StudijskiProgram implements Entitet<StudijskiProgram, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 2229254794367655386L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "studijski_program_id")
    private Long id;

    private String naziv;

    private String opis;

    private Integer brojGodinaStudija;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "fakultet_id")
    private Fakultet fakultet;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "nastavnik_id")
    private Nastavnik rukovodilac;

    @javax.persistence.OneToMany(mappedBy = "studijskiProgram")
    private java.util.List<GodinaStudija> godineStudija;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getNaziv() {
	return this.naziv;
    }

    public void setNaziv(String naziv) {
	this.naziv = naziv;
    }

    public Integer getBrojGodinaStudija() {
	return this.brojGodinaStudija;
    }

    public void setBrojGodinaStudija(Integer brojGodinaStudija) {
	this.brojGodinaStudija = brojGodinaStudija;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public Fakultet getFakultet() {
	return this.fakultet;
    }

    public void setFakultet(Fakultet fakultet) {
	this.fakultet = fakultet;
    }

    public Nastavnik getRukovodilac() {
	return this.rukovodilac;
    }

    public void setRukovodilac(Nastavnik rukovodilac) {
	this.rukovodilac = rukovodilac;
    }

    public java.util.List<GodinaStudija> getGodineStudija() {
	return this.godineStudija;
    }

    public void setGodineStudija(java.util.List<GodinaStudija> godineStudija) {
	this.godineStudija = godineStudija;
    }

    public StudijskiProgram() {
	super();
	this.godineStudija = new java.util.ArrayList<>();
    }

    public GodinaStudija getGodinaStudija(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addGodinaStudija(GodinaStudija godinaStudija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeGodinaStudija(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public String getOpis() {
	return opis;
    }

    public void setOpis(String opis) {
	this.opis = opis;
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((brojGodinaStudija == null) ? 0 : brojGodinaStudija.hashCode());
	result = prime * result + ((fakultet == null) ? 0 : fakultet.hashCode());
	result = prime * result + ((godineStudija == null) ? 0 : godineStudija.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((naziv == null) ? 0 : naziv.hashCode());
	result = prime * result + ((opis == null) ? 0 : opis.hashCode());
	result = prime * result + ((rukovodilac == null) ? 0 : rukovodilac.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	StudijskiProgram other = (StudijskiProgram) obj;
	if (brojGodinaStudija == null) {
	    if (other.brojGodinaStudija != null)
		return false;
	} else if (!brojGodinaStudija.equals(other.brojGodinaStudija))
	    return false;
	if (fakultet == null) {
	    if (other.fakultet != null)
		return false;
	} else if (!fakultet.equals(other.fakultet))
	    return false;
	if (godineStudija == null) {
	    if (other.godineStudija != null)
		return false;
	} else if (!godineStudija.equals(other.godineStudija))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (naziv == null) {
	    if (other.naziv != null)
		return false;
	} else if (!naziv.equals(other.naziv))
	    return false;
	if (opis == null) {
	    if (other.opis != null)
		return false;
	} else if (!opis.equals(other.opis))
	    return false;
	if (rukovodilac == null) {
	    if (other.rukovodilac != null)
		return false;
	} else if (!rukovodilac.equals(other.rukovodilac))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public StudijskiProgramDTO getDTO() {
	StudijskiProgramDTO studijskiProgramDTO = new StudijskiProgramDTO();
	studijskiProgramDTO.setId(id);
	studijskiProgramDTO.setBrojGodinaStudija(brojGodinaStudija);
	studijskiProgramDTO.setNaziv(naziv);
	studijskiProgramDTO.setStanje(stanje);
	studijskiProgramDTO.setVersion(version);
	studijskiProgramDTO.setOpis(opis);

	if (this.fakultet != null)
	    studijskiProgramDTO.setFakultetDTO(this.fakultet.getDTOinsideDTO());
	if (this.rukovodilac != null)
	    studijskiProgramDTO.setRukovodilacDTO(this.rukovodilac.getDTOinsideDTO());

	ArrayList<GodinaStudijaDTO> godineStudijaDTOs = new ArrayList<GodinaStudijaDTO>();
	for (GodinaStudija godinaStudija : this.godineStudija) {
	    godineStudijaDTOs.add(godinaStudija.getDTOinsideDTO());
	}
	studijskiProgramDTO.setGodineStudijaDTO(godineStudijaDTOs);

	return studijskiProgramDTO;
    }

    @Override
    public StudijskiProgramDTO getDTOinsideDTO() {
	StudijskiProgramDTO studijskiProgramDTO = new StudijskiProgramDTO();
	studijskiProgramDTO.setId(id);
	studijskiProgramDTO.setBrojGodinaStudija(brojGodinaStudija);
	studijskiProgramDTO.setNaziv(naziv);
	studijskiProgramDTO.setStanje(stanje);
	studijskiProgramDTO.setVersion(version);
	studijskiProgramDTO.setOpis(opis);
	return studijskiProgramDTO;
    }

    @Override
    public void update(StudijskiProgram entitet) {
	this.setBrojGodinaStudija(entitet.getBrojGodinaStudija());
	this.setNaziv(entitet.getNaziv());
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());
	this.setOpis(entitet.getOpis());

	this.setFakultet(entitet.getFakultet());
	this.setRukovodilac(entitet.getRukovodilac());

    }

}