package lms.dto;

import lms.dto.interfaces.DTO;
import lms.enums.StanjeModela;
import lms.enums.TipNastave;

public class TerminNastaveDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -1305731531150600900L;

    private Long id;

    private Integer trajanje;

    private StanjeModela stanje;

    private Long version;

    private TipNastave tipNastave;

    private ProstorijaDTO prostorijaDTO;

    private NastavnikDTO nastavnikDTO;

    private RealizacijaPredmetaDTO realizacijaPredmetaDTO;

    private VremeOdrzavanjaUNedeljiDTO vremeOdrzavanjaUNedeljiDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Integer getTrajanje() {
	return this.trajanje;
    }

    public void setTrajanje(Integer trajanje) {
	this.trajanje = trajanje;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public TipNastave getTipNastave() {
	return this.tipNastave;
    }

    public void setTipNastave(TipNastave tipNastave) {
	this.tipNastave = tipNastave;
    }

    public ProstorijaDTO getProstorijaDTO() {
	return this.prostorijaDTO;
    }

    public void setProstorijaDTO(ProstorijaDTO prostorijaDTO) {
	this.prostorijaDTO = prostorijaDTO;
    }

    public NastavnikDTO getNastavnikDTO() {
	return this.nastavnikDTO;
    }

    public void setNastavnikDTO(NastavnikDTO nastavnikDTO) {
	this.nastavnikDTO = nastavnikDTO;
    }

    public RealizacijaPredmetaDTO getRealizacijaPredmetaDTO() {
	return this.realizacijaPredmetaDTO;
    }

    public void setRealizacijaPredmetaDTO(RealizacijaPredmetaDTO realizacijaPredmetaDTO) {
	this.realizacijaPredmetaDTO = realizacijaPredmetaDTO;
    }

    public VremeOdrzavanjaUNedeljiDTO getVremeOdrzavanjaUNedeljiDTO() {
	return this.vremeOdrzavanjaUNedeljiDTO;
    }

    public void setVremeOdrzavanjaUNedeljiDTO(VremeOdrzavanjaUNedeljiDTO vremeOdrzavanjaUNedeljiDTO) {
	this.vremeOdrzavanjaUNedeljiDTO = vremeOdrzavanjaUNedeljiDTO;
    }

    public TerminNastaveDTO() {
	super();
    }

}