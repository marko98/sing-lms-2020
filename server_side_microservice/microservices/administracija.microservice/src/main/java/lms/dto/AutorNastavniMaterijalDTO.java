package lms.dto;

import lms.dto.interfaces.DTO;
import lms.enums.StanjeModela;

public class AutorNastavniMaterijalDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 1530491322303738643L;

    private Long id;

    private StanjeModela stanje;

    private Long version;

    private AutorDTO autorDTO;

    private NastavniMaterijalDTO nastavniMaterijalDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public AutorDTO getAutorDTO() {
	return this.autorDTO;
    }

    public void setAutorDTO(AutorDTO autorDTO) {
	this.autorDTO = autorDTO;
    }

    public NastavniMaterijalDTO getNastavniMaterijalDTO() {
	return this.nastavniMaterijalDTO;
    }

    public void setNastavniMaterijalDTO(NastavniMaterijalDTO nastavniMaterijalDTO) {
	this.nastavniMaterijalDTO = nastavniMaterijalDTO;
    }

    public AutorNastavniMaterijalDTO() {
	super();
    }

}