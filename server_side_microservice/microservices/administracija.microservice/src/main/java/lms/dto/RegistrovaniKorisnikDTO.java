package lms.dto;

import java.time.LocalDateTime;

import lms.dto.interfaces.DTO;
import lms.enums.StanjeModela;

public class RegistrovaniKorisnikDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 9178451261657788868L;

    private Long id;

    private String korisnickoIme;

    private String lozinka;

    private String email;

    private LocalDateTime datumRodjenja;

    private String jmbg;

    private String ime;

    private String prezime;

    private StanjeModela stanje;

    private Long version;

    private java.util.List<AdresaDTO> adreseDTO;

    private NastavnikDTO nastavnikDTO;

    private StudentskiSluzbenikDTO studentskiSluzbenikDTO;

    private AdministratorDTO administratorDTO;

    private StudentDTO studentDTO;

    private java.util.List<RegistrovaniKorisnikRolaDTO> registrovaniKorisnikRoleDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getKorisnickoIme() {
	return this.korisnickoIme;
    }

    public void setKorisnickoIme(String korisnickoIme) {
	this.korisnickoIme = korisnickoIme;
    }

    public String getLozinka() {
	return this.lozinka;
    }

    public void setLozinka(String lozinka) {
	this.lozinka = lozinka;
    }

    public String getEmail() {
	return this.email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public LocalDateTime getDatumRodjenja() {
	return this.datumRodjenja;
    }

    public void setDatumRodjenja(LocalDateTime datumRodjenja) {
	this.datumRodjenja = datumRodjenja;
    }

    public String getJmbg() {
	return this.jmbg;
    }

    public void setJmbg(String jmbg) {
	this.jmbg = jmbg;
    }

    public String getIme() {
	return this.ime;
    }

    public void setIme(String ime) {
	this.ime = ime;
    }

    public String getPrezime() {
	return this.prezime;
    }

    public void setPrezime(String prezime) {
	this.prezime = prezime;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public java.util.List<AdresaDTO> getAdreseDTO() {
	return this.adreseDTO;
    }

    public void setAdreseDTO(java.util.List<AdresaDTO> adreseDTO) {
	this.adreseDTO = adreseDTO;
    }

    public NastavnikDTO getNastavnikDTO() {
	return this.nastavnikDTO;
    }

    public void setNastavnikDTO(NastavnikDTO nastavnikDTO) {
	this.nastavnikDTO = nastavnikDTO;
    }

    public StudentskiSluzbenikDTO getStudentskiSluzbenikDTO() {
	return this.studentskiSluzbenikDTO;
    }

    public void setStudentskiSluzbenikDTO(StudentskiSluzbenikDTO studentskiSluzbenikDTO) {
	this.studentskiSluzbenikDTO = studentskiSluzbenikDTO;
    }

    public AdministratorDTO getAdministratorDTO() {
	return this.administratorDTO;
    }

    public void setAdministratorDTO(AdministratorDTO administratorDTO) {
	this.administratorDTO = administratorDTO;
    }

    public StudentDTO getStudentDTO() {
	return this.studentDTO;
    }

    public void setStudentDTO(StudentDTO studentDTO) {
	this.studentDTO = studentDTO;
    }

    public java.util.List<RegistrovaniKorisnikRolaDTO> getRegistrovaniKorisnikRoleDTO() {
	return registrovaniKorisnikRoleDTO;
    }

    public void setRegistrovaniKorisnikRoleDTO(
	    java.util.List<RegistrovaniKorisnikRolaDTO> registrovaniKorisnikRoleDTO) {
	this.registrovaniKorisnikRoleDTO = registrovaniKorisnikRoleDTO;
    }

    public RegistrovaniKorisnikDTO() {
	super();
	this.adreseDTO = new java.util.ArrayList<>();
	this.registrovaniKorisnikRoleDTO = new java.util.ArrayList<>();
    }

}