package lms.dto;

import lms.dto.interfaces.DTO;
import lms.enums.StanjeModela;

public class KontaktDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 5545946141973977682L;

    private Long id;

    private String mobilni;

    private String fiksni;

    private Long version;

    private StanjeModela stanje;

    private FakultetDTO fakultetDTO;

    private UniverzitetDTO univerzitetDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getMobilni() {
	return this.mobilni;
    }

    public void setMobilni(String mobilni) {
	this.mobilni = mobilni;
    }

    public String getFiksni() {
	return this.fiksni;
    }

    public void setFiksni(String fiksni) {
	this.fiksni = fiksni;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public FakultetDTO getFakultetDTO() {
	return this.fakultetDTO;
    }

    public void setFakultetDTO(FakultetDTO fakultetDTO) {
	this.fakultetDTO = fakultetDTO;
    }

    public UniverzitetDTO getUniverzitetDTO() {
	return this.univerzitetDTO;
    }

    public void setUniverzitetDTO(UniverzitetDTO univerzitetDTO) {
	this.univerzitetDTO = univerzitetDTO;
    }

    public KontaktDTO() {
	super();
    }

}