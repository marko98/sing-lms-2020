package lms.websocket;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.fasterxml.jackson.core.JsonProcessingException;

public class DataSocketHandler extends TextWebSocketHandler {
    private String path;
    private List<WebSocketSession> sessions = new CopyOnWriteArrayList<WebSocketSession>();

    public DataSocketHandler() {
    }

    public DataSocketHandler(String path) {
	this.path = path;
    }

    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message)
	    throws InterruptedException, IOException {

//	System.out.println(message.getPayload());

	for (WebSocketSession webSocketSession : sessions) {
	    if (!session.getId().equals(webSocketSession.getId()) && webSocketSession.isOpen()) {
		synchronized (webSocketSession) {
		    webSocketSession.sendMessage(new TextMessage(message.getPayload()));
		}
	    }
	}
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
//	System.out.println(session.getRemoteAddress().toString());
	System.out.println("konekcija");
	sessions.add(session);

	try {
	    if (session.isOpen()) {
		session.sendMessage(new TextMessage("{'message': 'CONNECTED'}"));
	    }
	} catch (JsonProcessingException e) {
	    e.printStackTrace();
	} catch (IOException e) {
	    e.printStackTrace();
	}

    }

    public List<WebSocketSession> getSessions() {
	return sessions;
    }

    public String getPath() {
	return path;
    }
}
