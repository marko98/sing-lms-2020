package lms.websocket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import org.springframework.web.socket.server.standard.ServletServerContainerFactoryBean;

import lms.model.Administrator;
import lms.model.StudentskaSluzba;
import lms.model.StudentskiSluzbenik;

@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

    @Autowired
    private SocketHandler<Administrator, Long> aSocketHandler;

    @Autowired
    private SocketHandler<StudentskaSluzba, Long> sSocketHandler;

    @Autowired
    private SocketHandler<StudentskiSluzbenik, Long> ssSocketHandler;

    private DataSocketHandler porukeVideiSocketHandler = new DataSocketHandler("/poruka_video");

    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {

	registry.addHandler(aSocketHandler, "/administrator_ws").setAllowedOrigins("*");

	registry.addHandler(sSocketHandler, "/studentska_sluzba_ws").setAllowedOrigins("*");

	registry.addHandler(ssSocketHandler, "/studentski_sluzbenik_ws").setAllowedOrigins("*");

	registry.addHandler(porukeVideiSocketHandler, porukeVideiSocketHandler.getPath()).setAllowedOrigins("*");

    }

    @Bean
    public ServletServerContainerFactoryBean createWebSocketContainer() {
	ServletServerContainerFactoryBean container = new ServletServerContainerFactoryBean();
	container.setMaxTextMessageBufferSize(500000);
	container.setMaxBinaryMessageBufferSize(500000);
	return container;
    }

}
