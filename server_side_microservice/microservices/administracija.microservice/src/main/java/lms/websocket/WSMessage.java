package lms.websocket;

import java.util.HashMap;

public class WSMessage {
    private HashMap<WSMessageKeys, Object> message = new HashMap<WSMessageKeys, Object>();

    public WSMessage() {
	super();
    }

    public HashMap<WSMessageKeys, Object> getMessage() {
	return message;
    }

    public void setMessage(HashMap<WSMessageKeys, Object> message) {
	this.message = message;
    }

}
