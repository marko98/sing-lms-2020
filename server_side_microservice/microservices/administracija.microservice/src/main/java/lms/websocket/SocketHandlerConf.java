package lms.websocket;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lms.model.Administrator;
import lms.model.StudentskaSluzba;
import lms.model.StudentskiSluzbenik;

@Configuration
public class SocketHandlerConf {
    @Bean
    public SocketHandler<Administrator, Long> getAdministratorSocketHandler() {
	return new SocketHandler<Administrator, Long>("/administrator_ws");
    }

    @Bean
    public SocketHandler<StudentskaSluzba, Long> getStudentskaSluzbaSocketHandler() {
	return new SocketHandler<StudentskaSluzba, Long>("/studentska_sluzba_ws");
    }

    @Bean
    public SocketHandler<StudentskiSluzbenik, Long> getStudentskiSluzbenikSocketHandler() {
	return new SocketHandler<StudentskiSluzbenik, Long>("/studentski_sluzbenik_ws");
    }
}