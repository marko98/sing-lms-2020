package lms.websocket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

import lms.model.Autor;
import lms.model.AutorNastavniMaterijal;
import lms.model.Dogadjaj;
import lms.model.DogadjajKalendar;
import lms.model.DrugiOblikNastave;
import lms.model.GodinaStudija;
import lms.model.Kalendar;
import lms.model.Konsultacija;
import lms.model.NastavniMaterijal;
import lms.model.Prostorija;
import lms.model.StudijskiProgram;
import lms.model.TerminNastave;
import lms.model.VremeOdrzavanjaUNedelji;

@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

    @Autowired
    private SocketHandler<Autor, Long> autor;
    @Autowired
    private SocketHandler<AutorNastavniMaterijal, Long> autorNastavniMaterijal;
    @Autowired
    private SocketHandler<Dogadjaj, Long> dogadjaj;
    @Autowired
    private SocketHandler<DogadjajKalendar, Long> dogadjajKalendar;
    @Autowired
    private SocketHandler<DrugiOblikNastave, Long> drugiOblikNastave;
    @Autowired
    private SocketHandler<GodinaStudija, Long> godinaStudija;
    @Autowired
    private SocketHandler<Kalendar, Long> kalendar;
    @Autowired
    private SocketHandler<Konsultacija, Long> konsultacija;
    @Autowired
    private SocketHandler<NastavniMaterijal, Long> nastavniMaterijal;
    @Autowired
    private SocketHandler<Prostorija, Long> prostorija;
    @Autowired
    private SocketHandler<StudijskiProgram, Long> studijskiProgram;
    @Autowired
    private SocketHandler<TerminNastave, Long> terminNastave;
    @Autowired
    private SocketHandler<VremeOdrzavanjaUNedelji, Long> vremeOdrzavanjaUNedelji;

    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {

	registry.addHandler(autor, "/autor_ws").setAllowedOrigins("*");
	registry.addHandler(autorNastavniMaterijal, "/autor_nastavni_materijal_ws").setAllowedOrigins("*");
	registry.addHandler(dogadjaj, "/dogadjaj_ws").setAllowedOrigins("*");
	registry.addHandler(dogadjajKalendar, "/dogadjaj_kalendar_ws").setAllowedOrigins("*");
	registry.addHandler(drugiOblikNastave, "/drugi_oblik_nastave_ws").setAllowedOrigins("*");
	registry.addHandler(godinaStudija, "/godina_studija_ws").setAllowedOrigins("*");
	registry.addHandler(kalendar, "/kalendar_ws").setAllowedOrigins("*");
	registry.addHandler(konsultacija, "/konsultacija_ws").setAllowedOrigins("*");
	registry.addHandler(nastavniMaterijal, "/nastavni_materijal_ws").setAllowedOrigins("*");
	registry.addHandler(prostorija, "/prostorija_ws").setAllowedOrigins("*");
	registry.addHandler(studijskiProgram, "/studijski_program_ws").setAllowedOrigins("*");
	registry.addHandler(terminNastave, "/termin_nastave_ws").setAllowedOrigins("*");
	registry.addHandler(vremeOdrzavanjaUNedelji, "/vreme_odrzavanja_u_nedelji_ws").setAllowedOrigins("*");

    }

}
