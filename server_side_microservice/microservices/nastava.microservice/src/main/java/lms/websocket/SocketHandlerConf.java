package lms.websocket;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lms.model.Autor;
import lms.model.AutorNastavniMaterijal;
import lms.model.Dogadjaj;
import lms.model.DogadjajKalendar;
import lms.model.DrugiOblikNastave;
import lms.model.GodinaStudija;
import lms.model.Kalendar;
import lms.model.Konsultacija;
import lms.model.NastavniMaterijal;
import lms.model.Prostorija;
import lms.model.StudijskiProgram;
import lms.model.TerminNastave;
import lms.model.VremeOdrzavanjaUNedelji;

@Configuration
public class SocketHandlerConf {
    @Bean
    public SocketHandler<Autor, Long> getAutorSocketHandler() {
	return new SocketHandler<Autor, Long>("/autor_ws");
    }

    @Bean
    public SocketHandler<AutorNastavniMaterijal, Long> getAutorNastavniMaterijalSocketHandler() {
	return new SocketHandler<AutorNastavniMaterijal, Long>("/autor_nastavni_materijal_ws");
    }

    @Bean
    public SocketHandler<Dogadjaj, Long> getDogadjajSocketHandler() {
	return new SocketHandler<Dogadjaj, Long>("/dogadjaj_ws");
    }
    
    @Bean
    public SocketHandler<DogadjajKalendar, Long> getDogadjajKalendarSocketHandler() {
	return new SocketHandler<DogadjajKalendar, Long>("/dogadjaj_kalendar_ws");
    }
    
    @Bean
    public SocketHandler<DrugiOblikNastave, Long> getDrugiOblikNastaveSocketHandler() {
	return new SocketHandler<DrugiOblikNastave, Long>("/drugi_oblik_nastave_ws");
    }
    
    @Bean
    public SocketHandler<GodinaStudija, Long> getGodinaStudijaObavestenjeSocketHandler() {
	return new SocketHandler<GodinaStudija, Long>("/godina_studija_ws");
    }
    
    @Bean
    public SocketHandler<Kalendar, Long> getKalendarSocketHandler() {
	return new SocketHandler<Kalendar, Long>("/kalendar_ws");
    }
    
    @Bean
    public SocketHandler<Konsultacija, Long> getKonsultacijaSocketHandler() {
	return new SocketHandler<Konsultacija, Long>("/konsultacija_ws");
    }
    
    @Bean
    public SocketHandler<NastavniMaterijal, Long> getNastavniMaterijalSocketHandler() {
	return new SocketHandler<NastavniMaterijal, Long>("/nastavni_materijal_ws");
    }
    @Bean
    public SocketHandler<Prostorija, Long> getProstorijaSocketHandler() {
	return new SocketHandler<Prostorija, Long>("/prostorija_ws");
    }
    @Bean
    public SocketHandler<StudijskiProgram, Long> getStudijskiProgramSocketHandler() {
	return new SocketHandler<StudijskiProgram, Long>("/studijski_program_ws");
    }
    @Bean
    public SocketHandler<TerminNastave, Long> getTerminNastaveSocketHandler() {
	return new SocketHandler<TerminNastave, Long>("/termin_nastave_ws");
    }
    @Bean
    public SocketHandler<VremeOdrzavanjaUNedelji, Long> getVremeOdrzavanjaUNedeljiSocketHandler() {
	return new SocketHandler<VremeOdrzavanjaUNedelji, Long>("/vreme_odrzavanja_u_nedelji_ws");
    }
}