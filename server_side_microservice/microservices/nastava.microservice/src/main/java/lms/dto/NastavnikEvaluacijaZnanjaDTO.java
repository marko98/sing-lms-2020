package lms.dto;

import java.time.LocalDateTime;

import lms.dto.interfaces.DTO;
import lms.enums.StanjeModela;

public class NastavnikEvaluacijaZnanjaDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 8188616100598972441L;

    private Long id;

    private LocalDateTime datum;

    private StanjeModela stanje;

    private Long version;

    private EvaluacijaZnanjaDTO evaluacijaZnanjaDTO;

    private NastavnikDTO nastavnikDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public LocalDateTime getDatum() {
	return this.datum;
    }

    public void setDatum(LocalDateTime datum) {
	this.datum = datum;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public EvaluacijaZnanjaDTO getEvaluacijaZnanjaDTO() {
	return this.evaluacijaZnanjaDTO;
    }

    public void setEvaluacijaZnanjaDTO(EvaluacijaZnanjaDTO evaluacijaZnanjaDTO) {
	this.evaluacijaZnanjaDTO = evaluacijaZnanjaDTO;
    }

    public NastavnikDTO getNastavnikDTO() {
	return this.nastavnikDTO;
    }

    public void setNastavnikDTO(NastavnikDTO nastavnikDTO) {
	this.nastavnikDTO = nastavnikDTO;
    }

    public NastavnikEvaluacijaZnanjaDTO() {
	super();
    }

}