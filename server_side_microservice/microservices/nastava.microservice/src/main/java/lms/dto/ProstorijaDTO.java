package lms.dto;

import lms.dto.interfaces.DTO;
import lms.enums.StanjeModela;
import lms.enums.TipProstorije;

public class ProstorijaDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -5141479240210959303L;

    private Long id;

    private String naziv;

    private StanjeModela stanje;

    private Long version;

    private TipProstorije tip;

    private OdeljenjeDTO odeljenjeDTO;

    private java.util.List<TerminNastaveDTO> terminiNastaveDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getNaziv() {
	return this.naziv;
    }

    public void setNaziv(String naziv) {
	this.naziv = naziv;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public TipProstorije getTip() {
	return this.tip;
    }

    public void setTip(TipProstorije tip) {
	this.tip = tip;
    }

    public OdeljenjeDTO getOdeljenjeDTO() {
	return this.odeljenjeDTO;
    }

    public void setOdeljenjeDTO(OdeljenjeDTO odeljenjeDTO) {
	this.odeljenjeDTO = odeljenjeDTO;
    }

    public java.util.List<TerminNastaveDTO> getTerminiNastaveDTO() {
	return this.terminiNastaveDTO;
    }

    public void setTerminiNastaveDTO(java.util.List<TerminNastaveDTO> terminiNastaveDTO) {
	this.terminiNastaveDTO = terminiNastaveDTO;
    }

    public ProstorijaDTO() {
	super();
	this.terminiNastaveDTO = new java.util.ArrayList<>();
    }

}