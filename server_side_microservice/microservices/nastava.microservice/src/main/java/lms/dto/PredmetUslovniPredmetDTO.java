package lms.dto;

import lms.dto.interfaces.DTO;
import lms.enums.StanjeModela;

public class PredmetUslovniPredmetDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 8970130984348088008L;

    private Long id;

    private StanjeModela stanje;

    private Long version;

    private PredmetDTO uslovDTO;

    private PredmetDTO predmetDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public PredmetDTO getUslovDTO() {
	return this.uslovDTO;
    }

    public void setUslovDTO(PredmetDTO uslovDTO) {
	this.uslovDTO = uslovDTO;
    }

    public PredmetDTO getPredmetDTO() {
	return this.predmetDTO;
    }

    public void setPredmetDTO(PredmetDTO predmetDTO) {
	this.predmetDTO = predmetDTO;
    }

    public PredmetUslovniPredmetDTO() {
	super();
    }

}