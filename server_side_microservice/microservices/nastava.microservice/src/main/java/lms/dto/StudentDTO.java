package lms.dto;

import lms.dto.interfaces.DTO;
import lms.enums.StanjeModela;
import lms.interfaces.Identifikacija;

public class StudentDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -2921447174155337208L;

    private Long id;

    private StanjeModela stanje;

    private Long version;

    private RegistrovaniKorisnikDTO registrovaniKorisnikDTO;

    private java.util.List<StudentNaStudijiDTO> studijeDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public RegistrovaniKorisnikDTO getRegistrovaniKorisnikDTO() {
	return this.registrovaniKorisnikDTO;
    }

    public void setRegistrovaniKorisnikDTO(RegistrovaniKorisnikDTO registrovaniKorisnikDTO) {
	this.registrovaniKorisnikDTO = registrovaniKorisnikDTO;
    }

    public java.util.List<StudentNaStudijiDTO> getStudijeDTO() {
	return this.studijeDTO;
    }

    public void setStudijeDTO(java.util.List<StudentNaStudijiDTO> studijeDTO) {
	this.studijeDTO = studijeDTO;
    }

    public StudentDTO() {
	super();
	this.studijeDTO = new java.util.ArrayList<>();
    }

    public StudentNaStudijiDTO getStudentNaStudijiDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addStudentNaStudijiDTO(StudentNaStudijiDTO studentNaStudijiDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeStudentNaStudijiDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

}