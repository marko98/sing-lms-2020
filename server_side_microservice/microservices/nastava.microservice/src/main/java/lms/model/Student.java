package lms.model;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.StudentDTO;
import lms.dto.StudentNaStudijiDTO;
import lms.enums.StanjeModela;
import lms.interfaces.Identifikacija;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE student SET stanje = 'OBRISAN' WHERE student_id = ? AND version = ? ", check = ResultCheckStyle.COUNT)
@Table(name = "student")
@Transactional
public class Student implements Entitet<Student, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -1821040413318033916L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "student_id")
    private Long id;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.OneToOne
    private RegistrovaniKorisnik registrovaniKorisnik;

    @javax.persistence.OneToMany(mappedBy = "student")
    private java.util.List<StudentNaStudiji> studije;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public RegistrovaniKorisnik getRegistrovaniKorisnik() {
	return this.registrovaniKorisnik;
    }

    public void setRegistrovaniKorisnik(RegistrovaniKorisnik registrovaniKorisnik) {
	this.registrovaniKorisnik = registrovaniKorisnik;
    }

    public java.util.List<StudentNaStudiji> getStudije() {
	return this.studije;
    }

    public void setStudije(java.util.List<StudentNaStudiji> studije) {
	this.studije = studije;
    }

    public Student() {
	super();
	this.studije = new java.util.ArrayList<>();
    }

    public StudentNaStudiji getStudija(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addStudija(StudentNaStudiji studija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeStudija(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.NEAKTIVAN; // aktivira ga administrativno osoblje
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((registrovaniKorisnik == null) ? 0 : registrovaniKorisnik.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((studije == null) ? 0 : studije.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Student other = (Student) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (registrovaniKorisnik == null) {
	    if (other.registrovaniKorisnik != null)
		return false;
	} else if (!registrovaniKorisnik.equals(other.registrovaniKorisnik))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (studije == null) {
	    if (other.studije != null)
		return false;
	} else if (!studije.equals(other.studije))
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public StudentDTO getDTO() {
	StudentDTO studentDTO = new StudentDTO();
	studentDTO.setId(id);
	studentDTO.setStanje(stanje);
	studentDTO.setVersion(version);

	if (this.registrovaniKorisnik != null)
	    studentDTO.setRegistrovaniKorisnikDTO(this.registrovaniKorisnik.getDTOinsideDTO());

	ArrayList<StudentNaStudijiDTO> studijeDTOs = new ArrayList<StudentNaStudijiDTO>();
	for (StudentNaStudiji studentNaStudiji : this.studije) {
	    studijeDTOs.add(studentNaStudiji.getDTOinsideDTO());
	}
	studentDTO.setStudijeDTO(studijeDTOs);

	return studentDTO;
    }

    @Override
    public StudentDTO getDTOinsideDTO() {
	StudentDTO studentDTO = new StudentDTO();
	studentDTO.setId(id);
	studentDTO.setStanje(stanje);
	studentDTO.setVersion(version);
	return studentDTO;
    }

    @Override
    public void update(Student entitet) {
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());

	this.setRegistrovaniKorisnik(entitet.getRegistrovaniKorisnik());

    }
}