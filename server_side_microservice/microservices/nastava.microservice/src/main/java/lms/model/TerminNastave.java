package lms.model;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.TerminNastaveDTO;
import lms.enums.StanjeModela;
import lms.enums.TipNastave;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE termin_nastave SET stanje = 'OBRISAN' WHERE termin_nastave_id = ? AND version = ? ", check = ResultCheckStyle.COUNT)
@Table(name = "termin_nastave")
@Transactional
public class TerminNastave implements Entitet<TerminNastave, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -5683645232443968590L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "termin_nastave_id")
    private Long id;

    @javax.persistence.OneToOne
    private VremeOdrzavanjaUNedelji vremeOdrzavanjaUNedelji;

    private Integer trajanje;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "prostorija_id")
    private Prostorija prostorija;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "nastavnik_id")
    private Nastavnik nastavnik;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "realizacija_predmeta_id")
    private RealizacijaPredmeta realizacijaPredmeta;

    @Enumerated(EnumType.STRING)
    private TipNastave tipNastave;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public VremeOdrzavanjaUNedelji getVremeOdrzavanjaUNedelji() {
	return this.vremeOdrzavanjaUNedelji;
    }

    public void setVremeOdrzavanjaUNedelji(VremeOdrzavanjaUNedelji vremeOdrzavanjaUNedelji) {
	this.vremeOdrzavanjaUNedelji = vremeOdrzavanjaUNedelji;
    }

    public Integer getTrajanje() {
	return this.trajanje;
    }

    public void setTrajanje(Integer trajanje) {
	this.trajanje = trajanje;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public Prostorija getProstorija() {
	return this.prostorija;
    }

    public void setProstorija(Prostorija prostorija) {
	this.prostorija = prostorija;
    }

    public Nastavnik getNastavnik() {
	return this.nastavnik;
    }

    public void setNastavnik(Nastavnik nastavnik) {
	this.nastavnik = nastavnik;
    }

    public RealizacijaPredmeta getRealizacijaPredmeta() {
	return this.realizacijaPredmeta;
    }

    public void setRealizacijaPredmeta(RealizacijaPredmeta realizacijaPredmeta) {
	this.realizacijaPredmeta = realizacijaPredmeta;
    }

    public TipNastave getTipNastave() {
	return this.tipNastave;
    }

    public void setTipNastave(TipNastave tipNastave) {
	this.tipNastave = tipNastave;
    }

    public TerminNastave() {
	super();
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((nastavnik == null) ? 0 : nastavnik.hashCode());
	result = prime * result + ((prostorija == null) ? 0 : prostorija.hashCode());
	result = prime * result + ((realizacijaPredmeta == null) ? 0 : realizacijaPredmeta.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((tipNastave == null) ? 0 : tipNastave.hashCode());
	result = prime * result + ((trajanje == null) ? 0 : trajanje.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	result = prime * result + ((vremeOdrzavanjaUNedelji == null) ? 0 : vremeOdrzavanjaUNedelji.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	TerminNastave other = (TerminNastave) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (nastavnik == null) {
	    if (other.nastavnik != null)
		return false;
	} else if (!nastavnik.equals(other.nastavnik))
	    return false;
	if (prostorija == null) {
	    if (other.prostorija != null)
		return false;
	} else if (!prostorija.equals(other.prostorija))
	    return false;
	if (realizacijaPredmeta == null) {
	    if (other.realizacijaPredmeta != null)
		return false;
	} else if (!realizacijaPredmeta.equals(other.realizacijaPredmeta))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (tipNastave != other.tipNastave)
	    return false;
	if (trajanje == null) {
	    if (other.trajanje != null)
		return false;
	} else if (!trajanje.equals(other.trajanje))
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	if (vremeOdrzavanjaUNedelji == null) {
	    if (other.vremeOdrzavanjaUNedelji != null)
		return false;
	} else if (!vremeOdrzavanjaUNedelji.equals(other.vremeOdrzavanjaUNedelji))
	    return false;
	return true;
    }

    @Override
    public TerminNastaveDTO getDTO() {
	TerminNastaveDTO terminNastaveDTO = new TerminNastaveDTO();
	terminNastaveDTO.setId(id);
	terminNastaveDTO.setStanje(stanje);
	terminNastaveDTO.setTipNastave(tipNastave);
	terminNastaveDTO.setTrajanje(trajanje);
	terminNastaveDTO.setVersion(version);

	if (this.nastavnik != null)
	    terminNastaveDTO.setNastavnikDTO(this.nastavnik.getDTOinsideDTO());
	if (this.prostorija != null)
	    terminNastaveDTO.setProstorijaDTO(this.prostorija.getDTOinsideDTO());
	if (this.realizacijaPredmeta != null)
	    terminNastaveDTO.setRealizacijaPredmetaDTO(this.realizacijaPredmeta.getDTOinsideDTO());
	if (this.vremeOdrzavanjaUNedelji != null)
	    terminNastaveDTO.setVremeOdrzavanjaUNedeljiDTO(this.vremeOdrzavanjaUNedelji.getDTOinsideDTO());

	return terminNastaveDTO;
    }

    @Override
    public TerminNastaveDTO getDTOinsideDTO() {
	TerminNastaveDTO terminNastaveDTO = new TerminNastaveDTO();
	terminNastaveDTO.setId(id);
	terminNastaveDTO.setStanje(stanje);
	terminNastaveDTO.setTipNastave(tipNastave);
	terminNastaveDTO.setTrajanje(trajanje);
	terminNastaveDTO.setVersion(version);
	return terminNastaveDTO;
    }

    @Override
    public void update(TerminNastave entitet) {
	this.setStanje(entitet.getStanje());
	this.setTipNastave(entitet.getTipNastave());
	this.setTrajanje(entitet.getTrajanje());
	this.setVersion(entitet.getVersion());

	this.setNastavnik(entitet.getNastavnik());
	this.setProstorija(entitet.getProstorija());
	this.setRealizacijaPredmeta(entitet.getRealizacijaPredmeta());
	this.setVremeOdrzavanjaUNedelji(entitet.getVremeOdrzavanjaUNedelji());

    }

}