package lms.nastava.microservice.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.DogadjajKalendar;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class DogadjajKalendarService extends CrudService<DogadjajKalendar, Long> {

}