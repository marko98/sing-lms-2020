package lms.nastava.microservice.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.VremeOdrzavanjaUNedelji;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class VremeOdrzavanjaUNedeljiService extends CrudService<VremeOdrzavanjaUNedelji, Long> {

}