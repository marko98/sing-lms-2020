package lms.nastava.microservice.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.Dogadjaj;

@Repository
@Scope("singleton")
public interface DogadjajPASRepository extends PagingAndSortingRepository<Dogadjaj, Long> {

}