package lms.nastava.microservice.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.StudijskiProgram;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class StudijskiProgramService extends CrudService<StudijskiProgram, Long> {

}