package lms.nastava.microservice.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.StudijskiProgram;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class StudijskiProgramRepository extends CrudRepository<StudijskiProgram, Long> {

}