package lms.nastava.microservice.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.DrugiOblikNastave;

@Controller
@Scope("singleton")
@RequestMapping(path = "drugi_oblik_nastave")
@CrossOrigin(origins = "*")
public class DrugiOblikNastaveController extends CrudController<DrugiOblikNastave, Long> {

}