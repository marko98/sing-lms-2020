package lms.nastava.microservice.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.Konsultacija;

@Controller
@Scope("singleton")
@RequestMapping(path = "konsultacija")
@CrossOrigin(origins = "*")
public class KonsultacijaController extends CrudController<Konsultacija, Long> {

}