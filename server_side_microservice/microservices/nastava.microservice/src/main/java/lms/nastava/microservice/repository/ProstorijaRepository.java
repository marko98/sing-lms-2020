package lms.nastava.microservice.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.Prostorija;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class ProstorijaRepository extends CrudRepository<Prostorija, Long> {

}