package lms.nastava.microservice.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.DogadjajKalendar;

@Controller
@Scope("singleton")
@RequestMapping(path = "dogadjaj_kalendar")
@CrossOrigin(origins = "*")
public class DogadjajKalendarController extends CrudController<DogadjajKalendar, Long> {

}