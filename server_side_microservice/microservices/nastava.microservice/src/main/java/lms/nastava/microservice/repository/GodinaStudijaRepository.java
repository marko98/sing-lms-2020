package lms.nastava.microservice.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.GodinaStudija;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class GodinaStudijaRepository extends CrudRepository<GodinaStudija, Long> {

}