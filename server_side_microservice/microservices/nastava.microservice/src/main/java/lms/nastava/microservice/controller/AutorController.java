package lms.nastava.microservice.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.Autor;

@Controller
@Scope("singleton")
@RequestMapping(path = "autor")
@CrossOrigin(origins = "*")
public class AutorController extends CrudController<Autor, Long> {

}