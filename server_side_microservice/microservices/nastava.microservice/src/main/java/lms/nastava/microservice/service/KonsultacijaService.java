package lms.nastava.microservice.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.Konsultacija;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class KonsultacijaService extends CrudService<Konsultacija, Long> {

}