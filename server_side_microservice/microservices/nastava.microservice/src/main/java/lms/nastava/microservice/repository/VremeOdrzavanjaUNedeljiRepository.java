package lms.nastava.microservice.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.VremeOdrzavanjaUNedelji;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class VremeOdrzavanjaUNedeljiRepository extends CrudRepository<VremeOdrzavanjaUNedelji, Long> {

}