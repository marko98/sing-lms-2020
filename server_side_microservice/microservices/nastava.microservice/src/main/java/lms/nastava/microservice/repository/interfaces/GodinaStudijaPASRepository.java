package lms.nastava.microservice.repository.interfaces;

import java.util.Collection;

import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.GodinaStudija;

@Repository
@Scope("singleton")
public interface GodinaStudijaPASRepository extends PagingAndSortingRepository<GodinaStudija, Long> {
    @Query("select gs from GodinaStudija gs where gs.studijskiProgram.id = ?1")
    public Collection<GodinaStudija> findAllByStudijskiProgramId(Long id);
}