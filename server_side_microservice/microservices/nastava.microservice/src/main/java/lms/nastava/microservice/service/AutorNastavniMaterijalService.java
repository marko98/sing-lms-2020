package lms.nastava.microservice.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.AutorNastavniMaterijal;
import lms.nastava.microservice.repository.interfaces.AutorNastavniMaterijalPASRepository;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class AutorNastavniMaterijalService extends CrudService<AutorNastavniMaterijal, Long> {
    public Iterable<AutorNastavniMaterijal> findAllByNastavniMaterijalId(Long id) {
	return ((AutorNastavniMaterijalPASRepository)this.repository.getRepository()).findAllByNastavniMaterijalId(id);
    }
}