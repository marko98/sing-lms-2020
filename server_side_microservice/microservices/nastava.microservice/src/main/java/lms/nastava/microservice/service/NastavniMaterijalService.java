package lms.nastava.microservice.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.NastavniMaterijal;
import lms.nastava.microservice.repository.interfaces.NastavniMaterijalPASRepository;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class NastavniMaterijalService extends CrudService<NastavniMaterijal, Long> {
    public Iterable<NastavniMaterijal> findAllByIshodId(Long id) {
	return ((NastavniMaterijalPASRepository)this.repository.getRepository()).findAllByIshodId(id);
    }
}