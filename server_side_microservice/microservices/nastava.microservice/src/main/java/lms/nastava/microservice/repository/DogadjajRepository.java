package lms.nastava.microservice.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.Dogadjaj;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class DogadjajRepository extends CrudRepository<Dogadjaj, Long> {

}