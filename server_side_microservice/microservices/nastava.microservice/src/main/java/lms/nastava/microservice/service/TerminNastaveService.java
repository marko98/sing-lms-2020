package lms.nastava.microservice.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.TerminNastave;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class TerminNastaveService extends CrudService<TerminNastave, Long> {

}