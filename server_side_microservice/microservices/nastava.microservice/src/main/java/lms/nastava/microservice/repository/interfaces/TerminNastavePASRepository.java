package lms.nastava.microservice.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.TerminNastave;

@Repository
@Scope("singleton")
public interface TerminNastavePASRepository extends PagingAndSortingRepository<TerminNastave, Long> {

}