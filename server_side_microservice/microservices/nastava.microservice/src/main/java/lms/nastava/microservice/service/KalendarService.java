package lms.nastava.microservice.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.Kalendar;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class KalendarService extends CrudService<Kalendar, Long> {

}