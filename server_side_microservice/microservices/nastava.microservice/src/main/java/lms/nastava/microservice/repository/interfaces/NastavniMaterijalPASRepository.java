package lms.nastava.microservice.repository.interfaces;

import java.util.Collection;

import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.NastavniMaterijal;

@Repository
@Scope("singleton")
public interface NastavniMaterijalPASRepository extends PagingAndSortingRepository<NastavniMaterijal, Long> {
    @Query("select nm from NastavniMaterijal nm where nm.ishod.id = ?1")
    public Collection<NastavniMaterijal> findAllByIshodId(Long id);
}