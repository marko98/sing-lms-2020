package lms.nastava.microservice.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.Kalendar;

@Repository
@Scope("singleton")
public interface KalendarPASRepository extends PagingAndSortingRepository<Kalendar, Long> {

}