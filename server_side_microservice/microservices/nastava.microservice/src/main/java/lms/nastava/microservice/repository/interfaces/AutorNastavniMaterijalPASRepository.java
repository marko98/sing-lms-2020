package lms.nastava.microservice.repository.interfaces;

import java.util.Collection;

import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.AutorNastavniMaterijal;

@Repository
@Scope("singleton")
public interface AutorNastavniMaterijalPASRepository extends PagingAndSortingRepository<AutorNastavniMaterijal, Long> {
    @Query("select anm from AutorNastavniMaterijal anm where anm.nastavniMaterijal.id = ?1")
    public Collection<AutorNastavniMaterijal> findAllByNastavniMaterijalId(Long id);
}