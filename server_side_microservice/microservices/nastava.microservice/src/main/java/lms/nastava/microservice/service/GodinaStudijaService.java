package lms.nastava.microservice.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.GodinaStudija;
import lms.nastava.microservice.repository.interfaces.GodinaStudijaPASRepository;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class GodinaStudijaService extends CrudService<GodinaStudija, Long> {
    public Iterable<GodinaStudija> findAllByStudijskiProgramId(Long id) {
	return ((GodinaStudijaPASRepository)this.repository.getRepository()).findAllByStudijskiProgramId(id);
    }
}