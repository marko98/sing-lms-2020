package lms.nastava.microservice.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.DogadjajKalendar;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class DogadjajKalendarRepository extends CrudRepository<DogadjajKalendar, Long> {

}