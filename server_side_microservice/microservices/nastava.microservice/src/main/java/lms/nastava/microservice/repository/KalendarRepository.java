package lms.nastava.microservice.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.Kalendar;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class KalendarRepository extends CrudRepository<Kalendar, Long> {

}