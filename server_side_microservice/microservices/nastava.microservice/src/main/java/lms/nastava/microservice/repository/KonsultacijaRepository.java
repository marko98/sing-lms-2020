package lms.nastava.microservice.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.Konsultacija;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class KonsultacijaRepository extends CrudRepository<Konsultacija, Long> {

}