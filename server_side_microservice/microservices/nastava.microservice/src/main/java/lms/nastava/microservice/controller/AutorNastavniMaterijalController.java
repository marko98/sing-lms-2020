package lms.nastava.microservice.controller;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import lms.controller.CrudController;
import lms.dto.interfaces.DTO;
import lms.model.AutorNastavniMaterijal;
import lms.nastava.microservice.service.AutorNastavniMaterijalService;

@Controller
@Scope("singleton")
@RequestMapping(path = "autor_nastavni_materijal")
@CrossOrigin(origins = "*")
public class AutorNastavniMaterijalController extends CrudController<AutorNastavniMaterijal, Long> {

    @RequestMapping(path = "/nastavni_materijal/{id}", method = RequestMethod.GET)
    public ResponseEntity<List<DTO<Long>>> findAll(@PathVariable("id") Long id) {

	List<AutorNastavniMaterijal> list = StreamSupport.stream(((AutorNastavniMaterijalService)this.service).findAllByNastavniMaterijalId(id).spliterator(), false).collect(Collectors.toList());

	return new ResponseEntity<List<DTO<Long>>>(list.stream().map(t -> t.getDTO()).collect(Collectors.toList()),
		HttpStatus.OK);
    }
    
}