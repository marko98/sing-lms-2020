package lms.nastava.microservice.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.DrugiOblikNastave;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class DrugiOblikNastaveRepository extends CrudRepository<DrugiOblikNastave, Long> {

}