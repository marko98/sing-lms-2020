package lms.dto;

import lms.dto.interfaces.DTO;
import lms.enums.StanjeModela;

public class RegistrovaniKorisnikRolaDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 5137572757501935487L;

    private Long id;

    private StanjeModela stanje;

    private Long version;

    private RegistrovaniKorisnikDTO registrovaniKorisnikDTO;

    private RolaDTO rolaDTO;

    public RegistrovaniKorisnikRolaDTO() {
	super();
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public StanjeModela getStanje() {
	return stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public RegistrovaniKorisnikDTO getRegistrovaniKorisnikDTO() {
	return registrovaniKorisnikDTO;
    }

    public void setRegistrovaniKorisnikDTO(RegistrovaniKorisnikDTO registrovaniKorisnikDTO) {
	this.registrovaniKorisnikDTO = registrovaniKorisnikDTO;
    }

    public RolaDTO getRolaDTO() {
	return rolaDTO;
    }

    public void setRolaDTO(RolaDTO rolaDTO) {
	this.rolaDTO = rolaDTO;
    }

}
