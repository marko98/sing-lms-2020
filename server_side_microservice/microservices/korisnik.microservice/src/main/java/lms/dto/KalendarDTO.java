package lms.dto;

import java.time.LocalDateTime;

import lms.dto.interfaces.DTO;
import lms.enums.StanjeModela;
import lms.enums.TipStudija;
import lms.interfaces.Identifikacija;

public class KalendarDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -8765830993339139468L;

    private Long id;
        
    private LocalDateTime pocetniDatum;
        
    private LocalDateTime krajnjiDatum;
        
    private StanjeModela stanje;
        
    private Long version;
        
    private TipStudija tipStudija;
        
    private UniverzitetDTO univerzitetDTO;
        
    private java.util.List<DogadjajKalendarDTO> dogadjajiKalendarDTO;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public LocalDateTime getPocetniDatum () {
        return this.pocetniDatum;
    }

    public void setPocetniDatum (LocalDateTime pocetniDatum) {
        this.pocetniDatum = pocetniDatum;
    }
    
    public LocalDateTime getKrajnjiDatum () {
        return this.krajnjiDatum;
    }

    public void setKrajnjiDatum (LocalDateTime krajnjiDatum) {
        this.krajnjiDatum = krajnjiDatum;
    }
    
    public StanjeModela getStanje () {
        return this.stanje;
    }

    public void setStanje (StanjeModela stanje) {
        this.stanje = stanje;
    }
    
    public Long getVersion () {
        return this.version;
    }

    public void setVersion (Long version) {
        this.version = version;
    }
    
    public TipStudija getTipStudija () {
        return this.tipStudija;
    }

    public void setTipStudija (TipStudija tipStudija) {
        this.tipStudija = tipStudija;
    }
    
    public UniverzitetDTO getUniverzitetDTO () {
        return this.univerzitetDTO;
    }

    public void setUniverzitetDTO (UniverzitetDTO univerzitetDTO) {
        this.univerzitetDTO = univerzitetDTO;
    }
    
    public java.util.List<DogadjajKalendarDTO> getDogadjajiKalendarDTO () {
        return this.dogadjajiKalendarDTO;
    }

    public void setDogadjajiKalendarDTO (java.util.List<DogadjajKalendarDTO> dogadjajiKalendarDTO) {
        this.dogadjajiKalendarDTO = dogadjajiKalendarDTO;
    }
    


    public KalendarDTO () {
        super();
        this.dogadjajiKalendarDTO = new java.util.ArrayList<>();
    }

    public DogadjajKalendarDTO getDogadjajKalendarDTO (Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addDogadjajKalendarDTO (DogadjajKalendarDTO dogadjajKalendarDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeDogadjajKalendarDTO (Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

}