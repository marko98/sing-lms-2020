package lms.model;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.FakultetDTO;
import lms.dto.KontaktDTO;
import lms.dto.NastavnikFakultetDTO;
import lms.dto.OdeljenjeDTO;
import lms.dto.StudijskiProgramDTO;
import lms.enums.StanjeModela;
import lms.interfaces.Identifikacija;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity
@SQLDelete(sql = "UPDATE fakultet SET stanje = 'OBRISAN' WHERE fakultet_id = ? AND version = ? ", check = ResultCheckStyle.COUNT)
@Table(name = "fakultet")
public class Fakultet implements Entitet<Fakultet, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 2530702344308151160L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "fakultet_id")
    private Long id;

    private String naziv;

    private String opis;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.OneToMany(mappedBy = "fakultet")
    private java.util.List<Odeljenje> odeljenja;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "univerzitet_id")
    private Univerzitet univerzitet;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "nastavnik_id")
    private Nastavnik dekan;

    @javax.persistence.OneToMany(mappedBy = "fakultet")
    private java.util.List<NastavnikFakultet> nastavniciFakultet;

    @javax.persistence.OneToMany(mappedBy = "fakultet")
    private java.util.List<StudijskiProgram> studijskiProgrami;

    @javax.persistence.OneToMany(mappedBy = "fakultet")
    private java.util.List<Kontakt> kontakti;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getNaziv() {
	return this.naziv;
    }

    public void setNaziv(String naziv) {
	this.naziv = naziv;
    }

    public String getOpis() {
	return this.opis;
    }

    public void setOpis(String opis) {
	this.opis = opis;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public java.util.List<Odeljenje> getOdeljenja() {
	return this.odeljenja;
    }

    public void setOdeljenja(java.util.List<Odeljenje> odeljenja) {
	this.odeljenja = odeljenja;
    }

    public Univerzitet getUniverzitet() {
	return this.univerzitet;
    }

    public void setUniverzitet(Univerzitet univerzitet) {
	this.univerzitet = univerzitet;
    }

    public Nastavnik getDekan() {
	return this.dekan;
    }

    public void setDekan(Nastavnik dekan) {
	this.dekan = dekan;
    }

    public java.util.List<NastavnikFakultet> getNastavniciFakultet() {
	return this.nastavniciFakultet;
    }

    public void setNastavniciFakultet(java.util.List<NastavnikFakultet> nastavniciFakultet) {
	this.nastavniciFakultet = nastavniciFakultet;
    }

    public java.util.List<StudijskiProgram> getStudijskiProgrami() {
	return this.studijskiProgrami;
    }

    public void setStudijskiProgrami(java.util.List<StudijskiProgram> studijskiProgrami) {
	this.studijskiProgrami = studijskiProgrami;
    }

    public java.util.List<Kontakt> getKontakti() {
	return this.kontakti;
    }

    public void setKontakti(java.util.List<Kontakt> kontakti) {
	this.kontakti = kontakti;
    }

    public Fakultet() {
	super();
	this.odeljenja = new java.util.ArrayList<>();
	this.nastavniciFakultet = new java.util.ArrayList<>();
	this.studijskiProgrami = new java.util.ArrayList<>();
	this.kontakti = new java.util.ArrayList<>();
    }

    public Odeljenje getOdeljenje(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addOdeljenje(Odeljenje odeljenje) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeOdeljenje(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public StudijskiProgram getStudijskiProgram(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addStudijskiProgram(StudijskiProgram studijskiProgram) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeStudijskiProgram(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public NastavnikFakultet getNastavnikFakultet(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addNastavnikFakultet(NastavnikFakultet nastavnikFakultet) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeNastavnikFakultet(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public Kontakt getKontakt(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addKontakt(Kontakt kontakt) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeKontakt(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((dekan == null) ? 0 : dekan.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((kontakti == null) ? 0 : kontakti.hashCode());
	result = prime * result + ((nastavniciFakultet == null) ? 0 : nastavniciFakultet.hashCode());
	result = prime * result + ((naziv == null) ? 0 : naziv.hashCode());
	result = prime * result + ((odeljenja == null) ? 0 : odeljenja.hashCode());
	result = prime * result + ((opis == null) ? 0 : opis.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((studijskiProgrami == null) ? 0 : studijskiProgrami.hashCode());
	result = prime * result + ((univerzitet == null) ? 0 : univerzitet.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Fakultet other = (Fakultet) obj;
	if (dekan == null) {
	    if (other.dekan != null)
		return false;
	} else if (!dekan.equals(other.dekan))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (kontakti == null) {
	    if (other.kontakti != null)
		return false;
	} else if (!kontakti.equals(other.kontakti))
	    return false;
	if (nastavniciFakultet == null) {
	    if (other.nastavniciFakultet != null)
		return false;
	} else if (!nastavniciFakultet.equals(other.nastavniciFakultet))
	    return false;
	if (naziv == null) {
	    if (other.naziv != null)
		return false;
	} else if (!naziv.equals(other.naziv))
	    return false;
	if (odeljenja == null) {
	    if (other.odeljenja != null)
		return false;
	} else if (!odeljenja.equals(other.odeljenja))
	    return false;
	if (opis == null) {
	    if (other.opis != null)
		return false;
	} else if (!opis.equals(other.opis))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (studijskiProgrami == null) {
	    if (other.studijskiProgrami != null)
		return false;
	} else if (!studijskiProgrami.equals(other.studijskiProgrami))
	    return false;
	if (univerzitet == null) {
	    if (other.univerzitet != null)
		return false;
	} else if (!univerzitet.equals(other.univerzitet))
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }
    
    @Override
    public FakultetDTO getDTO() {
	FakultetDTO fakultetDTO = new FakultetDTO();
	fakultetDTO.setId(id);
	fakultetDTO.setNaziv(naziv);
	fakultetDTO.setStanje(stanje);
	fakultetDTO.setVersion(version);
	fakultetDTO.setOpis(opis);

	if (this.dekan != null)
	    fakultetDTO.setDekanDTO(this.dekan.getDTOinsideDTO());
	if (this.univerzitet != null)
	    fakultetDTO.setUniverzitetDTO(this.univerzitet.getDTOinsideDTO());

	ArrayList<NastavnikFakultetDTO> nastavniciFakultetDTOs = new ArrayList<NastavnikFakultetDTO>();
	for (NastavnikFakultet nastavnikFakultet : this.nastavniciFakultet) {
	    nastavniciFakultetDTOs.add(nastavnikFakultet.getDTOinsideDTO());
	}
	fakultetDTO.setNastavniciFakultetDTO(nastavniciFakultetDTOs);

	ArrayList<OdeljenjeDTO> odeljenjaDTOs = new ArrayList<OdeljenjeDTO>();
	for (Odeljenje odeljenje : this.odeljenja) {
	    odeljenjaDTOs.add(odeljenje.getDTOinsideDTO());
	}
	fakultetDTO.setOdeljenjaDTO(odeljenjaDTOs);

	ArrayList<StudijskiProgramDTO> studijskiProgramiDTOs = new ArrayList<StudijskiProgramDTO>();
	for (StudijskiProgram studijskiProgram : this.studijskiProgrami) {
	    studijskiProgramiDTOs.add(studijskiProgram.getDTOinsideDTO());
	}
	fakultetDTO.setStudijskiProgramiDTO(studijskiProgramiDTOs);
	
	ArrayList<KontaktDTO> kontaktiDTOs = new ArrayList<KontaktDTO>();
	for (Kontakt kontakt : this.kontakti) {
	    kontaktiDTOs.add(kontakt.getDTOinsideDTO());
	}
	fakultetDTO.setKontaktiDTO(kontaktiDTOs);

	return fakultetDTO;
    }

    @Override
    public FakultetDTO getDTOinsideDTO() {
	FakultetDTO fakultetDTO = new FakultetDTO();
	fakultetDTO.setId(id);
	fakultetDTO.setNaziv(naziv);
	fakultetDTO.setStanje(stanje);
	fakultetDTO.setVersion(version);
	fakultetDTO.setOpis(opis);
	return fakultetDTO;
    }

    @Override
    public void update(Fakultet entitet) {
	this.setNaziv(entitet.getNaziv());
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());

	this.setDekan(entitet.getDekan());
	this.setUniverzitet(entitet.getUniverzitet());
    }

}