package lms.model;

import java.time.LocalDateTime;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.AdresaDTO;
import lms.dto.FakultetDTO;
import lms.dto.KalendarDTO;
import lms.dto.KontaktDTO;
import lms.dto.UniverzitetDTO;
import lms.enums.StanjeModela;
import lms.interfaces.Identifikacija;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity
@SQLDelete(sql = "UPDATE univerzitet SET stanje = 'OBRISAN' WHERE univerzitet_id = ? AND version = ? ", check = ResultCheckStyle.COUNT)
@Table(name = "univerzitet")
public class Univerzitet implements Entitet<Univerzitet, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -5186394196502314237L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "univerzitet_id")
    private Long id;

    private String naziv;

    private LocalDateTime datumOsnivanja;

    private String opis;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.OneToMany(mappedBy = "univerzitet")
    private java.util.List<Adresa> adrese;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "nastavnik_id")
    private Nastavnik rektor;

    @javax.persistence.OneToMany(mappedBy = "univerzitet")
    private java.util.List<Fakultet> fakulteti;

    @javax.persistence.OneToMany(mappedBy = "univerzitet")
    private java.util.List<Kalendar> kalendari;

    @javax.persistence.OneToOne
    private StudentskaSluzba studentskaSluzba;

    @javax.persistence.OneToMany(mappedBy = "univerzitet")
    private java.util.List<Kontakt> kontakti;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getNaziv() {
	return this.naziv;
    }

    public void setNaziv(String naziv) {
	this.naziv = naziv;
    }

    public LocalDateTime getDatumOsnivanja() {
	return this.datumOsnivanja;
    }

    public void setDatumOsnivanja(LocalDateTime datumOsnivanja) {
	this.datumOsnivanja = datumOsnivanja;
    }

    public String getOpis() {
	return this.opis;
    }

    public void setOpis(String opis) {
	this.opis = opis;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public java.util.List<Adresa> getAdrese() {
	return this.adrese;
    }

    public void setAdrese(java.util.List<Adresa> adrese) {
	this.adrese = adrese;
    }

    public Nastavnik getRektor() {
	return this.rektor;
    }

    public void setRektor(Nastavnik rektor) {
	this.rektor = rektor;
    }

    public java.util.List<Fakultet> getFakulteti() {
	return this.fakulteti;
    }

    public void setFakulteti(java.util.List<Fakultet> fakulteti) {
	this.fakulteti = fakulteti;
    }

    public java.util.List<Kalendar> getKalendari() {
	return this.kalendari;
    }

    public void setKalendari(java.util.List<Kalendar> kalendari) {
	this.kalendari = kalendari;
    }

    public StudentskaSluzba getStudentskaSluzba() {
	return this.studentskaSluzba;
    }

    public void setStudentskaSluzba(StudentskaSluzba studentskaSluzba) {
	this.studentskaSluzba = studentskaSluzba;
    }

    public java.util.List<Kontakt> getKontakti() {
	return this.kontakti;
    }

    public void setKontakti(java.util.List<Kontakt> kontakti) {
	this.kontakti = kontakti;
    }

    public Univerzitet() {
	super();
	this.adrese = new java.util.ArrayList<>();
	this.fakulteti = new java.util.ArrayList<>();
	this.kalendari = new java.util.ArrayList<>();
	this.kontakti = new java.util.ArrayList<>();
    }

    public Adresa getAdresa(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addAdresa(Adresa adresa) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeAdresa(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public Fakultet getFakultet(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addFakultet(Fakultet fakultet) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeFakultet(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public Kalendar getKalendar(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addKalendar(Kalendar kalendar) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeKalendar(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public Kontakt getKontakt(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addKontakt(Kontakt kontakt) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeKontakt(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((adrese == null) ? 0 : adrese.hashCode());
	result = prime * result + ((datumOsnivanja == null) ? 0 : datumOsnivanja.hashCode());
	result = prime * result + ((fakulteti == null) ? 0 : fakulteti.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((kalendari == null) ? 0 : kalendari.hashCode());
	result = prime * result + ((kontakti == null) ? 0 : kontakti.hashCode());
	result = prime * result + ((naziv == null) ? 0 : naziv.hashCode());
	result = prime * result + ((opis == null) ? 0 : opis.hashCode());
	result = prime * result + ((rektor == null) ? 0 : rektor.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((studentskaSluzba == null) ? 0 : studentskaSluzba.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Univerzitet other = (Univerzitet) obj;
	if (adrese == null) {
	    if (other.adrese != null)
		return false;
	} else if (!adrese.equals(other.adrese))
	    return false;
	if (datumOsnivanja == null) {
	    if (other.datumOsnivanja != null)
		return false;
	} else if (!datumOsnivanja.equals(other.datumOsnivanja))
	    return false;
	if (fakulteti == null) {
	    if (other.fakulteti != null)
		return false;
	} else if (!fakulteti.equals(other.fakulteti))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (kalendari == null) {
	    if (other.kalendari != null)
		return false;
	} else if (!kalendari.equals(other.kalendari))
	    return false;
	if (kontakti == null) {
	    if (other.kontakti != null)
		return false;
	} else if (!kontakti.equals(other.kontakti))
	    return false;
	if (naziv == null) {
	    if (other.naziv != null)
		return false;
	} else if (!naziv.equals(other.naziv))
	    return false;
	if (opis == null) {
	    if (other.opis != null)
		return false;
	} else if (!opis.equals(other.opis))
	    return false;
	if (rektor == null) {
	    if (other.rektor != null)
		return false;
	} else if (!rektor.equals(other.rektor))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (studentskaSluzba == null) {
	    if (other.studentskaSluzba != null)
		return false;
	} else if (!studentskaSluzba.equals(other.studentskaSluzba))
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }
    
    @Override
    public UniverzitetDTO getDTO() {
	UniverzitetDTO univerzitetDTO = new UniverzitetDTO();
	univerzitetDTO.setId(id);
	univerzitetDTO.setDatumOsnivanja(datumOsnivanja);
	univerzitetDTO.setNaziv(naziv);
	univerzitetDTO.setStanje(stanje);
	univerzitetDTO.setVersion(version);
	univerzitetDTO.setOpis(opis);

	if (this.rektor != null)
	    univerzitetDTO.setRektorDTO(this.rektor.getDTOinsideDTO());
	if (this.studentskaSluzba != null)
	    univerzitetDTO.setStudentskaSluzbaDTO(this.studentskaSluzba.getDTOinsideDTO());

	ArrayList<AdresaDTO> adreseDTOs = new ArrayList<AdresaDTO>();
	for (Adresa adresa : this.adrese) {
	    adreseDTOs.add(adresa.getDTOinsideDTO());
	}
	univerzitetDTO.setAdreseDTO(adreseDTOs);

	ArrayList<FakultetDTO> fakultetiDTOs = new ArrayList<FakultetDTO>();
	for (Fakultet fakultet : this.fakulteti) {
	    fakultetiDTOs.add(fakultet.getDTOinsideDTO());
	}
	univerzitetDTO.setFakultetiDTO(fakultetiDTOs);

	ArrayList<KalendarDTO> kalendariDTOs = new ArrayList<KalendarDTO>();
	for (Kalendar kalendar : this.kalendari) {
	    kalendariDTOs.add(kalendar.getDTOinsideDTO());
	}
	univerzitetDTO.setKalendariDTO(kalendariDTOs);
	
	ArrayList<KontaktDTO> kontaktiDTOs = new ArrayList<KontaktDTO>();
	for (Kontakt kontakt : this.kontakti) {
	    kontaktiDTOs.add(kontakt.getDTOinsideDTO());
	}
	univerzitetDTO.setKontaktiDTO(kontaktiDTOs);

	return univerzitetDTO;
    }

    @Override
    public UniverzitetDTO getDTOinsideDTO() {
	UniverzitetDTO univerzitetDTO = new UniverzitetDTO();
	univerzitetDTO.setId(id);
	univerzitetDTO.setDatumOsnivanja(datumOsnivanja);
	univerzitetDTO.setNaziv(naziv);
	univerzitetDTO.setStanje(stanje);
	univerzitetDTO.setVersion(version);
	univerzitetDTO.setOpis(opis);
	return univerzitetDTO;
    }

    @Override
    public void update(Univerzitet entitet) {
	this.setDatumOsnivanja(entitet.getDatumOsnivanja());
	this.setNaziv(entitet.getNaziv());
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());

	this.setRektor(entitet.getRektor());
	this.setStudentskaSluzba(entitet.getStudentskaSluzba());

    }

}