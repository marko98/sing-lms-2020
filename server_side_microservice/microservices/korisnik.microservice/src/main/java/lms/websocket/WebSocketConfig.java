package lms.websocket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

import lms.model.RegistrovaniKorisnik;
import lms.model.RegistrovaniKorisnikRola;
import lms.model.Rola;

@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

    @Autowired
    private SocketHandler<RegistrovaniKorisnik, Long> registrovaniKorisnik;

    @Autowired
    private SocketHandler<RegistrovaniKorisnikRola, Long> registrovaniKorisnikRola;

    @Autowired
    private SocketHandler<Rola, Long> rola;

    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {

	registry.addHandler(registrovaniKorisnik, "/registrovani_korisnik_ws").setAllowedOrigins("*");

	registry.addHandler(registrovaniKorisnikRola, "/registrovani_korisnik_rola_ws").setAllowedOrigins("*");

	registry.addHandler(rola, "/rola_ws").setAllowedOrigins("*");

    }

}
