package lms.websocket;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lms.model.RegistrovaniKorisnik;
import lms.model.RegistrovaniKorisnikRola;
import lms.model.Rola;

@Configuration
public class SocketHandlerConf {
    @Bean
    public SocketHandler<RegistrovaniKorisnik, Long> getRegistrovaniKorisnikSocketHandler() {
	return new SocketHandler<RegistrovaniKorisnik, Long>("/registrovani_korisnik_ws");
    }

    @Bean
    public SocketHandler<RegistrovaniKorisnikRola, Long> getRegistrovaniKorisnikRolaSocketHandler() {
	return new SocketHandler<RegistrovaniKorisnikRola, Long>("/registrovani_korisnik_rola_ws");
    }

    @Bean
    public SocketHandler<Rola, Long> getRolaSocketHandler() {
	return new SocketHandler<Rola, Long>("/rola_ws");
    }
}