package lms.korisnik.microservice.controller;

import java.util.Optional;

import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import lms.controller.CrudController;
import lms.dto.RolaDTO;
import lms.model.Rola;
import lms.websocket.WSMessageKeys;

@Controller
@Scope("singleton")
@RequestMapping(path = "rola")
@CrossOrigin(origins = "*")
public class RolaController extends CrudController<Rola, Long> {
    
    @Override
    @Secured({"ROLE_ADMINISTRATOR"})
    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<?> create(@RequestBody Rola model) {	
//	check for id
	if (model.getId() != null) {
//		id exists, lets try to find model with given id
	    Optional<Rola> t = this.service.findOne(model.getId());

	    if (t.isEmpty()) {
//			we didn't find model, lets create one and return DTO
		model = this.service.save(model);
		this.socketHandler.sendObjectAsMessage(WSMessageKeys.ON_ENTITY_CREATED, model.getDTO());
		return new ResponseEntity<RolaDTO>(model.getDTO(), HttpStatus.CREATED);
	    } else {
//			model is found, lets return HttpStatus.CONFLICT
		return new ResponseEntity<Void>(HttpStatus.CONFLICT);
	    }
	} else {
//		id doesn't exist, so model doesn't exist lets create one and return DTO
	    model = this.service.save(model);
	    this.socketHandler.sendObjectAsMessage(WSMessageKeys.ON_ENTITY_CREATED, model.getDTO());
	    return new ResponseEntity<RolaDTO>(model.getDTO(), HttpStatus.CREATED);
	}
    }

}
