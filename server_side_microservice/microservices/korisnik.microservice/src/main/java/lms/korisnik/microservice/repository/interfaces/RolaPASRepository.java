package lms.korisnik.microservice.repository.interfaces;

import java.util.ArrayList;

import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import lms.model.Rola;

@Repository
@Scope("singleton")
public interface RolaPASRepository extends PagingAndSortingRepository<Rola, Long> {
    
    @Query("select r from Rola r where r.naziv = ?1")
    public ArrayList<Rola> findByNaziv (@Param("naziv") String naziv);

}
