package lms.korisnik.microservice.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.RegistrovaniKorisnikRola;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class RegistrovaniKorisnikRolaService extends CrudService<RegistrovaniKorisnikRola, Long> {

}
