package lms.korisnik.microservice.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.core.JsonProcessingException;

import lms.controller.CrudController;
import lms.dto.RegistrovaniKorisnikDTO;
import lms.dto.interfaces.DTO;
import lms.enums.StanjeModela;
import lms.hbsf.HibernateSessionFactory;
import lms.korisnik.microservice.service.RegistrovaniKorisnikRolaService;
import lms.korisnik.microservice.service.RegistrovaniKorisnikService;
import lms.korisnik.microservice.service.RolaService;
import lms.model.RegistrovaniKorisnik;
import lms.model.RegistrovaniKorisnikRola;
import lms.model.Rola;
import lms.model.interfaces.Entitet;
import lms.security.service.SecurityService;
import lms.security.utils.TokenUtils;
import lms.websocket.SocketHandler;
import lms.websocket.WSMessageKeys;

@Controller
@Scope("singleton")
@RequestMapping(path = "registrovani_korisnik")
@CrossOrigin(origins = "*")
public class RegistrovaniKorisnikController extends CrudController<RegistrovaniKorisnik, Long> {

    @Autowired
    private HibernateSessionFactory hbsf;
    
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private TokenUtils tokenUtils;

    @Autowired
    @Qualifier("securityService")
    private SecurityService securityService;

    @Autowired
    private RolaService rolaService;

    @Autowired
    private RegistrovaniKorisnikRolaService registrovaniKorisnikRolaService;
    
    @Autowired
    private SocketHandler<Rola, Long> rolaSocketHandler;
    
    @Autowired
    private SocketHandler<RegistrovaniKorisnikRola, Long> registrovaniKorisnikRolaSocketHandler;

    @Override
    @RequestMapping(path = "", method = RequestMethod.POST)
//    @Secured({"ROLE_ADMINISTRATOR", "ROLE_STUDENTSKI_SLUZBENIK"})
    public ResponseEntity<?> create(@RequestBody RegistrovaniKorisnik registrovaniKorisnik) {

	ArrayList<Rola> r = this.rolaService.findByNaziv("ROLE_REGISTROVANI_KORISNIK");

	Rola rola = null;
	if (r.isEmpty()) {
	    rola = new Rola();
	    rola.setNaziv("ROLE_REGISTROVANI_KORISNIK");
	    rola = this.rolaService.save(rola);
	    this.rolaSocketHandler.sendObjectAsMessage(WSMessageKeys.ON_ENTITY_CREATED, rola.getDTO());
	} else {
	    rola = r.get(0);
	}

//	check for id
	if (registrovaniKorisnik.getId() != null) {
//		id exists, lets try to find model with given id
	    Optional<RegistrovaniKorisnik> t = this.service.findOne(registrovaniKorisnik.getId());

	    if (t.isEmpty()) {
//			we didn't find model, lets create one and return DTO
		registrovaniKorisnik.setLozinka(passwordEncoder.encode(registrovaniKorisnik.getLozinka()));
		registrovaniKorisnik = this.service.save(registrovaniKorisnik);
		this.socketHandler.sendObjectAsMessage(WSMessageKeys.ON_ENTITY_CREATED, registrovaniKorisnik.getDTO());

		RegistrovaniKorisnikRola registrovaniKorisnikRola = new RegistrovaniKorisnikRola(registrovaniKorisnik,
			rola);
		registrovaniKorisnikRola = this.registrovaniKorisnikRolaService.save(registrovaniKorisnikRola);
		this.registrovaniKorisnikRolaSocketHandler.sendObjectAsMessage(WSMessageKeys.ON_ENTITY_CREATED,
			registrovaniKorisnikRola.getDTO());

		registrovaniKorisnik.getRegistrovaniKorisnikRole().add(registrovaniKorisnikRola);

		registrovaniKorisnik = this.service.save(registrovaniKorisnik);
		this.socketHandler.sendObjectAsMessage(WSMessageKeys.ON_ENTITY_UPDATED, registrovaniKorisnik.getDTO());
		return new ResponseEntity<RegistrovaniKorisnikDTO>(registrovaniKorisnik.getDTO(), HttpStatus.CREATED);
	    } else {
//			model is found, lets return HttpStatus.CONFLICT
		return new ResponseEntity<Void>(HttpStatus.CONFLICT);
	    }
	} else {
//		id doesn't exist, so model doesn't exist lets create one and return DTO
	    registrovaniKorisnik.setLozinka(passwordEncoder.encode(registrovaniKorisnik.getLozinka()));
	    registrovaniKorisnik = this.service.save(registrovaniKorisnik);
	    this.socketHandler.sendObjectAsMessage(WSMessageKeys.ON_ENTITY_CREATED, registrovaniKorisnik.getDTO());

	    RegistrovaniKorisnikRola registrovaniKorisnikRola = new RegistrovaniKorisnikRola(registrovaniKorisnik,
		    rola);
	    registrovaniKorisnikRola = this.registrovaniKorisnikRolaService.save(registrovaniKorisnikRola);
	    this.registrovaniKorisnikRolaSocketHandler.sendObjectAsMessage(WSMessageKeys.ON_ENTITY_CREATED, registrovaniKorisnikRola.getDTO());

	    registrovaniKorisnik.getRegistrovaniKorisnikRole().add(registrovaniKorisnikRola);
	    registrovaniKorisnik = this.service.save(registrovaniKorisnik);
	    this.socketHandler.sendObjectAsMessage(WSMessageKeys.ON_ENTITY_UPDATED, registrovaniKorisnik.getDTO());
	    return new ResponseEntity<RegistrovaniKorisnikDTO>(registrovaniKorisnik.getDTO(), HttpStatus.CREATED);
	}
    }

    @RequestMapping(path = "/login", method = RequestMethod.POST)
    public ResponseEntity<HashMap<String, String>> login(@RequestBody RegistrovaniKorisnik registrovaniKorisnik) {

	Session session = null;
	try {
	    String lozinka = registrovaniKorisnik.getLozinka();

	    session = this.hbsf.getNewSession();
	    session.getTransaction().begin();

	    Query<RegistrovaniKorisnik> query = session.createQuery(
		    "select r from RegistrovaniKorisnik r where r.korisnickoIme = ?1", RegistrovaniKorisnik.class);
	    query.setParameter(1, registrovaniKorisnik.getKorisnickoIme());
	    registrovaniKorisnik = query.getSingleResult();

	    /**
	     * kreiramo specificniju autentifikaciju(UsernamePasswordAuthenticationToken,
	     * koja implementira interfejs -> Authentication)
	     */
	    UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
		    registrovaniKorisnik.getKorisnickoIme(), lozinka);

	    /**
	     * authenticationManager(AuthenticationManager) sluzi da autentifikuje neku
	     * autentifikaciju koju mu mi prosledimo
	     * 
	     * vraca najopstiji oblik, Authentication, autentifikacije u koje sve
	     * implementacije(konkretnije implmentacije interfejs-a Authentication) mogu da
	     * se svrstaju
	     */
	    Authentication authentication = authenticationManager.authenticate(token);
	    /**
	     * SecurityContextHolder je vezan za thread, mi stoga dohvatamo njegov sadrzaj i
	     * setujemo nasu autentifikaciju koju je vratio authenticationManager
	     * 
	     * SecurityContextHolder podesen je da radi na globalnom nivou zbog anotacije
	     * (@EnableGlobalMethodSecurity(securedEnabled = true) u
	     * rs.ac.singidunum.banka.security.SecurityConfiguration)
	     */
	    SecurityContextHolder.getContext().setAuthentication(authentication);

	    /**
	     * UserDetailsService je interfejs koji sluzi za dobavljanje podataka o user-u,
	     * izgleda da ce ovde ipak biti injektovana nasa implementacija tog interfejsa
	     * (rs.ac.singidunum.banka.services.UserDetailsServiceImpl)
	     */
	    UserDetails details = securityService.loadUserByUsername(registrovaniKorisnik.getKorisnickoIme());
	    String userToken = tokenUtils.generateToken(details);

	    HashMap<String, String> data = new HashMap<String, String>();
	    data.put("token", userToken);

	    session.getTransaction().commit();
	    session.close();
	    return new ResponseEntity<HashMap<String, String>>(data, HttpStatus.OK);

	} catch (Exception e) {
	    if (session != null) {
		session.getTransaction().commit();
		session.close();
	    }
	    return new ResponseEntity<HashMap<String, String>>(HttpStatus.UNAUTHORIZED);
	}
    }

    @RequestMapping(path = "/nastavnik/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> findOneByNastavnikId(@PathVariable("id") Long id) {
	RegistrovaniKorisnik t = ((RegistrovaniKorisnikService) this.service).findOneByNastavnikId(id);

	if (t != null)
	    return new ResponseEntity<DTO<Long>>(t.getDTO(), HttpStatus.OK);
	else
	    return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
    }
    
    @RequestMapping(path = "/korisnickoIme/{korisnickoIme}", method = RequestMethod.GET)
    public ResponseEntity<?> findOneByNastavnikId(@PathVariable("korisnickoIme") String korisnickoIme) {
	RegistrovaniKorisnik t = ((RegistrovaniKorisnikService) this.service).findOneByKorisnickoIme(korisnickoIme);

	if (t != null)
	    return new ResponseEntity<DTO<Long>>(t.getDTO(), HttpStatus.OK);
	else
	    return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/aktiviraj", method = RequestMethod.POST)
    @Secured({"ROLE_ADMINISTRATOR", "ROLE_STUDENTSKI_SLUZBENIK"})
    public ResponseEntity<?> activate(@RequestBody RegistrovaniKorisnik model) throws JsonProcessingException, IOException {
//	check for id
	if (model.getId() != null) {
//		id exists, lets try to find model with given id
	    Optional<RegistrovaniKorisnik> t = this.service.findOne(model.getId());

	    if (t.isEmpty()) {
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	    } else {
		RegistrovaniKorisnik tModel = t.get();
		tModel.setStanje(StanjeModela.AKTIVAN);
		tModel = this.service.save(tModel);
		this.socketHandler.sendObjectAsMessage(WSMessageKeys.ON_ENTITY_UPDATED, tModel.getDTO());
		return new ResponseEntity<DTO<Long>>(tModel.getDTO(), HttpStatus.OK);
	    }
	} else {
	    return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
    }

    @RequestMapping(path = "/deaktiviraj", method = RequestMethod.POST)
    @Secured({"ROLE_ADMINISTRATOR", "ROLE_STUDENTSKI_SLUZBENIK"})
    public ResponseEntity<?> deactivate(@RequestBody RegistrovaniKorisnik model) throws JsonProcessingException, IOException {
//	check for id
	if (model.getId() != null) {
//		id exists, lets try to find model with given id
	    Optional<RegistrovaniKorisnik> t = this.service.findOne(model.getId());

	    if (t.isEmpty()) {
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	    } else {
		RegistrovaniKorisnik tModel = t.get();
		tModel.setStanje(StanjeModela.NEAKTIVAN);
		tModel = this.service.save(tModel);
		this.socketHandler.sendObjectAsMessage(WSMessageKeys.ON_ENTITY_UPDATED, tModel.getDTO());
		return new ResponseEntity<DTO<Long>>(tModel.getDTO(), HttpStatus.OK);
	    }
	} else {
	    return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
    }

    @RequestMapping(path = "", method = RequestMethod.PUT)
    @Secured({"ROLE_ADMINISTRATOR", "ROLE_STUDENTSKI_SLUZBENIK", "ROLE_REGISTROVANI_KORISNIK"})
    public ResponseEntity<?> update(@RequestBody RegistrovaniKorisnik model) throws JsonProcessingException, IOException {
//	check for id
	if (model.getId() != null) {
//		id exists, lets try to find model
	    Optional<RegistrovaniKorisnik> t = this.service.findOne(model.getId());

	    if (t.isPresent()) {
//			model is found, lets update it and return DTO
		RegistrovaniKorisnik tModel = t.get();
		tModel.update(model);
		tModel = this.service.save(tModel);
		this.socketHandler.sendObjectAsMessage(WSMessageKeys.ON_ENTITY_UPDATED, tModel.getDTO());
		return new ResponseEntity<DTO<Long>>(tModel.getDTO(), HttpStatus.OK);
	    } else {
//			we didn't find model, lets return HttpStatus.NOT_FOUND
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	    }
	} else {
//		id doesn't exists, let return HttpStatus.NOT_FOUND
	    return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    @Secured({"ROLE_ADMINISTRATOR", "ROLE_STUDENTSKI_SLUZBENIK"})
    public ResponseEntity<?> delete(@PathVariable("id") Long id) throws JsonProcessingException, IOException {
//	try to find model
	Optional<RegistrovaniKorisnik> t = this.service.findOne(id);

	if (t.isPresent()) {
//		model is found, lets delete it and return HttpStatus.OK
	    Entitet<RegistrovaniKorisnik, Long> tModel = t.get();
	    tModel.setStanje(StanjeModela.OBRISAN);
	    DTO<Long> dto = tModel.getDTO();
	    this.service.delete(t.get());
	    this.socketHandler.sendObjectAsMessage(WSMessageKeys.ON_ENTITY_DELETED, dto);
	    return new ResponseEntity<Void>(HttpStatus.OK);
	} else {
//		we didn't find model, lets return HttpStatus.NOT_FOUND
	    return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
    }

    @RequestMapping(path = "", method = RequestMethod.DELETE)
    @Secured({"ROLE_ADMINISTRATOR", "ROLE_STUDENTSKI_SLUZBENIK"})
    public ResponseEntity<?> delete(@RequestBody RegistrovaniKorisnik model) throws JsonProcessingException, IOException {
//	check for id
	if (model.getId() != null) {
//		id exists, lets try to find model
	    Optional<RegistrovaniKorisnik> t = this.service.findOne(model.getId());

	    if (t.isPresent()) {
//			model is found, lets delete it and return HttpStatus.OK
		Entitet<RegistrovaniKorisnik, Long> tModel = t.get();
		tModel.setStanje(StanjeModela.OBRISAN);
		DTO<Long> dto = tModel.getDTO();
		this.service.delete(t.get());
		this.socketHandler.sendObjectAsMessage(WSMessageKeys.ON_ENTITY_DELETED, dto);
		return new ResponseEntity<Void>(HttpStatus.OK);
	    } else {
//			we didn't find model, lets return HttpStatus.NOT_FOUND
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	    }
	} else {
//		id doesn't exists, let return HttpStatus.NOT_FOUND
	    return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
    }
}