package lms.korisnik.microservice.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.RegistrovaniKorisnik;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class RegistrovaniKorisnikRepository extends CrudRepository<RegistrovaniKorisnik, Long> {

}