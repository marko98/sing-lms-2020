package lms.korisnik.microservice.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.RegistrovaniKorisnikRola;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class RegistrovaniKorisnikRolaRepository extends CrudRepository<RegistrovaniKorisnikRola, Long> {

}
