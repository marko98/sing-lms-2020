package lms.korisnik.microservice.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.RegistrovaniKorisnikRola;

@Controller
@Scope("singleton")
@RequestMapping(path = "registrovani_korisnik_rola")
@CrossOrigin(origins = "*")
public class RegistrovaniKorisnikRolaController extends CrudController<RegistrovaniKorisnikRola, Long> {

}
