package lms.korisnik.microservice.service;

import java.util.ArrayList;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import lms.korisnik.microservice.repository.interfaces.RolaPASRepository;
import lms.model.Rola;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class RolaService extends CrudService<Rola, Long> {

    public ArrayList<Rola> findByNaziv(@Param("naziv") String naziv) {
	return ((RolaPASRepository) this.repository.getRepository()).findByNaziv(naziv);
    }

}
