package lms.korisnik.microservice.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.RegistrovaniKorisnik;

@Repository
@Scope("singleton")
public interface RegistrovaniKorisnikPASRepository extends PagingAndSortingRepository<RegistrovaniKorisnik, Long> {

    @Query("select rk from RegistrovaniKorisnik rk where rk.nastavnik.id = ?1")
    public RegistrovaniKorisnik findOneByNastavnikId(Long id);
    
    @Query("select rk from RegistrovaniKorisnik rk where rk.korisnickoIme = ?1")
    public RegistrovaniKorisnik findOneByKorisnickoIme(String korisnickoIme);
    
}