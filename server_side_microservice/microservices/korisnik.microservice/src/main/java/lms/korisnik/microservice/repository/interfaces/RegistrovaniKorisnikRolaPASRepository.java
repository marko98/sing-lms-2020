package lms.korisnik.microservice.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.RegistrovaniKorisnikRola;

@Repository
@Scope("singleton")
public interface RegistrovaniKorisnikRolaPASRepository
	extends PagingAndSortingRepository<RegistrovaniKorisnikRola, Long> {

}
