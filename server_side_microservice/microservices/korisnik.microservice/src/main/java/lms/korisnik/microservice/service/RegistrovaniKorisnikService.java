package lms.korisnik.microservice.service;

import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.korisnik.microservice.repository.interfaces.RegistrovaniKorisnikPASRepository;
import lms.model.RegistrovaniKorisnik;
import lms.service.CrudService;

@Service
@Primary
@Scope("singleton")
public class RegistrovaniKorisnikService extends CrudService<RegistrovaniKorisnik, Long>{
    public RegistrovaniKorisnik findOneByNastavnikId(Long id) {
	return ((RegistrovaniKorisnikPASRepository) this.repository.getRepository())
		.findOneByNastavnikId(id);
    }
    
    public RegistrovaniKorisnik findOneByKorisnickoIme(String korisnickoIme) {
	return ((RegistrovaniKorisnikPASRepository) this.repository.getRepository())
		.findOneByKorisnickoIme(korisnickoIme);
    }
}