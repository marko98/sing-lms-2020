package lms.student.microservice.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.IstrazivackiRadStudentNaStudiji;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class IstrazivackiRadStudentNaStudijiRepository extends CrudRepository<IstrazivackiRadStudentNaStudiji, Long> {

}