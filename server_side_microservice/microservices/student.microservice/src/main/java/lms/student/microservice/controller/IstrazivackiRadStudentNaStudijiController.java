package lms.student.microservice.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.IstrazivackiRadStudentNaStudiji;

@Controller
@Scope("singleton")
@RequestMapping(path = "istrazivacki_rad_student_na_studiji")
@CrossOrigin(origins = "*")
public class IstrazivackiRadStudentNaStudijiController extends CrudController<IstrazivackiRadStudentNaStudiji, Long> {

}