package lms.student.microservice.service;

import java.util.ArrayList;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import lms.model.Rola;
import lms.service.CrudService;
import lms.student.microservice.repository.interfaces.RolaPASRepository;

@Service
@Scope("singleton")
public class RolaService extends CrudService<Rola, Long> {

    public ArrayList<Rola> findByNaziv(@Param("naziv") String naziv) {
	return ((RolaPASRepository) this.repository.getRepository()).findByNaziv(naziv);
    }

}
