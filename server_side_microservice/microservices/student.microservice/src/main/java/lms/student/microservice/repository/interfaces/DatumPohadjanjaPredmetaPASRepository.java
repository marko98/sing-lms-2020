package lms.student.microservice.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.DatumPohadjanjaPredmeta;

@Repository
@Scope("singleton")
public interface DatumPohadjanjaPredmetaPASRepository
	extends PagingAndSortingRepository<DatumPohadjanjaPredmeta, Long> {

}