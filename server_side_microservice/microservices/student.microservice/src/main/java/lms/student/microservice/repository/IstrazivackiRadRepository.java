package lms.student.microservice.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.IstrazivackiRad;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class IstrazivackiRadRepository extends CrudRepository<IstrazivackiRad, Long> {

}