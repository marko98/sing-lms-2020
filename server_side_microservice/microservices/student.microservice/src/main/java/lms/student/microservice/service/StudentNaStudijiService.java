package lms.student.microservice.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.StudentNaStudiji;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class StudentNaStudijiService extends CrudService<StudentNaStudiji, Long> {

}