package lms.student.microservice.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.DiplomskiRad;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class DiplomskiRadRepository extends CrudRepository<DiplomskiRad, Long> {

}