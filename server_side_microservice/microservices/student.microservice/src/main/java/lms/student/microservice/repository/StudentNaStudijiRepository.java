package lms.student.microservice.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.StudentNaStudiji;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class StudentNaStudijiRepository extends CrudRepository<StudentNaStudiji, Long> {

}