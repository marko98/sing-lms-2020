package lms.student.microservice.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.DiplomskiRad;

@Controller
@Scope("singleton")
@RequestMapping(path = "diplomski_rad")
@CrossOrigin(origins = "*")
public class DiplomskiRadController extends CrudController<DiplomskiRad, Long> {

}