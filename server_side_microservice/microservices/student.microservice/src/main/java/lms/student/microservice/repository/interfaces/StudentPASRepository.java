package lms.student.microservice.repository.interfaces;


import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.Student;

@Repository
@Scope("singleton")
public interface StudentPASRepository extends PagingAndSortingRepository<Student, Long> {
    @Query("select s from Student s where s.registrovaniKorisnik.id = ?1")
    public Student findOneByRegistrovaniKorisnikId(Long id);
}