package lms.student.microservice.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.Student;
import lms.service.CrudService;
import lms.student.microservice.repository.interfaces.StudentPASRepository;

@Service
@Scope("singleton")
public class StudentService extends CrudService<Student, Long> {

    public Student findOneByRegistrovaniKorisnikId(Long id) {
	return ((StudentPASRepository)this.repository.getRepository()).findOneByRegistrovaniKorisnikId(id);
    }
    
}