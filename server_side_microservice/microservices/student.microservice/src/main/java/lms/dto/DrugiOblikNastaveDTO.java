package lms.dto;

import java.time.LocalDateTime;

import lms.dto.interfaces.DTO;
import lms.enums.StanjeModela;
import lms.enums.TipDrugogOblikaNastave;

public class DrugiOblikNastaveDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 1512563448422186226L;

    private Long id;

    private LocalDateTime datum;

    private StanjeModela stanje;

    private Long version;

    private TipDrugogOblikaNastave tipDrugogOblikaNastave;

    private RealizacijaPredmetaDTO realizacijaPredmetaDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public LocalDateTime getDatum() {
	return this.datum;
    }

    public void setDatum(LocalDateTime datum) {
	this.datum = datum;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public TipDrugogOblikaNastave getTipDrugogOblikaNastave() {
	return this.tipDrugogOblikaNastave;
    }

    public void setTipDrugogOblikaNastave(TipDrugogOblikaNastave tipDrugogOblikaNastave) {
	this.tipDrugogOblikaNastave = tipDrugogOblikaNastave;
    }

    public RealizacijaPredmetaDTO getRealizacijaPredmetaDTO() {
	return this.realizacijaPredmetaDTO;
    }

    public void setRealizacijaPredmetaDTO(RealizacijaPredmetaDTO realizacijaPredmetaDTO) {
	this.realizacijaPredmetaDTO = realizacijaPredmetaDTO;
    }

    public DrugiOblikNastaveDTO() {
	super();
    }

}