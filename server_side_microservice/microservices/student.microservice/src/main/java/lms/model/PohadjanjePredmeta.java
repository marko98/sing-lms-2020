package lms.model;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.DatumPohadjanjaPredmetaDTO;
import lms.dto.PohadjanjePredmetaDTO;
import lms.dto.PolaganjeDTO;
import lms.enums.StanjeModela;
import lms.interfaces.Identifikacija;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE pohadjanje_predmeta SET stanje = 'OBRISAN' WHERE pohadjanje_predmeta_id = ? AND version = ? ", check = ResultCheckStyle.COUNT)
@Table(name = "pohadjanje_predmeta")
@Transactional
public class PohadjanjePredmeta implements Entitet<PohadjanjePredmeta, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 826299093549933411L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "pohadjanje_predmeta_id")
    private Long id;

    private Integer konacnaOcena;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "student_na_studiji_id")
    private StudentNaStudiji studentNaStudiji;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "realizacija_predmeta_id")
    private RealizacijaPredmeta realizacijaPredmeta;

    @javax.persistence.OneToMany(mappedBy = "pohadjanjePredmeta")
    private java.util.List<Polaganje> polaganja;

    @javax.persistence.OneToMany(mappedBy = "pohadjanjePredmeta")
    private java.util.List<DatumPohadjanjaPredmeta> prisustvo;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Integer getKonacnaOcena() {
	return this.konacnaOcena;
    }

    public void setKonacnaOcena(Integer konacnaOcena) {
	this.konacnaOcena = konacnaOcena;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public StudentNaStudiji getStudentNaStudiji() {
	return this.studentNaStudiji;
    }

    public void setStudentNaStudiji(StudentNaStudiji studentNaStudiji) {
	this.studentNaStudiji = studentNaStudiji;
    }

    public RealizacijaPredmeta getRealizacijaPredmeta() {
	return this.realizacijaPredmeta;
    }

    public void setRealizacijaPredmeta(RealizacijaPredmeta realizacijaPredmeta) {
	this.realizacijaPredmeta = realizacijaPredmeta;
    }

    public java.util.List<Polaganje> getPolaganja() {
	return this.polaganja;
    }

    public void setPolaganja(java.util.List<Polaganje> polaganja) {
	this.polaganja = polaganja;
    }

    public java.util.List<DatumPohadjanjaPredmeta> getPrisustvo() {
	return this.prisustvo;
    }

    public void setPrisustvo(java.util.List<DatumPohadjanjaPredmeta> prisustvo) {
	this.prisustvo = prisustvo;
    }

    public PohadjanjePredmeta() {
	super();
	this.polaganja = new java.util.ArrayList<>();
	this.prisustvo = new java.util.ArrayList<>();
    }

    public Polaganje getPolaganje(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addPolaganje(Polaganje polaganje) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removePolaganje(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public DatumPohadjanjaPredmeta getDatumPohadjanjaPredmeta(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addDatumPohadjanjaPredmeta(DatumPohadjanjaPredmeta datumPohadjanjaPredmeta) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeDatumPohadjanjaPredmeta(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((konacnaOcena == null) ? 0 : konacnaOcena.hashCode());
	result = prime * result + ((polaganja == null) ? 0 : polaganja.hashCode());
	result = prime * result + ((prisustvo == null) ? 0 : prisustvo.hashCode());
	result = prime * result + ((realizacijaPredmeta == null) ? 0 : realizacijaPredmeta.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((studentNaStudiji == null) ? 0 : studentNaStudiji.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	PohadjanjePredmeta other = (PohadjanjePredmeta) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (konacnaOcena == null) {
	    if (other.konacnaOcena != null)
		return false;
	} else if (!konacnaOcena.equals(other.konacnaOcena))
	    return false;
	if (polaganja == null) {
	    if (other.polaganja != null)
		return false;
	} else if (!polaganja.equals(other.polaganja))
	    return false;
	if (prisustvo == null) {
	    if (other.prisustvo != null)
		return false;
	} else if (!prisustvo.equals(other.prisustvo))
	    return false;
	if (realizacijaPredmeta == null) {
	    if (other.realizacijaPredmeta != null)
		return false;
	} else if (!realizacijaPredmeta.equals(other.realizacijaPredmeta))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (studentNaStudiji == null) {
	    if (other.studentNaStudiji != null)
		return false;
	} else if (!studentNaStudiji.equals(other.studentNaStudiji))
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public PohadjanjePredmetaDTO getDTO() {
	PohadjanjePredmetaDTO pohadjanjePredmetaDTO = new PohadjanjePredmetaDTO();
	pohadjanjePredmetaDTO.setId(id);
	pohadjanjePredmetaDTO.setKonacnaOcena(konacnaOcena);
	pohadjanjePredmetaDTO.setStanje(stanje);
	pohadjanjePredmetaDTO.setVersion(version);

	if (this.realizacijaPredmeta != null)
	    pohadjanjePredmetaDTO.setRealizacijaPredmetaDTO(this.realizacijaPredmeta.getDTOinsideDTO());
	if (this.studentNaStudiji != null)
	    pohadjanjePredmetaDTO.setStudentNaStudijiDTO(this.studentNaStudiji.getDTOinsideDTO());

	ArrayList<PolaganjeDTO> polaganjaDTOs = new ArrayList<PolaganjeDTO>();
	for (Polaganje polaganje : this.polaganja) {
	    polaganjaDTOs.add(polaganje.getDTOinsideDTO());
	}
	pohadjanjePredmetaDTO.setPolaganjaDTO(polaganjaDTOs);

	ArrayList<DatumPohadjanjaPredmetaDTO> datumPohadjanjaPredmetaDTOs = new ArrayList<DatumPohadjanjaPredmetaDTO>();
	for (DatumPohadjanjaPredmeta datumPohadjanjaPredmeta : this.prisustvo) {
	    datumPohadjanjaPredmetaDTOs.add(datumPohadjanjaPredmeta.getDTOinsideDTO());
	}
	pohadjanjePredmetaDTO.setPrisustvoDTO(datumPohadjanjaPredmetaDTOs);

	return pohadjanjePredmetaDTO;
    }

    @Override
    public PohadjanjePredmetaDTO getDTOinsideDTO() {
	PohadjanjePredmetaDTO pohadjanjePredmetaDTO = new PohadjanjePredmetaDTO();
	pohadjanjePredmetaDTO.setId(id);
	pohadjanjePredmetaDTO.setKonacnaOcena(konacnaOcena);
	pohadjanjePredmetaDTO.setStanje(stanje);
	pohadjanjePredmetaDTO.setVersion(version);
	return pohadjanjePredmetaDTO;
    }

    @Override
    public void update(PohadjanjePredmeta entitet) {
	this.setKonacnaOcena(entitet.getKonacnaOcena());
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());

	this.setRealizacijaPredmeta(entitet.getRealizacijaPredmeta());
	this.setStudentNaStudiji(entitet.getStudentNaStudiji());

    }

}