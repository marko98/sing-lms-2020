package lms.model;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.PolaganjeDTO;
import lms.enums.StanjeModela;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE polaganje SET stanje = 'OBRISAN' WHERE polaganje_id = ? AND version = ? ", check = ResultCheckStyle.COUNT)
@Table(name = "polaganje")
@Transactional
public class Polaganje implements Entitet<Polaganje, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 2134716319782723188L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "polaganje_id")
    private Long id;

    private Integer bodovi;

    private String napomena;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "pohadjanje_predmeta_id")
    private PohadjanjePredmeta pohadjanjePredmeta;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "evaluacija_znanja_id")
    private EvaluacijaZnanja evaluacijaZnanja;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Integer getBodovi() {
	return this.bodovi;
    }

    public void setBodovi(Integer bodovi) {
	this.bodovi = bodovi;
    }

    public String getNapomena() {
	return this.napomena;
    }

    public void setNapomena(String napomena) {
	this.napomena = napomena;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public PohadjanjePredmeta getPohadjanjePredmeta() {
	return this.pohadjanjePredmeta;
    }

    public void setPohadjanjePredmeta(PohadjanjePredmeta pohadjanjePredmeta) {
	this.pohadjanjePredmeta = pohadjanjePredmeta;
    }

    public EvaluacijaZnanja getEvaluacijaZnanja() {
	return this.evaluacijaZnanja;
    }

    public void setEvaluacijaZnanja(EvaluacijaZnanja evaluacijaZnanja) {
	this.evaluacijaZnanja = evaluacijaZnanja;
    }

    public Polaganje() {
	super();
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((bodovi == null) ? 0 : bodovi.hashCode());
	result = prime * result + ((evaluacijaZnanja == null) ? 0 : evaluacijaZnanja.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((napomena == null) ? 0 : napomena.hashCode());
	result = prime * result + ((pohadjanjePredmeta == null) ? 0 : pohadjanjePredmeta.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Polaganje other = (Polaganje) obj;
	if (bodovi == null) {
	    if (other.bodovi != null)
		return false;
	} else if (!bodovi.equals(other.bodovi))
	    return false;
	if (evaluacijaZnanja == null) {
	    if (other.evaluacijaZnanja != null)
		return false;
	} else if (!evaluacijaZnanja.equals(other.evaluacijaZnanja))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (napomena == null) {
	    if (other.napomena != null)
		return false;
	} else if (!napomena.equals(other.napomena))
	    return false;
	if (pohadjanjePredmeta == null) {
	    if (other.pohadjanjePredmeta != null)
		return false;
	} else if (!pohadjanjePredmeta.equals(other.pohadjanjePredmeta))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public PolaganjeDTO getDTO() {
	PolaganjeDTO polaganjeDTO = new PolaganjeDTO();
	polaganjeDTO.setId(id);
	polaganjeDTO.setBodovi(bodovi);
	polaganjeDTO.setNapomena(napomena);
	polaganjeDTO.setStanje(stanje);
	polaganjeDTO.setVersion(version);

	if (this.evaluacijaZnanja != null)
	    polaganjeDTO.setEvaluacijaZnanjaDTO(this.evaluacijaZnanja.getDTOinsideDTO());
	if (this.pohadjanjePredmeta != null)
	    polaganjeDTO.setPohadjanjePredmetaDTO(this.pohadjanjePredmeta.getDTOinsideDTO());

	return polaganjeDTO;
    }

    @Override
    public PolaganjeDTO getDTOinsideDTO() {
	PolaganjeDTO polaganjeDTO = new PolaganjeDTO();
	polaganjeDTO.setId(id);
	polaganjeDTO.setBodovi(bodovi);
	polaganjeDTO.setNapomena(napomena);
	polaganjeDTO.setStanje(stanje);
	polaganjeDTO.setVersion(version);
	return polaganjeDTO;
    }

    @Override
    public void update(Polaganje entitet) {
	this.setBodovi(entitet.getBodovi());
	this.setNapomena(entitet.getNapomena());
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());

	this.setEvaluacijaZnanja(entitet.getEvaluacijaZnanja());
	this.setPohadjanjePredmeta(entitet.getPohadjanjePredmeta());

    }

}