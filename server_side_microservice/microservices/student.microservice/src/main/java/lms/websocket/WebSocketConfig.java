package lms.websocket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

import lms.model.DatumPohadjanjaPredmeta;
import lms.model.DiplomskiRad;
import lms.model.IstrazivackiRad;
import lms.model.IstrazivackiRadStudentNaStudiji;
import lms.model.PohadjanjePredmeta;
import lms.model.Student;
import lms.model.StudentNaStudiji;

@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

    @Autowired
    private SocketHandler<DatumPohadjanjaPredmeta, Long> datumPohadjanjaPredmeta;

    @Autowired
    private SocketHandler<DiplomskiRad, Long> diplomskiRad;

    @Autowired
    private SocketHandler<IstrazivackiRad, Long> istrazivackiRad;

    @Autowired
    private SocketHandler<IstrazivackiRadStudentNaStudiji, Long> istrazivackiRadStudentNaStudiji;

    @Autowired
    private SocketHandler<PohadjanjePredmeta, Long> pohadjanjePredmeta;

    @Autowired
    private SocketHandler<Student, Long> student;

    @Autowired
    private SocketHandler<StudentNaStudiji, Long> studentNaStudiji;

    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {

	registry.addHandler(datumPohadjanjaPredmeta, "/datum_pohadjanja_predmeta_ws").setAllowedOrigins("*");
	registry.addHandler(diplomskiRad, "/diplomski_rad_ws").setAllowedOrigins("*");
	registry.addHandler(istrazivackiRad, "/istrazivacki_rad_ws").setAllowedOrigins("*");
	registry.addHandler(istrazivackiRadStudentNaStudiji, "/istrazivacki_rad_student_na_studiji_ws")
		.setAllowedOrigins("*");
	registry.addHandler(pohadjanjePredmeta, "/pohadjanje_predmeta_ws").setAllowedOrigins("*");
	registry.addHandler(student, "/student_ws").setAllowedOrigins("*");
	registry.addHandler(studentNaStudiji, "/student_na_studiji_ws").setAllowedOrigins("*");

    }

}
