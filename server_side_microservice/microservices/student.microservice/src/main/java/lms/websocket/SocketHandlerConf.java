package lms.websocket;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lms.model.DatumPohadjanjaPredmeta;
import lms.model.DiplomskiRad;
import lms.model.IstrazivackiRad;
import lms.model.IstrazivackiRadStudentNaStudiji;
import lms.model.PohadjanjePredmeta;
import lms.model.Student;
import lms.model.StudentNaStudiji;

@Configuration
public class SocketHandlerConf {
    @Bean
    public SocketHandler<DatumPohadjanjaPredmeta, Long> getDatumPohadjanjaPredmetaSocketHandler() {
	return new SocketHandler<DatumPohadjanjaPredmeta, Long>("/datum_pohadjanja_predmeta_ws");
    }

    @Bean
    public SocketHandler<DiplomskiRad, Long> getDiplomskiRadSocketHandler() {
	return new SocketHandler<DiplomskiRad, Long>("/diplomski_rad_ws");
    }

    @Bean
    public SocketHandler<IstrazivackiRad, Long> getIstrazivackiRadSocketHandler() {
	return new SocketHandler<IstrazivackiRad, Long>("/istrazivacki_rad_ws");
    }
    
    @Bean
    public SocketHandler<IstrazivackiRadStudentNaStudiji, Long> getIstrazivackiRadStudentNaStudijiSocketHandler() {
	return new SocketHandler<IstrazivackiRadStudentNaStudiji, Long>("/istrazivacki_rad_student_na_studiji_ws");
    }
    
    @Bean
    public SocketHandler<PohadjanjePredmeta, Long> getPohadjanjePredmetaSocketHandler() {
	return new SocketHandler<PohadjanjePredmeta, Long>("/pohadjanje_predmeta_ws");
    }
    
    @Bean
    public SocketHandler<Student, Long> getStudentSocketHandler() {
	return new SocketHandler<Student, Long>("/student_ws");
    }
    
    @Bean
    public SocketHandler<StudentNaStudiji, Long> getStudentNaStudijiSocketHandler() {
	return new SocketHandler<StudentNaStudiji, Long>("/student_na_studiji_ws");
    }
}