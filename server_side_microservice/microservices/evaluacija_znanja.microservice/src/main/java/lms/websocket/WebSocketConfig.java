package lms.websocket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

import lms.model.EvaluacijaZnanja;
import lms.model.EvaluacijaZnanjaNacinEvaluacije;
import lms.model.Odgovor;
import lms.model.Pitanje;
import lms.model.Polaganje;

@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

    @Autowired
    private SocketHandler<EvaluacijaZnanja, Long> evaluacijaZnanja;

    @Autowired
    private SocketHandler<EvaluacijaZnanjaNacinEvaluacije, Long> evaluacijaZnanjaNacinEvaluacije;

    @Autowired
    private SocketHandler<Odgovor, Long> odgovor;

    @Autowired
    private SocketHandler<Pitanje, Long> pitanje;

    @Autowired
    private SocketHandler<Polaganje, Long> polaganje;

    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {

	registry.addHandler(evaluacijaZnanja, "/evaluacija_znanja_ws").setAllowedOrigins("*");

	registry.addHandler(evaluacijaZnanjaNacinEvaluacije, "/evaluacija_znanja_nacin_evaluacije_ws")
		.setAllowedOrigins("*");

	registry.addHandler(odgovor, "/odgovor_ws").setAllowedOrigins("*");
	registry.addHandler(pitanje, "/pitanje_ws").setAllowedOrigins("*");
	registry.addHandler(polaganje, "/polaganje_ws").setAllowedOrigins("*");

    }

}
