package lms.websocket;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lms.model.EvaluacijaZnanja;
import lms.model.EvaluacijaZnanjaNacinEvaluacije;
import lms.model.Odgovor;
import lms.model.Pitanje;
import lms.model.Polaganje;

@Configuration
public class SocketHandlerConf {
    @Bean
    public SocketHandler<EvaluacijaZnanja, Long> getEvaluacijaZnanjaSocketHandler() {
	return new SocketHandler<EvaluacijaZnanja, Long>("/evaluacija_znanja_ws");
    }

    @Bean
    public SocketHandler<EvaluacijaZnanjaNacinEvaluacije, Long> getEvaluacijaZnanjaNacinEvaluacijeSocketHandler() {
	return new SocketHandler<EvaluacijaZnanjaNacinEvaluacije, Long>("/evaluacija_znanja_nacin_evaluacije_ws");
    }

    @Bean
    public SocketHandler<Odgovor, Long> getOdgovorSocketHandler() {
	return new SocketHandler<Odgovor, Long>("/odgovor_ws");
    }

    @Bean
    public SocketHandler<Pitanje, Long> getPitanjeSocketHandler() {
	return new SocketHandler<Pitanje, Long>("/pitanje_ws");
    }

    @Bean
    public SocketHandler<Polaganje, Long> getPolaganjeSocketHandler() {
	return new SocketHandler<Polaganje, Long>("/polaganje_ws");
    }
}