package lms.dto;

import lms.dto.interfaces.DTO;
import lms.enums.StanjeModela;

public class OdgovorDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 5534996348914323323L;

    private Long id;

    private String sadrzaj;

    private StanjeModela stanje;

    private Long version;

    private PitanjeDTO pitanjeDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getSadrzaj() {
	return this.sadrzaj;
    }

    public void setSadrzaj(String sadrzaj) {
	this.sadrzaj = sadrzaj;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public PitanjeDTO getPitanjeDTO() {
	return this.pitanjeDTO;
    }

    public void setPitanjeDTO(PitanjeDTO pitanjeDTO) {
	this.pitanjeDTO = pitanjeDTO;
    }

    public OdgovorDTO() {
	super();
    }

}