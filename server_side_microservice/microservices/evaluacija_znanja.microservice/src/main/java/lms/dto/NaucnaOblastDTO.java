package lms.dto;

import lms.dto.interfaces.DTO;
import lms.enums.StanjeModela;

public class NaucnaOblastDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -3223073976027812862L;

    private Long id;

    private String naziv;

    private StanjeModela stanje;

    private Long version;

    private java.util.List<TitulaDTO> titule;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getNaziv() {
	return this.naziv;
    }

    public void setNaziv(String naziv) {
	this.naziv = naziv;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public java.util.List<TitulaDTO> getTitule() {
	return this.titule;
    }

    public void setTitule(java.util.List<TitulaDTO> titule) {
	this.titule = titule;
    }

    public NaucnaOblastDTO() {
	super();
	this.titule = new java.util.ArrayList<>();
    }

}