package lms.dto;

import lms.dto.interfaces.DTO;
import lms.enums.StanjeModela;
import lms.interfaces.Identifikacija;

public class IstrazivackiRadDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -85698961367176844L;

    private Long id;

    private String tema;

    private StanjeModela stanje;

    private Long version;

    private NastavnikDTO nastavnikDTO;

    private RealizacijaPredmetaDTO realizacijaPredmetaDTO;

    private java.util.List<IstrazivackiRadStudentNaStudijiDTO> istrazivackiRadStudentiNaStudijamaDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getTema() {
	return this.tema;
    }

    public void setTema(String tema) {
	this.tema = tema;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public NastavnikDTO getNastavnikDTO() {
	return this.nastavnikDTO;
    }

    public void setNastavnikDTO(NastavnikDTO nastavnikDTO) {
	this.nastavnikDTO = nastavnikDTO;
    }

    public RealizacijaPredmetaDTO getRealizacijaPredmetaDTO() {
	return this.realizacijaPredmetaDTO;
    }

    public void setRealizacijaPredmetaDTO(RealizacijaPredmetaDTO realizacijaPredmetaDTO) {
	this.realizacijaPredmetaDTO = realizacijaPredmetaDTO;
    }

    public java.util.List<IstrazivackiRadStudentNaStudijiDTO> getIstrazivackiRadStudentiNaStudijamaDTO() {
	return this.istrazivackiRadStudentiNaStudijamaDTO;
    }

    public void setIstrazivackiRadStudentiNaStudijamaDTO(
	    java.util.List<IstrazivackiRadStudentNaStudijiDTO> istrazivackiRadStudentiNaStudijamaDTO) {
	this.istrazivackiRadStudentiNaStudijamaDTO = istrazivackiRadStudentiNaStudijamaDTO;
    }

    public IstrazivackiRadDTO() {
	super();
	this.istrazivackiRadStudentiNaStudijamaDTO = new java.util.ArrayList<>();
    }

    public IstrazivackiRadStudentNaStudijiDTO getIstrazivackiRadStudentNaStudijiDTO(
	    Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addIstrazivackiRadStudentNaStudijiDTO(
	    IstrazivackiRadStudentNaStudijiDTO istrazivackiRadStudentNaStudijiDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeIstrazivackiRadStudentNaStudijiDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

}