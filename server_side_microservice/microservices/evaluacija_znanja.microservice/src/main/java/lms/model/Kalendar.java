package lms.model;

import java.time.LocalDateTime;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.DogadjajKalendarDTO;
import lms.dto.KalendarDTO;
import lms.enums.StanjeModela;
import lms.enums.TipStudija;
import lms.interfaces.Identifikacija;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE kalendar SET stanje = 'OBRISAN' WHERE kalendar_id = ? AND version = ? ", check = ResultCheckStyle.COUNT)
@Table(name = "kalendar")
@Transactional
public class Kalendar implements Entitet<Kalendar, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 2488432338931738207L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "kalendar_id")
    private Long id;

    private LocalDateTime pocetniDatum;

    private LocalDateTime krajnjiDatum;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @Enumerated(EnumType.STRING)
    private TipStudija tipStudija;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "univerzitet_id")
    private Univerzitet univerzitet;

    @javax.persistence.OneToMany(mappedBy = "kalendar")
    private java.util.List<DogadjajKalendar> dogadjajiKalendar;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public LocalDateTime getPocetniDatum() {
	return this.pocetniDatum;
    }

    public void setPocetniDatum(LocalDateTime pocetniDatum) {
	this.pocetniDatum = pocetniDatum;
    }

    public LocalDateTime getKrajnjiDatum() {
	return this.krajnjiDatum;
    }

    public void setKrajnjiDatum(LocalDateTime krajnjiDatum) {
	this.krajnjiDatum = krajnjiDatum;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public TipStudija getTipStudija() {
	return this.tipStudija;
    }

    public void setTipStudija(TipStudija tipStudija) {
	this.tipStudija = tipStudija;
    }

    public Univerzitet getUniverzitet() {
	return this.univerzitet;
    }

    public void setUniverzitet(Univerzitet univerzitet) {
	this.univerzitet = univerzitet;
    }

    public java.util.List<DogadjajKalendar> getDogadjajiKalendar() {
	return this.dogadjajiKalendar;
    }

    public void setDogadjajiKalendar(java.util.List<DogadjajKalendar> dogadjajiKalendar) {
	this.dogadjajiKalendar = dogadjajiKalendar;
    }

    public Kalendar() {
	super();
	this.dogadjajiKalendar = new java.util.ArrayList<>();
    }

    public DogadjajKalendar getDogadjajKalendar(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addDogadjajKalendar(DogadjajKalendar dogadjajKalendar) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeDogadjajKalendar(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((dogadjajiKalendar == null) ? 0 : dogadjajiKalendar.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((krajnjiDatum == null) ? 0 : krajnjiDatum.hashCode());
	result = prime * result + ((pocetniDatum == null) ? 0 : pocetniDatum.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((tipStudija == null) ? 0 : tipStudija.hashCode());
	result = prime * result + ((univerzitet == null) ? 0 : univerzitet.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Kalendar other = (Kalendar) obj;
	if (dogadjajiKalendar == null) {
	    if (other.dogadjajiKalendar != null)
		return false;
	} else if (!dogadjajiKalendar.equals(other.dogadjajiKalendar))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (krajnjiDatum == null) {
	    if (other.krajnjiDatum != null)
		return false;
	} else if (!krajnjiDatum.equals(other.krajnjiDatum))
	    return false;
	if (pocetniDatum == null) {
	    if (other.pocetniDatum != null)
		return false;
	} else if (!pocetniDatum.equals(other.pocetniDatum))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (tipStudija != other.tipStudija)
	    return false;
	if (univerzitet == null) {
	    if (other.univerzitet != null)
		return false;
	} else if (!univerzitet.equals(other.univerzitet))
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public KalendarDTO getDTO() {
	KalendarDTO kalendarDTO = new KalendarDTO();
	kalendarDTO.setId(id);
	kalendarDTO.setKrajnjiDatum(krajnjiDatum);
	kalendarDTO.setPocetniDatum(pocetniDatum);
	kalendarDTO.setStanje(stanje);
	kalendarDTO.setTipStudija(tipStudija);
	kalendarDTO.setVersion(version);

	if (this.univerzitet != null)
	    kalendarDTO.setUniverzitetDTO(this.univerzitet.getDTOinsideDTO());

	ArrayList<DogadjajKalendarDTO> dogadjajiKalendarDTOs = new ArrayList<DogadjajKalendarDTO>();
	for (DogadjajKalendar dogadjajKalendar : this.dogadjajiKalendar) {
	    dogadjajiKalendarDTOs.add(dogadjajKalendar.getDTOinsideDTO());
	}
	kalendarDTO.setDogadjajiKalendarDTO(dogadjajiKalendarDTOs);

	return kalendarDTO;
    }

    @Override
    public KalendarDTO getDTOinsideDTO() {
	KalendarDTO kalendarDTO = new KalendarDTO();
	kalendarDTO.setId(id);
	kalendarDTO.setKrajnjiDatum(krajnjiDatum);
	kalendarDTO.setPocetniDatum(pocetniDatum);
	kalendarDTO.setStanje(stanje);
	kalendarDTO.setTipStudija(tipStudija);
	kalendarDTO.setVersion(version);
	return kalendarDTO;
    }

    @Override
    public void update(Kalendar entitet) {
	this.setKrajnjiDatum(entitet.getKrajnjiDatum());
	this.setPocetniDatum(entitet.getPocetniDatum());
	this.setStanje(entitet.getStanje());
	this.setTipStudija(entitet.getTipStudija());
	this.setVersion(entitet.getVersion());

	this.setUniverzitet(entitet.getUniverzitet());
    }

}