package lms.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.DatumPohadjanjaPredmetaDTO;
import lms.enums.StanjeModela;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE datum_pohadjanja_predmeta SET stanje = 'OBRISAN' WHERE datum_pohadjanja_predmeta_id = ? AND version = ? ", check = ResultCheckStyle.COUNT)
@Table(name = "datum_pohadjanja_predmeta")
@Transactional
public class DatumPohadjanjaPredmeta implements Entitet<DatumPohadjanjaPredmeta, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -3526799755133139585L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "datum_pohadjanja_predmeta_id")
    private Long id;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    private LocalDateTime datum;

    @Version
    private Long version;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "pohadjanje_predmeta_id")
    private PohadjanjePredmeta pohadjanjePredmeta;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public LocalDateTime getDatum() {
	return this.datum;
    }

    public void setDatum(LocalDateTime datum) {
	this.datum = datum;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public PohadjanjePredmeta getPohadjanjePredmeta() {
	return this.pohadjanjePredmeta;
    }

    public void setPohadjanjePredmeta(PohadjanjePredmeta pohadjanjePredmeta) {
	this.pohadjanjePredmeta = pohadjanjePredmeta;
    }

    public DatumPohadjanjaPredmeta() {
	super();
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((datum == null) ? 0 : datum.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((pohadjanjePredmeta == null) ? 0 : pohadjanjePredmeta.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	DatumPohadjanjaPredmeta other = (DatumPohadjanjaPredmeta) obj;
	if (datum == null) {
	    if (other.datum != null)
		return false;
	} else if (!datum.equals(other.datum))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (pohadjanjePredmeta == null) {
	    if (other.pohadjanjePredmeta != null)
		return false;
	} else if (!pohadjanjePredmeta.equals(other.pohadjanjePredmeta))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public DatumPohadjanjaPredmetaDTO getDTO() {
	DatumPohadjanjaPredmetaDTO datumPohadjanjaPredmetaDTO = new DatumPohadjanjaPredmetaDTO();
	datumPohadjanjaPredmetaDTO.setId(id);
	datumPohadjanjaPredmetaDTO.setDatum(datum);
	datumPohadjanjaPredmetaDTO.setStanje(stanje);
	datumPohadjanjaPredmetaDTO.setVersion(version);

	if (this.pohadjanjePredmeta != null)
	    datumPohadjanjaPredmetaDTO.setPohadjanjePredmetaDTO(this.pohadjanjePredmeta.getDTOinsideDTO());

	return datumPohadjanjaPredmetaDTO;
    }

    @Override
    public DatumPohadjanjaPredmetaDTO getDTOinsideDTO() {
	DatumPohadjanjaPredmetaDTO datumPohadjanjaPredmetaDTO = new DatumPohadjanjaPredmetaDTO();
	datumPohadjanjaPredmetaDTO.setId(id);
	datumPohadjanjaPredmetaDTO.setDatum(datum);
	datumPohadjanjaPredmetaDTO.setStanje(stanje);
	datumPohadjanjaPredmetaDTO.setVersion(version);

	return datumPohadjanjaPredmetaDTO;
    }

    @Override
    public void update(DatumPohadjanjaPredmeta entitet) {
	this.setDatum(entitet.getDatum());
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());

	this.setPohadjanjePredmeta(entitet.getPohadjanjePredmeta());
    }

}