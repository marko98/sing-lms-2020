package lms.model;

import java.time.LocalDateTime;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.IstrazivackiRadStudentNaStudijiDTO;
import lms.dto.PohadjanjePredmetaDTO;
import lms.dto.StudentNaStudijiDTO;
import lms.enums.StanjeModela;
import lms.interfaces.Identifikacija;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE student_na_studiji SET stanje = 'OBRISAN' WHERE student_na_studiji_id = ? AND version = ? ", check = ResultCheckStyle.COUNT)
@Table(name = "student_na_studiji")
@Transactional
public class StudentNaStudiji implements Entitet<StudentNaStudiji, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 4910748241763724626L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "student_na_studiji_id")
    private Long id;

    private LocalDateTime datumUpisa;

    private String brojIndeksa;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "student_id")
    private Student student;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "godina_studija_id")
    private GodinaStudija godinaStudija;

    @javax.persistence.OneToMany(mappedBy = "studentNaStudiji")
    private java.util.List<PohadjanjePredmeta> pohadjanjaPredmeta;

    @javax.persistence.OneToMany(mappedBy = "studentNaStudiji")
    private java.util.List<IstrazivackiRadStudentNaStudiji> istrazivackiRadoviStudentNaStudiji;

    @javax.persistence.OneToOne
    private DiplomskiRad diplomskiRad;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public LocalDateTime getDatumUpisa() {
	return this.datumUpisa;
    }

    public void setDatumUpisa(LocalDateTime datumUpisa) {
	this.datumUpisa = datumUpisa;
    }

    public String getBrojIndeksa() {
	return this.brojIndeksa;
    }

    public void setBrojIndeksa(String brojIndeksa) {
	this.brojIndeksa = brojIndeksa;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public Student getStudent() {
	return this.student;
    }

    public void setStudent(Student student) {
	this.student = student;
    }

    public GodinaStudija getGodinaStudija() {
	return this.godinaStudija;
    }

    public void setGodinaStudija(GodinaStudija godinaStudija) {
	this.godinaStudija = godinaStudija;
    }

    public java.util.List<PohadjanjePredmeta> getPohadjanjaPredmeta() {
	return this.pohadjanjaPredmeta;
    }

    public void setPohadjanjaPredmeta(java.util.List<PohadjanjePredmeta> pohadjanjaPredmeta) {
	this.pohadjanjaPredmeta = pohadjanjaPredmeta;
    }

    public java.util.List<IstrazivackiRadStudentNaStudiji> getIstrazivackiRadoviStudentNaStudiji() {
	return this.istrazivackiRadoviStudentNaStudiji;
    }

    public void setIstrazivackiRadoviStudentNaStudiji(
	    java.util.List<IstrazivackiRadStudentNaStudiji> istrazivackiRadoviStudentNaStudiji) {
	this.istrazivackiRadoviStudentNaStudiji = istrazivackiRadoviStudentNaStudiji;
    }

    public DiplomskiRad getDiplomskiRad() {
	return this.diplomskiRad;
    }

    public void setDiplomskiRad(DiplomskiRad diplomskiRad) {
	this.diplomskiRad = diplomskiRad;
    }

    public StudentNaStudiji() {
	super();
	this.pohadjanjaPredmeta = new java.util.ArrayList<>();
	this.istrazivackiRadoviStudentNaStudiji = new java.util.ArrayList<>();
    }

    public PohadjanjePredmeta getPohadjanjePredmeta(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addPohadjanjePredmeta(PohadjanjePredmeta pohadjanjePredmeta) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removePohadjanjePredmeta(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public IstrazivackiRadStudentNaStudiji getIstrazivackiRadStudentNaStudiji(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addIstrazivackiRadStudentNaStudiji(IstrazivackiRadStudentNaStudiji istrazivackiRadStudentNaStudiji) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeIstrazivackiRadStudentNaStudiji(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((brojIndeksa == null) ? 0 : brojIndeksa.hashCode());
	result = prime * result + ((datumUpisa == null) ? 0 : datumUpisa.hashCode());
	result = prime * result + ((diplomskiRad == null) ? 0 : diplomskiRad.hashCode());
	result = prime * result + ((godinaStudija == null) ? 0 : godinaStudija.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result
		+ ((istrazivackiRadoviStudentNaStudiji == null) ? 0 : istrazivackiRadoviStudentNaStudiji.hashCode());
	result = prime * result + ((pohadjanjaPredmeta == null) ? 0 : pohadjanjaPredmeta.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((student == null) ? 0 : student.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	StudentNaStudiji other = (StudentNaStudiji) obj;
	if (brojIndeksa == null) {
	    if (other.brojIndeksa != null)
		return false;
	} else if (!brojIndeksa.equals(other.brojIndeksa))
	    return false;
	if (datumUpisa == null) {
	    if (other.datumUpisa != null)
		return false;
	} else if (!datumUpisa.equals(other.datumUpisa))
	    return false;
	if (diplomskiRad == null) {
	    if (other.diplomskiRad != null)
		return false;
	} else if (!diplomskiRad.equals(other.diplomskiRad))
	    return false;
	if (godinaStudija == null) {
	    if (other.godinaStudija != null)
		return false;
	} else if (!godinaStudija.equals(other.godinaStudija))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (istrazivackiRadoviStudentNaStudiji == null) {
	    if (other.istrazivackiRadoviStudentNaStudiji != null)
		return false;
	} else if (!istrazivackiRadoviStudentNaStudiji.equals(other.istrazivackiRadoviStudentNaStudiji))
	    return false;
	if (pohadjanjaPredmeta == null) {
	    if (other.pohadjanjaPredmeta != null)
		return false;
	} else if (!pohadjanjaPredmeta.equals(other.pohadjanjaPredmeta))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (student == null) {
	    if (other.student != null)
		return false;
	} else if (!student.equals(other.student))
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public StudentNaStudijiDTO getDTO() {
	StudentNaStudijiDTO studentNaStudijiDTO = new StudentNaStudijiDTO();
	studentNaStudijiDTO.setId(id);
	studentNaStudijiDTO.setBrojIndeksa(brojIndeksa);
	studentNaStudijiDTO.setDatumUpisa(datumUpisa);
	studentNaStudijiDTO.setStanje(stanje);
	studentNaStudijiDTO.setVersion(version);

	if (this.diplomskiRad != null)
	    studentNaStudijiDTO.setDiplomskiRadDTO(this.diplomskiRad.getDTOinsideDTO());
	if (this.godinaStudija != null)
	    studentNaStudijiDTO.setGodinaStudijaDTO(this.godinaStudija.getDTOinsideDTO());
	if (this.student != null)
	    studentNaStudijiDTO.setStudentDTO(this.student.getDTOinsideDTO());

	ArrayList<IstrazivackiRadStudentNaStudijiDTO> istrazivackiRadoviStudentNaStudijiDTOs = new ArrayList<IstrazivackiRadStudentNaStudijiDTO>();
	for (IstrazivackiRadStudentNaStudiji istrazivackiRadStudentNaStudiji : this.istrazivackiRadoviStudentNaStudiji) {
	    istrazivackiRadoviStudentNaStudijiDTOs.add(istrazivackiRadStudentNaStudiji.getDTOinsideDTO());
	}
	studentNaStudijiDTO.setIstrazivackiRadoviStudentNaStudijiDTO(istrazivackiRadoviStudentNaStudijiDTOs);

	ArrayList<PohadjanjePredmetaDTO> pohadjanjaPredmetaDTOs = new ArrayList<PohadjanjePredmetaDTO>();
	for (PohadjanjePredmeta pohadjanjePredmeta : this.pohadjanjaPredmeta) {
	    pohadjanjaPredmetaDTOs.add(pohadjanjePredmeta.getDTOinsideDTO());
	}
	studentNaStudijiDTO.setPohadjanjaPredmetaDTO(pohadjanjaPredmetaDTOs);

	return studentNaStudijiDTO;
    }

    @Override
    public StudentNaStudijiDTO getDTOinsideDTO() {
	StudentNaStudijiDTO studentNaStudijiDTO = new StudentNaStudijiDTO();
	studentNaStudijiDTO.setId(id);
	studentNaStudijiDTO.setBrojIndeksa(brojIndeksa);
	studentNaStudijiDTO.setDatumUpisa(datumUpisa);
	studentNaStudijiDTO.setStanje(stanje);
	studentNaStudijiDTO.setVersion(version);
	return studentNaStudijiDTO;
    }

    @Override
    public void update(StudentNaStudiji entitet) {
	this.setBrojIndeksa(entitet.getBrojIndeksa());
	this.setDatumUpisa(entitet.getDatumUpisa());
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());

	this.setDiplomskiRad(entitet.getDiplomskiRad());
	this.setGodinaStudija(entitet.getGodinaStudija());
	this.setStudent(entitet.getStudent());

    }

}