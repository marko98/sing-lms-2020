package lms.model;

import java.sql.Time;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.VremeOdrzavanjaUNedeljiDTO;
import lms.enums.Dan;
import lms.enums.StanjeModela;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE vreme_odrzavanja_u_nedelji SET stanje = 'OBRISAN' WHERE vreme_odrzavanja_u_nedelji_id = ? AND version = ? ", check = ResultCheckStyle.COUNT)
@Table(name = "vreme_odrzavanja_u_nedelji")
@Transactional
public class VremeOdrzavanjaUNedelji implements Entitet<VremeOdrzavanjaUNedelji, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 3330588427860953996L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "vreme_odrzavanja_u_nedelji_id")
    private Long id;

    private Time vreme;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.OneToOne(mappedBy = "vremeOdrzavanjaUNedelji")
    private Konsultacija konsultacija;

    @Enumerated(EnumType.STRING)
    private Dan dan;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Time getVreme() {
	return this.vreme;
    }

    public void setVreme(Time vreme) {
	this.vreme = vreme;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public Konsultacija getKonsultacija() {
	return this.konsultacija;
    }

    public void setKonsultacija(Konsultacija konsultacija) {
	this.konsultacija = konsultacija;
    }

    public Dan getDan() {
	return this.dan;
    }

    public void setDan(Dan dan) {
	this.dan = dan;
    }

    public VremeOdrzavanjaUNedelji() {
	super();
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((dan == null) ? 0 : dan.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((konsultacija == null) ? 0 : konsultacija.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	result = prime * result + ((vreme == null) ? 0 : vreme.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	VremeOdrzavanjaUNedelji other = (VremeOdrzavanjaUNedelji) obj;
	if (dan != other.dan)
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (konsultacija == null) {
	    if (other.konsultacija != null)
		return false;
	} else if (!konsultacija.equals(other.konsultacija))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	if (vreme == null) {
	    if (other.vreme != null)
		return false;
	} else if (!vreme.equals(other.vreme))
	    return false;
	return true;
    }

    @Override
    public VremeOdrzavanjaUNedeljiDTO getDTO() {
	VremeOdrzavanjaUNedeljiDTO vremeOdrzavanjaUNedeljiDTO = new VremeOdrzavanjaUNedeljiDTO();
	vremeOdrzavanjaUNedeljiDTO.setId(id);
	vremeOdrzavanjaUNedeljiDTO.setDan(dan);
	vremeOdrzavanjaUNedeljiDTO.setStanje(stanje);
	vremeOdrzavanjaUNedeljiDTO.setVersion(version);
	vremeOdrzavanjaUNedeljiDTO.setVreme(vreme);

	if (this.konsultacija != null)
	    vremeOdrzavanjaUNedeljiDTO.setKonsultacijaDTO(this.konsultacija.getDTOinsideDTO());

	return vremeOdrzavanjaUNedeljiDTO;
    }

    @Override
    public VremeOdrzavanjaUNedeljiDTO getDTOinsideDTO() {
	VremeOdrzavanjaUNedeljiDTO vremeOdrzavanjaUNedeljiDTO = new VremeOdrzavanjaUNedeljiDTO();
	vremeOdrzavanjaUNedeljiDTO.setId(id);
	vremeOdrzavanjaUNedeljiDTO.setDan(dan);
	vremeOdrzavanjaUNedeljiDTO.setStanje(stanje);
	vremeOdrzavanjaUNedeljiDTO.setVersion(version);
	vremeOdrzavanjaUNedeljiDTO.setVreme(vreme);
	return vremeOdrzavanjaUNedeljiDTO;
    }

    @Override
    public void update(VremeOdrzavanjaUNedelji entitet) {
	this.setDan(entitet.getDan());
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());
	this.setVreme(entitet.getVreme());

	this.setKonsultacija(entitet.getKonsultacija());

    }

}