package lms.model;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.NastavnikNaRealizacijiDTO;
import lms.dto.NastavnikNaRealizacijiTipNastaveDTO;
import lms.enums.StanjeModela;
import lms.enums.Zvanje;
import lms.interfaces.Identifikacija;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE nastavnik_na_realizaciji SET stanje = 'OBRISAN' WHERE nastavnik_na_realizaciji_id = ? AND version = ? ", check = ResultCheckStyle.COUNT)
@Table(name = "nastavnik_na_realizaciji")
@Transactional
public class NastavnikNaRealizaciji implements Entitet<NastavnikNaRealizaciji, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -3788831995470607652L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "nastavnik_na_realizaciji_id")
    private Long id;

    private Integer brojCasova;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "realizacija_predmeta_id")
    private RealizacijaPredmeta realizacijaPredmeta;

    @Enumerated(EnumType.STRING)
    private Zvanje zvanje;

    @javax.persistence.OneToMany(mappedBy = "nastavnikNaRealizaciji")
    private java.util.List<NastavnikNaRealizacijiTipNastave> nastavnikNaRealizacijiTipoviNastave;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "nastavnik_id")
    private Nastavnik nastavnik;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Integer getBrojCasova() {
	return this.brojCasova;
    }

    public void setBrojCasova(Integer brojCasova) {
	this.brojCasova = brojCasova;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public RealizacijaPredmeta getRealizacijaPredmeta() {
	return this.realizacijaPredmeta;
    }

    public void setRealizacijaPredmeta(RealizacijaPredmeta realizacijaPredmeta) {
	this.realizacijaPredmeta = realizacijaPredmeta;
    }

    public Zvanje getZvanje() {
	return this.zvanje;
    }

    public void setZvanje(Zvanje zvanje) {
	this.zvanje = zvanje;
    }

    public java.util.List<NastavnikNaRealizacijiTipNastave> getNastavnikNaRealizacijiTipoviNastave() {
	return this.nastavnikNaRealizacijiTipoviNastave;
    }

    public void setNastavnikNaRealizacijiTipoviNastave(
	    java.util.List<NastavnikNaRealizacijiTipNastave> nastavnikNaRealizacijiTipoviNastave) {
	this.nastavnikNaRealizacijiTipoviNastave = nastavnikNaRealizacijiTipoviNastave;
    }

    public NastavnikNaRealizaciji() {
	super();
	this.nastavnikNaRealizacijiTipoviNastave = new java.util.ArrayList<>();
    }

    public NastavnikNaRealizacijiTipNastave getNastavnikNaRealizacijiTipNastave(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addNastavnikNaRealizacijiTipNastave(
	    NastavnikNaRealizacijiTipNastave nastavnikNaRealizacijiTipNastave) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeNastavnikNaRealizacijiTipNastave(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public Nastavnik getNastavnik() {
	return nastavnik;
    }

    public void setNastavnik(Nastavnik nastavnik) {
	this.nastavnik = nastavnik;
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((brojCasova == null) ? 0 : brojCasova.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((nastavnik == null) ? 0 : nastavnik.hashCode());
	result = prime * result
		+ ((nastavnikNaRealizacijiTipoviNastave == null) ? 0 : nastavnikNaRealizacijiTipoviNastave.hashCode());
	result = prime * result + ((realizacijaPredmeta == null) ? 0 : realizacijaPredmeta.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	result = prime * result + ((zvanje == null) ? 0 : zvanje.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	NastavnikNaRealizaciji other = (NastavnikNaRealizaciji) obj;
	if (brojCasova == null) {
	    if (other.brojCasova != null)
		return false;
	} else if (!brojCasova.equals(other.brojCasova))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (nastavnik == null) {
	    if (other.nastavnik != null)
		return false;
	} else if (!nastavnik.equals(other.nastavnik))
	    return false;
	if (nastavnikNaRealizacijiTipoviNastave == null) {
	    if (other.nastavnikNaRealizacijiTipoviNastave != null)
		return false;
	} else if (!nastavnikNaRealizacijiTipoviNastave.equals(other.nastavnikNaRealizacijiTipoviNastave))
	    return false;
	if (realizacijaPredmeta == null) {
	    if (other.realizacijaPredmeta != null)
		return false;
	} else if (!realizacijaPredmeta.equals(other.realizacijaPredmeta))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	if (zvanje != other.zvanje)
	    return false;
	return true;
    }

    @Override
    public NastavnikNaRealizacijiDTO getDTO() {
	NastavnikNaRealizacijiDTO nastavnikNaRealizacijiDTO = new NastavnikNaRealizacijiDTO();
	nastavnikNaRealizacijiDTO.setId(id);
	nastavnikNaRealizacijiDTO.setBrojCasova(brojCasova);
	nastavnikNaRealizacijiDTO.setStanje(stanje);
	nastavnikNaRealizacijiDTO.setVersion(version);
	nastavnikNaRealizacijiDTO.setZvanje(zvanje);
	
	if (this.nastavnik != null)
	    nastavnikNaRealizacijiDTO.setNastavnikDTO(this.nastavnik.getDTOinsideDTO());

	if (this.realizacijaPredmeta != null)
	    nastavnikNaRealizacijiDTO.setRealizacijaPredmetaDTO(this.realizacijaPredmeta.getDTOinsideDTO());

	ArrayList<NastavnikNaRealizacijiTipNastaveDTO> nastavnikNaRealizacijiTipoviNastaveDTOs = new ArrayList<NastavnikNaRealizacijiTipNastaveDTO>();
	for (NastavnikNaRealizacijiTipNastave nastavnikNaRealizacijiTipNastave : this.nastavnikNaRealizacijiTipoviNastave) {
	    nastavnikNaRealizacijiTipoviNastaveDTOs.add(nastavnikNaRealizacijiTipNastave.getDTOinsideDTO());
	}
	nastavnikNaRealizacijiDTO.setNastavnikNaRealizacijiTipoviNastaveDTO(nastavnikNaRealizacijiTipoviNastaveDTOs);

	return nastavnikNaRealizacijiDTO;
    }

    @Override
    public NastavnikNaRealizacijiDTO getDTOinsideDTO() {
	NastavnikNaRealizacijiDTO nastavnikNaRealizacijiDTO = new NastavnikNaRealizacijiDTO();
	nastavnikNaRealizacijiDTO.setId(id);
	nastavnikNaRealizacijiDTO.setBrojCasova(brojCasova);
	nastavnikNaRealizacijiDTO.setStanje(stanje);
	nastavnikNaRealizacijiDTO.setVersion(version);
	nastavnikNaRealizacijiDTO.setZvanje(zvanje);
	return nastavnikNaRealizacijiDTO;
    }

    @Override
    public void update(NastavnikNaRealizaciji entitet) {
	this.setBrojCasova(entitet.getBrojCasova());
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());
	this.setZvanje(entitet.getZvanje());

	this.setRealizacijaPredmeta(entitet.getRealizacijaPredmeta());
	this.setNastavnik(entitet.getNastavnik());
    }

}