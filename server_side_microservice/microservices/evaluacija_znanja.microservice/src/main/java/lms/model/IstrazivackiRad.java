package lms.model;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.IstrazivackiRadDTO;
import lms.dto.IstrazivackiRadStudentNaStudijiDTO;
import lms.enums.StanjeModela;
import lms.interfaces.Identifikacija;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE istrazivacki_rad SET stanje = 'OBRISAN' WHERE istrazivacki_rad_id = ? AND version = ? ", check = ResultCheckStyle.COUNT)
@Table(name = "istrazivacki_rad")
@Transactional
public class IstrazivackiRad implements Entitet<IstrazivackiRad, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -4693208751575050464L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "istrazivacki_rad_id")
    private Long id;

    private String tema;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.OneToMany(mappedBy = "istrazivackiRad")
    private java.util.List<IstrazivackiRadStudentNaStudiji> istrazivackiRadStudentiNaStudijama;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "nastavnik_id")
    private Nastavnik nastavnik;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "realizacija_predmeta_id")
    private RealizacijaPredmeta realizacijaPredmeta;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getTema() {
	return this.tema;
    }

    public void setTema(String tema) {
	this.tema = tema;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public java.util.List<IstrazivackiRadStudentNaStudiji> getIstrazivackiRadStudentiNaStudijama() {
	return this.istrazivackiRadStudentiNaStudijama;
    }

    public void setIstrazivackiRadStudentiNaStudijama(
	    java.util.List<IstrazivackiRadStudentNaStudiji> istrazivackiRadStudentiNaStudijama) {
	this.istrazivackiRadStudentiNaStudijama = istrazivackiRadStudentiNaStudijama;
    }

    public Nastavnik getNastavnik() {
	return this.nastavnik;
    }

    public void setNastavnik(Nastavnik nastavnik) {
	this.nastavnik = nastavnik;
    }

    public RealizacijaPredmeta getRealizacijaPredmeta() {
	return this.realizacijaPredmeta;
    }

    public void setRealizacijaPredmeta(RealizacijaPredmeta realizacijaPredmeta) {
	this.realizacijaPredmeta = realizacijaPredmeta;
    }

    public IstrazivackiRad() {
	super();
	this.istrazivackiRadStudentiNaStudijama = new java.util.ArrayList<>();
    }

    public IstrazivackiRadStudentNaStudiji getIstrazivackiRadStudentNaStudiji(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addIstrazivackiRadStudentNaStudiji(IstrazivackiRadStudentNaStudiji istrazivackiRadStudentNaStudiji) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeIstrazivackiRadStudentNaStudiji(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result
		+ ((istrazivackiRadStudentiNaStudijama == null) ? 0 : istrazivackiRadStudentiNaStudijama.hashCode());
	result = prime * result + ((nastavnik == null) ? 0 : nastavnik.hashCode());
	result = prime * result + ((realizacijaPredmeta == null) ? 0 : realizacijaPredmeta.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((tema == null) ? 0 : tema.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	IstrazivackiRad other = (IstrazivackiRad) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (istrazivackiRadStudentiNaStudijama == null) {
	    if (other.istrazivackiRadStudentiNaStudijama != null)
		return false;
	} else if (!istrazivackiRadStudentiNaStudijama.equals(other.istrazivackiRadStudentiNaStudijama))
	    return false;
	if (nastavnik == null) {
	    if (other.nastavnik != null)
		return false;
	} else if (!nastavnik.equals(other.nastavnik))
	    return false;
	if (realizacijaPredmeta == null) {
	    if (other.realizacijaPredmeta != null)
		return false;
	} else if (!realizacijaPredmeta.equals(other.realizacijaPredmeta))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (tema == null) {
	    if (other.tema != null)
		return false;
	} else if (!tema.equals(other.tema))
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public IstrazivackiRadDTO getDTO() {
	IstrazivackiRadDTO istrazivackiRadDTO = new IstrazivackiRadDTO();
	istrazivackiRadDTO.setId(id);
	istrazivackiRadDTO.setStanje(stanje);
	istrazivackiRadDTO.setTema(tema);
	istrazivackiRadDTO.setVersion(version);

	if (this.nastavnik != null)
	    istrazivackiRadDTO.setNastavnikDTO(this.nastavnik.getDTOinsideDTO());
	if (this.realizacijaPredmeta != null)
	    istrazivackiRadDTO.setRealizacijaPredmetaDTO(this.realizacijaPredmeta.getDTOinsideDTO());

	ArrayList<IstrazivackiRadStudentNaStudijiDTO> istrazivackiRadStudentiNaStudijamaDTOs = new ArrayList<IstrazivackiRadStudentNaStudijiDTO>();
	for (IstrazivackiRadStudentNaStudiji istrazivackiRadStudentNaStudiji : this.istrazivackiRadStudentiNaStudijama) {
	    istrazivackiRadStudentiNaStudijamaDTOs.add(istrazivackiRadStudentNaStudiji.getDTOinsideDTO());
	}
	istrazivackiRadDTO.setIstrazivackiRadStudentiNaStudijamaDTO(istrazivackiRadStudentiNaStudijamaDTOs);

	return istrazivackiRadDTO;
    }

    @Override
    public IstrazivackiRadDTO getDTOinsideDTO() {
	IstrazivackiRadDTO istrazivackiRadDTO = new IstrazivackiRadDTO();
	istrazivackiRadDTO.setId(id);
	istrazivackiRadDTO.setStanje(stanje);
	istrazivackiRadDTO.setTema(tema);
	istrazivackiRadDTO.setVersion(version);
	return istrazivackiRadDTO;
    }

    @Override
    public void update(IstrazivackiRad entitet) {
	this.setStanje(entitet.getStanje());
	this.setTema(entitet.getTema());
	this.setVersion(entitet.getVersion());

	this.setNastavnik(entitet.getNastavnik());
	this.setRealizacijaPredmeta(entitet.getRealizacijaPredmeta());

    }

}