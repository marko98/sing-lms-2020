package lms.evaluacija_znanja.microservice.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import lms.controller.CrudController;
import lms.dto.PolaganjeDTO;
import lms.evaluacija_znanja.microservice.service.PolaganjeService;
import lms.model.Polaganje;

@Controller
@Scope("singleton")
@RequestMapping(path = "polaganje")
@CrossOrigin(origins = "*")
public class PolaganjeController extends CrudController<Polaganje, Long> {

    @RequestMapping(path = "/evaluacija_znanja/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> findOneByRegistrovaniKorisnikId(@PathVariable("id") Long id) {
	List<PolaganjeDTO> polaganja = ((PolaganjeService) this.service).findAllByEvaluacijaZnanjaId(id).stream()
		.map(p -> p.getDTO()).collect(Collectors.toList());

	return new ResponseEntity<List<PolaganjeDTO>>(polaganja, HttpStatus.OK);
    }

}