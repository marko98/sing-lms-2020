package lms.evaluacija_znanja.microservice.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.Odgovor;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class OdgovorService extends CrudService<Odgovor, Long> {

}