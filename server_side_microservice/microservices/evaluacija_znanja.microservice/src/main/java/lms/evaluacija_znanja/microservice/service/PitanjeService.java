package lms.evaluacija_znanja.microservice.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.Pitanje;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class PitanjeService extends CrudService<Pitanje, Long> {

}