package lms.evaluacija_znanja.microservice.repository.interfaces;

import java.util.Collection;

import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.Polaganje;

@Repository
@Scope("singleton")
public interface PolaganjePASRepository extends PagingAndSortingRepository<Polaganje, Long> {
    @Query("select n from Polaganje n where n.evaluacijaZnanja.id = ?1")
    public Collection<Polaganje> findAllByEvaluacijaZnanjaId(Long id);
}