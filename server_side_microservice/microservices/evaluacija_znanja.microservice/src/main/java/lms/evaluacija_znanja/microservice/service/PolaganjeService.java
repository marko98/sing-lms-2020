package lms.evaluacija_znanja.microservice.service;

import java.util.Collection;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.evaluacija_znanja.microservice.repository.interfaces.PolaganjePASRepository;
import lms.model.Polaganje;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class PolaganjeService extends CrudService<Polaganje, Long> {
    public Collection<Polaganje> findAllByEvaluacijaZnanjaId(Long id) {
	return ((PolaganjePASRepository) this.repository.getRepository()).findAllByEvaluacijaZnanjaId(id);
    }
}