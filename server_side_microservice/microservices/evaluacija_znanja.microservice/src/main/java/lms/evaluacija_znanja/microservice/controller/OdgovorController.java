package lms.evaluacija_znanja.microservice.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.Odgovor;

@Controller
@Scope("singleton")
@RequestMapping(path = "odgovor")
@CrossOrigin(origins = "*")
public class OdgovorController extends CrudController<Odgovor, Long> {

}