package lms.evaluacija_znanja.microservice.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.EvaluacijaZnanjaNacinEvaluacije;

@Controller
@Scope("singleton")
@RequestMapping(path = "evaluacija_znanja_nacin_evaluacije")
@CrossOrigin(origins = "*")
public class EvaluacijaZnanjaNacinEvaluacijeController extends CrudController<EvaluacijaZnanjaNacinEvaluacije, Long> {

}