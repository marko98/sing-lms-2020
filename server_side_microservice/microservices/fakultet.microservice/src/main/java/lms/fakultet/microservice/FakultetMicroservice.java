package lms.fakultet.microservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jms.annotation.EnableJms;

@SpringBootApplication
@ComponentScan(basePackages = { "lms.*" })
@EntityScan(basePackages = { "lms.model" })
@EnableJpaRepositories({ "lms.fakultet.microservice.repository", "lms.security.repository" })
@EnableJms
public class FakultetMicroservice extends SpringBootServletInitializer {

    public static void main(String args[]) {
	SpringApplication.run(FakultetMicroservice.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
	return builder.sources(FakultetMicroservice.class);
    }

}
