package lms.fakultet.microservice.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.Fakultet;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class FakultetService extends CrudService<Fakultet, Long> {

}