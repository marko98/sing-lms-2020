package lms.fakultet.microservice.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.Kontakt;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class KontaktRepository extends CrudRepository<Kontakt, Long> {

}