package lms.fakultet.microservice.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.Odeljenje;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class OdeljenjeRepository extends CrudRepository<Odeljenje, Long> {

}