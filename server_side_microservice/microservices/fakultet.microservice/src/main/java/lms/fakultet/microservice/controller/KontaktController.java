package lms.fakultet.microservice.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.Kontakt;

@Controller
@Scope("singleton")
@RequestMapping(path = "kontakt")
@CrossOrigin(origins = "*")
public class KontaktController extends CrudController<Kontakt, Long> {

}