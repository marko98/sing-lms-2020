package lms.fakultet.microservice.repository.interfaces;

import java.util.Collection;

import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.Odeljenje;

@Repository
@Scope("singleton")
public interface OdeljenjePASRepository extends PagingAndSortingRepository<Odeljenje, Long> {
    
    @Query("select o from Odeljenje o where o.fakultet.id = ?1")
    public Collection<Odeljenje> findAllByFakultetId(Long id);

}