package lms.fakultet.microservice.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.Univerzitet;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class UniverzitetRepository extends CrudRepository<Univerzitet, Long> {

}