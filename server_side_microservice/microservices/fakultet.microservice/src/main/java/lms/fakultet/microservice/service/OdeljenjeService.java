package lms.fakultet.microservice.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.fakultet.microservice.repository.interfaces.OdeljenjePASRepository;
import lms.model.Odeljenje;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class OdeljenjeService extends CrudService<Odeljenje, Long> {
    public Iterable<Odeljenje> findAllByFakutetId(Long id) {
	return ((OdeljenjePASRepository)this.repository.getRepository()).findAllByFakultetId(id);
    }
}