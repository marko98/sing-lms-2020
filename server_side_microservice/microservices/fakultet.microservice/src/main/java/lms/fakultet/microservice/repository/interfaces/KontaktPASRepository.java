package lms.fakultet.microservice.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.Kontakt;

@Repository
@Scope("singleton")
public interface KontaktPASRepository extends PagingAndSortingRepository<Kontakt, Long> {

}