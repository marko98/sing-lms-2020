package lms.fakultet.microservice.controller;

import java.io.IOException;
import java.util.Optional;

import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.core.JsonProcessingException;

import lms.controller.CrudController;
import lms.dto.interfaces.DTO;
import lms.enums.StanjeModela;
import lms.model.Fakultet;
import lms.model.interfaces.Entitet;
import lms.websocket.WSMessageKeys;

@Controller
@Scope("singleton")
@RequestMapping(path = "fakultet")
@CrossOrigin(origins = "*")
public class FakultetController extends CrudController<Fakultet, Long> {
    @RequestMapping(path = "", method = RequestMethod.POST)
    @Secured({"ROLE_ADMINISTRATOR"})
    public ResponseEntity<?> create(@RequestBody Fakultet model) {
//	check for id
	if (model.getId() != null) {
//		id exists, lets try to find model with given id
	    Optional<Fakultet> t = this.service.findOne(model.getId());

	    if (t.isEmpty()) {
//			we didn't find model, lets create one and return DTO
		model = this.service.save(model);

		this.socketHandler.sendObjectAsMessage(WSMessageKeys.ON_ENTITY_CREATED, model.getDTO());
		return new ResponseEntity<DTO<Long>>(model.getDTO(), HttpStatus.CREATED);
	    } else {
//			model is found, lets return HttpStatus.CONFLICT
		return new ResponseEntity<Void>(HttpStatus.CONFLICT);
	    }
	} else {
//		id doesn't exist, so model doesn't exist lets create one and return DTO
	    model = this.service.save(model);
	    this.socketHandler.sendObjectAsMessage(WSMessageKeys.ON_ENTITY_CREATED, model.getDTO());
	    return new ResponseEntity<DTO<Long>>(model.getDTO(), HttpStatus.CREATED);
	}
    }

    @RequestMapping(path = "/aktiviraj", method = RequestMethod.POST)
    @Secured({"ROLE_ADMINISTRATOR"})
    public ResponseEntity<?> activate(@RequestBody Fakultet model) throws JsonProcessingException, IOException {
//	check for id
	if (model.getId() != null) {
//		id exists, lets try to find model with given id
	    Optional<Fakultet> t = this.service.findOne(model.getId());

	    if (t.isEmpty()) {
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	    } else {
		Fakultet tModel = t.get();
		tModel.setStanje(StanjeModela.AKTIVAN);
		tModel = this.service.save(tModel);
		this.socketHandler.sendObjectAsMessage(WSMessageKeys.ON_ENTITY_UPDATED, tModel.getDTO());
		return new ResponseEntity<DTO<Long>>(tModel.getDTO(), HttpStatus.OK);
	    }
	} else {
	    return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
    }

    @RequestMapping(path = "/deaktiviraj", method = RequestMethod.POST)
    @Secured({"ROLE_ADMINISTRATOR"})
    public ResponseEntity<?> deactivate(@RequestBody Fakultet model) throws JsonProcessingException, IOException {
//	check for id
	if (model.getId() != null) {
//		id exists, lets try to find model with given id
	    Optional<Fakultet> t = this.service.findOne(model.getId());

	    if (t.isEmpty()) {
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	    } else {
		Fakultet tModel = t.get();
		tModel.setStanje(StanjeModela.NEAKTIVAN);
		tModel = this.service.save(tModel);
		this.socketHandler.sendObjectAsMessage(WSMessageKeys.ON_ENTITY_UPDATED, tModel.getDTO());
		return new ResponseEntity<DTO<Long>>(tModel.getDTO(), HttpStatus.OK);
	    }
	} else {
	    return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
    }

    @RequestMapping(path = "", method = RequestMethod.PUT)
    @Secured({"ROLE_ADMINISTRATOR"})
    public ResponseEntity<?> update(@RequestBody Fakultet model) throws JsonProcessingException, IOException {
//	check for id
	if (model.getId() != null) {
//		id exists, lets try to find model
	    Optional<Fakultet> t = this.service.findOne(model.getId());

	    if (t.isPresent()) {
//			model is found, lets update it and return DTO
		Fakultet tModel = t.get();
		tModel.update(model);
		tModel = this.service.save(tModel);
		this.socketHandler.sendObjectAsMessage(WSMessageKeys.ON_ENTITY_UPDATED, tModel.getDTO());
		return new ResponseEntity<DTO<Long>>(tModel.getDTO(), HttpStatus.OK);
	    } else {
//			we didn't find model, lets return HttpStatus.NOT_FOUND
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	    }
	} else {
//		id doesn't exists, let return HttpStatus.NOT_FOUND
	    return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    @Secured({"ROLE_ADMINISTRATOR"})
    public ResponseEntity<?> delete(@PathVariable("id") Long id) throws JsonProcessingException, IOException {
//	try to find model
	Optional<Fakultet> t = this.service.findOne(id);

	if (t.isPresent()) {
//		model is found, lets delete it and return HttpStatus.OK
	    Entitet<Fakultet, Long> tModel = t.get();
	    tModel.setStanje(StanjeModela.OBRISAN);
	    DTO<Long> dto = tModel.getDTO();
	    this.service.delete(t.get());
	    this.socketHandler.sendObjectAsMessage(WSMessageKeys.ON_ENTITY_DELETED, dto);
	    return new ResponseEntity<Void>(HttpStatus.OK);
	} else {
//		we didn't find model, lets return HttpStatus.NOT_FOUND
	    return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
    }

    @RequestMapping(path = "", method = RequestMethod.DELETE)
    @Secured({"ROLE_ADMINISTRATOR"})
    public ResponseEntity<?> delete(@RequestBody Fakultet model) throws JsonProcessingException, IOException {
//	check for id
	if (model.getId() != null) {
//		id exists, lets try to find model
	    Optional<Fakultet> t = this.service.findOne(model.getId());

	    if (t.isPresent()) {
//			model is found, lets delete it and return HttpStatus.OK
		Entitet<Fakultet, Long> tModel = t.get();
		tModel.setStanje(StanjeModela.OBRISAN);
		DTO<Long> dto = tModel.getDTO();
		this.service.delete(t.get());
		this.socketHandler.sendObjectAsMessage(WSMessageKeys.ON_ENTITY_DELETED, dto);
		return new ResponseEntity<Void>(HttpStatus.OK);
	    } else {
//			we didn't find model, lets return HttpStatus.NOT_FOUND
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	    }
	} else {
//		id doesn't exists, let return HttpStatus.NOT_FOUND
	    return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
    }
}