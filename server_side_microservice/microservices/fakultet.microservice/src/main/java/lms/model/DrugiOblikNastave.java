package lms.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.DrugiOblikNastaveDTO;
import lms.enums.StanjeModela;
import lms.enums.TipDrugogOblikaNastave;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE drugi_oblik_nastave SET stanje = 'OBRISAN' WHERE drugi_oblik_nastave_id = ? AND version = ? ", check = ResultCheckStyle.COUNT)
@Table(name = "drugi_oblik_nastave")
@Transactional
public class DrugiOblikNastave implements Entitet<DrugiOblikNastave, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 2651134221616004871L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "drugi_oblik_nastave_id")
    private Long id;

    private LocalDateTime datum;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @Enumerated(EnumType.STRING)
    private TipDrugogOblikaNastave tipDrugogOblikaNastave;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "realizacija_predmeta_id")
    private RealizacijaPredmeta realizacijaPredmeta;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public LocalDateTime getDatum() {
	return this.datum;
    }

    public void setDatum(LocalDateTime datum) {
	this.datum = datum;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public TipDrugogOblikaNastave getTipDrugogOblikaNastave() {
	return this.tipDrugogOblikaNastave;
    }

    public void setTipDrugogOblikaNastave(TipDrugogOblikaNastave tipDrugogOblikaNastave) {
	this.tipDrugogOblikaNastave = tipDrugogOblikaNastave;
    }

    public RealizacijaPredmeta getRealizacijaPredmeta() {
	return this.realizacijaPredmeta;
    }

    public void setRealizacijaPredmeta(RealizacijaPredmeta realizacijaPredmeta) {
	this.realizacijaPredmeta = realizacijaPredmeta;
    }

    public DrugiOblikNastave() {
	super();
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((datum == null) ? 0 : datum.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((realizacijaPredmeta == null) ? 0 : realizacijaPredmeta.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((tipDrugogOblikaNastave == null) ? 0 : tipDrugogOblikaNastave.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	DrugiOblikNastave other = (DrugiOblikNastave) obj;
	if (datum == null) {
	    if (other.datum != null)
		return false;
	} else if (!datum.equals(other.datum))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (realizacijaPredmeta == null) {
	    if (other.realizacijaPredmeta != null)
		return false;
	} else if (!realizacijaPredmeta.equals(other.realizacijaPredmeta))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (tipDrugogOblikaNastave != other.tipDrugogOblikaNastave)
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public DrugiOblikNastaveDTO getDTO() {
	DrugiOblikNastaveDTO drugiOblikNastaveDTO = new DrugiOblikNastaveDTO();
	drugiOblikNastaveDTO.setId(id);
	drugiOblikNastaveDTO.setDatum(datum);
	drugiOblikNastaveDTO.setStanje(stanje);
	drugiOblikNastaveDTO.setTipDrugogOblikaNastave(tipDrugogOblikaNastave);
	drugiOblikNastaveDTO.setVersion(version);

	if (this.realizacijaPredmeta != null)
	    drugiOblikNastaveDTO.setRealizacijaPredmetaDTO(this.realizacijaPredmeta.getDTOinsideDTO());

	return drugiOblikNastaveDTO;
    }

    @Override
    public DrugiOblikNastaveDTO getDTOinsideDTO() {
	DrugiOblikNastaveDTO drugiOblikNastaveDTO = new DrugiOblikNastaveDTO();
	drugiOblikNastaveDTO.setId(id);
	drugiOblikNastaveDTO.setDatum(datum);
	drugiOblikNastaveDTO.setStanje(stanje);
	drugiOblikNastaveDTO.setTipDrugogOblikaNastave(tipDrugogOblikaNastave);
	drugiOblikNastaveDTO.setVersion(version);
	return drugiOblikNastaveDTO;
    }

    @Override
    public void update(DrugiOblikNastave entitet) {
	this.setDatum(entitet.getDatum());
	this.setStanje(entitet.getStanje());
	this.setTipDrugogOblikaNastave(entitet.getTipDrugogOblikaNastave());
	this.setVersion(entitet.getVersion());

	this.setRealizacijaPredmeta(entitet.getRealizacijaPredmeta());

    }

}