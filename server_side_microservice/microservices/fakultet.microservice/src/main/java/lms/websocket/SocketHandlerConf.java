package lms.websocket;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lms.model.Fakultet;
import lms.model.Kontakt;
import lms.model.Odeljenje;
import lms.model.Univerzitet;

@Configuration
public class SocketHandlerConf {
    @Bean
    public SocketHandler<Fakultet, Long> getFakultetSocketHandler() {
	return new SocketHandler<Fakultet, Long>("/fakultet_ws");
    }

    @Bean
    public SocketHandler<Kontakt, Long> getKontaktSocketHandler() {
	return new SocketHandler<Kontakt, Long>("/kontakt_ws");
    }

    @Bean
    public SocketHandler<Odeljenje, Long> getOdeljenjeSocketHandler() {
	return new SocketHandler<Odeljenje, Long>("/odeljenje_ws");
    }
    
    @Bean
    public SocketHandler<Univerzitet, Long> getUniverzitetSocketHandler() {
	return new SocketHandler<Univerzitet, Long>("/univerzitet_ws");
    }
}