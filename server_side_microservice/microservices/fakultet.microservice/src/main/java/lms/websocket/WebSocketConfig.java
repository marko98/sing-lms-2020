package lms.websocket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

import lms.model.Fakultet;
import lms.model.Kontakt;
import lms.model.Odeljenje;
import lms.model.Univerzitet;

@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

    @Autowired
    private SocketHandler<Fakultet, Long> fakultet;

    @Autowired
    private SocketHandler<Kontakt, Long> kontakt;

    @Autowired
    private SocketHandler<Odeljenje, Long> odeljenje;

    @Autowired
    private SocketHandler<Univerzitet, Long> univerzitet;

    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {

	registry.addHandler(fakultet, "/fakultet_ws").setAllowedOrigins("*");
	registry.addHandler(kontakt, "/kontakt_ws").setAllowedOrigins("*");
	registry.addHandler(odeljenje, "/odeljenje_ws").setAllowedOrigins("*");
	registry.addHandler(univerzitet, "/univerzitet_ws").setAllowedOrigins("*");

    }

}
