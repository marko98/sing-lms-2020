package lms.dto;

import lms.dto.interfaces.DTO;
import lms.enums.StanjeModela;

public class RolaDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 6900021219545681654L;

    private Long id;

    private String naziv;

    private java.util.List<RegistrovaniKorisnikRolaDTO> registrovaniKorisniciRolaDTO;

    private StanjeModela stanje;

    private Long version;

    public RolaDTO() {
	super();
	this.registrovaniKorisniciRolaDTO = new java.util.ArrayList<>();
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getNaziv() {
	return naziv;
    }

    public void setNaziv(String naziv) {
	this.naziv = naziv;
    }

    public java.util.List<RegistrovaniKorisnikRolaDTO> getRegistrovaniKorisniciRolaDTO() {
	return registrovaniKorisniciRolaDTO;
    }

    public void setRegistrovaniKorisniciRolaDTO(
	    java.util.List<RegistrovaniKorisnikRolaDTO> registrovaniKorisniciRolaDTO) {
	this.registrovaniKorisniciRolaDTO = registrovaniKorisniciRolaDTO;
    }

    public StanjeModela getStanje() {
	return stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

}
