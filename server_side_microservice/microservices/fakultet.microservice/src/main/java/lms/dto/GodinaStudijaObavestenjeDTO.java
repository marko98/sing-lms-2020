package lms.dto;

import lms.dto.interfaces.DTO;
import lms.enums.StanjeModela;

public class GodinaStudijaObavestenjeDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -6345307414256208697L;

    private Long id;

    private StanjeModela stanje;

    private Long version;

    private GodinaStudijaDTO godinaStudijaDTO;

    private ObavestenjeDTO obavestenjeDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public GodinaStudijaDTO getGodinaStudijaDTO() {
	return this.godinaStudijaDTO;
    }

    public void setGodinaStudijaDTO(GodinaStudijaDTO godinaStudijaDTO) {
	this.godinaStudijaDTO = godinaStudijaDTO;
    }

    public ObavestenjeDTO getObavestenjeDTO() {
	return this.obavestenjeDTO;
    }

    public void setObavestenjeDTO(ObavestenjeDTO obavestenjeDTO) {
	this.obavestenjeDTO = obavestenjeDTO;
    }

    public GodinaStudijaObavestenjeDTO() {
	super();
    }

}