package lms.dto;

import java.time.LocalDateTime;

import lms.dto.interfaces.DTO;
import lms.enums.StanjeModela;
import lms.interfaces.Identifikacija;

public class ObavestenjeDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 6850674361936700152L;

    private Long id;

    private String naslov;

    private LocalDateTime datum;

    private String sadrzaj;

    private StanjeModela stanje;

    private Long version;

    private java.util.List<GodinaStudijaObavestenjeDTO> godineStudijaObavestenjeDTO;

    private java.util.List<FajlDTO> fajloviDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getNaslov() {
	return this.naslov;
    }

    public void setNaslov(String naslov) {
	this.naslov = naslov;
    }

    public LocalDateTime getDatum() {
	return this.datum;
    }

    public void setDatum(LocalDateTime datum) {
	this.datum = datum;
    }

    public String getSadrzaj() {
	return this.sadrzaj;
    }

    public void setSadrzaj(String sadrzaj) {
	this.sadrzaj = sadrzaj;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public java.util.List<GodinaStudijaObavestenjeDTO> getGodineStudijaObavestenjeDTO() {
	return this.godineStudijaObavestenjeDTO;
    }

    public void setGodineStudijaObavestenjeDTO(
	    java.util.List<GodinaStudijaObavestenjeDTO> godineStudijaObavestenjeDTO) {
	this.godineStudijaObavestenjeDTO = godineStudijaObavestenjeDTO;
    }

    public java.util.List<FajlDTO> getFajloviDTO() {
	return this.fajloviDTO;
    }

    public void setFajloviDTO(java.util.List<FajlDTO> fajloviDTO) {
	this.fajloviDTO = fajloviDTO;
    }

    public ObavestenjeDTO() {
	super();
	this.godineStudijaObavestenjeDTO = new java.util.ArrayList<>();
	this.fajloviDTO = new java.util.ArrayList<>();
    }

    public GodinaStudijaObavestenjeDTO getGodinaStudijaObavestenjeDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addGodinaStudijaObavestenjeDTO(GodinaStudijaObavestenjeDTO godinaStudijaObavestenjeDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeGodinaStudijaObavestenjeDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public FajlDTO getFajlDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addFajlDTO(FajlDTO fajlDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeFajlDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

}