package lms.nastavnik.microservice.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.NastavnikNaRealizaciji;
import lms.nastavnik.microservice.repository.interfaces.NastavnikNaRealizacijiPASRepository;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class NastavnikNaRealizacijiService extends CrudService<NastavnikNaRealizaciji, Long> {
    public Iterable<NastavnikNaRealizaciji> findAllByRealizacijaPredmetaId(Long id) {
	return ((NastavnikNaRealizacijiPASRepository) this.repository.getRepository())
		.findAllByRealizacijaPredmetaId(id);
    }
}