package lms.nastavnik.microservice.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.NastavnikFakultet;

@Controller
@Scope("singleton")
@RequestMapping(path = "nastavnik_fakultet")
@CrossOrigin(origins = "*")
public class NastavnikFakultetController extends CrudController<NastavnikFakultet, Long> {

}