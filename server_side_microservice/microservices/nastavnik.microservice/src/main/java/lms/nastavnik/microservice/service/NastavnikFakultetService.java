package lms.nastavnik.microservice.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.NastavnikFakultet;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class NastavnikFakultetService extends CrudService<NastavnikFakultet, Long> {

}