package lms.nastavnik.microservice.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.Nastavnik;
import lms.nastavnik.microservice.repository.interfaces.NastavnikPASRepository;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class NastavnikService extends CrudService<Nastavnik, Long> {
    public Nastavnik findOneByRegistrovaniKorisnikId(Long id) {
	return ((NastavnikPASRepository) this.repository.getRepository()).findOneByRegistrovaniKorisnikId(id);
    }
}