package lms.nastavnik.microservice.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.NastavnikNaRealizacijiTipNastave;

@Controller
@Scope("singleton")
@RequestMapping(path = "nastavnik_na_realizaciji_tip_nastave")
@CrossOrigin(origins = "*")
public class NastavnikNaRealizacijiTipNastaveController extends CrudController<NastavnikNaRealizacijiTipNastave, Long> {

}