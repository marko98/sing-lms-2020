package lms.nastavnik.microservice.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.Rola;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class RolaRepository extends CrudRepository<Rola, Long> {

}
