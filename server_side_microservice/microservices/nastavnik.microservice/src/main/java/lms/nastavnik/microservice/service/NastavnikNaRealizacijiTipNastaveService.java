package lms.nastavnik.microservice.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.NastavnikNaRealizacijiTipNastave;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class NastavnikNaRealizacijiTipNastaveService extends CrudService<NastavnikNaRealizacijiTipNastave, Long> {

}