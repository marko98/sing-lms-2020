package lms.nastavnik.microservice.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.NaucnaOblast;

@Repository
@Scope("singleton")
public interface NaucnaOblastPASRepository extends PagingAndSortingRepository<NaucnaOblast, Long> {

}