package lms.nastavnik.microservice.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.NastavnikDiplomskiRad;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class NastavnikDiplomskiRadRepository extends CrudRepository<NastavnikDiplomskiRad, Long> {

}