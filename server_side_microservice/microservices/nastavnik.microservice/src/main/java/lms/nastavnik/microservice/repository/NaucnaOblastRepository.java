package lms.nastavnik.microservice.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.NaucnaOblast;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class NaucnaOblastRepository extends CrudRepository<NaucnaOblast, Long> {

}