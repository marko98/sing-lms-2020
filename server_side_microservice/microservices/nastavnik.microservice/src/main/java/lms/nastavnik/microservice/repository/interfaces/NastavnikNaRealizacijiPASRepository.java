package lms.nastavnik.microservice.repository.interfaces;

import java.util.Collection;

import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.NastavnikNaRealizaciji;

@Repository
@Scope("singleton")
public interface NastavnikNaRealizacijiPASRepository extends PagingAndSortingRepository<NastavnikNaRealizaciji, Long> {
    
    @Query("select nnr from NastavnikNaRealizaciji nnr where nnr.realizacijaPredmeta.id = ?1")
    public Collection<NastavnikNaRealizaciji> findAllByRealizacijaPredmetaId(Long id);

}