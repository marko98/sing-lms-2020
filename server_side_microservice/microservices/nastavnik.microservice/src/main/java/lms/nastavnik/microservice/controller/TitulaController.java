package lms.nastavnik.microservice.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lms.controller.CrudController;
import lms.model.Titula;

@Controller
@Scope("singleton")
@RequestMapping(path = "titula")
@CrossOrigin(origins = "*")
public class TitulaController extends CrudController<Titula, Long> {

}