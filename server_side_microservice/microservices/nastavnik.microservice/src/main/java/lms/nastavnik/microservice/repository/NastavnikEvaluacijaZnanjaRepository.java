package lms.nastavnik.microservice.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.NastavnikEvaluacijaZnanja;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class NastavnikEvaluacijaZnanjaRepository extends CrudRepository<NastavnikEvaluacijaZnanja, Long> {

}