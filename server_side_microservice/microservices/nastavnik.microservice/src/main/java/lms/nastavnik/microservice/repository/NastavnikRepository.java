package lms.nastavnik.microservice.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.Nastavnik;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class NastavnikRepository extends CrudRepository<Nastavnik, Long> {

}