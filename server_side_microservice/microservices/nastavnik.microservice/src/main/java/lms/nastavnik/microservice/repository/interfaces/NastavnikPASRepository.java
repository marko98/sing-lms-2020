package lms.nastavnik.microservice.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lms.model.Nastavnik;

@Repository
@Scope("singleton")
public interface NastavnikPASRepository extends PagingAndSortingRepository<Nastavnik, Long> {
    @Query("select n from Nastavnik n where n.registrovaniKorisnik.id = ?1")
    public Nastavnik findOneByRegistrovaniKorisnikId(Long id);
}