package lms.model;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.AdresaDTO;
import lms.dto.GradDTO;
import lms.enums.StanjeModela;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE grad SET stanje = 'OBRISAN' WHERE grad_id = ? AND version = ? ", check = ResultCheckStyle.COUNT)
@Table(name = "grad")
@Transactional
public class Grad implements Entitet<Grad, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -6571142162662262011L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "grad_id")
    private Long id;

    private String naziv;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "drzava_id")
    private Drzava drzava;

    @javax.persistence.OneToMany(mappedBy = "grad")
    private java.util.List<Adresa> adrese;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getNaziv() {
	return this.naziv;
    }

    public void setNaziv(String naziv) {
	this.naziv = naziv;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public Drzava getDrzava() {
	return this.drzava;
    }

    public void setDrzava(Drzava drzava) {
	this.drzava = drzava;
    }

    public java.util.List<Adresa> getAdrese() {
	return this.adrese;
    }

    public void setAdrese(java.util.List<Adresa> adrese) {
	this.adrese = adrese;
    }

    public Grad() {
	super();
	this.adrese = new java.util.ArrayList<>();
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((adrese == null) ? 0 : adrese.hashCode());
	result = prime * result + ((drzava == null) ? 0 : drzava.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((naziv == null) ? 0 : naziv.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Grad other = (Grad) obj;
	if (adrese == null) {
	    if (other.adrese != null)
		return false;
	} else if (!adrese.equals(other.adrese))
	    return false;
	if (drzava == null) {
	    if (other.drzava != null)
		return false;
	} else if (!drzava.equals(other.drzava))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (naziv == null) {
	    if (other.naziv != null)
		return false;
	} else if (!naziv.equals(other.naziv))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public GradDTO getDTO() {
	GradDTO gradDTO = new GradDTO();
	gradDTO.setId(id);
	gradDTO.setNaziv(naziv);
	gradDTO.setStanje(stanje);
	gradDTO.setVersion(version);

	if (this.drzava != null)
	    gradDTO.setDrzavaDTO(this.drzava.getDTOinsideDTO());

	ArrayList<AdresaDTO> adreseDTOs = new ArrayList<AdresaDTO>();
	for (Adresa adresa : this.adrese) {
	    adreseDTOs.add(adresa.getDTOinsideDTO());
	}
	gradDTO.setAdreseDTO(adreseDTOs);

	return gradDTO;
    }

    @Override
    public GradDTO getDTOinsideDTO() {
	GradDTO gradDTO = new GradDTO();
	gradDTO.setId(id);
	gradDTO.setNaziv(naziv);
	gradDTO.setStanje(stanje);
	gradDTO.setVersion(version);
	return gradDTO;
    }

    @Override
    public void update(Grad entitet) {
	this.setNaziv(entitet.getNaziv());
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());

	this.setDrzava(entitet.getDrzava());

    }

}