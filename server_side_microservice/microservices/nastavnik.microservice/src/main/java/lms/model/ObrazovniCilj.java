package lms.model;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.ObrazovniCiljDTO;
import lms.enums.StanjeModela;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE obrazovni_cilj SET stanje = 'OBRISAN' WHERE obrazovni_cilj_id = ? AND version = ? ", check = ResultCheckStyle.COUNT)
@Table(name = "obrazovni_cilj")
@Transactional
public class ObrazovniCilj implements Entitet<ObrazovniCilj, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 1816100949404901013L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "obrazovni_cilj_id")
    private Long id;

    private String opis;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "realizacija_predmeta_id")
    private RealizacijaPredmeta realizacijaPredmeta;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "ishod_id")
    private Ishod ishod;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getOpis() {
	return this.opis;
    }

    public void setOpis(String opis) {
	this.opis = opis;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public RealizacijaPredmeta getRealizacijaPredmeta() {
	return this.realizacijaPredmeta;
    }

    public void setRealizacijaPredmeta(RealizacijaPredmeta realizacijaPredmeta) {
	this.realizacijaPredmeta = realizacijaPredmeta;
    }

    public Ishod getIshod() {
	return this.ishod;
    }

    public void setIshod(Ishod ishod) {
	this.ishod = ishod;
    }

    public ObrazovniCilj() {
	super();
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((ishod == null) ? 0 : ishod.hashCode());
	result = prime * result + ((opis == null) ? 0 : opis.hashCode());
	result = prime * result + ((realizacijaPredmeta == null) ? 0 : realizacijaPredmeta.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	ObrazovniCilj other = (ObrazovniCilj) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (ishod == null) {
	    if (other.ishod != null)
		return false;
	} else if (!ishod.equals(other.ishod))
	    return false;
	if (opis == null) {
	    if (other.opis != null)
		return false;
	} else if (!opis.equals(other.opis))
	    return false;
	if (realizacijaPredmeta == null) {
	    if (other.realizacijaPredmeta != null)
		return false;
	} else if (!realizacijaPredmeta.equals(other.realizacijaPredmeta))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public ObrazovniCiljDTO getDTO() {
	ObrazovniCiljDTO obrazovniCiljDTO = new ObrazovniCiljDTO();
	obrazovniCiljDTO.setId(id);
	obrazovniCiljDTO.setOpis(opis);
	obrazovniCiljDTO.setStanje(stanje);
	obrazovniCiljDTO.setVersion(version);

	if (this.ishod != null)
	    obrazovniCiljDTO.setIshodDTO(this.ishod.getDTOinsideDTO());
	if (this.realizacijaPredmeta != null)
	    obrazovniCiljDTO.setRealizacijaPredmetaDTO(this.realizacijaPredmeta.getDTOinsideDTO());

	return obrazovniCiljDTO;
    }

    @Override
    public ObrazovniCiljDTO getDTOinsideDTO() {
	ObrazovniCiljDTO obrazovniCiljDTO = new ObrazovniCiljDTO();
	obrazovniCiljDTO.setId(id);
	obrazovniCiljDTO.setOpis(opis);
	obrazovniCiljDTO.setStanje(stanje);
	obrazovniCiljDTO.setVersion(version);
	return obrazovniCiljDTO;
    }

    @Override
    public void update(ObrazovniCilj entitet) {
	this.setOpis(entitet.getOpis());
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());

	this.setIshod(entitet.getIshod());
	this.setRealizacijaPredmeta(entitet.getRealizacijaPredmeta());
    }
}