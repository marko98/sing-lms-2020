package lms.model;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.KonsultacijaDTO;
import lms.enums.StanjeModela;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE konsultacija SET stanje = 'OBRISAN' WHERE konsultacija_id = ? AND version = ? ", check = ResultCheckStyle.COUNT)
@Table(name = "konsultacija")
@Transactional
public class Konsultacija implements Entitet<Konsultacija, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -7906473179376523072L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "konsultacija_id")
    private Long id;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "godina_studija_id")
    private GodinaStudija godinaStudija;

    @javax.persistence.OneToOne
    private VremeOdrzavanjaUNedelji vremeOdrzavanjaUNedelji;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "nastavnik_id")
    private Nastavnik nastavnik;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public GodinaStudija getGodinaStudija() {
	return this.godinaStudija;
    }

    public void setGodinaStudija(GodinaStudija godinaStudija) {
	this.godinaStudija = godinaStudija;
    }

    public VremeOdrzavanjaUNedelji getVremeOdrzavanjaUNedelji() {
	return this.vremeOdrzavanjaUNedelji;
    }

    public void setVremeOdrzavanjaUNedelji(VremeOdrzavanjaUNedelji vremeOdrzavanjaUNedelji) {
	this.vremeOdrzavanjaUNedelji = vremeOdrzavanjaUNedelji;
    }

    public Nastavnik getNastavnik() {
	return this.nastavnik;
    }

    public void setNastavnik(Nastavnik nastavnik) {
	this.nastavnik = nastavnik;
    }

    public Konsultacija() {
	super();
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((godinaStudija == null) ? 0 : godinaStudija.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((nastavnik == null) ? 0 : nastavnik.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	result = prime * result + ((vremeOdrzavanjaUNedelji == null) ? 0 : vremeOdrzavanjaUNedelji.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Konsultacija other = (Konsultacija) obj;
	if (godinaStudija == null) {
	    if (other.godinaStudija != null)
		return false;
	} else if (!godinaStudija.equals(other.godinaStudija))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (nastavnik == null) {
	    if (other.nastavnik != null)
		return false;
	} else if (!nastavnik.equals(other.nastavnik))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	if (vremeOdrzavanjaUNedelji == null) {
	    if (other.vremeOdrzavanjaUNedelji != null)
		return false;
	} else if (!vremeOdrzavanjaUNedelji.equals(other.vremeOdrzavanjaUNedelji))
	    return false;
	return true;
    }

    @Override
    public KonsultacijaDTO getDTO() {
	KonsultacijaDTO konsultacijaDTO = new KonsultacijaDTO();
	konsultacijaDTO.setId(id);
	konsultacijaDTO.setStanje(stanje);
	konsultacijaDTO.setVersion(version);

	if (this.godinaStudija != null)
	    konsultacijaDTO.setGodinaStudijaDTO(this.godinaStudija.getDTOinsideDTO());
	if (this.nastavnik != null)
	    konsultacijaDTO.setNastavnikDTO(this.nastavnik.getDTOinsideDTO());
	if (this.vremeOdrzavanjaUNedelji != null)
	    konsultacijaDTO.setVremeOdrzavanjaUNedeljiDTO(this.vremeOdrzavanjaUNedelji.getDTOinsideDTO());

	return konsultacijaDTO;
    }

    @Override
    public KonsultacijaDTO getDTOinsideDTO() {
	KonsultacijaDTO konsultacijaDTO = new KonsultacijaDTO();
	konsultacijaDTO.setId(id);
	konsultacijaDTO.setStanje(stanje);
	konsultacijaDTO.setVersion(version);
	return konsultacijaDTO;
    }

    @Override
    public void update(Konsultacija entitet) {
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());

	this.setGodinaStudija(entitet.getGodinaStudija());
	this.setNastavnik(entitet.getNastavnik());
	this.setVremeOdrzavanjaUNedelji(entitet.getVremeOdrzavanjaUNedelji());

    }

}