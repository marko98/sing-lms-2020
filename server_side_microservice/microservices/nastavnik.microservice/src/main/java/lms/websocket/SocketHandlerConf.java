package lms.websocket;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lms.model.Nastavnik;
import lms.model.NastavnikDiplomskiRad;
import lms.model.NastavnikEvaluacijaZnanja;
import lms.model.NastavnikFakultet;
import lms.model.NastavnikNaRealizaciji;
import lms.model.NastavnikNaRealizacijiTipNastave;
import lms.model.NaucnaOblast;
import lms.model.Titula;

@Configuration
public class SocketHandlerConf {
    @Bean
    public SocketHandler<Nastavnik, Long> getNastavnikSocketHandler() {
	return new SocketHandler<Nastavnik, Long>("/nastavnik_ws");
    }

    @Bean
    public SocketHandler<NastavnikDiplomskiRad, Long> getNastavnikDiplomskiRadSocketHandler() {
	return new SocketHandler<NastavnikDiplomskiRad, Long>("/nastavnik_diplomski_rad_ws");
    }

    @Bean
    public SocketHandler<NastavnikEvaluacijaZnanja, Long> getNastavnikEvaluacijaZnanjaSocketHandler() {
	return new SocketHandler<NastavnikEvaluacijaZnanja, Long>("/nastavnik_evaluacija_znanja_ws");
    }

    @Bean
    public SocketHandler<NastavnikFakultet, Long> getNastavnikFakultetSocketHandler() {
	return new SocketHandler<NastavnikFakultet, Long>("/nastavnik_fakultet_ws");
    }

    @Bean
    public SocketHandler<NastavnikNaRealizaciji, Long> getNastavnikNaRealizacijiSocketHandler() {
	return new SocketHandler<NastavnikNaRealizaciji, Long>("/nastavnik_na_realizaciji_ws");
    }

    @Bean
    public SocketHandler<NastavnikNaRealizacijiTipNastave, Long> getNastavnikNaRealizacijiTipNastaveSocketHandler() {
	return new SocketHandler<NastavnikNaRealizacijiTipNastave, Long>("/nastavnik_na_realizaciji_tip_nastave_ws");
    }

    @Bean
    public SocketHandler<NaucnaOblast, Long> getNaucnaOblastSocketHandler() {
	return new SocketHandler<NaucnaOblast, Long>("/naucna_oblast_ws");
    }

    @Bean
    public SocketHandler<Titula, Long> getTitulaSocketHandler() {
	return new SocketHandler<Titula, Long>("/titula_ws");
    }

}