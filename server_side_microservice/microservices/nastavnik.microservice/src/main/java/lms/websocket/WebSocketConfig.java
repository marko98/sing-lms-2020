package lms.websocket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

import lms.model.Nastavnik;
import lms.model.NastavnikDiplomskiRad;
import lms.model.NastavnikEvaluacijaZnanja;
import lms.model.NastavnikFakultet;
import lms.model.NastavnikNaRealizaciji;
import lms.model.NastavnikNaRealizacijiTipNastave;
import lms.model.NaucnaOblast;
import lms.model.Titula;

@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

    @Autowired
    private SocketHandler<Nastavnik, Long> nastavnik;
    @Autowired
    private SocketHandler<NastavnikDiplomskiRad, Long> nastavnikDiplomskiRad;
    @Autowired
    private SocketHandler<NastavnikEvaluacijaZnanja, Long> nastavnikEvaluacijaZnanja;
    @Autowired
    private SocketHandler<NastavnikFakultet, Long> nastavnikFakultet;
    @Autowired
    private SocketHandler<NastavnikNaRealizaciji, Long> nastavnikNaRealizaciji;
    @Autowired
    private SocketHandler<NastavnikNaRealizacijiTipNastave, Long> nastavnikNaRealizacijiTipNastave;
    @Autowired
    private SocketHandler<NaucnaOblast, Long> naucnaOblast;
    @Autowired
    private SocketHandler<Titula, Long> titula;

    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {

	registry.addHandler(nastavnik, "/nastavnik_ws").setAllowedOrigins("*");
	registry.addHandler(nastavnikDiplomskiRad, "/nastavnik_diplomski_rad_ws").setAllowedOrigins("*");
	registry.addHandler(nastavnikEvaluacijaZnanja, "/nastavnik_evaluacija_znanja_ws").setAllowedOrigins("*");
	registry.addHandler(nastavnikFakultet, "/nastavnik_fakultet_ws").setAllowedOrigins("*");
	registry.addHandler(nastavnikNaRealizaciji, "/nastavnik_na_realizaciji_ws").setAllowedOrigins("*");
	registry.addHandler(nastavnikNaRealizacijiTipNastave, "/nastavnik_na_realizaciji_tip_nastave_ws")
		.setAllowedOrigins("*");
	registry.addHandler(naucnaOblast, "/naucna_oblast_ws").setAllowedOrigins("*");
	registry.addHandler(titula, "/titula_ws").setAllowedOrigins("*");

    }

}
