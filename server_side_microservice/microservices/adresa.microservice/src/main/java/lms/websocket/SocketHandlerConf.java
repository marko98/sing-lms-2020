package lms.websocket;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lms.model.Adresa;
import lms.model.Drzava;
import lms.model.Grad;

@Configuration
public class SocketHandlerConf {
    @Bean
    public SocketHandler<Grad, Long> getGradSocketHandler() {
	return new SocketHandler<Grad, Long>("/grad_ws");
    }

    @Bean
    public SocketHandler<Drzava, Long> getDrzavaSocketHandler() {
	return new SocketHandler<Drzava, Long>("/drzava_ws");
    }

    @Bean
    public SocketHandler<Adresa, Long> getAdresaSocketHandler() {
	return new SocketHandler<Adresa, Long>("/adresa_ws");
    }
}