package lms.websocket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

import lms.model.Adresa;
import lms.model.Drzava;
import lms.model.Grad;

@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

    @Autowired
    private SocketHandler<Grad, Long> gSocketHandler;

    @Autowired
    private SocketHandler<Drzava, Long> dSocketHandler;

    @Autowired
    private SocketHandler<Adresa, Long> aSocketHandler;

    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {

	registry.addHandler(aSocketHandler, "/adresa_ws").setAllowedOrigins("*");

	registry.addHandler(gSocketHandler, "/grad_ws").setAllowedOrigins("*");

	registry.addHandler(dSocketHandler, "/drzava_ws").setAllowedOrigins("*");

    }

}
