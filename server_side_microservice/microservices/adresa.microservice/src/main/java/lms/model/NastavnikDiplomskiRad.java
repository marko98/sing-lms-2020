package lms.model;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.NastavnikDiplomskiRadDTO;
import lms.enums.StanjeModela;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE nastavnik_diplomski_rad SET stanje = 'OBRISAN' WHERE nastavnik_diplomski_rad_id = ? AND version = ? ", check = ResultCheckStyle.COUNT)
@Table(name = "nastavnik_diplomski_rad")
@Transactional
public class NastavnikDiplomskiRad implements Entitet<NastavnikDiplomskiRad, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 4907119473759778316L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "nastavnik_diplomski_rad_id")
    private Long id;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "nastavnik_id")
    private Nastavnik nastavnikUKomisiji;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "diplomski_rad_id")
    private DiplomskiRad diplomskiRad;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public Nastavnik getNastavnikUKomisiji() {
	return this.nastavnikUKomisiji;
    }

    public void setNastavnikUKomisiji(Nastavnik nastavnikUKomisiji) {
	this.nastavnikUKomisiji = nastavnikUKomisiji;
    }

    public DiplomskiRad getDiplomskiRad() {
	return this.diplomskiRad;
    }

    public void setDiplomskiRad(DiplomskiRad diplomskiRad) {
	this.diplomskiRad = diplomskiRad;
    }

    public NastavnikDiplomskiRad() {
	super();
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((diplomskiRad == null) ? 0 : diplomskiRad.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((nastavnikUKomisiji == null) ? 0 : nastavnikUKomisiji.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	NastavnikDiplomskiRad other = (NastavnikDiplomskiRad) obj;
	if (diplomskiRad == null) {
	    if (other.diplomskiRad != null)
		return false;
	} else if (!diplomskiRad.equals(other.diplomskiRad))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (nastavnikUKomisiji == null) {
	    if (other.nastavnikUKomisiji != null)
		return false;
	} else if (!nastavnikUKomisiji.equals(other.nastavnikUKomisiji))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public NastavnikDiplomskiRadDTO getDTO() {
	NastavnikDiplomskiRadDTO nastavnikDiplomskiRadDTO = new NastavnikDiplomskiRadDTO();
	nastavnikDiplomskiRadDTO.setId(id);
	nastavnikDiplomskiRadDTO.setStanje(stanje);
	nastavnikDiplomskiRadDTO.setVersion(version);

	if (this.diplomskiRad != null)
	    nastavnikDiplomskiRadDTO.setDiplomskiRadDTO(this.diplomskiRad.getDTOinsideDTO());
	if (this.nastavnikUKomisiji != null)
	    nastavnikDiplomskiRadDTO.setNastavnikUKomisijiDTO(this.nastavnikUKomisiji.getDTOinsideDTO());

	return nastavnikDiplomskiRadDTO;
    }

    @Override
    public NastavnikDiplomskiRadDTO getDTOinsideDTO() {
	NastavnikDiplomskiRadDTO nastavnikDiplomskiRadDTO = new NastavnikDiplomskiRadDTO();
	nastavnikDiplomskiRadDTO.setId(id);
	nastavnikDiplomskiRadDTO.setStanje(stanje);
	nastavnikDiplomskiRadDTO.setVersion(version);
	return nastavnikDiplomskiRadDTO;
    }

    @Override
    public void update(NastavnikDiplomskiRad entitet) {
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());

	this.setDiplomskiRad(entitet.getDiplomskiRad());
	this.setNastavnikUKomisiji(entitet.getNastavnikUKomisiji());

    }

}