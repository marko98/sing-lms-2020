package lms.model;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.PredmetDTO;
import lms.dto.PredmetTipDrugogOblikaNastaveDTO;
import lms.dto.PredmetTipNastaveDTO;
import lms.dto.PredmetUslovniPredmetDTO;
import lms.enums.StanjeModela;
import lms.interfaces.Identifikacija;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE predmet SET stanje = 'OBRISAN' WHERE predmet_id = ? AND version = ? ", check = ResultCheckStyle.COUNT)
@Table(name = "predmet")
@Transactional
public class Predmet implements Entitet<Predmet, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -5791582229508027776L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "predmet_id")
    private Long id;

    private String naziv;

    private boolean obavezan;

    private Integer brojPredavanja;

    private Integer brojVezbi;

    private Integer brojCasovaZaIstrazivackeRadove;

    private Integer espb;

    private Integer trajanjeUSemestrima;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "godina_studija_id")
    private GodinaStudija godinaStudija;

    @javax.persistence.OneToOne
    private Silabus silabus;

    @javax.persistence.OneToMany(mappedBy = "predmet")
    private java.util.List<PredmetTipDrugogOblikaNastave> predmetTipoviDrugogOblikaNastave;

    @javax.persistence.OneToMany(mappedBy = "predmet")
    private java.util.List<PredmetTipNastave> predmetTipoviNastave;

    @javax.persistence.OneToMany(mappedBy = "uslov")
    private java.util.List<PredmetUslovniPredmet> predmetiZaKojeSamUslov;

    @javax.persistence.OneToMany(mappedBy = "predmet")
    private java.util.List<PredmetUslovniPredmet> uslovniPredmeti;

    @javax.persistence.OneToOne(mappedBy = "predmet")
    private RealizacijaPredmeta realizacijaPredmeta;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getNaziv() {
	return this.naziv;
    }

    public void setNaziv(String naziv) {
	this.naziv = naziv;
    }

    public boolean getObavezan() {
	return this.obavezan;
    }

    public void setObavezan(boolean obavezan) {
	this.obavezan = obavezan;
    }

    public Integer getBrojPredavanja() {
	return this.brojPredavanja;
    }

    public void setBrojPredavanja(Integer brojPredavanja) {
	this.brojPredavanja = brojPredavanja;
    }

    public Integer getBrojVezbi() {
	return this.brojVezbi;
    }

    public void setBrojVezbi(Integer brojVezbi) {
	this.brojVezbi = brojVezbi;
    }

    public Integer getBrojCasovaZaIstrazivackeRadove() {
	return this.brojCasovaZaIstrazivackeRadove;
    }

    public void setBrojCasovaZaIstrazivackeRadove(Integer brojCasovaZaIstrazivackeRadove) {
	this.brojCasovaZaIstrazivackeRadove = brojCasovaZaIstrazivackeRadove;
    }

    public Integer getEspb() {
	return this.espb;
    }

    public void setEspb(Integer espb) {
	this.espb = espb;
    }

    public Integer getTrajanjeUSemestrima() {
	return this.trajanjeUSemestrima;
    }

    public void setTrajanjeUSemestrima(Integer trajanjeUSemestrima) {
	this.trajanjeUSemestrima = trajanjeUSemestrima;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public GodinaStudija getGodinaStudija() {
	return this.godinaStudija;
    }

    public void setGodinaStudija(GodinaStudija godinaStudija) {
	this.godinaStudija = godinaStudija;
    }

    public Silabus getSilabus() {
	return this.silabus;
    }

    public void setSilabus(Silabus silabus) {
	this.silabus = silabus;
    }

    public java.util.List<PredmetTipDrugogOblikaNastave> getPredmetTipoviDrugogOblikaNastave() {
	return this.predmetTipoviDrugogOblikaNastave;
    }

    public void setPredmetTipoviDrugogOblikaNastave(
	    java.util.List<PredmetTipDrugogOblikaNastave> predmetTipoviDrugogOblikaNastave) {
	this.predmetTipoviDrugogOblikaNastave = predmetTipoviDrugogOblikaNastave;
    }

    public java.util.List<PredmetTipNastave> getPredmetTipoviNastave() {
	return this.predmetTipoviNastave;
    }

    public void setPredmetTipoviNastave(java.util.List<PredmetTipNastave> predmetTipoviNastave) {
	this.predmetTipoviNastave = predmetTipoviNastave;
    }

    public java.util.List<PredmetUslovniPredmet> getPredmetiZaKojeSamUslov() {
	return this.predmetiZaKojeSamUslov;
    }

    public void setPredmetiZaKojeSamUslov(java.util.List<PredmetUslovniPredmet> predmetiZaKojeSamUslov) {
	this.predmetiZaKojeSamUslov = predmetiZaKojeSamUslov;
    }

    public java.util.List<PredmetUslovniPredmet> getUslovniPredmeti() {
	return this.uslovniPredmeti;
    }

    public void setUslovniPredmeti(java.util.List<PredmetUslovniPredmet> uslovniPredmeti) {
	this.uslovniPredmeti = uslovniPredmeti;
    }

    public RealizacijaPredmeta getRealizacijaPredmeta() {
	return this.realizacijaPredmeta;
    }

    public void setRealizacijaPredmeta(RealizacijaPredmeta realizacijaPredmeta) {
	this.realizacijaPredmeta = realizacijaPredmeta;
    }

    public Predmet() {
	super();
	this.predmetTipoviDrugogOblikaNastave = new java.util.ArrayList<>();
	this.predmetTipoviNastave = new java.util.ArrayList<>();
	this.predmetiZaKojeSamUslov = new java.util.ArrayList<>();
	this.uslovniPredmeti = new java.util.ArrayList<>();
    }

    public Predmet getPreduslov(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addPreduslov(Predmet preduslov) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removePreduslov(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public PredmetTipNastave getPredmetTipNastave(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addPredmetTipNastave(PredmetTipNastave predmetTipNastave) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removePredmetTipNastave(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public PredmetTipDrugogOblikaNastave getPredmetTipDrugogOblikaNastave(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addPredmetTipDrugogOblikaNastave(PredmetTipDrugogOblikaNastave predmetTipDrugogOblikaNastave) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removePredmetTipDrugogOblikaNastave(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public PredmetUslovniPredmet getPredmetZaKojiSamUslov(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addPredmetZaKojiSamUslov(PredmetUslovniPredmet predmetUslovniPredmet) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removePredmetZaKojiSamUslov(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public PredmetUslovniPredmet getUslovniPredmet(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addUslovniPredmet(PredmetUslovniPredmet predmetUslovniPredmet) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeUslovniPredmet(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result
		+ ((brojCasovaZaIstrazivackeRadove == null) ? 0 : brojCasovaZaIstrazivackeRadove.hashCode());
	result = prime * result + ((brojPredavanja == null) ? 0 : brojPredavanja.hashCode());
	result = prime * result + ((brojVezbi == null) ? 0 : brojVezbi.hashCode());
	result = prime * result + ((espb == null) ? 0 : espb.hashCode());
	result = prime * result + ((godinaStudija == null) ? 0 : godinaStudija.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((naziv == null) ? 0 : naziv.hashCode());
	result = prime * result + (obavezan ? 1231 : 1237);
	result = prime * result
		+ ((predmetTipoviDrugogOblikaNastave == null) ? 0 : predmetTipoviDrugogOblikaNastave.hashCode());
	result = prime * result + ((predmetTipoviNastave == null) ? 0 : predmetTipoviNastave.hashCode());
	result = prime * result + ((predmetiZaKojeSamUslov == null) ? 0 : predmetiZaKojeSamUslov.hashCode());
	result = prime * result + ((realizacijaPredmeta == null) ? 0 : realizacijaPredmeta.hashCode());
	result = prime * result + ((silabus == null) ? 0 : silabus.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((trajanjeUSemestrima == null) ? 0 : trajanjeUSemestrima.hashCode());
	result = prime * result + ((uslovniPredmeti == null) ? 0 : uslovniPredmeti.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Predmet other = (Predmet) obj;
	if (brojCasovaZaIstrazivackeRadove == null) {
	    if (other.brojCasovaZaIstrazivackeRadove != null)
		return false;
	} else if (!brojCasovaZaIstrazivackeRadove.equals(other.brojCasovaZaIstrazivackeRadove))
	    return false;
	if (brojPredavanja == null) {
	    if (other.brojPredavanja != null)
		return false;
	} else if (!brojPredavanja.equals(other.brojPredavanja))
	    return false;
	if (brojVezbi == null) {
	    if (other.brojVezbi != null)
		return false;
	} else if (!brojVezbi.equals(other.brojVezbi))
	    return false;
	if (espb == null) {
	    if (other.espb != null)
		return false;
	} else if (!espb.equals(other.espb))
	    return false;
	if (godinaStudija == null) {
	    if (other.godinaStudija != null)
		return false;
	} else if (!godinaStudija.equals(other.godinaStudija))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (naziv == null) {
	    if (other.naziv != null)
		return false;
	} else if (!naziv.equals(other.naziv))
	    return false;
	if (obavezan != other.obavezan)
	    return false;
	if (predmetTipoviDrugogOblikaNastave == null) {
	    if (other.predmetTipoviDrugogOblikaNastave != null)
		return false;
	} else if (!predmetTipoviDrugogOblikaNastave.equals(other.predmetTipoviDrugogOblikaNastave))
	    return false;
	if (predmetTipoviNastave == null) {
	    if (other.predmetTipoviNastave != null)
		return false;
	} else if (!predmetTipoviNastave.equals(other.predmetTipoviNastave))
	    return false;
	if (predmetiZaKojeSamUslov == null) {
	    if (other.predmetiZaKojeSamUslov != null)
		return false;
	} else if (!predmetiZaKojeSamUslov.equals(other.predmetiZaKojeSamUslov))
	    return false;
	if (realizacijaPredmeta == null) {
	    if (other.realizacijaPredmeta != null)
		return false;
	} else if (!realizacijaPredmeta.equals(other.realizacijaPredmeta))
	    return false;
	if (silabus == null) {
	    if (other.silabus != null)
		return false;
	} else if (!silabus.equals(other.silabus))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (trajanjeUSemestrima == null) {
	    if (other.trajanjeUSemestrima != null)
		return false;
	} else if (!trajanjeUSemestrima.equals(other.trajanjeUSemestrima))
	    return false;
	if (uslovniPredmeti == null) {
	    if (other.uslovniPredmeti != null)
		return false;
	} else if (!uslovniPredmeti.equals(other.uslovniPredmeti))
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public PredmetDTO getDTO() {
	PredmetDTO predmetDTO = new PredmetDTO();
	predmetDTO.setId(id);
	predmetDTO.setBrojCasovaZaIstrazivackeRadove(brojCasovaZaIstrazivackeRadove);
	predmetDTO.setBrojPredavanja(brojPredavanja);
	predmetDTO.setBrojVezbi(brojVezbi);
	predmetDTO.setEspb(espb);
	predmetDTO.setNaziv(naziv);
	predmetDTO.setObavezan(obavezan);
	predmetDTO.setStanje(stanje);
	predmetDTO.setTrajanjeUSemestrima(trajanjeUSemestrima);
	predmetDTO.setVersion(version);

	if (this.godinaStudija != null)
	    predmetDTO.setGodinaStudijaDTO(this.godinaStudija.getDTOinsideDTO());
	if (this.realizacijaPredmeta != null)
	    predmetDTO.setRealizacijaPredmetaDTO(this.realizacijaPredmeta.getDTOinsideDTO());
	if (this.silabus != null)
	    predmetDTO.setSilabusDTO(this.silabus.getDTOinsideDTO());

	ArrayList<PredmetUslovniPredmetDTO> predmetiZaKojeSamUslovDTOs = new ArrayList<PredmetUslovniPredmetDTO>();
	for (PredmetUslovniPredmet predmetZaKojiSamUslov : this.predmetiZaKojeSamUslov) {
	    predmetiZaKojeSamUslovDTOs.add(predmetZaKojiSamUslov.getDTOinsideDTO());
	}
	predmetDTO.setPredmetiZaKojeSamUslovDTO(predmetiZaKojeSamUslovDTOs);

	ArrayList<PredmetTipDrugogOblikaNastaveDTO> predmetTipoviDrugogOblikaNastaveDTOs = new ArrayList<PredmetTipDrugogOblikaNastaveDTO>();
	for (PredmetTipDrugogOblikaNastave predmetTipDrugogOblikaNastave : this.predmetTipoviDrugogOblikaNastave) {
	    predmetTipoviDrugogOblikaNastaveDTOs.add(predmetTipDrugogOblikaNastave.getDTOinsideDTO());
	}
	predmetDTO.setPredmetTipoviDrugogOblikaNastaveDTO(predmetTipoviDrugogOblikaNastaveDTOs);

	ArrayList<PredmetTipNastaveDTO> predmetTipoviNastaveDTOs = new ArrayList<PredmetTipNastaveDTO>();
	for (PredmetTipNastave predmetTipNastave : this.predmetTipoviNastave) {
	    predmetTipoviNastaveDTOs.add(predmetTipNastave.getDTOinsideDTO());
	}
	predmetDTO.setPredmetTipoviNastaveDTO(predmetTipoviNastaveDTOs);

	ArrayList<PredmetUslovniPredmetDTO> uslovniPredmeti = new ArrayList<PredmetUslovniPredmetDTO>();
	for (PredmetUslovniPredmet uslovniPredmet : this.uslovniPredmeti) {
	    uslovniPredmeti.add(uslovniPredmet.getDTOinsideDTO());
	}
	predmetDTO.setUslovniPredmetiDTO(uslovniPredmeti);

	return predmetDTO;
    }

    @Override
    public PredmetDTO getDTOinsideDTO() {
	PredmetDTO predmetDTO = new PredmetDTO();
	predmetDTO.setId(id);
	predmetDTO.setBrojCasovaZaIstrazivackeRadove(brojCasovaZaIstrazivackeRadove);
	predmetDTO.setBrojPredavanja(brojPredavanja);
	predmetDTO.setBrojVezbi(brojVezbi);
	predmetDTO.setEspb(espb);
	predmetDTO.setNaziv(naziv);
	predmetDTO.setObavezan(obavezan);
	predmetDTO.setStanje(stanje);
	predmetDTO.setTrajanjeUSemestrima(trajanjeUSemestrima);
	predmetDTO.setVersion(version);
	return predmetDTO;
    }

    @Override
    public void update(Predmet entitet) {
	this.setBrojCasovaZaIstrazivackeRadove(entitet.getBrojCasovaZaIstrazivackeRadove());
	this.setBrojPredavanja(entitet.getBrojPredavanja());
	this.setBrojVezbi(entitet.getBrojVezbi());
	this.setEspb(entitet.getEspb());
	this.setNaziv(entitet.getNaziv());
	this.setObavezan(entitet.getObavezan());
	this.setStanje(entitet.getStanje());
	this.setTrajanjeUSemestrima(entitet.getTrajanjeUSemestrima());
	this.setVersion(entitet.getVersion());

	this.setGodinaStudija(entitet.getGodinaStudija());
	this.setRealizacijaPredmeta(entitet.getRealizacijaPredmeta());
	this.setSilabus(entitet.getSilabus());
    }

}