package lms.model;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.RolaDTO;
import lms.enums.StanjeModela;
import lms.model.interfaces.Entitet;

@SQLDelete(sql = "UPDATE rola SET stanje = 'OBRISAN' WHERE id = ? AND version = ? ", check = ResultCheckStyle.COUNT)
@Table(name = "rola")
@Transactional
@Entity
public class Rola implements Entitet<Rola, Long> {
    /**
     * 
     */
    private static final long serialVersionUID = 2026044080551116327L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @Column(nullable = false, unique = true)
    private String naziv;

    @OneToMany(mappedBy = "rola")
    private List<RegistrovaniKorisnikRola> registrovaniKorisniciRola;

    public Rola() {
	super();
	this.registrovaniKorisniciRola = new java.util.ArrayList<>();
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getNaziv() {
	return naziv;
    }

    public void setNaziv(String naziv) {
	this.naziv = naziv;
    }

    public List<RegistrovaniKorisnikRola> getRegistrovaniKorisniciRola() {
	return registrovaniKorisniciRola;
    }

    public void setRegistrovaniKorisniciRola(List<RegistrovaniKorisnikRola> registrovaniKorisniciRola) {
	this.registrovaniKorisniciRola = registrovaniKorisniciRola;
    }

    public StanjeModela getStanje() {
	return stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((naziv == null) ? 0 : naziv.hashCode());
	result = prime * result + ((registrovaniKorisniciRola == null) ? 0 : registrovaniKorisniciRola.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Rola other = (Rola) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (naziv == null) {
	    if (other.naziv != null)
		return false;
	} else if (!naziv.equals(other.naziv))
	    return false;
	if (registrovaniKorisniciRola == null) {
	    if (other.registrovaniKorisniciRola != null)
		return false;
	} else if (!registrovaniKorisniciRola.equals(other.registrovaniKorisniciRola))
	    return false;
	return true;
    }

    @Override
    public RolaDTO getDTO() {
	RolaDTO rolaDTO = new RolaDTO();
	rolaDTO.setId(id);
	rolaDTO.setNaziv(naziv);
	rolaDTO.setStanje(stanje);
	rolaDTO.setVersion(version);

	rolaDTO.setRegistrovaniKorisniciRolaDTO(
		this.registrovaniKorisniciRola.stream().map(t -> t.getDTOinsideDTO()).collect(Collectors.toList()));

	return rolaDTO;
    }

    @Override
    public RolaDTO getDTOinsideDTO() {
	RolaDTO rolaDTO = new RolaDTO();
	rolaDTO.setId(id);
	rolaDTO.setNaziv(naziv);
	rolaDTO.setStanje(stanje);
	rolaDTO.setVersion(version);
	return rolaDTO;
    }

    @Override
    public void update(Rola entitet) {
	this.setNaziv(entitet.getNaziv());
	this.setStanje(entitet.getStanje());
	this.setVersion(entitet.getVersion());

    }

}
