package lms.dto;

import lms.dto.interfaces.DTO;
import lms.enums.StanjeModela;
import lms.interfaces.Identifikacija;

public class PitanjeDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -8889986573377487482L;

    private Long id;

    private String oblast;

    private String pitanje;

    private String putanjaZaSliku;

    private StanjeModela stanje;

    private Long version;

    private EvaluacijaZnanjaDTO evaluacijaZnanjaDTO;

    private java.util.List<OdgovorDTO> odgovoriDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getOblast() {
	return this.oblast;
    }

    public void setOblast(String oblast) {
	this.oblast = oblast;
    }

    public String getPitanje() {
	return this.pitanje;
    }

    public void setPitanje(String pitanje) {
	this.pitanje = pitanje;
    }

    public String getPutanjaZaSliku() {
	return this.putanjaZaSliku;
    }

    public void setPutanjaZaSliku(String putanjaZaSliku) {
	this.putanjaZaSliku = putanjaZaSliku;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public EvaluacijaZnanjaDTO getEvaluacijaZnanjaDTO() {
	return this.evaluacijaZnanjaDTO;
    }

    public void setEvaluacijaZnanjaDTO(EvaluacijaZnanjaDTO evaluacijaZnanjaDTO) {
	this.evaluacijaZnanjaDTO = evaluacijaZnanjaDTO;
    }

    public java.util.List<OdgovorDTO> getOdgovoriDTO() {
	return this.odgovoriDTO;
    }

    public void setOdgovoriDTO(java.util.List<OdgovorDTO> odgovoriDTO) {
	this.odgovoriDTO = odgovoriDTO;
    }

    public PitanjeDTO() {
	super();
	this.odgovoriDTO = new java.util.ArrayList<>();
    }

    public OdgovorDTO getOdgovorDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addOdgovorDTO(OdgovorDTO odgovorDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeOdgovorDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

}