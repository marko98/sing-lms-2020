package lms.dto;

import lms.dto.interfaces.DTO;
import lms.enums.StanjeModela;
import lms.interfaces.Identifikacija;

public class PredmetDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 5179330854110308446L;

    private Long id;

    private String naziv;

    private boolean obavezan;

    private Integer brojPredavanja;

    private Integer brojVezbi;

    private Integer brojCasovaZaIstrazivackeRadove;

    private Integer espb;

    private Integer trajanjeUSemestrima;

    private StanjeModela stanje;

    private Long version;

    private SilabusDTO silabusDTO;

    private GodinaStudijaDTO godinaStudijaDTO;

    private java.util.List<PredmetTipDrugogOblikaNastaveDTO> predmetTipoviDrugogOblikaNastaveDTO;

    private java.util.List<PredmetTipNastaveDTO> predmetTipoviNastaveDTO;

    private java.util.List<PredmetUslovniPredmetDTO> predmetiZaKojeSamUslovDTO;

    private java.util.List<PredmetUslovniPredmetDTO> uslovniPredmetiDTO;

    private RealizacijaPredmetaDTO realizacijaPredmetaDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getNaziv() {
	return this.naziv;
    }

    public void setNaziv(String naziv) {
	this.naziv = naziv;
    }

    public boolean getObavezan() {
	return this.obavezan;
    }

    public void setObavezan(boolean obavezan) {
	this.obavezan = obavezan;
    }

    public Integer getBrojPredavanja() {
	return this.brojPredavanja;
    }

    public void setBrojPredavanja(Integer brojPredavanja) {
	this.brojPredavanja = brojPredavanja;
    }

    public Integer getBrojVezbi() {
	return this.brojVezbi;
    }

    public void setBrojVezbi(Integer brojVezbi) {
	this.brojVezbi = brojVezbi;
    }

    public Integer getBrojCasovaZaIstrazivackeRadove() {
	return this.brojCasovaZaIstrazivackeRadove;
    }

    public void setBrojCasovaZaIstrazivackeRadove(Integer brojCasovaZaIstrazivackeRadove) {
	this.brojCasovaZaIstrazivackeRadove = brojCasovaZaIstrazivackeRadove;
    }

    public Integer getEspb() {
	return this.espb;
    }

    public void setEspb(Integer espb) {
	this.espb = espb;
    }

    public Integer getTrajanjeUSemestrima() {
	return this.trajanjeUSemestrima;
    }

    public void setTrajanjeUSemestrima(Integer trajanjeUSemestrima) {
	this.trajanjeUSemestrima = trajanjeUSemestrima;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public SilabusDTO getSilabusDTO() {
	return this.silabusDTO;
    }

    public void setSilabusDTO(SilabusDTO silabusDTO) {
	this.silabusDTO = silabusDTO;
    }

    public GodinaStudijaDTO getGodinaStudijaDTO() {
	return this.godinaStudijaDTO;
    }

    public void setGodinaStudijaDTO(GodinaStudijaDTO godinaStudijaDTO) {
	this.godinaStudijaDTO = godinaStudijaDTO;
    }

    public java.util.List<PredmetTipDrugogOblikaNastaveDTO> getPredmetTipoviDrugogOblikaNastaveDTO() {
	return this.predmetTipoviDrugogOblikaNastaveDTO;
    }

    public void setPredmetTipoviDrugogOblikaNastaveDTO(
	    java.util.List<PredmetTipDrugogOblikaNastaveDTO> predmetTipoviDrugogOblikaNastaveDTO) {
	this.predmetTipoviDrugogOblikaNastaveDTO = predmetTipoviDrugogOblikaNastaveDTO;
    }

    public java.util.List<PredmetTipNastaveDTO> getPredmetTipoviNastaveDTO() {
	return this.predmetTipoviNastaveDTO;
    }

    public void setPredmetTipoviNastaveDTO(java.util.List<PredmetTipNastaveDTO> predmetTipoviNastaveDTO) {
	this.predmetTipoviNastaveDTO = predmetTipoviNastaveDTO;
    }

    public java.util.List<PredmetUslovniPredmetDTO> getPredmetiZaKojeSamUslovDTO() {
	return this.predmetiZaKojeSamUslovDTO;
    }

    public void setPredmetiZaKojeSamUslovDTO(java.util.List<PredmetUslovniPredmetDTO> predmetiZaKojeSamUslovDTO) {
	this.predmetiZaKojeSamUslovDTO = predmetiZaKojeSamUslovDTO;
    }

    public java.util.List<PredmetUslovniPredmetDTO> getUslovniPredmetiDTO() {
	return this.uslovniPredmetiDTO;
    }

    public void setUslovniPredmetiDTO(java.util.List<PredmetUslovniPredmetDTO> uslovniPredmetiDTO) {
	this.uslovniPredmetiDTO = uslovniPredmetiDTO;
    }

    public RealizacijaPredmetaDTO getRealizacijaPredmetaDTO() {
	return this.realizacijaPredmetaDTO;
    }

    public void setRealizacijaPredmetaDTO(RealizacijaPredmetaDTO realizacijaPredmetaDTO) {
	this.realizacijaPredmetaDTO = realizacijaPredmetaDTO;
    }

    public PredmetDTO() {
	super();
	this.predmetTipoviDrugogOblikaNastaveDTO = new java.util.ArrayList<>();
	this.predmetTipoviNastaveDTO = new java.util.ArrayList<>();
	this.predmetiZaKojeSamUslovDTO = new java.util.ArrayList<>();
	this.uslovniPredmetiDTO = new java.util.ArrayList<>();
    }

    public PredmetDTO getPreduslov(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addPreduslov(PredmetDTO preduslov) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removePreduslov(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public PredmetTipNastaveDTO getPredmetTipNastaveDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addPredmetTipNastaveDTO(PredmetTipNastaveDTO predmetTipNastaveDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removePredmetTipNastaveDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public PredmetTipDrugogOblikaNastaveDTO getPredmetTipDrugogOblikaNastaveDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addPredmetTipDrugogOblikaNastaveDTO(
	    PredmetTipDrugogOblikaNastaveDTO predmetTipDrugogOblikaNastaveDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removePredmetTipDrugogOblikaNastaveDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public PredmetUslovniPredmetDTO getPredmetZaKojiSamUslovDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addPredmetZaKojiSamUslovDTO(PredmetUslovniPredmetDTO predmetUslovniPredmetDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removePredmetZaKojiSamUslovDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public PredmetUslovniPredmetDTO getUslovniPredmetDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean addUslovniPredmetDTO(PredmetUslovniPredmetDTO predmetUslovniPredmetDTO) {
	throw new RuntimeException("Not Implmented");
    }

    public boolean removeUslovniPredmetDTO(Identifikacija<Long> identifikacija) {
	throw new RuntimeException("Not Implmented");
    }

}