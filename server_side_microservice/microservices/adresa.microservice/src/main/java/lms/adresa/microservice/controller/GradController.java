package lms.adresa.microservice.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import lms.adresa.microservice.service.GradService;
import lms.controller.CrudController;
import lms.dto.GradDTO;
import lms.dto.interfaces.DTO;
import lms.model.Grad;

@Controller
@Scope("singleton")
@RequestMapping(path = "grad")
@CrossOrigin(origins = "*")
public class GradController extends CrudController<Grad, Long> {
    @RequestMapping(path = "/drzava/{id}", method = RequestMethod.GET)
    public ResponseEntity<Page<GradDTO>> findByCountryId (@PathVariable("id") Long id, Pageable pageable) {
	
//	get Page<T>
	Page<Grad> page = ((GradService)this.service).findByDrzavaId(id, pageable);

//	map T to DTO and get Page<DTO>
	Page<GradDTO> pageDTO = page.map(t -> t.getDTO());
	
	return new ResponseEntity<Page<GradDTO>>(pageDTO, HttpStatus.OK);
    }
    
    @RequestMapping(path = "/drzava/{id}/naziv/{nazivGrada}", method = RequestMethod.GET)
    public ResponseEntity<Page<GradDTO>> findByCountryId (@PathVariable("id") Long id, @PathVariable("nazivGrada") String nazivGrada, Pageable pageable) {
	
//	get Page<T>
	Page<Grad> page = ((GradService)this.service).findByDrzavaIdIGradNaziv(id, nazivGrada, pageable);

//	map T to DTO and get Page<DTO>
	Page<GradDTO> pageDTO = page.map(t -> t.getDTO());
	
	return new ResponseEntity<Page<GradDTO>>(pageDTO, HttpStatus.OK);
    }
    
    @RequestMapping(path = "/naziv/{naziv}", method = RequestMethod.GET)
    public ResponseEntity<Page<DTO<Long>>> findAll(@PathVariable(value = "naziv") String naziv, Pageable pageable) {
//	System.out.println("Pageable: " + pageable); // Page request [number: 0, size 20, sort: UNSORTED]
	
//	get Page<T>
	Page<Grad> page = ((GradService)this.service).findByNaziv(naziv, pageable);

//	map T to DTO and get Page<DTO>
	Page<DTO<Long>> pageDTO = page.map(t -> t.getDTO());

	return new ResponseEntity<Page<DTO<Long>>>(pageDTO, HttpStatus.OK);
    }
}