package lms.adresa.microservice.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import lms.adresa.microservice.service.DrzavaService;
import lms.controller.CrudController;
import lms.dto.interfaces.DTO;
import lms.model.Drzava;

@Controller
@Scope("singleton")
@RequestMapping(path = "drzava")
@CrossOrigin(origins = "*")
public class DrzavaController extends CrudController<Drzava, Long> {

    @RequestMapping(path = "/naziv/{naziv}", method = RequestMethod.GET)
    public ResponseEntity<Page<DTO<Long>>> findAll(@PathVariable(value = "naziv") String naziv, Pageable pageable) {
//	System.out.println("Pageable: " + pageable); // Page request [number: 0, size 20, sort: UNSORTED]
	
//	get Page<T>
	Page<Drzava> page = ((DrzavaService)this.service).findByNaziv(naziv, pageable);

//	map T to DTO and get Page<DTO>
	Page<DTO<Long>> pageDTO = page.map(t -> t.getDTO());

	return new ResponseEntity<Page<DTO<Long>>>(pageDTO, HttpStatus.OK);
    }
    
}