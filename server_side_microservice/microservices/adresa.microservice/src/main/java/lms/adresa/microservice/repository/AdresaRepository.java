package lms.adresa.microservice.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.Adresa;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class AdresaRepository extends CrudRepository<Adresa, Long> {

}