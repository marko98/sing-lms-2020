package lms.adresa.microservice.service;

import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import lms.adresa.microservice.repository.interfaces.GradPASRepository;
import lms.model.Grad;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class GradService extends CrudService<Grad, Long> {
    public Page<Grad> findByDrzavaId(Long id, Pageable pageable) {
	return ((GradPASRepository)this.repository.getRepository()).findByDrzavaId(id, pageable);
    }
    
    public Page<Grad> findByDrzavaIdIGradNaziv(Long id, String nazivGrada, Pageable pageable) {
	return ((GradPASRepository)this.repository.getRepository()).findByDrzavaIdIGradNaziv(id, nazivGrada, pageable);
    }
    
    public Page<Grad> findByNaziv (String naziv, Pageable pageable) {
	return ((GradPASRepository)this.repository.getRepository()).findByNaziv(naziv, pageable);
    }
}