package lms.adresa.microservice.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import lms.model.Grad;

@Repository
@Scope("singleton")
public interface GradPASRepository extends PagingAndSortingRepository<Grad, Long> {
    @Query("select g from Grad g where g.drzava.id = :id")
    public Page<Grad> findByDrzavaId(@Param("id") Long id, Pageable pageable);
    
    @Query("select g from Grad g where g.drzava.id = :id and g.naziv LIKE :nazivGrada%")
    public Page<Grad> findByDrzavaIdIGradNaziv(@Param("id") Long id, @Param("nazivGrada") String nazivGrada, Pageable pageable);
    
    @Query("select g from Grad g where g.naziv LIKE :naziv%")
    public Page<Grad> findByNaziv (@Param("naziv") String naziv, Pageable pageable);
}