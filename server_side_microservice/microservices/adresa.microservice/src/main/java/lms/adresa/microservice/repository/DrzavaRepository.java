package lms.adresa.microservice.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.Drzava;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class DrzavaRepository extends CrudRepository<Drzava, Long> {

}