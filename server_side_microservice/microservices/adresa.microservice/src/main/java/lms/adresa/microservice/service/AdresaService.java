package lms.adresa.microservice.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lms.model.Adresa;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class AdresaService extends CrudService<Adresa, Long> {

}