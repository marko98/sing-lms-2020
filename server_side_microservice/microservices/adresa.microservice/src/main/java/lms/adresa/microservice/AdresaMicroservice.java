package lms.adresa.microservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jms.annotation.EnableJms;

@SpringBootApplication
@ComponentScan(basePackages = { "lms.*" })
@EntityScan(basePackages = { "lms.model" })
@EnableJpaRepositories({ "lms.adresa.microservice.repository", "lms.security.repository" })
@EnableJms
@EnableEurekaClient
public class AdresaMicroservice extends SpringBootServletInitializer {

    public static void main(String args[]) {
	SpringApplication.run(AdresaMicroservice.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
	return builder.sources(AdresaMicroservice.class);
    }
}
